function varargout = extract(s1,s2,varargin)
%EXTRACT Extract data from HDB (ORACLE)
%
%[DATA1,DATA2,...]=HDBO.EXTRACT(T1,T2,SIGNAL1,SIGNAL2,...)
%
%T1:        Start time, as a date string (dd/MM/yyyy HH:mm:ss) or date number
%T2:        End time, as a date string or date number
%SIGNALn:   String identifying the signal (ex.: 'sr/d-ct/1/current')
%
%DATAn:     hdb.series containing the data
%
%DATA=HDBO.EXTRACT(T1,T2,SIGNAL1,SIGNAL2,...)
%
%DATA       1xN array of hdb.series containing the data
%
%[...]=HDBO.EXTRACT(T1,T2,INTERVAL,...)
%           Resample the data at the given interval [s]
%
%See also: HDB.CONNECT, HDB.EXTRACT, HDB.SERIES

[varargout{1:nargout}]=hdb.connect('ORACLE').extract(s1,s2,varargin{:});

end