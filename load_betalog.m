function [save_bpm,save_steer,nux,nuz,periods,alpha,ll,strname]=load_betalog(path)
%[BPM,STEER,NUX,NUZ,PERIODS,ALPHA,C,NAME]=LOAD_BETALOG	Load optics parameters
%	Loads "betalog.mat" if existing, otherwise scans the file "BETALOG"
%	in the current directory and returns
%	BPM parameters (name PICK) and STEERER parameters (SX elements).
%
%	theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux
%
% deprecated : use LOAD_OPTICS instead

if nargin < 1, path=pwd; end
[params,save_bpm,save_steer]=load_optics(path,'bpm','sx');
nux=params.nuh;
nuz=params.nuv;
periods=params.periods;
alpha=params.alpha;
ll=params.ll;
strname=params.strname;
