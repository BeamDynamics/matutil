function [xt,zt,it]=load_cortable(fname)
%[xtable,ztable,intens]=LOAD_CORTABLE(file_name) load BPM correction table

global APPHOME
if nargin < 1, fname=fullfile(APPHOME,'sr','bpm','correction_table');end
[s,w]=unix(['wc -l <' fname]); 
if s==0
length=str2double(w)/225;
[s,w]=unix(['split -l 225 -a 1 ' fname ' /tmp/cor']); %#ok<NASGU>
end
if s~=0, error('file:erraccess','Error accessing file %s',fname); end
num=0;
suflist='abcdefghijklmnopqrstuvwxyz';
for suffix=suflist(1:length)
   [x,z,i]=load_orbit(['/tmp/cor' suffix]);
   num=num+1;
   it(num)=i; %#ok<AGROW>
   xt(:,num)=x; %#ok<AGROW>
   zt(:,num)=z; %#ok<AGROW>
end
unix('rm /tmp/cor[a-z]');
