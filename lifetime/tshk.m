function y=tshk(v,code)

nv=length(v);

y1=-3*exp(-v);

tmp=zeros(nv,1);
y2=zeros(nv,1);
[vs vi]=sort(v);
ws=[vs(2:nv);max(2*vs(nv),20)];
for i=1:nv
	tmp(i)=quad8('lnu',vs(i),ws(i));
end
y2(vi)=flipud(cumsum(flipud(tmp)));
y2=v.*y2;


y3=(3*v-v.*log(v)+2).*expint(v);

if nargin > 1
	y=(0.5*sqrt(v)*ones(1,3)).*[y1 y2 y3];
else
	y=0.5*sqrt(v).*(y1 + y2 + y3);
end
return
