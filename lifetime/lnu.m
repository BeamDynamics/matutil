function y=lnu(u)

if isinf(u)
	y=zeros(size(u));
else
	y=log(u)./exp(u)./u;
end
return
