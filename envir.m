function envir
%ENVIR sets global variables PRINTER from machine environment

global PRINTER
PRINTER=getenv('PSDEST');
if isempty(PRINTER)
   PRINTER=input('Printer name :','s');
end
