function [at,args,mach] = getmodelat(varargin)
%GETMODELAT		% scan an argument list looking for machine structure
%
%[AT,ARGS,MACH]=GETMODELAT(VARARGIN) scans the input arguments looking for:
%
%- ['sr'|'sy'|'ebs',]atstruct
%           machine name, Accelerator Toolbox structure
%
%- ['sr'|'sy'|'ebs',]'nombz2.5'
%           machine and optics name
%
%- 'sr'|'sy'|'ebs'
%           machine name, path defaults to $(APPHOME)/mach/optics/settings/theory
%
%- ['sr'|'sy'|'ebs',]'/machfs/appdata/sr/optics/settings/theory':
%           machine name, full path. It looks in path for:
%               path/betamodel.mat or
%               path/betamodel.str or
%               path is an ORM measurement directory. In that case,
%          GETMODELAT(...,'QuadCor') uses the computed quad corrections
%          GETMODELAT(...,'SkewCor') uses the computed skew corrections
%
%AT:	Resulting AT structure
%ARGS:	remaining argument
%MACH:	'sr', 'sy' or 'ebs'
%
%[...] = GETMODELAT(...,'Tunes',[nux nuz],...)
%   Retune the ring to the desired tunes

[pth,args,mach]=getmodelpath(varargin{:});
[tunes,args]=getoption(args,'Tunes',[]);
[qcor,args]=getflag(args,'QuadCor');
[scor,args]=getflag(args,'SkewCor');
if length(args) >= 1 && iscell(args{1})
    fprintf('initializing the optics using AT structure\n')
    at=args{1};
    args(1)=[];
elseif isdir(pth)
    try
        at=load_at(pth,mach);
    catch err2
        if ~strcmp(err2.identifier,'ReadBeta:couldNotReadFile'), err2.rethrow(); end
        at=qemsem(pth,qcor,scor);
    end
else
    at=load_mat(pth);
end
if ~isempty(tunes)
    at=reshape(atfittune(at(:),tunes,'QD6','QF7'),size(at));
end

    function at=load_mat(matfile)
        a=load(matfile);
        at=a.betamodel;
        fprintf('initializing the optics using %s\n',matfile);
    end

    function at=load_at(path,mach)
        try
            at=load_mat(fullfile(path,'betamodel.mat'));
        catch err
            if ~strcmp(err.identifier,'MATLAB:load:couldNotReadFile'), err.rethrow(); end
            check=selectplane(mach,'choices',{'sr','sy','ebs'},{@sr.model.checklattice,@(rg) rg,@(rg) rg});
            disp(err.message);
            betafile=fullfile(path,'betamodel.str');
            at=check(atreadbeta(betafile));
            fprintf('initializing the optics using %s\n',betafile);
        end
    end

    function at=qemsem(dirname,qcor,scor)
        [qemres,~,qemb]=qempanelset(dirname);           % load quad corrections
        [qemb(2).kn,qemb(2).dipdelta,qemb(2).dxs]=...   % load quad errors
            qemerrload(qemres,fullfile(qemres.datadir,'quaderrors.mat'));
        s=load(fullfile(qemres.datadir,'skewerrors.mat'));  % load skew errors
        qemb(2).ks=s.ks;
        qemb(2).diptilt=s.diprot;
        if qcor
            qemb(2).cor=load_correction(fullfile(qemres.datadir,'cornew.dat'),...
                qemres.qpcode,qemres.opticsdir);        % load quad corrections
        end
        if scor
            qemb(2).skewcor=load_correction(fullfile(qemres.datadir,'skewnew.dat'),...
                qemres.skcode,qemres.opticsdir);        % load skew corrections
        else
            qemb(2).skewcor=load_correction(fullfile(qemres.datadir,'skewcor.dat'),...
                qemres.skcode,qemres.opticsdir);        % load skew corrections
        end
        
        at=qemat(qemres,qemb(2),true);
    end

end

