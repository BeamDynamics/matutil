function startoctave(rootpath)
global APPHOME

   if nargin < 1, rootpath='/operation/machine'; end
optpath(...
      fullfile(rootpath,'octmex',getenv('MACHARCH')),...
      fullfile(rootpath,'matlab'));%   ,'-end');

if isempty(APPHOME), APPHOME=getenv('APPHOME'); end

function optpath(varargin)
for i=nargin:-1:1
   if (exist(varargin{i},'dir') == 7),addpath(genpath(varargin{i})); end
end
