function varargout=varextract(dirlist,filename,varargin)
%[V1,V2,...]=VAREXTRACT(DIRLIST,FILENAME,VAR1,VAR2,...)
%DIRLIST:   cell array containing selected directories
%FILENAME:  filename present in each directory from DIRLIST
%
%V1:        cell array containing the variables VAR1 of each selected file
%
nvars=nargin-2;
varargout=cell(1,nvars);
nf=length(dirlist);
for s=1:nvars
    varargout{s}=cell(1,nf);
end
for f=1:nf
    a=load(fullfile(dirlist{f},filename));
    for s=1:nvars
        if isfield(a,varargin{s})
            varargout{s}{f}=a.(varargin{s});
        end
    end
end
