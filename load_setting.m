function [devval,devname]=load_setting(appliname, filename, devname, defval)
%LOAD_SETTING reads application settings file
%
%[VALUE,DEVNAME]=LOAD_SETTING(APPLI,FILENAME)
%    reads a binary application file and returns the values and device names
%
%[VALUE,DEVNAME]=LOAD_SETTING('binaryfile',FILEPATH)
%    reads a binary file and returns the values and device names
%
%[VALUE,DEVNAME]=LOAD_SETTING('textfile',FILEPATH)
%    reads a text file and returns the values and device names
%
%[VALUE,DEVNAME]=LOAD_SETTING('appli','appliname:filename')
%    reads a binary application file and returns the values and device names
%
%[VALUE,DEVNAME]=LOAD_SETTING('tango','appliname:filename')
%    uses the tango settings manager and returns the values and device names
%
%[VALUE,DEVNAMES]=LOAD_SETTING('string',INPUT)
%    scan the string INPUT and returns the values and device names
%
%VALUE=LOAD_SETTING(...,DEVNAMES)
%    looks for the requested devices and sets the missing values to NaN
%
%VALUE=LOAD_SETTING(...,DEVNAMES,DEFAULTVALUE)
%    looks for the requested devices
%

if nargin < 4, defval=NaN; end
switch appliname
    case {'','binaryfile'}
        [list,val]=getappli(filename);
    case 'textfile'
        fid=fopen(filename,'rt');
        if (fid == -1)
            error('LoadSetting:FileError','Cannot read %s',filename);
        end
        [list,val]=decode(fid,'%s%f');
        fclose(fid);
    case 'string'
        [list,val]=decode(filename,'%s%f');
    case 'tango'
        applifile=strsplit(filename,':');
        [tgappli,tgfile]=deal(applifile{:});
        txt=tango.Device(['sys/settings/' tgappli]).GetSettingsFileContent(tgfile);
        [list,val]=decode(txt,'%s %f','Delimiter',':','CommentStyle','#');
    case 'appli'
        applifile=strsplit(filename,':');
        [tgappli,tgfile]=deal(applifile{:});
        [list,val]=getappli(['"' tgappli '" ' tgfile]);
    otherwise
        [list,val]=getappli(['"' appliname '" ' filename]);
end
if nargin < 3
    [devval,devname]=deal(val,list);
else
    nl=length(list);
    devval=cellfun(@dscan,devname);
end

    function vv=dscan(dname)
        cpt=tango.split(dname);
        lookedfor=strjoin(cpt(3:5),'/');
        for jj=1:nl
            if ~isempty(strfind(lookedfor,list{jj}))
                vv=val(jj);
                break;
            end
        end
    end

    function [a,b]=decode(varargin)
        data=textscan(varargin{:});
        [a,b]=deal(data{:});
    end

    function [list,val]=getappli(filen)
        [status,message]=unix(['prfile ' filen ' 2>/dev/null']); 
        if (status ~= 0)
            error('LoadSetting:FileError','Cannot read %s',filen);
        end
        [list,val]=decode(message,'%s%f');
    end
end
