function [varargout] = min2(varargin)
%MIN2    Smallest component. Same as min

if nargout == 0
  builtin('min', varargin{:});
else
  [varargout{1:nargout}] = builtin('min', varargin{:});
end
