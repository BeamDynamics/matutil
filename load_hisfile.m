function [m,t] = load_hisfile(filename)
%[M,T] = LOAD_HISFILE(file_name) loads history file
% M: matrix containing the data
% T: column one is processed as UNIX time converted into datenums

disp(filename)
[s,w]=unix(['matlab_hisfile ' filename ' > /tmp/mat.his']);
%m=load('/tmp/mat.his');
m=dlmread('/tmp/mat.his','\t');
%t=(m(:,1)-m(1,1))/3600;
t=datenum(1970,1,1,0,0,0)+m(:,1)/24/3600;
delete /tmp/mat.his
