function [strength,devlist]=read_correction(code,varargin)
%READ_CORRECTION    Read corrector values from the control system
%
%CURRENT=READ_CORRECTION(CODE)
%   Return the corrector currents
%
%CODE	1: 16 quad correctors
%	2: 16 skew quad correctors
%	3: 12 sextupole correctors
%	4:  8 horizontal correctors
%	5:  8 vertical correctors
%	6: 32 quad correctors      106: without qp-n4/c4
%	7: 32 skew correctors
%	8: all H steerers
%	9: all V steerers
%      10: 64 skew correctors      110: with qp-s4/c4
%      11: 12 sextupole corrector + 8 indep. sextupoles
%
%STRENGTH=READ_CORRECTION(CODE,OPTICS)
%   Return the corrector strengths
%
%OPTICS   : may be one of the following:
%
%- 'sr'
%           machine name, path defaults to $(APPHOME)/mach/optics/settings/theory
%- ['sr',]'/machfs/appdata/sr/optics/settings/opticsname':
%           full path of optics directory
%- ['sr',]'opticsname'
%           machine and optics name
%
%[...,DEVNAME]=READ_CORRECTION(CODE,...)
%   Return in addition the device names

[list,devlist]=selcor(code,'%s/Current'); %#ok<ASGLU>
strength=cellfun(@(name) tango.Attribute(name).setpoint('',NaN),devlist);
if ~isempty(varargin)
    strength=strength.*reshape(load_corcalib(varargin{:},code),size(strength));
end
end
