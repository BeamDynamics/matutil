function [varargout] = max2(varargin)
%MAX    Largest component. SAme as max

if nargout == 0
  builtin('max', varargin{:});
else
  [varargout{1:nargout}] = builtin('max', varargin{:});
end
