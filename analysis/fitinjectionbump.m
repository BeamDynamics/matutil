function k1234=fitinjectionbump(line,ampl,centreidx,centredata,kickers,k4data)

centregoal=@(x) goal(line(1:centreidx),kickers(1:2),x,[ampl;0;0;0;0;0],centredata);
k12=fminsearch(centregoal,[0;0],...
	optimset(optimset('fminsearch'),'Display','iter','TolX',1.e-6));

line=setcellstruct(line,'KickAngle',kickers(1:2),k12,1);

endgoal=@(x) goal(line,kickers(3:4),x,[0;0;0;0;0;0],k4data);
k34=fminsearch(endgoal,[k12(2);k12(1)],...
	optimset(optimset('fminsearch'),'Display','iter','TolX',1.e-6));
k1234=[k12;k34];


function chi2=goal(line,varpt,valtest,rgoal,data)
for i=1:length(varpt)
    line{varpt(i)}.KickAngle(1)=valtest(i);
end
rout=linepass(line,zeros(6,1))-rgoal;
chi2=invar(rout,data);

function chi2=invar(r,data)
x=r(1);
xp=data.alpha(1)*r(1)+data.beta(1)*r(2);
chi2=(x*x+xp*xp)/data.beta(1);