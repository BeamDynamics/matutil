[tuned,periods]=atreadbeta('/machfs/laurent/dbeta/noqd1qd8betaz3_5.str');
[vqp,vs4,vs6]=atgetparam(tuned,'QP',{'PolynomB',3},'S4',{'PolynomB',3},'S6');
esrf=atreadbeta('/machfs/laurent/dbeta/injectionbz2.5.str');
kickers=[findcells(esrf,'FamName','K1') findcells(esrf,'FamName','K2') ...
         findcells(esrf,'FamName','K3') findcells(esrf,'FamName','K4')];
esrf=setcellstruct(esrf,'PassMethod',kickers,'CorrectorPass');
ringidx=[kickers(1)-2:length(esrf) 1:kickers(4)+2];
injidx=zeros(1,length(esrf));
injidx(ringidx)=1:length(ringidx);
allpts=1:(length(ringidx)+1);

esrf=atsetparam(esrf,'QP',vqp,{'PolynomB',3},'S4',vs4,{'PolynomB',3},'S6',vs6);
lindata=atlinopt(esrf,0,[1 kickers(4)+3]);         % save optical parameters
ering=atsetparam(esrf,{'PolynomB',3},'S4',0.4,{'PolynomB',3},'S6',-1.12236905);
%[vs4,vs6]=atgetparam(ering,{'PolynomB',3},'S4',{'PolynomB',3},'S6')

%inj=esrf(ringidx);
inj=ering(ringidx);

k1234=fitinjectionbump(inj,-0.011,injidx(1),lindata(1),injidx(kickers),lindata(2));

npl=11;
rout=zeros(6,npl);
ampl=linspace(0,1,npl);
for i=1:npl
    inj=setcellstruct(inj,'KickAngle',injidx(kickers),ampl(i)*k1234,1);
    rout(:,i)=linepass(inj,zeros(6,1),length(inj)+1);
end
eps=atinvariant(rout,lindata(2));
a=sqrt(lindata(1).beta(1)*eps);

s=findspos(inj,allpts);
rout=linepass(inj,zeros(6,1),allpts);
plot(s,rout(1,:))
