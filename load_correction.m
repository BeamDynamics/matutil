function [strength,devlist]=load_correction(filename,code,varargin)
%STRENGTH=LOAD_CORRECTION(FILENAME,CODE,OPTICS)		Read corrector strengths
%
%FILENAME:	text file containing corrector currents
%CODE	1: 16 quad correctors
%	2: 16 skew quad correctors
%	3: 12 sextupole correctors
%	4:  8 horizontal correctors
%	5:  8 vertical correctors
%	6: 32 quad correctors      106: without qp-n4/c4
%	7: 32 skew correctors
%	8: all H steerers
%	9: all V steerers
%      10: 64 skew correctors      110: with qp-s4/c4
%      11: 12 sextupole corrector + 8 indep. sextupoles
%
%OPTICS   : may be one of the following:
%
%- 'sr'|'sy'
%           machine name, path defaults to $(APPHOME)/mach/optics/settings/theory
%- ['sr'|'sy',]'/machfs/appdata/sr/optics/settings/opticsname':
%           full path of optics directory
%- ['sr'|'sy',]'opticsname'
%           machine and optics name
%
%[STRENGTH,DEVNAME]=LOAD_CORRECTION(...) also outputs device names

[fp,message]=fopen(filename,'rt');
if fp < 0
    error('File:Open','%s: %s',message,filename);
else
    fprintf('Loading corrections from %s\n',filename);
    data=textscan(fp,'%s%f');
    fclose(fp);
end

[dvname,dvcur]=deal(data{:});

if strncmpi(dvname{1},'tango:',6)
    [list,devlist]=selcor(code); %#ok<ASGLU>
else
    [list,devlist]=selcor(code,'%s'); %#ok<ASGLU>
end

current=zeros(size(devlist));
for i=1:length(dvname)
    current(strcmpi(dvname{i},devlist))=dvcur(i);
end
strength=current.*reshape(load_corcalib(varargin{:},code),size(current));
