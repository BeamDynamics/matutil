function legend=gcl

ha=gca;
hl=findobj(get(gcf,'Children'),'flat','Type','axes');
for hll=(hl(:))'
   tmp=get(hll,'userdata');
   eval('ok=(tmp(2)==ha);','ok=0;');
   if (ok),legend=hll;end
end
