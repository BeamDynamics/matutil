function [newdz,sextresponse]=sempanelfits(qemb,qemres,semres,varargin)
%SEMPANELFITS Vary sextupole vertical position to fit the measured response matrix
%NEWDZ=SEMPANELFITS(MACH,QEMRES,SEMRES,INIKS,OKVAR,OKBPM)
%
%QEMB:
%SEMRES:    global parameters (hlist,vlist,sxl,sextresponse,resph,respv,frespz)
%OKVAR:     Select a subset of variable parameters (default: all)
%OKBPM:     Select a subset of valid BPMS (default:all)
%
%NEWDZ:     New sextupole position

if isfield(semres,'sextresponse')
    sextresponse=semres.sextresponse;
else
    sextresponse=semquadresp(qemb,qemres,semres,varargin{:});
end
orbit0=findsyncorbit(qemb.at,qemres.ct,qemres.bpmidx);

[rh,rh2v,rv2h,rv,frh,frv]=qemdecode(qemb.at,qemres.ct,qemres,...
    qemres.resph,semres.respv,semres.resph,qemres.respv,...
    qemres.frespx,semres.frespz,...
    qemres.khrot,qemres.khgain,qemres.kvrot,qemres.kvgain,...
    qemres.brot,qemres.bhgain,qemres.bvgain); %#ok<ASGLU>
[rh0,rh2v0,rv2h0,rv0,frh0,frv0]=qemcode(qemb.at,qemres.ct,...
    qemres.steerhidx(qemres.hlist),qemres.steervidx(qemres.vlist),...
    qemres.bpmidx,[],[],[],[],[],[],[],orbit0); %#ok<ASGLU>

resp0=[rh2v0(:);rv2h0(:);frv0];
resp=[rh2v(:);rv2h(:);semres.bvscale*frv];
mode=struct('vnorm',sqrt(ones(7,1)));
newdz=qemb.dzs+semerrfit(length(semres.hlist),length(semres.vlist),resp-resp0,...
    sextresponse,mode,varargin{:});
end
