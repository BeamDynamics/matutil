function sempanelplot(fig,plotcode,qemb0,qemb,qemres)
%SEMPANELPLOT Plot various lattice parameters
%   Detailed explanation goes here

sbpm=cat(1,qemb.lindata.SPos);
sskew=findspos(qemb.at(:),qemres.skewidx);
figure(fig);
switch plotcode
   case 1       % ID25 spot
	  semplotemit(1.0e12*qemb.id25data.beam66([1 3],[1 3]))
	  xlabel('x [\mum]');
	  ylabel('z [\mum]');
   case 2       % D9 spot
	  semplotemit(1.0e12*qemb.d9data.beam66([1 3],[1 3]))
	  xlabel('x [\mum]');
	  ylabel('z [\mum]');
   case 3       % D9 spot
	  semplotemit(1.0e12*qemb.d11lensdata.beam66([1 3],[1 3]))
	  xlabel('x [\mum]');
	  ylabel('z [\mum]');
   case 4       % RDT
	  [f1,f2]=semrdt(qemb0.at,qemres,qemb.ks,qemb.skewcor(:,end));
	  plot(sbpm,[abs(f1) abs(f2)]);
	  ax=axis;
	  axis([0 844.39 0 ax(4)]);
	  plotskew(sskew);
	  xlabel('s [m]');
	  ylabel('RDT');
	  grid on
   case 5       % Gamma-1
	  gamma=cat(1,qemb.lindata.gamma);
	  plot(sbpm,gamma-1);
	  ax=axis;
	  ym=max(abs(ax([3 4])));
	  axis([0 844.39 -ym ym]);
	  plotskew(sskew);
	  xlabel('s [m]');
	  ylabel('Coupling factor \gamma-1');
	  grid on
   otherwise
end
end

function plotskew(sskew)
ns=size(sskew,2);
ylim=get(gca,'Ylim');
hold('on');
plot([sskew;sskew],repmat([0 0;-.1 .1]*ylim',1,ns),'k');
hold('off');
end
