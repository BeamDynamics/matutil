function varargout = sempanel2(varargin)
% SEMPANEL2 M-file for sempanel2.fig
%      SEMPANEL2, by itself, creates a new SEMPANEL2 or raises the existing
%      singleton*.
%
%      H = SEMPANEL2 returns the handle to a new SEMPANEL2 or the handle to
%      the existing singleton*.
%
%      SEMPANEL2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEMPANEL2.M with the given input arguments.
%
%      SEMPANEL2('Property','Value',...) creates a new SEMPANEL2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sempanel2_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sempanel2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sempanel2

% Last Modified by GUIDE v2.5 22-Mar-2018 16:54:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @sempanel2_OpeningFcn, ...
    'gui_OutputFcn',  @sempanel2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before sempanel2 is made visible.
function sempanel2_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sempanel2 (see VARARGIN)

% Choose default command line output for sempanel2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes sempanel2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);
set(handles.popupmenu2,'Value',2);
set(handles.edit4,'String','0.4');
set([handles.text5 handles.edit4],'Visible','on');
handles.semdata=[];
guidata(hObject,handles);


% --- Outputs from this function are returned to the command line.
function varargout = sempanel2_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1: select data directory
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
%wrongbpms=sr.bpmindex(6,4);	% Eliminate BPM C6-4
semres.iaxemz=semloadiax(qemres.datadir);
handles.semdata=sempanelset(qemres.datadir,handles);
qemb(2).skewcor=load_correction(fullfile(qemres.datadir,'skewcor.dat'),...
    qemres.skcode,qemres.opticsdir);
set(handles.pushbutton3,'Enable','On');
qemb(2)=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set(handles.edit1,'String',qemres.datadir);
guidata(hObject,handles);
set(handles.statustext,'String','initialized working directory');


function edit1_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3: Errors->Fit quads
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
set(handles.statustext,'String','fitting quadrupoles');
semres.wdisp=str2double(get(handles.edit3,'String'));
if get(handles.AnalyticFitCheck,'Value') == 1
    hw=waitbar(0,'Fitting quadrupole rotations...');
else
    hw=waitbar(0,'Fitting quadrupole rotations...');
end
dispfunc=@(i,itot) waitbar(i/itot,hw);

if get(handles.AnalyticFitCheck,'Value') == 1
    [qemb(2).ks,semres.quadresponse]=sempanelfitqAnalytic(qemb(2),qemres,semres,dispfunc);
else
    [qemb(2).ks,semres.quadresponse]=sempanelfitq(qemb(2),qemres,semres,dispfunc);
end
delete(hw);
qemb(2)=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set([handles.semdata.errdef handles.pushbutton9],'Enable','On');
set(handles.statustext,'String','finished quadrupoles fit');


% --- Executes on button press in pushbutton9: Errors->Fit dipoles
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
set(handles.statustext,'String','fitting dipoles');
if get(handles.AnalyticFitCheck,'Value') == 1
    qemb(2).diptilt=qemb(2).diptilt+semfitdipoleAnalytic(qemb(2).at,qemres,semres);
else
    qemb(2).diptilt=qemb(2).diptilt+semfitdipole(qemb(2).at,qemres,semres);
end
qemb(2)=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set(handles.statustext,'String','finished dipoles fit');


% --- Executes on button press in pushbutton10: Errors->Zero
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
qemb(2).ks=qemb(1).ks;
qemb(2).diptilt=qemb(1).diptilt;
qemb(2).dzs=qemb(1).dzs;
qemb(2)=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set(handles.semdata.errdef,'Enable','On');
set(hObject,'Enable','Off');

% --- Executes on button press in pushbutton11: Errors->Save
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
set(handles.statustext,'String','saving errors');
qfile=fullfile(qemres.datadir, 'skewerrors.mat');
gfile=fullfile(qemres.datadir, 'gainfit.mat');
ok=(exist(qfile,'file') ~= 2);
if ~ok
    [fname,fpath]=uiputfile('*.mat','Save errors:',qfile);
    if ischar(fname), qfile=fullfile(fpath,fname); ok=true; end
end
if ok
    s.ks=qemb(2).ks;
    s.diprot=qemb(2).diptilt;
    s.dzs=qemb(2).dzs; %#ok<STRNU>
    save(qfile,'-struct','s');
    [w.khrot,w.khgain,w.kvrot,w.kvgain,w.bhrot,w.bhgain,w.bvrot,w.bvgain]=...
        qemguess(qemb(2).at,qemres,...
        qemres.resph,semres.respv,semres.resph,qemres.respv); 
    save(gfile,'-struct','w');
end
set(handles.statustext,'String','errors saved');


% --- Executes on button press in pushbutton16: Errors->load
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
qfile=fullfile(qemres.datadir, 'skewerrors.mat');
[fname,fpath]=uigetfile('*.mat','Load errors:',qfile);
if ischar(fname)
    s=load(fullfile(fpath,fname));
    qemb(2).ks=s.ks;
    qemb(2).diptilt=s.diprot;
    qemb(2).dzs=zeros(length(qemres.sextidx),1);
    if isfield(s,'dzs'), qemb(2).dzs=s.dzs; end
    qemb(2)=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
    set(handles.semdata.errdef,'Enable','On');
end


% --- Executes on button press in pushbutton12: Corrections->Correct
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
set(handles.statustext,'String','computing correction');
method=get(handles.popupmenu2,'Value');
if method == 1
elseif method == 2
    dispweight=str2double(get(handles.edit4,'String'));
    hw=waitbar(0,'Fitting skew correctors...');
    dispfunc=@(i,itot) waitbar(i/itot,hw);
    qemb(2).skewcor(:,2)=semsolverdt(qemb(2),qemres,semres,...
        qemb(2).skewcor(:,end),dispweight,dispfunc);
    delete(hw);
    set(handles.semdata.cordef,'Enable','On');
else
    mach=qemat(qemres,qemb(2),true);
    options=optimset(optimset('fminsearch'),'Display','iter',...
        'OutputFcn',@(x,y,z) semcorout(x,y,z,handles),'TolFun',1.e-7);
    [fun,options]=semcorargs(method,mach,qemres.bpmidx,qemres.skewidx,options);
    set(handles.pushbutton19,'Visible','on'); drawnow;
    inicor=qemb(2).skewcor(:,end)./qemres.skewl;
    endcor=fminsearch(fun,inicor,options);
    qemb(2).skewcor(:,2)=endcor.*qemres.skewl;
    set(handles.pushbutton19,'Visible','off');
    set(handles.semdata.cordef,'Enable','On');
end
qemb(2)=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set(handles.statustext,'String','correction computed');


% --- Executes on button press in pushbutton13: Corrections->Initial
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
qemb(2).skewcor(:,2)=load_correction(fullfile(qemres.datadir,'skewcor.dat'),...
    qemres.skcode,qemres.opticsdir);
set(handles.semdata.cordef,'Enable','On');
set(hObject,'Enable','Off');
qemb(2)=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);


% --- Executes on button press in pushbutton14: Corrections->Zero
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
qemb(2).skewcor(:,2)=qemb(1).skewcor;
set(handles.semdata.cordef,'Enable','On');
set(hObject,'Enable','Off');
qemb(2)=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);


% --- Executes on button press in pushbutton15: Corrections->Save
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres
set(handles.statustext,'String','saving correction');
corfile=fullfile(qemres.datadir, 'skewnew.dat');
ok=(exist(corfile,'file') ~= 2);
if ~ok
    [fname,fpath]=uiputfile('*.dat','Save correction:',corfile);
    if ischar(fname), corfile=fullfile(fpath,fname); ok=true; end
end
if ok
    save_correction(corfile,qemres.skcode,qemb(2).skewcor(:,end),...
        qemres.opticsdir);
    set(handles.pushbutton18,'Enable','On');
end
set(handles.statustext,'String','correction saved');

% --- Executes on button press in pushbutton17: Corrections->Load
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
corfile=fullfile(qemres.datadir, 'skewcor.dat');
[fname,fpath]=uigetfile('*.dat','Load correction:',corfile);
if ischar(fname)
    qemb(2).skewcor(:,2)=load_correction(fullfile(fpath,fname),...
        qemres.skcode,qemres.opticsdir);
    set(handles.semdata.cordef,'Enable','On');
    qemb(2)=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
end


% --- Executes on button press in pushbutton18: Correction->Apply
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemres
bindir=[getenv('MACH_HOME') '/bin/' getenv('MACHARCH') '/'];
corfile=fullfile(qemres.datadir, 'skewnew.dat');
sr.setcorbase(corfile,'sr/qp-s/all','sr/reson-skew/m1_n1_p50','base_skew2');	% Set resonance device
[s,w]=unix([bindir 'dvset <' corfile]); %#ok<ASGLU>         % Set values
disp(w);
set(handles.statustext,'String','correction applied');


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
method=get(hObject,'Value');
if method == 2
    set([handles.text5 handles.edit4],'Visible','on');
else
    set([handles.text5 handles.edit4],'Visible','off');
end


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton19: Stop correction
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global interrupt
interrupt=true;



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton20: retune
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
qemb(2)=qemretune(qemb(2),qemres,qemres.tunes);
qemb(2)=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);


function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3
global qemb qemres
sempanelplot(5,get(hObject,'Value'),qemb(1),qemb(2),qemres);

% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in AnalyticFitCheck.
function AnalyticFitCheck_Callback(hObject, eventdata, handles)
% hObject    handle to AnalyticFitCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AnalyticFitCheck
set(handles.checkbox2,'Value',0.0)
set(handles.AnalyticFitCheck,'Value',1.0)


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2
set(handles.checkbox2,'Value',1.0)
set(handles.AnalyticFitCheck,'Value',0.0)
