function qemb=sempaneldisp(qemres,semres,qemb,qemb0,handles)

if nargin < 5, handles=struct(); end

qemb.at=qemat(qemres,qemb,true);
qemb=qemoptics(qemres,qemb,qemb0);
sbpm=cat(1,qemb.lindata.SPos);

if isfield(handles,'axes1')
    sqcheck(qemb.ks,qemb.kn,qemb.diptilt,handles.axes1);
else
    sqcheck(qemb.ks,qemb.kn,qemb.diptilt);
end

mess={};
if (isfield(handles,'compare') && handles.compare) || ...
        (isfield(handles,'pushbutton13') && strcmpi(get(handles.pushbutton13,'Enable'),'Off'))
    if all(isfield(semres,{'resph','respv','frespz'}))
        diffv2h=qemb.atrespv2h-semres.resph;    % Response V -> H in m/rad
        diffh2v=qemb.atresph2v-semres.respv;    % Response H -> V in m/rad
        diff2=[diffv2h diffh2v];
        residual=std2(diff2(:));
        vdispresidual=qemb.pm.alpha*std2(semres.bvscale*semres.frespz-qemb.frespz);
        fprintf('residual V2H = %g m/rad\n', std2(diffv2h(:)));
        fprintf('residual H2V = %g m/rad\n', std2(diffh2v(:)));
        mess={['orbit residual = '  num2str(residual)];...
            ['V disp. residual = ' num2str(vdispresidual)]};
        qemplotresp(4,diffv2h,diffh2v,'deviation');
    end
end

mess0=semdisp(qemb,qemres,semres);      % figures 1 and 2
mess=[mess;...
    sprintf('tunes H/V: %.4f/%.4f',qemb.pm.fractunes);...
    mess0];

if isfield(handles,'text3')             % displays values
    set(handles.text3,'String',mess);
else
    fprintf('%s\n',mess{:});
end

if isfield(handles,'axes4')             % displays corr. strengths
    bar(handles.axes4,qemb.skewcor);
    set(handles.axes4,'XLim',[0 size(qemb.skewcor,1)+1]);
    title(handles.axes4,'Corrector strengths');
    grid(handles.axes4,'on');
end

if isfield(handles,'axes2')             % displays vertical dispersion
    if isfield(semres,'frespz')
        plot(handles.axes2,sbpm,-qemb.pm.alpha*[semres.bvscale*semres.frespz qemb.frespz]);
    else
        plot(handles.axes2,sbpm,-qemb.pm.alpha*qemb.frespz);
    end
    title(handles.axes2,'Vertical dispersion');
    ax=axis(handles.axes2);
    ym=max(abs(ax([3 4])));
    axis(handles.axes2,[0 844.39 -ym ym]);
    ylabel(handles.axes2,'\eta_z [m]');
    grid(handles.axes2,'on');
end

if isfield(handles,'axes3')             % displays horizontal dispersion
    if isfield(qemres,'frespx')
        plot(handles.axes3,sbpm,-qemb.pm.alpha*[qemres.bhscale*qemres.frespx-qemb0.frespx qemb.frespx-qemb0.frespx]);
    else
        plot(handles.axes3,sbpm,-qemb.pm.alpha*(qemb.frespx-qemb0.frespx));
    end
    title(handles.axes3,'Horizontal dispersion');
    ax=axis(handles.axes3);
    ym=max(abs(ax([3 4])));
    axis(handles.axes3,[0 844.39 -ym ym]);
    ylabel(handles.axes3,'\eta_x [m]');
    grid(handles.axes3,'on');
end

if isfield(handles,'selplot')           % additional plot
    sempanelplot(5,handles.selplot,qemb0,qemb,qemres);
elseif isfield(handles,'popupmenu3')
    sempanelplot(5,get(handles.popupmenu3,'Value'),qemb0,qemb,qemres);
end
