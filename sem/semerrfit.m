function deltaset=semerrfit(nhst,nvst,resp,dresp,mode,okfit,okbpm)
%QEMPANELFIT Vary lattice parameters to fit the measured response matrix
%DELTAV=QEMPANELFIT(NHST,NVST,RESP,DRESP,MODE,OKVAR,OKBPM)
%
%NHST:      Number of horizontal kicks
%NHST:      Number of vertical kicks
%RESP:      Deviation of measured reponse matrices from the model
%DRESP:     Derivatives of response matrices vs. variable elements
%MODE:      Structure allowing control of SVD inversion. (default:)
%OKVAR:     Select a subset of variable parameters (default: all)
%OKBPM:     Select a subset of valid BPMS (default:all)
%
%DELTAV:    Variation of selected parameters

[no,nq]=size(dresp);
nbpm=no/(nhst+nvst+1);
orbitrange=nbpm*(nhst+nvst);
if nargin<7, okbpm=true(nbpm,1); end
if nargin<6, okfit=true(nq,1); end
if nargin<5 || isempty(mode), mode=struct; end
if ~isfield(mode,'nsets'), mode.nsets=4; end
if ~isfield(mode,'neigs'), mode.neigs=100; end
if ~isfield(mode,'dispweight'), mode.dispweight=0; end
if ~isfield(mode,'vnorm')
    v=sr.fold(std(dresp(1:orbitrange,:),1,1));
    mode.vnorm=sqrt(32./sum(v.*v,2));   % normalize each column of dresp
%   mode.vnorm=ones(nq/32,1);           % no normalization
end
inisel=false(size(resp));
if mode.dispweight>0, inisel(orbitrange+(1:nbpm))=true; end  % Keep dispersion
bok=repmat(okbpm,nhst+nvst+1,1) & isfinite(resp);
v=sr.unfold(repmat(mode.vnorm,1,32));
w=[ones(orbitrange,1);mode.dispweight*ones(224,1)];
hsets=reshape(1:nhst,mode.nsets,[]);
vsets=reshape(1:nvst,[],mode.nsets)';
% hsets=reshape(1:nhst,[],mode.nsets)';
% vsets=reshape(1:nvst,mode.nsets,[]);
sk=semsolvex(dresp(:,okfit).*(w*v(okfit)'),resp.*w,bok,hsets,vsets,mode.neigs);
deltaset=zeros(nq,1);
deltaset(okfit)=mean(sk,2).*v(okfit);
devdev=std(sk,1,2).*v(okfit);
fprintf('Deviation between sets: %g\n',sqrt(mean(devdev.*devdev)));

    function sel=lselected(sh,sv)
        sel=inisel;
        sel(semselect(sh,sv))=true;
    end

    function sk=semsolvex(dresp,resp,ok,hsets,vsets,neig)
        [nl,nc]=size(dresp);
        ngs=min([nl nc neig]);
        [nsets nh]=size(hsets); %#ok<ASGLU>
        [nsets nv]=size(vsets);
        fprintf('Solving %i sets(%i H steerers, %i V steerers) with %i Eigen vectors\n',nsets,nh,nv,ngs);
        sk=zeros(nc,nsets);
        for i=1:nsets
            lok=lselected(hsets(i,:),vsets(i,:)) & ok;
            sk(:,i)=qemsvd(dresp(lok,:),resp(lok),ngs);
        end
    end
end
