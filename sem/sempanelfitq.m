function [newks,quadresponse]=sempanelfitq(qemb,qemres,semres,varargin)
%SEMPANELFITQ Vary quadrupole strengths to fit the measured response matrix
%NEWK=SEMPANELFITS(MACH,QEMRES,SEMRES,OKVAR,OKBPM)
%
%QEMB:
%QEMRES:    global parameters (bpmidx,steerhidx,steervidx,hlist,vlist,
%           khrot,khgain,kvrot,kvgain,brot,bhgain,bvgain,resph,respv,frespx)
%SEMRES:    global parameters (hlist,vlist,bvscale,quadresponse,resph,respv,frespz)
%OKVAR:     Select a subset of variable parameters (default: all)
%OKBPM:     Select a subset of valid BPMS (default:all)
%
%NEWKS:       New skew quadrupole strengths

% narg=length(varargin);
% if narg<3
%     disparg={};
% else
%     disparg={@(i,itot) nselect(4,varargin{3},i,itot)};
%     narg=narg-1;
% end

if isfield(semres,'quadresponse')
    quadresponse=semres.quadresponse;
else
    quadresponse=semquadresp(qemb,qemres,semres,varargin{:});
end
orbit0=findsyncorbit(qemb.at,qemres.ct,qemres.bpmidx);

[rh,rh2v,rv2h,rv,frh,frv]=qemdecode(qemb.at,qemres.ct,qemres,...
    qemres.resph,semres.respv,semres.resph,qemres.respv,...
    qemres.frespx,semres.frespz,...
    qemres.khrot,qemres.khgain,qemres.kvrot,qemres.kvgain,...
    qemres.brot,qemres.bhgain,qemres.bvgain); %#ok<ASGLU>
[rh0,rh2v0,rv2h0,rv0,frh0,frv0]=qemcode(qemb.at,qemres.ct,...
    qemres.steerhidx(qemres.hlist),qemres.steervidx(qemres.vlist),...
    qemres.bpmidx,[],[],[],[],[],[],[],orbit0); %#ok<ASGLU>

resp0=[rh2v0(:);rv2h0(:);frv0];
resp=[rh2v(:);rv2h(:);semres.bvscale*frv];
mode=struct('vnorm',sqrt([0.77;2;0.9;1.15;1.15;0.9;2;1]));
% newks=qemb.ks+semerrfit(length(semres.hlist),length(semres.vlist),resp-resp0,...
%     quadresponse,mode,varargin{:});
newks=qemb.ks+semerrfit(length(semres.hlist),length(semres.vlist),resp-resp0,...
    quadresponse,mode);
end
