function [fun,options]=semcorargs(value,mach,bpmidx,skewidx,options)

switch value
    case 3	% rms of vertical size in all cells
        bpmlist=reshape(1:224,7,32);
        allidx=bpmidx(bpmlist(3,:));
        [radmach,radindex,cavindex]=atradon(mach); %#ok<NASGU>
        fun=@(x) fitrmsiax(x,radmach,skewidx,allidx,radindex);
        options=optimset(options,'TolFun', 1.e-8);
    case 4	% average mode emittance at BPM locations
        [radmach,radindex,cavindex]=atradon(mach); %#ok<NASGU>
        fun=@(x) fitmode(x,mach,radmach,skewidx,bpmidx,radindex);
        options=optimset(options,'TolFun', 1.e-13);
    case 5	% max of vertical size in all cells
        bpmlist=reshape(1:224,7,32);
        allidx=bpmidx(bpmlist(3,:));
        [radmach,radindex,cavindex]=atradon(mach); %#ok<NASGU>
        fun=@(x) fitmaxiax(x,radmach,skewidx,allidx,radindex);
        options=optimset(options,'TolFun', 1.e-8);
    case 6	% rms gamma
        options=optimset(options,'TolFun', 1.e-10);
        fun=@(x) fitstdgamma(x,mach,skewidx,bpmidx);
    case 7	% rms vertical dispersion
        fun=@(x) fitdisp(x,mach,skewidx,bpmidx);
        options=optimset(options,'TolFun', 1.e-7);
    otherwise
        fun=[];
        options=[];
end

    function f=fitstdgamma(skewcor,mach,skewidx,bpmidx)
        mch=setcellstruct(mach,'PolynomA',skewidx,skewcor,2);
        lindata=linopt(mch,0,bpmidx);
        gamma=cat(1,lindata.gamma)-1;
        %f=max(abs(gamma));
        f=sqrt(sum(gamma.*gamma)/length(bpmidx));
    end

    function f=fitdisp(skewcor,mach,skewidx,bpmidx)
        mch=setcellstruct(mach,'PolynomA',skewidx,skewcor,2);
        [lindata,tunes,xsi]=atlinopt(mch,0,bpmidx); %#ok<ASGLU,NASGU>
        eta=cat(2,lindata.Dispersion)';
        f=std(eta(:,3),1);
    end

    function f=fitrmsiax(skewcor,mach,skewidx,iaxidx,radidx)
        mch=setcellstruct(mach,'PolynomA',skewidx,skewcor,2);
        [envelope,espread,blength]=ohmienvelope(mch,radidx,iaxidx); %#ok<ASGLU,NASGU>
        bm66=reshape(cat(2,envelope.R),6,6,[]);
        vsize=squeeze(bm66(3,3,:));
        f=sqrt(sum(vsize)/length(vsize));
    end

    function f=fitmaxiax(skewcor,mach,skewidx,iaxidx,radidx)
        mch=setcellstruct(mach,'PolynomA',skewidx,skewcor,2);
        [envelope,espread,blength]=ohmienvelope(mch,radidx,iaxidx); %#ok<ASGLU,NASGU>
        beam66=reshape(cat(2,envelope.R),6,6,[]);
        vsize=squeeze(beam66(3,3,:));
        f=max(sqrt(vsize));
    end

    function f=fitmode(skewcor,mach,radmach,skewidx,bpmidx,radidx)
        mch=setcellstruct(mach,'PolynomA',skewidx,skewcor,2);
        lindata=linopt(mch,0,bpmidx);
        mch=setcellstruct(radmach,'PolynomA',skewidx,skewcor,2);
        [envelope,espread,blength]=ohmienvelope(mch,radidx,bpmidx); %#ok<ASGLU,NASGU>
        modemit=NaN(length(lindata),2);
        for i=1:length(lindata)
            [beamA,beamB]=beam44(lindata(i));  % betatron 4x4 coupled matrices
            bm66=envelope(i).R;
            siginv=inv(bm66);
            bm44=inv(siginv(1:4,1:4));
            modemit(i,:)=([beamA(:) beamB(:)]\bm44(:))';
        end
        emittance=mean(modemit);
        f=emittance(2);
    end
end
