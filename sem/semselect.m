function lines=semselect(hfit,vfit)
%LINES=SEMSELECT(HLIST,VLIST)		Select lines in response matrix

[vx,vy]=meshgrid(224*(hfit-1),(1:224)');
[hx,hy]=meshgrid(224*(vfit-1),(1:224)'+16*224);
res=[vx+vy hx+hy];
lines=res(:)';
