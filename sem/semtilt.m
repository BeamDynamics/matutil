function [kl, tilt]=semtilt(klnorm,klskew)
%SEMTILT    Computes quadrupole strength and tilt
%
%[KL,TILT]=SEMTILT(KNORM,KSKEW)
%
% KNORM : normal quad strength
% KSKEW : skew quad strength
%
% KNORM= KL*COS(2*TILT)
% KSKEW=-KL*SiN(2*TILT)
%
oknorm=(klnorm~=0);
kl=klskew;
tilt=-pi/4*ones(size(klskew));
ratio=klskew(oknorm)./klnorm(oknorm);
kl(oknorm)=klnorm(oknorm).*sqrt(1+ratio.*ratio);
tilt(oknorm)=-0.5*atan(ratio);
