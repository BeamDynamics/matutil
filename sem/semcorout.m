function stop=semcorout(x,iter,state,handles)

global interrupt
persistent tstart
stop=false;
switch state
   case 'init'
	  interrupt=false;
	  tstart=tic;
	  set(handles.text3,'String',sprintf('Running (%d-%g)',...
		 iter.iteration,iter.fval));
   case 'iter'
	  drawnow;
	  stop=interrupt;
	  if toc(tstart) > 5
		 tstart=tic;
		 set(handles.text3,'String',sprintf('Running (%d-%g)',...
			iter.iteration,iter.fval));
	  end
end
