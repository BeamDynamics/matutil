function semexport(filename)
%SEMEXPORT Summary of this function goes here
%   Detailed explanation goes here
global qemb semres

if nargin < 1, filename='errors'; end
[kl,tilt]=semtilt(qemb(2).kn,qemb(2).ks);
[kl0,tilt0]=semtilt(qemb(1).kn,qemb(1).ks);
qerr=[kl0 kl kl-kl0 (kl-kl0)./kl0 tilt];
derr=qemb(2).diptilt;
qcor=qemb(2).cor;
scor=qemb(2).skewcor;
fresp=[semres.frespx semres.frespz];
save([filename '1.txt'],'-ascii','-double','-tabs','qerr');
save([filename '2.txt'],'-ascii','-double','-tabs','derr');
save([filename '3.txt'],'-ascii','-double','-tabs','qcor');
save([filename '4.txt'],'-ascii','-double','-tabs','scor');
save([filename '5.txt'],'-ascii','-double','-tabs','fresp');
end

