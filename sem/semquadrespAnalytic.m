function quadresponse = semquadrespAnalytic(qemb,qemres,semres,dispfunc)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if nargin>=4
    vargs1={};
    vargs2={@(i,itot) nselect(2,dispfunc,i,itot)};
else
    vargs1={};
    vargs2={@(i,itot) nselect(2,@(j,jtot) fprintf('\b\b\b\b\b\b\b\b\b%4d/%4d',[j,jtot]),i,itot)};
end

quadresp=semderivAnalytic(qemb.at(:),qemres.dpp,qemres.qpidx,qemres.bpmidx,...
    qemres.steerhidx(semres.hlist),qemres.steervidx(semres.vlist),vargs1{:});
%%
%profile on

%[~,quadetaresp0]=qemdispderiv(qemb.at(:),qemres.ct,@setskew,1.e-4,...
%    qemres.qpidx,qemres.bpmidx,vargs2{:});

[S]=dDxyDskew(qemb.at(:),qemres.bpmidx,qemres.qpidx);
Lq=cellfun(@(a)a.Length(1),qemb.at(qemres.qpidx),'un',1)';
S=-S./mcf(qemb.at(:)).*repmat(Lq,length(qemres.bpmidx),1);
df=vargs2{:};
df(10,10);

quadetaresp=S;

%profile viewer
%%
% figure; plot(S(:)); hold on; plot(quadetaresp0(:))
% figure; plot(S(:)./quadetaresp(:))

lq=size(quadresp,1)+size(quadetaresp,1);
quadresponse=[quadresp;quadetaresp].*qemres.ql(:,ones(1,lq))';

    function elt=setskew(elt,dval)
        vini=elt.PolynomA(2);
        elt.PolynomA(2)=vini+dval;
    end
end
