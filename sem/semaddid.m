function newmach = semaddid(mach,cell,lmid)
%SEMADDID Add skew components at the location of ids
%NEWMACH=SEMADDID(MACH,CELL,LMID)
%	creates a new AT structure with thin skew quads,
%	labelled IDH1, IDH2, IDH3... at a distance lmid
%	from the center of the straight section
%
%	CELL: number of the ID straight section (defaut: all sections)
%   LMID: Distance of IDs from the center of the straight section
%         (default [0 1.8]
%
%See also: SEMADDIDMARKER

if nargin < 3, lmid=[0 1.8]; end
if nargin < 2, cell=0; end

if cell > 0
   period=mod(cell-4,32)+1;
   mach2=semaddidmarker(mach,skewq('ID%c%d'),lmid);
   [o1,o2]=idbpm(mach,period);
   [n1,n2]=idbpm(mach2,period);
   if n2 > n1
	  newmach=[mach(1:o1) mach2(n1+1:n2-1) mach(o2:end)]';
   else
	  newmach=[mach2(1:n2-1) mach(o2:o1) mach2(n1+1:end)]';
   end
else
   newmach=semaddidmarker(mach,skewq('ID%c%d'),lmid);
end
end

function sk=skewq(famnam)
sk.FamName=famnam;
sk.Length=0;
sk.PassMethod='ThinMPolePass';
sk.PolynomA=[0 0];
sk.PolynomB=[0 0];
sk.MaxOrder=1;
end

function [n1,n2]=idbpm(mach,period)
bpmlist=findcells(mach(:),'BetaCode','PU');
bpmlist=reshape(bpmlist([224 1:223]),7,32);
n1=bpmlist(1,period);
n2=bpmlist(2,period);
end
