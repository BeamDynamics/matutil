function [k,tilts,stdf,meanf]=sqcheck(ks,kn,diptilt,ax)
%SQCHECK		Analyzes quadrupole strength errors
%
%DKK=SQCHECK(KL,KL0)
%
nm=size(ks,2);
nq=size(ks,1)/32;
if nq == 10
    labels={'QD1','QF2','QD3','QD4','QF5','QF5','QD4','QD6','QF7','QD8','Dip'};
else
    labels={'QF2','QD3','QD4','QF5','QF5','QD4','QD6','QF7','Dip'};
end

k=zeros(size(ks));
tilts=zeros(size(ks));

stdf=NaN*ones(nq,nm);
meanf=NaN*ones(nq,nm);
for i=1:nm
    [k(:,i),tilts(:,i)]=semtilt(kn,ks(:,i));
    tiltf=sr.fold(tilts(:,i));
    stdf(:,i)=std(tiltf,1,2);
    meanf(:,i)=mean(tiltf,2);
end
stda=std(tilts,1,1);
stdf(nq+1,:)=std(diptilt);
meanf(nq+1,:)=mean(diptilt);
fprintf('%s:\t% .3e\n','all',stda(1));
for i=1:nq+1
    fprintf('%s:\t% .3e\t% .3e\n',labels{i},stdf(i,1),meanf(i,1));
end

if nargin >= 4             % displays quad rotations
    bar(ax,stdf(1:nq));
    title(ax,'Magnet rotation errors');
    ylabel(ax,'\Theta');
    grid(ax,'on');
    set(ax,'Xlim',[0 nq+1],'XTickLabel',labels);
end
