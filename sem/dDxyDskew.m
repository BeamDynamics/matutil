function [S,N]=dDxyDskew(r,indbpm,indquad,varargin)
% function [S,N]=dDxyDskew(r,indbpm,indquad,varargin)
%
% Computes the derivative of the dispersion with respect to dipole angles.
% units are [(m/[%])/rad]
% 
% Inputs:
% r         : AT lattice    
% indbpm    : BPM indexes
% indquad   : quadrupole manget indexes (all the dipoles in r)
% magmodel  : '1slice', 'thick' (default)
%
% Outputs:
% S: vertical dispersion derivative with respect to skew quadrupoles.
% N: horizontal dispersion derivative with respect to skew quadrupoles.
% 
% Example:
% [S,N]=dDxyDskew(r,....
%                  find(atgetcells(r,'Class','Monitor'))',....
%                  find(atgetcells(r,'Class','Bend')),...
%                  'magmodel','thick');
% 
%see also: atlinopt 

% parse inputs
expectedmodels={'thick','1slice'};

p=inputParser;

addRequired(p,'r');
addRequired(p,'indbpm');
addRequired(p,'indquad');
addParameter(p,'magmodel',expectedmodels{1},...
    @(x) any(validatestring(x,expectedmodels)));
  
parse(p,r,indbpm,indquad,varargin{:});
   
magmodel = p.Results.magmodel; 

switch magmodel
    case '1slice'
        % optics at center
        machx2=cellfun(@(a)atdivelem(a,[0.5,0.5])',r,'un',0);
        machx2=[machx2{:}, atmarker('end')]';
        [lx2,~,~]=atlinopt(machx2,0,1:length(machx2)+1);
        l=lx2(2:2:length(machx2)+1);
    case 'thick'
        % optics at entrance
        [l,~,~]=atlinopt(r,0,1:length(r)+1);
end

nbpm=length(indbpm);

% dipoles angle and length
K_quad = cellfun(@(a)a.PolynomB(2),r(indquad),'un',1)';
len_quad = cellfun(@(a)a.Length,r(indquad),'un',1)';

% beta x at bpm and quadrupole
by_bpm = arrayfun(@(a)a.beta(2),l(indbpm));
by_quad = arrayfun(@(a)a.beta(2),l(indquad));
bx_bpm = arrayfun(@(a)a.beta(1),l(indbpm));
bx_quad = arrayfun(@(a)a.beta(1),l(indquad));

% alpha x at quadrupole
ay_quad = arrayfun(@(a)a.alpha(2),l(indquad));
ax_quad = arrayfun(@(a)a.alpha(1),l(indquad));

% dispersion at quadrupoles
dx_quad  = arrayfun(@(a)a.Dispersion(1),l(indquad));
dxp_quad = arrayfun(@(a)a.Dispersion(2),l(indquad));
dy_quad  = arrayfun(@(a)a.Dispersion(3),l(indquad));
dyp_quad = arrayfun(@(a)a.Dispersion(4),l(indquad));

% phase advance x at bpm and bends
phx_bpm = arrayfun(@(a)a.mu(1),l(indbpm));
phx_quad = arrayfun(@(a)a.mu(1),l(indquad));
phy_bpm = arrayfun(@(a)a.mu(2),l(indbpm));
phy_quad = arrayfun(@(a)a.mu(2),l(indquad));

% tunes including integer part
Q=l(end).mu/2/pi;


% define all quantities as matrices, 
%  quad related are identical rows,
%  bpm related are identical columns
DXquad =repmat(     dx_quad,nbpm,1);
DXPquad=repmat(    dxp_quad,nbpm,1);
DYquad =repmat(     dy_quad,nbpm,1);
DYPquad=repmat(    dyp_quad,nbpm,1);
[PYquad,PYbpm] = meshgrid(     phy_quad,phy_bpm);
[BYquad,BYbpm] = meshgrid(sqrt(by_quad),sqrt(by_bpm)); % square root here for speed
[PXquad,PXbpm] = meshgrid(     phx_quad,phx_bpm);
[BXquad,BXbpm] = meshgrid(sqrt(bx_quad),sqrt(bx_bpm)); % square root here for speed
AYquad = repmat(      ay_quad,nbpm,1);
AXquad = repmat(      ax_quad,nbpm,1);

sqrtKq  = sqrt(abs(K_quad));
sqrtKqL = sqrtKq.*len_quad;

 Kq   = repmat(           (K_quad), nbpm,1);
aKq   = repmat(        abs(K_quad), nbpm,1);
signKq = Kq./aKq;
Kq32 = repmat( abs(K_quad).^(3/2), nbpm,1);
sqKq = repmat(         sqrtKq, nbpm,1);
sKq  = repmat(   sin(sqrtKqL), nbpm,1);% sin/cos here for speed
cKq  = repmat(   cos(sqrtKqL), nbpm,1);
shKq = repmat(  sinh(sqrtKqL), nbpm,1);% sinh/cosh here for speed
chKq = repmat(  cosh(sqrtKqL), nbpm,1);
Lquad = repmat(      len_quad, nbpm,1);

% define phase distance
dphY = (PYbpm-PYquad);
dphX = (PXbpm-PXquad);

tau_ymj = abs(dphY) - pi*Q(2);
tau_xmj = abs(dphX) - pi*Q(1);

    function y=signtilde(x)
        y=sign(x)-double(x==0);
    end

% phase term
switch magmodel
    case '1slice'
        
        % hor disp/ d skew
        JCmjx = BXquad.*DYquad.*cos(tau_xmj);  % J_{C,mj}^{(Dy)}
       
        % ver disp/ d skew
        JCmjy = BYquad.*DXquad.*cos(tau_ymj);  % J_{C,mj}^{(Dx)}
        
    case 'thick'
        
        % ver disp/ d skew
        TSym = DXquad ./ (2.*aKq.*Lquad.*BYquad) .* ...
                  (sKq.*shKq + signKq.*cKq.*chKq - signKq) + ...
               DXPquad ./ (2.*Kq32.*Lquad.*BYquad) .* ...
                  (sKq.*chKq - cKq.*shKq)  ;
        
        TCym = DXquad .*BYquad ./ (2.*sqKq.*Lquad) .* ...
               (sKq.*chKq + cKq.*shKq)  + ...
            -AYquad .*DXPquad ./ (2.*Kq32.*Lquad.*BYquad) .* ...
               (sKq.*chKq - cKq.*shKq)  + ...
            -AYquad .*DXquad ./ (2.*aKq.*Lquad.*BYquad) .* ...
               (sKq.*shKq + signKq.* cKq.*chKq - signKq)  + ...
            +BYquad .*DXPquad ./ (2.*aKq.*Lquad) .* ...
               (sKq.*shKq - signKq.* cKq.*chKq + signKq) ;
        
        JCmjy = TCym.*cos(tau_ymj) + TSym.*signtilde(dphY).*sin(tau_ymj);
        
        % hor disp/ d skew
        
        % ver disp/ d skew
        TSxm = DYquad ./ (2.*aKq.*Lquad.*BXquad) .* ...
                  (sKq.*shKq - signKq.*cKq.*chKq + signKq) + ...
               DYPquad ./ (2.*Kq32.*Lquad.*BXquad) .* ...
                  (sKq.*chKq - cKq.*shKq)  ;
        
        TCxm = DYquad .*BXquad ./ (2.*sqKq.*Lquad) .* ...
               (sKq.*chKq + cKq.*shKq)  + ...
            -AXquad .*DYPquad ./ (2.*Kq32.*Lquad.*BXquad) .* ...
               (sKq.*chKq - cKq.*shKq)  + ...
            -AXquad .*DYquad ./ (2.*aKq.*Lquad.*BXquad) .* ...
               (sKq.*shKq - signKq.* cKq.*chKq + signKq)  + ...
            +BXquad .*DYPquad ./ (2.*aKq.*Lquad) .* ...
               (sKq.*shKq + signKq.* cKq.*chKq - signKq) ;
        
        JCmjx = TCxm.*cos(tau_xmj) + TSxm.*signtilde(dphX).*sin(tau_xmj);
        
end

% dispersion derivative respect to bending angles
S = BYbpm./(2*sin(pi*Q(2))) .* ( JCmjy );
N = BXbpm./(2*sin(pi*Q(1))) .* ( JCmjx );

end
