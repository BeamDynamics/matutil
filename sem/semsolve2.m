function sk=semsolve2(drv,resp,neig)

if nargin < 3, neig=60; end
nfit=8;
sk=zeros(size(drv,2),nfit);
for i=1:nfit
    lines=semselect([2*i-1 2*i],[]);
    ok=isfinite(resp(lines));
    sk(:,i)=qemsvd(drv(lines(ok),:),resp(lines(ok)),neig);
    r=resp-drv*sk(:,i);
    disp(sprintf('%g %g',std2(resp(lines),1),std2(r(lines),1)));
end
