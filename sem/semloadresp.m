function [resph,respv]=semloadresp(qemres,semres,wrongbpms,datadir,opticsdir)
%SEMLOADRESP	load response matrices
%
%[RESPH,RESPV,RESPIAX]=SEMLOADRESP(QEMRES,SEMRES,BADBPMS,DATADIR,OPTICSDIR)
%
% QEMRES:
% SEMRES:	global parameters
% BADBPMS:	BPM to remove from data
% DATADIR:	Directory containing the response matrices (default: qemres.datadir)
% OPTICSDIR:Directory containing the model optics (default: datadir/optics)
%
%RESPH,RESPV : normalized response matrices in m

if nargin < 4
   datadir=qemres.datadir;
   opticsdir=qemres.opticsdir;
elseif nargin < 5
   opticsdir=fullfile(datadir,'optics');
end

resph=sr.load_normresp(datadir,opticsdir,'v2h',semres.vlist);   % Load response
respv=sr.load_normresp(datadir,opticsdir,'h2v',semres.hlist);
% rh=sr.load_normresp(datadir,opticsdir,'v2h',semres.vlist);   % Load response
% rv=sr.load_normresp(datadir,opticsdir,'h2v',semres.hlist);
% khgain=qemres.khgain(semres.hlist);                     % Correct for
% kvgain=qemres.kvgain(semres.vlist);                     % gain end tilt
% cbrot=cos(qemres.brot);
% sbrot=sin(qemres.brot);
% resph=((qemres.bhgain.*cbrot)*kvgain).*rh-((qemres.bvgain.*sbrot)*khgain).*rv;
% respv=((qemres.bhgain.*sbrot)*kvgain).*rh+((qemres.bvgain.*cbrot)*khgain).*rv;
respv(wrongbpms,:)=NaN;
resph(wrongbpms,:)=NaN;
