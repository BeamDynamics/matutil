function mess=semdisp(qemb,qemres,semres)

nbpm=length(qemres.bpmidx);
alllist=1:nbpm;                 % all BPMS
blist=circshift(reshape(alllist,[],32),[0 3]);
iaxcells=[5;10;11;14;18;21;25;26;29;31;3];
inairlist=blist(3,iaxcells);	% IAX
d9=nbpm+1;					% D9 pinhole
d11lens=nbpm+2;				% D911 lens
id25=nbpm+3;				% ID25 pinhole
iaxname=[arrayfun(@(i) sprintf('C%i',i),iaxcells,'UniformOutput',false);...
    {'D9';'ID11-LENS';'ID25'}];

emittances=qemb.emittances;
sbpm=cat(1,qemb.lindata.SPos);
sskew=findspos(qemb.at(:),qemres.skewidx);
spinhole=[qemb.d9data.SPos;qemb.d11lensdata.SPos;qemb.id25data.SPos];

mess={...
    sprintf('em. H [nm]: %7.3f %7.3f %7.3f',1.e9*mean(emittances(alllist,1:2:end)));...
    sprintf('em. V [pm]: %7.3f %7.3f %7.3f',1.e12*mean(emittances(alllist,2:2:end)));...
    sprintf('V. dispersion [m]: %g',qemb.pm.alpha*std(qemb.frespz,1));...
    'in-air V [pm]:';...
    sprintf('  %3d:%8.4f     %3d:%8.4f\n',[iaxcells 1.e12*emittances(inairlist,6)]');...
    sprintf('aver.:%8.4f\n',1.e12*mean(emittances(inairlist,6)));...
    'pinhole V [pm]:';...
    sprintf('  D9:%8.4f',1.e12*emittances(d9,6));...
    sprintf(' D11:%8.4f',1.e12*emittances(d11lens,6));...
    sprintf('ID25:%8.4f',1.e12*emittances(id25,6))};

figure(1);	% emittances
plot(sbpm,1.e12*emittances(alllist,2:2:end));
hold on
plot(sbpm(inairlist),1.e12*emittances(inairlist,end),'ro','MarkerFaceColor','r');
plot(spinhole,1.e12*emittances([d9 d11lens id25],end),'ko','MarkerFaceColor','k');
hold off
ax=axis;
axis([0 844.39 0 max([ax(4);1.e-12])]);
plotskew(sskew);
xlabel('s [m]');
ylabel('\epsilon_z [pm]');
legend('Eigen emittance','projected emittance','measured emittance');
grid on

figure(2)   % Comparison model / iax
bar(1.e12*[semres.iaxemz qemb.emittances([inairlist d9 d11lens id25],6)]);
set(gca,'XTick',1:length(iaxname),'XTickLabel',iaxname);
legend('measurement','model');
grid on

function plotskew(sskew)
ns=size(sskew,2);
ylim=get(gca,'Ylim');
hold('on');
plot([sskew;sskew],repmat([0 0;-.1 .1]*ylim',1,ns),'k');
hold('off');
