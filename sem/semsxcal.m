function [ks,idmach]=semsxcal(mach,resph,respv,cell,sx)
%SEMSXCAL	Fit a skew corrector to match measured response matrices
%
%KS=SEMSXCAL(MACH,RESPH,RESPV,CELL,SX)
%
%MACH:	AT structure describing the machine
%RESPH:	Measured horizontal response matrix to vertical steerers
%RESPV: Measured vertical response matrix to horizontal steerers
%CELL:	Cell number of the fitted skew corrector
%SX:	Sextupole id (4,13,20,24)
%
%KS:	Fitted skew corrector strength [m-1]
%
%RESPH and RESPV should be differences between responses with corrector
%ON and corrector OFF
%
%[KS,IDMACH]=SEMSXCAL(...) also returns IDMACH, AT structure with the
%fitted skew corrector
%
%See also: SEMADDID, SEMADDIDMARKER, SEMIDCAL

switch sx
case 4, line=1;
case 13, line=3;
case 20, line=5;
case 24, line=7;
end
cellidx=mod(cell-4,32);
period=floor(cellidx/2);
if cellidx > 2*period, line=15-line; end
idmach=mach(:);
bpmidx=findcells(idmach,'BetaCode','PU');
thinsext=findcells(idmach,'BetaCode','LD3');
thicksext=findcells(idmach,'BetaCode','SX');
sextidx=sort([thinsext thicksext]);
hidx=sextidx(selcor(8));	% select horizontal steerers
vidx=sextidx(selcor(9));	% select vertical steerers
ididx=sextidx(14*period+line);
disp(mach{ididx});
idresp=semderiv(idmach,0,ididx,bpmidx,hidx(1:6:96),vidx(1:6:96));
resp=[respv(:);resph(:)];
%sk=semsolve4(idresp,resp,1);
sk=semsolve0(idresp,resp);
ks=mean(sk,2);
idmach=setcellstruct(mach,'PolynomA',ididx,ks,2);

function sk=semsolve0(drv,resp)
ok=isfinite(resp);
sk=sum(drv(ok).*resp(ok))/sum(drv(ok).*drv(ok));
