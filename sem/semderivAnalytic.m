function [resp,tunes]=semderivAnalytic(mach,dpp,fitlist,blist,hslist,vslist,dispfunc)
%SEMDERIV       Compute derivatives of response matrix
%
%[resp,tunes]=semderiv(mach,fitidx,bpmidx,hsteeridx,vsteeridx)
%
%MACH:      AT machine structure
%FITIDX:   Index of varying skew quadrupoles
%BPMIDX:    Index of BPMs
%HSTEERIDX: Index of horizontal steerers
%VSTEERIDX: Index of vertical steerers
%
%RESP: response matrix for skew quads: [nbpms*(nhsteer+nvsteer) length(fitidx)]
%TUNES: initial tunes

if nargin < 7, dispfunc=@(i,itot) 0; end
nq=length(fitlist);
nbpm=length(blist);
nhst=length(hslist);
nvst=length(vslist);
% [v,j,kdx]=unique([fitlist blist hslist vslist]); %#ok<ASGLU>
% qidx=kdx(1:nq);
% bidx=kdx(nq+(1:nbpm));
% hsidx=kdx(nq+nbpm+(1:nhst));
% vsidx=kdx(nq+nbpm+nhst+(1:nvst));
% alphal=findspos(mach,length(mach)+1)*mcf(mach);
% 
% [lindata,beta,mu,eta]=atavedata(mach,dpp,[v length(mach)+1]);
% mutot=lindata(end).mu;
% tunes=mutot/2/pi;
% phase=mu./mutot(ones(length(lindata),1),:);
% 
% hkq=responsem([beta(qidx,1) phase(qidx,1) eta(qidx,1)],[beta(hsidx,1) phase(hsidx,1) eta(hsidx,1)],{tunes(1),alphal});
% vqb=responsem([beta(bidx,2) phase(bidx,2)],[beta(qidx,2) phase(qidx,2)],tunes(2));
% 
% vkq=responsem([beta(qidx,2) phase(qidx,2)],[beta(vsidx,2) phase(vsidx,2)],tunes(2));
% hqb=responsem([beta(bidx,1) phase(bidx,1) eta(bidx,1)],[beta(qidx,1) phase(qidx,1) eta(qidx,1)],{tunes(1),alphal});

disp('ANALYTIC QEMPANEL Response V2')
bndidx=find(atgetcells(mach,'BendingAngle'));
%bndidx=sort(bndidx([1:4:end,4:4:end]));% only long part

%% get anlytical RM response
[...
    dX_dq, dY_dq, ...
    dDx_dq, dDx_db, Dx, ...
    dXY_ds, dYX_ds, ...
    dDx_ds, dDy_ds,dDy_da...
    ]=CalcRespXXRespMat_thick_V2(...
    mach',dpp,...
    blist',... % bpm
    hslist,...  % correctors
    fitlist,... % quadrupoles
    bndidx,... % any index, unused.
    fitlist);

resp=zeros(nbpm*(nhst+nvst),nq);
respa=resp;
LQ=atgetfieldvalues(mach,fitlist,'Length');

qind=find(atgetcells(mach,'Class','Quadrupole'))'; % response matrix computed always at all quadrupoles
quadforresponse=find(ismember(qind,fitlist)); % quadrupoles to use for fit amoung all

for iq=1:nq
   % rv=vqb(:,ib)*hkq(ib,:);	% ATTENTION
   % rh=hqb(:,ib)*vkq(ib,:);
   % resp(:,ib)=[rv(:);rh(:)];
    ib = quadforresponse(iq); % use only selected quadrupoles
   
    rva=dYX_ds(:,:,ib);	% ANALYTIC
    rha=dXY_ds(:,:,ib);
    respa(:,iq)=[rva(:);rha(:)]./LQ(iq); %FACTOR LQ length of quadrupoles to make the 2 response identical.
    dispfunc(iq,nq);
end

% % to test several times, run in command line >> global semres; semres=rmfield(semres,'quadresponse');

% figure; 
% plot(resp(:));
%     hold on;
%     plot(respa(:));
%     legend('analytic sempanel','analytic x L_{Quad}');
%     storefigure('skewderivCompTest')

    % use analytic for fit.
    resp=respa;
    