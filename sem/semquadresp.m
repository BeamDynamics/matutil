function quadresponse = semquadresp(qemb,qemres,semres,dispfunc)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if nargin>=4
    vargs1={};
    vargs2={@(i,itot) nselect(2,dispfunc,i,itot)};
else
    vargs1={};
    vargs2={@(i,itot) nselect(2,@(j,jtot) fprintf('\b\b\b\b\b\b\b\b\b%4d/%4d',[j,jtot]),i,itot)};
end

quadresp=semderiv(qemb.at(:),qemres.dpp,qemres.qpidx,qemres.bpmidx,...
    qemres.steerhidx(semres.hlist),qemres.steervidx(semres.vlist),vargs1{:});
[~,quadetaresp]=qemdispderiv(qemb.at(:),qemres.ct,@setskew,1.e-4,...
    qemres.qpidx,qemres.bpmidx,vargs2{:});
lq=size(quadresp,1)+size(quadetaresp,1);
quadresponse=[quadresp;quadetaresp].*qemres.ql(:,ones(1,lq))';

    function elt=setskew(elt,dval)
        vini=elt.PolynomA(2);
        elt.PolynomA(2)=vini+dval;
    end
end
