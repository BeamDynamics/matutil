function mach=sembuildat(mach,qemres,kn,klncor,dipdelta,ks,klscor,diptilt)
%SEMBUILDAT		Updates the AT structure with strengths, tilts
%
%MACH=SEMBUILDAT(MACH0,QEMRES,KN,NCOR,KS,SCOR,DIPTILT,DIPDELTA)
%
%MACH : initial machine AT structure
%QEMRES : fields bpmidx,qpidx,dipidx,qcoridx,skewidx,qcorl,skewl
%KN : Normal quadrupole strengths
%NCOR : Normal corrector strengths
%KS : Skew quadrupole strengths
%SCOR : Skew corrector strengths
%DIPTILT: Dipole tilt
%DIPTILT: Dipole field error
%
%MACH : Resulting AT structure
%
if nargin >= 8
    mach=atsettilt(mach,qemres.dipidx,diptilt);
end
if nargin >= 7
    mach=setcellstruct(mach,'PolynomA',qemres.skewidx,klscor./qemres.skewl,2);
end
if nargin >= 6
    [k,tilts]=semtilt(kn,ks);
    mach=atsettilt(mach,qemres.qpidx,tilts);
elseif nargin >= 3
    k=kn;
end
if nargin >= 5
    bend=getcellstruct(mach,'BendingAngle',qemres.dipidx);
    mach=setcellstruct(mach,'BendingAngle',qemres.dipidx,(1+dipdelta).*bend);
end
if nargin >= 4
    mach=setcellstruct(mach,'PolynomB',qemres.qcoridx,klncor./qemres.qcorl,2);
end
if nargin >= 3
    mach=setcellstruct(mach,'K',qemres.qpidx,k);
    mach=setcellstruct(mach,'PolynomB',qemres.qpidx,k,2);
end
