function [x,f0,f]=skewcalib(mach,skewidx,bpmidx,steeridx,steerfit)

[rv,rh]=semgetresp('/machfs/MDT/2008/Jan16/resp2',steerfit,steerfit);
[rv0,rh0]=semgetresp('/machfs/MDT/2008/Jan16/resp1',steerfit,steerfit);
rdiff=[rv(:)-rv0(:);rh(:)-rh0(:)];
ok=isfinite(rdiff);
s4=[2 3 4 5 7 8 9 10 11 12];
s13=[1 6 13 14 15 16];
s20=17:32;
skewcor=load_correction('/machfs/MDT/2008/Jan16/resp2/skewcor.dat',7);
corr4=semderiv(mach,0,skewidx(s4),bpmidx,steeridx,steeridx)*skewcor(s4);
corr13=semderiv(mach,0,skewidx(s13),bpmidx,steeridx,steeridx)*skewcor(s13);
corr20=semderiv(mach,0,skewidx(s20),bpmidx,steeridx,steeridx)*skewcor(s20);
rdifok=rdiff(ok);
corrs=[corr4(ok) corr13(ok)+corr20(ok)];

schi1=@(x) semchi2(x,rdifok,sum(corrs,2));
schi2=@(x) semchi2(x,rdifok,corrs);

f0=schi2([1;1]);
%[x,f]=fminsearch(schi1,[1]);
[x,f]=fminsearch(schi2,[1;1]);

function v=semchi2(x,rdiff,cors)
dd=rdiff-cors*x;
v=sqrt(sum(dd.*dd)/224);
