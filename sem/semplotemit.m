function semplotemit(sig)
%SEMPLOTEMIT Plot emittance ellipse
%   Detailed explanation goes here

emittance2=sig(1,1)*sig(2,2)-sig(1,2)*sig(2,1);
xmax=2*sqrt(sig(1,1));
[xx,zz]=meshgrid(linspace(-xmax,xmax,80),linspace(-0.8*xmax,0.8*xmax,120));
dd=exp(-(sig(2,2)*xx.*xx-(sig(1,2)+sig(2,1))*xx.*zz+sig(1,1)*zz.*zz)/2/emittance2);
%surf(xx,zz,dd);
contourf(xx,zz,dd,'LineColor','none');
axis equal
grid on
end

