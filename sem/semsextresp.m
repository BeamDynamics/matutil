function sextresponse = semsextresp(qemb,qemres,semres,dispfunc)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if nargin>=4
    vargs1={@(i,itot) nselect(16,dispfunc,i,5*itot)};
    vargs2={@(i,itot) nselect(8,dispfunc,2*itot+2*i,5*itot)};
else
    vargs1={};
    vargs2={};
end

sxh=getcellstruct(qemres.at(:),'PolynomB', qemres.sextidx,3);
Hl=-2*(qemres.sxl.*sxh);
sextresp=semderiv(qemb.at(:),qemres.dpp,qemres.sextidx,qemres.bpmidx,...
    qemres.steerhidx(semres.hlist),qemres.steervidx(semres.vlist),vargs1{:});
[~,sextetaresp]=qemdispderiv(qemb.at(:),qemres.ct,@setdzs,0.001,...
    qemres.sextidx,qemres.bpmidx,vargs2{:});
sextresponse=[sextresp.*Hl(:,ones(1,size(sextresp,1)))';sextetaresp];

    function elem=setdzs(elem,dzs)
        inidx=elem.T2(1);
        inidz=elem.T2(3);
        elem=atshiftelem(elem,inidx,inidz+dzs);
    end
end
