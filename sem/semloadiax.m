function iaxemz = semloadiax(datadir)
%IAXEMZ=SEMLOADIAX(DATADIR) load IAX values

iaxemz=NaN(14,1);
try
    dest=[1:12 14 13];      % destination of the values read from file
    iax=load(fullfile(datadir, 'iaxemittance'));
    iaxemz(dest(1:size(iax.iax_list,1)))=1.0e-9*iax.iax_values;
catch
    warning('sem:noiax','Cannot read IAX data from the data directory');
end
end

