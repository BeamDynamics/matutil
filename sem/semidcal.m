function [ks,idmach]=semidcal(mach,resph,respv,cell,id,lmid)
%SEMIDCAL	Fit a skew corrector to match measured response matrices
%
%KS=SEMIDCAL(MACH,RESPH,RESPV,CELL,ID,LMID)
%
%MACH:	AT structure describing the machine
%RESPH:	Measured horizontal response matrix to vertical steerers
%RESPV: Measured vertical response matrix to horizontal steerers
%CELL:	Cell number of the fitted skew corrector
%ID:	ID number in the straight section (1,2 or 3)
%LMID:  Distance of IDs 1 and 3 from the center (default [0 1.8])
%
%KS:	Fitted skew corrector strength [m-1]
%
%RESPH and RESPV should be differences between responses with Gap open
%and gap closed
%
%[KS,IDMACH]=SEMIDCAL(...) also returns IDMACH, AT structure with the
%fitted id
%
%See also: SEMADDID, SEMADDIDMARKER, SEMSXCAL


if mod(cell,2)==0
%  if id==1, cell=mod(cell-3,32)+1; end
   pat='IDH%d';
else
   pat='IDL%d';
end
if nargin < 6
   idmach=semaddid(mach,cell);
else
   idmach=semaddid(mach,cell,lmid);
end
bpmidx=findcells(idmach,'BetaCode','PU');
thinsext=findcells(idmach,'BetaCode','LD3');
thicksext=findcells(idmach,'BetaCode','SX');
sextidx=sort([thinsext thicksext]);
hidx=sextidx(selcor(8));
vidx=sextidx(selcor(9));
ididx=findcells(idmach,'FamName',sprintf(pat,id));
idresp=semderiv(idmach,0,ididx,bpmidx,hidx(1:6:96),vidx(1:6:96));
resp=[respv(:);resph(:)];
%sk=semsolve4(idresp,resp,1);
sk=semsolve0(idresp,resp);
ks=mean(sk,2);
fprintf('Residual: %g -> %g\n',std2(resp),std2(resp-ks*idresp));
idmach=setcellstruct(idmach,'PolynomA',ididx,ks,2);

function sk=semsolve0(drv,resp)
ok=isfinite(resp);
sk=sum(drv(ok).*resp(ok))/sum(drv(ok).*drv(ok));
