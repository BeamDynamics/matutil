function [cor,skewresponse]=semsolverdt(qemb,qemres,semres,cor0,dispweight,varargin)
%SEMRDT compute resonance driving terms at BPM locations
%
%SKEWCOR=SEMSOLVERDT(QEMB,QEMRES,SEMRES,SKEWCOR,DISPWEIGHT)
%

mach=qemat(qemres,qemb,false);
a2=mcf(mach)*dispweight;
if isfield(semres,'skewresponse')
    skewresponse=semres.skewresponse;
else
    skewresponse=semskewresp(qemb,qemres,semres,varargin{:});
end
[resp1,resp2]=semrdtresp(mach,qemres.bpmidx,[qemres.qpidx qemres.skewidx]);
strength=[qemres.ql.*qemb.ks;cor0];
f1=resp1*strength;
f2=resp2*strength;

nbpm=length(qemres.bpmidx);
if isfield(semres,'skewkeep')
    skmask=semres.skewkeep;
else
    skmask=true(size(qemres.skewidx));
end
orbitrange=size(skewresponse,1)-nbpm;
sk=[false(size(qemres.qpidx)) skmask];
a1=1-a2;
rsp=[...
    a1*real(resp1(:,sk));a1*imag(resp1(:,sk));...
    a1*real(resp2(:,sk));a1*imag(resp2(:,sk));...
    a2*skewresponse(orbitrange+(1:nbpm),skmask)];
b=[...
    a1*real(f1);a1*imag(f1);...
    a1*real(f2);a1*imag(f2);...
    a2*qemb.frespz];

cor=cor0;
cor(skmask)=cor(skmask)-rsp\b;
end