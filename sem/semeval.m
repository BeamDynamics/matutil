function stats=semeval(cors,dirname,steerlist)
%stats=SEMEVAL(cors,dirname,steerlist)	Evaluates deviation from model
%
%SEMEVAL:
% - reads responses from $DIRNAME/steer[HV]xx
% - returns the average of fitted strengths
%
% CORS    : skew quadrupole strengths
% DIRNAME : directory of response matrix files (default $APPHOME/sem/skewresp)
% STEERLIST : list of steerer numbers (default 1:12:96)
%
% STATS : rms measured coupled response, model respons, and difference
%
%See also SEMDATA, SEMSOLVE
%
if nargin < 3, steerlist=1:12:96; end	% first steerer of each pair
if nargin < 2, dirname=[]; end

q=zeros(length(steerlist),3);
count=1;
for steerer1=steerlist
   steerer2=steerer1+6;
   rh=getorbit(dirname,steerer1);
   rv=getorbit(dirname,steerer2);
   try
      disp(['Steerer ' int2str([steerer1 steerer2]) ': Processing.']);
      q(count,:)=semdiff(cors,steerer1,rh,steerer2,rv);
      disp(0.03560472*q(count,:));
      count=count+1;
   catch
      [err,errid]=lasterr;
      disp(err);
   end
end
stats=sqrt(sum(q.*q)/count);
disp(0.03560472*stats);

function resp=getorbit(dir,steerer)
for wait=1:200
   try
      resp=semdata(dir,steerer);
      break;
   catch
      disp(['Steerer ' int2str(steerer) ': Waiting for input file.']);
      pause(4);
   end
end
