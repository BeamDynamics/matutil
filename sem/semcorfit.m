function [ks,is]=semcorfit(semres1,semres2,opticsdir,neig)

% analyzes the difference of 2 response matrices in terms of corrector
% change


% ORBIT.*W = RESPONSE * (KS./V)

if nargin < 4, neig=64; end
resp=[semres2.respv(:)-semres1.respv(:);...
semres2.resph(:)-semres1.resph(:);...
semres2.frespz-semres1.frespz];
orbitrange=size(semres1.skewresponse,1)-224;
v=sr.fold(std(semres1.skewresponse(1:orbitrange,:),1,1));		% normalize response matrix
v=sr.unfold(repmat(1./sqrt(sum(v.*v,2)/32),1,32));
w=[ones(orbitrange,1);semres1.wdisp*ones(224,1)];
sk=semsolve4(semres1.skewresponse.*(w*v'),resp.*w,neig);
ks=mean(sk,2).*v;
if nargin >= 3
   coef=load_corcalib(opticsdir,10);
   is=ks./coef(:);
end
