function w=fwhm(sig,x)
%FWHM	full width half maximum of a distribution

l=find(sig >= 0.5*max(sig));
l1=l(1);
l2=l(length(l));
if nargin > 1
w=x(l2)-x(l1);
else
w=l2-l1;
end
