function [body,head]=cutline(file,bodylist,headlist)
for i=1:10000
   cline=fgetl(file);
   if ~ischar(cline), break, end
   tabs=strfind(cline,char(9));
   beg=[1 tabs+1];
   fnd=[tabs-1 length(cline)];
   if nargin >= 3
       head{i}=cline(beg(headlist):fnd(headlist)); %#ok<AGROW>
   end
   if nargin >= 2
      beg=beg(bodylist);
      fnd=fnd(bodylist);
   end
   vals=NaN*ones(1,length(beg));
   for j=find(fnd>=beg)
      vals(j)=str2num(cline(beg(j):fnd(j)));
   end
   body(i,:)=vals; %#ok<AGROW>
end
