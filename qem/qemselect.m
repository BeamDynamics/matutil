function lines=qemselect(steerlist)

[ix,iy]=meshgrid(448*(steerlist-1),(1:448)');
nlines=448*length(steerlist);
lines=reshape(ix+iy,nlines,1);
