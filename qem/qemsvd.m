function dq=qemsvd(a,b,neig)

[u,s,v]=svd(a,0);
lambda=diag(s);
nmax=length(lambda);
eigsorb=u'*b;
if neig > nmax
    neig=nmax;
    warning('Svd:maxsize',['number of vectors limited to ' num2str(nmax)]);
end
eigscor=eigsorb(1:neig)./lambda(1:neig);
dq=v(:,1:neig)*eigscor;
