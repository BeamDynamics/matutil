function [rx,rz]=qemdispderivAnalytic(mach,varidx,bpmidx)
%QEMDISPDERIV	compute derivatives of the frequency response
%
%[RX,RZ]=qemdispderivAnalytic(MACH,VARIDX,BPMIDX)
%
%MACH:      AT machine structure
%VARIDX:	Index of varying elements
%BPMIDX:	Index of BPMs
%
%RX:        derivative of the horizontal response to frequency
%RZ:        derivative of the vertical response to frequency

disp('Analytic');

nb=length(bpmidx);

alldip=find(atgetcells(mach,'BendingAngle'));
Ndip=numel(alldip);
dipsel=sort([1:4:Ndip,4:4:Ndip]);
bndidx=sort(alldip(dipsel));% only long part
bang=atgetfieldvalues(mach,alldip,'BendingAngle'); 

% make sure order is as in qempanel vector.
[~,b]=ismember(varidx,bndidx);

%% compute analytic response

[N,S,~,~]=dDxyDtheta(mach,bpmidx,alldip,'magmodel','thick'); 

% [N,S,Dx,Dy]=dDxyDtheta(mach,bpmidx,alldip,'magmodel','thick'); 
% [Ndq,Sdq,Dxdq,Dydq]=dDxyDthetaDQ(mach,bpmidx,alldip,'magmodel','thick'); 
% [l,~,~]=atlinopt(mach,0,bpmidx);
% Dx0=arrayfun(@(a)a.Dispersion(1),l)';
% s=arrayfun(@(a)a.SPos(1),l);
% figure; 
% %plot(s,Dx0,'x-'); hold on; 
% plot(s,Dx-Dx0,'o-'); hold on; 
% plot(s,Dxdq-Dx0,'s-'); 
% xlabel('s [m]');
% ylabel('\eta_x - \eta_{x,0} [m], ESRF 2017');
% legend(' analytic Bend response',' analytic Bend + DQ response');

% negative sign, probably due to dipole sign convention
N_sort=-N(:,dipsel(b));  
% qempanel response in [m/Hz] instead of [m/%]
N_sort=N_sort/mcf(mach); 
% in qempanel the response is for scaling factors, not deltas
N_sort=N_sort.*repmat(bang(dipsel(b))',nb,1);


S_sort=S(:,dipsel(b));  
% qempanel response in [m/Hz] instead of [m/%]
S_sort=S_sort/mcf(mach); 
% in qempanel the response is for scaling factors, not deltas
S_sort=S_sort.*repmat(bang(dipsel(b))',nb,1);


rx=N_sort;
rz=S_sort;

end
