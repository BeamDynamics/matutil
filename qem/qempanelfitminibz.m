function [newkn,newknlow]=qempanelfitminibz(qemb,qemres,semres,okbpm,varargin)
%QEMPANELFITMINIBZ Vary quadrupole strengths to fit the measured response matrix
%KN=QEMPANELFITMINIBZ(QEMB,QEMRES,SEMRES,OKBPM,DISPFUNC)
%
%           Vary strenghts of QF5, QD4, QD6, QF7 quad. pairs in ID23
%           plus all families
%
%QEMB:
%QEMRES:    global parameters (qpidx,bpmidx,steerhidx,steervidx,hlist,vlist,
%           khrot,khgain,kvrot,kvgain,brot,bhgain,bvgain,
%           tunes,resph,respv,bhscale,frespx)
%SEMRES:    global parameters (resph,respv,frespz)
%OKBPM:     Select a subset of valid BPMS (default:all)
%DISPFUNC:  Function to display the progress: DISPFUNC(I,IMAX)
%
%KN=QEMPANELFITMINIBZ(QEMB,QEMRES,SEMRES,OKBPM,MODE,DISPFUNC)
%MODE:      Structure allowing control of SVD inversion. (default:)
%
%KN:        New quadrupole strengths

if nargin<4, okbpm=[]; end;
narg=length(varargin);
if narg>0 && isa(varargin{narg},'function_handle')
    disparg={@(i,itot) nselect(2,varargin{narg},i,itot)};
    narg=narg-1;
else
    disparg={};
end
if narg>0 && isstruct(varargin{narg})
    mode=varargin{narg};
    narg=narg-1;
else
    mode=struct;
end
shidx=qemres.steerhidx(qemres.hlist);
svidx=qemres.steervidx(qemres.vlist);

qid=reshape(1:256,16,16);
qf2qid=reshape(qid([1 16],:),1,[]);
qd3qid=reshape(qid([2 15],:),1,[]);
qd4qid=[reshape(qid([3 14],:),1,[]) reshape(qid([6 11],[1:9 11:end]),1,[])];
qf5qid=[reshape(qid([4 13],:),1,[]) reshape(qid([5 12],[1:9 11:end]),1,[])];
% qd6qid=reshape(qid([7 10],:),1,[]);
% qf7qid=reshape(qid([8  9],:),1,[]);
qd6qid=reshape(qid([7 10],[1:9 11:end]),1,[]);
qf7qid=reshape(qid([8  9],[1:9 11:end]),1,[]);
qf5c23qid=reshape(qid([5 12],10),1,[]);
qd4c23qid=reshape(qid([6 11],10),1,[]);
qd6c23qid=reshape(qid([7 10],10),1,[]);
qf7c23qid=reshape(qid([8  9],10),1,[]);

kn=squeeze(atgetfieldvalues(qemb.at,qemres.qpidx,'PolynomB',{2}));
knlow=squeeze(atgetfieldvalues(qemb.at,qemres.qdlowidx,'PolynomB',{2}));

machfunc={...
    @(mach,dv) atsetfieldvalues(mach,qemres.qpidx(qf2qid),'PolynomB',{2},kn(qf2qid)+dv),...
    @(mach,dv) atsetfieldvalues(mach,qemres.qpidx(qd3qid),'PolynomB',{2},kn(qd3qid)+dv),...
    @(mach,dv) atsetfieldvalues(mach,qemres.qpidx(qd4qid),'PolynomB',{2},kn(qd4qid)+dv),...
    @(mach,dv) atsetfieldvalues(mach,qemres.qpidx(qf5qid),'PolynomB',{2},kn(qf5qid)+dv),...
    @(mach,dv) atsetfieldvalues(mach,qemres.qpidx(qd6qid),'PolynomB',{2},kn(qd6qid)+dv),...
    @(mach,dv) atsetfieldvalues(mach,qemres.qpidx(qf7qid),'PolynomB',{2},kn(qf7qid)+dv),...
    @(mach,dv) atsetfieldvalues(mach,qemres.qpidx(qf5c23qid),'PolynomB',{2},kn(qf5c23qid)+dv),...
    @(mach,dv) atsetfieldvalues(mach,qemres.qpidx(qd4c23qid),'PolynomB',{2},kn(qd4c23qid)+dv),...
    @(mach,dv) atsetfieldvalues(mach,qemres.qpidx(qd6c23qid),'PolynomB',{2},kn(qd6c23qid)+dv),...
    @(mach,dv) atsetfieldvalues(mach,qemres.qpidx(qf7c23qid),'PolynomB',{2},kn(qf7c23qid)+dv),...
    @(mach,dv) atsetfieldvalues(mach,qemres.qdlowidx,'PolynomB',{2},knlow+dv)};

orbit0=findsyncorbit(qemb.at,qemres.ct,qemres.bpmidx);
dpp=orbit0(5);
dresp=qemderivmach(qemb.at,dpp,machfunc,0.0001,qemres.bpmidx,shidx,svidx,disparg{:});
[rh,rh2v,rv2h,rv,frh,frv]=qemdecode(qemb.at,qemres.ct,qemres,...
    qemres.resph,semres.respv,semres.resph,qemres.respv,...
    qemres.frespx,semres.frespz,...
    qemres.khrot,qemres.khgain,qemres.kvrot,qemres.kvgain,...
    qemres.brot,qemres.bhgain,qemres.bvgain); %#ok<ASGLU>
[rh0,rh2v0,rv2h0,rv0,frh0,frv0]=qemcode(qemb.at,qemres.ct,...
    qemres.steerhidx(qemres.hlist),qemres.steervidx(qemres.vlist),...
    qemres.bpmidx,[],[],[],[],[],[],[],orbit0); %#ok<ASGLU>
resp0=[rh0(:);rv0(:);frh0;qemb.pm.fractunes'];
resp=[rh(:);rv(:);qemres.bhscale*frh;qemres.fractunes'];
mode.vnorm=[0.1*ones(1,6) ones(1,5)];
dd=qemerrfit(length(shidx),length(svidx),resp-resp0,dresp,mode,okbpm);

newkn=qemb.kn;
newkn(qf2qid)=qemb.kn(qf2qid)+dd(1);
newkn(qd3qid)=qemb.kn(qd3qid)+dd(2);
newkn(qd4qid)=qemb.kn(qd4qid)+dd(3);
newkn(qf5qid)=qemb.kn(qf5qid)+dd(4);
newkn(qd6qid)=qemb.kn(qd6qid)+dd(5);
newkn(qf7qid)=qemb.kn(qf7qid)+dd(6);
newkn(qf5c23qid)=qemb.kn(qf5c23qid)+dd(7);
newkn(qd4c23qid)=qemb.kn(qd4c23qid)+dd(8);
newkn(qd6c23qid)=qemb.kn(qd6c23qid)+dd(9);
newkn(qf7c23qid)=qemb.kn(qf7c23qid)+dd(10);
newknlow=knlow+dd(11);
end
