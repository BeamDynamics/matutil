function  setPID(IX, IZ, PX, PZ, N1X, N1Z ,N2X, N2Z,MaxX,MaxZ)

fofbcorh=dvopen('tango:sr/d-fofbcorrection/globalX');
fofbcorv=dvopen('tango:sr/d-fofbcorrection/globalX');
devIX=dvopen('tango:sr/d-fofbcorrection/globalX/IntegralGain');
devPX=dvopen('tango:sr/d-fofbcorrection/globalX/ProportionalGain');
devN1X=dvopen('tango:sr/d-fofbcorrection/globalX/Notch1Gain');
devN2X=dvopen('tango:sr/d-fofbcorrection/globalX/Notch2Gain');
devMaxPosX=dvopen('tango:sr/d-fofbcorrection/globalX/PositionMaxDeviation');
devIZ=dvopen('tango:sr/d-fofbcorrection/globalZ/IntegralGain');
devPZ=dvopen('tango:sr/d-fofbcorrection/globalZ/ProportionalGain');
devN1Z=dvopen('tango:sr/d-fofbcorrection/globalZ/Notch1Gain');
devN2Z=dvopen('tango:sr/d-fofbcorrection/globalZ/Notch2Gain');
devMaxPosZ=dvopen('tango:sr/d-fofbcorrection/globalZ/PositionMaxDeviation');
err=dvcmd(devIX,'DevWrite',double(IX));
err=dvcmd(devPX,'DevWrite',double(PX));
err=dvcmd(devN1X,'DevWrite',double(N1X));
err=dvcmd(devN2X,'DevWrite',double(N2X));
err=dvcmd(devIZ,'DevWrite',double(IZ));
err=dvcmd(devPZ,'DevWrite',double(PZ));
err=dvcmd(devN1Z,'DeVWrite',double(N1Z));
err=dvcmd(devN2Z,'DevWrite',double(N2Z));
MaxX=dvcmd(devMaxPosX,'DevWrite',double(MaxX));
MaxZ=dvcmd(devMaxPosZ,'DevWrite',double(MaxZ));
pause(1)
err=dvcmd(fofbcorh,'WritePIDCoeffsToFPGA'); 
err=dvcmd(fofbcorv,'WritePIDCoeffsToFPGA');
pause(1)
