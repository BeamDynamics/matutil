function [normI,phi,firstX,MresH,MresVH] = CalMresfdaH(n_and_dateX,Xamp,qemres)
%uses calculated sine and cosine instead of fda CorNSTEXZstuf signals with a shift by 22 samples for the Q Cor signal
% to get the Iand Q data

% set_path % TO BE COMMENTED OUT WHEN MERGING WITH QEMPANEL
tic
%load lastMresV
[Xsnif, Zsnif] =getFDAreaderXZ(n_and_dateX);
[XCor, ZCor] =getFDAreaderXZCor(n_and_dateX);
siz=size(Xsnif);
NBPM=siz(1); NSTE=size(XCor,1);
MresH=zeros(NBPM,NSTE);
% MresV=zeros(NBPM,NSTE);
MresVH=zeros(NBPM,NSTE);
% MresHI=zeros(NBPM,NSTE);
% MresVI=zeros(NBPM,NSTE);

% MresHQ=zeros(NBPM,NSTE);
% MresVQ=zeros(NBPM,NSTE);
% MresHVQ=zeros(NBPM,NSTE);

%for i=1:NSTE
%    CorNSTEXstuf(i,1:siz(2))=XCor(i,1:siz(2))-mean(XCor(i,:));    
%end
CorNSTEXstuf=XCor-repmat(mean(XCor,2)',siz(2),1)';

disp(['Xamp=' num2str(Xamp) ...
      '   # steerers=', num2str(size(CorNSTEXstuf,1)) ...
      '   buffer size=',num2str(size(CorNSTEXstuf,2)) ])
  
[ CXI,CXQ,firstX,lastX,lengthX]  = Corfast_fda2(CorNSTEXstuf,Xamp,Xamp/20);

% Xres=zeros(1,siz(2));
% Zres=zeros(1,siz(2));
% CX=zeros(1,siz(2));
% CZ=zeros(1,siz(2));

C=CXI./repmat(sum(abs(CXI),2),1,siz(2));
A=max(abs(CXI),[],2);
MresHI=Xsnif*C'./repmat(A',NBPM,1);
MresVI=Zsnif*C'./repmat(A',NBPM,1);
CQ=CXQ./repmat(sum(abs(CXQ),2),1,siz(2));
AQ=max(abs(CXQ),[],2);
MresHQ=Xsnif*CQ'./repmat(AQ',NBPM,1);
MresVQ=Zsnif*CQ'./repmat(AQ',NBPM,1);   

% for j=1:NSTE
%     j
%     
%     CX(1:siz(2))=CXI(j,1:siz(2));
%     C=CX/sum(abs(CX))/max(abs(CX));
%     
%     [ DX] = CXQ(j,1:siz(2));
%     D=DX/sum(abs(DX))/max(abs(DX));
%         
%     for k=1:NBPM
%        
%         Xres(1:siz(2))=Xsnif(k,1:siz(2));
%         Zres(1:siz(2))=Zsnif(k,1:siz(2));
% 
%     
%         MresHI(k,j)=MresH(k,j)+Xres*C';
%         MresVI(k,j)=MresV(k,j)+Zres*C';
% 
%         MresHQ(k,j)=MresH(k,j)+Xres*D';
%         MresVQ(k,j)=MresV(k,j)+Zres*D';
%            
%     end
%     
% end

stdHI=std(MresHI);
stdHQ=std(MresHQ);
% stdVI=std(MresVI);
% stdVQ=std(MresVQ);
%calibration factor OK for.1A=32768
G=1e9*.2/pi*2*.5/32768;

normI=sqrt(stdHI.^2+stdHQ.^2)./stdHI;
normQ=sqrt(stdHI.^2+stdHQ.^2)./stdHQ;
phi=stdHQ./sqrt(stdHI.^2+stdHQ.^2);



for i=1:NSTE
    if phi(i)< .8
        MresH(:,i)=MresHI(:,i)*normI(i)/G;
        MresVH(:,i)=MresVI(:,i)*normI(i)/G;
    else
        MresH(:,i)=MresHQ(:,i)*normQ(i)/G;
        MresVH(:,i)=MresVQ(:,i)*normQ(i)/G;
    end
end

time=time_label;
file=['TimeLabelMresX',time];

save(fullfile(qemres.datadir,file),'n_and_dateX' , 'MresH','MresVH');

toc