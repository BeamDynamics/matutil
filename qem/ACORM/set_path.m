function set_path

%addpath /users/diag/matlab/libera_data


addpath /mntdirect/_users/diag/matlab/FOC_op
addpath /mntdirect/_users/diag/matlab/libera_data
addpath /mntdirect/_users/diag/matlab/TMonLibera
addpath /users/diag/matlab/liberaFOC_test
addpath /segfs/tango/cppserver/machine/diagnostic/bpm/FastDataArchiver/fa-archiver/matlab
addpath /users/diag/matlab/FastMres
