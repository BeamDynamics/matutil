function [Xsnif, Zsnif] =getFDAreaderXZ(n_and_date)
%uses the fda data extractor to get Intensity n data recorded at the date
%"date"
%Extractor argin description: (DevVarStringArray)
%[0] Number of samples to read
%[1] Start date (ex: 22/06/2015 16:15:00[.103647], les us sont optionnelles)
%for instance: n_and_date = {'100','28/09/2015 08:00:00'} or {'100','28/09/2015 08:00:00.178546'} 
n=str2double(n_and_date(1));
fda='tango:sr/d-bpmlibera/fda_reader';
devfda=dvopen(fda);
dvtimeout(devfda,50);

pause(10); % some times 5s are not enough

XZ=dvcmd(devfda,'ReadXZPositions',n_and_date);

size(XZ);
X=XZ(1:224*n);
Z=XZ(1+(224*n):448*n);
Xsnif=reshape(X,224,n);
Zsnif=reshape(Z,224,n);


