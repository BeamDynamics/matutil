function [normI,phi,firstZ,MresHV,MresV] = CalMresfdaV(n_and_dateZ,Zamp,qemres)

%uses calculated sine and cosine instead of fda CorNSTEXZstuf signals with a shift by 22 samples for the Q Cor signal
% to get the Iand Q data

% set_path % TO BE COMMENTED OUT WHEN MERGING WITH QEMPANEL
tic
%load lastMresV
[Xsnif, Zsnif] =getFDAreaderXZ(n_and_dateZ);
[XCor, ZCor] =getFDAreaderXZCor(n_and_dateZ);
siz=size(Xsnif);
NBPM=siz(1); NSTE=size(XCor,1);
% MresH=zeros(NBPM,NSTE);
MresV=zeros(NBPM,NSTE);
MresHV=zeros(NBPM,NSTE);
% MresHI=zeros(NBPM,NSTE);
% MresVI=zeros(NBPM,NSTE);

% MresHQ=zeros(NBPM,NSTE);
% MresVQ=zeros(NBPM,NSTE);
% MresHVQ=zeros(NBPM,NSTE);
    
% for i=1:NSTE
%    CorNSTEZstuf(i,1:siz(2))=ZCor(i,1:siz(2))-mean(ZCor(i,:));
% end 
CorNSTEZstuf=ZCor-repmat(mean(ZCor,2)',siz(2),1)';
    
disp(['Zamp=' num2str(Zamp) ...
      '   # steerers=', num2str(size(CorNSTEZstuf,1)) ...
      '   buffer size=',num2str(size(CorNSTEZstuf,2)) ])
  
[ CZI,CZQ,firstZ,lastZ,lengthZ]  = Corfast_fda2(CorNSTEZstuf,Zamp,Zamp/10);

% Xres=zeros(1,siz(2));
% Zres=zeros(1,siz(2));
% CX=zeros(1,siz(2));
% CZ=zeros(1,siz(2));
 
C=CZI./repmat(sum(abs(CZI),2),1,siz(2));
A=max(abs(CZI),[],2);
MresHI=Xsnif*C'./repmat(A',NBPM,1);
MresVI=Zsnif*C'./repmat(A',NBPM,1);
CQ=CZQ./repmat(sum(abs(CZQ),2),1,siz(2));
AQ=max(abs(CZQ),[],2);
MresHQ=Xsnif*CQ'./repmat(AQ',NBPM,1);
MresVQ=Zsnif*CQ'./repmat(AQ',NBPM,1); 

%     for j=1:NSTE
%      j
%         
%         for k=1:NBPM
%              
%            Xres(1:siz(2))=Xsnif(k,1:siz(2));
%             Zres(1:siz(2))=Zsnif(k,1:siz(2));
%             %CX(1:siz(2))=CXI(j,1:siz(2));
%             CZ(1:siz(2))=CZI(j,1:siz(2));
%             
%             C=CZ/sum(abs(CZ));
%             A=max(abs(CZ));
%            
%          
%        
%         MresHI(k,j)=MresH(k,j)+Xres*C'/A;
%         MresVI(k,j)=MresV(k,j)+Zres*C'/A;
%        
%             
%             [ CZ ] = CZQ(j,1:siz(2));
%             
%           
%             C=CZ/sum(abs(CZ));
%             A=max(abs(CZ));
%            
%             
%         MresHQ(k,j)=MresH(k,j)+Xres*C'/A;
%         MresVQ(k,j)=MresV(k,j)+Zres*C'/A;
% 
%         
%           end
%        
%     end

% stdHI=std(MresHI);
% stdHQ=std(MresHQ);
stdVI=std(MresVI);
stdVQ=std(MresVQ);
%calibration factor OK for.1A=32768
G=1e9*.2/pi*2*.5/32768;

normI=sqrt(stdVI.^2+stdVQ.^2)./stdVI;
normQ=sqrt(stdVI.^2+stdVQ.^2)./stdVQ;
phi=stdVQ./sqrt(stdVI.^2+stdVQ.^2);



for i=1:NSTE
    if phi(i)< .8
MresV(:,i)=MresVI(:,i)*normI(i)/G;
MresHV(:,i)=MresHI(:,i)*normI(i)/G;
    else
MresV(:,i)=MresVQ(:,i)*normQ(i)/G;
MresHV(:,i)=MresHQ(:,i)*normQ(i)/G;        
    end       
end

time=time_label;
file=['TimeLabelMresZ',time];

save(fullfile(qemres.datadir,file),'n_and_dateZ' , 'MresV','MresHV');

toc