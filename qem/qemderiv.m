function [dresp,resp0]=qemderiv(mach,dpp,elemfunc,dv,varidx,bpmidx,hsidx,vsidx,dispfunc)
%QEMDERIV       Compute derivatives of response matrix
%
%[dresp,resp0]=qemderiv(mach,dpp,elemfunc,dval,varidx,bpmidx,hsteeridx,vsteeridx,dispfunc)
%
%MACH:      AT machine structure
%DPP:       Momentum deviation
%ELEMFUNC:  function NEWELEM=ELEMFUNC(OLDELEM,DVAL) which modifies each
%           varying element
%DVAL:      Parameter increment to compute the derivative
%VARIDX:	Index of varying elements
%BPMIDX:	Index of BPMs
%HSTEERIDX:	Index of horizontal steerers
%VSTEERIDX:	Index of vertical steerers
%DISPFUNC:  Function to display the progress: DISPFUNC(I,IMAX)
%
%DRESP: Derivative of response matrix for elements in fitidx:
%RESP0: Response matrix of the unperturbed mchine on 1 column:
%       [HRESPONSE(:);VRESPONSE(:);DFFRESPONSE;TUNES]

global NUMDIFPARAMS

dpsave=isfield(NUMDIFPARAMS,'DPStep');
if dpsave, dpsp=NUMDIFPARAMS.DPStep; end
NUMDIFPARAMS.DPStep=0.00255; % Compute the dispersion for a step equivalnent
%                               to the one used in the measurement
if nargin < 9
    fprintf('          ');
    dispfunc=@(i,itot) fprintf('\b\b\b\b\b\b\b\b\b%4d/%4d',[i itot]);
end
if nargin < 8, vsidx=hsidx; end
nq=length(varidx);
nbpm=length(bpmidx);
nhst=length(hsidx);
nvst=length(vsidx);
[refpts,j,kdx]=unique([bpmidx hsidx vsidx]); %#ok<ASGLU>
bidx=kdx(1:nbpm);
hsidx=kdx(nbpm+(1:nhst));
vsidx=kdx(nbpm+nhst+(1:nvst));
alpha=mcf(mach,dpp);
alphal=alpha*findspos(mach,length(mach)+1);
resp0=getresp(mach,dpp,refpts,bidx,hsidx,vsidx,alpha,alphal);
dval(1:nq)=dv;
dresp=zeros(size(resp0,1),nq);
for ib=1:nq
    iq=varidx(ib);
    qsave=mach{iq};
    mach{iq}=elemfunc(qsave,dval(ib));
    resp=getresp(mach,dpp,refpts,bidx,hsidx,vsidx,alpha,alphal);
    dresp(:,ib)=(resp-resp0)/dval(ib);
    mach{iq}=qsave;
    dispfunc(ib,nq);
end

NUMDIFPARAMS=rmfield(NUMDIFPARAMS,'DPStep');
if dpsave, NUMDIFPARAMS.DPStep=dpsp; end

    function rhvd=getresp(mach,dpp,refpts,bidx,hsidx,vsidx,alpha,alphal)
        %[lindata,beta,avemu,disp,tunes,xsi]=atavedata(mach,dpp,refpts,zeros(6,1));  %#ok<ASGLU>
        [lindata,beta,avemu,disp,tunes,xsi]=atavedata(mach,dpp,refpts); %#ok<ASGLU>
        mutot=2*pi*tunes;
        phase=avemu./mutot(ones(length(lindata),1),:);
        rh=responsem([beta(bidx,1) phase(bidx,1) disp(bidx,1)],[beta(hsidx,1) phase(hsidx,1) disp(hsidx,1)],{tunes(1),alphal});
        rv=responsem([beta(bidx,2) phase(bidx,2)],[beta(vsidx,2) phase(vsidx,2)],tunes(2));
        rhvd=[rh(:);rv(:);-disp(bidx,1)/alpha;tunes'];
    end
end
