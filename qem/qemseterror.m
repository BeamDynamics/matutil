function [mach,qemb]=qemseterror(qemres,qemb)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%dkk=0.005*randn(4,1);
dkk=[0.002;0.002;0.002;0.002];
%dkk=[0;0;0;.005];        % QF7
dkk=dkk([1:4 4:-1:1])
qemb.kn(149:156)=qemb.kn(149:156).*(1+dkk);
mach=atfittune(qemat(qemres,qemb),qemres.tunes,'QD6','QF7');
mach=atfittune(mach,qemres.tunes,'QD6','QF7');
%mach=qemat(qemres,qemb);
qemb.kn=getcellstruct(mach(:),'PolynomB',qemres.qpidx,2);
end
