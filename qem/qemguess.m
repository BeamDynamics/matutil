function [khrot,khgain,kvrot,kvgain,bhrot,bhgain,bvrot,bvgain]=...
    qemguess(mach,qemres,resph,resph2v,respv2h,respv)
%QEMGUESS	Fits BPM and steerer errors
%[KHROT,KHGAIN,KVROT,KVGAIN,BHROT,BHGAIN,BVROT,BVGAIN]=
%   QEMGUESS(MACH,QEMRES,RESPH,RESPH2V,RESPV2H,RESPV)
%
%MACH:
%QEMRES:    global parameters (bpmidx,steerhidx,steervidx,hlist,vlist,
%           bhscale,khgain,kvgain,bhgain,bvgain)

orbit0=findsyncorbit(mach,qemres.ct,qemres.bpmidx);

khrot=qemres.khrot;
khgain=qemres.khgain;
kvrot=qemres.kvrot;
kvgain=qemres.kvgain;
bhr=qemres.brot;
bvr=qemres.brot;
bhgain=qemres.bhgain;
bvgain=qemres.bvgain;
khr=khrot(qemres.hlist);
khg=khgain(qemres.hlist);
kvr=kvrot(qemres.vlist);
kvg=kvgain(qemres.vlist);
bok=all(isfinite([resph resph2v respv2h respv]),2);

% Build model without errors
[rh0,rh2v0]=qemresp(mach,qemres.ct,qemres.bpmidx,qemres.steerhidx(qemres.hlist),...
    @(elem,ib,kick) qemsetsteerer(elem,kick,0),orbit0);
[rv2h0,rv0]=qemresp(mach,qemres.ct,qemres.bpmidx,qemres.steervidx(qemres.vlist),...
    @(elem,ib,kick) qemsetsteerer(elem,0,kick),orbit0);
[bid,rv2]= qemresp(mach,qemres.ct,qemres.bpmidx(bok),qemres.steerhidx(qemres.hlist),...
    @(elem,ib,kick) qemsetsteerer(elem,0,kick),orbit0(:,bok));
[rh2,bid]= qemresp(mach,qemres.ct,qemres.bpmidx(bok),qemres.steervidx(qemres.vlist),...
    @(elem,ib,kick) qemsetsteerer(elem,kick,0),orbit0(:,bok));

% Get new gains
% [khgain,bhgain]=qempanelgain(qemres.hlist,resph,rh,ones(size(qemres.khgain)),...
%     qemres.bhscale*ones(size(qemres.bhgain)));
% [kvgain,bvgain]=qempanelgain(qemres.vlist,respv,rv,qemres.kvgain,...
%     ones(size(qemres.bvgain)));
[khg,bhgain]=qempanelgain(resph,rh0,khg,qemres.bhscale*bhgain);
[kvg,bvgain]=qempanelgain(respv,rv0,kvg,bvgain);

for loop=2:5
    [rhx,rh2vx,rv2hx,rvx]=qemcode(mach,qemres.ct,...
        qemres.steerhidx(qemres.hlist),qemres.steerhidx(qemres.hlist),qemres.bpmidx(bok),...
        khr(end,:),khg,kvr(end,:),kvg,[],[],[],orbit0(:,bok));
    
    [resphx,resph2vx]=qembpmdecode(resph,resph2v,bhr(:,loop-1),bhgain,bvgain);
    [respv2hx,respvx]=qembpmdecode(respv2h,respv,bvr(:,loop-1),bhgain,bvgain); 
    
    % Get new BPM rotations
    %     [rh,rh2v]=qembpmcode(rhx,rh2vx,bhr(:,end),bhgain,bvgain);
    %     [rv2h,rv]=qembpmcode(rv2hx,rvx,bvr(:,end),bhgain,bvgain);
    bhr(bok,loop)=bhr(bok,loop-1)-qemfitkbrot(rhx,resph2vx(bok,:)-rh2vx,2);   % vertical response
    bvr(bok,loop)=bvr(bok,loop-1)+qemfitkbrot(rvx,respv2hx(bok,:)-rv2hx,2);   % horizontal response
    %    brot=0.5*(bhrot+bvrot);
    
    % Get new steerer rotations
    khr(loop,:)=khr(loop-1,:)+qemfitkbrot(rv2,resph2vx(bok,:)-rh2vx,1);
    kvr(loop,:)=kvr(loop-1,:)-qemfitkbrot(rh2,respv2hx(bok,:)-rv2hx,1);
end
khrot(qemres.hlist)=khr(end,:);
khgain(qemres.hlist)=khg;
kvrot(qemres.vlist)=kvr(end,:);
kvgain(qemres.vlist)=kvg;
bhrot=bhr(:,end);
bvrot=bvr(:,end);
end

