function [ qemres ] = qemACresponse( qemres )
%QEMACRESPONSE Summary of this function goes here
%   Detailed explanation goes here

clear ACres*  DCres*

iskew=0;

%========= DC ORMs
%datadir='/machfs/MDT/2017/Dec07/resp1';
DCdatadir='/machfs/MDT/2018/Feb07/resp1';
[DCresph,curh,algoh]=sr.load_resp(DCdatadir,'h',qemres.hlist);
[DCrespv,curv,algov]=sr.load_resp(DCdatadir,'v',qemres.vlist);
[DCresph2v]=sr.load_resp(DCdatadir,'h2v',qemres.hlist);
[DCrespv2h]=sr.load_resp(DCdatadir,'v2h',qemres.vlist);

% remove NAN (broken BPMs)
indx=~isnan(DCrespv(:,1));


%======== AC ORM 
%load /users/diag/matlab/libera_data/TimeLabel/TimeLabelMresX07-Feb-2018_14h40.mat 
%load /users/diag/matlab/libera_data/TimeLabel/TimeLabelMresZ07-Feb-2018_14h53.mat 
load Mres_H_Xamp_8000_188mA.mat
load Mres_V_Zamp_1000_188mA.mat
ACresph=MresH(:,qemres.hlist); 
ACrespv=MresV(:,qemres.hlist); 
ACresphv=MresVH(:,qemres.hlist); 
ACrespvh=MresHV(:,qemres.hlist);
ACresph_norm=-ACresph/2.8;  
ACrespvh_norm=-ACrespvh/3.8;
ACrespv_norm=-ACrespv/3.8;  
ACresphv_norm=-ACresphv/2.8;

% compute ORM difference before using calibration factors
rms_diff_before=[std2(std2(ACresph_norm(indx,:)-DCresph(indx,:))) ...
    std2(std2(ACrespv_norm(indx,:)-DCrespv(indx,:)))]

% compute new calibration factors
GainX=std2(ACresph_norm(indx,:))./std2(DCresph(indx,:));
GainZ=std2(ACrespv_norm(indx,:))./std2(DCrespv(indx,:));
save Gain_new GainX GainZ

% apply new calibration factors
ACresph_norm = bsxfun(@rdivide, ACresph_norm, GainX);
ACrespv_norm = bsxfun(@rdivide, ACrespv_norm, GainZ);

% compute ORM difference after using calibration factors
rms_diff_after=[std2(std2(ACresph_norm(indx,:)-DCresph(indx,:))) ...
    std2(std2(ACrespv_norm(indx,:)-DCrespv(indx,:)))]

% ================ off-diagonal blocks ====================================

% compute ORM difference before using calibration factors
rms_diff_before=[std2(std2(ACresphv_norm(indx,:)-DCresph2v(indx,:))) ...
    std2(std2(ACrespvh_norm(indx,:)-DCrespv2h(indx,:)))]

% apply new calibration factors
ACresphv_norm = bsxfun(@rdivide, ACresphv_norm, GainX);
ACrespvh_norm = bsxfun(@rdivide, ACrespvh_norm, GainZ);

% compute ORM difference after using calibration factors
rms_diff_after=[std2(std2(ACresphv_norm(indx,:)-DCresph2v(indx,:))) ...
    std2(std2(ACrespvh_norm(indx,:)-DCrespv2h(indx,:)))]



%% save for qempanel
fileformat=['%.4f\t%s,%d' repmat('\t%.5f',1,224)];
deltaH=0.08; % from respmat.py
deltaV=0.10; % from respmat.py

acdir = [qemres.datadir '_AC'];

mkdir(acdir);

for ist=1:length(qemres.hlist)
    f=fopen([acdir '/steerH' num2str(qemres.hlist(ist),'%.2d')],'w');
    fprintf(f,fileformat,deltaH,sr.steername(qemres.hlist(ist),'SR/ST-H%i/C%i/CURRENT'),0,ACresph_norm(:,ist)*1e3);
    fclose('all');
    f=fopen([acdir '/steerV' num2str(qemres.hlist(ist),'%.2d')],'w');
    fprintf(f,fileformat,deltaV,sr.steername(qemres.hlist(ist),'SR/ST-V%i/C%i/CURRENT'),0,ACrespv_norm(:,ist)*1e3);
    fclose('all');
    
    % off diagonal
    f=fopen([acdir '/steerH2V' num2str(qemres.hlist(ist),'%.2d')],'w');
    fprintf(f,fileformat,deltaH,sr.steername(qemres.hlist(ist),'SR/ST-H%i/C%i/CURRENT'),0,ACresphv_norm(:,ist)*1e3);
    fclose('all');
    f=fopen([acdir '/steerV2H' num2str(qemres.hlist(ist),'%.2d')],'w');
    fprintf(f,fileformat,deltaV,sr.steername(qemres.hlist(ist),'SR/ST-V%i/C%i/CURRENT'),0,ACrespvh_norm(:,ist)*1e3);
    fclose('all');
end

% copy missing files from DC measurement to resp1_AC
filename={...
    'steerF2H00','steerF2V00',...
    'iax','iaxemittance.mat','iaxH2V.mat','iaxV.mat',...
    'settings.mat'};
for ifile = 1:length(filename)
    copyfile(fullfile(DCdatadir,filename{ifile}),fullfile(acdir,filename{ifile}));
end

% copy files created in selected folder to _AC folder
filename={...
    'srmag.dat',...
    'magcor.dat',...
    'skewcor.dat',...
    'sextcor.dat',...
    'optics'};
for ifile = 1:length(filename)
    try
        copyfile(fullfile(qemres.datadir,filename{ifile}),fullfile(acdir,filename{ifile}));
    catch err
        disp(err)
    end
end
%% set qemres datadir to AC directory
qemres.datadir = acdir;

end

