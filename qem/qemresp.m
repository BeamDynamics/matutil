function [rh,rv] = qemresp(mach,dct,bidx,sidx,elemfunc,varargin)
%QEMRESP Builds the model response matrix
%

nb=length(bidx);
nq=length(sidx);
vargs={[],[]};
vargs(1:length(varargin))=varargin;
orbit0=vargs{1};
kick=0.000025*ones(1,nq);       % default kick: 25 urad
if isempty(orbit0), orbit0=findsyncorbit(mach,dct,bidx); end
if ~isempty(vargs{2}), kick(:)=vargs{2}; end
rh=zeros(nb,nq);
rv=zeros(nb,nq);
for iq=1:nq
    idq=sidx(iq);
    esave=mach{idq};
    mach{idq}=elemfunc(esave,iq,kick(iq));
    orbit=(findsyncorbit(mach,dct,bidx)-orbit0)/kick(iq);
    mach{idq}=esave;
    rh(:,iq)=orbit(1,:)';
    rv(:,iq)=orbit(3,:)';
end
end
