function [resph,respv,fresph,frespv]=qemcheckresp(qemres,semres,wrongbpms,datadir,opticsdir)
%QEMCHECKRESP	Displays information on measured response matrices
%
%[RESPH,RESPV,FRESPH,FRESPV]=QEMCHECKRESP(QEMRES,BADBPMS,DATADIR,OPTICSDIR)
%
% QEMRES:	global parameters
% BADBPMS:	BPM to remove from data
% DATADIR:	Directory containing the response matrices (default: qemres.datadir)
% OPTICSDIR:Directory containing the model optics (default: datadir/optics)
%
%RESPH,RESPV : normalized response matrices in m
%FRESPH,FRESPV: frequency response (m/(df/f))

if nargin < 4
   datadir=qemres.datadir;
   opticsdir=qemres.opticsdir;
elseif nargin < 5
   opticsdir=fullfile(datadir,'optics');
end

resph=sr.load_normresp(datadir,opticsdir,'h',qemres.hlist);   % Load response
respv=sr.load_normresp(datadir,opticsdir,'v',qemres.vlist);

f0=992*2.997924E8/findspos(qemres.at(:),length(qemres.at(:))+1);
[oh,ov]=sr.load_fresp(qemres.datadir);
fresph=f0*oh;
frespv=f0*ov;

resph(wrongbpms,:)=NaN;
respv(wrongbpms,:)=NaN;
fresph(wrongbpms,:)=NaN;
frespv(wrongbpms,:)=NaN;
end
