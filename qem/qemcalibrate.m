function [khrot,khgain,kvrot,kvgain,bhrot,bhgain,bvrot,bvgain]=qemcalibrate(dirlist)
%QEMCALIBRATE		update BPM and steerer gain calibration
%
%QEMCALIBRATE(DIRLIST)

global MACHFS

[bhgain,bhrot,bvgain,bvrot]=qembpmstat(dirlist);
[khgain,khrot,kvgain,kvrot]=qemsteerstat(dirlist);

initname=fullfile(MACHFS,'MDT','respmat','');
q1=load(fullfile(initname,'qeminit.mat'));
if ~isfield(q1,'khrot'), q1.khrot=zeros(1,96); end
if ~isfield(q1,'kvrot'), q1.kvrot=zeros(1,96); end
if isfield(q1,'bhscale'), q1=rmfield(q1,'bhscale'); end
if isfield(q1,'brot')
    q1.bhrot=q1.brot;
    q1.bvrot=q1.brot;
    q1=rmfield(q1,'brot') %#ok<NOPRT>
end
if isfield(q1,'krot')
    q1.khrot=q1.krot;
    q1.kvrot=q1.krot;
    q1=rmfield(q1,'krot') %#ok<NOPRT>
end
qr=q1;
qr.bhrot=mean(bhrot,2);
qr.bhgain=mean(bhgain,2);
qr.bvrot=mean(bvrot,2);
qr.bvgain=mean(bvgain,2);
qr.khrot=mean(khrot,1);
qr.khgain=mean(khgain,1);
qr.kvrot=mean(kvrot,1);
qr.kvgain=mean(kvgain,1);

figure(1)
subplot(2,2,1);
bar([q1.bhrot(1:20) qr.bhrot(1:20)]);
title('H BPM rotation');
% hold on
% bar(bhr(1:20,:));
% hold off

subplot(2,2,2);
bar([q1.bhgain(1:20) qr.bhgain(1:20)]-1);
title('H BPM gain');
% hold on
% bar(bhg(1:20,:)-1);
% hold off

subplot(2,2,3);
bar([q1.bvrot(1:20) qr.bvrot(1:20)]);
title('V BPM rotation');
% hold on
% bar(bvr(1:20,:));
% hold off

subplot(2,2,4);
bar([q1.bvgain(1:20) qr.bvgain(1:20)]-1);
title('V BPM gain');
% hold on
% bar(bvg(1:20,:)-1);
% hold off
% 

figure(2)
subplot(2,2,1);
bar([q1.khrot(1:6:96);qr.khrot(1:6:96)]');
title('H steerer rotation');
% hold on
% bar(khr(:,1:6:96)');
% hold off

subplot(2,2,2);
bar([q1.khgain(1:6:96);qr.khgain(1:6:96)]'-1);
title('H steerer gain');
% hold on
% bar(khg(:,1:6:96)'-1);
% hold off

subplot(2,2,3);
bar([q1.kvrot(1:6:96);qr.kvrot(1:6:96)]');
title('V steerer rotation');
% hold on
% bar(kvr(:,1:6:96)');
% hold off

subplot(2,2,4);
bar([q1.kvgain(1:6:96);qr.kvgain(1:6:96)]'-1);
title('V steerer gain');
% hold on
% bar(kvg(:,1:6:96)'-1);
% hold off

save(fullfile(initname,'qeminitnew.mat'),'-struct','qr');
