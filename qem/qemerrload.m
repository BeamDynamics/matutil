function [kn,dipdelta,dxs]=qemerrload(qemres,filename)
%[KN,DIPDELTA,DXS]=QEMERRLOAD(QEMRES,FILENAME)		reads a set of quadrupole strengths
%
% FILENAME : file name (default : [APPHOME '/qem/race_quadco.dat'])
%
% kn:       quadrupole strengths [m^-2]
% dipdelta: dipole field error
% dxs:      horizontal sextupole displacement

global APPHOME
if nargin < 2
    filename=fullfile(APPHOME,'qem','race_quadco.dat');
end

dipdelta=zeros(length(qemres.dipidx),1);
dxs=zeros(length(qemres.sextidx),1);
[p,n,ext]=fileparts(filename); %#ok<ASGLU>
switch ext
    case '.dat'
        %   File format:
        %      Header: ?       nb.errors
        %      Line:   name	s[m]        K[m^-2]     deltaK [m^-2]   deltaK/K
        %                               -------
        %	Warning : the sign convention for quad strength in "qem" is the opposite of
        %	BETA convention.
        [fq,errmess]=fopen(filename,'rt');
        if fq==-1
            error('MATLAB:load:couldNotReadFile',...
                'Unable to read file %s: %s.',filename,errmess);
        end
        ln=fgetl(fq);
        lg=sscanf(ln,'%*i%i');
        q=NaN(lg,1);
        for l=1:lg
            ln=fgetl(fq);
            q(l)=sscanf(ln,'%*s%*lf%lf%*lf%*lf');
        end
        kn=-q(qemres.qpfit);
    case '.mat'
        s=load(filename);
        kn=s.k;
        dipdelta=s.dipdelta;
        if isfield(s,'dxs'), dxs=s.dxs; end
end
