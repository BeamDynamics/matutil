function qemb = qemretune(qemb,qemres,tunes)
%QEMB=QEMRETUNE(QEMB,QEMRES) Retune

[k,tilt]=semtilt(qemb.kn,qemb.ks); %#ok<ASGLU>
mach=atfittune(qemb.at,tunes,'QD6','QF7');
knew=getcellstruct(mach,'PolynomB',qemres.qpidx,2);
qemb.kn=knew.*cos(2*tilt);
qemb.ks=-knew.*sin(2*tilt);
end
