function qemb = qemoptics(qemres,qemb,qemb0)
%QEMOPTICS Compute optical parameters for the fitted machine

nbpm=length(qemres.bpmidx);
[idx,j,kdx]=unique([qemres.bpmidx qemres.d9idx qemres.d11lensidx qemres.id25idx]); %#ok<ASGLU>

orbit0=findsyncorbit(qemb.at,qemres.ct,qemres.bpmidx);
dpp=orbit0(5,1);

[lindata,qemb.pm]=atx(qemb.at,dpp,idx);
lindata=lindata(kdx);

qemb.lindata=lindata(1:nbpm);       % BPM results
qemb.d9data=lindata(nbpm+1);        % D9 results
qemb.d11lensdata=lindata(nbpm+2);	% D11 results
qemb.id25data=lindata(nbpm+3);      % ID25 results
dispersion=cat(2,qemb.lindata.Dispersion)';
qemb.tunes=qemb.pm.fulltunes;
qemb.beta=cat(1,qemb.lindata.beta);
qemb.eta=dispersion(:,[1 3]);
% qemb.emittances=[cat(1,lindata.modemit) cat(1,lindata.emit44)];
modemit=cat(1,lindata.modemit);
qemb.emittances=[modemit(:,1:2) cat(1,lindata.emit44)];

[qemb.atresph,qemb.atresph2v,qemb.atrespv2h,qemb.atrespv,...
    qemb.frespx,qemb.frespz]=qemcode(qemb.at,qemres.ct,...
    qemres.steerhidx(qemres.hlist),qemres.steervidx(qemres.vlist),qemres.bpmidx,...
    qemres.khrot(qemres.hlist),qemres.khgain(qemres.hlist),...
    qemres.kvrot(qemres.hlist),qemres.kvgain(qemres.hlist),...
    qemres.brot,qemres.bhgain,qemres.bvgain,orbit0);

if nargin >= 3
    lindata0=[qemb0.lindata qemb0.d9data qemb0.d11lensdata qemb0.id25data];
    beta0=cat(1,lindata0.beta);
    beam66=cat(3,lindata.beam66);
    sigma2=[squeeze(beam66(1,1,:)) squeeze(beam66(3,3,:))];
    qemb.emittances=[qemb.emittances sigma2./beta0];
end

end
