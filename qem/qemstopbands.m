function stpb=qemstopbands(qemres,qemb,qemb0)

[idx,ik]=sort([qemres.qpidx qemres.qcoridx]);
[lind,avb,avm]=atavedata(qemb0.at,0,idx); %#ok<ASGLU>
beta(ik,:)=avb;
phas(ik,:)=avm./qemb0.tunes(ones(size(avm,1),1),:);
kl=[(qemb.kn-qemb0.kn).*qemres.ql;qemb.cor(:,end)];

stpb.h73=gline(73,kl,beta(:,1),phas(:,1));
stpb.v29=gline(29,kl,beta(:,2),phas(:,2));
stpb.h72=gline(72,kl,beta(:,1),phas(:,1));
stpb.v28=gline(28,kl,beta(:,2),phas(:,2));

    function vv=gline(harm,kl,beta,phas)
        vr=sum(kl.*beta.*cos(harm*phas));
        vi=sum(kl.*beta.*sin(harm*phas));
        vv=sqrt(vr.*vr+vi.*vi)/2/pi;
    end

end
