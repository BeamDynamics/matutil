function [rh,rh2v,rv2h,rv,frespx,frespz]=qemsimul(dirname,mach,qemres,hlist,vlist)
%QEMSIMUL Generate simulated response matrices
%
% QEMSIMUL(DIRNAME,MACH,QEMRES)
%   Generate response matrices from MACH, using errors from
%   QEMRES.KHROT, QEMRES.KHGAIN
%   QEMRES.KVROT, QEMRES.KVGAIN
%   QEMRES.BROT, QEMRES.BHGAIN, QEMRES.BVGAIN

if nargin<5, vlist=qemres.vlist; end
if nargin<4, hlist=qemres.hlist; end
orbit0=findsyncorbit(mach,qemres.ct,qemres.bpmidx);

curh=0.08*ones(size(qemres.steerhidx));
curv=0.10*ones(size(qemres.steervidx));
deltaf=160;
frf=992*2.997924e8/findspos(mach,length(mach)+1);
[kh,kv]=load_corcalib(qemres.opticsdir,'h','v');
hkick=repmat(kh,1,16).*curh;
vkick=repmat(kv,1,16).*curv;
dff=deltaf/frf;

[rh,rh2v,rv2h,rv,frespx,frespz]=qemcode(mach,qemres.ct,...
    qemres.steerhidx(hlist),qemres.steervidx(vlist),qemres.bpmidx,...
    qemres.khrot(hlist),qemres.khgain(hlist),...
    qemres.kvrot(vlist),qemres.kvgain(vlist),...
    qemres.brot,qemres.bhgain,qemres.bvgain,orbit0,dff,...
    hkick(hlist),vkick(vlist));

amplh=hkick(ones(224,1),hlist);
amplv=vkick(ones(224,1),vlist);

[resph,resph2v,respv2h,respv]=deal(NaN(224,96));
resph(:,hlist)=rh.*amplh;
resph2v(:,hlist)=rh2v.*amplh;
respv2h(:,vlist)=rv2h.*amplv;
respv(:,vlist)=rv.*amplv;

sr.save_resp(dirname,curh,resph,'h',hlist);
sr.save_resp(dirname,curh,resph2v,'h2v',hlist);
sr.save_resp(dirname,curv,respv2h,'v2h',vlist);
sr.save_resp(dirname,curv,respv,'v',vlist);

encodef=@(idx) 'tango:SY/MS/1/FREQUENCY';
sr.save_steerresp(fullfile(dirname,'steerF2H00'),0,deltaf,frespx*dff,encodef);
sr.save_steerresp(fullfile(dirname,'steerF2V00'),0,deltaf,frespz*dff,encodef);
end
