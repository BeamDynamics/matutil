function dirname=qemlinkdir(dirname,measname)

global MACHFS

measbase=fullfile(MACHFS,'MDT');
if nargin < 1
    dirname=uigetdir(fullfile(measbase,datestr(now(),'yyyy'),''));
    if ~ischar(dirname), error('qem:nodir','No directory selected'); end
else
    if ~isdir(dirname), error('qem:nodir','Wrong directory'); end
end

if nargin <2
%   system(['cd ' dirname ' && echo inputdlg > bid.txt']);
    pause(0.2);		% to avoid crash on alcoholic stations
    measname=inputdlg({'Name of the measurement:'},'',1,{[datestr(now(),'mmmdd') '_']});
%   system(['cd ' dirname ' && echo done >> bid.txt']);
end

if ~isempty(measname)
    linkname=measname{1};
    fromname=dirname;
    prefix=strfind(dirname,measbase);
    if ~isempty(prefix), fromname=fullfile('..','..',dirname(prefix(1)+length(measbase)+1:end)); end
    workdir=fullfile(measbase,'respmat');
    [status,result]=system(['cd ' fullfile(workdir,'quad') ' && ln -s ' fromname ' ' linkname]); %#ok<*NASGU>
    [status,result]=system(['cd ' fullfile(workdir,'skew') ' && ln -s ' fromname ' ' linkname]);
%   [status,result]=system(['cd ' workdir ' && rm betasymm && ln -s ' fullfile('quad',linkname) ' betasymm']);
%   [status,result]=system(['cd ' workdir ' && rm skewresp && ln -s ' fullfile('skew',linkname) ' skewresp']);
end
