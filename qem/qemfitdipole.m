function [dipdelta,dipresponse] = qemfitdipole(mach,qemres,semres,okfit,okbpm)
%QEMFITDIPOLE Fits dipole beanding angle to match the measured dispersion
%[DIPDELTA,DIPRESPONSE]=QEMFITDIPOLE(MACH,QEMRES,SEMRES,OKBPM)
%
%QEMRES:    global parameters (dipidx,bpmidx,brot,bhgain,bvgain,bhscale,frespx)
%SEMRES:    global parameters (frespz)
%OKBPM:     Select a subset of valid BPMS (default:all)

if nargin<5, okbpm=[]; end
if nargin<4, okfit=[]; end
if isempty(okbpm), okbpm=true(length(qemres.bpmidx),1); end
if isempty(okfit), okfit=true(size(qemres.dipidx)); end
[frh,frv]=qembpmdecode(qemres.frespx,semres.frespz,...
    qemres.brot,qemres.bhgain,qemres.bvgain);  %#ok<ASGLU>
[frh0,frv0]=qemfresp(mach,qemres.ct,qemres.bpmidx,[],[],[]);  %#ok<ASGLU>
dfrh=qemres.bhscale*frh-frh0;
[dipresponse,~]=qemdispderiv(mach,qemres.ct,@setbendangle,1.e-3,...
    qemres.dipidx,qemres.bpmidx);
bok=isfinite(dfrh)&okbpm;
dipdelta=dipresponse(bok,okfit)\dfrh(bok);

    function elt=setbendangle(elt,dval)
       ba=elt.BendingAngle;
       elt.BendingAngle=(1+dval)*ba;
    end

end
