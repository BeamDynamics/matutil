function [rh,rv] = qemsteercode(mach,dct,krot,kgain,sidx,bpmidx,varargin)
%QEMSTEERCODE	Apply Steerer gain and rotation to generate response matrices

chk=cos(krot)./kgain;
shk=sin(krot)./kgain;
[rh,rv]=qemresp(mach,dct,bpmidx,sidx,...
    @(elem,ib,kick) qemsetsteerer(elem, kick*chk(ib),kick*shk(ib)),...
    varargin{:});
end
