function [rhbpm,rvbpm] = qembpmcode(rh0,rv0,brot,bhgain,bvgain)
%QEMBPMCODE     Apply BPM gain and rotation to response matrices
%[RHBPM,RVBPM]=QEMBPMCODE(RH0,RV0,ROTATION,HGAIN,VGAIN)

nk=size(rh0,2);
cb=cos(brot);
sb=sin(brot);
rhbpm= rh0.*repmat(cb./bhgain,1,nk) + rv0.*repmat(sb./bhgain,1,nk);
rvbpm=-rh0.*repmat(sb./bvgain,1,nk) + rv0.*repmat(cb./bvgain,1,nk);
end
