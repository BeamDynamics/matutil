function [rh,rh2v,rv2h,rv]=qemsteerdecode(mach,dct,qemres,resph,resph2v,respv2h,respv,khrot,khgain,kvrot,kvgain,varargin)
%QEMSTEERDECODE	Correct response matrices for steerer gain and rotation
%
%[RH,RH2V,RV2H,RV]=QEMSTEERDECODE(MACH,QEMRES,RHM,RH2VM,RV2HM,RVM,...
%   KHROT,KHGAIN,KVROT,KVGAIN)

nbpm=size(resph,1);
chk=cos(khrot);
shk=sin(khrot);
cvk=cos(kvrot);
svk=sin(kvrot);
ck=cos(khrot-kvrot);

xh=false(size(qemres.steerhidx));
xh(qemres.hlist)=true;
xv=false(size(qemres.steervidx));
xv(qemres.vlist)=true;
both=xh&xv;
honly=xh&(~xv);
vonly=xv&(~xh);
% both=false(1,96);
% honly=xh;
% vonly=xv;

vok=both(qemres.hlist);

if sum(vok)>0
    rh(:,vok)=(repmat(cvk(both).*khgain(both),nbpm,1).*resph(:,vok)-...
        repmat(shk(both).*kvgain(both),nbpm,1).*respv2h(:,vok))./...
        repmat(ck(both),nbpm,1);
    rh2v(:,vok)=(repmat(cvk(both).*khgain(both),nbpm,1).*resph2v(:,vok)-...
        repmat(shk(both).*kvgain(both),nbpm,1).*respv(:,vok))./...
        repmat(ck(both),nbpm,1);
end

if sum(honly)>0
    [rv2hmodel,rvmodel]=qemresp(mach,dct,qemres.bpmidx,qemres.steerhidx(honly),...
        @(elem,ib,kick) qemsetsteerer(elem,0,kick),varargin{:});
    rh(:,~vok)=(repmat(khgain(honly),nbpm,1).*resph(:,~vok)-...
        repmat(shk(honly),nbpm,1).*rv2hmodel)./...
        repmat(chk(honly),nbpm,1);
    rh2v(:,~vok)=(repmat(khgain(honly),nbpm,1).*resph2v(:,~vok)-...
        repmat(shk(honly),nbpm,1).*rvmodel)./...
        repmat(chk(honly),nbpm,1);
end

hok=both(qemres.vlist);

if sum(hok)>0
    rv2h(:,hok)=(repmat(chk(both).*kvgain(both),nbpm,1).*respv2h(:,hok)+...
        repmat(svk(both).*khgain(both),nbpm,1).*resph(:,hok))./...
        repmat(ck(both),nbpm,1);
    rv(:,hok)=(repmat(chk(both).*kvgain(both),nbpm,1).*respv(:,hok)+...
        repmat(svk(both).*khgain(both),nbpm,1).*resph2v(:,hok))./...
        repmat(ck(both),nbpm,1);
end

if sum(vonly)>0
    [rhmodel,rh2vmodel]=qemresp(mach,dct,qemres.bpmidx,qemres.steervidx(vonly),...
        @(elem,ib,kick) qemsetsteerer(elem,kick,0),varargin{:});
    rv2h(:,~hok)=(repmat(kvgain(vonly),nbpm,1).*respv2h(:,~hok)+...
        repmat(svk(vonly),nbpm,1).*rhmodel)./...
        repmat(cvk(vonly),nbpm,1);
    rv(:,~hok)=(repmat(kvgain(vonly),nbpm,1).*respv(:,~hok)+...
        repmat(svk(vonly),nbpm,1).*rh2vmodel)./...
        repmat(cvk(vonly),nbpm,1);
end
end

