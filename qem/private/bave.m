function bave=modrn(b)
bb=mean(reshape(b,14,16),2);
bave=reshape(bb(:,ones(1,16)),224,1);
