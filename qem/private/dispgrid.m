function dispgrid(q,cor)
[bh,bv]=qemanal(q,cor);
bave=modrn(bh);
modulh=(bh-bave)./bave;
bave=modrn(bv);
modulv=(bv-bave)./bave;
disp([' modRN[HOR]= ' num2str(std(modulh,1)) ',  modRN[VER]= ' num2str(std(modulv,1))]);
