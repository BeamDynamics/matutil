function [cor,resp] = qemcorsext(qemres,mach,fns0)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

nsx=length(qemres.sextidx);
isx=srfold(1:224);
s16=isx(1,12:13);
s30=isx(1,26:27);
s24c23=isx(7,19:20);
s22c23=isx(6,19:20);
sxcoridx=qemres.sextidx([selcor(3);s16(:);s30(:);s24c23(:);s22c23(:)]);
[f0,f1,f2,f3]=qemsextrdtresp(mach,qemres.bpmidx,[qemres.sextidx sxcoridx]);
[qkl,shl]=load_srmag('textfile',fullfile(qemres.datadir,'srmag.dat'),20.15);
resp=[f0;f1;f2;f3];

fns=resp(:,1:nsx)*shl*2;
rhs=fns0-fns;

corresp=[resp(:,nsx+(1:18)) sum(resp(:,nsx+(19:20)),2)]; % Couple S22/C23

% mask=1:12;          % correctors only
mask=[1:8 9:19];	% everything (corrector S24/C23 removed)
% mask=13:19;         % independent sextupoles
% mask=17:19;         % C23
corresp=resp(:,nsx+mask);
cor=zeros(20,1);
cor(mask)=0.5*([real(corresp);imag(corresp)]\[real(rhs);imag(rhs)]);
cor(20)=cor(19);    % Uncouple S22/C23

rhs2=fns0-(fns+corresp*cor*2);
v1=[real(rhs);imag(rhs)];
v2=[real(rhs2);imag(rhs2)];
disp(sum(v1.*v1));
disp(sum(v2.*v2));
end

