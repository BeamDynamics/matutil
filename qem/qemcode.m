function varargout=qemcode(mach,dct,shidx,svidx,bpmidx,varargin)
%QEMCODE	Compute the steerer response of the model
%[RH,RH2V,RV2H,RV]=QEMCODE(MACH,SHIDX,SVIDX,BPMIDX,...
%   KHROT,KHGAIN,KVROT,KVGAIN,BROT,BHGAIN,BVGAIN,...
%   ORBIT0,DFF,HKICK,VKICK)
%
%[RH,RH2V,RV2H,RV,RFH,RFV]=QEMCODE(MACH,SHIDX,SVIDX,BPMIDX,...
%   KHROT,KHGAIN,KVROT,KVGAIN,BROT,BHGAIN,BVGAIN,...
%   ORBIT0,DFF,HKICK,VKICK)
%
vargs=cell(1,11);
vargs(1:length(varargin))=varargin;
[khrot,khgain,kvrot,kvgain]=deal(vargs{1:4});
if isempty(khrot), khrot=zeros(1,length(shidx)); end
if isempty(khgain), khgain=ones(1,length(shidx)); end
if isempty(kvrot), kvrot=zeros(1,length(svidx)); end
if isempty(kvgain), kvgain=ones(1,length(svidx)); end
if isempty(vargs{5}), vargs{5}=zeros(length(bpmidx),1); end     % brot
if isempty(vargs{6}), vargs{6}=ones(length(bpmidx),1); end      % bhgain
if isempty(vargs{7}), vargs{7}=ones(length(bpmidx),1); end      % bvgain
if isempty(vargs{8}), vargs{8}=findsyncorbit(mach,dct,bpmidx); end

[rh,rh2v]=qemsteercode(mach,dct,khrot,khgain,shidx,bpmidx,vargs{[8 10]});
[rv2h,rv]=qemsteercode(mach,dct,kvrot+0.5*pi,kvgain,svidx,bpmidx,vargs{[8 11]});

[rh,rh2v]=qembpmcode(rh,rh2v,vargs{5:7});
[rv2h,rv]=qembpmcode(rv2h,rv,vargs{5:7});
if nargout>=5
    [frh,frv]=qemfresp(mach,dct,bpmidx,vargs{5:9});
    varargout={rh,rh2v,rv2h,rv,frh,frv};
else
    varargout={rh,rh2v,rv2h,rv};
end
end
