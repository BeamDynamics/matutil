function [dvname,dvcurrent]=qemapplyid23(qemres,qemb)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

fn=fullfile(qemres.datadir,'srmag.dat');
[fp,message]=fopen(fn,'rt');
if fp < 0
    error('File:Open','%s: %s',message,fn);
else
    data=textscan(fp,'%s%f');
    fclose(fp);
end
[dvname,dvcurrent]=deal(data{:});

ratio=qemb(1).kn./qemb(2).kn;
fn=fullfile(qemres.datadir,'srmagnew.dat');
[fp,message]=fopen(fn,'wt');
if fp < 0
    error('File:Open','%s: %s',message,fn);
else
    reprint(fp,'tango:SR/PS-QF5/C22/CURRENT',ratio(149));
    reprint(fp,'tango:SR/PS-QD4/C22/CURRENT',ratio(150));
    reprint(fp,'tango:SR/PS-QD6HG/C22/CURRENT',ratio(151));
    reprint(fp,'tango:SR/PS-QF7HG/C22/CURRENT',ratio(152));
    reprint(fp,'tango:SR/PS-QF7HG/C23/CURRENT',ratio(153));
    reprint(fp,'tango:SR/PS-QD6HG/C23/CURRENT',ratio(154));
    reprint(fp,'tango:SR/PS-QD4/C23/CURRENT',ratio(155));
    reprint(fp,'tango:SR/PS-QF5/C23/CURRENT',ratio(156));
    fclose(fp);
end

    function reprint(fp,devname,scaling)
        oldval=dvcurrent(strcmpi(devname,dvname));
        newval=gl2i(devname,scaling*i2gl(devname,oldval));
        if isscalar(newval)
            fprintf(fp,'%s\t%g\n',devname, newval);
        end
    end
end

