function qemb0 = qemsymmetrize(qemb0,qemb)
%QEMB0=QEMSYMMETRIZE(QEMB0,QEMB)

famlist={'QF2','QD3','QD4','QF5','QD6','QF7'};
for i=1:length(famlist)
   idx=findcells(qemb0.at,'FamName',famlist{i});
   vmean=mean(getcellstruct(qemb.at,'PolynomB',idx,2));
   qemb0.at=setcellstruct(qemb0.at,'K',idx,vmean);
   qemb0.at=setcellstruct(qemb0.at,'PolynomB',idx,vmean,2);
end
end
