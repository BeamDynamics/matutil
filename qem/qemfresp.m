function [fresph,frespv] = qemfresp(mach,dct,bpmidx,varargin)
%QEMFRESP Computes the frequency response of the model
%[FRESPH,FRESPV]=QEMFRESP(MACH,BPMIDX,BROT,BHGAIN,BVGAIN,ORBIT0,DFF)

vargs=cell(1,5);
vargs(1:length(varargin))=varargin;
[brot,bhgain,bvgain,orbit0,dff]=deal(vargs{1:5});
if isempty(brot), brot=zeros(length(bpmidx),1); end
if isempty(bhgain), bhgain=ones(length(bpmidx),1); end
if isempty(bvgain), bvgain=ones(length(bpmidx),1); end
if isempty(orbit0), orbit0=findsyncorbit(mach,dct,bpmidx); end
if isempty(dff), dff=5e-7; end
circ=findspos(mach,length(mach)+1);
d2ct=-circ*dff;
%orbitf=(findsyncorbit(mach,dct+d2ct,bpmidx)-orbit0)/dff;
orbitf=(findsyncorbit(mach,dct+d2ct/2,bpmidx)-...
        findsyncorbit(mach,dct-d2ct/2,bpmidx))/dff;

[fresph,frespv]=qembpmcode(orbitf(1,:)',orbitf(3,:)',brot,bhgain,bvgain);
end
