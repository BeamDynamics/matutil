function [cor,df] = qemsextrdtcor(resp,rhs,chresp)
%QEMSEXTRDTCOR Compute sextupole RDT correction
%
%[COR,RESIDUAL]=QEMSEXTRDTCOR(RESPONSE,RHS)

corid=selcor(11);
absrhs=abs(rhs);
residual=sqrt(sum(absrhs.*absrhs)/length(rhs))
if nargin>=3
    fullresp=[resp;0.5*chresp];
    fullrhs=[rhs;0;0];
else
    fullresp=resp;
    fullrhs=rhs;
end
                                                % Solve
%cresp=resp(:,corid);
cmode=0;
imode=3;
switch cmode
    case 0          % No correctors
        res=zeros(1,20);
        cresp=zeros(size(fullresp,1),0);
    case 1          % All correctors
        res=[1:12 zeros(1,8)];
        cresp=fullresp(:,corid(1:12));
    case 2          % S24/C23 removed
        res=[1:8 0 9:11 zeros(1,8)];
        cresp=fullresp(:,corid([1:8 10:12]));
end
ideb=size(cresp,2);
switch imode
    case 1          % c22/c23 coupled
        res(13:20)=ideb+[1:7 7];
        cresp=[cresp fullresp(:,corid(13:18)) sum(fullresp(:,corid(19:20)),2)];
    case 2          % coupled by pairs
        res(13:20)=ideb+[1 1 2 2 3 3 4 4];
        cresp=[cresp fullresp(:,corid(13:2:20))+fullresp(:,corid(14:2:20))];
    case 3          % all C4 coupled
        res(13:20)=ideb+[1 1 1 1 2 2 3 3];
        cresp=[cresp sum(fullresp(:,corid(13:16)),2) sum(fullresp(:,corid(17:18)),2) sum(fullresp(:,corid(19:20)),2)];
end
val=-0.5*([real(cresp);imag(cresp)]\[real(fullrhs);imag(fullrhs)]);
cor=zeros(20,1);
cor(res>0)=val(res(res>0));
% mask=1:12;          % correctors only
% mask=[1:8 10:19];	% everything (corrector S24/C23 removed)
% mask=13:19;         % independent sextupoles
% mask=17:19;         % C23

% Evaluate result
df=rhs+resp(:,corid)*cor*2;
absrhs=abs(df);
residual=sqrt(sum(absrhs.*absrhs)/length(rhs))
end
