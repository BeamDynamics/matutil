function [dkk,stdf,meanf,stdf23,meanf23]=qcheck(k,k0,ax)
%QCHECK		Analyzes quadrupole strength errors
%
%DKK=QCHECK(KL,KL0)
%
[nq,nm]=size(k);
nfamq=nq/32;
if nfamq == 10
    labels={'QD1','QF2','QD3','QD4','QF5','QF5','QD4','QD6','QF7','QD8'};
else
    labels={'QF2','QD3','QD4','QF5','QF5','QD4','QD6','QF7'};
end
k0l=k0(:,ones(1,nm));
dkk=(k-k0l)./k0l;
stdf=NaN*ones(nfamq,nm);
meanf=NaN*ones(nfamq,nm);
stdf23=NaN*ones(4,nm);
meanf23=NaN*ones(4,nm);
for i=1:nm
    dkkf=sr.fold(dkk(:,i));
    dkkf23=dkkf(5:8,19:20);
    dkkf(5:8,19:20)=NaN;
    stdf(:,i)=std2(dkkf,1,2);
    meanf(:,i)=mean2(dkkf,2);
    stdf23(:,i)=std(dkkf23,1,2);
    meanf23(:,i)=mean(dkkf23,2);
end
stda=std(dkk,1,1);
fprintf('%s:\t% .3e\n','all',stda(1));
for i=1:nfamq
    fprintf('%s:\t% .3e\t% .3e\n',labels{i},stdf(i,1),meanf(i,1));
end

if nargin >= 3                 % displays quad. errors
    bar(ax,stdf);
    title(ax,'Gradient errors');
    ylabel(ax,'\DeltaK/K');
    grid(ax,'on');
    set(ax,'Xlim',[0 nfamq+1],'XTickLabel',labels);
end
