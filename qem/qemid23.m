function qerr = qemid23(dirname,nbvects)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

nfitq=3;
nfitd=2;
wrongbpms=[];
computeq=true;

[qemres,semres,qemb]=qempanelset(dirname);
[qemres.resph,qemres.respv,qemres.frespx,semres.frespz]=qemcheckresp(qemres,wrongbpms);
qemb(2)=qempaneldisp(qemres,qemb(2),qemb(1),struct('compare',true));
fprintf('\n\n------------------ end panelset ---------------------\n\n');

if computeq
    for i=1:nfitq
        qemb(2).kn=qempanelfit2(qemb(2).at,qemres,qemres.qpfit,okbpm);
        qemb(2).at=qemat(qemres,qemb(2),false); % update at model
        fprintf('\n\n------------------ end panelfit ---------------------\n\n');
    end
    qemb(2)=qempaneldisp(qemres,qemb(2),qemb(1),struct('compare',true));
    fprintf('\n\n------------------ end fit quads ---------------------\n\n');
    
    fresp=qemfresponse(qemb(2),qemres);
    for i=1:nfitd
        dipdelta = qemfitdipole(qemb(2).at(:),...
            qemres.bhscale*qemres.frespx-fresp(:,1),qemres.bpmidx,qemres.dipidx);
        qemb(2).dipdelta=qemb(2).dipdelta+dipdelta-mean(dipdelta);
        qemb(2).at=qemat(qemres,qemb(2),false); % update at model
        fresp=qemfresponse(qemb(2),qemres);
        qemres.bhscale=qembhscale(qemres.frespx,fresp(:,1));
        fprintf('\n\n------------------ end fitdipole ---------------------\n\n');
    end
    qemb(2)=qempaneldisp(qemres,qemb(2),qemb(1),struct('compare',true));
    fprintf('\n\n------------------ end fit dips ---------------------\n\n');
    
    if false
        s=struct('k',qemb(2).kn,'dipdelta',qemb(2).dipdelta);
        save(fullfile(qemres.datadir, 'quaderrors2.mat'),'-struct','s');
        [khgain,bhgain]=qempanelgain(qemres.steerlist,qemres.resph,qemb(2).atresph,qemres.khgain,qemres.bhscale*qemres.bhgain);
        [kvgain,bvgain]=qempanelgain(qemres.steerlist,qemres.respv,qemb(2).atrespv,qemres.kvgain,qemres.bvgain);
        save(fullfile(qemres.datadir, 'gainfit.mat'),'khgain','kvgain','bhgain','bvgain');
    end
else
    s=load(fullfile(qemres.datadir, 'quaderrors2.mat'));
    qemb(2).kn=s.k;
    qemb(2).dipdelta=s.dipdelta;
    qemb(2).at=qemat(qemres,qemb(2),false); % update at model
    fresp=qemfresponse(qemb(2),qemres);
    qemres.bhscale=qembhscale(qemres.frespx,fresp(:,1));
    qemb(2)=qempaneldisp(qemres,qemb(2),qemb(1),struct('compare',true));
    fprintf('\n\n------------------ end load_files--------------------\n\n');
end
qerr=(qemb(2).kn-qemb(1).kn)./qemb(1).kn;
end
