function qemmodulplot(ax,s,y,tit)
axes(ax);
plot(s,y);
set(gca,'XLim',[0 844.39]);
title([tit ' modulation']);
grid on
