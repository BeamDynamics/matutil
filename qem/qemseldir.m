function dirname=qemseldir(dirname)
%dirname=qemseldir(dirname)     Select and prepares data directory for QEM
%
%qemseldir checks the presence of the "optics" link and "magcor.dat"
%corrector data and prompts for values if necessary
%
% dirname:  data directory

global APPHOME MACHFS

measbase=fullfile(MACHFS,'MDT');
if nargin < 1
    dirname=uigetdir(fullfile(measbase,datestr(now(),'yyyy'),''),'Select the data directory:');
    if ~ischar(dirname), error('qem:nodir','No directory selected'); end
else
    if ~isdir(dirname), error('qem:nodir','Wrong directory'); end
end

if exist(fullfile(dirname,'optics'),'file') == 0        % Choose optics
    opticsbase=fullfile(APPHOME,'optics','');
    d=dir(opticsbase);
    ddir=find(cat(1,d.isdir));
    opticslist={d(ddir(3:end)).name};
    [selection,ok]=listdlg('ListString',opticslist,'SelectionMode','single');
    if ~ok, error('qem:nooptics','No optics selected'); end
    pth=getrealpath(fullfile(opticsbase,opticslist{selection}));
    [status,result]=system(['cd ' dirname ' && ln -s ' pth ' optics']);
    if status ~= 0, error('qem:wrongoptics',result); end
end

if exist(fullfile(dirname,'srmag.dat'),'file') == 0    % Select magnet file
%     srmagbase=fullfile(APPHOME,'sr','mag','settings2','*');
%     [fname,fpath]=uigetfile('*','Select magnet file:',srmagbase);
%     if ~ischar(fname), error('qem:nocor','No magnet file selected'); end
%     fullf=fullfile(fpath,fname);
%     base=strfind(fullf,'/settings2/')+11;
%     subpath=fullf(base(end):end);
%
%     [status,result]=system(['cd ' dirname ' && prfile srmag ' subpath ' > srmag.dat']);
%     if status ~= 0, error('qem:wrongmag',result); end
    sr.save_mag(fullfile(dirname,'srmag.dat'),sr.read_mag(sr.magname()));
end

if exist(fullfile(dirname,'magcor.dat'),'file') == 0    % Select correction file
%     magcorbase=fullfile(APPHOME,'sr','mag_cor','settings2','*');
%     [fname,fpath]=uigetfile('*','Select correction file:',magcorbase);
%     if ~ischar(fname), error('qem:nocor','No correction file selected'); end
%     fullf=fullfile(fpath,fname);
%     base=strfind(fullf,'/settings2/')+11;
%     subpath=fullf(base(end):end);

%     [status,result]=system(['cd ' dirname ' && prfile mag_cor ' subpath ' | grep QP-N > magcor.dat']);
%     if status ~= 0, error('qem:wrongcor',result); end
    save_correction(fullfile(dirname,'magcor.dat'),106,read_correction(106));

%     [status,result]=system(['cd ' dirname ' && prfile mag_cor ' subpath ' | grep QP-S > skewcor.dat']);
%     if status ~= 0, error('qem:wrongcor',result); end
    save_correction(fullfile(dirname,'skewcor.dat'),110,read_correction(110));

%     [status,result]=system(['cd ' dirname ' && prfile mag_cor ' subpath ' | grep SX-C > sextcor.dat']);
%     if status ~= 0, error('qem:wrongcor',result); end
    save_correction(fullfile(dirname,'sextcor.dat'),3,read_correction(3));
end


    function rlpth=getrealpath(pth)
        here=pwd;
        cd(pth);
        loc=pwd;
        beg=strfind(loc,'machfs');
        rlpth=loc([1 beg:end]);
        cd(here);
    end

end
