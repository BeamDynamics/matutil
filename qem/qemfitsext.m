function newhn=qemfitsext(qemb,qemres,semres,okfit,okbpm,varargin)
%QEMFITSEXT Vary sextupole strengths to fit the measured response matrix
%HN=QEMFITSEXT(QEMB,QEMRES,SEMRES,OKVAR,OKBPM,DISPFUNC)
%
%QEMB:
%QEMRES:    global parameters (qpidx,bpmidx,steerhidx,steervidx,hlist,vlist,
%           khrot,khgain,kvrot,kvgain,brot,bhgain,bvgain,
%           tunes,resph,respv,bhscale,frespx)
%SEMRES:    global parameters (resph,respv,frespz)
%OKVAR:     Select a subset of variable parameters (default: all)
%OKBPM:     Select a subset of valid BPMS (default:all)
%DISPFUNC:  Function to display the progress: DISPFUNC(I,IMAX)
%
%KN=QEMFITSEXT(QEMB,QEMRES,SEMRES,OKVAR,OKBPM,MODE,DISPFUNC)
%MODE:      Structure allowing control of SVD inversion. (default:)
%
%HN:        New sextupole strengths

    function elem=setx(elem,dk)
        strength=elem.PolynomB(2)+dk;
        elem.PolynomB(3)=strength;
    end

if nargin<5, okbpm=[]; end;
if nargin<4, okfit=[]; end;
narg=length(varargin);
if narg>0 && isa(varargin{narg},'function_handle')
    disparg={@(i,itot) nselect(2,varargin{narg},i,itot)};
    narg=narg-1;
else
    disparg={};
end
if narg>0 && isstruct(varargin{narg})
    mode=varargin{narg};
    narg=narg-1;
else
    mode=struct;
end
if isempty(okfit), okfit=true(size(qemres.sextidx)); end
shidx=qemres.steerhidx(qemres.hlist);
svidx=qemres.steervidx(qemres.vlist);
nhst=length(shidx);
nvst=length(svidx);

orbit0=findsyncorbit(qemb.at,qemres.ct,qemres.bpmidx);
dpp=orbit0(5);
dresp=qemderiv(qemb.at,dpp,@setx,0.05,qemres.sextidx(okfit),qemres.bpmidx,shidx,svidx,disparg{:});

[rh,rh2v,rv2h,rv,frh,frv]=qemdecode(qemb.at,qemres.ct,qemres,...
    qemres.resph,semres.respv,semres.resph,qemres.respv,...
    qemres.frespx,semres.frespz,...
    qemres.khrot,qemres.khgain,qemres.kvrot,qemres.kvgain,...
    qemres.brot,qemres.bhgain,qemres.bvgain); %#ok<ASGLU>
[rh0,rh2v0,rv2h0,rv0,frh0,frv0]=qemcode(qemb.at,qemres.ct,...
    qemres.steerhidx(qemres.hlist),qemres.steervidx(qemres.vlist),...
    qemres.bpmidx,[],[],[],[],[],[],[],orbit0); %#ok<ASGLU>
resp0=[rh0(:);rv0(:);frh0;qemb.pm.fractunes'];
resp=[rh(:);rv(:);qemres.bhscale*frh;qemres.fractunes'];

% if ~isfield(mode,'vnorm')
%     vv=sqrt([0.2;1.1;1.95;2;2;1.05;0.64;0.9]);
%     vv=sr.unfold(repmat(vv,1,32));
%     mode.vnorm=vv(okfit)';
% end
newhn=atgetfieldvalues(qemb.at(qemres.sextidx),'PolynomB',{3});
newhn(okfit)=newhn(okfit)+qemerrfit(nhst,nvst,resp-resp0,dresp,mode,okbpm);
end
