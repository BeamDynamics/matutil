function qemb=qempaneldisp(qemres,semres,qemb,qemb0,handles) %#ok<INUSL>

if nargin < 5, handles=struct(); end

if isfield(handles,'axes1')
    qcheck(qemb.kn,qemb0.kn,handles.axes1);
else
    qcheck(qemb.kn,qemb0.kn);
end

qemb.at=qemat(qemres,qemb,true);    % builds the at structure
qemb=qemoptics(qemres,qemb);        % stores at optical parameters

%avebeta=srbpmaverage(qemb.beta,'unfold');
%modrnh=bmodul(qemb.beta(:,1),avebeta(:,1));
modulh=bmodul(qemb.beta(:,1),qemb0.beta(:,1));
%modrnv=bmodul(qemb.beta(:,2),avebeta(:,2));
modulv=bmodul(qemb.beta(:,2),qemb0.beta(:,2));

stpb=qemstopbands(qemres,qemb,qemb0);
mess={...
    sprintf('tunes H/V: %.4f/%.4f',qemb.tunes);...
    sprintf('deltap/p: %.3e',qemres.dpp);...
%   [' modRN[HOR]= ' num2str(std(modrnh,1)) ',  modRN[VER]= ' num2str(std(modrnv,1))];...
    [' modul[HOR]= ' num2str(std(modulh,1)) ',  modul[VER]= ' num2str(std(modulv,1))];...
    ['   2*NuX = 73: ' num2str(stpb.h73)];...
    ['   2*NuZ = 29: ' num2str(stpb.v29)];...
    ['   2*NuX = 72: ' num2str(stpb.h72)];...
    ['   2*NuZ = 28: ' num2str(stpb.v28)]};

if all(isfield(handles,{'axes2','axes3'}))	% displays beta modulation
    sbpm=cat(1,qemb.lindata.SPos);
    qemmodulplot(handles.axes2, sbpm, modulh, '\beta_x');
    qemmodulplot(handles.axes3, sbpm, modulv, '\beta_z');
end

if (isfield(handles,'compare') && handles.compare) || ...
        (isfield(handles,'pushbutton13') && strcmpi(get(handles.pushbutton13,'Enable'),'Off'))
    if all(isfield(qemres,{'resph','respv','frespx'}))
        diffh=qemb.atresph-qemres.resph;    % Response H -> H in m/rad
        diffv=qemb.atrespv-qemres.respv;    % Response V -> V in m/rad
        diff1=[diffh diffv];
        residual=std2(diff1(:));
        hdispresidual=qemb.pm.alpha*std2(qemres.bhscale*qemres.frespx-qemb.frespx);
        fprintf('residual H = %g m/rad\n', std2(diffh(:)));
        fprintf('residual V = %g m/rad\n', std2(diffv(:)));
        mess=[mess;' ';['orbit residual = '  num2str(residual)];...
            ['H disp. residual = ' num2str(hdispresidual)];...
            [];...
            [num2str(length(qemres.wrongbpms)) ' bpms not used: ' num2str(qemres.wrongbpms,'%d ')]];
        qemplotresp(4,diffh,diffv,'deviation');
    end
end

if isfield(handles,'text3')                 % displays values
    set(handles.text3,'String',mess);
else
    fprintf('%s\n',mess{:});
end

if isfield(handles,'axes4')                 % displays corr. strengths
    bar(handles.axes4,qemb.cor);
    title(handles.axes4,'Corrector strengths');
    grid(handles.axes4,'on');
end

    function bave=baverage(b)
        bb=mean(sr.fold(b),2);
        bave=repmat([bb;bb(end:-1:1)],16,1);
    end

    function mdl=bmodul(beta,beta0)
        mdl=(beta-beta0)./beta0;
    end
end
