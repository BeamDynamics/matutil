function [measured,desired,response,correction] = qemsextrdtaf()
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

[c,ia,ic]=unique(selcor(11));

datadir='/machfs/laurent/correction';
here=pwd();
cd(datadir);
get_sext_correctors;
cd(here);

desired=prepdata(sext_zf3vector_ideal);
measured=prepdata(zf3_meas);
response=prepdata(sext_zf3_RMS);
response=response(:,ic);
correction=0.5*sext_K2vectorS_svd(ic);
correction(9)=0;
end

function b=prepdata(a)
acell=reshape(mat2cell(a,224*ones(1,8),size(a,2)),2,4);
b=complex(cat(1,acell{1,:}),cat(1,acell{2,:}));
end
