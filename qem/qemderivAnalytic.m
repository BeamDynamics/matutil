function [dresp,drespskew,dDx_db,dDy_ds]=qemderivAnalytic(mach,dpp,elemfunc,dv,varidx,bpmidx,hsidx,vsidx,dispfunc)
%QEMDERIV       Compute derivatives of response matrix
%
%[dresp,resp0]=qemderiv(mach,dpp,elemfunc,dval,varidx,bpmidx,hsteeridx,vsteeridx,dispfunc)
%
%MACH:      AT machine structure
%DPP:       Momentum deviation
%ELEMFUNC:  function NEWELEM=ELEMFUNC(OLDELEM,DVAL) which modifies each
%           varying element
%DVAL:      Parameter increment to compute the derivative
%VARIDX:	Index of varying elements
%BPMIDX:	Index of BPMs
%HSTEERIDX:	Index of horizontal steerers
%VSTEERIDX:	Index of vertical steerers
%DISPFUNC:  Function to display the progress: DISPFUNC(I,IMAX)
%
%DRESP: Derivative of response matrix for elements in fitidx:
%RESP0: Response matrix of the unperturbed mchine on 1 column:
%       [HRESPONSE(:);VRESPONSE(:);DFFRESPONSE;TUNES]

global NUMDIFPARAMS

dpsave=isfield(NUMDIFPARAMS,'DPStep');
if dpsave, dpsp=NUMDIFPARAMS.DPStep; end
NUMDIFPARAMS.DPStep=0.00255; % Compute the dispersion for a step equivalnent
%                               to the one used in the measurement
if nargin < 9
    fprintf('          ');
    dispfunc=@(i,itot) fprintf('\b\b\b\b\b\b\b\b\b%4d/%4d',[i itot]);
end
if nargin < 8, vsidx=hsidx; end

%warning('for test');
%varidx=varidx([1,2]);

hsidxlat=hsidx;
vsidxlat=vsidx;

nq=length(varidx);
nbpm=length(bpmidx);
nhst=length(hsidx);
nvst=length(vsidx);

dval(1:nq)=dv;
dresp=zeros((nhst+nvst)*nbpm+nbpm+2,nq);
drespskew=zeros((nhst+nvst)*nbpm+nbpm,nq);

bndidx=find(atgetcells(mach,'BendingAngle'));
bndidx=sort(bndidx([1:4:end,4:4:end]));% only long part

% disp('ANALYTIC QEMPANEL Response')
% % get anlytical RM response
% [...
%     dX_dq, dY_dq, ...
%     dDx_dq, dDx_db, Dx, ...
%     dXY_ds, dYX_ds, ...
%     XY, YX, ...
%     dDx_ds, dDy_ds...
%     ]=CalcRespXXRespMat_thick_mod(...
%     mach',dpp,...
%     bpmidx',... % bpm
%     hsidxlat,...  % correctors
%     varidx,... % quadrupoles
%     bndidx',...
%     varidx);
dispfunc(1,3);
disp('ANALYTIC QEMPANEL Response V2')
% get anlytical RM response
[...
    dX_dq, dY_dq, ...
    dDx_dq, dDx_db, Dx, ...
    dXY_ds, dYX_ds, ...
    dDx_ds, dDy_ds...
    ]=CalcRespXXRespMat_thick_V2(...
    mach',dpp,...
    bpmidx',... % bpm
    hsidxlat,...  % correctors
    varidx,... % quadrupoles
    bndidx',...
    varidx);


qind=find(atgetcells(mach,'Class','Quadrupole'))'; % response matrix computed always at all quadrupoles
quadforresponse=find(ismember(qind,varidx)); % quadrupoles to use for fit amoung all

dispfunc(2,3);
for iq=1:nq
    
    ib = quadforresponse(iq); % use only selected quadrupoles
    rha=dX_dq(:,:,ib);
    rva=dY_dq(:,:,ib);
    dxdqa=[-dDx_dq(:,ib);0;0]/dval(iq);% analytic, no tune dispersion derivative very different from 
    dresp(:,iq)=[rha(:);rva(:);dxdqa];
    rhsa=dXY_ds(:,:,ib);
    rvsa=dYX_ds(:,:,ib);
    dxdqsa=[-dDy_ds(:,ib)]/dval(iq);% analytic, no tune dispersion derivative very different from 
    drespskew(:,iq)=[rhsa(:);rvsa(:);dxdqsa];
        
    
end

NUMDIFPARAMS=rmfield(NUMDIFPARAMS,'DPStep');
dispfunc(3,3);

end
