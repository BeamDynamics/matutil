function cor=qemcorrdt(qemres,kn,cor0)
%QEMCORRDT computes quad correction (rdt method)
%   COR=QEMCORRDT(QEMRES,QERRORS,COR0)
%    Q:     quadrupole strengths
%    COR0:	initial corrector strengths
%
%   see also: QEMCOR, QEMCORFIT, QEMCORGRID

mach=qemres.at(:);
[respx,respz]=qemrdtresp(mach,qemres.bpmidx,[qemres.qpidx qemres.qcoridx]);

dkn=kn-atgetfieldvalues(mach(qemres.qpidx),'PolynomB',{2});
strength=[qemres.ql.*dkn;cor0];
fx=respx*strength;
fz=respz*strength;

if isfield(qemres,'qcorkeep')
    qpmask=qemres.qcorkeep;
else
    qpmask=true(size(qemres.qcoridx));
end
sk=[false(size(qemres.qpidx)) qpmask];
rsp=[...
    real(respx(:,sk));imag(respx(:,sk));...
    real(respz(:,sk));imag(respz(:,sk))];
b=[...
    real(fx);imag(fx);...
    real(fz);imag(fz)];

    cor=cor0;
    cor(qpmask)=cor(qpmask)-rsp\b;
end
