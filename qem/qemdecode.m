function varargout=qemdecode(mach,dct,qemres,varargin)
%QEMDECODE	Correct response matrices for steerer and BPM errors
%
%[RH,RH2V,RV2H,RV]=QEMDECODE(MACH,DCT,QEMRES,...
%   RHM,RH2VM,RV2HM,RVM,...
%   KHROT,KHGAIN,KVROT,KVGAIN,BROT,BHGAIN,BVGAIN)
%
%[RH,RH2V,RV2H,RV,FRH,FRV]=QEMDECODE(MACH,DCT,QEMRES,...
%   RHM,RH2VM,RV2HM,RVM,FRHM,FRVM,...
%   KHROT,KHGAIN,KVROT,KVGAIN,BROT,BHGAIN,BVGAIN)

orbit0=findsyncorbit(mach,dct,qemres.bpmidx);
if nargout>=5
    [rh,rh2v]=qembpmdecode(varargin{[1 2 11:13]});
    [rv2h,rv]=qembpmdecode(varargin{[3 4 11:13]});
    [frh,frv]=qembpmdecode(varargin{[5 6 11:13]});
    [rh,rh2v,rv2h,rv]=qemsteerdecode(mach,dct,qemres,rh,rh2v,rv2h,rv,...
        varargin{7:10},orbit0);
    varargout={rh,rh2v,rv2h,rv,frh,frv};
else
    [rh,rh2v]=qembpmdecode(varargin{[1 2 9:11]});
    [rv2h,rv]=qembpmdecode(varargin{[3 4 9:11]});
    [rh,rh2v,rv2h,rv]=qemsteerdecode(mach,dct,qemres,rh,rh2v,rv2h,rv,...
        varargin{5:8},orbit0);
    varargout={rh,rh2v,rv2h,rv};
end
end
