function [mach,qkl] = qemsetquad(mach,qpidx,qkl0,tunes)
%MACH=QEMSETQUAD(MACH0,SQPIDX,KL0) get/set integrated quadrupole strenghs

if nargin >= 3 && ~isempty(qkl0)
    ql=getcellstruct(mach,'Length',qpidx);
    mach=setcellstruct(mach,'PolynomB',qpidx,qkl0./ql,2);
end
if nargin >= 4 && ~isempty(tunes)
    mach=atfittune(mach,tunes-floor(tunes),'QD6','QF7');
end
if nargout >= 2
    if ~exist('kl','var')
        ql=getcellstruct(mach,'Length',qpidx);
    end
    qkl=getcellstruct(mach,'PolynomB',qpidx,2).*ql;
end
