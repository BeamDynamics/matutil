function [fns,dfns]=qemsextrdttest(response,fns0,shl,scor)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

fns=response*shl*2;
if nargin >= 4
    fns=fns+resp(:,selcor(11))*scor*2;
end
dfns=abs(fns-fns0);
residual=sqrt(sum(dfns.*dfns)/length(dfns)) %#ok<NOPRT,NASGU>
end

