function [rv2h,rv] = qemvresp(mach,bidx,sidx,orbit0,kick,gain,tilt)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


if nargin < 7, tilt=zeros(size(sidx)); end
if nargin < 6, gain=ones(size(sidx)); end
if nargin < 5, kick=[]; end
if nargin < 4, orbit0=[]; end
ck=gain.*cos(tilt);
sk=gain.*sin(tilt);
setvsteerer=@(elem,ib,kick) qemsetsteerer(elem,-kick*sk(ib),kick*ck(ib));
[rv2h,rv]=qemresp(mach,0,bidx,sidx,setvsteerer,orbit0,kick);
end
