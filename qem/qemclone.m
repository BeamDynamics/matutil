function qemb=qemclone(qemres,semres,qemb,qemb0,dirname)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

[qemres0,semres0]=qempanelset(dirname);
k0=getcellstruct(qemres0.at(:),'PolynomB',qemres0.qpidx,2);
a=load(fullfile(qemres0.datadir,'quaderrors2.mat'));
b=load(fullfile(qemres0.datadir,'skewerrors2.mat'));
[k,tilt]=semtilt(b.kn,b.ks);
dkk=(k-k0)./k0;
knew=qemb0.kn.*(1+dkk);
qemb.kn=knew.*cos(2*tilt);
%qemb.ks=-knew.*sin(2*tilt);
qemb.dipdelta=a.dipdelta;
%qemb.diptilt=b.diprot;
qemb.at=atfittune(qemat(qemres,qemb),qemres.tunes,'QD6','QF7');
qemb.kn=getcellstruct(qemb.at,'PolynomB',qemres.qpidx,2);
qemb=qemoptics(qemres,qemb);
end

