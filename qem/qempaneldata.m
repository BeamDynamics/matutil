function qemdata=qempaneldata(dirname,handles)
%QEMDATA=QEMPANELDATA((DIRNAME,HANDLES) Initialize button states
%
%QEMDATA:  errdef, cordef
%
errok=[handles.pushbutton11 handles.pushbutton16];		% save, load
errno=[handles.pushbutton3 handles.pushbutton19 handles.pushbutton9];	% fitq, fitd, initial
if exist(fullfile(dirname,'optics','quadinierrors.mat'),'file')
    errok=[errok handles.pushbutton10];	% zero, save, load
    qemdata.errdef=[handles.pushbutton9 handles.pushbutton10 handles.pushbutton16]; % initial, zero, load
else
    errno=[errno handles.pushbutton10];	% fitq, fitd, initial, zero
    qemdata.errdef=[handles.pushbutton10 handles.pushbutton16]; % zero, load
end
qemdata.cordef=[handles.pushbutton13 handles.pushbutton14];	% initial, zero
corok=[handles.pushbutton14 handles.pushbutton12 handles.pushbutton15 handles.pushbutton17]; % zero, fit, save, load
corno=handles.pushbutton13;                                 % initial
if exist(fullfile(dirname, 'cornew.dat'),'file') == 2
    corok=[corok handles.pushbutton18];
else
    corno=[corno  handles.pushbutton18];
end
set([errok corok handles.pushbutton2],'Enable','On');
set([errno corno],'Enable','Off');
end
