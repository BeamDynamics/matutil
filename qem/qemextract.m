function [qemb,qemres,semres] = qemextract(dirname)
%QEMEXTRACT extract measurement and model from directory
%
%[QEMB,QEMRES,SEMRES]=qemextract(DIRNAME)
%

filesRM=dir(fullfile(dirname,'steer*'));
if length(filesRM)==66 
    full_partial=0; % case partial matrix
elseif length(filesRM)==386
    full_partial=1; % case full matrix
else
    error('wrong number of files for response matrix');
end
    
[qemres,semres,qemb]=qempanelset(dirname,full_partial);      % load quad corrections
[qemb(2).kn,qemb(2).dipdelta,qemb(2).dxs]=...   % load quad errors
    qemerrload(qemres,fullfile(qemres.datadir,'quaderrors.mat'));

qemb(2).skewcor=load_correction(fullfile(qemres.datadir,'skewcor.dat'),...
    qemres.skcode,qemres.opticsdir);            % load skew corrections
s=load(fullfile(qemres.datadir,'skewerrors.mat'));  % load skew errors
qemb(2).ks=s.ks;
qemb(2).diptilt=s.diprot;

qemb(2).at=qemat(qemres,qemb(2),true);
qemb(2)=qemoptics(qemres,qemb(2),qemb(1));
end
