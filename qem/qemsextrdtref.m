function [fns0,resp0,shl0] = qemsextrdtref(datadir,chromaticity)
%QEMSEXTRDTREF Compute response and RDT values for perfect machine
%
%[FNS,RESPONSE,HL]=QEMSEXTRDTREF(DATADIR,CHROMATICITY)
global qemb

if nargin < 2, chromaticity=[0.11 0.40]; end
%mach=[repmat(qemb(1).at(1:102),16,1);qemb(1).at(end)];
%qpidx=findcells(mach,'Class','Quadrupole');     % Get sextupoles
%thinsext=findcells(mach,'BetaCode','LD3');
%thicksext=findcells(mach,'Class','Sextupole');
%sextidx=sort([thinsext thicksext]);
%[mach,qkl]=qemsetquad(mach,qpidx,[],[0.44 0.39]);
%[mach,shl]=qemsetsext(mach,sextidx,shl,[36.44 13.39].*chromaticity);
mach=atreadbeta('/machfs/laurent/orbit/esrf2012a_multibunch/betaperiodicmodel.str');
% mach=atreadbeta('/machfs/laurent/orbit/s13s20thick/betamodel.str');
% mach=[reshape(mach(1:end-1,ones(1,16)),[],1);mach(end,1)];
% mach{end}.Voltage=8e6;
[fns0,resp0,shl0]=qemsextrdtval(datadir,mach,chromaticity,2);
end

