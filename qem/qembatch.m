function [qemb,qemres,semres] = qembatch(dirname,varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

options=struct(varargin{:});
if ~isfield(options,'readq'), options.readq='quaderrors.mat'; end
if ~isfield(options,'reads'), options.reads='skewerrors.mat'; end
if ~isfield(options,'computeq'), options.computeq=0; end
if ~isfield(options,'computes'), options.computes=0; end
if ~isfield(options,'saveq'), options.saveq=[]; end
if ~isfield(options,'saves'), options.saves=[]; end
if ~isfield(options,'wrongbpms'), options.wrongbpms=[]; end
if ~isfield(options,'savegain'), options.savegain=[]; end
if ~isfield(options,'verbose'), options.verbose=false; end
if ~isfield(options,'respmat'), options.respmat=false; end

[qemres,semres,qemb]=qempanelset(dirname);
okbpm=true(length(qemres.bpmidx),1);
okbpm(options.wrongbpms)=false;
%fprintf('\n\n------------------ end quad panelset ---------------------\n\n');
if options.respmat || (options.computeq > 0) || (options.computes > 0)
    [qemres.resph,qemres.respv,qemres.frespx,semres.frespz]=qemcheckresp(qemres,options.wrongbpms);
    qemres.respiax=load_sriaxresp(qemres.datadir,qemres.opticsdir,'v',qemres.vlist);
    [semres.resph,semres.respv]=semloadresp(qemres,semres,options.wrongbpms);
    semres.respiax=load_sriaxresp(qemres.datadir,qemres.opticsdir,'h2v',semres.hlist);
    semres.iaxemz=semloadiax(qemres.datadir);
end
%fprintf('\n\n------------------ end load responses ---------------------\n\n');

if ~isempty(options.readq)
    [qemb(2).kn,qemb(2).dipdelta,qemb(2).dxs]=qemerrload(qemres,...
        fullfile(qemres.datadir,options.readq));
    %fprintf('\n\n------------------ end load quad errors--------------------\n\n');
end

if options.computeq > 0
    for i=1:options.computeq
        qemb(2).at=qemat(qemres,qemb(2));
        %qemb(2)=qemoptics(qemres,qemb(2),qemb(1));  % To get qemb(2).fractunes
        qemb(2).kn=qempanelfitq(qemb(2),qemres,semres,qemres.qpfit,okbpm);
        qemb(2).at=qemat(qemres,qemb(2));
        dipdelta = qemfitdipole(qemb(2).at,qemres,semres);
        qemb(2).dipdelta=qemb(2).dipdelta+dipdelta-mean(dipdelta);
    end
    %fprintf('\n\n--------------- end fit normal quads + dipoles ------------------\n\n');
end

if ~isempty(options.saveq)
    s=struct('k',qemb(2).kn,'dipdelta',qemb(2).dipdelta,'dxs',qemb(2).dxs); %#ok<NASGU>
    save(fullfile(qemres.datadir,options.saveq),'-struct','s');
end

if ~isempty(options.reads)
    qemb(2).skewcor=load_correction(fullfile(qemres.datadir,'skewcor.dat'),...
        qemres.skcode,qemres.opticsdir);
    s=load(fullfile(qemres.datadir,options.reads));
    qemb(2).ks=s.ks;
    qemb(2).diptilt=s.diprot;
    %fprintf('\n\n------------------ end load skew errors--------------------\n\n');
end

if options.computes > 0
    semres=semresponse(qemres,semres,qemb(2));
    for i=1:options.computes
        qemb(2).at=qemat(qemres,qemb(2));
        qemb(2).ks=sempanelfitq(qemb(2),qemres,semres);
        qemb(2).at=qemat(qemres,qemb(2)); % update at model
        qemb(2).diptilt=qemb(2).diptilt+semfitdipole(qemb(2).at,qemres,semres);
    end
    %fprintf('\n\n------------------ end fit skew quads + dipoles ------------------\n\n');
end

if ~isempty(options.saves)
    s=struct('kn',qemb(2).kn,'ks',qemb(2).ks,'diprot',qemb(2).diptilt,...
        'dzs',qemb(2).dzs); %#ok<NASGU>
    save(fullfile(qemres.datadir,options.saves),'-struct','s');
end

if ~isempty(options.savegain)
    [w.khrot,w.khgain,w.kvrot,w.kvgain,w.bhrot,w.bhgain,w.bvrot,w.bvgain]=...
        qemguess(qemb(2).at,qemres,...
        qemres.resph,semres.respv,semres.resph,qemres.respv);
    save(fullfile(qemres.datadir,options.savegain),'-struct','w');
end

if options.verbose
    qemb(2)=qempaneldisp(qemres,semres,qemb(2),qemb(1),struct('compare',true));
    qemb(2)=sempaneldisp(qemres,semres,qemb(2),qemb(1),struct('compare',true));
else
    qemb(2).at=qemat(qemres,qemb(2));
    qemb(2)=qemoptics(qemres,qemb(2),qemb(1));
end
end
