function mach=qemat(qemres,qemb,coupling)
%SEMBUILDAT		Updates the AT structure with strengths, tilts
%
%MACH=QEMAT(QEMRES,QEMB,COUPLING)
%
%QEMRES:	fields bpmidx,qpidx,dipidx,qcoridx,skewidx,qcorl,skewl
%QEMB:      fields kn,cor,dipdelta,ks,skewcor,diptilt
%COUPLING:  flag enabling voupled machine
%
%MACH : Resulting AT structure
%
if nargin < 3, coupling=true; end
if coupling
%     mach=sembuildat(qemres.at(:),qemres,qemb.kn,qemb.cor(:,end),...
%         qemb.dipdelta,qemb.ks,qemb.skewcor(:,end),qemb.diptilt);
    [k,tilts]=semtilt(qemb.kn,qemb.ks);
    mach=atsetshift(qemres.at(:),qemres.sextidx,qemb.dxs,qemb.dzs);
    mach=atsettilt(mach,qemres.dipidx,qemb.diptilt);
    mach=setcellstruct(mach,'PolynomA',qemres.skewidx,qemb.skewcor(:,end)./qemres.skewl,2);
    mach=atsettilt(mach,qemres.qpidx,tilts);
    bend=getcellstruct(mach,'BendingAngle',qemres.dipidx);
    mach=setcellstruct(mach,'BendingAngle',qemres.dipidx,(1+qemb.dipdelta).*bend);
    mach=setcellstruct(mach,'PolynomB',qemres.qcoridx,qemb.cor(:,end)./qemres.qcorl,2);
    mach=setcellstruct(mach,'K',qemres.qpidx,k);
    mach=setcellstruct(mach,'PolynomB',qemres.qpidx,k,2);
else
%     mach=sembuildat(qemres.at(:),qemres,qemb.kn,qemb.cor(:,end),...
%         qemb.dipdelta);
    k=qemb.kn;
    mach=atsetshift(qemres.at(:),qemres.sextidx,qemb.dxs,0);
    bend=getcellstruct(mach,'BendingAngle',qemres.dipidx);
    mach=setcellstruct(mach,'BendingAngle',qemres.dipidx,(1+qemb.dipdelta).*bend);
    mach=setcellstruct(mach,'PolynomB',qemres.qcoridx,qemb.cor(:,end)./qemres.qcorl,2);
    mach=setcellstruct(mach,'K',qemres.qpidx,k);
    mach=setcellstruct(mach,'PolynomB',qemres.qpidx,k,2);
end
