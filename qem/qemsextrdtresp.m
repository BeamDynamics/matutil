function [fns,bpm,sext]=qemsextrdtresp(mach,bpmidx,sextidx)
%QEMSEXTRDTRESP compute resonance driving terms at BPM locations
%
%[FNS0,FNS1,FNS2,FNS3]=QEMSEXTRDTRESP(QEMB,BPMIDX,SEXTIDX)
%
global f3000 f1200 f1020 f0120 f0111
nbpm=length(bpmidx);
nsext=length(sextidx);

[refpts,~,kl]=unique([bpmidx sextidx length(mach)+1]);
jbpm=kl(1:nbpm);
jsext=kl(nbpm+(1:nsext));
jend=kl(end);
[vdata,avebeta,avemu,~,~,chroms]=atavedata(mach,0,refpts);
tns=vdata(jend).mu/2/pi;
disp(tns);
disp(chroms./tns);
mtx=vdata(jend).mu(1);
mtz=vdata(jend).mu(2);

if nargout >= 6
    sext.beta=avebeta(jsext,:);
    sext.phase=avemu(jsext,:);
end
if nargout >= 5
    bpm.beta=avebeta(jbpm,:);
    bpm.phase=avemu(jbpm,:);
end

sbetaxxx=sqrt(avebeta(jsext,1)').*avebeta(jsext,1)';
sbetaxzz=sqrt(avebeta(jsext,1)').*avebeta(jsext,2)';

dphix=dphase(avemu(jbpm,1),avemu(jsext,1),mtx);
dphiz=dphase(avemu(jbpm,2),avemu(jsext,2),mtz);

f3000=-sbetaxxx(ones(nbpm,1),:).*complex(cos(3*dphix),sin(3*dphix))/...
    (1-complex(cos(3*mtx),sin(3*mtx)))/48;
f1200=-sbetaxxx(ones(nbpm,1),:).*complex(cos(-dphix),sin(-dphix))/...
    (1-complex(cos(-mtx),sin(-mtx)))/16;
f1020=sbetaxzz(ones(nbpm,1),:).*complex(cos(dphix+2*dphiz),sin(dphix+2*dphiz))/...
    (1-complex(cos(mtx+2*mtz),sin(mtx+2*mtz)))/16;
f0120=sbetaxzz(ones(nbpm,1),:).*complex(cos(-dphix+2*dphiz),sin(-dphix+2*dphiz))/...
    (1-complex(cos(-mtx+2*mtz),sin(-mtx+2*mtz)))/16;
f0111=sbetaxzz(ones(nbpm,1),:).*complex(cos(-dphix),sin(-dphix))/...
    (1-complex(cos(-mtx),sin(-mtx)))/8;

fns=[...
    3*f3000-conj(f1200);...     % fns3
    f1020-f0120;...             % fns2
    2*f1020-conj(f0111);...     % fns1
    2*f0120-f0111];             % fns0

    function dph=dphase(phib,phik,mtune)
        nb=length(phib);
        nk=length(phik);
        dph=phib(:,ones(1,nk))-phik(:,ones(nb,1))';
        neg=(dph < 0);
        dph(neg)=dph(neg)+mtune;
    end

end

