function varargout = qembpmdecode(varargin)
%QEMBPMDECODE   Correct response matrices for BPM gain and rotation
%[RH0,RV0]=QEMBPMDECODE(RHBPM,RVBPM,ROTATION,HGAIN,VGAIN)
%R0=QEMBPMDECODE(RBPM,GAIN)


nk=size(varargin{1},2);
if length(varargin) <= 2
    [rbpm,bgain]=deal(varargin{1:2});
    varargout={rbpm.*repmat(bgain,1,nk)};
else
    [rhbpm,rvbpm,brot,bhgain,bvgain]=deal(varargin{1:5});
    cb=cos(brot);
    sb=sin(brot);
    rh0= rhbpm.*repmat(cb.*bhgain,1,nk) - rvbpm.*repmat(sb.*bvgain,1,nk);
    rv0= rhbpm.*repmat(sb.*bhgain,1,nk) + rvbpm.*repmat(cb.*bvgain,1,nk);
    varargout={rh0,rv0};
end
end
