function varargout = qempanel2(varargin)
% QEMPANEL2 M-file for qempanel2.fig
%      QEMPANEL2, by itself, creates a new QEMPANEL2 or raises the existing
%      singleton*.
%
%      H = QEMPANEL2 returns the handle to a new QEMPANEL2 or the handle to
%      the existing singleton*.
%
%      QEMPANEL2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in QEMPANEL2.M with the given input arguments.
%
%      QEMPANEL2('Property','Value',...) creates a new QEMPANEL2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before qempanel2_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to qempanel2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help qempanel2

% Last Modified by GUIDE v2.5 24-Aug-2018 14:19:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @qempanel2_OpeningFcn, ...
    'gui_OutputFcn',  @qempanel2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before qempanel2 is made visible.
function qempanel2_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to qempanel2 (see VARARGIN)

% Choose default command line output for qempanel2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes qempanel2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);
handles.qemdata=[];
guidata(hObject,handles);


% --- Outputs from this function are returned to the command line.
function varargout = qempanel2_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1: select data directory
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
dirname=qemseldir();
qemres.datadir=dirname;

[qemres,semres,qemb]=qempanelset(dirname,get(handles.PartialRMcheckbox,'Value'));
handles.qemdata=qempaneldata(dirname,handles);
set(handles.edit1,'String',dirname);
set(handles.ExcludeBPMList,'String','');
pushbutton2_Callback(hObject, eventdata, handles); % qem check response
guidata(hObject,handles);


function edit1_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2: Check response
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres

% disable changes in RM . restart qempanel to get them back.
set(handles.ACRMcheckbox,'Enable','off')
set(handles.DCRMcheckbox,'Enable','off')
set(handles.FullRMcheckbox,'Enable','off')
set(handles.PartialRMcheckbox,'Enable','off')

wrongbpms=qemres.wrongbpms; % find(bpmm.All_Status.read~=0)
[semres.resph,semres.respv]=semloadresp(qemres,semres,wrongbpms);
[qemres.resph,qemres.respv,qemres.frespx,semres.frespz]=qemcheckresp(qemres,semres,wrongbpms);
semres.respiax=load_sriaxresp(qemres.datadir,qemres.opticsdir,'h2v',semres.hlist);
qemres.respiax=load_sriaxresp(qemres.datadir,qemres.opticsdir,'v',qemres.vlist);
qemplotresp(3,qemres.resph,qemres.respv,'response');
set(handles.pushbutton3,'Enable','On');
qemb(2)=qempaneldisp(qemres,semres,qemb(2),qemb(1),handles);

if get(handles.ACRMcheckbox,'Value')
    set(handles.statustext,'String','Loaded AC response data');
elseif get(handles.DCRMcheckbox,'Value')
    set(handles.statustext,'String','Loaded DC response data');
end

% --- Executes on button press in pushbutton3: Fit Quads
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
set(handles.statustext,'String','fitting quadrupoles');

if get(handles.AnalyticFitCheck,'Value') == 1
    hw=waitbar(0,'Fitting quadrupole strengths (analytic)...');
else
    hw=waitbar(0,'Fitting quadrupole strengths...');
end
dispfunc=@(i,itot) waitbar(i/itot,hw);
okbpm=true(length(qemres.bpmidx),1);
okbpm(qemres.wrongbpms) = false;

if get(handles.AnalyticFitCheck,'Value') == 1
    qemb(2).kn=qempanelfitqAnalytic(qemb(2),qemres,semres,qemres.qpfit,okbpm,qemres.qmode,dispfunc);
else
    qemb(2).kn=qempanelfitq(qemb(2),qemres,semres,qemres.qpfit,okbpm,qemres.qmode,dispfunc);
end
set([handles.qemdata.errdef handles.pushbutton19],'Enable','On');
delete(hw);
qemb(2)=qempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set(handles.statustext,'String','finished quadrupoles fit');

% --- Executes on button press in pushbutton19: Fit Dips
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
okbpm=true(length(qemres.bpmidx),1);
okbpm(qemres.wrongbpms) = false;
set(handles.statustext,'String','fitting dipoles');

if get(handles.AnalyticFitCheck,'Value') == 1
    dipdelta = qemfitdipoleAnalytic(qemb(2).at(:),qemres,semres,[],okbpm);
else
    dipdelta = qemfitdipole(qemb(2).at(:),qemres,semres,[],okbpm);
end
%qemb(2).dipdelta=qemb(2).dipdelta+dipdelta;
qemb(2).dipdelta=qemb(2).dipdelta+dipdelta-mean(dipdelta);
qemb(2).at=qemat(qemres,qemb(2),true); % update at model
qemres.bhscale=qembhscale(qemres,qemb(2).at);
qemb(2)=qempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set(handles.statustext,'String','finished dipoles fit');

% --- Executes on button press in pushbutton7: create new directory
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres MACHFS
tnow=now();
measpath=fullfile(MACHFS,'MDT',datestr(tnow,'yyyy'),datestr(tnow,'mmmdd'));
list=dir(fullfile(measpath,'resp*'));
namelist={list.name};

mainname='resp';
if  get(handles.ACRMcheckbox,'Value')
    mainname='respAC';
end

for i=1:100
    measname=[mainname num2str(i)];
    if ~any(strcmp([mainname num2str(i)],namelist)); break; end
end
answer=inputdlg({'Directory name:'},'Choose directory name',1,{fullfile(measpath,measname)});
if ~isempty(answer)
    [measpath,measname,ext]=fileparts(answer{1});
    measdir=fullfile(measpath,[measname ext]);
    if exist(measdir,'dir') == 7
        error('qem:nodir','Directory already existing');
    end
    [ok,message,messageid]=mkdir(measpath,[measname ext]);
    if ~ok
        error(messageid,['Cannot create measurement directory: ' message])
    end
    dirname=qemlinkdir(qemseldir(qeminitdir(measdir,handles)));
    [qemres,semres,qemb]=qempanelset(dirname,get(handles.PartialRMcheckbox,'Value'));
    handles.qemdata=qempaneldata(dirname,handles);
    guidata(hObject,handles);
    set(handles.edit1,'String',dirname);
    set(handles.pushbutton8,'Enable','On');
end
set(handles.statustext,'String','new directory created');


% --- Executes on button press in pushbutton8 : Start measurement
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%set(hObject,'Enable','Off');
global qemres
if get(handles.DCRMcheckbox,'Value') % DC/AC DC measurement
    if get(handles.PartialRMcheckbox,'Value') % FULL/PARTIAL
        set(handles.statustext,'String','DC Partial RM measurement in progress');
        [status,out]=termappli('respmat.py -i foc','response matrix',qemres.datadir);
    elseif  get(handles.FullRMcheckbox,'Value')
        set(handles.statustext,'String','DC Full RM measurement in progress');
        [status,out]=termappli('respmat.py -i -g 0 full','response matrix',qemres.datadir);
    end
else % AC rm measurement
    
    % ADD WARNING MESSAGE or question window to test FOFB is running.
    
    answer = questdlg('TEST FOFB (ON, then OFF). When ok press Yes','FOFB test');
    
    if strcmp(answer,'Yes')
        
        set(handles.statustext,'String','AC RM measurement');
        
        n=80000; % total excitation duration in 0.1 ms (10kHz samples)
        % (about 80000 corresponds approx. to 8s total,
        % excited in blocks of 4 steerers)
        Xamp=8000; % amplitude of horizontal excitation (a.u.)
        Zamp=1500; % amplitude of vertical excitation (a.u.)
        
        % ensure that the correct functions are used 
        addpath('/mntdirect/_operation/machine/matlab/qem/ACORM')
        
        % measure RM
        [n_and_dateX] = StartMresStimX(n,Xamp,qemres);
        [n_and_dateZ] = StartMresStimZ(n,Zamp,qemres);
        
        % get data from Fast Data Archiver
        [normI,phi,firstX,MresH,MresVH] = CalMresfdaH(n_and_dateX,Xamp,qemres); %#ok<ASGLU>
        [normI,phi,firstZ,MresHV,MresV] = CalMresfdaV(n_and_dateZ,Zamp,qemres); %#ok<ASGLU>
        
        responsematrixACfile = fullfile(qemres.datadir,'AC_RM_raw.mat');
        
        save(responsematrixACfile,'MresH','MresVH','MresHV','MresV');
        % same data is  /users/diag/matlab/libera_data/TimeLabel/TimeLabelMres[XZ]**-XXX-****_**h**.mat
        
        gainfile = 'Gain_new_180604'; % this file is in (local clone of matutil repository)/qem/ACORM
        ReadFullRespMatData(qemres.datadir, responsematrixACfile,gainfile)
           
        % use respmat.py to compute all other files
        [status,out]=termappli('respmat.py -i -g 0 nop','response matrix',qemres.datadir);
    
        set(handles.statustext,'String','Finished AC RM measurement');
        
    else
        
        set(handles.statustext,'String','AC steerers NOT OK. Measurement aborted.');
        status = 1;
        
    end
end
set(hObject,'Enable','On');
if status ~= 0, error('qem:noexec',out); end
set(handles.statustext,'String','measurement completed');

% --- Executes on button press in pushbutton9 : initial errors
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
try
    [kn,qemb(2).dipdelta,qemb(2).dxs]=qemerrload(qemres,...
        fullfile(qemres.opticsdir,'quadinierrors.mat'));
    qemb(2).kn=qemb(1).kn.*(1+srmodulation(kn));
catch %#ok<CTCH>
    warning('qem:noinit','No initial errors in the optics directory');
    qemb(2).kn=qemb(1).kn;
    qemb(2).dipdelta=qemb(1).dipdelta;
    qemb(2).dxs=qemb(1).dxs;
end
mach=atfittune(qemat(qemres,qemb(2),true),qemres.tunes,'QD6','QF7'); % retune the model
qemb(2).kn=getcellstruct(mach,'PolynomB',qemres.qpidx,2);
qemres.bhscale=1;
set(handles.qemdata.errdef,'Enable','On');
set(hObject,'Enable','Off');
qemb(2)=qempaneldisp(qemres,semres,qemb(2),qemb(1),handles);

% --- Executes on button press in pushbutton10 : zero errors
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
qemb(2).kn=qemb(1).kn;
qemb(2).dipdelta=qemb(1).dipdelta;
qemb(2).dxs=qemb(1).dxs;
mach=atfittune(qemat(qemres,qemb(2),true),qemres.tunes,'QD6','QF7'); % retune the model
qemb(2).kn=getcellstruct(mach,'PolynomB',qemres.qpidx,2);
set(handles.qemdata.errdef,'Enable','On');
set(hObject,'Enable','Off');
qemb(2)=qempaneldisp(qemres,semres,qemb(2),qemb(1),handles);

% --- Executes on button press in pushbutton11: save errors
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
set(handles.statustext,'String','saving errors');

qfile=fullfile(qemres.datadir, 'quaderrors.mat');
gfile=fullfile(qemres.datadir, 'gainfit.mat');
ok=(exist(qfile,'file') ~= 2);
if ~ok
    [fname,fpath]=uiputfile('*.mat','Save errors:',qfile);
    if ischar(fname), qfile=fullfile(fpath,fname); ok=true; end
end
if ok
    s=struct('k',qemb(2).kn,'dipdelta',qemb(2).dipdelta,'dxs',qemb(2).dxs); %#ok<NASGU>
    save(qfile,'-struct','s');
    [khrot,khgain,kvrot,kvgain,bhrot,bhgain,bvrot,bvgain]=...
        qemguess(qemb(2).at,qemres,...
        qemres.resph,semres.respv,semres.resph,qemres.respv); %#ok<ASGLU>
    save(gfile,'khrot','khgain','kvrot','kvgain','bhrot','bhgain','bvrot','bvgain');
end
set(handles.statustext,'String','errors saved');


% --- Executes on button press in pushbutton16: Load errors
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
qfile=fullfile(qemres.datadir, 'quaderrors.mat');
[fname,fpath]=uigetfile({'*.mat';'*.dat'},'Load errors:',qfile);
if ischar(fname)
    [qemb(2).kn,qemb(2).dipdelta,qemb(2).dxs]=qemerrload(qemres,fullfile(fpath,fname));
    set(handles.qemdata.errdef,'Enable','On');
    qemb(2).at=qemat(qemres,qemb(2),true); % update at model
    qemres.bhscale=qembhscale(qemres,qemb(2).at);
    qemb(2)=qempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
end


% --- Executes on button press in pushbutton12: Fit corrections
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
set(handles.statustext,'String','computing correction');
fitproc={[],@qemcorrdt};
fitmethod=fitproc{get(handles.popupmenu1,'Value')};
if ~isempty(fitmethod)
    qemb(2).cor(:,2)=fitmethod(qemres,qemb(2).kn,qemb(2).cor(:,end));
    set(handles.qemdata.cordef,'Enable','On');
end
qemb(2)=qempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set(handles.statustext,'String','correction computed');


% --- Executes on button press in pushbutton13: Corrections Initial
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
%qemb(2).cor(:,2)=-qemreadcor(fullfile(qemres.datadir,'/magcor.dat'));
qemb(2).cor(:,2)=load_correction(fullfile(qemres.datadir,'magcor.dat'),...
    qemres.qpcode,qemres.opticsdir);
set(handles.qemdata.cordef,'Enable','On');
set(hObject,'Enable','Off');
qemb(2)=qempaneldisp(qemres,semres,qemb(2),qemb(1),handles);


% --- Executes on button press in pushbutton14: Corrections Zero
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
qemb(2).cor(:,2)=qemb(1).cor;
set(handles.qemdata.cordef,'Enable','On');
set(hObject,'Enable','Off');
qemb(2)=qempaneldisp(qemres,semres,qemb(2),qemb(1),handles);


% --- Executes on button press in pushbutton15: Correction Save
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres
set(handles.statustext,'String','saving correction');
corfile=fullfile(qemres.datadir, 'cornew.dat');
ok=(exist(corfile,'file') ~= 2);
if ~ok
    [fname,fpath]=uiputfile('*.dat','Save correction:',corfile);
    if ischar(fname), corfile=fullfile(fpath,fname); ok=true; end
end
if ok
    %   qemwritecor(corfile,-qemb(2).cor(:,end));
    save_correction(corfile,qemres.qpcode,qemb(2).cor(:,end),qemres.opticsdir);
    set(handles.pushbutton18,'Enable','On');
end
set(handles.statustext,'String','correction saved');

% --- Executes on button press in pushbutton17: Correction Load
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
corfile=fullfile(qemres.datadir, 'magcor.dat');
[fname,fpath]=uigetfile('*.dat','Load correction:',corfile);
if ischar(fname)
    %   qemb(2).cor(:,2)=-qemreadcor(fullfile(fpath,fname));
    qemb(2).cor(:,2)=load_correction(fullfile(fpath,fname),qemres.qpcode,qemres.opticsdir);
    set(handles.qemdata.cordef,'Enable','On');
    qemb(2)=qempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton18: Correction apply
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemres
bindir=[getenv('MACH_HOME') '/bin/' getenv('MACHARCH') '/'];
corfile=fullfile(qemres.datadir, 'cornew.dat');
sr.setcorbase(corfile,'sr/qp-n/all','sr/reson-quad/m2_n0_p73','base_quad2'); % Set resonance device
[s,w]=unix([bindir 'dvset <' corfile]); %#ok<ASGLU>         % Set values
disp(w);
set(handles.statustext,'String','correction applied');



function edit3_Callback(hObject, eventdata, handles)	% Vertical tune
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)	% Horizontal tune
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in AnalyticFitCheck.
function AnalyticFitCheck_Callback(hObject, eventdata, handles)
% hObject    handle to AnalyticFitCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AnalyticFitCheck
set(handles.NumericFitcheckbox,'Value',0.0)
set(handles.AnalyticFitCheck,'Value',1.0)


% --- Executes on button press in PartialRMcheckbox.
function PartialRMcheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to PartialRMcheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PartialRMcheckbox
set(handles.PartialRMcheckbox,'Value',1.0)
set(handles.FullRMcheckbox,'Value',0.0)


% --- Executes on button press in FullRMcheckbox.
function FullRMcheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to FullRMcheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of FullRMcheckbox
set(handles.PartialRMcheckbox,'Value',0.0)
set(handles.FullRMcheckbox,'Value',1.0)


% --- Executes on button press in NumericFitcheckbox.
function NumericFitcheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to NumericFitcheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NumericFitcheckbox
set(handles.NumericFitcheckbox,'Value',1.0)
set(handles.AnalyticFitCheck,'Value',0.0)


% --- Executes on button press in DCRMcheckbox.
function DCRMcheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to DCRMcheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of DCRMcheckbox
set(handles.DCRMcheckbox,'Value',1.0)
set(handles.ACRMcheckbox,'Value',0.0)


% --- Executes on button press in ACRMcheckbox.
function ACRMcheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to ACRMcheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ACRMcheckbox
set(handles.ACRMcheckbox,'Value',1.0)
set(handles.DCRMcheckbox,'Value',0.0)

if get(handles.PartialRMcheckbox,'Value') % FULL/PARTIAL
    set(handles.PartialRMcheckbox,'Value',0.0)
    set(handles.FullRMcheckbox,'Value',1.0)
    disp('Force Full for AC measurement')
end
        



function ExcludeBPMList_Callback(hObject, eventdata, handles)
% hObject    handle to ExcludeBPMList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ExcludeBPMList as text
%        str2double(get(hObject,'String')) returns contents of ExcludeBPMList as a double
global qemres
str = get(hObject,'String');
a=cellfun(@str2double,strsplit(str,','));

% append to already excluded BPMs
qemres.wrongbpms = unique([qemres.wrongbpms a]);
mess=[num2str(length(qemres.wrongbpms)) ' bpms not used: ' num2str(qemres.wrongbpms,'%d ')];
set(handles.text3,'String',mess);

pushbutton2_Callback(hObject, eventdata, handles); % qem check response

% --- Executes during object creation, after setting all properties.
function ExcludeBPMList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ExcludeBPMList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
