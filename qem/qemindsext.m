function [cor,resp] = qemindsext(qemres,mach,fns0)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

isx=srfold(1:224);
% s16=isx(1,12:13);
% s30=isx(1,26:27);
s24c23=isx(7,19:20);
s22c23=isx(6,19:20);
nsx=length(qemres.sextidx);
sextidx=qemres.sextidx([1:end s24c23(:)' s22c23(:)']);
[f0,f1,f2,f3]=qemsextrdtresp(mach,qemres.bpmidx,sextidx);
[qkl,shl]=load_srmag('textfile',fullfile(qemres.datadir,'srmag.dat'),20.15);
resp=[f0;f1;f2;f3];

fns=resp(:,1:nsx)*shl*2;
rhs=fns0-fns;

corresp=[sum(resp(:,nsx+1:nsx+2),2) sum(resp(:,nsx+3:nsx+4),2)];

cor=0.5*([real(corresp);imag(corresp)]\[real(rhs);imag(rhs)]);

cor=cor([1 1 2 2]);

rhs2=fns0-(fns+resp(:,nsx+1:end)*cor*2);
v1=[real(rhs);imag(rhs)];
v2=[real(rhs2);imag(rhs2)];
disp(sum(v1.*v1));
disp(sum(v2.*v2));
end
