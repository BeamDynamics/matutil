function [rx,rz,N,S,bang,alldip,usedip]=qemdispderivAnalyticDQ(mach,varidx,bpmidx)
%QEMDISPDERIV	compute derivatives of the frequency response
%
%[RX,RZ]=qemdispderivAnalytic(MACH,VARIDX,BPMIDX)
%
%MACH:      AT machine structure
%VARIDX:	Index of varying elements
%BPMIDX:	Index of BPMs
%
%RX:        derivative of the horizontal response to frequency
%RZ:        derivative of the vertical response to frequency

disp('Analytic');

nb=length(bpmidx);

alldip=find(atgetcells(mach,'BendingAngle'));
bang=atgetfieldvalues(mach,alldip,'BendingAngle'); 
alldip=alldip(bang>1e-9);
bang=bang(bang>1e-9);

% make sure order is as in qempanel vector.
[usedip,b]=ismember(varidx,alldip);

%% compute analytic response with all dipoles always.

[N,S,~,~]=dDxyDthetaDQ(mach,bpmidx,alldip,'magmodel','thick'); 

% negative sign, probably due to dipole sign convention
N_sort=-N(:,b);  
% qempanel response in [m/Hz] instead of [m/%]
N_sort=N_sort/mcf(mach); 
% in qempanel the response is for scaling factors, not deltas
N_sort=N_sort.*repmat(bang(b)',nb,1);


S_sort=S(:,b);  
% qempanel response in [m/Hz] instead of [m/%]
S_sort=S_sort/mcf(mach); 
% in qempanel the response is for scaling factors, not deltas
S_sort=S_sort.*repmat(bang(b)',nb,1);


rx=N_sort;
rz=S_sort;

end
