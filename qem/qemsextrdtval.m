function [fns,resp,shl,chresp] = qemsextrdtval(datadir,mach,chromaticity,np)
%QEMSEXTRDTVAL Compute response and RDT values for actual machine
%
%[FNS,RESPONSE,HL]=QEMSEXTRDTVAL(DATADIR,MACH,CHROMATICITY)

if nargin < 4, np=1; end
if nargin < 3, chromaticity=[0.11 0.40]; end
tunes=[36.44 13.39];
args={'textfile','families'};

qpidx=findcells(mach,'Class','Quadrupole');     % Get sextupoles
thinsext=findcells(mach,'BetaCode','LD3');
thicksext=findcells(mach,'Class','Sextupole');
sextidx=sort([thinsext thicksext]);
[qkl,shl]=load_srmag(args{1:np},fullfile(datadir,'srmag.dat'),20.15);
[mach,qkl]=qemsetquad(mach,qpidx,[],tunes);
[mach,shl]=qemsetsext(mach,sextidx,shl,tunes.*chromaticity);
                                                % Compute response
bpmidx=findcells(mach,'BetaCode','PU');
resp=qemsextrdtresp(mach,bpmidx,sextidx);
chresp=qemsextchromaresp(mach,sextidx);

fns=resp*shl*2;
end
