function qk1 = qemnewmodel(dirlist,qk0)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[~,~,~,~,~,~,dqk]=qemerrstat(dirlist,qk0);
if nargin < 3
    dqk3=aveoptics(dqk);
else
    dqk23=mean2(dqk(149:156,:),2);
    aa=mean2(reshape(dqk23([1:4 8:-1:5]),4,2),2);
    dqk(149:156,:)=NaN;         % remove cell 22 23
    dqk3=aveoptics(dqk);
    dqk3(149:156)=aa([1:4 4:-1:1]);
end
qk1=qk0.*(1+dqk3);

    function k1=aveoptics(k0)
        ka=sr.fold(mean2(k0,2));	% average all samples
        kb=mean2(ka,2);         % average all cells
        kc=repmat(kb,1,32);
        k1=sr.unfold(kc);
    end

end
