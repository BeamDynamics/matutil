function line=fskip(fid,code,offset)

if nargin < 3, offset=0; end
while true
    line=fgetl(fid);
    if ~ischar(line)
        break
    elseif ~isempty(strfind(line,code))
        for i=1:offset
            line=fgetl(fid);
        end
        break
    end
end
