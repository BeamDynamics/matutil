function [idx,kdx]=getindex(npercell,ncells,celloffset,bfunc,varargin)
%GETINDEX	find index of elements in a periodic lattice
%
%IDX=GETINDEX(NPERCELL,NCELLS,CELLOFFSET,SCANFUNCTION,NAME)
%
% NPERCELL:     Number of elements per cell
% NCELLS:       Total number of cells
% CELLOFFSET:	Number of cells to skip
% SCANFUNCTION: Function returning [NCELL,NID] from the name
% NAME:         string, char array or cell array of strings
%
%IDX=GETINDEX(NPERCELL,NCELLS,CELLOFFSET,SCANFUNCTION,CELL,NUM)
%IDX=GETINDEX(NPERCELL,NCELLS,CELLOFFSET,SCANFUNCTION,[CELL NUM])
%
% CELL:         Cell number (1..NCELLS)
% NUM:          Index in the cell (1..NPERCELL)
%
%IDX=GETINDEX(NPERCELL,NCELLS,CELLOFFSET,SCANFUNCTION,KDX)
%
% KDX:          Index starting in cell 1
%
%[IDX,KDX]=GETINDEX(...)	returns in addition the "hardware" index
%
%See also: GETMASK GETCELLNUM

npc=zeros(1,ncells);
npc(:)=reshape(npercell,[],1);
c2=[0 cumsum(circshift(npc,[0 celloffset]))];
arg1=varargin{1};
if iscell(arg1)                 % cell array of strings
    [cellnum,id]=cellfun(bfunc,arg1);
    kdx=reshape(c2(cellnum),size(cellnum))+id;
elseif ischar(arg1)             % char array
    [cellnum,id]=cellfun(bfunc,cellstr(arg1));
    kdx=reshape(c2(cellnum),size(cellnum))+id;
elseif length(varargin) >= 2	% cell,id
    kdx=reshape(c2(arg1),size(arg1))+varargin{2};
elseif size(arg1,2) == 2        % [cell id]
    kdx=reshape(c2(arg1(:,1)),[],1)+arg1(:,2);
else                            % "hardware" index
    kdx=arg1;
end
idx=mod(kdx-c2(celloffset+1)-1,sum(npc))+1;
end
