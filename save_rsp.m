function save_rsp(plane,a)
%SAVE_RSP(plane,a)
%	plane:	'h' or 'v'
%	a:	response matrix (mm/mA)

slist=[6,19,22];

if strcmp(plane,'h')
filename='resHmod.rsp';
border=[1 2 4 6 9 11 13 14 7 8 3 5 10 12];
deltaI=0.08;
elseif strcmp(plane,'h2v')
filename='resH2Vmod.rsp';
border=[1 2 3 5 10 12 13 14 4 6 9 11 7 8];
deltaI=0.08;
elseif strcmp(plane,'v2h')
filename='resV2Hmod.rsp';
border=[1 2 4 6 9 11 13 14 7 8 3 5 10 12];
deltaI=0.10;
else
filename='resVmod.rsp';
border=[1 2 3 5 10 12 13 14 4 6 9 11 7 8];
deltaI=0.10;
end

nb=length(border);
periods=size(a,1)/nb;
nk=size(a,2)/periods;

bpm=reshape(1:nb*periods,nb,periods)';
bpm=bpm(:,border);
steerer=reshape(1:nk*periods,nk,periods)';

curs=deltaI*ones(1,nk*periods);
bid=deltaI*1000.0*a(bpm(:),steerer(:));

[fid,mess]=fopen(filename,'wt');
if fid >0
   fprintf(fid,'\t');
   for j=1:size(bid,1)
      fprintf(fid,'\t%s',bpm_name(bpm(j)));
   end
   fprintf(fid,'\n');
   for j=1:size(bid,2)
      fprintf(fid,'%.4f\t%s',curs(j),steerer_name(steerer(j),plane,slist));
      fprintf(fid,'\t%.1f',bid(:,j));
      fprintf(fid,'\n');
   end
   fclose(fid);
else
   disp(mess)
end

function nm=bpm_name(bpm)
cell=floor((bpm-1)/7);
num=bpm-7*cell;
cell=cell+4;
if cell > 32, cell=cell-32; end
nm=sprintf('C%i-%i',cell,num);

function nm=steerer_name(steerer,plane,list)
cell=floor((steerer-1)/3);
num=steerer-3*cell;
if bitget(cell,1), num=4-num; end
cell=cell+4;
if cell > 32, cell=cell-32; end
nm=sprintf('SR/ST-%c%i/C%i',upper(plane),list(num),cell);
