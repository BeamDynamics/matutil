function optpath(varargin)

here = pwd;
for i=1:nargin
   doit(deblank(varargin{i}))
end
cd(here);

function doit(arg)
if iscell(arg)
   [rows,cols]=size(arg);
   for i=1:rows*cols
      doit(arg{i})
   end
elseif ischar(arg)
   if eval('cd(arg)','0')
      path(path,arg);
   end
else
end
