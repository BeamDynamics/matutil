function pyarray=array2numpy(array,varargin)
%ARRAY2NUMPY Convert a Matlab array to a numpy array
%
%PYARR=ARRAY2NUMPY(MARR) converts a matlab array by keeping the memory
% order : the dimensions of the numpy array are swapped.
%
%PYARR=ARRAY2NUMPY(MARR).T gives a numpy array with the same dimensions,
% but in Fortran order
%
%PYARR=ARRAY2NUMPY(MARR') gives a numpy array with the same dimensions, in
% C order

pyarray=py.numpy.asarray(array(:)',pyargs(varargin{:}));
if ~isvector(array)
    shape=uint32(size(array));
    pyarray=pyarray.reshape(shape(end:-1:1));
end
end
