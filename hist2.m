function [varargout]=hist2(y,varargin)
% plot histogram with mean and std. deviations printed
% See "hist" for details

[varargout{1:nargout}]=hist(y,varargin{:});
v=axis;
text(0.9*v(1)+0.1*v(2),v(3)+0.9*v(4),['std. dev. ' num2str(std(y))]);
text(0.9*v(1)+0.1*v(2),v(3)+0.85*v(4),['average   ' num2str(mean(y))]);
