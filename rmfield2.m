function s2 = rmfield2(s1,fieldnames)
%RMFIELD Remove fields from a structure array.
%   S = RMFIELD(S,'field')
%   S = RMFIELD(S,FIELDS)
%	removes the specified field(s) from S if they exist. It ignores
%   non-existing fields.
%   S must be a 1-by-1 structure.
%
%   See also RMFIELD.

if ischar(fieldnames)
    fieldnames=cellstr(fieldnames);
end
ok=cellfun(@(fn) isfield(s1,fn), fieldnames);
s2=rmfield(s1,fieldnames(ok));
end
