function nax=ppplot(varargin)
%PPPLOT	Adapt a plot for PowerPoint presentation
%
%PPPLOT()       Acts on all axes on the current figure
%
%PPPLOT(FIGURE) Acts on all axes on the specified figure
%
%PPPLOT(AXIS)	Acts on the specified axis
%
%PPPLOT(...,[FONTSIZE LINEWIDTH])
%               Uses the specified values
%
%AXES=PPPLOT(...)   Returns the modified axes

narg=1;
axs=findobj(gcf,'Type','axes');
[axsize,linewidth]=deal(14,1.25);
if narg<=nargin && isscalar(varargin{narg}) && ishandle(varargin{narg})
    switch get(varargin{narg},'Type')
        case 'axes'
            axs=varargin{narg};
        case 'figure'
            axs=findobj(varargin{narg},'Type','axes');
    end
    narg=narg+1;
end
if narg<=nargin
    v=num2cell(varargin{narg});
    [axsize,linewidth]=deal(v{:});
end

labsize=1.1*axsize;
for ax=axs'
    v=get(ax,{'Title','Xlabel','Ylabel'});
    hlab=cat(1,v{:},legend(ax));
    htxt=[ax;findobj(ax,'Type','Text')];
    set(htxt,'FontUnits','points','FontSize',axsize);
    set(hlab,'FontUnits','points','FontSize',labsize);
    set(findobj(ax,'Type','line'),'LineWidth',linewidth);
    set(findobj(ax,'Type','ErrorBar'),'LineWidth',linewidth);
    %set(findobj(ax,'Type','patch'),'LineStyle','none');
    if nargout>=1, nax=axs; end
end
