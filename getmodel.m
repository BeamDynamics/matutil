function [model,args] = getmodel(varargin)
%GETATMACH		% factory function to build a ring model
%
%[MODEL,ARGS]=GETMODEL(VARARGIN) scans the input arguments looking for:
%
%- 'sr'|'sy'|'ebs'
%           machine name, path defaults to $(APPHOME)/mach/optics/settings/theory
%- ['sr'|'sy'|'ebs',]'/machfs/appdata/sr/optics/settings/theory':
%           full path of optics directory
%- ['sr'|'sy'|'ebs',]'nombz2.5'
%           machine and optics name
%- ['sr'|'sy'|'ebs',]atstrcut
%           machine and Accelerator Toolbox structure
%
%MODEL:	Resulting model (subclass of atmodel)
%ARGS:	remaining arguments

if nargin >= 1 && isa(varargin{1},'atmodel')
    model=varargin{1};
    args=varargin(2:end);
else
    [at,args,mach]=getmodelat(varargin{:});
    cls=selectplane(mach,'choices',{'sr','sy','ebs'},{@sr.model,@sy.model,@ebs.model});
    model=cls(at);
end

end
