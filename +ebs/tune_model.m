function [quadmat]=tune_model(varargin)
%RESON_MODEL	produces tune correction matrix for
%Tango://ebs-simu:10000/srdiag/beam-tune/adjust
%	[quadmat]=tune_model()
%
%RESON_MODEL()               takes the lattice from
%           $APPHOME/ebs/optics/settings/theory/betamodel.mat
%RESON_MODEL('opticsname')   takes the lattice from
%           $APPHOME/ebs/optics/settings/opticsname/betamodel.mat
%RESON_MODEL(path)           takes the lattice from
%           path or path/betamodel.mat or path/betamodel.str
%RESON_MODEL(AT)             Uses the given AT structure
%

args={pwd};
args(1:length(varargin))=varargin;
pth=args{1};

atmod=ebs.model(pth);

quadmat=ebs.tuneresponse(atmod);%,'Matching');

end
