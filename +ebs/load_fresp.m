function [oh,ov]=load_fresp(filename,pm)
%LOAD_FRESP    get measured non-normalized dispersion (m/Hz)
%
%[DX,DZ]=LOAD_FRESP(DIRNAME)
%
%DIRNAME:  directory containing files "steerF2[HV]00"
%
%[DX,DZ]=LOAD_FRESP(FILENAME,PARAMS)
%
%FILENAME:  orbit reference file name or
%           directory containing an orbit file named "dispersion"
%PARAMS:    structure containing fields "alpha" and "ll" 
%           ( for example PARAMS=ebs.model() )
%
%DX: horizontal dispersion [m/Hz]
%DZ: vertical dispersion [m/Hz]
%
%See also: ebs.load_disp

if nargin<2
    [~,cur,rsp]=ebs.load_steerresp(fullfile(filename,'steerF2H000'), @(x)0);
    oh=rsp/cur;
    [~,cur,rsp]=ebs.load_steerresp(fullfile(filename,'steerF2V000'), @(x)0);
    ov=rsp/cur;
else
    cnst=-pm.ll/pm.alpha/992/2.997924E8;
    if isdir(filename)
        if exist(fullfile(filename, 'dispersion'),'file') ~= 0
            [dh,dv]=ebs.load_orbit(fullfile(filename, 'dispersion'));
            oh=cnst*dh;
            ov=cnst*dv;
        else
            [oh,ov]=ebs.load_fresp(filename);
        end
    else
        [dh,dv]=ebs.load_orbit(filename);
        oh=cnst*dh;
        ov=cnst*dv;
    end
end
end
