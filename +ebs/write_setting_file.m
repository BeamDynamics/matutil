function [path_to_file] = write_setting_file(maggroup,strengths,varargin)
%EBS.write_setting_file    
%   Create files that may be loaded by the Setting Manager
%
%[path_to_file]=EBS.write_setting_file(maggroup)
%
%EBS.write_setting_file(maggroup)
%   writes a setting manager compliant file.
%
% MAGGROUP: a setting manger device group
%           'quad',
%           'sextu',
%           'octu',
%           'families'(default),
%           'steerer',
%           'skew' (string)
%           
%           (future development: cell array of tango device names)
%
% STRENGTHS: vector of strenghts of the correct size.
% 
% OPTIONAL: (...,'name',value,'name',value,....)
% 'filename' (default: CreatedSetting(MAGGROUP))
% 'author'  (defualt: '')
% 'copy_to_settingmanger' true/false(default)
%
%
%see also:

hostname=['Tango://ebs-simu:10000/'];

% parse inputs
p=inputParser;

addRequired(p,'maggroup',@ischar);
addRequired(p,'strengths',@isnumeric);
addParameter(p,'filename',['CreatedSetting' maggroup],@ischar);
addParameter(p,'author','',@ischar);
addParameter(p,'copy_to_settingmanger',false,@islogical);

parse(p,maggroup,strengths,varargin{:});

filename=p.Results.filename;
author=p.Results.author;
copy_to_settingmanger=p.Results.copy_to_settingmanger;

% get setting manager device
setmanagerdev=tango.Device([hostname 'sys/settings/m-' maggroup]);

% get device list
devlist=setmanagerdev.DefaultAttributes.read;

% check if length of strengths is ok.
NDev = length(devlist);
NStr = length(strengths);

if NDev~=NStr
    error(['found ' num2str(NStr) ' for ' nu2str(NDev) ' devices in ' maggroup ]);
end

% write file
[fp,message]=fopen(filename,'wt');
fprintf(fp,['# '  ' \n']);
fprintf(fp,['# ' filename ' \n']);
fprintf(fp,['# '  ' \n']);
fprintf(fp,['# ' author ' \n']);
fprintf(fp,['# ' datestr(now) ' \n']);
fprintf(fp,['# '  ' \n']);

if fp < 0
    error('File:Open','%s: %s',message,filename);
else
    for dev=1:NDev
        fprintf(fp,'%s\t%.6f\n',devlist{dev},strengths(dev));
    end
    fclose(fp);
end

% copy to setting manager folder to be loaded
if copy_to_settingmanger
    
    [~,n,e]=fileparts(filename);
    
    try
        copyfile(filename,fullfile(setmanagerdev.SettingsPath.read,[n e]));
    catch errormsg
        disp(errormsg)
    end
    
end

path_to_file=which(filename);


end

