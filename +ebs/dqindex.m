function [idx]=dqindex(varargin)
%DQINDEX	find index of quadrupole from its name
%
%IDX=DQINDEX(QPNAME)
%   DQNAME:    string, char array or cell array of strings
%
%IDX=DQINDEX(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:17
%   CELL and ID must have the same dimensions or be a scalar
%
%
%   See also EBS.DQNAME

persistent sl 
if isempty(sl) 
    sl={'dq1b','dq2c','dq1d'};
end

[idx]=ebs.anyindex(repmat(3,1,32),@dqindex,varargin{:});

    function [cellnum,id]=dqindex(bname)
        qpattern='(?:^srmag/m-|^m-|^)(?<quadname>dq[12])/c(?<cellname>\d{1,2})-(?<celpos>[bcd])$';
        toks=regexpi(bname,qpattern,'names');
        if isempty(toks),error('DipoleQUAD:wrongName','Wrong dipole quadrupole name'); end
        cellnum=str2double(toks.cellname);
        
        id=find(strcmp([toks.quadname toks.celpos],sl));
        
        if isempty(id), error('DipoleQUAD:wrongName','Wrong dipole quadrupole name'); end
    end
end

