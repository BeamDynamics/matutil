function varargout=dqname(idx,format)
%EBS.DQNAME	returns the name of a dipole quadrupole with index idx
%
%QPNAME=QPNAME(IDX)	returns the name of Quadrupole with index IDX
% IDX: vector of indexes in the range [1 96] (1 is C4-1)
%       or n x 2 matrix in the form [cell index] with
%       cell in [1 32], index in [1 3]
%
%QPNAME=DQNAME(IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : 'srmag/m-******')
%
%[NM1,NM2...]=DQNAME(...)    May be used to generate several names
%
%[...,KDX]=DQNAME(...)       returns in addition the "hardware" index KDX
%
%KDX=DQNAME(IDX,'')          returns only the "hardware" index KDX
% 
% examples : 
% >> ebs.dqname(96)
% >> ebs.dqname([23 4 11]')
%
%   See also ebs.DQINDEX

if nargin < 2, format='srmag/m-dq'; end

%[kdx,celnum,id]=ebs.cellnum([17,repmat(16,1,30),17],idx);
[kdx,celnum,id]=ebs.cellnum(repmat(3,1,32),idx);


if isempty(format) || strcmp(format,'noname')
    varargout={};
else
    
    devnamefun= @(celnum,id)textfunc(format,celnum,id);
   
    nms=arrayfun(devnamefun,celnum,id,'UniformOutput',false);
    nin=length(nms);
    if (nargout==nin) || (nargout==(nin+1))
        varargout=nms;
    else
        varargout={nms};
    end
end

nout=length(varargout);
if nargout > nout
    varargout{nout+1}=kdx;
end

    function devnam=textfunc(format,celnum,id)
        num='121';
        pos='bcd';
        
        devnam=[format num(id) '/c' num2str(celnum,'%.2d') '-' pos(id)];
        
    end

end
