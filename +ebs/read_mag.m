function [strength,devlist] = read_mag(varargin)
%EBS.READ_MAG    Read the values of magnet families from the control system
%
%CURRENT=EBS.READ_MAG()
%   Return the family currents [A]
%
%CURRENT=EBS.READ_MAG(MAGLIST)
%   Return the family currents for the selected magnets [A]
%
%MAGLIST:   List of magnet family names
%
%[...,DEVNAME]=EBS.READ_MAG(...)
%   Return in addition the device names

devlist=getargs(varargin,{ebs.magname()});
strength=cellfun(@(d)tango.Attribute(d).setpoint('',NaN),devlist,'un',0);

end
