function [bumpsH,bumpsV]=orbitbumps_model(varargin)
%ORBITBUMPS_MODEL Creates data files for the orbit bumps device server
%
%[bumpsH,bumpsV]=EBS.ORBITBUMPS_MODEL(PATH)
%PATH:  Data directory (Default: pwd)
%

args={'theory'};
%args={pwd};
[exper,options]=getflag(varargin,'exp');
args(1:length(options))=options;
pth=args{1};
if exper, rmpth=args{2}; end

atmodel=ebs.model(pth);
[bpmidx,sthidx,stvidx]=atmodel.select('bpm','steerh','steerv');
r=atmodel.ring;

% initialize KickAngle
r = atsetfieldvalues(r,sthidx,'KickAngle',{1,1},0);
r = atsetfieldvalues(r,sthidx,'KickAngle',{1,2},0);
r = atsetfieldvalues(r,stvidx,'KickAngle',{1,1},0);
r = atsetfieldvalues(r,stvidx,'KickAngle',{1,2},0);

% % compute orbit Response Matrix
% fprintf('Computing horizontal response\n');
% h_resp=-ebs.orm(r,'h',sthidx,bpmidx);
% fprintf('Computing vertical response\n');
% v_resp=-ebs.orm(r,'v',stvidx,bpmidx);

bpmidx = find(bpmidx);
sthidx = find(sthidx);
stvidx = find(stvidx);

% correctors list (columns, 1-9) per BPM (row, 1-10) in cell
% bump chose to minimize strengths required (more efficent)
% cor 0 refers to last of previous cell
% cor 10 first of next cell

H_bpm_cor=[...
    0 1 3;... 1
    1 2 4;... 2
    1 3 5;... 3
    4 5 6;... 4
    3 5 7;... 5
    3 5 7;... 6
    4 5 6;... 7 
    5 7 9;... 8
    6 8 9;... 9 
    8 9 10;... 10
    ];

V_bpm_cor=[...
    0 1 4;... 1
    1 2 4;... 2
    1 3 5;... 3
    4 5 6;... 4
    4 5 6;... 5
    4 5 6;... 6
    4 5 6;... 7
    5 7 9;... 8
    6 8 9;... 9
    7 9 10;... 10
    ];


cellnum=repmat(1:32,10,1);
cellnum=cellnum(:);

cor_for_bump_ind = mod(1:length(bpmidx),10);
cor_for_bump_ind(cor_for_bump_ind==0)=10;

AllHCor=H_bpm_cor(cor_for_bump_ind,:) + repmat((cellnum-1)*9,1,3);
AllVCor=V_bpm_cor(cor_for_bump_ind,:) + repmat((cellnum-1)*9,1,3);
AllHCor(AllHCor==0)=288;
AllHCor(AllHCor==289)=1;
AllVCor(AllVCor==0)=288;
AllVCor(AllVCor==289)=1;


% compute bumps
bumph=1e-4;
bumpv=1e-4;

% initialize file strings
hr={};
vr={};

% open file
fidh = fopen('h_bump.txt', 'w+');
fidv = fopen('v_bump.txt', 'w+');

% display computed bumps
figure;
s=findspos(r,bpmidx);

for ib = 1:length(bpmidx)
    
  
    % COMPUTE BUMP
    refx = zeros(size(bpmidx))';
    refx(ib) = bumph;
    refy = zeros(size(bpmidx))';
    refy(ib) = bumpv;
    
    % select BPM for bump, 
    %selbpm = AllBpm(ib,:) ;
    refx=[0 0 0 0 0 bumph 0 0 0 0 0];
    refy=[0 0 0 0 0 bumpv 0 0 0 0 0];
    
    refbumppos = [...
        sthidx(min(AllHCor(ib,1),AllVCor(ib,1)))-50 , ...
        sthidx(min(AllHCor(ib,1),AllVCor(ib,1)))-10 , ...
        sthidx(min(AllHCor(ib,1),AllVCor(ib,1)))-5 , ...
        sthidx(min(AllHCor(ib,1),AllVCor(ib,1)))-4 , ...
        sthidx(min(AllHCor(ib,1),AllVCor(ib,1)))-2 , ...
        bpmidx(ib) ,...
        sthidx(max(AllHCor(ib,3),AllVCor(ib,3)))+2 , ...
        sthidx(max(AllHCor(ib,3),AllVCor(ib,3)))+4, ...
        sthidx(max(AllHCor(ib,3),AllVCor(ib,3)))+5 , ...
        sthidx(max(AllHCor(ib,3),AllVCor(ib,3)))+10 , ...
        sthidx(max(AllHCor(ib,3),AllVCor(ib,3)))+50 , ...
        ]; % close bump just before/after correctors
    
    belowlen=refbumppos<0;
    refbumppos(belowlen) = length(r)+refbumppos(belowlen);
    
    overlen=refbumppos>length(r);
    refbumppos(overlen) = -length(r)+refbumppos(overlen);
    
    % sort BPM indexes (findorbit6 accepts only increasing BPM indexes)
   [refbumppossort,ordbumppos]=sort(refbumppos);
   
    % the steerers to use
    selcorh = AllHCor(ib,:) ;
    selcorv = AllVCor(ib,:) ;
    
    % initial closed orbit guess
    inCOD =zeros(6,1);
%     if ib == 1
%         
%         % first BPM, initial COD ~! 0
%         inCOD(1)=bumph;
%         inCOD(3)=bumpv;
% 
%     end
    
    disp('ref. positions')
    disp(refbumppos)
    disp('correctors used')
    disp(sthidx(selcorh)')
    disp(stvidx(selcorv)')
   
    % no radiation, no RF
    r=atsetcavity(r,0.0001e6,0,992);
    
    % invert matrix 
    [rbump,inCOD,hs,vs]=atcorrectorbit(...
        r,...    lattice
        refbumppossort,...  % BPMS to use
        sthidx(selcorh),... % horizontal steerers to use
        stvidx(selcorv),... % vertical steerers to use
        inCOD,...           % initial closed orbit
        repmat([3 3],5,1),... % correction iterations and eigenvectors 
        [false false false],...   % no energy, no average zero, rad
        1.0,...             % scale factor = 100%
        [],... ModelRMbump,...     % response matrix
        [refx(ordbumppos);...   % reference orbit (wished bump)
         refy(ordbumppos)],...  %
        [],...              % steerers Limit
        true);              % display output
    
    % read orbit
    o=findorbit4(rbump,0,bpmidx,inCOD);
    
    % display orbit (for debug)
    subplot(2,1,1)
    plot(s,o(1,:),'DisplayName',ebs.bpmname(ib)); hold on;
    plot(s(ib),bumph,'ro');
    subplot(2,1,2)
    plot(s,o(3,:),'DisplayName',ebs.bpmname(ib)); hold on;
    plot(s(ib),bumpv,'ro');
    drawnow;
    
    % CREATE FILE 
    % that contains 1 line for each bump:
    % Device1 strength/1m Device2 strength/1m .. DeviceN strength/1m
    % Ind_first_bpm N_bpms_concerned Amplitude_weigth_1 Amplitude_weigth_2 ..
    % Amplitude_weigth_N_bpms_concerned
    %
    % example
    % sr/hst-sd1a/C1 100 sr/hst-sf2/C1 -200 sr/hst-sd1b/C1 100  32 5 0 0.5 1 0.5 0
    
    % get bump location
    
	initbumph = find(bpmidx<sthidx(AllHCor(ib,1)),1,'last');
    if isempty(initbumph), initbumph = 1; end
    
    endbumph = find(bpmidx>sthidx(AllHCor(ib,3)),1,'first');
    if isempty(endbumph), endbumph = 320; end
    
    if initbumph < endbumph
        bumpindexesh=[initbumph:endbumph];
    elseif initbumph > endbumph
        bumpindexesh=[initbumph:length(bpmidx),1:endbumph];
    end
    NbumpBumpH = length(bumpindexesh);
    % initbumph
    % endbumph
    % NbumpBumpH
    
    % build string for Tango Device property definition
    hr{ib} = [....
        ebs.steername(AllHCor(ib,1),'srmag/hst-') ... steerer name
        ' ' num2str(hs(1)./ bumph) ... steerer force / bump amplitude
        ' ' ebs.steername(AllHCor(ib,2),'srmag/hst-') ...steerer name
        ' ' num2str(hs(2) ./ bumph) ... steerer force / bump amplitude
        ' ' ebs.steername(AllHCor(ib,3),'srmag/hst-') ...steerer name
        ' ' num2str(hs(3) ./ bumph) ... steerer force / bump amplitude
        ' ' num2str(initbumph-1) ... first BPM within bump [0-319]
        ' ' num2str(NbumpBumpH) ... # of BPM involved
        ' ' num2str(o(1,bumpindexesh)./bumph) ...  amplitude at bpm / bump amplitude
        ''];
    
    
    initbumpv = find(bpmidx<stvidx(AllVCor(ib,1)),1,'last');
    if isempty(initbumpv), initbumpv = 1; end
    
    endbumpv = find(bpmidx>stvidx(AllVCor(ib,3)),1,'first');
    if isempty(endbumpv), endbumpv = 320; end
    
    if initbumpv < endbumpv
        bumpindexesv=[initbumpv:endbumpv];
    elseif initbumpv > endbumpv
        bumpindexesv=[initbumpv:length(bpmidx),1:endbumpv];
    end
    NbumpBumpV = length(bumpindexesv);
    
    
    vr{ib} = [...
        ebs.steername(AllVCor(ib,1),'srmag/vst-') ... steerer name
        ' ' num2str(vs(1)./ bumpv) ... steerer force / bump amplitude
        ' ' ebs.steername(AllVCor(ib,2),'srmag/vst-') ...steerer name
        ' ' num2str(vs(2) ./ bumpv) ... steerer force / bump amplitude
        ' ' ebs.steername(AllVCor(ib,3),'srmag/vst-') ...steerer name
        ' ' num2str(vs(3) ./ bumpv) ... steerer force / bump amplitude
        ' ' num2str(initbumpv-1) ... first BPM within bump [0-319]
        ' ' num2str(NbumpBumpV) ... # of BPM involved
        ' ' num2str(o(3,bumpindexesv)./bumpv) ...  amplitude at bpm / bump amplitude
        ''];
    
   
    fprintf(fidh, '%s\n', hr{ib});
    fprintf(fidv, '%s\n', vr{ib});

end

fclose(fidh);
fclose(fidv);

end

