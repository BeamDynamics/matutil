function [idx,gradient,resp,algo]=get_steerresp(inpt,decodef)
%GET_STEERRESP(INPUT)    get response of a single steerer
%
%[IDX,EXCITATION,RESPONSE,ALGORITHM]=GET_STEERRESP(INPUT,DECODEF)
%
%INPUT:   tab delimited text line
%DECODEF: function to decode steerer name into index
%         (default: ebs.steerindex())
%
%IDX:       Steerer index
%EXCITATION:Steerer gradient [rad]
%RESPONSE:	BPM response [m]
%ALGORITHM:	BPM algorithm (0 or 1)

if nargin<2, decodef=@ebs.steerindex; end
% [s1,s2,strs]=celldeal(strsplit(inpt,'\t','CollapseDelimiters',false));
% [sname,salgo,~]=celldeal(strsplit(s2,',','CollapseDelimiters',false));
[s1,s2,strs]=celldeal(regexp(inpt,'\t','split'));
[sname,salgo,~]=celldeal(regexp(s2,',','split'));
gradient=str2double(s1);
idx=decodef(sname);
resp=1.0E-3*str2double(strs');	% convert mm to m
algo=str2double(salgo);
if isnan(algo), algo=1; end
end
