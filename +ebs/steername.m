function varargout=steername(idx,format)
%STEERNAME	returns the name of Steerer with index idx
%
%STEERNAME=STEERNAME(IDX)		returns the name of steerer with index IDX
% IDX: vector of indexes in the range 1:288 (1 is (h|v)st-sh1/c04-a)
%       logical array of length 
%       or n x 2 matrix in the form [cell index] with
%       or 1 x n array for the names of n magnets
%       cell in 1:32, index in 1:9
%
%STEERNAME=STEERNAME(IDX,FORMAT)	uses FORMAT to generate the name
%					default : '', 
%                   'hst-' to obtain horizontal steerers
%                   'vst-' to obtain vertical steerers
%                   'sqp-' to obtain skew quadrupoles
%                   
%[NM1,NM2...]=STEERNAME(...)		May be used to generate several names
%
% for horizontal steerers names
% ebs.steername([1,9,103],'hst-')% array
% ebs.steername([19,4],'hst-')% cell idx
% 
% for vertical steerers names
% ebs.steername([1,23,59],'vst-') % array
% ebs.steername([23,1],'hst-') % cell idx
%  
% for skew quadrupole names
% ebs.steername([1,23,59],'sqp-') % array
% ebs.steername([23,1],'sqp-') % cell idx
% 
%   See also EBS.STEERINDEX, EBS.QPNAME, EBS.BPMNAME, EBS.SXNAME

persistent fmt
if isempty(fmt)
fmt={'sh1/c%02d-a','sd1/c%02d-a','sf2/c%02d-a','sd1/c%02d-b','sh2/c%02d-b',...
    'sd1/c%02d-d','sf2/c%02d-e','sd1/c%02d-e','sh3/c%02d-e'};
end

if nargin < 2, format=''; end

[~,celnum,id]=ebs.cellnum(9,idx);
textfunc=@(cellnum,id) sprintf([format fmt{id}],cellnum);
nms=arrayfun(textfunc,celnum,id,'UniformOutput',false);
if nargout==length(nms)
    varargout=nms;
else
    varargout={nms};
end
end
