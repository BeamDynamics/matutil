function [resp,devs]=chromresponse(atmodel)

% get initial tunes

[~,~,Q0]=atlinopt(atmodel.ring,0,1);

chrommode='SF2SD1';
%chrommode='InjSext';

Mat=[];

% change horizontal chromaticity
DQ=.1;

Q1=Q0+[DQ 0.0];

latnewchrom=ebs.model(atmodel);
latnewchrom.setchrom(Q1,chrommode);
DGL=latnewchrom.get(1,'sx') - atmodel.get(1,'sx');

Mat(:,1)=DGL/DQ;

% change vertical chromaticity

Q1=Q0+[0.0 DQ];

latnewchrom=ebs.model(atmodel);
latnewchrom.setchrom(Q1,chrommode);
DGL=latnewchrom.get(1,'sx') - atmodel.get(1,'sx');

Mat(:,2)=DGL/DQ;

% select changed gradients and get device familiy names.
[uMat,iq,~]=unique(Mat,'rows');
selnonzero=uMat(:,1)~=0 & uMat(:,2)~=0;
devs=ebs.sxfamname(iq(selnonzero));
resp=[uMat(selnonzero,1),uMat(selnonzero,2)];


% save file .csv

fff=fopen('chrommat.csv','w+');
for iq=1:length(devs)
    fprintf(fff,'%s,%2.10f,%2.10f,\n',devs{iq},resp(iq,1),resp(iq,2));
end
fclose(fff);

end
