function [idx]=ocindex(varargin)
%OCINDEX	find index of sextupole from its name
%
%IDX=OCINDEX(OCNAME)
%   OCNAME:    string, char array or cell array of strings
%
%IDX=OCINDEX(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:7
%   CELL and ID must have the same dimensions or be a scalar
%
%   See also SR.OCNAME

persistent sl
if isempty(sl)
    sl={'of1b','of1d'};
end

[idx]=ebs.anyindex(2,@bindex,varargin{:});

    function [cellnum,id]=bindex(bname)       
        qpattern='(?:^srmag/m-|^m-|^)(?<sextname>o[f][1])/c(?<cellname>\d{1,2})-(?<celpos>[bd])$';
        toks=regexpi(bname,qpattern,'names');
        if isempty(toks),error('SX:wrongName','Wrong octupole name'); end
        cellnum=str2double(toks.cellname);
        id=find(strcmp([toks.sextname toks.celpos],sl));
        if isempty(id), error('SX:wrongName','Wrong octupole name'); end
    end
end

