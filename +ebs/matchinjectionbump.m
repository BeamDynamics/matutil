function [K]=matchinjectionbump(ebsmodel,bumpamplitude,varargin)
% K = mathcinjectionbump(ebsmodel,bumpamplitude,varargin)
%
% Input:
% ebsmodel      : AT lattice
% bumpamplitude : amplitude of bump, vector [1xN].
%
% Optional Input:
% 'plot'        : true/false (default: false), plots bump
% 'verbose'     : 0-3 (default: 0), matching verbosity.
%
% Output:
% K             : [4xN] kicker DR_K3,DR_K4,DR_K1,DR_K2
%
% Examples:
% % match bump at -15 mm
% K=ebs.mathcinjectionbump(r,-15e-3)
% match bump from 0 to 110% of amplitude, with hihger resolution close to
% nominal value and show plot.
% K=ebs.mathcinjectionbump(ebsmodel,[0:0.1:0.9,0.91:0.01:1.1]*(-15e-3),'plot',true)
%
%
%see also: atmatch, ebs.model

p=inputParser;

addRequired(p,'ebsmodel');
addRequired(p,'bumpamplitude');
addParameter(p,'plot',false);
addParameter(p,'verbose',0);

parse(p,ebsmodel,bumpamplitude,varargin{:});

doplot  = p.Results.plot;
verb    = p.Results.verbose;
bumpH   = p.Results.bumpamplitude;

id_ind=find(atgetcells(ebsmodel,'FamName','ID\w*'))';

ainj=[ebsmodel(id_ind(end):end);ebsmodel(1:id_ind(2))];

injpos=length(ebsmodel(id_ind(end):end));

kickers_ind=find(atgetcells(ainj,'FamName','DR_K[1234]'))';

%
Variab=atVariableBuilder(ainj,{kickers_ind([1,4]),kickers_ind([2,3])},{{'PolynomB',{1,1}}});

K=zeros(length(bumpH),length(kickers_ind));

[twiin,~,~]=atlinopt(ainj,0,1);

ainj1=ainj;

if doplot,
    figure;
    indall=1:length(ainj1);
    s=findspos(ainj,indall);
end

for ii=1:length(bumpH)
    
    if verb~=0
        disp(['bump ' num2str(bumpH(ii),'%2.2f') ' mm'])
    end
    
    % orbit and angle at injection
    LinConstr1=atlinconstraint(...
        injpos,...
        {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
        [bumpH(ii),0],...
        [bumpH(ii),0],...
        [1 1]);
    
    % orbit and angle at first ID + 10 elements
    LinConstr2=atlinconstraint(...
        kickers_ind(end)+10,...
        {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
        [0,0],...
        [0,0],...
        [1 1]);
    
    Constr=[LinConstr1,LinConstr2];
    
    % match lattice
    ainj1=atmatch(ainj1,Variab,Constr,10^-12,1000,verb,@lsqnonlin,twiin);%,'fminsearch');%
    
    % get kicker strengths 1/m
    K(ii,:)=atgetfieldvalues(ainj1,kickers_ind([3,4,1,2]),'PolynomB',{1,1}); % order as in ebsmodel.ring
    
    if doplot
        hold on;
        l=twissline(ainj1,0.0,twiin,indall,'chrom');
        ox=arrayfun(@(a)a.ClosedOrbit(1),l);
        plot(s,ox,'DisplayName',['bump ' num2str(bumpH(ii),'%2.2f') ' mm']);
    end
    
end

if doplot
    xlabel('s [m]');ylabel('x_{co} [m]');
    hold on;
    subplot(2,2,4);
    plot(bumpH,K);
    legend('K3','K4','K1','K2');
    xlabel('bump amplitude [m]');ylabel('K [1/m]');
    legend;
end