function [idx]=qpindex(varargin)
%QPINDEX	find index of quadrupole from its name
%
%IDX=QPINDEX(QPNAME)
%   QPNAME:    string, char array or cell array of strings
%
%IDX=QPINDEX(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:17
%   CELL and ID must have the same dimensions or be a scalar
%
%
%   See also SR.QPNAME

persistent sl sl3 sl4
if isempty(sl) || isempty(sl3) || isempty(sl4)
    sl={'qf1a','qd2a','qd3a','qf4a','qf4b','qd5b','qf6b','qf8b','qf8d','qf6d','qd5d','qf4d','qf4e','qd3e','qd2e','qf1e'};
    sl3={'qf1a','qd2a','qd3a','qf4a','qf4b','qd5b','qf6b','qf8b','qf8d','qf6d','qd5d','qf4d','qf4e','qd3e','qf2e','qd2e','qf1e'};
    sl4={'qf1a','qd2a','qf2a','qd3a','qf4a','qf4b','qd5b','qf6b','qf8b','qf8d','qf6d','qd5d','qf4d','qf4e','qd3e','qd2e','qf1e'};
end

[idx]=ebs.anyindex([17,repmat(16,1,30),17],@qindex,varargin{:});

    function [cellnum,id]=qindex(bname)
        qpattern='(?:^srmag/m-|^m-|^)(?<quadname>q[df][1234568])/c(?<cellname>\d{1,2})-(?<celpos>[abde])$';
        toks=regexpi(bname,qpattern,'names');
        if isempty(toks),error('QUAD:wrongName','Wrong quadrupole name'); end
        cellnum=str2double(toks.cellname);
        
        if cellnum~=3 && cellnum~=4
            SL=sl;
        elseif cellnum==3
            SL=sl3;
        elseif cellnum==4
            SL=sl4;
        end
        
        id=find(strcmp([toks.quadname toks.celpos],SL));
        
        if isempty(id), error('QUAD:wrongName','Wrong quadrupole name'); end
    end
end

