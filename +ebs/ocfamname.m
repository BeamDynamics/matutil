function varargout=ocfamname(idx,format)
%EBS.OCFAMNAME	returns the name of Octupole with index idx
%
%SXNAME=OCFAMNAME(IDX)	returns the name of Octupoles with index IDX
% IDX: vector of indexes in the range [1 196] (1 is C4-1)
%       or n x 2 matrix in the form [cell index] with
%       cell in [1 32], index in [1 7]
%
%SXNAME=OCFAMNAME(IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : 'srmag/m-o******')
%
%[NM1,NM2...]=OCFAMNAME(...)    May be used to generate several names
%
%[...,KDX]=OCFAMNAME(...)       returns in addition the "hardware" index KDX
%
%KDX=OCFAMNAME(IDX,'')          returns only the "hardware" index KDX
% 
% examples : 
% >> ebs.OCFAMNAME(320)
%    ans = 
%     'srmag/m-of1/c03-e'
%
%
%   See also EBS.OCINDEX

persistent fmt
if isempty(fmt)
    fmt={'all','all'};
end

if nargin < 2, format='srmag/m-of1/'; end

[~,celnum,id]=ebs.cellnum(2,idx);
textfunc=@(cellnum,id) sprintf([format fmt{id}],cellnum);
nms=arrayfun(textfunc,celnum,id,'UniformOutput',false);
if nargout==length(nms)
    varargout=nms;
else
    varargout={nms};
end
end
