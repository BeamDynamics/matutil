function [injbumpmat]=injbump_model(varargin)
%INJBUMP_MODEL	produces tune correction matrix for
%Tango://ebs-simu:10000/srdiag/beam-tune/adjust
%	[quadmat]=tune_model()
%
%INJBUMP_MODEL()               takes the lattice from
%           $APPHOME/ebs/optics/settings/theory/betamodel.mat
%INJBUMP_MODEL('opticsname')   takes the lattice from
%           $APPHOME/ebs/optics/settings/opticsname/betamodel.mat
%INJBUMP_MODEL(path)           takes the lattice from
%           path or path/betamodel.mat or path/betamodel.str
%INJBUMP_MODEL(AT)             Uses the given AT structure
%

args={pwd};
args(1:length(varargin))=varargin;
pth=args{1};

atmodel=ebs.model(pth);

bampl=[0:0.1:0.9,0.91:0.01:1.1]*(-15e-3);

injbumpmat=ebs.mathcinjectionbump(atmodel.ring,bampl);
dlmwrite('injbumpmat.cvs',[bampl' injbumpmat],....
    'delimiter','\t','precision','%.6f');

end
