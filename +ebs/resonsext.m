function resp=resonsext(atmodel)

sext=atmodel.get('sx');

% % order magnets as in Control System
% idsext=cellfun(@(nm) sr.sxindex(nm),tango.Device('sr/sx-c/all').CorrectorNames.read');
% sext=sext(idsext,:);

nuh=atmodel.nuh;
nuv=atmodel.nuv;
h1=floor(3*nuh);
h3=floor(nuh+2*nuv);
h5=floor(nuh-2*nuv);

ampscale=1; % scale to have maximum resonance amplitude ~= 1

%					response 8x192
resp=[...
    ampscale*r3nux(sext,h1,nuh);...            % 3nux=228
    ampscale*r3nux(sext,h1+1,nuh);...          % 3nux=229
    ampscale*rnuxp2nuz(sext,h3+1,nuh,nuv);...    % nux+2nuz=131
    ampscale*rnuxm2nuz(sext,h5+1,nuh,nuv);...	% nux-2nuz=22
    ];

calr=ones(1,length(sext));

dlmwrite('sextcor.csv',resp.*calr(ones(8,1),:),'precision',10);    % Response for the new device

%figure(4);
%resonplot(resp(1:6,:),[h1 h1+1 h3+1]);


    function exc=r3nux(sext,p,nuxx)
        disp(['3*nuX = ' num2str(p)]);
        n=(3*nuxx-p)/3;
        nx=nuxx-n;
        phase=2*pi*(3*nx*sext(:,3)+(p-3*nx)*sext(:,1));
%         phase=2*pi*(3*nuxx*sext(:,3)-p*sext(:,1));
        ampl=sext(:,2).^1.5;
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

    function exc=rnuxp2nuz(sext,p,nuxx,nuzz)
        disp(['nuX + 2 nuZ = ' num2str(p)]);
        m=(nuxx+2*nuzz-p)/3;
        nx=nuxx-m;
        nz=nuzz-m;
        phase=2*pi*(nx*sext(:,3)+2*nz*sext(:,6)+(p-(nx+2*nz))*sext(:,1));
%         phase=2*pi*(nuxx*sext(:,3)+2*nuzz*sext(:,6)-p*sext(:,1));
        ampl=sqrt(sext(:,2)).*sext(:,5);
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

    function exc=rnuxm2nuz(sext,p,nuxx,nuzz)
        disp(['nuX - 2 nuZ = ' num2str(p)]);
        m=(nuxx-2*nuzz-p)/3;    % Dissonance
        nx=nuxx-m;
        nz=nuzz+m;
        phase=2*pi*(nx*sext(:,3)-2*nz*sext(:,6)+(p-(nx-2*nz))*sext(:,1));
%         phase=2*pi*(nuxx*sext(:,3)-2*nuzz*sext(:,6)-p*sext(:,1));
        ampl=sqrt(sext(:,2)).*sext(:,5);
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

end
