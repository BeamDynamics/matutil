function varargout=pinholename(idx)
%PINHOLENAME	returns the name of IAX with index idx
%
%NAME=PINHOLENAME(CELL)	returns the name of Pinhole with index IDX
%  IDX:     vector of indexes in the range 1:32 (1 is C4)
%
%[NM1,NM2...]=PINHOLENAME(...)	May be used to generate several names
%
%   See also 

persistent npincels ff
if isempty(npincels)
    npincels=zeros(1,32);
    ff=cell(1,32);
    ff(ebs.model.pinhole_id)={'id%.2d'};
    ff(ebs.model.pinhole_bm)={'d%.2d'};
    npincels(ebs.model.pinhole_id)=1;
    npincels(ebs.model.pinhole_bm)=1;
    npincels=circshift(npincels,[0 -3]);
end

if nargin < 2, format='srdiag/emittance/'; end
 
[~,celnum,~]=ebs.cellnum(npincels,idx);

nms=arrayfun(@(cellnum)textfunc(cellnum),celnum,'UniformOutput',false);
if nargout==length(nms)
    varargout=nms;
else
    varargout={nms};
end

    function s=textfunc(cn)
        s=sprintf([format ff{cn}], cn);
    end

end


