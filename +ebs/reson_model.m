function [qexc,kexc,sexc]=reson_model(varargin)
%RESON_MODEL	produces resonance correction files for skmag
%	[qexc,kexc,sexc]=reson_model()
%
%RESON_MODEL()               takes the lattice from the current directory
%
%RESON_MODEL('opticsname')   takes the lattice from
%           $APPHOME/ebs/optics/settings/opticsname/betamodel.mat
%RESON_MODEL(path)           takes the lattice from
%           path or path/betamodel.mat or path/betamodel.str
%RESON_MODEL(AT)             Uses the given AT structure
%

args={pwd};
args(1:length(varargin))=varargin;

atmodel=ebs.model(args{:});

qexc=ebs.resonquad(atmodel);
kexc=ebs.resonskew(atmodel);
sexc=ebs.resonsext(atmodel);
%oexc=ebs.resonoctu(atmodel);

end
