function [sextmat]=chrom_model(varargin)
%chrom_model	produces tune correction matrix for
%Tango://ebs-simu:10000/srdiag/beam-chrom/adjust
%	[quadmat]=chrom_model()
%
%chrom_model()               takes the lattice from
%           $APPHOME/ebs/optics/settings/theory/betamodel.mat
%chrom_model('opticsname')   takes the lattice from
%           $APPHOME/ebs/optics/settings/opticsname/betamodel.mat
%chrom_model(path)           takes the lattice from
%           path or path/betamodel.mat or path/betamodel.str
%chrom_model(AT)             Uses the given AT structure
%

args={pwd};
args(1:length(varargin))=varargin;
pth=args{1};

atmodel=ebs.model(pth);

sextmat=ebs.chromresponse(atmodel);

end
