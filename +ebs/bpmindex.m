function [idx,kdx]=bpmindex(varargin)
%EBS.BPMINDEX	find index of BPM from its name
%
%IDX=BPMINDEX(BPMNAME)
%   BPMNAME:    string, char array or cell array of strings
%
%IDX=BPMINDEX(KDX)
%   KDX:        "hardware" index of the BPM
%
%IDX=BPMINDEX(CELL,ID)
%   CELL:   cell number in [1 32]
%   ID:     index in cell, in [1 7]
%   CELL and ID must have the same dimensions or be a scalar
%
%[IDX,KDX]=BPMINDEX(...)	returns in addition the "hardware" index
%
% examples:
% >> ebs.bpmindex('srdiag/beam-position/c03-10')
% >> ebs.bpmindex({'srdiag/beam-position/c03-10','srdiag/beam-position/c04-01'})
%
%   See also BPMNAME, QPINDEX, SXINDEX

[idx,kdx]=ebs.anyindex(10,@bindex,varargin{:});

    function [cellnum,id]=bindex(bname)
        bpattern='^(?:(srdiag/)?beam-position/)?c(\d{1,2})-(\d{1,2})$';
        toks=regexpi(bname,bpattern,'tokens');
        if isempty(toks),error('BPM:wrongName','Wrong BPM name'); end
        cellnum=str2double(toks{1}{1});
        id=str2double(toks{1}{2});
    end
end
