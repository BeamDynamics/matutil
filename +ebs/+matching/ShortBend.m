function [ r, arc ] = ShortBend( r,idnum,varargin )
%ShortBend inserts a Shor Bend in ring 'r' at cell 'idnum'
%
%Inputs:
% r: EBS upgrade lattice
% idnum: ID number where the SB is placed
%
%Optional Inputs:
% AngSB: SB angle (defaut 2.09988e-3 rad)
% ploton: prints out plots, true/false (default)
% verbose: prints matching process true/false (default)
%
%Output:
% r: EBS upgrade lattice with SB at id idnum
% arc : cell with SB
%
%Description:
% SB inserted in DR_25
% DQ angles changed to match survey
% QF4 rematched to fix residual dispersion
%
%Example:
% >> [rsb,arcsb]=ebs.matching.ShortBend(LOW_EMIT_RING_INJ,5,'ploton',true)
%
%Warning:
% this function is not supposed to be used inside ebs.model, as it changes
% the number of elements in the lattice
%
%see also:


% 2PW parameters
LSB=0.0488588; %  (0.063+0.038)/2; % m
AngSB_def=2.09988e-3;% rad +2.122e-3; % rad
Dist_sourcepoint_fromQF8=0.10825;

p=inputParser;
addRequired(p,'r',@iscell);
addRequired(p,'idnum',@isnumeric);
addOptional(p,'AngSB',AngSB_def,@isnumeric);
addOptional(p,'ploton',false,@islogical);
addOptional(p,'verbose',false,@islogical);

parse(p,r,idnum,varargin{:});

AngSB=p.Results.AngSB;
ploton=p.Results.ploton;
verbose=p.Results.verbose;

% get cell
[cellstartindex,cellendindex]=ebs.getcellstartend(r,idnum);

ARCA=r(cellstartindex:cellendindex);

% remove eventual BMHARD markers
ARCA(atgetcells(ARCA,'FamName','BMHARD'))=[];

dr25ind=findcells(ARCA,'FamName','DR_25\w*');
ARCA{dr25ind(1)}.Length= ARCA{dr25ind(1)}.Length +ARCA{dr25ind(2)}.Length;
ARCA(dr25ind(2))=[];
dr25ind=findcells(ARCA,'FamName','DR_25\w*');

% add Short Bend at correct locations
welems={...
    atdrift('DMB',ARCA{dr25ind}.Length-Dist_sourcepoint_fromQF8-LSB/2);...
    atsbend('SBM',LSB/2,AngSB/2,0,'BndMPoleSymplectic4Pass');...
    atmarker(['SBM' '-BM' num2str(idnum,'%.2d')]);...
    atsbend('SBM',LSB/2,AngSB/2,0,'BndMPoleSymplectic4Pass');...
    atdrift('DMB',Dist_sourcepoint_fromQF8-LSB/2);...
    };

dr25ind=findcells(ARCA,'FamName','DR_25\w*');

ARCAW=[...
    ARCA(1:dr25ind-1);...
    welems;...
    ARCA((dr25ind+1):end);...
    ];

arc0=ARCAW;
arc0SB=ARCAW;
arc0=atsetfieldvalues(arc0,atgetcells(arc0,'FamName','SBM'),'BendingAngle',0); % no minibend

% remove ~1mrad from DQ2C and DQ1B to keep total angle constant
i1=find(atgetcells(ARCAW,'FamName','\w*DQ2C\w*'));
i2=find(atgetcells(ARCAW,'FamName','\w*DQ1D\w*'));
i3=find(atgetcells(ARCAW,'FamName','\w*DQ1B\w*'));
b1=atgetfieldvalues(ARCAW,i1,'BendingAngle',{1,1});
b2=atgetfieldvalues(ARCAW,i2,'BendingAngle',{1,1});
b3=atgetfieldvalues(ARCAW,i3,'BendingAngle',{1,1});
l1=atgetfieldvalues(ARCAW,i1,'Length',{1,1});
l2=atgetfieldvalues(ARCAW,i2,'Length',{1,1});
l3=atgetfieldvalues(ARCAW,i3,'Length',{1,1});

ARCAW=atsetfieldvalues(ARCAW,i1,'BendingAngle',b1-AngSB/2/sum(l1).*l1);
ARCAW=atsetfieldvalues(ARCAW,i2,'BendingAngle',b2-AngSB/2/sum(l2).*l2);


% fit survey
[arc,~]=matchSurveyml(ARCAW,arc0,arc0SB,...
    {'\w*DQ1D\w*'},{'\w*DQ2C\w*'},{'\w*DQ1B\w*'},...
    ploton,verbose);

[l,~,~]=atlinopt(r,0,1:(cellendindex+1)); % get optics at cell end (could be canted cell or injection, so use whole latice to get optics)

% get optics at reference locations
sfaeindr=find(atgetcells(r(1:(cellendindex+1)),'FamName','DR_10','DR_35'),2,'last')+1;
centindr=find(atgetcells(r(1:(cellendindex+1)),'FamName','DQ2C\w*'),1,'last');
twiin=l(cellstartindex);
twiend=l(cellendindex+1);
twimid=l(centindr);
twisf=l(sfaeindr);

sfaeind=find(atgetcells(arc,'FamName','DR_10','DR_35'),2,'last')+1;
centind=find(atgetcells(arc,'FamName','DQ2C\w*'),1,'last');
excludequad=false(19,1);
excludequad([8:12])=true; % exclude DQ and QF8
excludeconstr=false(19,1);
excludeconstr([8:14])=true; % rm12 rm34, mux, muy, optics@end
arc=ebs.matching.arcopen(...
    arc,...
    twiin,twiend,...
    twisf,...
    twimid,...
    sfaeind,...
    centind,...
    excludequad,...
    excludeconstr,...
    true);


% plot if required
if ploton
    thetaDQ2Chalf=sum(atgetfieldvalues(ARCA,atgetcells(ARCA,'FamName','\w*DQ2C\w*'),'BendingAngle'))/2;
    thetaDQ1B=sum(atgetfieldvalues(ARCA,atgetcells(ARCA,'FamName','\w*DQ1B\w*'),'BendingAngle'));
    [parot]=atgeometry(ARCA,1:length(ARCA)+1,'Hangle',2*pi/32/2 );%- thetaDQ2Chalf -thetaDQ1B);
    [sb]=atgeometry(arc0SB,1:length(arc0SB)+1,'Hangle',2*pi/32/2);% - thetaDQ2Chalf -thetaDQ1B);
    [sbrot]=atgeometry(ARCAW,1:length(ARCAW)+1,'Hangle',2*pi/32/2);% - thetaDQ2Chalf -thetaDQ1B);
    [sbmrot]=atgeometry(arc,1:length(arc)+1,'Hangle',2*pi/32/2);% - thetaDQ2Chalf -thetaDQ1B);
    figure;%('units','normalized','position',[0.3 0.2 0.6 0.3]);
    plot([parot.x],[parot.y],'x-','DisplayName','standard','Linewidth',2);hold on;
    plot([sb.x],[sb.y],'d-.','DisplayName','SB','Linewidth',2);
    plot([sbrot.x],[sbrot.y],'s:','DisplayName','SB fix \theta_{DQ}','Linewidth',2);
    plot([sbmrot.x],[sbmrot.y],'o--','DisplayName','SB matched \theta_{DQ}','Linewidth',2);
    legend toggle;
    legend('Location','best');
    xlim([11.6 14.6]);
    xlabel('x [m]'); ylabel('y [m]')
end


% reconstruct lattice with new cell
r=[r(1:cellstartindex-1);...
    arc;... % arcwith 2PW
    r(cellendindex+1:end)];

end

function r=funUnbQF4(r,v,pb0,indqf4)

r=atsetfieldvalues(r,indqf4([1,2]),'PolynomB',{1,2},pb0([1 2])+v(1));
r=atsetfieldvalues(r,indqf4([3,4]),'PolynomB',{1,2},pb0([3,4])-v(1));

end

function [rok,d]=matchSurveyml(r,r0,r0sb,BndToChange,BndToChange2,BndToChange3,ploton,verbose)
% function matchSurvey()
%
% added magnetic lengths, cosntant rho along slices
% and entance exit angle adjustment.
%
% match the survey


idpos=findcells(r,'FamName','SBM-BM\w*');
idpos0=findcells(r0,'FamName','SBM-BM\w*');

%idpos=[1:idpos-10,(idpos+10):length(r)];
%idpos0=[1:idpos0-10,(idpos0+10):length(r0)];

allpos0=1:length(r0);
allpos=1:length(r);

%idpos=allpos;
%idpos0=allpos0;

%idpos=[(length(r)-10):length(r)]';
%idpos0=[(length(r0)-10):length(r0)]';

idpos=[findcells(r,'FamName','IDMarker'); length(r)+1];
idpos0=[findcells(r0,'FamName','IDMarker'); length(r0)+1];

of=0;
of0=0;
p0=atgeometry(r0,allpos0);%,of0);,'centered'
p0sb=atgeometry(r0sb,allpos0);%,of0);,'centered'
p=atgeometry(r,allpos);%,of);
xx=arrayfun(@(p)p.x,p);
yy=arrayfun(@(p)p.y,p);
xx0=arrayfun(@(p)p.x,p0);
yy0=arrayfun(@(p)p.y,p0);
xx0sb=arrayfun(@(p)p.x,p0sb);
yy0sb=arrayfun(@(p)p.y,p0sb);

if ploton
    figure;
    plot(xx0,yy0,'k+-',xx,yy,'r+-');
end
dif= getposdif(r,idpos,r0,idpos0);

zz=zeros(size(dif));
oo= ones(size(dif))/100000;

indB1=findcells(r,'FamName',BndToChange{:});
indB2=findcells(r,'FamName',BndToChange2{:});%
indB3=findcells(r,'FamName',BndToChange3{:});%

v1=atVariableBuilder(r,...
    {@(ar,deltabend)Vary3Bend(ar,deltabend,indB1,indB2,indB3)},{[0.0, 0.0]'});

indsmb=findcells(r,'FamName','SBM-BM\w*');%

v2=atVariableBuilder(r,...
    {@(ar,deltabend)atsetfieldvalues(ar,indsmb,'Length',deltabend)},{0.045});

indqf4=find(atgetcells(r,'FamName','QF4\w*'));
pb0qf=atgetfieldvalues(r,indqf4,'PolynomB',{1,2});% r{indqf4(1)}.PolynomB(2);
v3=atVariableBuilder(r,{@(r,v)funUnbQF4(r,v,pb0qf,indqf4)},{[0.0]'});

cSURV=struct(...
    'Fun',@(r,~,~)getposdif(r,idpos,r0,idpos0),...
    'Weight',oo,...
    'RefPoints',[1],...
    'Min',zz,...
    'Max',zz...
    );

sbpos=findcells(r,'FamName','DQ2C\w*');
sbpos0=findcells(r0,'FamName','DQ2C\w*');
ddd=getposdif(r,sbpos,r0,sbpos0);
zz=zeros(size(ddd));
oo=ones(size(ddd));

cBUMP=struct(...
    'Fun',@(r,~,~)getposdif(r,sbpos,r0,sbpos0),...
    'Weight',oo,...
    'RefPoints',[1],...
    'Min',zz-2e-3,...
    'Max',zz...
    );

[ll,~,~]=atlinopt(r0,0,1);
dxid=ll.Dispersion(1);

LinConstrETA=atlinconstraint(...
    length(r)+1,...
    {{'Dispersion',{1}}},...
    [dxid],...
    [dxid],...
    [0.01]);

LinConstrETAp=atlinconstraint(...
    length(r)+1,...
    {{'Dispersion',{2}}},...
    [0.0],...
    [0.0],...
    [1]);

cETA=[LinConstrETA LinConstrETAp]; % LinConstr1 LinConstr2  LinConstr3

verb=0;
if verbose
    verb=3;
end

[rok,~,d]=atmatch(r,[v1, v2, v3],[cSURV cETA],10^-15,20,verb,@lsqnonlin);

p=atgeometry(rok,allpos);%,'centered'
xxm=arrayfun(@(p)p.x,p);
yym=arrayfun(@(p)p.y,p);

if ploton
    hold on;
    
    plot(xxm,yym,'+-','Color',[0,0.5,0]);
    
    legend('actual',['mini bend'],['minibend close bump: ' num2str(d')]);
    
    figure;
    plot(findspos(rok,1:length(xx0)),[xx0sb-xx0,xx-xx0,xxm-xx0],'.-');
    title('x difference from r0 cell');
    legend(['SB'],['SB + (DQ2C, DQ1D) -SB/2 mrad'],['SB + matched DQ1[BD], DQ2C angles'],'Location','SouthOutside');
    ylim([-1 1]*1e-3)%legend(['mini bend'],['minibend close bump: ' num2str(d')],'Location','SouthOutside');
    xlabel('s');ylabel('x');
    
    figure;plot(findspos(rok,1:length(yy0)),[yy-yy0,yym-yy0],'.-');
    title('y difference from r0 cell');
    legend(['SB'],['SB + (DQ2C, DQ1D) -SB/2 mrad'],['SB + matched DQ1[BD], DQ2C angles'],'Location','SouthOutside');
    xlabel('s');ylabel('y');
    
    figure;
    radius=sqrt(xx.^2+yy.^2);
    radius0=sqrt(xx0.^2+yy0.^2);
    radiusm=sqrt(xxm.^2+yym.^2);
    plot(findspos(rok,1:length(xx0)),[radius-radius0,radiusm-radius0],'.-');
    title('radial difference from r0 cell');
    legend(['SB'],['SB + (DQ2C, DQ1D) -SB/2 mrad'],['SB + matched DQ1[BD], DQ2C angles'],'Location','SouthOutside');
    xlabel('s');ylabel('rho');
    
    figure;
    plot(findspos(rok,1:length(xx0)),[radiusm-radius0],'.-');
    title('radial difference from r0 cell');
    xlabel('s');ylabel('rho');
    atplotsyn(gca,rok);
    
end


end


function ar=Vary3Bend(ar,deltabend,indB1,indB2,indB3)
% vary angle of DL1I and DL2I keeping total angle constant
B1=getcellstruct(ar,'BendingAngle',indB1);
B2=getcellstruct(ar,'BendingAngle',indB2);
B3=getcellstruct(ar,'BendingAngle',indB3);
% vary angle of DL1I and DL2I keeping total angle constant
L1=getcellstruct(ar,'Length',indB1);
L2=getcellstruct(ar,'Length',indB2);
L3=getcellstruct(ar,'Length',indB3);

testrho=false;
if testrho
    disp(ar{indB1(1)}.FamName)
    disp(L1./B1);
    disp(ar{indB2(1)}.FamName)
    disp(L2./B2);
    disp(ar{indB3(1)}.FamName)
    disp(L3./B3);
end


% keep total angle constant and radius fo curvature of each slice identical
ar=setcellstruct(ar,'BendingAngle',indB1,B1+deltabend(1)/sum(L1).*L1);
ar=setcellstruct(ar,'BendingAngle',indB2,B2+deltabend(2)/sum(L2).*L2);
ar=setcellstruct(ar,'BendingAngle',indB3,B3-sum(deltabend)/sum(L3).*L3);

BT1=sum(cellfun(@(a)a.BendingAngle,ar(indB1),'un',1));
BT2=sum(cellfun(@(a)a.BendingAngle,ar(indB2),'un',1));
BT3=sum(cellfun(@(a)a.BendingAngle,ar(indB3),'un',1));

ar = atsetfieldvalues(ar,indB1(1),'EntranceAngle',BT1/2);
ar = atsetfieldvalues(ar,indB1(end),'ExitAngle',BT1/2);
ar = atsetfieldvalues(ar,indB2(1),'EntranceAngle',BT2/2);
ar = atsetfieldvalues(ar,indB2(end),'ExitAngle',BT2/2);
ar = atsetfieldvalues(ar,indB3(1),'EntranceAngle',BT3/2);
ar = atsetfieldvalues(ar,indB3(end),'ExitAngle',BT3/2);

if testrho
    B1=getcellstruct(ar,'BendingAngle',indB1);
    B2=getcellstruct(ar,'BendingAngle',indB2);
    B3=getcellstruct(ar,'BendingAngle',indB3);
    % vary angle of DL1I and DL2I keeping total angle constant
    L1=getcellstruct(ar,'Length',indB1);
    L2=getcellstruct(ar,'Length',indB2);
    L3=getcellstruct(ar,'Length',indB3);
    disp(ar{indB1(1)}.FamName)
    disp(L1./B1);
    disp(ar{indB2(1)}.FamName)
    disp(L2./B2);
    disp(ar{indB3(1)}.FamName)
    disp(L3./B3);
    pause(5);
end

end


function posdif=getposdif(r,idpos,r0,idpos0)

% [p0,of0]=atgeometry(r0,idpos0);
% [p,of]=atgeometry(r,idpos);
of=0;
of0=0;
p0=atgeometry(r0,idpos0,of0);
p=atgeometry(r,idpos,of);

xx=arrayfun(@(p)p.x,p);
yy=arrayfun(@(p)p.y,p);

xx0=arrayfun(@(p)p.x,p0);
yy0=arrayfun(@(p)p.y,p0);

%[t,R]=cart2pol(xx,yy);
%[t0,R0]=cart2pol(xx0,yy0);

posdif=[(xx-xx0)./xx0;(yy-yy0)./yy0]';

%radius=sqrt(xx.^2+yy.^2);
%radius0=sqrt(xx0.^2+yy0.^2);
%posdif=((radius-radius0)./radius0)';

% L0=xx0.^2+yy0.^2
%posdif=[R-R0;t-t0]';

% [p0,of0]=atgeometry(r0,idpos0);
% [p,of]=atgeometry(r,idpos);
% posdif=of-of0;
end
