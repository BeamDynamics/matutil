function [ rcm ] = Canting( r, idnum, AngCantDip, varargin )
%Canting inserts canting in ring 'r' at id 'idnum'
%
%Inputs:
% r: EBS upgrade lattice
% idnum: ID number where canting is done
% AngCantDip: angle of canting +theta -2*theta  +theta
%
%Optional Inputs:
% LCantDip : length of canting dipoles
% SextFix : adjust sextupole about Canting ID
% ploton: prints out plots, true/false (default)
% verbose: prints matching process true/false (default)
%
%Output:
% r: EBS upgrade lattice with canting at id idnum
%
%Description:
% 1) Canting dipole inserted in DR_01 DR_45
% 2) QF1, QD2, QD3, QF4, QD5, QF6
%    matched to flatten dispersion in following ID
%    note: this implies different beta at these IDs
% 3) (optional, SextFix) adapt sextupole strenghts about canted ID
%
%Example:
% >> rc = ebs.matching.Canting(LOW_EMIT_RING_INJ,5,5.4e-3,'ploton',true)
%
%Warning:
% 1) this function is not supposed to be used inside ebs.model, as it changes
% the number of elements in the lattice
% 2) it assumes that cells IDx and IDx+1 are symmetric
% 
%see also: ebs.matching.CantingOptics 

% Canting parameters

LCantDip_def = 0.105/2; % m

p=inputParser;
addRequired(p,'r',@iscell);
addRequired(p,'idnum',@isnumeric);
addRequired(p,'AngCantDip',@isnumeric);
addOptional(p,'LCantDip',LCantDip_def,@isnumeric);
addOptional(p,'SextFix',false,@islogical);
addOptional(p,'ploton',false,@islogical);
addOptional(p,'verbose',false,@islogical);

parse(p,r,idnum,AngCantDip,varargin{:});

LCantDip=p.Results.LCantDip;
SextFix=p.Results.SextFix;
ploton=p.Results.ploton;
verbose=p.Results.verbose;

% no canting in injection cells.
if ismember(idnum, [3 4 5]);
    error('Canting Forbidded for cells 3,4,5');
end

% get cell index
[indid,~]=ebs.getcellstartend(r,idnum);

% add canting dipole at ID center (10cm, full angle)
cantdip=atrbend(['CantingDipoleID' num2str(idnum,'%.2d')],LCantDip,AngCantDip,'PassMethod','BndMPoleSymplectic4Pass');

rc=r;
rc{indid-1}.Length=rc{indid-1}.Length-LCantDip;
rc{indid+1}.Length=rc{indid+1}.Length-LCantDip;
cantmark=rc{indid};
cantmark.FamName=['CantedID' num2str(idnum,'%.2d')];
rc=[rc(1:indid-1);cantdip;rc(indid);cantmark;cantdip;rc(indid+1:end)];


% set dipole angles to new values
indDL1A_5=find(atgetcells(r,'FamName','DL1A_5'));
indDL1E_5=find(atgetcells(r,'FamName','DL1E_5'));
dip10=indDL1E_5(find(indDL1E_5<indid,1,'last'));
dip30=indDL1A_5(find(indDL1A_5>indid,1,'first'));

indDL1A_5=find(atgetcells(rc,'FamName','DL1A_5'));
indDL1E_5=find(atgetcells(rc,'FamName','DL1E_5'));
dip1=indDL1E_5(find(indDL1E_5<indid,1,'last'));
dip3=indDL1A_5(find(indDL1A_5>indid,1,'first'));

BDL=atgetfieldvalues(rc,[dip1 dip3],'BendingAngle');

rc=atsetfieldvalues(rc,[dip1 dip3],'BendingAngle',BDL-AngCantDip);
addname=['ID' num2str(idnum,'%.2d')];
% rename dipoles (for conversion to MADX)
rc=atsetfieldvalues(rc,[dip1 dip3],'FamName',{['DL1E_5' '_' addname],['DL1A_5' '_' addname]});


% match survey
cl=rc([dip1-10:dip3+10]);
l=r([dip10-10:dip30+10]);
[rok,~]=matchCantedIDLength( cl ,length(cl),...
    l ,length(l),...
    {'DR_01','DR_45'},ploton,verbose);

rc(dip1-10:dip3+10)=rok;

% show survey
if ploton
    figure('Units','normalized','Position',[0.3 0.2 0.7 0.4]);
    p=atgeometry(r((dip10-10):indid));
    startang=-p(end).angle;
    p0=atgeometry(r((dip10-10):(dip30+10)),1:length((dip10-10):(dip30+10)),[0,0],startang);
    pc=atgeometry(rc((dip1-10):(dip3+10)),1:length((dip1-10):(dip3+10)),[0,0],startang); % +1 for new inserted element
    plot([p0.x],[p0.y],'r.-'); hold on;
    plot([pc.x],[pc.y],'bx-');
    xlabel('x [m]');
    ylabel('y [m]');
    storefigure(['CantedID' num2str(idnum,'%.2d') 'Survey']);
end

% rematch optics using quadrupoles.
rcm=ebs.matching.CantingOptics(rc,r,idnum,ploton,verbose);

%% sextupole adjustment
if SextFix

    indidc=indid+1;
    
    addpath('/machfs/liuzzo/EBS/S28D/LATTICE/AT/InjectionSextupoles');
    ringi=rcm; % lattice to be corrected
    ring0=r;   % reference lattice
    
    indSext0=find(atgetcells(ring0,'FamName','S[FDIJ]\w*'))';%nSext0=length(indSext0);
    indSexti=find(atgetcells(ringi,'FamName','S[FDIJ]\w*'))';%nSexti=length(indSexti);
    indmoni0=find(atgetcells(ring0,'Class','Monitor'))';
    indmonii=find(atgetcells(ringi,'Class','Monitor'))';
    
    % this are the indices where you want to have the quantities corrected.
    % I want to correct in the cells 2:31, in all the sextupoles and in all the
    % bpm
    indWhereCorr0=[ indSext0([1:(find(indSext0<indid,1,'last')-3),(find(indSext0>indid,1,'first')+3):end]),...
        indmoni0([1:(find(indmoni0<indid,1,'last')-5),(find(indmoni0>indid,1,'first')+5):end]) ];
    indWhereCorri=[ indSexti([1:(find(indSexti<indidc,1,'last')-3),(find(indSexti>indidc,1,'first')+3):end]),...
        indmonii([1:(find(indmonii<indidc,1,'last')-5),(find(indmonii>indidc,1,'first')+5):end]) ];
    
    % I put the sextupoles of ring0 in the ringi lattice
    indSextCant0=((find(indSext0<indid,1,'last')-2):(find(indSext0>indid,1,'first')+2));
    indSextCanti=((find(indSexti<indidc,1,'last')-2):(find(indSexti>indidc,1,'first')+2));
    sextinj_noinj=atgetfieldvalues(ring0,indSext0(indSextCant0),'PolynomB',{1,3});
    ringi_wrongSext=atsetfieldvalues(ringi,indSexti(indSextCanti),'PolynomB',{1,3},sextinj_noinj);
    
    indQuad0=findQuad(ring0); %nquad0=length(indQuad0);
    indQuadi=findQuad(ringi); %nquadi=length(indQuadi);
    indoct0=[];indocti=[];
    
    % Weights=[...
    %    0;0; 0;0; 0;0; 0;0; 0;0;...
    %    1;1; ...
    %    1;1; 1;1; 1;1; ...
    %    1;1;0 ];
    
    Weights=[...
        0;0; 0;0; 0;0; 0;0; 0;0;...
        10000;10000; ...
        1;1; 1;1; 1;1; ...
        1;1;0 ];
    
    % The cells you want to have with different sextupoles, [1 32] is my
    % personal suggestion, but you can try [1 4 29 32] as it was in past
    
    indSextCorr=indSextCanti;
    
    [ ringicor, b3L_Corr, L_corr ]= ...
        CalcSextCorrections(...
        ring0,ringi_wrongSext,...
        indWhereCorr0,indWhereCorri,...
        indQuad0,indQuadi,...
        indSext0,indSexti,...
        indoct0,indocti,...
        indSextCorr,...
        Weights,.9);
    
    s0=[1,2,3,6,7,8]/10;
    s=[];
    DeltaSext=zeros(1,length(indSexti));
    for i=1:length(indSexti)
        DeltaSext(i)=ringicor{indSexti(i)}.PolynomB(1,3)-ring0{indSext0(i)}.PolynomB(1,3);
        disp([ringicor{indSexti(i)}.FamName ' : '...
            num2str(ringicor{indSexti(i)}.PolynomB(1,3),'%2.3f') ' -> '...
            num2str(ring0{indSext0(i)}.PolynomB(1,3),'%2.3f') ' Delta '...
            num2str(DeltaSext(i),'%2.3f') ' 1/m3 '...
            ])
    end
    
    if ploton
        
        figure; hold on; grid on;
        for i=1:32
            line([i i], [-10 10],'LineWidth',1,'Color',[.8 .8 .8]);
            s=[s,s0+i-1];
        end
        bar(s,DeltaSext,'r');
        xlim([0,32]);
        xlabel('Cell number');
        ylabel('Sextupole change (m^{-3})')
        storefigure('SextupoleVariations')
        %  figure; atplot(ring0,@plotRDT_rm,'geometric1');
        %  figure; atplot(ringi_wrongSext,@plotRDT_rm,'geometric1');
        %  figure; atplot(ringicor,@plotRDT_rm,'geometric1');
        
        figure; atplot(ring0,'comment',[],@plotChromFunctions,[1,2]); storefigure('NominalChromfunctions');
        figure; atplot(ringi_wrongSext,'comment',[],@plotChromFunctions,[1,2]); storefigure('CantingChromfunctions');
        figure; atplot(ringicor,'comment',[],@plotChromFunctions,[1,2]);  storefigure('CantingAdjustedChromfunctions');
        
        figure; atplot(ring0,'comment',[],@plotWdispP); storefigure('NominalWfunctions');
        figure; atplot(ringi_wrongSext,'comment',[],@plotWdispP); storefigure('CantingWfunctions');
        figure; atplot(ringicor,'comment',[],@plotWdispP);  storefigure('CantingAdjustedWfunctions');
        
        % store corrections
        rcm=ringicor;
    end
    
end

if ploton
    % show canting and matching
    nelshow=150;
    figure('Units','normalized','Position',[0.3 0.2 0.6 0.6]);
    atplot(r,[findspos(r,dip10-nelshow) findspos(r,dip30+nelshow)],'comment',[]);
    storefigure(['nominalID' num2str(idnum,'%.2d')]);
    figure('Units','normalized','Position',[0.3 0.2 0.6 0.6]);
    atplot(rc,[findspos(rc,dip1-nelshow) findspos(rc,dip3+nelshow)],'comment',[]);
    storefigure(['CantedID' num2str(idnum,'%.2d')]);
    figure('Units','normalized','Position',[0.3 0.2 0.6 0.6]);
    atplot(rcm,[findspos(rcm,dip1-nelshow) findspos(rcm,dip3+nelshow)],'comment',[]);
    storefigure(['MatchedCantedID' num2str(idnum,'%.2d')]);
    
    %
    [twi,~,~]=atlinopt(r,0,dip10+(-nelshow:nelshow));
    s0=arrayfun(@(a)a.SPos,twi,'un',1);
    bx0=arrayfun(@(a)a.beta(1),twi,'un',1);
    by0=arrayfun(@(a)a.beta(2),twi,'un',1);
    dx0=arrayfun(@(a)a.Dispersion(1),twi,'un',1);
    [twic,~,~]=atlinopt(rc,0,dip1+(-nelshow:nelshow));
    sc=arrayfun(@(a)a.SPos,twic,'un',1);
    bxc=arrayfun(@(a)a.beta(1),twic,'un',1);
    byc=arrayfun(@(a)a.beta(2),twic,'un',1);
    dxc=arrayfun(@(a)a.Dispersion(1),twic,'un',1);
    [twim,~,~]=atlinopt(rcm,0,dip1+(-nelshow:nelshow));
    sm=arrayfun(@(a)a.SPos,twim,'un',1);
    bxm=arrayfun(@(a)a.beta(1),twim,'un',1);
    bym=arrayfun(@(a)a.beta(2),twim,'un',1);
    dxm=arrayfun(@(a)a.Dispersion(1),twim,'un',1);
    
    %
    figure('Units','normalized','Position',[0.3 0.2 0.6 0.6]);
    subplot(3,1,1);
    plot(s0,bx0,'-','LineWidth',2); hold on;
    plot(sc,bxc,'--','LineWidth',2); hold on;
    plot(sm,bxm,'-.','LineWidth',2); hold on;
    ylabel('\beta_x [m]');
    legend('Nominal','Canting','Matched');
    subplot(3,1,2);
    plot(s0,by0,'-','LineWidth',2); hold on;
    plot(sc,byc,'--','LineWidth',2); hold on;
    plot(sm,bym,'-.','LineWidth',2); hold on;
    ylabel('\beta_y [m]');
    subplot(3,1,3);
    plot(s0,dx0,'-','LineWidth',2); hold on;
    plot(sc,dxc,'--','LineWidth',2); hold on;
    plot(sm,dxm,'-.','LineWidth',2); hold on;
    ylabel('\eta_x [m]');
    xlabel('s [m]');
    
    storefigure('MatchedCantingOptics')
    % % whole lattice
    [twi,~,~]=atlinopt(r,0,atgetcells(r,'Class','Quadrupole','Sextupole','Monitor','Drift'));
    s0=arrayfun(@(a)a.SPos,twi,'un',1);
    bx0=arrayfun(@(a)a.beta(1),twi,'un',1);
    by0=arrayfun(@(a)a.beta(2),twi,'un',1);
    dx0=arrayfun(@(a)a.Dispersion(1),twi,'un',1);
    [twic,~,~]=atlinopt(rc,0,atgetcells(rc,'Class','Quadrupole','Sextupole','Monitor','Drift'));
    sc=arrayfun(@(a)a.SPos,twic,'un',1);
    bxc=arrayfun(@(a)a.beta(1),twic,'un',1);
    byc=arrayfun(@(a)a.beta(2),twic,'un',1);
    dxc=arrayfun(@(a)a.Dispersion(1),twic,'un',1);
    [twim,~,~]=atlinopt(rcm,0,atgetcells(rcm,'Class','Quadrupole','Sextupole','Monitor','Drift'));
    sm=arrayfun(@(a)a.SPos,twim,'un',1);
    bxm=arrayfun(@(a)a.beta(1),twim,'un',1);
    bym=arrayfun(@(a)a.beta(2),twim,'un',1);
    dxm=arrayfun(@(a)a.Dispersion(1),twim,'un',1);
    
    figure('Units','normalized','Position',[0.3 0.1 0.6 0.8]);
    ax1=subplot(3,1,1);
    %plot(s0,bx0,'-','LineWidth',2); hold on;
    plot(sc,(bxc-bx0)./bx0,'--','LineWidth',2); hold on;
    plot(sm,(bxm-bx0)./bx0,'-.','LineWidth',2); hold on;
    ylabel('(\beta_x-\beta_{x,0})/\beta_{x,0} [m]');
    legend('Canting','Matched');
    ax2=subplot(3,1,2);
    %plot(s0,by0,'-','LineWidth',2); hold on;
    plot(sc,(byc-by0)./by0,'--','LineWidth',2); hold on;
    plot(sm,(bym-by0)./by0,'-.','LineWidth',2); hold on;
    ylabel('(\beta_y-\beta_{y,0})/\beta_{y,0} [m]');
    ax3=subplot(3,1,3);
    %plot(s0,dx0,'-','LineWidth',2); hold on;
    plot(sc,(dxc-dx0),'--','LineWidth',2); hold on;
    plot(sm,(dxm-dx0),'-.','LineWidth',2); hold on;
    ylabel('\eta_x -\eta_{x,0} [m]');
    xlabel('s [m]');
    storefigure('MatchedCantingOpticsDistortion')
    
    linkaxes([ax1,ax2,ax3],'x')
    xlim([sid-20,sid+20]);
    storefigure('MatchedCantingOpticsDistortionZoom')
end


end


function [rok,drLok]=matchCantedIDLength(r,idpos,r0,idpos0,DRTOCHANGE,ploton,verbose)
% function matchCantedIDLength(r,idpos,r0,idpos0,DRTOCHANGE)
%
% match the CANTED ID striagth sections lenght.
%
% inputs:
% r,   canted ID lattice
% idpos, positions to match in r
% r0,  standard ID lattice
% idpos0, positions to match in r0
% DRTOCHANGE drifts to change in r
%
% output:
% rok, lattice with matched drift lengths
% dsLok, total final length of DRTOCHANGE.
%
%see also: atgeometry atmatch

allpos0=1:length(r0);
allpos=1:length(r);

of=0;
of0=0;
p0=atgeometry(r0,allpos0,of0);
p=atgeometry(r,allpos,of);
xx=arrayfun(@(p)p.x,p);
yy=arrayfun(@(p)p.y,p);
xx0=arrayfun(@(p)p.x,p0);
yy0=arrayfun(@(p)p.y,p0);

if ploton
    plot(xx0,yy0,'k+-',xx,yy,'r+-');
end

dif= getposdif(r,idpos,r0,idpos0);

zz=zeros(size(dif));
oo= ones(size(dif));

% % % define variable drift

indDR1=findcells(r,'FamName',DRTOCHANGE{:});
DR1L=atgetfieldvalues(r,indDR1,'Length',{1,1});
v=atVariableBuilder(r,...
    {@(ar,dl)VaryDriftLengths(ar,dl,indDR1)},{DR1L(1)});

% % % constraint= difference in position at last point
c=struct(...
    'Fun',@(r,~,~)getposdif(r,idpos,r0,idpos0),...
    'Weight',oo,...
    'RefPoints',[1],...
    'Min',zz,...
    'Max',zz...
    );


verb=0;
if verbose, verb=3; end
[rok,~,~]=atmatch(r,v,c,10^-25,50,verb,@lsqnonlin);

%[rok,~,~]=atmatch(r,[v],c,10^-25,500,3,@fminsearch);


p=atgeometry(rok,allpos,of);
xx=arrayfun(@(p)p.x,p);
yy=arrayfun(@(p)p.y,p);

if ploton
    hold on;
    %plot(s,xx-xx0,'m+-',s,yy-yy0,'b+-');
    plot(xx,yy,'o--','Color',[0,0.5,0]);
    
end
indDR78=findcells(r,'FamName',DRTOCHANGE{:});
drL(1)=r{indDR78(1)}.Length;

indDR78=findcells(rok,'FamName',DRTOCHANGE{:});
drLok(1)=rok{indDR78(1)}.Length;

if ploton
    
    legend('ID',['CANTED ID DR@ID=' num2str(drL)],['CANTED ID \Delta DR@ID=' num2str(drLok-drL,'%2.2g')])
end

end


function ar=VaryDriftLengths(ar,dl,indDR1)
% vary angle of DL1I and DL2I keeping total angle constant

ar=atsetfieldvalues(ar,indDR1,'Length',{1,1},dl);
end


function posdif=getposdif(r,idpos,r0,idpos0)

of=0;
of0=0;
p0=atgeometry(r0,1:length(r0),of0);
p=atgeometry(r,1:length(r),of);

xx=arrayfun(@(p)p.x,p(idpos));
yy=arrayfun(@(p)p.y,p(idpos));

xx0=arrayfun(@(p)p.x,p0(idpos0));
yy0=arrayfun(@(p)p.y,p0(idpos0));

posdif=[xx-xx0;yy-yy0]';

end

