function arcm=arcopen(...
    arc,...
    twiin,twiend,...
    twisf,...
    twimid,...
    sfaeind,...
    centind,...
    excludequad,...
    excludeconstr,...
    verbose)
%arcopen match optics in arc given input optics at entrance, exit, SF and
%center using all quadrupoles
%
% arcm=arcopen(...
%     arc,...               EBS upgrade arc lattice
%     twiin,...             wished initial twiss
%                           ( [twiin,~,~]=atlinopt(reflat,0,1) )
%     twiend,...            wished final twiss
%                           [twiend,~,~]=atlinopt(reflat,0,1+length(reflat))
%     twisf,...             twiss at sfaeind indexes (2x1 twiss structure)
%                           ( [twisf,~,~]=atlinopt(reflat,0,sfaeind) )
%     twimid,...            twiss at cell center
%                           ( [twimid,~,~]=atlinopt(reflat,0,centind) )
%     sfaeind,...           SFA and SFE indexes in arc
%     centind,...           center of arc cell
%     excludequad,...       1x19 logical to exclude quadrupoles
%     excludeconstr,...     1x19 logical to exclude constraints
%     verbose)
%
% quadrupole use for matching (lattice order):
% 1     'QF1[A,LM]\w*'
% 2     'QD2[A,LM]\w*'
% 3     'QD3[A,LM]\w*'
% 4     'QF4[A,LM]\w*'
% 5     'QF4[B,LM]\w*'
% 6     'QD5[B,LM]\w*'
% 7     'QF6B\w*'
% 8     'DQ1B\w*'
% 9     'QF8B\w*'
% 10    'DQ2\w*'
% 11    'QF8D\w*'
% 12    'DQ1D\w*'
% 13    'QF6D\w*'
% 14    'QD5[D,LM]\w*'
% 15    'QF4[D,LM]\w*'
% 16    'QF4[E,LM]\w*'
% 17    'QD3[E,LM]\w*'
% 18    'QD2[E,LM]\w*'
% 19    'QF1[E,LM]\w*'
%     
%
% constraint list
% 1     rm12 (amoung sfaeind)
% 2     rm34 (amoung sfaeind)
% 3     end mux
% 4     end muy
% 5     sfA beta y
% 6     sfA alpha y
% 7     sfA dispersion x
% 8     sfE beta y
% 9     sfE alpha y
% 10    sfE dispersion x
% 11    mid beta y
% 12    mid alpha x
% 13    mid alpha y
% 14    mid disp x prime
% 15    end beta x       x10
% 16    end alpha x      x10
% 17    end alpha y      x10
% 18    end disp x       x10
% 19    end disp x prime x10
%
%
% example to exlude DQ (frozen gradient) and QF8D (2PW)
% excludequad=false(19,1);
% excludequad([8,9,10,11,12])=true;
%
%
% function to match the ARC cells open line
%
% EBS upgrade lattice
%
%see also: matchingS17injsection
if verbose, verb=3; end
if ~verbose, verb=0; end

famnames={...
    'QF1[A,LM]\w*',...  1
    'QD2[A,LM]\w*',...  2
    'QD3[A,LM]\w*',...  3
    'QF4[A,LM]\w*',...  4
    'QF4[B,LM]\w*',...  5
    'QD5[B,LM]\w*',...  6
    'QF6B\w*',...       7
    'DQ1B\w*',...       8
    'QF8B\w*',...       9
    'DQ2\w*',...        10
    'QF8D\w*',...       11
    'DQ1D\w*',...       12
    'QF6D\w*',...       13    
    'QD5[D,LM]\w*',...  14
    'QF4[D,LM]\w*',...  15
    'QF4[E,LM]\w*',...  16
    'QD3[E,LM]\w*',...  17
    'QD2[E,LM]\w*',...  18  
    'QF1[E,LM]\w*'...   19
    }; %

Variab=[];
for ifam=1:length(famnames)
    Variab=[Variab atVariableBuilder(arc,famnames(ifam),{{'PolynomB',{1,2}}})]; %#ok<AGROW>
end

%% constraints
rm44_12a=RM44(twisf,1,2);
rm44_34a=RM44(twisf,3,4);

rm12=struct(...
    'Fun',@(~,ld,~)RM44(ld,1,2),...
    'Weight',1e-1,...
    'RefPoints',[sfaeind(1),sfaeind(2)],...
    'Min',rm44_12a,...
    'Max',rm44_12a);

rm34=struct(...
    'Fun',@(~,ld,~)RM44(ld,3,4),...
    'Weight',1e-1,...
    'RefPoints',[sfaeind(1),sfaeind(2)],...
    'Min',rm44_34a,...
    'Max',rm44_34a);

muxid=(twiend.mu(1)-twiin.mu(1))/2/pi;
muyid=(twiend.mu(2)-twiin.mu(2))/2/pi;
mx=struct(...
    'Fun',@(~,ld,~)mux(ld),...
    'Weight',1e-2,...
    'RefPoints',[1:length(arc)+1],...
    'Min',muxid,...
    'Max',muxid);

my=struct(...
    'Fun',@(~,ld,~)muy(ld),...
    'Weight',1e-2,...
    'RefPoints',[1:length(arc)+1],...
    'Min',muyid,...
    'Max',muyid);

sf2Aopt=[...
    atlinconstraint(sfaeind(1),{{'beta',{2}}},twisf(1).beta(2),twisf(1).beta(2),1),...
    atlinconstraint(sfaeind(1),{{'alpha',{2}}},twisf(1).alpha(2),twisf(1).alpha(2),1),...
    atlinconstraint(sfaeind(1),{{'Dispersion',{1}}},twisf(1).Dispersion(1),twisf(1).Dispersion(1),1),...
    ];

sf2Eopt=[...
    atlinconstraint(sfaeind(2),{{'beta',{2}}},twisf(2).beta(2),twisf(2).beta(2),1),...
    atlinconstraint(sfaeind(2),{{'alpha',{2}}},twisf(2).alpha(2),twisf(2).alpha(2),1),...
    atlinconstraint(sfaeind(2),{{'Dispersion',{1}}},twisf(2).Dispersion(1),twisf(2).Dispersion(1),1),...
    ];

midopt=[...
    atlinconstraint(centind(1),{{'beta',{2}}},twimid.beta(2),twimid.beta(2),10),...
    atlinconstraint(centind(1),{{'alpha',{1}}},twimid.alpha(1),twimid.alpha(1),1),... % some cells are asymmetric, remove alpha zero
    atlinconstraint(centind(1),{{'alpha',{2}}},twimid.alpha(2),twimid.alpha(2),1),...
    atlinconstraint(centind(1),{{'Dispersion',{2}}},twimid.Dispersion(2),twimid.Dispersion(2),1),...
    ];

endopt=[...
    atlinconstraint(length(arc)+1,{{'beta',{1}}},twiend.beta(1),twiend.beta(1),1e-2),...
    atlinconstraint(length(arc)+1,{{'alpha',{1}}},twiend.alpha(1),twiend.alpha(1),1e-2),...
    atlinconstraint(length(arc)+1,{{'alpha',{2}}},twiend.alpha(2),twiend.alpha(2),1e-2),...
    atlinconstraint(length(arc)+1,{{'Dispersion',{1}}},twiend.Dispersion(1),twiend.Dispersion(1),1e-2),... 
    atlinconstraint(length(arc)+1,{{'Dispersion',{2}}},twiend.Dispersion(2),twiend.Dispersion(2),1e-2),... 
    ];

% soft lmits
softlims=[...
    atlinconstraint(1:length(arc)+1,{{'beta',{1}}},0,12,1),...
    atlinconstraint(1:length(arc)+1,{{'beta',{2}}},0,20,1),...
];

Constr=[rm12,...
        rm34, ...
        mx, ...
        my,...
        sf2Aopt,...
        sf2Eopt,...
        midopt,...
        endopt,...
        softlims...
        ];

% exclude magnets from matching
Variab(excludequad)=[];
Constr(excludeconstr)=[];

%% execute least square fit
arcm=arc;

arcm=atmatch(arcm,Variab,Constr,10^-9,500,verb,@lsqnonlin,twiin);%,'fminsearch');%
%arcm=atmatch(arcm,Variab,Constr,10^-10,1000,verb,@fminsearch,twiin);%,'fminsearch');%
%arcm=atmatch(arcm,Variab,Constr,10^-15,100,verb,@lsqnonlin,twiin);%,'fminsearch');%

return

function m=muy(lindata)

m=lindata(end).mu(2)/2/pi;

return

function m=mux(lindata)

m=lindata(end).mu(1)/2/pi;

return

function [r11]=RM44(lindata,ind1,ind2)
% get value of of indeces ind1 and ind2 (1 to 4) of
% M44 between two points first and last

Mlast=lindata(2).M44;
Mfirst=lindata(1).M44;

Mfirstinv=[[Mfirst(2,2),-Mfirst(1,2);-Mfirst(2,1),Mfirst(1,1)],...
    zeros(2,2);...
    zeros(2,2),...
    [Mfirst(4,4),-Mfirst(3,4);-Mfirst(4,3),Mfirst(3,3)]];

R=Mlast*Mfirstinv;
%R=Mlast/Mfirst;

r11=R(ind1,ind2);

return

