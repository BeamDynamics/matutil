function [ r, arc ] = ThreePoleWiggler( r,idnum,varargin )
%TWOPOLEWIGGLER inserts a 2PW in ring 'r' at cell 'idnum'
%
%Inputs:
% r: EBS upgrade lattice
% idnum: ID number where the 3PW is placed
%
%Optional Inputs:
% Ang3PW: 3PW angle (defaut 5.4842e-04)
%
%Output:
% r: EBS upgrade lattice with 3PW at id idnum
% arc : cell with 3pw
%
%Description:
% 3PW inserted in DR_25
%
%Example:
% >> [r3pw,arc3pw]=ebs.matching.ThreePoleWiggler(LOW_EMIT_RING_INJ,5)
%
%Warning:
% this function is not supposed to be used inside ebs.model, as it changes
% the number of elements in the lattice
%
%see also:

% 3PW parameters
Dist_sourcepoint_fromQF8=0.10825;
LPole=0.0126993333333333;
DistPole=0.015;
Ang3PW_def=5.4842e-04;

p=inputParser;
addRequired(p,'r',@iscell);
addRequired(p,'idnum',@isnumeric);
addOptional(p,'Ang3PW',Ang3PW_def,@isnumeric);

parse(p,r,idnum,varargin{:});

Ang3PW=p.Results.Ang3PW;

% get cell
[cellstartindex,cellendindex]=ebs.getcellstartend(r,idnum);

ARCA=r(cellstartindex:cellendindex);

% remove eventual BMHARD markers
ARCA(atgetcells(ARCA,'FamName','BMHARD'))=[]; 

dr25ind=findcells(ARCA,'FamName','DR_25\w*');
   ARCA{dr25ind(1)}.Length= ARCA{dr25ind(1)}.Length +ARCA{dr25ind(2)}.Length;
ARCA(dr25ind(2))=[];
dr25ind=findcells(ARCA,'FamName','DR_25\w*');


% 3 Pole Wiggler
welems={ atdrift('DW1',ARCA{dr25ind(1)}.Length-(Dist_sourcepoint_fromQF8+2*LPole+DistPole));...
    atmultipole('W1',LPole,0,-Ang3PW/LPole);...
        atdrift('DW2',DistPole);...
    atmultipole('W1',LPole,0,Ang3PW/LPole);...
       atmarker('BMHARD');...
       atmarker(['3PW' '-BM' num2str(idnum,'%.2d')]);...
    atmultipole('W1',LPole,0,Ang3PW/LPole);...
        atdrift('DW2',DistPole);...
    atmultipole('W1',LPole,0,-Ang3PW/LPole);...
        atdrift('DW3',Dist_sourcepoint_fromQF8-2*LPole-DistPole);...
        };
    
dr25ind=findcells(ARCA,'FamName','DR_25\w*');
   
arc=[...
    ARCA(1:dr25ind-1);...
    welems;...
    ARCA((dr25ind+1):end);...
    ];


[l,~,~]=atlinopt(r,0,1:(cellendindex+1)); % get optics at cell end (could be canted cell or injection, so use whole latice to get optics)

% get optics at reference locations
sfaeindr=find(atgetcells(r(1:(cellendindex+1)),'FamName','DR_10','DR_35'),2,'last')+1;
centindr=find(atgetcells(r(1:(cellendindex+1)),'FamName','DQ2C\w*'),1,'last');
twiin=l(cellstartindex);
twiend=l(cellendindex+1);
twimid=l(centindr);
twisf=l(sfaeindr);

sfaeind=find(atgetcells(arc,'FamName','DR_10','DR_35'),2,'last')+1;
centind=find(atgetcells(arc,'FamName','DQ2C\w*'),1,'last');
excludequad=false(19,1);
excludequad([8:12])=true; % exclude DQ and QF8
excludeconstr=false(19,1);
excludeconstr([8:14])=true; % rm12 rm34, mux, muy, optics@end
arc=ebs.matching.arcopen(...
    arc,...
    twiin,twiend,...
    twisf,...
    twimid,...
    sfaeind,...
    centind,...
    excludequad,...
    excludeconstr,...
    true);


% reconstruct lattice with new cell
r=[r(1:cellstartindex-1);...
    arc;... % arcwith 2PW
    r(cellendindex+1:end)];
end



