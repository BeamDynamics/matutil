function [resp,gradients,algos,steerlist]=load_resp(filename,plane,varargin)
%LOAD_RESP    get measured response matrix
%
%[RESPONSE,GRADIENTS,ALGORITHM]=LOAD_RESP(DIRNAME,PLANE,STEERLIST)
%           returns the non-normalized response and the corresponding
%           excitations
%
%DIRNAME:   Directory containing measured data or
%           file containing a full response matrix
%PLANE:     'h|H|x|X|1' 'v|V|z|Z|2' 'h2v|H2V|x2z|X2Z|3' 'v2h|V2H|z2x|Z2X|4'
%STEERLIST: list of steerers (1..288)
%
%RESPONSE:	BPM response [m]
%CURRENT:	list of steerer gradients [rad]
%ALGORITHM:	BPM algorithm
%
%[...]=LOAD_RESP(...,'Zeros',action,...)
%[...]=LOAD_RESP(...,'Fixed',action,...)
%[...]=LOAD_RESP(...,'TooLarge',action,...)
%           Define the behaviour for Zero readings, Constant readings and
%           abnormal values (default: keep)
%           	action>0: keep the values
%               action=0: ask
%               action<0: reject the values
%
% See also

nb=320;
nkt=288;

[elzeros,options]=getoption(varargin,'Zeros',1);
[elfixed,options]=getoption(options,'Fixed',1);
[eltoolarge,options]=getoption(options,'TooLarge',1);
args={1:nkt};
args(1:length(options))=options;
steerlist=args{1};
fn=fullfile(filename,['steer' selectplane(plane,{'H','V','V2H','H2V'}) '%.3d']);
nk=length(steerlist);
resp=NaN(nb,nk);
gradients=NaN(1,nk);
algos=NaN(1,nk);
if isdir(filename)
    for col=1:nk
        steer=steerlist(col);
        [idx,grad,rsp,alg]=ebs.load_steerresp(sprintf(fn,steer));
        if idx == steer
            resp(:,col)=rsp;
            gradients(col)=grad;
            algos(col)=alg;
        end
    end
else % the input DIRNAME is filename
    dest=zeros(1,nkt);
    dest(steerlist)=1:length(steerlist);
    fid=fopen(filename,'rt');
    if fid > 0
        %				header : list of bpms
        cline=fgetl(fid);
        [~,~,strs]=celldeal(regexp(cline,'\t','split'));
        nbpms=length(strs);
        bpmid=zeros(1,nbpms);
        for bp=1:nbpms
            bpmid(bp)=ebs.bpmindex(strs{bp});
        end
        %				body : response
        for st=1:1000
            cline=fgetl(fid);
            if ~ischar(cline), break, end
            [idx,grad,rsp,alg]=ebs.get_steerresp(cline);
            
            col=dest(idx);
            if col > 0
                resp(bpmid,col)=rsp;
                gradients(col)=grad;
                algos(col)=alg;
            end
        end
        fclose(fid);
    else
        error('Resp:nofile',['Cannot open input global file ' filename])
    end
end

periods=1;
if mod(nk,periods) == 0
    rms_orbit=reshape(std2(resp,1),nk/periods,periods)';
    rms_orbit %#ok<NOPRT>
    average=mean2(rms_orbit) %#ok<NASGU,NOPRT>
    stdev=std2(rms_orbit) %#ok<NASGU,NOPRT>
end
norm=repmat(2.5*std2(resp,1,1),nb,1);
% resp([42;51],12)=0.01;  % outliers
% resp([111 7 25],27)=0;	% zero readings
% resp(62,:)=0.0001;      % fixed

failing=abs(std2(resp,1,2)) < 5.E-6;	% eliminate fixed
if disp1(failing,elfixed,'strange BPM readings (fixed value)...');
    resp(failing,:)=NaN;
end

outliers=(abs(resp) > norm);            % eliminate outliers
if disp2(outliers,eltoolarge,'strange BPM readings (more then 2.5*std)...');
    resp(outliers)=NaN;
end

zeroval=(resp == 0);                    % eliminate 0 readings
if disp2(zeroval,elzeros,'strange BPM readings (0.0 value)...');
    resp(zeroval)=NaN;
end

missing=~any(isfinite(resp),2);         % list missing
disp1(missing,1,' missing BPMs:');

    function rmv=disp1(wrong,action, message)
        if action <= 0 && any(wrong)
            fprintf('\n%d %s\n',sum(wrong),message);
            for bpm=find(wrong)'
                [bname,kdx]=ebs.bpmname(bpm);
                fprintf('%3i:%s\n',kdx,bname);
            end
            rmv=ask(action);
        else
            rmv=false;
        end
    end

    function rmv=disp2(wrong,action, message)
        if action <= 0 && any(any(wrong))
            fprintf('\n%d %s\nkick BPMlist\n',sum(sum(wrong)),message);
            for kick=find(any(wrong))
                fprintf('%s\t%s\n',ebs.steername(kick),num2str(find(wrong(:,kick))'));
            end
            rmv=ask(action);
        else
            rmv=false;
        end
    end
    function rmv=ask(action)
        if action==0
            rmv=~strcmp(input('suppress these data (y/n): ','s'),'n');
        else
            rmv=action<0;
        end
    end
end
