function resp=resonskew(atmodel)

[bpm,skew]=atmodel.get('bpm','skew');

% idskew=cellfun(@(n)ebs.skewindex(n),tango.Device('srmag/sqp/all').SteererNames.read');
% sext=sext(idskew,:);

nuh=atmodel.nuh;
nuv=atmodel.nuv;
h1=round(nuh+nuv);
h2=round(nuh-nuv);
p=0.5*(nuh+nuv-h1);
m=0.5*(nuh-nuv-h2);

ampscale=1; % scale to have maximum resonance amplitude ~= 1

scale=skew(:,4)';
resp=responsem(bpm(:,5:6),skew(:,5:6),nuv).*scale(ones(size(bpm,1),1),:);
%					response (2+2+320)x288
resp=[...
    ampscale*rnuxpnuz(skew,h1,nuh-p,nuv-p);...      % nux+nuz=104
    ampscale*rnuxmnuz(skew,h2,nuh-m,nuv+m);...      % nux-nuz=49
    ampscale*0.225*resp ...                         % dispersion
    ];

dlmwrite('skewcor.csv',resp,'precision',10);    % Response for the new device

%figure(2);
%resonplot(resp(1:4,:),[h1 h2]);

    function exc=rnuxpnuz(sext,p,nux,nuz)
        disp(['nuX + nuZ = ' num2str(p)]);
        phase=2*pi*(nux*sext(:,3)+nuz*sext(:,6)+(p-(nux+nuz))*sext(:,1));
        ampl=sqrt(sext(:,2).*sext(:,5));
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

    function exc=rnuxmnuz(sext,p,nux,nuz)
        disp(['nuX - nuZ = ' num2str(p)]);
        phase=2*pi*(nux*sext(:,3)-nuz*sext(:,6)+(p-(nux-nuz))*sext(:,1));
        ampl=sqrt(sext(:,2).*sext(:,5));
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end


end
