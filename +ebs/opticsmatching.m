function [rM, qpk,dqpk,qf8dpk]=opticsmatching(r,varargin)
% match lattice optics for S28D ESRF-EBS
%
% OUTPUT:
% rm    : S28D ESRF-EBS AT lattice with matched optics knobs
% qpk   : matched quadurpole strengths
% dqpk   : matched dipole-quadurpole strengths
%
% INPUT:
% r     : S28D ESRF-EBS AT lattice
%
% OPTIONAL INPUT (pairs 'name', value, 'name2',value,... ):
% 'mux'     : double (76.21) , total hor. phase advance
% 'muy'     : double (27.34) , total ver. phase advance
% 'rm12'    : double (0.20)  , transfer matrix element (1,2) between SF centers
% 'rm34'    : double (0.60)  , transfer matrix element (3,4) between SF centers
% 'bxid'    : double (6.90)  , horizontal beta at ID
% 'aysf'    : double (0.680) , vertical alpha at SF center
% 'bysf'    : double (5.40)  , vertical beta at SF center
% 'dxsf'    : double (0.0882), horizotnal dispersion at SF center
% 'bymid'   : double (4.70)  , vertical beta at DQ2C center
%
% 'matchCanting' : logical (true), match also canted cells (14,15,16,29,30)
% 'match2PW'     : logical (true), match also 2PW cells (1,5,7,14,25,29,31)
% 'matchSB'      : logical (true), match also SB cells (2,8,16,20,26,28,30,32)
% 'verbose'      : logical (false), print out text information
% 'noDQ'         : logical (false), excludes DQ quadrupoles for optics matching.
% 'noDQ1'        : logical (false), excludes DQ1 quadrupoles for optics matching.
%                  bymid, bxid removed from objectives
%
% DESCRIPTION:
% 0) separate ARCA (CELL06), ARCA_INJ, ARCB_INJ and split SF if necessary
% 1) ARCA matching
% 2) set matched quadrupole gradients from ARCA to all other cells
% 3) ARCA_INJ ARCB_INJ matching
% 4) match canted beamlines in ID 15(2.0 mrad),16(2.7 mrad),30(2.2 mrad)
%
% EXAMPLES:
% rm=ebs.opticsmatching(r);
% rm=ebs.opticsmatching(r,'verbose',true);
% rm=ebs.opticsmatching(r,'mux',76.47,'rm12',0.5,'noDQ',true);
% rm=ebs.opticsmatching(r,'chrom',[0,0],'muy',27.23,'noDQ',true);
%
% NOTES:
% assumes ID markers placed before cell ID04---CELL04---ID05---CELL05,....
%
%see also: ebs.matching.arc ebs.matching.inj ebs.matching.CantedOptics

p = inputParser;

defaultmux  = 76.21;
defaultmuy  = 27.34;
defaultrm12 = 0.20;     % detuning horizontal
defaultrm34 = 0.60;     % detuning crossed
defaultbxid = 6.9;
defaultaysf = 0.680;    % detuning vertical
defaultbysf = 5.40;
defaultdxsf = 0.0882;
defaultbymid= 4.70;
defaultrfv=6.5e6;
defaultchrom=[6,4];

addRequired(p,'r',@iscell);
addOptional(p,'mux',defaultmux,@isnumeric);
addOptional(p,'muy',defaultmuy,@isnumeric);
addOptional(p,'rm12',defaultrm12,@isnumeric);
addOptional(p,'rm34',defaultrm34,@isnumeric);
addOptional(p,'bxid',defaultbxid,@isnumeric);
addOptional(p,'aysf',defaultaysf,@isnumeric);
addOptional(p,'bysf',defaultbysf,@isnumeric);
addOptional(p,'dxsf',defaultdxsf,@isnumeric);
addOptional(p,'bymid',defaultbymid,@isnumeric);

addOptional(p,'matchCanting',true,@islogical);
addOptional(p,'match2PW',true,@islogical);
addOptional(p,'matchSB',true,@islogical);
addOptional(p,'verbose',false,@islogical);
addOptional(p,'noDQ',false,@islogical);
addOptional(p,'noDQ1',false,@islogical);

parse(p,r,varargin{:});

muxid      = p.Results.mux/32;
muyid      = p.Results.muy/32;
rm_sxt_12  = p.Results.rm12;
rm_sxt_34  = p.Results.rm34;
bxid       = p.Results.bxid;
aysf       = p.Results.aysf;
bysf       = p.Results.bysf;
dxsf       = p.Results.dxsf;
bymid      = p.Results.bymid;

matchCanting  = p.Results.matchCanting;
match2PW      = p.Results.match2PW;
matchSB       = p.Results.matchSB;
verbose       = p.Results.verbose;
noDQ       = p.Results.noDQ;
noDQ1       = p.Results.noDQ1;


%% reduce lattice (remove pinholes)
r_input=r;
rmod = ebs.model(r,'reduce',true,'keep','ID\w*|Canting\w*|CellCenter|\w*Dispersion\w*');

r = rmod.ring;

%% split lattice, obtain ARCA

% recover cells with adjusted standard cell matching
[startcell,endcell]=ebs.getcellstartend(r,6); % Cell 6 is sandard, 3 and 4 injection
ARCA = r(startcell:endcell);

% remove cavity from ARCs
indrfc=findcells(ARCA,'Class','RFCavity');
ARCA(indrfc)=[];

% split SF2
splitsf=[false false false];
splitdq=[false false false];

    function [arc,splitsf]=splitmag(arc,splitsf,magfam)
        ind=find(atgetcells(arc,'FamName',magfam))'; % get sf indexes
        nel=1;
        
        % do not split if already splitted
        if strcmp(magfam,'S[FIJ]2\w*')
            nel=2;
        end
        if strcmp(magfam,'DQ2\w*')
            nel=2;
        end
        
        if length(ind)==nel % split
            splitsf=true;
            if verbose, disp(['splitting ' magfam ' ']); end
            arc=atinsertelems(arc,ind,0.5,[]);
        end
    end

    function [arc]=mergemag(arc,splitsf,magfam)
        if splitsf % merge only if splitting took place previously in splitmag
            if verbose, disp(['merging ' magfam ' ']); end
            indsf=find(atgetcells(arc,'FamName',magfam))'; % get sf indexes
            L=atgetfieldvalues(arc,indsf(1:2:end),'Length');
            arc=atsetfieldvalues(arc,indsf(1:2:end),'Length',L*2);
            
            exfield=exitfields();
            for ii=1:2:length(indsf)
                [exitf,~]=mvfield(struct(),arc{indsf(ii+1)},exfield);    % extract exit fields
                arc{indsf(ii)}=mvfield(arc{indsf(ii)},exitf);     % Set back exit fields
            end
            
            bendind=atgetcells(arc(indsf),'BendingAngle');
            if ~isempty(find(bendind,1))
                indsf=indsf(bendind);
                B=atgetfieldvalues(arc,indsf,'BendingAngle');
                for ii=1:2:length(indsf)
                    arc=atsetfieldvalues(arc,indsf(ii),'BendingAngle',B(ii)+B(ii+1));
                end
            else
                disp('no bend')
            end
            
            arc(indsf(2:2:end))=[];
            
        end
    end

ARCA = splitmag(ARCA,splitsf(1),'S[FIJ]2\w*');
ARCA = splitmag(ARCA,splitdq(1),'DQ2C\w*');

% get current optics. go to matching only if modified.
if verbose
    disp('! - - - - - - - - - - - - - - - - - - !');
    disp('!  Optics matching input parameters   !');
    disp('!                                     !');
    disp(['! bysf: ' num2str(bysf,'%2.4f') ' ']);
    disp(['! aysf: ' num2str(aysf,'%2.4f') ' ']);
    disp(['! dxsf: ' num2str(dxsf,'%2.4f') ' ']);
    disp(['! rm_sxt_12: ' num2str(rm_sxt_12,'%2.4f') ' ']);
    disp(['! rm_sxt_34: ' num2str(rm_sxt_34,'%2.4f') ' ']);
    disp(['! bxid: ' num2str(bxid,'%2.4f') ' ']);
    disp(['! bymid: ' num2str(bymid,'%2.4f') ' ']);
    disp(['! muxid: ' num2str(muxid,'%2.4f') ' ']);
    disp(['! muyid: ' num2str(muyid,'%2.4f') ' ']);
    disp(['! Q_x: ' num2str(muxid*32,'%2.4f') ' ']);
    disp(['! Q_y: ' num2str(muyid*32,'%2.4f') ' ']);
    disp('!                                     !');
    disp('! - - - - - - - - - - - - - - - - - - !');
end

%% match optics in ARCA
if verbose, disp(['Match Cell' num2str(6)]); end

% match ARCA cell
indQF1=find(atgetcells(ARCA,'FamName','QF1\w*'))';
indQD2=find(atgetcells(ARCA,'FamName','QD2\w*'))';
indQD3=find(atgetcells(ARCA,'FamName','QD3\w*'))';
indQF4=find(atgetcells(ARCA,'FamName','QF4\w*'))';
indQD5=find(atgetcells(ARCA,'FamName','QD5\w*'))';
indQF6=find(atgetcells(ARCA,'FamName','QF6\w*'))';
indQF8=find(atgetcells(ARCA,'FamName','QF8\w*'))';
indDQ1=find(atgetcells(ARCA,'FamName','DQ1\w*'))';
indDQ2=find(atgetcells(ARCA,'FamName','DQ2\w*'))';
DBind =find(atgetcells(ARCA,'FamName','SF2\w*'))';
DBind =DBind(2:2:end);
centind=find(atgetcells(ARCA,'FamName','DQ2C\w*'))';
centind =centind(2:2:end);
mk_ids=length(ARCA);

ARCA_1=ebs.matching.arc(ARCA,...
    bysf,aysf,dxsf,bxid,...
    bymid,muxid,muyid,...
    rm_sxt_12,rm_sxt_34,...
    indQF1,...
    indQD2,...
    indQD3,...
    indQF4,...
    indQD5,...
    indQF6,...
    indQF8,...
    indDQ1,...
    indDQ2,...
    centind,...
    DBind,...
    mk_ids,...
    noDQ,...
    noDQ1,...
    verbose);

%% assign quarupole strengths as in ARCA_1 to all other cells
% this action make so that all other cells must be rematched anyway.
if verbose, disp(['assign gradients of cell ' num2str(6) ' to all other cells']); end

famnamesi={...
    'QF1[A,LM]\w*',...
    'QF1[E,LM]\w*',...
    'QD2[A,LM]\w*',...
    'QD2[E,LM]\w*',...
    'QD3[A,LM]\w*',...
    'QD3[E,LM]\w*',...
    'QF4[A,LM]\w*',...
    'QF4[B,LM]\w*',...
    'QF4[D,LM]\w*',...
    'QF4[E,LM]\w*',...
    'QD5[B,LM]\w*',...
    'QD5[D,LM]\w*',...
    'QF6B\w*',...
    'QF6D\w*',...
    'DQ1\w*',...
    'QF8B\w*',...
    'QF8D\w*',...
    'DQ2\w*',...
    'QF1[IJ]\w*',...
    'QD2[IJ]\w*',...
    'QD3[IJ]\w*',...
    'QF4[IJ]\w*',...
    }; % names in ARCA_INJ



% set ARCA optics to all cells
%ra=setquadK(ARCA,ARCA_1,r,famnamesi); % ra ring with arcA quad K in all cells
ra=copyquadK(ARCA_1,r,famnamesi); % ra ring with arcA quad K in all cells

% recover cells with adjusted standard cell matching
[startcell,endcell]=ebs.getcellstartend(ra,[6,4,3]); % Cell 6
ARCA_1  =ra(startcell(1):endcell(1));
ARCA_INJ=ra(startcell(2):endcell(2));
ARCB_INJ=ra(startcell(3):endcell(3));


[ARCA_1,   splitsf(1)] = splitmag(ARCA_1,splitsf(1),'S[FIJ]2\w*');
[ARCA_INJ, splitsf(2)] = splitmag(ARCA_INJ,splitsf(2),'S[FIJ]2\w*');
[ARCB_INJ, splitsf(3)] = splitmag(ARCB_INJ,splitsf(3),'S[FIJ]2\w*');

[ARCA_1,   splitdq(1)] = splitmag(ARCA_1,splitdq(1),'DQ2C\w*');
[ARCA_INJ, splitdq(2)] = splitmag(ARCA_INJ,splitdq(2),'DQ2C\w*');
[ARCB_INJ, splitdq(3)] = splitmag(ARCB_INJ,splitdq(3),'DQ2C\w*');

%% match optics in ARCB_INJ;ARCA_INJ
if verbose, disp(['Match cells ' num2str([3 4]) '']); end

[twid,~,~]=atlinopt(ARCA_1,0,1); %%%% ARCA_1 !!!!!!!
byid=twid.beta(2);
dxid=twid.Dispersion(1);
[twsf,~,~]=atlinopt(ARCA_1,0,DBind(1)); %%%% ARCA_1 !!!!!!!
bxsf=twsf.beta(1);


% match injection section
ARC_INJ=[ARCB_INJ;ARCA_INJ];

arc=ARC_INJ;

% match injection section
indQF1J=find(atgetcells(arc,'FamName','QF1J','QF1I'))';
indQD2J=find(atgetcells(arc,'FamName','QD2J','QD2I'))';
indQF2J=find(atgetcells(arc,'FamName','QF2J','QF2I'))';
indQD3J=find(atgetcells(arc,'FamName','QD3J','QD3I'))';
indQF4J=find(atgetcells(arc,'FamName','QF4J','QF4I'))';

indDR_01I=find(atgetcells(arc,'FamName','DR_01I'))';%,'DR_14'
indSJ2ACent=find(atgetcells(arc,'FamName','S[IJ]2[AE]'))';
indSJ2ACent=indSJ2ACent(2:2:end);
indCellCenter=find(atgetcells(arc,'FamName','DQ2C\w*'))';
indCellCenter=indCellCenter(2:2:end);

indDrSept=find(atgetcells(arc,'FamName','ID04'))';

axmidinj=0.0;

ARC_INJ_1=ebs.matching.inj(...
    arc,...
    bxid,byid,dxid,...
    aysf,bxsf,bysf,dxsf,...
    muxid,muyid,axmidinj,...
    indQF1J,...
    indQD2J,...
    indQF2J,...
    indQD3J,...
    indQF4J,...
    indDR_01I,...
    indSJ2ACent,...
    indCellCenter,...
    verbose);

% split injection cells
ARCA_INJ_1=ARC_INJ_1(indDrSept:end);
ARCB_INJ_1=ARC_INJ_1(1:indDrSept-1);

% % merge SF2  (not needed, later gradients copied into unmodified lattice)
%ARCA_1     = mergemag(ARCA_1,     splitsf(1),'S[FIJ]2\w*');
ARCA_INJ_1 = mergemag(ARCA_INJ_1, splitsf(2),'S[FIJ]2\w*');
ARCB_INJ_1 = mergemag(ARCB_INJ_1, splitsf(3),'S[FIJ]2\w*');
ARCA_1     = mergemag(ARCA_1,     splitdq(1),'DQ2C\w*');
ARCA_INJ_1 = mergemag(ARCA_INJ_1, splitdq(2),'DQ2C\w*');
ARCB_INJ_1 = mergemag(ARCB_INJ_1, splitdq(3),'DQ2C\w*');


%% rebuild lattice
% some cells have splitted elements, in this way the splitting is kept
% also usefull in future for 2PW, canting, SB introduction.
if verbose, disp(['include cells ' num2str([3 4]) ' in final lattice']); end

[startcell,endcell]=ebs.getcellstartend(ra,[5,2]); % Cell 5
ARCs30=ra(startcell(1):endcell(end));

rb=[ARCA_INJ_1;ARCs30;ARCB_INJ_1]; % canted cells not rematched here.

%% find, save and remove bending angles for 2PW, SB+ DQ12, QF8D ,.
% Canting and DL1[AE]_[15] kept for following matching

cantedid = [15,16,30];

dipBM = find(...
    atgetcells(rb,'FamName','DQ[12]\w*','mlDQ[12]\w*','W1','SMB','SBM','QF8D\w*') & ...
    atgetcells(rb,'PassMethod','BndMPoleSymplectic4Pass'));%

dipCant = find(atgetcells(rb,'FamName','Canting\w*ID\w*'));
dipDL0 = find(atgetcells(rb,'FamName','DL1[AE]_5'),1,'first');
ang_dipDL0 = atgetfieldvalues(rb,dipDL0,'BendingAngle');

dipDL = find(atgetcells(rb,'FamName','DL1[AE]_5_ID\w*'));

% keepDL=[];
% for ic=1:2:6
%     keepDL=[keepDL find(dipDL<dipCant(ic),1,'last')];
%     keepDL=[keepDL find(dipDL>dipCant(ic),1,'first')];
% end
% dipDL = dipDL(keepDL);

ang_dipBM = atgetfieldvalues(rb,dipBM,'BendingAngle');
ang_dipCant = atgetfieldvalues(rb,dipCant,'BendingAngle');
ang_dipDL = atgetfieldvalues(rb,dipDL,'BendingAngle');

% set to zero BM dipoles
rb_tmp= rb;

rb = atsetfieldvalues(rb,dipBM,'BendingAngle',0);
rb = atsetfieldvalues(rb,dipCant,'BendingAngle',0);
rb = atsetfieldvalues(rb,dipDL,'BendingAngle',ang_dipDL0); % DL canted back to nominal angle

% set ARCA bending to all cells
famnamesdq ={...
    'DQ1\w*',...
    'DQ2\w*',...
    'mlDQ1\w*',...
    'mlDQ2\w*',...
    };

% all bends to stadnard cll value
rb=setbend(ARCA_1,rb,famnamesdq); % ra ring with arcA dipole angles in all cells
%rb=copyquadK(ARCA_1,rb,famnamesi); % ra ring with arcA quadrupole fields in all cells


%% canted cells

r0 = [ARCA_INJ_1;repmat(ARCA_1,30,1);ARCB_INJ_1]; % no canting no BM.

disp([ 'tot angle/2/pi no BM no canting angles: ' num2str(sum(atgetfieldvalues(rb,atgetcells(rb,'BendingAngle'),'BendingAngle'))/2/pi)]);
disp([ 'tot angle/2/pi inj-30xarc-inj: ' num2str(sum(atgetfieldvalues(r0,atgetcells(r0,'BendingAngle'),'BendingAngle'))/2/pi)]);
disp([ 'tot angle/2/pi : ' num2str(sum(atgetfieldvalues(r,atgetcells(r,'BendingAngle'),'BendingAngle'))/2/pi)]);
if matchCanting
for idnum=cantedid
    
    % assign canted beamlines bending angles only up to present cell
    [cellstartindex,cellendindex]=ebs.getcellstartend(rb,idnum);
    midcell = floor(mean([cellstartindex,cellendindex]));
    rb = atsetfieldvalues(rb,dipCant(dipCant<midcell),'BendingAngle',ang_dipCant(dipCant<midcell));
    disp([ 'tot angle/2/pi no BM + canting dipole: ' num2str(sum(atgetfieldvalues(rb,atgetcells(rb,'BendingAngle'),'BendingAngle'))/2/pi)]);
    rb = atsetfieldvalues(rb,dipDL(dipDL<midcell),'BendingAngle',ang_dipDL(dipDL<midcell));
    disp([ 'tot angle/2/pi no BM + canting dipole + DL1[AE]_5: ' num2str(sum(atgetfieldvalues(rb,atgetcells(rb,'BendingAngle'),'BendingAngle'))/2/pi)]);
    % match
    rb = ebs.matching.CantingOptics( rb, r0, idnum, false, verbose );
    
    totang=sum(atgetfieldvalues(rb,atgetcells(rb,'BendingAngle'),'BendingAngle'))/2/pi;
    disp([ 'tot angle/2/pi no BM canting ' num2str(idnum) ' matched: ' num2str(totang)]);
    if (totang-1)>1e-9
        error('total angle not 2pi')
    end
end

end

%% restore 2PW and DQ angles for BM
rb0=rb;
% rb = atsetfieldvalues(rb,dipBM,'BendingAngle',ang_dipBM);

%% match 2PW and SB
% 2PW cells
%error('last debug the below part did not work! matching of 2PW bad. impossible to restore ebs.model')

% index of cells that have canting (on one or both sides).
id2pw=[1,5,7,14,25,29,31]; id2pwstandard=[1,5,7,25,31];
idsb=[2,8,16,20,26,28,30,32]; idsbstandard=[2,8,20,26,28,32];

idcant = sort([id2pw,idsb]);

tpwarc=[];
sbarc=[];

% separate cell of interest
[twi,~,~]=atlinopt(rb0,0,1:length(rb0)+1); % changed to lattice with Injection and canting
[cellstartindex,cellendindex]=ebs.getcellstartend(rb0,idcant); % Cell 5


% match canted beamlines optics
if match2PW || matchSB
    
    for id=1:length(idcant)
        if verbose, disp(['Match cell ' num2str(idcant(id)) '']); end
        idnum=idcant(id);
        
        % assign relevant dipole fields to rb
        [cellstartindexs,cellendindexs]=ebs.getcellstartend(rb,idnum);
        seldip=(dipBM<cellendindexs & dipBM>cellstartindexs);
        rb = atsetfieldvalues(rb,dipBM(seldip),'BendingAngle',ang_dipBM(seldip));
        disp([ 'tot angle/2/pi + BM : ' ...
            num2str(sum(atgetfieldvalues(rb,...
            atgetcells(rb,'BendingAngle'),'BendingAngle'))/2/pi)]);
        
        % changed to lattice with Injection and canting
        
        % get optics at reference locations from initial lattice
        twiin  = twi(cellstartindex(id));
        twiend = twi(cellendindex(id)+1);
        
        % mathced optics for central from intial cell
        % changed to lattice with Injection and canting
        sfaeind=find(atgetcells(rb0(1:cellendindex(id)+1),'FamName','DR_10','DR_35'),2,'last')+1;
        centind=find(atgetcells(rb0(1:cellendindex(id)+1),'FamName','DQ2C\w*'),1,'last');
        twimid=twi(centind);
        twisf=twi(sfaeind);
        
        
        % get arc to match and relevant locations from
        %arc=ra(cellstartindexa(id):cellendindexa(id));
        arc=rb(cellstartindex(id):cellendindex(id));
        sfaeind=find(atgetcells(arc,'FamName','DR_10','DR_35'),2,'last')+1;
        centind=find(atgetcells(arc,'FamName','DQ2C\w*'),1,'last');
        excludequad=false(19,1);
        %excludequad([8,10,11,12])=true; % exclude DQ and QF8B
        excludeconstr=false(19,1);
        %excludeconstr([5,6,8,9])=true; % exclude beta and alpha at SF (free in canted cells)
        
        % exclude DQ
        excludequad([8,10,12])=true;
        %excludeconstr([12,13,14])=true;
        
        if ismember(idcant(id),id2pw) % if 2PW
            excludequad(11)=true; % exclude tilted QF8D
        end
        
        % start matching from matched point if 1 2pw has already been matched
        if ~isempty(tpwarc) & ismember(idcan(id),id2pwstandard)
            arc=copyquadK(tpwarc,arc,famnamesi); % ra ring with arcA quad K in all cells
        end
        
        % start matching from matched point if 1 sb has already been matched
        if ~isempty(tpwarc) & ismember(idcan(id),idsbstandard)
            arc=copyquadK(sbarc,arc,famnamesi); % ra ring with arcA quad K in all cells
        end
        
%         rbm=rb;
%         if ismember(idcant(id),id2pwstandard) % if 2PW
%             
%             [rb,arc]=ebs.matching.TwoPoleWigglerOptics(rb,r0,idcant(id));
%             %save 2pw arc ,matched
%             if ~isempty(tpwarc) & ismember(idcan(id),id2pwstandard)
%                 tpwarc=arc;
%             end
%             
%         elseif ismember(idcant(id),idsbstandard) % if SB
%             
%             [rb,arc]=ebs.matching.ShortBendOptics(rb,r0,idcant(id));
%             
%             %save sb arc ,matched
%             if ~isempty(sbarc) & ismember(idcan(id),idsbstandard)
%                 sbarc=arc;
%             end
%             
%         else % canted + BM
%             
            % match as transfer line
            arc=ebs.matching.arcopen(...
                arc,...
                twiin,twiend,...
                twisf,...
                twimid,...
                sfaeind,...
                centind,...
                excludequad,...
                excludeconstr,...
                true);
            
            % save 2pw arc ,matched
            if ~isempty(tpwarc) & ismember(idcan(id),id2pwstandard)
                tpwarc=arc;
            end
            % save sb arc ,matched
            if ~isempty(sbarc) & ismember(idcan(id),idsbstandard)
                sbarc=arc;
            end
            
        % reconstruct lattice with new cell
        if verbose, disp(['include cell ' num2str(idcant(id)) ' in final lattice']); end
        rb=[rb(1:cellstartindex(id)-1);...
            arc;... % arcwith 2PW
            rb(cellendindex(id)+1:end)];
        
       % end
        
        
        
        if verbose
            figure('name',['cell ' num2str(idcant(id))]); atplot(arc,'inputtwiss',twiin);
            figure('name',['cell B0 ' num2str(idcant(id))]); atplot(arc,'inputtwiss',twiin,@plotB0curlyh);
            figure('name',['ring ' num2str(idcant(id))]); atplot(rb);
        end
    end
end


%% reconstruct lattice

% get matched quadrupole gradients
% qpib=atgetcells(rb,'Class','Quadrupole') ;
% qpk=atgetfieldvalues(rb,qpib,'PolynomB',{1,2});
% 
% % get matched quadrupole gradients
% q6ib=atgetcells(rb,'FamName','QF6D\w*') ;
% q6k=atgetfieldvalues(rb,q6ib,'PolynomB',{1,2});
% 
% % get matched dipole-quadrupole gradients
% dqpib=atgetcells(rb,'FamName','DQ\w*');
% dqpk=atgetfieldvalues(rb,dqpib,'PolynomB',{1,2});
% 
% % set rm gradients in r, to garantee correct ring size and elements.
% qpi=atgetcells(r,'Class','Quadrupole');
% rM=atsetfieldvalues(r,qpi,'PolynomB',{1,2},qpk);
% q6pi=atgetcells(rM,'FamName','QF6D\w*');
% rM=atsetfieldvalues(rM,q6pi,'PolynomB',{1,2},q6k);
% dqpi=atgetcells(rM,'FamName','DQ\w*');
% rM=atsetfieldvalues(rM,dqpi,'PolynomB',{1,2},dqpk);

% add back pinholes markers
rbmod = ebs.model(rb);
rM = rbmod.ring;
qpk = rbmod.getfieldvalue('qp','PolynomB',{1,2});
dqpk = rbmod.getfieldvalue('dq','PolynomB',{1,2});
qf8dpk = rbmod.getfieldvalue('qf8d','PolynomB',{1,2});

end



function [ri]=setbend(r0,ri,famnamesi)
% scales gradients of families listed in famnamesi from r0 to ri

for ifam=1:length(famnamesi)
    % get bending angle from r0
    ind=atgetcells(r0,'FamName',famnamesi{ifam});
    K=atgetfieldvalues(r0,ind,'BendingAngle',{1,1});
    % set bending angle percentual variation in ri
    try
        indi=atgetcells(ri,'FamName',famnamesi{ifam});
        ri=atsetfieldvalues(ri,indi,'BendingAngle',{1,1},K(1));
    catch
        disp([famnamesi{ifam} ' : not present in destination lattice']);
    end
end

end


function [ri]=setquadK(r0,rm,ri,famnamesi)
% scales gradients of families listed in famnamesi from r0 to ri

for ifam=1:length(famnamesi)
    % get gradient from r0
    ind=atgetcells(r0,'FamName',famnamesi{ifam});
    K=atgetfieldvalues(r0,ind,'PolynomB',{1,2});
    % get gradient from rm
    ind=atgetcells(rm,'FamName',famnamesi{ifam});
    Km=atgetfieldvalues(rm,ind,'PolynomB',{1,2});
    % set gradient percentual variation in ri
    try
        indi=atgetcells(ri,'FamName',famnamesi{ifam});
        Ki=atgetfieldvalues(ri,indi,'PolynomB',{1,2});
        ri=atsetfieldvalues(ri,indi,'PolynomB',{1,2},Ki.*(Km(1)./K(1)));
        %disp(Km./K)
        % ri=atsetfieldvalues(ri,indi,'PolynomB',{1,2},Ki + (Km-K));
    catch
        disp([famnamesi{ifam} ' : not present in destination lattice']);
    end
end

end



function [ri]=copyquadK(r0,ri,famnamesi)
% scales gradients of families listed in famnamesi from r0 to ri

for ifam=1:length(famnamesi)
    % get gradient from r0
    ind=atgetcells(r0,'FamName',famnamesi{ifam});
    K=atgetfieldvalues(r0,ind,'PolynomB',{1,2});
    % set gradient in ri
    try
        indi=atgetcells(ri,'FamName',famnamesi{ifam});
        ri=atsetfieldvalues(ri,indi,'PolynomB',{1,2},K(1));
    catch
        disp([famnamesi{ifam} ' : not present in destination lattice']);
    end
end

end
