function resp=resonquad(atmodel)

% reduced model with merged DQ (3 of them have a pinhole marker)
atmodel=ebs.model(atmodel.ring,'reduce',true,'keep','BPM.*|ID.*|2PW*|QF8D*');

% pure quadrupoles
iq  = atmodel.get(0,'qp');
q  = atmodel.get('qp');

% dipole quadrupoles
idq = atmodel.get(0,'dq');
dq = atmodel.get('dq');

% rotated quadrupoles
iqf8d = atmodel.get(0,'qf8d');
q8 = atmodel.get('qf8d');
% count q8 slices
i2pw = atmodel.get(0,'2pw');
n2pw = length(i2pw);
nslices = length(iqf8d)/n2pw; % slices for each qf8d
% average qf8 parameters over slices
for i2 = 1:n2pw
    q8red(i2,:)=mean(q8((nslices*(i2-1)+1):(nslices*i2),:));
end

[~,iqa]= sort([iq;idq;iqf8d(floor(nslices/2):nslices:end);]); % get quadrupoles order

Kqa=[q; dq; q8red;];
quad=Kqa(iqa,:);


% % order magnets as in control system
% idquad=cellfun(@(a)ebs.qpindex(a(find(a==' ',1,'last')+1:end)),tango.Device('Tango://ebs-simu:10000/srmag/m-q/all').PowerSupplyNames.read');
% quad=quad(idquad,:);

nuh=atmodel.nuh;
nuv=atmodel.nuv;
px1=ceil(2*nuh);
px2=floor(2*nuh);
pz1=ceil(2*nuv);
pz2=floor(2*nuv);

ampscale=1; % scale to have maximum resonance amplitude ~= 1

%				response 8x514+96
resp=[...
    ampscale*r2nux(quad,px1,0.5*px1);...    % 2nux=153
    ampscale*r2nux(quad,px2,0.5*px2);...    % 2nux=152
    ampscale*r2nuz(quad,pz1,0.5*pz1);...    % 2nuz=55
    ampscale*r2nuz(quad,pz2,0.5*pz2);...    % 2nuz=54
    ];

dlmwrite('quadcor.csv',resp,'precision',10);    % Response for the new device

% figure(1);
% resonplot(resp,[px1 px2 pz1 pz2]);

    function exc=r2nux(sext,p,nux)
        disp(['2*nuX = ' num2str(p)]);
        phase=2*pi*(2*nux*sext(:,3)+(p-2*nux)*sext(:,1));
        exc=[sext(:,2).*cos(phase) sext(:,2).*sin(phase)]';
    end

    function exc=r2nuz(sext,p,nuz)
        disp(['2*nuZ = ' num2str(p)]);
        phase=2*pi*(2*nuz*sext(:,6)+(p-2*nuz)*sext(:,1));
        exc=[sext(:,5).*cos(phase) sext(:,5).*sin(phase)]';
    end


end
