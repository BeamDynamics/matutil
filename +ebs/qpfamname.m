function varargout=qpfamname(idx,format)
%EBS.QPFAMNAME	returns the name of quadrupole with index idx
%
%QPNAME=QPFAMNAME(IDX)	returns the name of Sextupole with index IDX
% IDX: vector of indexes in the range [1 196] (1 is C4-1)
%       or n x 2 matrix in the form [cell index] with
%       cell in [1 32], index in [1 7]
%
%QPNAME=QPFAMNAME(IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : 'srmag/m-q******')
%
%[NM1,NM2...]=QPFAMNAME(...)    May be used to generate several names
%
%[...,KDX]=QPFAMNAME(...)       returns in addition the "hardware" index KDX
%
%KDX=QPFAMNAME(IDX,'')          returns only the "hardware" index KDX
% 
% examples : 
% >> ebs.qpfamname(192)
% >> ebs.qpfamname([320 234 10]')
%
%   See also QPINDEX

if nargin < 2, format='srmag/m-q'; end

[kdx,celnum,id]=ebs.cellnum([17,repmat(16,1,30),17],idx);


if isempty(format) || strcmp(format,'noname')
    varargout={};
else
    
    devnamefun= @(celnum,id)textfunc(format,celnum,id);
   
    nms=arrayfun(devnamefun,celnum,id,'UniformOutput',false);
    nin=length(nms);
    if (nargout==nin) || (nargout==(nin+1))
        varargout=nms;
    else
        varargout={nms};
    end
end

nout=length(varargout);
if nargout > nout
    varargout{nout+1}=kdx;
end

    function devnam=textfunc(format,celnum,id)
        if celnum~=3 && celnum~=4
            num='1234456886544321';
            pos=arrayfun(@(a)'all',1:16,'un',0);
            foc='fddffdffffdffddf';
        elseif celnum==3
            num='12344568865443221';
            pos=[arrayfun(@(a)'all',1:12,'un',0),...
                 arrayfun(@(a)'inject',1:5,'un',0)...
                ];
            foc='fddffdffffdffdfdf';
        elseif celnum==4
            num='12234456886544321';
            pos=[arrayfun(@(a)'inject',1:5,'un',0),...
                 arrayfun(@(a)'all',1:12,'un',0)...
                ];
            foc='fdfdffdffffdffddf';
        end
        
        devnam=[format foc(id) num(id) '/' pos{id}];
        
    end

end
