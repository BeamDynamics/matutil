function [cellstartindex,cellendindex]=getcellstartend(r,idnum)
%[cellstartindex,cellendindex]=getcellstartend(r,idnum)
%returns indexes of start and end of given cells idnum marked buy an ID*
%marker at the begining. cells are numbered [4-32 1-3]
% 
% >> [cellstartindex,cellendindex]=ebs.getcellstartend(LOW_EMIT_RING_INJ,[3,4])
% 
%see also: 

cellstartindex=find(atgetcells(r,'FamName','ID\w*'));
cellendindex=[cellstartindex(2:end)-1; length(r)];

cellstartindex=cellstartindex([30:32,1:29]);
cellendindex=cellendindex([30:32,1:29]);

cellstartindex=cellstartindex(idnum);
cellendindex=cellendindex(idnum);

end