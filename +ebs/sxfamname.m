function varargout=sxfamname(idx,format)
%EBS.sxfamname	returns the name of Sextupole with index idx
%
%SXNAME=sxfamname(IDX)	returns the name of Sextupole with index IDX
% IDX: vector of indexes in the range [1 196] (1 is C4-1)
%       or n x 2 matrix in the form [cell index] with
%       cell in [1 32], index in [1 7]
%
%SXNAME=sxfamname(IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : 'srmag/m-s******')
%
%[NM1,NM2...]=sxfamname(...)    May be used to generate several names
%
%[...,KDX]=sxfamname(...)       returns in addition the "hardware" index KDX
%
%KDX=sxfamname(IDX,'')          returns only the "hardware" index KDX
% 
% examples : 
% >> ebs.sxfamname(192)
% ans = 
%     'srmag/m-sd1ae/inj-inner'
% >>ebs.sxfamname([192 32 10]')
% ans = 
%     'srmag/m-sd1ae/inj-inner'
%     'srmag/m-sf2/all'
%     'srmag/m-sd1bd/all'
%
%   See also SXINDEX

if nargin < 2, format='srmag/m-s'; end

[kdx,celnum,id]=ebs.cellnum(6,idx);

if isempty(format) || strcmp(format,'noname')
    varargout={};
else
    
    devnamefun= @(celnum,id)textfunc(format,celnum,id);
   
    nms=arrayfun(devnamefun,celnum,id,'UniformOutput',false);
    nin=length(nms);
    if (nargout==nin) || (nargout==(nin+1))
        varargout=nms;
    else
        varargout={nms};
    end
end

nout=length(varargout);
if nargout > nout
    varargout{nout+1}=kdx;
end

    function devnam=textfunc(format,celnum,id)
        if celnum==3
            num='121121';
            pos={'ae','inj-outer';...
                'ae','inj-outer';...
                'bd','inj-outer';...
                'bd','inj-inner';...
                'ae','inj-inner';...
                'ae','inj-inner';...
                };
           foc='dfddfd';
        elseif celnum==4
            num='121121';
            pos={'ae','inj-inner';...
                'ae','inj-inner';...
                'bd','inj-inner';...
                'bd','inj-outer';...
                'ae','inj-outer';...
                'ae','inj-outer';...
                };
           foc='dfddfd';
        else
            num='121121';
            pos={'ae','all';...
                '','all';...
                'bd','all';...
                'bd','all';...
                '','all';...
                'ae','all';...
                };
            foc='dfddfd';
        end
        devnam=[format foc(id) num(id) pos{id,1} '/' pos{id,2}];
        
    end

end
