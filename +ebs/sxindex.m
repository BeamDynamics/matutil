function [idx]=sxindex(varargin)
%SXINDEX	find index of sextupole from its name
%
%IDX=SXINDEX(SXNAME)
%   SXNAME:    string, char array or cell array of strings
%
%IDX=SXINDEX(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:7
%   CELL and ID must have the same dimensions or be a scalar
%
%   See also SR.SXNAME

persistent sl
if isempty(sl)
    sl={'sd1a','sf2a','sd1b','sd1d','sf2e','sd1e'};
end

[idx]=ebs.anyindex(6,@bindex,varargin{:});

    function [cellnum,id]=bindex(bname)       
        qpattern='(?:^srmag/m-|^m-|^)(?<sextname>s[df][12])/c(?<cellname>\d{1,2})-(?<celpos>[abde])$';
        toks=regexpi(bname,qpattern,'names');
        if isempty(toks),error('SX:wrongName','Wrong sextupole name'); end
        cellnum=str2double(toks.cellname);
        id=find(strcmp([toks.sextname toks.celpos],sl));
        if isempty(id), error('SX:wrongName','Wrong sextuopole name'); end
    end
end

