function magnetsstrengths_model(filename,varargin)
%EBS.magnetsstrengths_model    Store magnets strengths in three files:
% Design.ts : for the nominal model strenghts
% Correctors.ts : for the quadrupole, sextupole and octupole correctors
% Steerers.ts : for orbit steerers and DQ coils
% 
% EBS.magnetsstrengths_model(FILENAME,atmodel)
% FILENAME is a cell array
%
% atmodel:  AT model class object
%
%magnetsstrengths_model()               takes the lattice from the current directory
%
%magnetsstrengths_model('opticsname')   takes the lattice from
%           $APPHOME/ebs/optics/settings/opticsname/betamodel.mat
%magnetsstrengths_model(path)           takes the lattice from
%           path or path/betamodel.mat or path/betamodel.str
%magnetsstrengths_model(AT)             Uses the given AT structure
%
%see also: eb.read_mag ebs.magname ebs.model

args={pwd};
args(1:length(varargin))=varargin;
pth=args{1};

ebsmodel=ebs.model(pth,'reduce',true,'keep','BPM.*|ID.*');
save('modellat','ebsmodel');

% file contains quadrupoles (in lttice order, quadrupoles and DQ), then sextupoles, then octupoles.
iq  = ebsmodel.get(0,'qp');
idq = ebsmodel.get(0,'dq');
[~,iqa]= sort([iq;idq]); % get quadrupoles order

Kq  = ebsmodel.get(1,'qp');
Kdq = ebsmodel.get(1,'dq');

Kqa=[Kq; Kdq];
Kqa=Kqa(iqa);

Ks  = ebsmodel.get(1,'sx');
Ko  = ebsmodel.get(1,'oc');

Kqdevlist=[...
ebsmodel.getfieldvalue('qp','Device');...
    ebsmodel.getfieldvalue('dq','Device');...
    ];
devlist=[...
    Kqdevlist(iqa);...
    ebsmodel.getfieldvalue('sx','Device');...
    ebsmodel.getfieldvalue('oc','Device')];

strength=[Kqa;Ks;Ko];

[fp,message]=fopen(filename,'wt');
if fp < 0
    error('File:Open','%s: %s',message,filename);
else
    for dev=1:length(devlist)
        fprintf(fp,['%s,' repmat('%.8f',1,length(strength(dev))) '\n'],devlist{dev},strength(dev));
    end
    fclose(fp);
end
end

