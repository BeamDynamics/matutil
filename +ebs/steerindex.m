function [idx,plane]=steerindex(varargin)
%STEERINDEX	find index of Steerer with name steername
%
%IDX=STEERINDEX(STEERNAME)
%   STEERNAME:    string, char array or cell array of strings
%                 HST-SH1/C04-A
%                 hst-sh1/c04-a/Strength  
%                 SRMAG/hst-sh1/c04-a  
% 
%IDX=STEERINDEX(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:9
%   CELL and ID must have the same dimensions or be a scalar
%
%[IDX,PLANE]=STEERINDEX(...)
%   Returns the hardware index and the plane of the last steerer in the list
%
%   See also EBS.STEERNAME

persistent sl
if isempty(sl)
    sl={'sh1-a','sd1-a','sf2-a','sd1-b','sh2-b','sd1-d','sf2-e','sd1-e','sh3-e'};
end

[idx]=ebs.anyindex(9,@bindex,varargin{:});

    function [cellnum,id]=bindex(bname)       
        qpattern='(?:^srmag/|^)(?<plane>hst-|vst-|sqp-|^|\w{0,0})(?<sextname>s[hdf][123])/c(?<cellname>\d{1,2})-(?<celpos>[abde])(?:/\w*$|$)';
        toks=regexpi(bname,qpattern,'names');
        if isempty(toks),error('SX:wrongName',['Wrong steerer name ' bname]); end
        if ~isempty(toks.plane), plane=upper(toks.plane(1));else plane=toks.plane; end
        cellnum=str2double(toks.cellname);
        id=find(strcmpi([toks.sextname '-' toks.celpos],sl));
        if isempty(id), error('SX:wrongName',['Wrong steerer name ' bname]); end
    end
end
