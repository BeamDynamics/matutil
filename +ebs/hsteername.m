function varargout=hsteername(idx,format)
%HSTEERNAME	returns the name of Steerer with index idx
%
%HSTEERNAME=HSTEERNAME(IDX)		returns the name of steerer with index IDX
% IDX: vector of indexes in the range 1:288 (1 is (h|v)st-sh1/c04-a)
%       logical array of length 
%       or n x 2 matrix in the form [cell index] with
%       or 1 x n array for the names of n magnets
%       cell in 1:32, index in 1:9
%
%HSTEERNAME=HSTEERNAME(IDX,FORMAT)	uses FORMAT to generate the name
%					default : '', 
%                   'hst-' to obtain horizontal steerers
%                   'vst-' to obtain vertical steerers
%                   'sqp-' to obtain skew quadrupoles
%                   
%[NM1,NM2...]=HSTEERNAME(...)		May be used to generate several names
%
% for horizontal steerers names
% ebs.hsteername([1,9,103],'hst-')% array
% ebs.hsteername([19,4],'hst-')% cell idx
% 
% for vertical steerers names
% ebs.steername([1,23,59],'vst-') % array
% ebs.steername([23,1],'vst-') % cell idx
%  
% for skew quadrupole names
% ebs.steername([1,23,59],'sqp-') % array
% ebs.steername([23,1],'sqp-') % cell idx
% 
%   See also EBS.HSTEERINDEX, EBS.STEERINDEX, EBS.QPNAME, EBS.BPMNAME, EBS.SXNAME

persistent fmt
if isempty(fmt)
fmt={'sh1/c%02d-a','sd1/c%02d-a','sf2/c%02d-a','sd1/c%02d-b',...
     'dq1/c%02d-b','sh2/c%02d-b','dq2/c%02d-c','dq1/c%02d-d',...
     'sd1/c%02d-d','sf2/c%02d-e','sd1/c%02d-e','sh3/c%02d-e'};
end

if nargin < 2, format=''; end

[~,celnum,id]=ebs.cellnum(12,idx);
textfunc=@(cellnum,id) sprintf([format fmt{id}],cellnum);
nms=arrayfun(textfunc,celnum,id,'UniformOutput',false);
if nargout==length(nms)
    varargout=nms;
else
    varargout={nms};
end
end
