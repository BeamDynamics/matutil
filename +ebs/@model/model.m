classdef model < atmodel
    %Class providing access to SR optical values
    %
    %Available locations:
    %
    %bpm:       All BPMs
    %hbpm:      Horizontal BPMs
    %vbpm:      Vertical BPMs
    %di:        All dipole
    %qp:        All quadrupoles
    %qf8d:      Tilted QF8D quadrupoles (splitted for 2PW modelling)
    %dq:        All dipole-quadrupoles
    %sx:        All sextpoles
    %oc:        All octupoles
    %ki:        All kickers
    %steerh:    Horizontal steerers
    %steerv:    Vertical steerers
    %skew:      skew quadrupoles
    %id:        All IDs
    %idx:       ID n
    %bm:        All BMs 
    %bmx:       BM n
    %3pw:       All 3PW 
    %2pw:       All 2PW 
    %2pwa:      All 2PW configuration A 
    %2pwb:      All 2PW configuration B
    %sbm:       All SBM 
    %diag:      Diagnostics section (DR_38)
    %kickers:   K3, K4, K1, K2
    %pinhole    All 5 pinhole source points
    %pinholeID07  Pinhole id 07
    %pinholeD17	Pinhole bm 17
    %pinholeID25 Pinhole id 25
    %pinholeD27	Pinhole bm 27
    %pinholeD01	Pinhole bm 01
    %
    %
    %Available parameters:
    %
    %0:     index in the AT structure
    %1:     Integrated strength
    %2:     theta/2pi
    %3:     betax
    %4:     alphax
    %5:     phix/2pi/nux
    %6:     etax
    %7:     eta'x
    %8:     betaz
    %9:     alphaz
    %10:    phiz/2pi/nux
    %11:    etaz
    %12:    eta'z
    %
    %Default parameters:
    %
    %[   2         3         5        6      8       10      ]
    %[theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux]

    properties (Constant, Hidden)
        pinhole_id=[7 25]
        pinhole_bm=[17 27 1]
    end
    
    properties (Constant, Access=private, Hidden)
        directory=fullfile(getenv('APPHOME'),'ebs','optics','settings')
    end

    methods (Static)
        newring=checklattice(ring,varargin)
        
        function pth=getdirectory(varargin)
            nm=getargs(varargin,{'theory'});
            pth=fullfile(ebs.model.directory,nm);
        end
    end
    
    methods
        function this=model(varargin)
            %Builds the linear model of the ring
            %
            %M=EBS.MODEL(AT)            Uses the given AT structure
            %
            %M=EBS.MODEL()              takes the lattice from
            %           $APPHOME/ebs/optics/settings/theory/betamodel.mat
            %
            %M=EBS.MODEL('opticsname')  takes the lattice from
            %           $APPHOME/ebs/optics/settings/opticsname/betamodel.mat
            %
            %M=EBS.MODEL(path)          takes the lattice from
            %           path/betamodel.mat
            
            this@atmodel(ebs.getat(varargin{:}));
        end
    end
    
  
    methods (Access=protected)
        
        function recompute(this,varargin)
            
            recompute@atmodel(this,varargin{:});
            
            % add dq integrated field (EBS specific)
            idz=this.getid('dq');
            this.vlave(idz,12)=setintegfield(this.ring(idz),'PolynomB',{2});
            idz=this.getid('qf8d');
            this.vlave(idz,12)=setintegfield(this.ring(idz),'PolynomB',{2});
            this.modified=false;
            this.emith_=[];    % Triger computation of emittances
            
            function v=setintegfield(at,varargin)
                k=atgetfieldvalues(at,varargin{:});
                l=atgetfieldvalues(at,'Length');
                v=k.*l;
            end
        end
        
        function idx=elselect(this,code)
            %Return indices of selected elements
            switch lower(code)
                case {'bpm','hbpm','vbpm'}
                    idx=atgetcells(this.ring,'Class','Monitor');
                case 'steerh'
                    idx=atgetcells(this.ring,'FamName','SH..') | ...
                        atgetcells(this.ring,'Class','Sextupole');
                case 'steerv'
                    idx=atgetcells(this.ring,'FamName','SH..') | ...
                        atgetcells(this.ring,'Class','Sextupole');
                case 'skew'
                    idx=atgetcells(this.ring,'FamName','SH..') | ...
                        atgetcells(this.ring,'Class','Sextupole');
                case 'sx'
                    idx=atgetcells(this.ring,'Class','Sextupole');
                case 'oc'
                    idx=atgetcells(this.ring,'FamName','O[FDIJ]\w*');
                case 'qp'
                    idx=atgetcells(this.ring,'Class','Quadrupole');
                case 'qf8d'
                    idx=atgetcells(this.ring,'FamName','QF8D\w*') & atgetcells(this.ring,'Class','Bend') ;
                case 'dq'
                    idx=atgetcells(this.ring,'FamName','DQ\w*');
                case 'di'
                    idx=atgetcells(this.ring,'Class','Bend');
                case 'ki'
                    idx=atgetcells(this.ring,'FamName','DR_K[1234]');
                case 'pinhole'
                    idx=atgetcells(this.ring,'FamName','PINHOLE_.*');
                case 'id'
                    idx=atgetcells(this.ring,'FamName','IDMarker$|^id\d+');
                case 'bm'
                    idx=atgetcells(this.ring,'FamName','BM\w*|^bm\d+');
                case '3pw'
                    idx=atgetcells(this.ring,'FamName','3PW\w*-w*');
                case '2pw'
                    idx=atgetcells(this.ring,'FamName','2PW[AB]-\w*');
                case '2pwa'
                    idx=atgetcells(this.ring,'FamName','2PWA-\w*');
                case '2pwb'
                    idx=atgetcells(this.ring,'FamName','2PWB-\w*');
                case 'sbm'
                    idx=atgetcells(this.ring,'FamName','SBM-\w*');
                case 'diag'
                    idx=atgetcells(this.ring,'FamName','DR_38$|^DR_K1');    % K1 occupies the space in cell 3
                case 'kickers'
                    idx=atgetcells(this.ring,'FamName','DR_K[1234]');
                otherwise
                    if strncmp(code,'id',2) % relies on "case 'id'" above
                        idx=this.mcellid(this.getid('id'),str2double(code(3:end)),1);
                    elseif strncmp(code,'bm',2) % relies on "case 'bm'" above
                        idx=this.mcellid(this.getid('id'),str2double(code(3:end)),1);
                    elseif strncmp(code,'pinhole',7) % relies on "case 'pinhole'" above
                        idx=atgetcells(this.ring,'FamName',['PINHOLE_' upper(code(8:end))]);
                    else
                        idx=atgetcells(this.ring,'FamName',upper(code));
                    end
            end
        end
        
        
        
    end
    
    methods (Access=public)
        
        function settune(this,tunes,varargin) 
            %Change tunes
            %
            %SETTUNE(TUNES,tunefixmode)
            % TUNES:    1x2 vector of desired tunes
            % tunefixmode :    string, 'QF1QD2' (default) or 'Matching'
            % varargin : 'name', value,... see ebs.opticsmatching
            %           use only if tunefixmode is 'Matching'
            %
            %
            %see also: atfittune ebs.opticsmatching
            
            
            expectedmode={'QF1QD2','Matching'};
            
            if ~isempty(varargin)
                tunefixmode=varargin{1};
            else
                tunefixmode=expectedmode{1};
            end
            
            switch tunefixmode
                
                case expectedmode{1}
                    
                    % move tune to Q1
                    rt=atfittune(this.ring,tunes,'QF1\w*','QD2\w*');
                    qpk=atgetfieldvalues(rt,this.getid('qp'),'PolynomB',{1,2});
                    this.setfieldvalue('qp','PolynomB',{1,2},qpk);
                     
                case expectedmode{2}
                    % match tune
                    muxc = tunes(1);
                    muyc = tunes(2);
                    
                    if tunes(1)<1
                        muxc=(76 + tunes(1));
                    end
                    if tunes(2)<1
                        muyc=(27 + tunes(2));
                    end
                    
                    [~,qpk,dqk,qf8dk]=ebs.opticsmatching(this.ring,...
                        'mux',muxc,'muy',muyc,...
                        'noDQ',true,varargin{2:end});
                    
                    this.setfieldvalue('qp','PolynomB',{1,2},qpk)
                    this.setfieldvalue('dq','PolynomB',{1,2},dqk)
                    this.setfieldvalue('qf8d','PolynomB',{1,2},qf8dk)
                    
                otherwise
                    
                error('tunefix mode not correct. see help ebs.model.settune');    
            end
            
            % at the next request for optics, recompute them.
            this.modified=true;
            this.emith_=[];
        end
        
        function setchrom(this,chrom,varargin)
            %Change chromaticity
            %
            %setchrom(chrom,chromfixmode)
            % CHROM:    1x2 vector of desired chromaticity
            % chromfixmode :    string, 'SF2SD1' (default) or 'InjSext'
            % varargin : 'name', value,... see ebs.fixchrominjsext
            %           use only if tunefixmode is 'InjSext'
            %
            %
            %see also: atfitchrom ebs.fixchrominjsext
            
            expectedmode={'SF2SD1','InjSext'};
            
            if ~isempty(varargin)
                chromfixmode=varargin{1};
            else
                chromfixmode=expectedmode{1};
            end
            
            switch chromfixmode
                
                case expectedmode{1}
                    
                    % move tune to Q1
                    rt=atfitchrom(this.ring,chrom,'S[IJD]1\w*','S[IJF]2\w*');
                    sxk=atgetfieldvalues(rt,this.getid('sx'),'PolynomB',{1,3});
                    this.setfieldvalue('sx','PolynomB',{1,3},sxk);
                     
                case expectedmode{2}
                    error('not done yet, use SF2SD1')
            end
            
            % at the next request for optics, recompute them.
            this.modified=true;
            this.emith_=[];
        end
        
        function setinjecetionbump(this,amplitude,varargin) 
        % 
        % computers gradients for injection bump kickers at given amplitude
        %
            K=ebs.mathcinjectionbump(this.ring,amplitude,varargin{:});
            this.setfieldvalue('ki','PolynomB',{1,1},K');
            this.modified=true;
            this.emith_=[];       
            
        end
        
    end
end

