function ring = checklattice(ring,varargin)
%NEWRING = EBS.CHECKLATTICE(RING)   Check the validity of an EBS lattice
%
%CHECKLATTICE will
%-  set the QUADRUPOLE, BEND and MULTIPOLE pass methods
%-  Add if necessary markers at ID locations (centre of straight sections before the cell)
%-  Add if necessary markers at BM locations 
%-  Add if necessary markers at pinhole camera locations
%-  Rename the BPMs with their device name
%
%CHECKLATTICE(...,'energy',energy)
%   Set the ring energy
%
%CHECKLATTICE(...,'reduce',true,'keep',pattern)
%   Remove elements with PassMethod='IdentityPass' and merge adjacent
%   similar elements, but keeps elements with FamName matching "pattern"
%   Pattern may be a logical mask.
%   (Default: 'BPM.*|ID.*' for SR, '^BPM_..|^ID.*|JL1[AE].*|CA[02][57]').
%
%CHECKLATTICE(...,'remove',famnames)
%   remove elements identified by the family names (cell array)
%
%CHECKLATTICE(...,'reduce',true)
%   group similar elements together by calling atreduce
%
%CHECKLATTICE(...,'MaxOrder',n)
%   Limit the order of polynomial field expansion to n at maximum
%
%CHECKLATTICE(...,'NumIntSteps',m)
%   Set the NumIntSteps integration parameter to at least m
%
%CHECKLATTICE(...,'QuadFields',quadfields)
%   Set quadrupoles fields to the specified ones.
%   Default: {}
%
%CHECKLATTICE(...,'BendFields',bendfields)
%   Set bending magnet fields to the specified ones.
%   Default: {}
%
%CHECKLATTICE(...,'DeviceNames',true)
%   add device names in AT structure.

[check,options]=getoption(varargin,'CheckLattice',true);
if check
    [energy,periods]=atenergy(ring);
    [energy,options]=getoption(options,'Energy',energy);
    [periods,options]=getoption(options,'Periods',periods);
    reduce=getoption(options,'reduce',false);
    [keep,options]=getoption(options,'keep',false(size(ring)));
    if ~islogical(keep), keep=atgetcells(ring,'FamName',keep); end
    [devnam,options]=getoption(options,'DeviceNames',true);
    
    cellnb=circshift(1:32,[0 -3]);
    
    precious=atgetcells(ring,'FamName','ID.*$|^JL1[AE].*') |...
        atgetcells(ring,'FamName','BM\w*') |...
        atgetcells(ring,'Class','Monitor');
    ring=atclean(ring,options{:},'keep',keep|precious);
    ring=rename_bpms(ring);
    ring=rename_ids(ring);
    ring=rename_bms(ring);
    
    if ~reduce
        ring=addpinholes(ring);
    end
    
    if devnam
        ring=adddevicenames(ring);
    end
    
    rp=atgetcells(ring,'Class','RingParam');
    if any(rp)
        params=ring(rp);
        params{1}.Energy=energy;
        params{1}.Periodicity=periods;
    else
        params={atringparam('EBS',energy,periods)};
    end
    ring=[params(1);ring(~rp)];
end

    function mach=rename_bpms(mach)
        bpmid=atgetcells(mach,'FamName','BPM_..');
        if sum(bpmid) == 320    % Full machine
            fprintf('Rename BPMs...\n');
            [cell,nb]=meshgrid(cellnb,1:10);
            bpmname=arrayfun(@(c,i) sprintf('BPM_C%02d-%02d',c,i),cell(:),nb(:),'UniformOutput',false);
            mach(bpmid)=atsetfieldvalues(mach(bpmid),'FamName',bpmname);
        end
    end

    function mach=rename_ids(mach)
        idid=atgetcells(mach,'FamName','IDMarker');
        if sum(idid) == 32      % Full machine
            
                fprintf('Rename IDs...\n');
                idname=arrayfun(@(c) sprintf('ID%02d',c),cellnb(:),'UniformOutput',false);
                mach(idid)=atsetfieldvalues(mach(idid),'FamName',idname);
           
        end
    end

    function mach=rename_bms(mach)
        
        idid=atgetcells(mach,'FamName','BMHARD');
        BMtype=cell(sum(idid),1);
        BMtype([1,5,7,25,31])={'2PW_A'};
        BMtype([14,29])={'2PW_B'};
        BMtype(23)={'3PW'};
        BMtype(18)={'3PW18'}; 
        BMtype([2,8,16,20,26,28,30,32])={'SBM'};

        if sum(idid) == 32      % Full machine
            
            fprintf('Rename BMs...\n');
            idname=arrayfun(@(c) sprintf('BM%02d',c),cellnb(:),'UniformOutput',false);
            mach(idid)=atsetfieldvalues(mach(idid),'FamName',idname);
            mach(idid)=atsetfieldvalues(mach(idid),'BMType',BMtype);
            
        end
    end

    function mach=adddevicenames(mach)
        fprintf('Add device names...\n');
        ind = find(atgetcells(mach,'Class','Quadrupole') | atgetcells(mach,'FamName','Q[FDIJ]\w*'))';
        mach(ind)=atsetfieldvalues(mach(ind),'Device',''); % initialize Device field to empty
        firstsliceQF8D=diff(ind)~=1;% some QF8D are splitted, name first slice only!
        indi=[ind(firstsliceQF8D) ind(end)]; % real number of Quads (removing overcount for splitted elements)       
        mach(indi)=atsetfieldvalues(mach(indi),'Device',ebs.qpname(1:length(indi)));
        
        mach(ind(end:-1:1)) = nameunamedaslastnamed(mach(ind(end:-1:1)));
        
        function r=nameunamedaslastnamed(r)
            % name devices without a name with the name of the last device
            % with a name
            lastname = '';
            for ii=1:length(r)
                if ~isempty(r{ii}.Device)
                    lastname = r{ii}.Device;
                else
                    r{ii}.Device = lastname;
                end
                
            end
        end
        
        
        ind=find(atgetcells(mach,'FamName','DQ\w*'))';
        firstslicedq=diff(ind)~=2;% some DQ are splitted, name first slice
        indi=[ind(firstslicedq) ind(end)]; % real number of DQ (removing overcount for splitted elements)       
        mach(indi)=atsetfieldvalues(mach(indi),'Device',ebs.dqname(1:length(indi)));
        secondslicedq=ind(diff(ind)==2);
        firstofsecondslicedq=secondslicedq+2; % index of first slice for DQ with second slice
        dqsecondslicename=atgetfieldvalues(mach(firstofsecondslicedq),'Device'); % read device name fro first slice
        mach(secondslicedq)=atsetfieldvalues(mach(secondslicedq),'Device',dqsecondslicename); % assign to second slice
        
        ind=atgetcells(mach,'Class','Sextupole');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',ebs.sxname(1:sum(ind)));
        
        ind=atgetcells(mach,'FamName','O[FIJ]\w*');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',ebs.ocname(1:sum(ind)));
        
        ind=atgetcells(mach,'Class','Monitor');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',ebs.bpmname(1:sum(ind)));
        
        ind=atgetcells(mach,'FamName','PINHOLE_.*');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',ebs.pinholename(1:sum(ind)));
    end

    function mach=addpinholes(mach)
        pinid=~any(atgetcells(mach,'FamName','PINHOLE_ID(\d\d)?'));
        pind=~any(atgetcells(mach,'FamName','PINHOLE_D(\d\d)?'));
        if pinid && ~isempty(ebs.model.pinhole_id)
            fprintf('Add ID pinholes...\n');
            dl1a5 = atgetcells(mach, 'FamName', 'DL1A_5');
            nbdl = sum(dl1a5);
            if nbdl == 1        % standard cell or injection cell
                msk=true;
                pinlist=atmarker('PINHOLE_ID');
            else                % 31 or 32: full machine
                msk=pinmask(ebs.model.pinhole_id);
                if nbdl == 31
                    msk=msk(2:end); % No DL1A_% in cell 4
                end
                pinlist=arrayfun(@(c) atmarker(sprintf('PINHOLE_ID%02d',c)),ebs.model.pinhole_id,'UniformOutput',false);
            end
            dl1a5(dl1a5)=msk(1:nbdl);
            loc=1.5e-3./cellfun(@(el) el.BendingAngle, mach(dl1a5));
            mach=atinsertelems(mach,dl1a5,loc,pinlist);
        end
        if pind && ~isempty(ebs.model.pinhole_bm)
            fprintf('Add D pinholes...\n');
            dq1d=atgetcells(mach,'FamName','DQ1D');
            ffdq1d=find(atgetcells(mach,'FamName','mlDQ1D'),1);
            nbdl = sum(dq1d);
            if (nbdl == 1) || (nbdl == 2)	% injection cell or standard cell
                msk=true(2,1);
                pinlist=atmarker('PINHOLE_D');
            else                            % 32: full machine
                msk=pinmask(ebs.model.pinhole_bm);
                pinlist=arrayfun(@(c) atmarker(sprintf('PINHOLE_D%02d',c)),ebs.model.pinhole_bm,'UniformOutput',false);
            end
            dq1d(dq1d)=msk(1:nbdl);
            if isempty(ffdq1d)
                ffangle=0;
            else
                ffangle=mach{ffdq1d}.BendingAngle;
            end
            loc=(1.5e-3-ffangle)./cellfun(@(el) el.BendingAngle, mach(dq1d));
            mach=atinsertelems(mach,dq1d,loc,pinlist);
        end
        
        function msk=pinmask(pin)
            msk=false(1,32);
            pin=mod(pin-4,32)+1;
            msk(pin)=true;
        end
    end

end

