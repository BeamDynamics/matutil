function [resp,devs]=tuneresponse(latmodel,varargin)
%TUNERESPONSE compute tune response for tune correction 
%
% INPUT: 
% atmodel  : EBS lattice model (ex: atmodel = ebs.model(ring))
% tunemode : 'QF1QD2'(default), 'Matching' 
% 
% OUTPUT:
% resp   : n quadrupole x 2 tune response
% devs   : device names
%
%see also: ebs.model

if nargin<2
    tunemode = 'QF1QD2';
elseif nargin==2
    tunemode = varargin{1};
end

% get initial tunes
nuh=latmodel.nuh;
nuv=latmodel.nuv;

% get quadurpole and DQ indexes
iq  = latmodel.get(0,'qp');    % quadrupoles 
iq8  = latmodel.get(0,'qf8d'); % tilted qf8d (varying dipole component, 50 slices)
idq = latmodel.get(0,'dq');    % dipole quadrupole (fixed dipole component, but may be splitted for markers)
allq_unsort = [iq;iq8;idq];
% get order of quadrupoles components in lattice
[allq,iqa] = sort(allq_unsort); 

%% assign a group number to each qf8d, to be able to reconstruct.
q_group = NaN(size(allq));
    
ii=1;
ig=1;
while ii<length(allq)
    
    q_group(ii)=ig;
    % if not subsequent or after subsequent (mid. marker), change group 
    if allq(ii+1)-allq(ii)~=1 && allq(ii+1)-allq(ii)~=2
        ig=ig+1;
    end
    
    ii=ii+1;
    
end
Nq= q_group(end-1)+1;
q_group(end) = Nq;

    function dgls=sumk(dgl,qg)
        % sum integrated gradient change for magnets of the same group
        % (slices)
        % dgl are the delta gradient, qg the quadrupole number, both 
        % in lattice order.
        nq=max(qg);
        dgls=zeros(nq,1);
        
        for inq=1:nq
            dgls(inq)=sum(dgl(qg==inq));% sum gradients
        end
        
    end

%% compute gradient variation for small tune change
Mat=[];

Q0=[nuh nuv];

% change horizontal tune
DQ=.001;

Q1=Q0+[DQ 0.0];

latnewtunes=ebs.model(latmodel);%,'reduce',true);
latnewtunes.settune(Q1,tunemode);
DGL_=latnewtunes.get(1,'qp') - latmodel.get(1,'qp');
D8GL=latnewtunes.get(1,'qf8d') - latmodel.get(1,'qf8d');
DqGL=latnewtunes.get(1,'dq') - latmodel.get(1,'dq');
DGL=[DGL_; D8GL; DqGL];

% sum gradient changes for sliced magnets
DGLsum = sumk(DGL(iqa),q_group);

Mat(:,1)=DGLsum/DQ;

% change vertical tune

Q1=Q0+[0.0 DQ];

latnewtunes=ebs.model(latmodel);%,'reduce',true);
latnewtunes.settune(Q1,tunemode);
DGL_=latnewtunes.get(1,'qp') - latmodel.get(1,'qp');
D8GL=latnewtunes.get(1,'qf8d') - latmodel.get(1,'qf8d');
DqGL=latnewtunes.get(1,'dq') - latmodel.get(1,'dq');
DGL=[DGL_; D8GL; DqGL];

DGLsum = sumk(DGL(iqa),q_group);

Mat(:,2)=DGLsum/DQ;

%% output response matrix in correct format for Tango
% select changed gradients and get device familiy names.
%[uMat,iq,~]=unique(Mat,'rows');
%selnonzero=uMat(:,1)~=0 & uMat(:,2)~=0;
uMat = Mat;
selnonzero = 1:length(Mat) ;
%N_q  = size(unique(q_group(ismember(allq,iq))));
%N_q8 = size(unique(q_group(ismember(allq,iq8))));
%N_dq = size(unique(q_group(ismember(allq,idq))));

[~,iu,~]=unique(q_group);
devs = atgetfieldvalues(latmodel.ring(allq(iu)),'Device');

resp=[uMat(selnonzero,1),uMat(selnonzero,2)];

% write to file
fff=fopen('tunemat.csv','w+');
for iq=1:length(devs)
    fprintf(fff,'%s,%2.10f,%2.10f,\n',devs{iq},resp(iq,1),resp(iq,2));
end
fclose(fff);

end
