function [idx,kdx]=anyindex(npercell,bfunc,arg1,arg2)
%EBS.ANYINDEX	find index of elems from their name
%
%IDX=ANYINDEX(NPERCELL,SCANFUNCTION,NAME)
%  NPERCELL:    Number of elements per cell or vector of elements per cell
%  SCANFUNCTION:Function returning
%  NAME:        string, char array or cell array of strings
% 
%IDX=ANYINDEX(NPERCELL,SCANFUNCTION,KDX)
%  KDX:         "hardware" index of the BPM
%
%IDX=ANYINDEX(NPERCELL,SCANFUNCTION,CELL,NUM)
%  CELL (1..32) and NUM (1..7)
%
%
%[IDX,KDX]=ANYINDEX(...)	returns in addition the "hardware" index
%

injcellleft=3;

if length(npercell)==1
    npercell=repmat(npercell,1,32);
end

npercell=npercell([(end-injcellleft+1):end,1:(end-injcellleft)]);

    function kdx=bfind(name)
        [cellnum,id]=bfunc(name);
        kdx=sum(npercell(1:(cellnum-1)))+id;
    end

[nrows,ncols]=size(arg1);


if isnumeric(arg1)
    if ncols==2
        
            kdx=sum(npercell(1:(arg1(:,1)-1)))+arg1(:,2);
            
    elseif nargin < 4
        
        kdx=arg1;
        
    else
        
            kdx=sum(npercell(1:(arg1-1)))+arg2;
       
    end
elseif iscell(arg1)		% cell array of strings
    kdx=zeros(nrows,ncols);
    for ik=1:nrows*ncols, kdx(ik)=bfind(arg1{ik}); end
elseif nrows > 1		% char array
    kdx=zeros(nrows,1);
    for row=1:nrows, kdx(row)=bfind(arg1(row,:)); end
else					% string
    kdx=bfind(arg1);
end


    offset=sum(npercell(1:injcellleft));
    idx=kdx-offset + sum(npercell)*(kdx<=offset);


end
