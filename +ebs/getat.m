function atstruct = getat(varargin)
%GETAT	% Return an AT structure
%
%ATSTRUCT=GETAT(VARARGIN) scans the input arguments looking for:
%
%- 'opticsname'
%           optics name (defaults to 'theory')
%- '/machfs/appdata/ebs/optics/settings/opticsname':
%           full path of optics directory
%- atstruct:
%           AT structure
%- ebs.model:
%           ebs.model object
%
%
%ATSTRUCT:	Resulting AT structure
%
%GETAT(...,'energy',energy)
%   Set the ring energy
%
%GETAT(...,'reduce',true,'keep',pattern)
%   Remove elements with PassMethod='IdentityPass' and merge adjacent
%   similar elements, but keeps elements with FamName matching "pattern"
%   Pattern may be a logical mask. (Default: 'BPM.*|ID.*').
%
%GETAT(...,'remove',famnames)
%   remove elements identified by the family names (cell array)
%
%GETAT(...,'MaxOrder',n)
%   Limit the order of polynomial field expansion to n at maximum.
%   Default 999
%
%GETAT(...,'NumIntSteps',m)
%   Set the NumIntSteps integration parameter to at least m.
%   Default 20
%
%GETAT(...,'QuadFields',quadfields)
%   When reading a BETA file, set quadrupoles fields to the specified ones.
%   Default: {}
%
%GETAT(...,'BendFields',bendfields)
%   When reading a BETA file, set bending magnet fields to the specified ones.
%   Default: {}
%
%GETAT(...,'DeviceNames',true)
%   add device names in AT structure.
%
%GETAT(...,'PinholeID',cellnbs)
%   Select the cells where ID pinhole cameras are located. Default [7 25]
%
%GETAT(...,'PinholeBM',cellnbs)
%   Select the cells where BM pinhole cameras are located. Default [17 27 1]

[machid,location,args]=atmodel.getpath('ebs',varargin{:});
atstruct=atmodel.getatstruct(machid,location,args{:});

end

