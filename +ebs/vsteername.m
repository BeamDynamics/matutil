function out=vsteername(idx)
%VSTEERNAME	returns the name of Steerer with index idx
%
%STEERNAME=VSTEERNAME(IDX)		returns the name of v steerer with index IDX
% IDX: vector of indexes in the range 1:288 (1 is vst-sh1/c04-a)
%       logical array of length 
%       or n x 2 matrix in the form [cell index] with
%       or 1 x n array for the names of n magnets
%       cell in 1:32, index in 1:9
%                   
%[NM1,NM2...]=VSTEERNAME(...)		May be used to generate several names
%
% for skew quadrupole names
% ebs.vsteername([1,23,59]) % array
% ebs.vsteername([23,1]) % cell idx
% 
%   See also EBS.STEERNAME

out=ebs.steername(idx,'vst-');

end