function varargout=sxname(idx,format)
%EBS.SXNAME	returns the name of Sextupole with index idx
%
%SXNAME=SXNAME(IDX)	returns the name of Sextupole with index IDX
% IDX: vector of indexes in the range [1 196] (1 is C4-1)
%       or n x 2 matrix in the form [cell index] with
%       cell in [1 32], index in [1 7]
%
%SXNAME=SXNAME(IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : 'srmag/m-s******')
%
%[NM1,NM2...]=SXNAME(...)    May be used to generate several names
%
%[...,KDX]=SXNAME(...)       returns in addition the "hardware" index KDX
%
%KDX=SXNAME(IDX,'')          returns only the "hardware" index KDX
% 
% examples : 
% >> ebs.SXNAME(192)
% ans = 
%     'srmag/m-sd1/c03-e'
% >> ebs.SXNAME([320 234 10]')
%    ans = 
%     'srmag/m-sd1/c03-e'
%     'srmag/m-sd1/c23-e'
%     'srmag/m-sd1/c04-d' 
%
%
%   See also SXINDEX

if nargin < 2, format='srmag/m-s'; end

[kdx,celnum,id]=ebs.cellnum(6,idx);

if isempty(format) || strcmp(format,'noname')
    varargout={};
else
    
    devnamefun= @(celnum,id)textfunc(format,celnum,id);
   
    nms=arrayfun(devnamefun,celnum,id,'UniformOutput',false);
    nin=length(nms);
    if (nargout==nin) || (nargout==(nin+1))
        varargout=nms;
    else
        varargout={nms};
    end
end

nout=length(varargout);
if nargout > nout
    varargout{nout+1}=kdx;
end

    function devnam=textfunc(format,celnum,id)
        
        num='121121';
        pos='aabdee';
        foc='dfddfd';
            
        devnam=[format foc(id) num(id) '/c' num2str(celnum,'%.2d') '-' pos(id)];
        
    end

end
