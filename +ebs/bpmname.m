function varargout=bpmname(idx,format)
%EBS.BPMNAME	returns the name of BPM with index idx
%
%BPMNAME=BPMNAME(IDX)	returns the name of BPM with index IDX
% IDX: vector of indexes in the range [1 224] (1 is C4-1)
%       or n x 2 matrix in the form [cell index] with
%       cell in [1 32], index in [1 7]
%
%BPMNAME=BPMNAME(IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : 'srdiag/beam-position/c%02i-%02i')
%
%[NM1,NM2...]=BPMNAME(...)    May be used to generate several names
%
%[...,KDX]=BPMNAME(...)       returns in addition the "hardware" index KDX
%
%KDX=BPMNAME(IDX,'')          returns only the "hardware" index KDX
% 
% examples : 
% >> ebs.bpmname(320)
% ans = 
%     'srdiag/beam-position/c03-10'
% >> ebs.bpmname([320 234 10]')
% ans = 
%     'srdiag/beam-position/c03-10'
%     'srdiag/beam-position/c27-04'
%     'srdiag/beam-position/c04-10'
%     
%   See also BPMINDEX, QPNAME, SXNAME

if nargin < 2, format='srdiag/beam-position/c%02i-%02i'; end

[kdx,celnum,id]=ebs.cellnum(10,idx);
if isempty(format) || strcmp(format,'noname')
    varargout={};
else
    textfunc=@(celnum,id) sprintf(format,celnum,id);
    nms=arrayfun(textfunc,celnum,id,'UniformOutput',false);
    nin=length(nms);
    if (nargout==nin) || (nargout==(nin+1))
        varargout=nms;
    else
        varargout={nms};
    end
end

nout=length(varargout);
if nargout > nout
    varargout{nout+1}=kdx;
end
