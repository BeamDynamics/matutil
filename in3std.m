function xok=in3std(xx,frac)

if nargin < 2, frac=3.0; end
copyd=ones(size(xx,1),1);
xstd=copyd*std2(xx);
xave=copyd*mean2(xx);
nok=find(abs(xx-xave) > frac*xstd);
xok=xx;
xok(nok)=NaN*ones(size(nok));
