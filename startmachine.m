function startmachine(rootpath)
global APPHOME

if ~(exist('isdeployed','builtin') && isdeployed)
    if nargin < 1, rootpath='/operation/machine'; end
    optpath(...             % Same order as in the resulting PATH
        fullfile(rootpath,'matmex'),...
        fullfile(rootpath,'matlab'),...
        fullfile(rootpath,'matusers'),...
        fullfile(rootpath,'at/atmat'),...
        fullfile(rootpath,'at/atintegrators'),...
        fullfile(rootpath,'at/machine_data'));
end

set(0,'DefaultFigurePaperType','A4',...
    'DefaultFigurePaperUnit','centimeters',...
    'DefaultFigurePaperPosition',[2.5 9 16 12]);

if isempty(APPHOME), APPHOME=getenv('APPHOME'); end

function optpath(varargin)
for i=nargin:-1:1
    if (exist(varargin{i},'dir') == 7), addpath(genp(varargin{i})); end
end

function p = genp(d)
classsep = '@';  % qualifier for overloaded class directories
packagesep = '+';  % qualifier for overloaded package directories
skipc=['.' classsep packagesep];
skips={'CVS','private','obsolete'};
p = '';           % path to be returned

files = dir(d);
if ~isempty(files)
    
    % Add d to the path even if it is empty.
    p = [p d pathsep];
    
    % set logical vector for subdirectory entries in d
    isdir = logical(cat(1,files.isdir));
    %
    % Recursively descend through directories which are neither
    % private nor "class" directories.
    %
    dirs = files(isdir); % select only directory entries from the current listing
    
    for i=1:length(dirs)
        dirname = dirs(i).name;
        if	~(any(dirname(1)==skipc) || any(strcmp(dirname,skips)))
            p = [p genp(fullfile(d,dirname))]; %#ok<AGROW> % recursive calling of this function.
        end
    end
end
