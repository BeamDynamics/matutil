function rsp=load_rsp2(pl,varargin)
%LOAD_RSP2	Load response matrix
%
%A=LOAD_RSP2(PLANE)		Load response matrix from the
%                       current directory and builds the filename
%                       according to PLANE
%
%A=LOAD_RSP2(PLANE,FILENAME)	Uses the specified file
%	FILENAME: Directory containing measured data or
%			  file containing a full response matrix
%
%   The function uses SR.LOAD_RESP and performs consistency checks on the
%   data. It eliminates fixed BPMs, outliers...
%
%	plane:	'h|H|x|X|1' 'v|V|z|Z|2' 'h2v|H2V|x2z|X2Z|3' 'v2h|V2H|z2x|Z2X|4'
%	a:	response matrix (m/A)
%
%   See also SR.LOAD_RESP, SR.LOAD_NORMRESP.

args={selectplane(pl,{'resH.rsp','resV.rsp','resV2H.rsp','resH2V.rsp'}),[0 0]};
args(1:length(varargin))=varargin;
[filename,action]=deal(args{:});
[orb,cur]=sr.load_resp(filename,pl,1:96);
[nbt,nkt]=size(orb);  %#ok<ASGLU>

failing=find(abs(std2(orb,1,2)) < 5.E-6);	% eliminate fixed
if ~isempty(failing)
    disp([char(10) num2str(length(failing)) ' strange BPM readings (fixed value)...']);
    for j=failing', disp(sr.bpmname(j)); end
    orb(failing,:)=NaN;
end

norm=repmat(2.5*std2(orb,1,1),nbt,1);		% eliminate outliers
toolarge=(abs(orb) > norm);
strange=find(toolarge);
if ~isempty(strange)
    disp([char(10) num2str(length(strange)) ' strange BPM readings (more then 2.5*std)...']);
    disp('kick BPMlist');
    for j=find(any(toolarge))
        disp([sr.steername(j,'SR/ST-S%i/C%i') char(9) num2str(find(toolarge(:,j))')]);
    end
    if action(1)==0
        suppr=~strcmp(input('suppress these data (y/n): ','s'),'n');
    else
        suppr=action<0;
    end
    if suppr
        orb(strange)=NaN;
    end
end

toolarge=(abs(orb) == 0);					% eliminate 0 readings
strange=find(toolarge);
if ~isempty(strange)
    disp([char(10) num2str(length(strange)) ' strange BPM readings (0.0 value)...']);
    disp('kick BPMlist');
    for j=find(any(toolarge))
        disp([sr.steername(j,'SR/ST-S%i/C%i') char(9) num2str(find(toolarge(:,j))')]);
    end
    if action(2)==0
        suppr=strcmp(input('suppress these data (n/y): ','s'),'y');
    else
        suppr=action<0;
    end
    if suppr
        orb(strange)=NaN;
    end
end

missing=~any(isfinite(orb),2);				% list missing
strange=find(missing);
if ~isempty(strange)
    disp([char(10) num2str(length(strange)) ' missing BPMs:']);
    for bpm=strange'
        [bname,kdx]=sr.bpmname(bpm);
        disp([num2str(kdx,'%3i') ':' bname]);
    end
end

rsp=orb./repmat(cur,nbt,1);
