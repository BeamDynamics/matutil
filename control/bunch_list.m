classdef bunch_list
    % Bunch list for the various filling patterns
    
    properties
        patname={'uniform','16b','7/8+1','hybrid'}
        patmask={true(1,992),false(1,992),false(1,992),false(1,992)}
    end
    
    methods
        function self=bunch_list()
            val=false(1,992);
            self.patmask{2}(1:62:992)=true;
            self.patmask{3}(64:930)=true; % No markers, no single
            arrayfun(@set2,148:31:862)
            self.patmask{4}=val;
            function set2(n)
                val((1:8)+n)=true;
            end
        end
        function mask=get(self,mode,sb_position)
            if nargin<3, sb_position=1; end
            ok=find(strcmp(mode,self.patname),1);
            mask=circshift(self.patmask{ok},sb_position-1,2);
        end
    end
    
end

