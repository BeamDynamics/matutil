function varargout = boff(varargin)
% BOFF MATLAB code for boff.fig
%      BOFF, by itself, creates a new BOFF or raises the existing
%      singleton*.
%
%      H = BOFF returns the handle to a new BOFF or the handle to
%      the existing singleton*.
%
%      BOFF('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BOFF.M with the given input arguments.
%
%      BOFF('Property','Value',...) creates a new BOFF or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before boff_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to boff_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help boff

% Last Modified by GUIDE v2.5 24-Aug-2016 16:17:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @boff_OpeningFcn, ...
    'gui_OutputFcn',  @boff_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before boff is made visible.
function boff_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to boff (see VARARGIN)

% Choose default command line output for boff
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes boff wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = boff_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
if hObject.Value == 1
    set([handles.bpmid handles.bpmidlabel],'Visible','off');
else
    set([handles.bpmid handles.bpmidlabel],'Visible','on');
end
guidata(hObject,select(handles));


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in cellid.
function cellid_Callback(hObject, eventdata, handles)
% hObject    handle to cellid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns cellid contents as cell array
%        contents{get(hObject,'Value')} returns selected item from cellid
guidata(hObject,select(handles));


% --- Executes during object creation, after setting all properties.
function cellid_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cellid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in bpmid.
function bpmid_Callback(hObject, eventdata, handles)
% hObject    handle to bpmid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns bpmid contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bpmid
guidata(hObject,select(handles));


% --- Executes during object creation, after setting all properties.
function bpmid_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bpmid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in bcalibrate.
function bcalibrate_Callback(hObject, eventdata, handles)
% hObject    handle to bcalibrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
vhv=handles.offset.calibrate();
set(handles.bcontrol,'Enable','on');


% --- Executes on button press in badjust.
function badjust_Callback(hObject, eventdata, handles)
% hObject    handle to badjust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
vhv=handles.offset.adjust();
set(handles.bcontrol,'Enable','on');


% --- Executes on button press in bcontrol.
function bcontrol_Callback(hObject, eventdata, handles)
% hObject    handle to bcontrol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
vhv=handles.offset.control();

function handles=select(handles)
global APPHOME
cla(handles.axevalh);
cla(handles.axevalv);
cla(handles.axbump);
cla(handles.axfit1);
cla(handles.axfit2);
cell=get(handles.cellid,'Value');
out1=struct('tbump',handles.tbump1,...
    'tstep',handles.tstep1,'tstd',handles.tstd1,'tfit',handles.tfit1,...
    'tbpm',handles.val6,'taltbpm',handles.val7,'tlog',handles.tlogger);
structfun(@(t) set(t,'String','-'),out1);
diary(fullfile(APPHOME,'shuntfiles',['out_' datestr(now,29)]));
diary('off');
if handles.popupmenu1.Value==1      % ID selected
    cellm1=mod(cell-2,32)+1;
    out2=struct('tbump',handles.tbump2,...
        'tstep',handles.tstep2,'tstd',handles.tstd2,'tfit',handles.tfit2,...
        'tbpm',handles.val2,'taltbpm',handles.val1,'tlog',handles.tlogger);
    structfun(@(t) set(t,'String','-'),out2);
    handles.offset=idoffset(cell,'axshunt',[handles.axevalh handles.axevalv],...
        'axbump',handles.axbump,'axfit',{handles.axfit1 handles.axfit2},...
        'logger',{@(mess,v) sdisp(out1,mess,v),@(mess,v) sdisp(out2,mess,v)},...
        'dispshunt',{@(arg) ftest(handles.tshunt1,arg),@(arg) ftest(handles.tshunt2,arg)});
    set(handles.bcalibrate,'Enable','off');
    set(handles.uipanel3,'Title',sprintf('ID%d Upstream',cell));
    set(handles.uipanel4,'Title',sprintf('ID%d Downstream',cell));
    set(handles.uipanel4,'Visible','on');
    set(handles.bpm6,'String',sprintf('C%d-%d',cellm1,6));
    set(handles.bpm7,'String',sprintf('C%d-%d',cellm1,7));
    set(handles.bpm1,'String',sprintf('C%d-%d',cell,1));
    set(handles.bpm2,'String',sprintf('C%d-%d',cell,2));
    diary('on');
    fprintf('\nID %3d\n',cell);
else                                % BPM selected
    id=handles.bpmid.Value;
    handles.offset=bpmoffset(cell,id,'axshunt',[handles.axevalh handles.axevalv],...
        'axbump',handles.axbump,'axfit',handles.axfit1,...
        'logger',@(mess,v) sdisp(out1,mess,v),...
        'dispshunt',@(arg) ftest(handles.tshunt1,arg));
    set(handles.bcalibrate,'Enable','on');
    set(handles.uipanel4,'Visible','off');
    set(handles.uipanel3,'Title',sprintf('BPM C%d-%d',cell,id));
    set(handles.bpm6,'String',sprintf('C%d-%d',cell,id));
    set([handles.bpm7 handles.bpm1 handles.bpm2],'String','-');
    diary('on');
    fprintf('\nBPM %3d %s\n',handles.offset.id,sprintf('SR/D-BPMLIBERA/C%d-%d',cell,id));
end
diary('off');
set([handles.val6 handles.val7 handles.val1 handles.val2],'String','-');
handles.offset.plot_theobump();
set(handles.badjust,'Enable','on');
set(handles.bcontrol,'Enable','off');
set(handles.tlogger,'String',{});

function ftest(w,arg)
set(w,'String',['shunt ' arg]);

function sdisp(out,mess,v)
diary('on');
bpmoffset.rdisp(mess,v);
diary('off');
if isfield(v,'ampl')
    set(out.tstep,'String',upper(mess));
    set(out.tbump,'String',sprintf('%6.3f/%6.3f',1000*v.ampl));
    set(out.tstd,'String',sprintf('%6.3f/%6.3f',1000*v.std));
    set(out.tfit,'String',sprintf('%6.3f/%6.3f',1000*v.fit));
    set(out.tbpm,'String',sprintf('%6.3f/%6.3f',1000*v.orbit));
    mess=sprintf('%15s: ampl: %6.3f/%6.3f, BPM: %6.3f/%6.3f, rms: %6.3f/%6.3f, fit: %.4g/%.4g',...
        upper(mess), 1000*[v.ampl,v.orbit,v.std,v.fit]);
else
    set(out.taltbpm,'String',sprintf('%6.3f/%6.3f',1000*v.orbit));
    mess=sprintf('%15s:                      BPM: %6.3f/%6.3f',...
        upper(mess), 1000*v.orbit);
end
if isfield(out,'tlog')
    out.tlog.String=[out.tlog.String;{mess}];
end

% --- Executes on button press in breset.
function breset_Callback(hObject, eventdata, handles)
% hObject    handle to breset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.offset.setbump(0);



function tlogger_Callback(hObject, eventdata, handles)
% hObject    handle to tlogger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tlogger as text
%        str2double(get(hObject,'String')) returns contents of tlogger as a double


% --- Executes during object creation, after setting all properties.
function tlogger_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tlogger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in bbest.
function bbest_Callback(hObject, eventdata, handles)
% hObject    handle to bbest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
vhv=handles.offset.best();