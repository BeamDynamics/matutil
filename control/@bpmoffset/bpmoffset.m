classdef bpmoffset < handle
    %BPMOFFSET	BPM offset measurement
    %   The BPMOFFSET object controls the measurement of BPM offsets by
    %   driving local bumps and activating quasdrupole shunts
    
    
    properties (Constant, Access=private, Hidden)
        bpm2shunt={     %Format of the 14 shunt names for one cell
            'SR/SH-QF2/C%02d',...
            'SR/SH-QF2/C%02d',...
            'SR/SH-QD4/C%02d-1',...
            'SR/SH-QF5/C%02d-1',...
            'SR/SH-QD4/C%02d-2',...
            'SR/SH-QF7/C%02d',...
            'SR/SH-QF7/C%02d',...
            'SR/SH-QF7/C%02d',...
            'SR/SH-QF7/C%02d',...
            'SR/SH-QD4/C%02d-1',...
            'SR/SH-QF5/C%02d-1',...
            'SR/SH-QD4/C%02d-2',...
            'SR/SH-QF2/C%02d',...
            'SR/SH-QF2/C%02d'};
        c23quads={...
            'sr/ps-qd4/c22',...
            'sr/ps-qf7hg/c22',...
            'sr/ps-qf7hg/c23',...
            'sr/ps-qd4/c23',...
            'sr/ps-qf5/c23'};
        bpm2qp=[1;1;3;4;6;8;8;9;9;11;12;14;16;16];  %Index of reference quadrupole for the 14 bpms of one cell
        qpindex=16*floor((0:223)/14)'+reshape(bpmoffset.bpm2qp(:,ones(1,16)),224,1);	%Index of reference quadrupole for the 224 bpms
    end
    
    properties (Constant)
        ring=sr.getmodel()  % Ring model (AT structure)
        sh=orbumps.get('h') % Horizontal bump control
        sv=orbumps.get('v') % Vertical bump control
        bpmdev=tango.SrBpmDevice()  % BPM device
    end
    properties (Constant, Access=protected, Hidden)
        shuntnames=bpmoffset.shnames()  % All shunt names
        shuntfuncs=bpmoffset.shfuncs()  % All shunt activation functions
    end
    
    properties
        id  % BPM index (1 <=> C4-1)
        shuntdev=[]     % Shunt device
        shuntfunc=[]    % Shunt activation function
        theoresp=[]     % Theoretical orbit from the shunt activation
        next=[0 0]      % Estimated bump amplitudes
        vhv=[];         % History of measurements
        step            % Bump step
        logger
        marker
    end
    
    properties (Access=private, Hidden)
        axshunt
        axbump
        axfit
        dispshunt
        offsim          % Simulation
    end
    
    methods (Static, Access=private, Hidden)
        function names=shnames()
            %Generate the shunt name for each bpm
            nincell=mod((0:223),14)+1;
            cellnum=floor((0:223)/7)+4;
            cellnum(cellnum>32)=cellnum(cellnum>32)-32;
            names=arrayfun(@(nm,cl) sprintf(bpmoffset.bpm2shunt{nm},cl),...
                nincell,cellnum,'UniformOutput',false);
            names(131:137)=bpmoffset.c23quads([1 2 2 3 3 4 5]);
        end
        function shuntf=shfuncs()
            %Generate the shunt activation function for each bpm
            qpv=arrayfun(@(d) d.Current.set,tango.Device(bpmoffset.c23quads));    % Initial QP values
            shuntf=repmat({@(dev,cmd) dev.cmd(cmd)},1,224);
            shuntf(131:137)=arrayfun(@(iniv) @(dev,cmd) qpset(dev,cmd,iniv),...
                qpv([1 2 2 3 3 4 5]),'UniformOutput',false);
            
            function qpset(qpdev,cmd,offvalue)
                if strcmp(cmd,'On')
                    qpdev.Current=0.99*offvalue;
                else
                    qpdev.Current=offvalue;
                end
            end
        end
    end
    
    methods (Static)
        function ampls=set_amplitudes(estimate, step)
            %Prepare an array of bump amplitudes
            active=true(size(estimate));
            active(~(abs(estimate) < 3.0)) = false;
            ampls=repmat(estimate,size(step,1),1)+step;
            ampls(:,~active)=0;
        end
        
        function rdisp(mess,v)
            %Display a measurement evaluation
            if isfield(v,'ampl')
                fprintf('%15s: ampl: %6.3f/%6.3f, BPM: %6.3f/%6.3f, rms: %6.3f/%6.3f, fit: %.4g/%.4g\n',...
                    upper(mess), 1000*[v.ampl,v.orbit,v.std,v.fit]);
            else
                fprintf('%15s:                      BPM: %6.3f/%6.3f\n',upper(mess),1000*v.orbit);
            end
        end
        
        function v=scanline(line)
            a1=0.001*sscanf(line,'%*s ampl:%g/%g, BPM:%g/%g, rms:%g/%g, fit:%g/%g');
            v=struct('ampl',a1(1:2)','orbit',a1(3:4)','fit',a1(7:8)','std',a1(5:6)');
        end
        
        function vshunt(shuntstate)
            disp(['                     Shunt ' upper(shuntstate)]);
        end
        
        function [res,f]=solve(fit,ampl)
            [resh,fh]=slv(fit(:,1:2:end),ampl(:,1:2:end));
            [resv,fv]=slv(fit(:,2:2:end),ampl(:,2:2:end));
            res=reshape([resh;resv],1,[]);
            f=reshape([fh;fv],size(fit));
            function [res,f]=slv(osc,ampl)
                ampl1=[ones(size(ampl,1),1) ampl];
                ba=ampl1\osc;
                res=-ba(1,:)/ba(2:end,:);
                f=ampl1*ba;
            end
        end
        
        plot_harms(h,vhv)
        plot_calib(h,ampl,vhv)
    end
    
    methods
        function this=bpmoffset(cell,id,varargin)
            %BPMOFFSET	Create the BPMOFFSET object
            %
            %BPMOFFSET(CELL,ID)
            %
            %BPMOFFSET(...,FLAG,...)
            %   BPMOFFSET accepts the following flags
            %
            %'nop':         Inactive BPM (no shunt, no bump)
            %
            %BPMOFFSET(...,OPTNAME,OPVALUE,...)
            %   BPMOFFSET accepts the following options:
            %
            %'axshunt':     Axes for plotting the orbit oscillation
            %'axbump':      Axes for plotting the bump shape
            %'axfit':       Axes for plotting fir of oscillations
            %'dispshunt':	Function for displaying shunt state
            %'logger':      Function for displaying the sequence
            
            id=sr.bpmindex(cell,id);
            rg=this.ring;
            [nop,options]=getflag(varargin,'nop');
            [this.axshunt,options]=getoption(options,'axshunt');
            [this.axbump,options]=getoption(options,'axbump');
            [this.axfit,options]=getoption(options,'axfit');
            [this.dispshunt,options]=getoption(options,'dispshunt',@bpmoffset.vshunt);
            [this.logger,options]=getoption(options,'logger',@bpmoffset.rdisp); %#ok<ASGLU>
            if ~isempty(this.axfit)
                cla(this.axfit,'reset');
            end
            this.id=id;
            this.step=[150e-6 150e-6];
            if ~nop
                this.shuntfunc=this.shuntfuncs{id};
                this.shuntdev=tango.Device(this.shuntnames{id});
                qpdata=rg.qp;
                this.theoresp=[...
                    responsem(rg.bpm([3 5]),qpdata(id,2:3),rg.nuh) ...
                    responsem(rg.bpm([8 10]),qpdata(id,5:6),rg.nuv)];
            end
            this.bpmdev.Averaging=3;
            this.bpmdev.IgnoreIncoh=true;
            this.marker='o';
            this.offsim=off(id);            % Simulation
            function offsim=off(id)         % Simulation
                d=1.e-5*(mod(id-1,7)+1);	% Simulation
                offsim=[d -d];              % Simulation
            end                             % Simulation
        end
        
        function delete(this)
            if ~isempty(this.shuntdev)
                this.setbump(0);
            end
        end
        
        function plot_theobump(this,varargin)
            %Plot the theoretical orbit bump
            theobump=[this.sh.showbump(this.id,this.step(1)) ...
                this.sv.showbump(this.id,this.step(2))];
            this.plot_bumps(theobump,varargin{:});
        end
        
        function [v,orbit]=eval(this,ampl)
            %EVAL   Power the shunt and measure the deviation and evaluate the penalty
            %[VHV,ORBIT]=EVAL() Quantify offset error
            %
            %VHV:       Evaluation function: bpm, harm, fit, std
            %ORBIT:     Initial orbit (without shunt)
            
            %             try
            %                 this.dispshunt('on');
            %                 this.shuntfunc(this.shuntdev, 'On');			% DANGEROUS
            %                 pause(3);
            %                 [x,z]=this.bpmdev.read_sa('Wait',true);
            %                 this.dispshunt('off');
            %                 this.shuntfunc(this.shuntdev, 'Off');			% DANGEROUS
            %                 pause(3);
            %             catch error
            %                 disp('-Error: setting shunt');
            %                 this.dispshunt('off');
            %                 this.shuntfunc(this.shuntdev, 'Off');			% DANGEROUS
            %                 rethrow(error);
            %             end
            %             [x0,z0]=this.bpmdev.read_sa('Wait',true);
            %             orbit=[x0 z0];
            %             dxz=[x z]-orbit;
            this.dispshunt('on');                       % Simulation
            pause(3);                                   % Simulation
            this.dispshunt('off');                      % Simulation
            pause(3);                                   % Simulation
            orbit=[this.sh.getorbit() this.sv.getorbit()];% Simulation
            dd=0.5*(orbit(this.id,:)-this.offsim);      % Simulation
            dxz=this.theoresp.*dd(ones(224,1),:);       % Simulation
            
            if ~isempty(this.axshunt)
                plot(this.axshunt(1),1.e6*dxz(:,1));
                ylabel(this.axshunt(1),'x [\mum]');
                title(this.axshunt(1),'Oscillation');
                plot(this.axshunt(2),1.e6*dxz(:,2));
                xlabel(this.axshunt(2),'BPM number');
                ylabel(this.axshunt(2),'z [\mum]');
            end
            hvfit=fit_orbit(this.theoresp,dxz);
            
            v=struct('ampl',this.step.*ampl,'orbit',orbit(this.id,:),'fit',hvfit,'std',std2(dxz));
            this.plot_fit(v,'measure');
            
            function res=fit_orbit(theoresp,y)
                ok=all(isfinite(y),2);
                xok=theoresp(ok,:);
                res=sum(xok.*y(ok,:))./sum(xok.*xok);
            end
        end
        
        function [v,orbit]=evalbpm(this,mess,ampl)
            %Set a bump (2 amplitudes) and evaluate the penalty (1 bpm)
            %bump,bpm,harm,fit,std
            if nargin >= 3
                this.setbump(ampl);
            else
                ampl=[0.0 0.0];
            end
            [v,orbit]=this.eval(ampl);
            this.logger(mess,v);
        end
        
        function setbump(this,normampl)
            %Set a bump in both planes
            ampl=this.step.*normampl;
            this.sh.setbump(this.id,ampl(1));
            this.sv.setbump(this.id,ampl(2));
        end
        
        function vhv=calibrate(this)
            %CALIBRATE  1st step: use estimate and estimate+step
            %VHV=CALIBRATE()
            %
            %VHV:       Penalty function
            
            this.marker='+';
            estimate=this.next;
            sequence=[0 0;1 1];
            ampl=bpmoffset.set_amplitudes(estimate,sequence);
            
            amp0=bpmoffset.set_amplitudes(estimate,-0.1);   % dummy point
            this.setbump(amp0);
            pause(1);
            
            [vhv(1),orbit0]=this.evalbpm('Initial',ampl(1,:));          % Test Initial
            [vhv(2),orbit ]=this.evalbpm('Calibration',ampl(2,:));      % Test +
            this.plot_bumps(orbit-orbit0,'off');	% Plot real bump
            
            this.next=bpmoffset.solve(cat(1,vhv.fit),cat(1,vhv.ampl))./this.step;
            this.vhv=[this.vhv vhv];
        end
        
        function vhv=adjust(this)
            %ADJUST  2nd step: use estimate-step, estimate and estimate+step
            %VHV=ADJUST()
            %
            %VHV:       Penalty function
            
            this.marker='o';
            estimate=this.next;
            sequence=[-1 -1;0 0;1 1];
            ampl=bpmoffset.set_amplitudes(estimate,sequence);
            
            amp0=bpmoffset.set_amplitudes(estimate,-1.1);	% dummy point
            this.setbump(amp0);
            pause(1);
            
             vhv(1)        =this.evalbpm('Estimate-step',ampl(1,:));	% Test -
            [vhv(2),orbit0]=this.evalbpm('Estimate',ampl(2,:));         % Test 0
            [vhv(3),orbit ]=this.evalbpm('Estimate+step',ampl(3,:));	% Test +
            this.plot_bumps(orbit-orbit0,'off');	% Plot real bump
            this.plot_fit(vhv,'fit');                % Plot fit
            
            this.next=bpmoffset.solve(cat(1,vhv.fit),cat(1,vhv.ampl))./this.step;
            this.vhv=[this.vhv vhv];
        end
        
        function vhv=control(this)
            %CONTROL 3rd step: measure and give a new estimate
            %VHV=CONTROL()
            %
            %VHV:       Penalty function
            %
            
            this.marker='*';
            estimate=this.next;
            ampl=bpmoffset.set_amplitudes(estimate,0);
            
            amp0=bpmoffset.set_amplitudes(estimate,-0.1);	% dummy point
            this.setbump(amp0);
            pause(1);
            
            vhv=this.evalbpm('Control',ampl);
            
            this.vhv=[this.vhv vhv];
            this.next=bpmoffset.solve(cat(1,this.vhv.fit),cat(1,this.vhv.ampl))./this.step;
        end
        
        function vhv=best(this)
            [~,idhv]=min(abs(cat(1,this.vhv.fit)));
            idh=idhv(1);
            idv=idhv(2);
            vhv=struct('ampl',[this.vhv(idh).ampl(1) this.vhv(idv).ampl(2)],...
                'orbit',[this.vhv(idh).orbit(1) this.vhv(idv).orbit(2)],...
                'fit',[this.vhv(idh).fit(1) this.vhv(idv).fit(2)],...
                'std',[this.vhv(idh).std(1) this.vhv(idv).std(2)]);
            this.logger('Best',vhv);
        end
        
        function vhv=scan(this,fname)
            this.marker='o';
            fid=fopen(fname);
            vhv=[];
            ok=true;
            while ok
                line=fgetl(fid);
                if ~ischar(line)
                    break;
                end
                vhv=[vhv bpmoffset.scanline(line)]; %#ok<AGROW>
            end
            fclose(fid);
            this.show_vhv(vhv);
            this.vhv=vhv;
            this.next=bpmoffset.solve(cat(1,vhv.fit),cat(1,vhv.ampl))./this.step;
        end
        
        function show_vhv(this,vhv)
            if ~isempty(this.axfit)
                hold(this.axfit,'off');
                this.plot_fit(vhv,'measure');
                this.plot_fit(vhv,'fit');
            end
        end
        
        plot_bumps(this,orbits,hld)
        plot_fit(this,vhv,code)
        
    end
end

