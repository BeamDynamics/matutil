function plot_calib(h,varargin)

figure(h);
%  [ampl,vhv]=deal(varargin{:});
ampl=1.e6*varargin{1};
vhv=1.e6*varargin{2};
plot(ampl(3:5,1),vhv(3:5,1),'-o',ampl(3:5,2),vhv(3:5,2),'-o');
legend('H','V');
if isfinite(ampl(1,1))
    hold on
    ls=plot(ampl(1:2,1),vhv(1:2,1),'-o',ampl(1:2,2),vhv(1:2,2),'-o');
    set(ls,'LineWidth',1.5);
    hold off
end
title('Bump efficiency');
xlabel('Bump amplitude [\mum]');
ylabel('BPM reading [\mum]');
grid on
