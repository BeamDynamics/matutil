function plot_fit(this,vhv,code)

if ~isempty(this.axfit)
    ampl=1.e6*cat(1,vhv.ampl);
    fit=1.e6*cat(1,vhv.fit);
    new=~ishold(this.axfit);
    switch code
        case 'measure'
            set(this.axfit,'ColorOrderIndex',1);
            plot(this.axfit,ampl(:,1),fit(:,1),this.marker,ampl(:,2),fit(:,2),this.marker); % Plot meas. points
            hold(this.axfit,'on');
        case 'fit'
            [~,fhv]=bpmoffset.solve(fit,ampl);
            set(this.axfit,'ColorOrderIndex',1);
            h=plot(this.axfit,ampl(:,1),fhv(:,1),'-',ampl(:,2),fhv(:,2),'-');   % Plot fit
            legend(this.axfit,h,'H','V','Location','northwest');
            % if isfinite(ampl(1,1))
            %     ls=plot(ampl(1:2,1),vhv(1:2,1),'-o',ampl(1:2,2),vhv(1:2,2),'-o');
            %     set(ls,'LineWidth',1.5);
            % end
    end
    if new
        xlabel(this.axfit,'Bump amplitude [\mum]');
        ylabel(this.axfit,'Fitted oscillation [\mum]');
        grid(this.axfit,'on');
    end
end
end
