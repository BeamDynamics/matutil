function plot_harms(h,vhv1,vhv2)

pt3=[-1 -1;0 -1;1 -1];
figure(h);
AB=pt3\vhv1;
best=real(AB(2,:)./AB(1,:));
plot(vhv1,'o');	% measured points
hold on
plot([-1 -1;0 -1;1 -1]*AB);	% linear fit of measured points
% best estimate
plot([best(1) -1]*AB(:,1),'s','MarkerEdgeColor',[0 0 1]);
plot([best(2) -1]*AB(:,2),'s','MarkerEdgeColor',[0 0.5 0]);
% control
plot(vhv2(6,1),'*','MarkerEdgeColor',[0 0 1]);
plot(vhv2(6,2),'*','MarkerEdgeColor',[0 0.5 0]);
hold off
grid on
