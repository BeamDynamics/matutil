function plot_orbit(h,hharm,vharm)

figure(h);
subplot(2,1,1);
bar(abs(hharm));
subplot(2,1,2);
bar(abs(vharm));
grid on
