function plot_bumps(this,orbits,hld)

if ~isempty(this.axbump)
    if nargin<3, hld='off';end
    hold(this.axbump,hld);
    set(this.axbump,'ColorOrderIndex',1);
    plot(this.axbump,(1:224)',1.e6*orbits);
    hold(this.axbump,'on');
    set(this.axbump,'ColorOrderIndex',1);
    plot(this.axbump,this.id,1.e6*orbits(this.id,:),'o');
    hold(this.axbump,'off');
    title(this.axbump,'Bump');
    xlabel(this.axbump,'BPM number');
    ylabel(this.axbump,'x/z [\mum]');
    legend(this.axbump,'H','V','Location','northwest');
    grid(this.axbump,'on');
end
end
