classdef idoffset < handle
    %IDOFFSET	BPM offset measurement in an ID section
    %   The IDOFFSET object control the measurement of BPM offsets in a
    %   straight section by driving a local bump at each end of the section
    
    properties (Constant)
        sh=bpmoffset.sh	% Horizontal bump control
        sv=bpmoffset.sv	% Vertical bump control
    end
    
    properties
        bpms                    % the 4 bpmoffset objects
        next=[0 0 0 0]          % Estimated bump amplitudes
        step                    % Bump step
        vhv=[]                  % History of measurements
    end
    
    methods (Static)
        function v=scanline(line)
            a2=0.001*sscanf(line,'%*s %*s %g/%g');
            v=struct('orbit',a2(1:2)');
        end
    end
    
    methods
        function this=idoffset(id,varargin)
            %IDOFFSET   Create the IDOFFSET object
            %
            %IDOFFSET(ID)
            %
            %IDOFFSET(...,OPTNAME,OPTVALUE,...)
            %   IDOFFSET accepts the following options:
            %
            %'axshunt':     Axes for plotting the orbit oscillation
            %'axbump':      Axes for plotting the bump shape
            %'axfit':       Axes for plotting fir of oscillations
            %'dispshunt':	Function for displaying shunt state
            %'logger':      Function for displaying the sequence
            
            [axshunt,options]=getoption(varargin,'axshunt');
            [axbump,options]=getoption(options,'axbump');
            [axfit,options]=getoption(options,'axfit',{[],[]});
            [dispshunt,options]=getoption(options,'dispshunt',{@bpmoffset.vshunt,@bpmoffset.vshunt});
            [logger,options]=getoption(options,'logger',{@bpmoffset.rdisp,@bpmoffset.rdisp}); %#ok<ASGLU>
            idm1=id-1;
            if idm1 < 1, idm1=idm1+32; end
            this.bpms=[bpmoffset(idm1,6,'axshunt',axshunt,'axbump',axbump,...
                'axfit',axfit{1},'logger',logger{1},'dispshunt',dispshunt{1}),...
                bpmoffset(idm1,7,'nop','logger',logger{1}),...
                bpmoffset(id,1,'nop','logger',logger{2}),...
                bpmoffset(id,2,'axshunt',axshunt,'axbump',axbump,...
                'axfit',axfit{2},'logger',logger{2},'dispshunt',dispshunt{2})];
            this.step=[this.bpms(1).step this.bpms(4).step];
        end
        
        function plot_theobump(this,varargin)
            %Plot the theoretical orbit bumps
            this.bpms(1).plot_theobump(varargin{:});
            this.bpms(4).plot_theobump('on');
        end
        
        function [v,orbit]=evalid(this,mess,ampl)
            %Set a bump (4 amplitudes) and evaluate the penalty (4 bpms)
            % bump_up, bump_down, bpm1, bpm2, bpm3, bpm4, fit_up, fit_down, std_up, std_down
            if nargin >= 3
                this.setbump(ampl);
            else
                ampl=[0.0 0.0 0.0 0.0];
            end
            [v1,orb1]=this.bpms(1).eval(ampl(1:2));
            [v4,orb4]=this.bpms(4).eval(ampl(3:4));
            orbit=0.5*(orb1+orb4);
            v1.orbit=orbit(this.bpms(1).id,:);
            v4.orbit=orbit(this.bpms(4).id,:);
            v=struct('ampl',[v1.ampl v4.ampl],'orbit',reshape(orbit([this.bpms.id],:)',1,8),...
                'fit',[v1.fit v4.fit],'std',[v1.std v4.std]);

            this.bpms(1).logger(mess,v1);
            this.bpms(2).logger(mess,struct('orbit',orbit(this.bpms(2).id,:)));
            this.bpms(3).logger(mess,struct('orbit',orbit(this.bpms(3).id,:)));
            this.bpms(4).logger(mess,v4);
        end
        
        function setbump(this,normampl)
            %Set the 2 bumps in both planes
            ampl=this.step.*normampl;
            ids=[this.bpms([1 4]).id];
            this.sh.setbump(ids,ampl([1 3]));
            this.sv.setbump(ids,ampl([2 4]));
        end
        
        function vhv=adjust(this)
            %ADJUST  2nd step: use estimate-step, estimate and estimate+step
            %VHV=ADJUSTID()
            %
            %VHV:       Penalty function
            
            this.bpms(1).marker='o';
            this.bpms(4).marker='o';
            estimate=this.next;
            sequence=[0 0 0 0 ;-1 -1 0 0;1 1 0 0;0 0 -1 -1;0 0 1 1];
            ampl=bpmoffset.set_amplitudes(estimate,sequence);
            
            amp0=bpmoffset.set_amplitudes(estimate,[-1.1 -1.1 -0.1 -0.1]);	% dummy point
            this.setbump(amp0);
            pause(1);
            
             vhv(2)        =this.evalid('Up-step',ampl(2,:));   % Test upstream -
            [vhv(3),orbit1]=this.evalid('Up+step',ampl(3,:));	% Test upstream +
            
            amp0=bpmoffset.set_amplitudes(estimate,[-0.1 -0.1 -1.1 -1.1]);	% dummy point
            this.setbump(amp0);
            pause(1);
            
             vhv(4)        =this.evalid('Down-step',ampl(4,:));	% Test dowstream -
            [vhv(1),orbit0]=this.evalid('Initial',ampl(1,:));	% Test dowstream 0
            [vhv(5),orbit2]=this.evalid('Down+step',ampl(5,:));	% Test dowstream +
            
            fit=cat(1,vhv.fit);
            ap=cat(1,vhv.ampl);
            this.bpms(1).plot_bumps(orbit1-orbit0,'off');
            this.bpms(4).plot_bumps(orbit2-orbit0,'on');
            this.bpms(1).plot_fit(struct('ampl',ap(:,1:2),'fit',fit(:,1:2)),'fit');
            this.bpms(4).plot_fit(struct('ampl',ap(:,3:4),'fit',fit(:,3:4)),'fit');
            
            this.next=bpmoffset.solve(fit,ap)./this.step;
            this.vhv=[this.vhv vhv];
        end
        
        function vhv=control(this)
            %CONTROL 3rd step: measure and give a new estimate
            %VHV=CONTROL()
            %
            %VHV:       Penalty function
            
            this.bpms(1).marker='*';
            this.bpms(4).marker='*';
            estimate=this.next;
            ampl=bpmoffset.set_amplitudes(estimate,0);
            
            amp0=bpmoffset.set_amplitudes(estimate,-0.1);	% dummy point
            this.setbump(amp0);
            pause(1);
            
            vhv=this.evalid('Control',ampl);
            
            this.vhv=[this.vhv vhv];
            this.next=bpmoffset.solve(cat(1,this.vhv.fit),cat(1,this.vhv.ampl))./this.step;
        end
        
        function vhv=best(this)
            [~,idhv]=min(abs(cat(1,this.vhv.fit)));
            orbit=cat(1,this.vhv.orbit);
            orb14=orbit(:,[1 2 7 8]);
            [~,orb23]=bpmoffset.solve(orbit(:,3:6),orb14);
            [ampl1,ampl4]=sel(cat(1,this.vhv.ampl));
            [orbit1,orbit4]=sel(orb14);
            [orbit2,orbit3]=sel(orb23);
            [fit1,fit4]=sel(cat(1,this.vhv.fit));
            [std1,std4]=sel(cat(1,this.vhv.std));
            vhv=struct('ampl',[ampl1 ampl4],'orbit',[orbit1 orbit2 orbit3 orbit4],...
                'fit',[fit1 fit4],'std',[std1 std4]);

            this.bpms(1).logger('Best',struct('ampl',ampl1,'orbit',orbit1,'fit',fit1,'std',std1));
            this.bpms(2).logger('Best',struct('orbit',orbit2));
            this.bpms(3).logger('Best',struct('orbit',orbit3));
            this.bpms(4).logger('Best',struct('ampl',ampl4,'orbit',orbit4,'fit',fit4,'std',std4));

            function [resup,resdown]=sel(arr)
                resup=[arr(idhv(1),1) arr(idhv(2),2)];
                resdown=[arr(idhv(3),3) arr(idhv(4),4)];
            end
        end
        
        function vhv=scan(this,fname)
            this.bpms(1).marker='o';
            this.bpms(4).marker='o';
            fid=fopen(fname);
            vhv=[];
            ok=true;
            while ok
                line=fgetl(fid);
                if ~ischar(line)
                    break;
                end
                v1=bpmoffset.scanline(line);
                v2=idoffset.scanline(fgetl(fid));
                v3=idoffset.scanline(fgetl(fid));
                v4=bpmoffset.scanline(fgetl(fid));
                vhv=[vhv struct('ampl',[v1.ampl v4.ampl],...
                    'orbit',[v1.orbit v2.orbit v3.orbit v4.orbit],...
                    'fit',[v1.fit v4.fit],'std',[v1.std v4.std])]; %#ok<AGROW>
            end
            fclose(fid);
            ampl=cat(1,vhv.ampl);
            fit=cat(1,vhv.fit);
            this.bpms(1).show_vhv(struct('ampl',ampl(:,1:2),'fit',fit(:,1:2)));
            this.bpms(4).show_vhv(struct('ampl',ampl(:,3:4),'fit',fit(:,3:4)));
            this.vhv=vhv;
            this.next=bpmoffset.solve(cat(1,vhv.fit),cat(1,vhv.ampl))./this.step;
        end
    end
    
end

