function array = numpy2array(pyarray)
%NUMPY2ARRAY Convert a numpy array into a Matlab array
%
%MARR=NUMPY2ARRAY(PYARR) converts a numpy array by keeping the memory
%order: the dimensions of the matlab array are swapped
%
%MARR=NUMPY2ARRAY(PYARR.T) gives a Matlab array with the same dimensions

shape=double(py.array.array('d',pyarray.T.shape));
array=double(py.array.array('d',pyarray.ravel()));
if length(shape) > 1
    array=reshape(array,shape);
end
end
