function wysiwyg

oldu = get( gcf,'Units');
set( gcf,'Units', get( gcf,'PaperUnits') );
newp = get( gcf,'PaperPosition');
oldp = get( gcf,'Position');
set( gcf,'Position', [ oldp(1) oldp(2)+oldp(4)-newp(4) newp( 3:4 ) ] );
set( gcf,'Units', oldu );
