classdef Error < cs.DevError
    %Error Exception thrown by a TACO access error
    
% hex2dec('1C080000') 470286336 : 'invalid error code (LF)'
% hex2dec('1C080001') 470286337 : 'device server unknown problem'
% hex2dec('1C080002') 470286338 : 'no connection'
% hex2dec('1C080003') 470286339 : 'unknown command'
% hex2dec('1C080004') 470286340 : 'wrong device state'
% hex2dec('1C080005') 470286341 : 'data type error'
% hex2dec('1C080006') 470286342 : 'file access error'
% hex2dec('1C080007') 470286343 : 'out of range'
% hex2dec('1C080008') 470286344 : 'cancelled operation'
% hex2dec('1C080009') 470286345 : 'asynchronous timeout'
% hex2dec('1C08000A') 470286346 : 'missing data'
% hex2dec('1C08000B') 470286347 : 'Error accessing Tango database'
% 
    properties
        errorcode
    end
    
    methods
        function err=Error(taconame,varargin)
            %Error(devname,funcname,errorcode)
            %Error(devname,identifier,funcname,errorcode)
            %Error(devname,identifier,funcname,reason,desc)
            if length(varargin)==2
                [id,fname,errnum]=deal(varargin{[1 1:end]});
                errmess=dverrmess(errnum);
            elseif isnumeric(varargin{3})
                [id,fname,errnum]=deal(varargin{1:end});
                errmess=dverrmess(errnum);
            else
                [id,fname,reason,desc]=deal(varargin{1:4});
                errnum=int32(470286336);
                errmess=[reason ' : ' desc];
            end
            err@cs.DevError(taconame,['Taco:' id],...
                'Error evaluating %s(''%s''): %s', fname, taconame,errmess);
            err.errorcode=errnum;
        end
    end
end
