classdef (ConstructOnLoad) Device < cs.Device
    %Device Interface to TACO devices
    %
    %Constructor:
    %   >> dev=taco.Device(device_name);
    %
    %Attribute access:
    %   >> value=dev.attribute_name;
    %   >> dev.attribute_name=value;
    %
    %Command access:
    %   >> argout=dev.command_name(argin);
    %
    % Device Properties:
    %Name:      Name of the device (RO)
    %State:     State of the device (taco.DevState enumeration, RO)
    %Status:    Status string (RO)
    %Value:     Read Value (RO)
    %Setpoint:  Set point (RW)
    %XValue:    Structure with fields 'set' and 'read' (RW)
    %timeout:   Client time-out (WO)
    %
    % Device Methods:
    %cmd        Send a command
    %check      Send an optional command and wait for a resulting condition
    %attributes List of attributes
    %commands   List of Taco commands
    %signals    List of Taco signals
    %isState    Checks the device state
    %......     Any Taco command is available as a method
    
    properties(Access=private,Hidden)
        taconame
    end
    properties(Constant,Access=private,Hidden)
        alist={'State';'Status';'Value';'Setpoint'}
        connector=taco.connector()
    end
    properties(Dependent=true,SetAccess=private)
        Name	% Device name (RO)
        State	% State of the device, taco.DevState enumeration (RO)
        Status	% Status string (RO)
        Value   % Device value
    end
    properties(Dependent=true)
        Timeout % Client time-out (WO)
    end
    properties(Dependent=true)
        XValue
        Setpoint % Device Setpoint
    end
    properties(Transient,Access=private,Hidden)
        handle=0
    end
    
    methods(Static,Access=protected,Hidden)
        function st=any2state(code)
            st=taco.DevState.Get(code);
        end
    end
    methods(Access=protected,Hidden)
        function val=get_signal(dev,attrnum)
            %Get a Taco attribute
            %value=device.get_signal(attrname)
            %attrname:   attribute name
            attr=dev.dvz(@dvcmd,'DevReadSigValues');
            %           val=struct('set',cell(size(attrnum)),'read',num2cell(attr(attrnum)));
            val=attr(attrnum);
        end
        function varargout=dvz(dev,dvfunc,varargin)
            if nargout==0
                [~,errcode]=dvfunc(dev.handle, varargin{:});
            else
                varargout=cell(1,nargout);
                [varargout{:},errcode]=dvfunc(dev.handle, varargin{:});
            end
            if any(errcode~=0)
                taco.Error(dev.Name,func2str(dvfunc),errcode).throw();
            end
        end
    end
    
    methods
        
        % Constructor
        
        function dev=Device(varargin)
            % build interface to a Taco device
            allnames=cs.allnames(varargin{:});
            n=length(allnames);
            if cs.dbgprintf(2,'taco.Device constructor(%d)',n), dbstack; end
            if n>1, dev(n,1)=taco.Device();end
            for i=1:n
                dev(i).taconame=allnames{i}; %#ok<AGROW>
            end
        end
        
        % Property access
        
        function set.taconame(dev,name)
            if ischar(name) && ~isempty(name)
                dev.taconame=name;
                dev.handle=dev.connector.open(name); %#ok<MCSUP>
                proplist=dev.signals();
                arrayfun(@(i) scanattribute(proplist{i},i),1:length(proplist));
                cellfun(@scancommands,dev.commands());
            else
                error('Taco:badname','Device name must be a non-empty string.');
            end
            function scanattribute(attrname,i)
                dev.addattribute(attrname,@(dev) get_signal(dev,i));
            end
            function scancommands(cmdname)
                dev.addcommand(cmdname);
            end
        end
        function nam=get.Name(dev)
            nam=dvname(dev.handle);
        end
        function State=get.State(dev)
            State=taco.DevState.Get(dev.dvz(@dvcmd,'DevState'));
        end
        function Status=get.Status(dev)
            Status=dev.dvz(@dvcmd,'DevStatus');
        end
        function Value=get.Value(dev)
            [~,Value]=dev.dvz(@dvread);
        end
        function Setpoint=get.Setpoint(dev)
            [Setpoint,~]=dev.dvz(@dvread);
        end
        function val=get_attribute(dev,attrname) %#ok<INUSD>
            [vset,vread]=dev.dvz(@dvread);
            val=struct('set',vset,'read',vread);
        end
        function val=trycatch_attribute(dev,attrname,defval) %#ok<INUSL>
            if nargin < 3, defval=NaN; end
            [vset,vread,errcode]=dvread(dev.handle);
            if all(errcode == 0)
                val=struct('set',vset,'read',vread,'error',0);
            else
                err=taco.Error(dev.Name,'dvread',errcode);
                val=struct('set',defval,'read',defval,'error',err);
            end
        end
        function XValue=get.XValue(dev)
            [vset,vread]=dev.dvz(@dvread);
            XValue=struct('set',vset,'read',vread);
        end
        function set.Setpoint(dev,Value)
            dev.dvz(@dvset,Value);
        end
        function set.XValue(dev,Value)
            dev.dvz(@dvset,Value);
        end
        function set.Timeout(dev,Value)
            dev.dvz(@dvtimeout,Value);
        end
        
        % Methods
        
        function cmdlist=commands(dev)
            % Lists the available Taco commands
            %   list=device.commands();
            if nargout==0
                dvquery(dev.handle);
            else
                cmdlist=dvquery(dev.handle);
            end
        end
        function attrlist=attributes(dev)
            % Lists the available Taco attributes
            %   device.attributes()
            %   list=device.attributes()
            if nargout==0
                sigs=dev.signals();
                fprintf('Attributes for device %s\n',dev.Name);
                fprintf('    %s\n',dev.alist{:},sigs{:});
            else
                attrlist=[dev.alist;dev.signals()];
            end
        end
        function siglist=signals(dev)
            cmdlist=dev.commands();
            if any(strcmp('DevGetSigConfig',cmdlist))
                buf=dev.dvz(@dvcmd,'DevGetSigConfig');
                nb=str2double(buf{1});
                sigs=reshape(buf(2:end),nb,[]);
                snms=regexp(sigs(1,:),'[^/]*$','match');
                siglist=[snms{:}]';
                pb=strcmp(siglist,'Value');
                if any(pb)
                    warning('Taco:WrongSignal','A signal named "Value" was renamed as "SigValue"');
                    siglist(pb)={'SigValue'};
                end
            else
                siglist={};
            end
        end
        function varargout=cmd(dev,varargin)
            %Send a Taco command
            %argout=device.cmd(command,argin)
            %command:   command name
            varargout=cell(1,nargout);
            [varargout{:}]=dev.dvz(@dvcmd,varargin{:});
        end
        function setparam(dev,varargin)
            %Set parameters
            %
            %	device.dvsetparam(name,value,...) calls device-specific functions
            %   to set certain properties
            %
            %    name: string specifying the parameter name
            %    value: parameter value to be set.
            %	   numerical values must be specified as DOUBLE arrays
            %	   logical values must be specified with 'on' or 'off' strings
            %
            %   Currently supported parameters
            %	'ResonSetOffset'	string: base file name
            %      for Resonance Correction (amplitude or phase device)
            dev.dvz(@dvsetparam,varargin{:});
        end
        function err=error(dev,id,varargin)
            %Generates a Taco error
            %error(id,errorcode)
            %error(id,reason,desc)
            origin='';
            st=dbstack(1);
            if length(st)>=1, origin=st(1).name; end
            err=taco.Error(dev.Name,id,origin,varargin{:});
        end
        function delete(dev)
            if cs.dbgprintf(2,'taco.Access destructor'), dbstack; end
            if dev.handle ~= 0
                dev.connector.close(dev.handle);
            else
                cs.dbgprintf(1,'Deleting empty devices');
            end
        end
        
        % Compatibility function
        
        function varargout=dvcmd(dev,varargin)
            %Send a Taco command
            varargout=cell(1,nargout);
            [varargout{:}]=dvcmd(dev.handle,varargin{:});
        end
        function varargout=dvread(dev)
            %Read the device value
            varargout=cell(1,nargout);
            [varargout{:}]=dvread(dev.handle);
        end
        function varargout=dvsetparam(dev,varargin)
            %Set parameters
            varargout=cell(1,nargout);
            [varargout{:}]=dvsetparam(dev.handle,varargin{:});
        end
    end
end
