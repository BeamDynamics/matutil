classdef DevState < cs.DevState
    %DevState Enumeration representing device states
    % (On,Off,Fault,...)
    %
    % Comparison operators are redefined to use a severity order,
    % where "On" is minimum and "Unknown" is maximum
    %
    % constant state values may be declared as:
    %       taco.DevState.On
    %
    
    methods(Static)
        function state=Get(code)
            %returns the DevState value corresponding to a string
            persistent sval sstr totango
            if ischar(code)
                if isempty(sstr)
                    [sval,sstr]=enumeration('taco.DevState');
                end
                state=sval(find(strcmpi(code,sstr),1));
            else
                if isempty(totango)
                    totango=[13,1,0,2,3,14,15,4,5,6,16,9,7,17,10,18:25,8,26:42,13,43,44,10,11,12,45,46];
                end
                state=taco.DevState(totango(code+1));
            end
        end
        function state=Undef(varargin)
            if nargin==1 && isscalar(varargin{1})
                state=taco.DevState.Undef(varargin{[1 1]});
            else
                state=repmat(taco.DevState.Unknown,varargin{:});
            end
        end
    end
    
    enumeration
        % Tango states
        
        On          (0)
        Off         (1)
        Closed      (2)
        Open		(3)
        Inserted	(4)
        Extracted   (5)
        Moving      (6)
        Standby     (7)
        Fault       (8)
        Init		(9)
        Run         (10)
        Running     (10)
        Alarm       (11)
        Disabled	(12)
        Unknown     (13)
        % Additional Taco States
        
        Low         (14)
        High		(15)
        Warmup      (16)
        Service     (17)
        Local       (18)
        Remote      (19)
        Automatic   (20)
        Ramping		(21)
        Tripped     (22)
        HVEnabled   (23)
        BeamEnabled (24)
        Blocked     (25)
        Starting	(26)
        Stopping	(27)
        StartRequested	(28)
        StopRequested	(29)
        PositiveEndStop	(30)
        NegativeEndStop	(31)
        BakeRequested   (32)
        Baking         	(33)
        StopBake        (34)
        ForcedOpen      (35)
        ForcedClosed    (36)
        OffUnauthorised (37)
        OnNotRegular    (38)
        Resetting       (39)
        Forbidden       (40)
        Opening         (41)
        Closing         (42)
        Counting        (43)
        Stopped         (44)
        StandbyNotReached	(45)
        OnNotReached	(46)
        
    end
end
