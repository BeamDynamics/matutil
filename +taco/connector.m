classdef connector < handle
    %UNTITLED5 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        list=[]
    end
    
    methods(Access=?taco.Device)
        function c=connector()
            cs.dbgprintf(2,'taco.connector constructor');
        end
    end
    methods
        function handle=open(self,devname)
            handle=dvope1(devname);
            if length(self.list) >= handle
                nb=self.list(handle);
            else
                nb=0;
            end
            if nb == 0
                cs.dbgprintf(1,'Connected to <%s>',devname);
            end
            self.list(handle)=nb+1;
        end
        function close(self,handle)
            if (handle>0) && (length(self.list)>=handle) && (self.list(handle)>0)
                self.list(handle)=self.list(handle)-1;
                if self.list(handle) <= 0
                    dvclose(handle)
                    cs.dbgprintf(1,'Disconnected from <%s>',dvname(handle));
                end
            else
                error('Taco:NoDevice','Attempt to close a missing device');
            end
        end
        function delete(self)  %#ok<INUSD>
            cs.dbgprintf(2,'taco.connector destructor');
        end
    end
end

