classdef (ConstructOnLoad) TestDevice < taco.Device
    
    methods
        % Constructor
        function dev=TestDevice(varargin)
            dev=dev@taco.Device(varargin{:});
            disp(mfilename('class'));
        end
        
        % New method
        function [v1,v2]=doublecmd(dev,cmd1,cmd2,arg1,arg2)
            v1=dev.cmd(cmd1,arg1); % Syntax 1
            v2=dev.(cmd2)(arg2);   % Syntax 2
        end
        
        function pl=testplane(dev,varargin)
            dev.error('Cs:Test','test','this is a test of error method').throw();
            pl=cs.planeid(varargin{:});
        end
    end
    
end
