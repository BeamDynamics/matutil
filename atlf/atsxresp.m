function [respd,respo] = atsxresp(mach,plane)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

step=1.e-5;
if strcmp(plane,'h')
   coord=1;
   move=@(elem,dx) atshiftelem(elem,dx,0);
else
   coord=3;
   move=@(elem,dz) atshiftelem(elem,0,dz);
end
bpmidx=findcells(mach,'BetaCode','PU');
sxidx=allsexts(mach);
[lin0,tune,xsi]=atlinopt(mach,0.0,bpmidx);
respd=NaN(length(bpmidx),length(sxidx));
respo=NaN(length(bpmidx),length(sxidx));
for i=1:length(sxidx)
   mach{sxidx(i)}=move(mach{sxidx(i)},step);
   [lin,tune,xsi]=atlinopt(mach,0.0,bpmidx);
   orbit=(cat(2,lin.ClosedOrbit)-cat(2,lin0.ClosedOrbit))';
   dispersion=(cat(2,lin.Dispersion)-cat(2,lin0.Dispersion))';
   respo(:,i)=orbit(:,coord)/step;
   respd(:,i)=dispersion(:,coord)/step;
   mach{sxidx(i)}=move(mach{sxidx(i)},0);
end
end

function idx=allsexts(atstruct)
thinsext=findcells(atstruct,'BetaCode','LD3');
thicksext=findcells(atstruct,'BetaCode','SX');
idx=sort([thinsext thicksext]);
end
