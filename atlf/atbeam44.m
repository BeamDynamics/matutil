function [bm1,bm2,lindata,varargout]=atbeam44(varargin)
global NUMDIFPARAMS

if nargin >= 3
   REFPTS=varargin{3};
else
   REFPTS= 1;
end

spos = findspos(varargin{1},REFPTS);
[M44,MS,orb]=findm44(varargin{:});

lindata = struct('ElemIndex',num2cell(REFPTS),'SPos',num2cell(spos),...
    'ClosedOrbit',num2cell(orb,1),'M44',squeeze(num2cell(MS,[1 2]))');

[vv,ee]=eig(M44);
bm1=-2*real(vv(:,1)*vv(:,1)')/imag(det(vv(1:2,1:2))+det(vv(3:4,1:2)));
bm2=-2*real(vv(:,3)*vv(:,3)')/imag(det(vv(1:2,3:4))+det(vv(3:4,3:4)));
vp=diag(ee);
varargout{1}=angle(vp([1 3]))'/2/pi;

if nargout >= 5
    if isfield(NUMDIFPARAMS,'DPStep')
        dDP = NUMDIFPARAMS.DPStep';
    else
        dDP =  1e-8;
    end
    % Calculate tunes for DP+dDP
    DP2=varargin{2}+dDP;
    orbD = findorbit4(varargin{1},DP2,REFPTS);
    [bd1,bd2,LD,tunesD] = atbeam44(varargin{1},DP2);
    varargout{2} = (tunesD - varargout{1})/dDP;
    DISPERSION = num2cell((orbD-orb)/dDP,1);
    
    [lindata.Dispersion] = deal(DISPERSION{:});
end
