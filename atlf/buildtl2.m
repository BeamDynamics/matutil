function datain=buildtl2
dp=rbend('DIP', 2.094, 0.0951998, 0.0475999, 0.0475999, 0, 'BendLinearPass');

se2=rbend('SE2', 1.6, -0.080281, -0.04014255, -0.04014255, 0, 'BendLinearPass');
dr01=drift('DR01', 0.3, 'DriftPass');
fs1=marker('FS1', 'IdentityPass');
dr02=drift('DR02', 0.8, 'DriftPass');
cv0=marker('CV0', 'IdentityPass');
dr03=drift('DR03', 2.204, 'DriftPass');
cv1=marker('CV1', 'IdentityPass');
drfs=drift('DR04', 0.277, 'DriftPass');
fs2=marker('FS2', 'IdentityPass');
fs3=marker('FS3', 'IdentityPass');
fs4=marker('FS4', 'IdentityPass');
fs5=marker('FS5', 'IdentityPass');
fs6=marker('FS6', 'IdentityPass');
fs7=marker('FS7', 'IdentityPass');
fs9=marker('FS8', 'IdentityPass');
fs8=marker('FS9', 'IdentityPass');
fs10=marker('FS10', 'IdentityPass');
fs11=marker('FS11', 'IdentityPass');
ch1=marker('CH1', 'IdentityPass');
drk=drift('DRK', 0.373, 'DriftPass');
qf1=quadrupole('QF1', 0.6, 0.622514, 'QuadLinearPass'); %1.06611e-02
dr04=drift('DR04', 1.810, 'DriftPass');
ch2=marker('CH2', 'IdentityPass');
qd2=quadrupole('QD2', 0.6, -0.547073, 'QuadLinearPass'); % -1.04653e-02
dr05=drift('DR05', 1.079, 'DriftPass');
dr06=drift('DR06', 3.395, 'DriftPass');
qf3=quadrupole('QF3', 0.6, 1.00322, 'QuadLinearPass'); %1.06751e-02
cv3=marker('CV3', 'IdentityPass');
ch3=marker('CH3', 'IdentityPass');
dr07=drift('DR07', 1.473, 'DriftPass');
dr08=drift('DR08', 0.377, 'DriftPass');
qf4=quadrupole('QF4', 0.6, 1.0437, 'QuadLinearPass'); %1.06590e-2
dr09=drift('DR08', 0.847, 'DriftPass');
qd5=quadrupole('QD5', 0.6, -0.835929, 'QuadLinearPass'); % -1.05352e-02
cv4=marker('CV4', 'IdentityPass');
ch4=marker('CH4', 'IdentityPass');
dr10=drift('DR10', 4.577, 'DriftPass');
ws=marker('WS', 'IdentityPass');
dr11=drift('DR11', 0.584, 'DriftPass');
qf6=quadrupole('QF6', 0.6, 0.432020, 'QuadLinearPass'); %1.06684e-02
cv5=marker('CV5', 'IdentityPass');
ch5=marker('CH5', 'IdentityPass');
dr12=drift('DR12', 0.6849, 'DriftPass');
dr13=drift('DR13', 2.5900, 'DriftPass');
cv6=marker('CV6', 'IdentityPass');
ch6=marker('CH6', 'IdentityPass');
qf7=quadrupole('QF7', 0.6, 0.710275, 'QuadLinearPass'); %1.06645e-02
dr14=drift('DR14', 3.659, 'DriftPass');
dr15=drift('DR15', 1.7544, 'DriftPass');
qd8=quadrupole('QD8', 0.6, -0.565262, 'QuadLinearPass'); %1.06737e-02
dr16=drift('DR16', 0.543, 'DriftPass');
cv7=marker('CV7', 'IdentityPass');
ch7=marker('CH7', 'IdentityPass');
qf9=quadrupole('QF9', 0.6, 0.369646, 'QuadLinearPass'); %1.06545e-02
dr17=drift('DR17', 0.735, 'DriftPass');
qd10=quadrupole('QD10', 0.6, -0.534678, 'QuadLinearPass'); %1.06598e-02
dr18=drift('DR18', 1.770, 'DriftPass');
qf11=quadrupole('QF11', 0.6, 0.780754, 'QuadLinearPass'); %1.06344e-02
dr19=drift('DR19', 0.379, 'DriftPass');
dr20=drift('DR20', 0.727, 'DriftPass');
qd12=quadrupole('QD12', 0.6, -0.954485, 'QuadLinearPass'); %1.06837e-02
cv8=marker('CV8', 'IdentityPass');
ch8=marker('CH8', 'IdentityPass');
dr21=drift('DR21', 0.607, 'DriftPass');
cv9=marker('CV9', 'IdentityPass');
qf13=quadrupole('QF13', 0.6, 1.14377, 'QuadLinearPass'); %1.06610e-02
dr22=drift('DR22', 0.517, 'DriftPass');
qd14=quadrupole('QD14', 0.6, -0.378535, 'QuadLinearPass'); %1.06598e-02
dr23=drift('DR23', 0.830, 'DriftPass');
dr24=drift('DR24', 0.935, 'DriftPass');
dr25=drift('DR25', 0.360, 'DriftPass');
ts12=0.0426733;
s12=rbend('S12', 22.9183*ts12, ts12, ts12/2, ts12/2, 0, 'BendLinearPass');
dr26=drift('DR26', 0.150, 'DriftPass');
dr27=drift('DR27', 1.225, 'DriftPass');
ts3=0.0218166;
s3=rbend('S3', 22.9183*ts3, ts3, ts3/2, ts3/2, 0, 'BendLinearPass');
dr28=drift('DR28', 0.0, 'DriftPass');


tl2=[se2 dr01 fs1 dr02 cv0 dr03 cv1 drfs fs2 drfs ch1 drk,...
	qf1 dr04 drfs fs3 drfs ch2 drk qd2 dr05 dp dr06 qf3 drk,...
	cv3 drfs fs4 drfs ch3 dr07 dp  dr08 qf4 dr09 qd5 drk cv4,...
	drfs fs5 drfs ch4 dr10 ws dr11 qf6 drk cv5 drfs fs6 drfs,...
	ch5 dr12 dp dr13 cv6 drfs fs7 drfs ch6 drk qf7 dr14 dp,...
	dr15 qd8 dr16 cv7 drfs fs8 drfs ch7 drk qf9 dr17 dr17 qd10,...
	dr18 qf11 dr19 dp dr20 qd12 drk cv8 drfs fs9 drfs ch8,...
	dr21 qf13 dr22 qd14 dr23 cv9 dr24 fs10 dr25 s12 dr26 s12,...
	dr27 s3 fs11 dr28];

buildlat(tl2);
evalin('caller','global THERING FAMLIST');
datain.ClosedOrbit=zeros(4,1);
datain.beta=[2.1 1.17];
datain.alpha=[-0.66062 2.36278];
datain.mu=[0 0];
