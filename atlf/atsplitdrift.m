function newring=atsplitdrift(ring,position,frac,newelem)
%ATSPLITDRIFT   inserts an element into a drift space
%
% NEWRING=ATSPLITDRIFT(RING,DRIFTPOS,SPLIT) inserts a marker (zero-length) element
%   at distance SPLIT ( 0 < SPLIT < 1) into a drift space 
%   located at DRIFTPOS in RING
% 
% NEWRING=ATSPLITDRIFT(RING,DRIFTPOS,SPLIT,NEWELEMENT) inserts NEWELEMENT
%   at distance SPLIT ( 0 < SPLIT < 1) into a drift space 
%   located at DRIFTPOS in RING
% 
% Number of elements in the RING is thus increased by 2
% SPLIT (controls the position of the split 
% L1 = L0*SPLIT
% L2 = L0(1-SPLIT)
%  where L0 is the length of the original DRIFT
%   
% See also: MERGEDRIFT
 
L0 = ring{position}.Length;
if (frac < 0 | frac >1)
	error('SPLIT argument must be (0..1)');
elseif nargin < 4
    newelem=struct('FamName','TEMPSPLIT', 'Length', 0, 'PassMethod','IdentityPass');
end
 % Check if a new element will fit
L1=L0*frac;
L2=L0*(1-frac)-newelem.Length;
if L2 < 0, warning('The inserted element is too long.'); end
part1=ring(1:position);
part2=ring(position:end);
part1{position}.Length = L1;
part2{1}.Length = L2;
newring=[part1(:);{newelem};part2(:)];
