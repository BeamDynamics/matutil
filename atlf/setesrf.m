function ring = setesrf(v,varargin)
%ESRF = SETESRF(RING)
%
%Take en ESRF lattice and add:
%- ID source points ('IDn')
%- Hard and Soft bending magnet source points ('BMHard' and 'BMSOFT')
%- IAX locations ('SR/D-EMIT/Cn')
%- Pinhole camera locations ('SR/D-EMIT/xx')
%And call 'atclean'
%
%SETESRF(...,'hardangle',value)
%SETESRF(...,'softangle',value)
%   Set the angle of bending magnet sources
%
%SETESRF(...,'remove',famnames)
%   remove elements identified by the family names (cell array)
%
%SETESRF(...,'reduce',true)
%   group similar elements together by calling atreduce
%
%SETESRF(...,'MaxOrder',n)
%   Limit the order of polynomial field expansion to n at maximum
%
%SETESRF(...,'NumIntSteps',m)
%   Set the NumIntSteps integration parameter to at least m

[energy,options]=getoption(varargin,'energy',6.04E9);
[hardangle,options]=getoption(options,'hardangle',0.009);
[softangle,options]=getoption(options,'softangle',0.003);
reduce=getoption(options,'reduce',false);
a=addidsourcepoints(v);
keep=atgetcells(a,'FamName','IDMarker|ID\d+') | atgetcells(a,'Class','Monitor');
a=atclean(a,options{:},'keep',keep);
if ~reduce
    a=addpinholes(a);
    a=addiaxsourcepoints(a);
    a=addbmsourcepoints(a);
end
bpms=atgetcells(a,'Class','Monitor');
a(bpms)=atsetfieldvalues(a(bpms),'FamName',sr.bpmname((1:sum(bpms))'));
rp=atgetcells(a,'Class','RingParam');
ring=[{atringparam('ESRF',energy,1)};a(~rp)];

    function mach=addbmsourcepoints(mach)
        if ~any(atgetcells(mach,'FamName','BMSOFT|BMHARD'))
            soft=atgetcells(mach,'FamName','B2S');	% Add bending magnet source points
            nb1=sum(soft);
            hard=soft([end 1:end-1]);   % Next element
            softmag=atgetfieldvalues(mach(soft),'BendingAngle');
            softfrac=softangle./softmag;
            hardfrac=(hardangle-softmag)./atgetfieldvalues(mach(hard),'BendingAngle');
            frac=reshape([softfrac';hardfrac'],[],1);
%             if nb1==32
%                 elems=reshape(...
%                     [arrayfun(@(cell) atmarker(sprintf('BMSOFT%d',cell)),[4:32 1:3]);...
%                     arrayfun(@(cell) atmarker(sprintf('BMHARD%d',cell)),[4:32 1:3])],...
%                     [],1);
%             else
                elems=repmat({atmarker('BMSOFT');atmarker('BMHARD')},nb1,1);
%             end
            mach=atinsertelems(mach,soft|hard,frac,elems);
        end
    end
    function mach=addiaxsourcepoints(mach)
        if ~any(atgetcells(mach,'FamName','IAX|SR/D-EMIT/C\d+'))
            h1=atgetcells(mach,'FamName','B1H');	% Add IAX source points
            h1angle=atgetfieldvalues(mach(h1),'BendingAngle');
            h1frac=0.04715./h1angle;
            if sum(h1)==32
                iax=arrayfun(@(cell) atmarker(sprintf('SR/D-EMIT/C%d',cell)),[4:32 1:3]);
            else
                iax=atmarker('IAX');
            end
            %mach=atinsertelems(mach,h1,h1frac,atmarker('IAX'));
            mach=atinsertelems(mach,h1,h1frac,iax);
        end
    end
    function mach=addidsourcepoints(mach)
        if ~any(atgetcells(mach,'FamName','IDMarker|ID\d+'))
            sdf=atgetcells(mach,'FamName','SD[HL].');
            sdf(sdf)=bitand(1:sum(sdf),1);    % Take every 2nd drift
            if sum(sdf)==32
                id=arrayfun(@(cell) atmarker(sprintf('ID%d',cell)),[4:32 1:3]);
            else %2
                id=atmarker('IDMarker');
            end
            mach=atinsertelems(mach,sdf,0,id);
        end
    end
    function mach=addpinholes(mach)
        if ~any(atgetcells(mach,'FamName','PINHOLE|SR/D-EMIT/ID\d+'))
            h1=atgetcells(mach,'FamName','B1H');
            if sum(h1)==32
                h1=mcellid(h1,25,1);
                pinid=arrayfun(@(cell) atmarker(sprintf('SR/D-EMIT/ID%d',cell)),25);
            else
                pinid=atmarker('PINHOLE');
            end
            mach=atinsertelems(mach,h1,0,pinid);
        end
        if ~any(atgetcells(mach,'FamName','PINHOLE|SR/D-EMIT/D\d+'))
            s2=atgetcells(mach,'FamName','B2S');
            if sum(s2)==32
                s2=mcellid(s2,[9 11],1);
                pind=arrayfun(@(cell) atmarker(sprintf('SR/D-EMIT/ID%d',cell)),[9 11]);
            else
                pind=atmarker('PINHOLE');
            end
            mach=atinsertelems(mach,s2,0,pind);
        end
    end
    function idx=mcellid(idx,cell,id)
        sel=reshape(false(sum(idx),1),32,[]);
        sel(cell,id)=true;
        idx(idx)=circshift(sel,-3);
    end

end
