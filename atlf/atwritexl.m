function atwritexl(ring,filename)
%ATWRITEXL Write a xlOptics input file
%
%ATWRITEXL(RING)
%   Prints the result in the command window
%ATWRITEXL(RING,FILENAME)
%   Prints the result in a text file
%
%See also ATWRITESURVEY

if nargin>=2
    [pname,fname,ext]=fileparts(filename);
    if isempty(ext), ext='.txt'; end
    fn=fullfile(pname,[fname ext]);
    [fid,mess]=fopen(fullfile(pname,[fname ext]),'wt');
    if fid==-1
        error('AT:FileErr','Cannot Create file %s\n%s',fn,mess);
    end
else
    fid=1;
end

%dips=atgetcells(ring,'Class','Bend');
%mrks=atgetcells(ring,'Class','Marker');
dips=cellfun(@(elem) strcmp(atguessclass(elem),'Bend'),ring);
mrks=cellfun(@(elem) strcmp(atguessclass(elem),'Marker'),ring);
radius=NaN(size(ring));
radius(dips)=atgetfieldvalues(ring(dips),'Length')./atgetfieldvalues(ring(dips),'BendingAngle');
radint=0.5*(radius([2:end 1])+radius([end 1:end-1]));
radius(mrks)=radint(mrks);      % skip markers between 2 identical dipoles
sameasnext=(abs(radius-radius([2:end 1])) < 0.00001);
sameasprev=sameasnext([end 1:end-1]);
try
    [energy,periods,voltage,harmnumber]=atenergy(ring);
catch
    periods=1;
    energy=1;
    voltage=0;
    harmnumber=1;
end

fprintf(fid,'%s\tTitle\n','XXXX');
fprintf(fid,'\tEnergy\t%i\n',1.e-9*energy);
fprintf(fid,'\tNcell\t%i\n',periods);
fprintf(fid,'\tBound\t%s\n','P');
fprintf(fid,'\tCoupling\t%g\n',0.01);
fprintf(fid,'\tVrf\t%g\n',1.e-6*voltage);
fprintf(fid,'\tHrf\t%g\n',harmnumber);
fprintf(fid,'\tParams\t%i\n',4);
fprintf(fid,'\n');
ok=cellfun(@printlm,ring,num2cell(~[sameasprev,sameasnext],2)); %#ok<NASGU>
fprintf(fid,'\tEnd\n');

if nargin>2
    fclose(fid);
end

    function ok=printlm(elem,wedge)
        elstr=at2xl(elem,wedge);
        ok=fprintf(fid,'%s\n',elstr{:});
    end
end

