function lost=atphasespace(ring,numturns,xini,dpp)
%ATPHASESPACE plots phase-space
%
%ATPHASESPACE(RING,XINI,NUMTURNS)
%
%RING:      Call array
%XINI:      Vector of initial conditions
%NTURNS:    Number of turns

if isvector(xini)
    rin=zeros(6,length(xini));
    rin(1,:)=xini(:)';
    rin(5,:)=dpp;
else
    rin=xini;
end

% rout=ringpass(ring(:,1),rin(:,1),1);
% tic;[rout,lost]=ringpass(ring(:,1),rin,numturns,'reuse');toc  % Crash
[rout,lost]=ringpass(ring(:),rin,numturns);
x=[rin(1,:);reshape(rout(1,:),[],numturns)'];
xp=[rin(2,:);reshape(rout(2,:),[],numturns)'];
plot(x,xp,'.','MarkerSize',8);
end
