function atwritebeta(ring,filename,split)
%ATWRITEBETA(ATSTRUCT,FILENAME,SPLIT)     Write a BETA file
%
%ATSTRUCT:  Cell array containing the AT description
%FILENAME:  Resulting BETA file
%SPLIT:     Elements to be printed at a beginning of line (default:none)

if nargin < 3, split=[]; end

[npercell,nperiods]=size(ring);
if isnumeric(split)
    a=false(npercell,1);
    a(split)=true;
else
    a=split;
end
tbl=atindex(ring(1:npercell));
elnames=fieldnames(tbl);
nelems=length(elnames);

for i=1:length(elnames)	% Count elements
    elnam=elnames{i};
    lm=ring{tbl.(elnam)(1)};
    if isdipole(lm)
        nelems=nelems+2;
    end
end

nstru=npercell;         % Count structure
for i=1:npercell
    lm=ring{i};
    if isdipole(lm)
        nstru=nstru+2;
    end
end

fid=fopen(filename,'w+t');
fprintf(fid,' *** MODE ***\n BETA\n');
fprintf(fid,' *** LIST OF ELEMENTS ***\n%6d\n',nelems);
for i=1:length(elnames)
    elnam=elnames{i};
    lm=ring{tbl.(elnam)(1)};
    if isfield(lm,'Class')
        switch lm.Class
            case 'Drift'
                write_elem(fid,elnam,'SD',[lm.Length]);
            case 'Monitor'
                write_elem(fid,elnam,'PU',[0,0]);
            case 'Corrector'
                write_elem(fid,elnam,'KI',lm.KickAngle);
            case {'Bend','Dipole'}
                radius=lm.Length/lm.BendingAngle;
                findex=-lm.PolynomB(2)*radius*radius;
                write_elem(fid,[elnam '_IN'],'CO',[lm.EntranceAngle,radius]);
                write_elem(fid,elnam,'DI',[lm.BendingAngle,radius,findex]);
                write_elem(fid,[elnam '_OUT'],'CO',[lm.ExitAngle,radius]);
            case 'Quadrupole'
                write_elem(fid,elnam,'QP',[lm.Length,lm.PolynomB(2)]);
            case 'Marker'
                write_elem(fid,elnam,'SD',0);
            case 'Sextupole'
                write_elem(fid,elnam,'SX',[lm.Length,lm.PolynomB(3)]);
            case 'RFCavity'
                write_elem(fid,elnam,'CA',[lm.Voltage,lm.HarmNumber,lm.Energy]);
            otherwise
                write_multipole(fid,lm,tbl.(elnam)(1));
        end
    elseif isfield(lm,'BetaCode')
        switch lm.BetaCode
            case 'SD'
                write_elem(fid,elnam,'SD',[lm.Length]);
            case 'PU'
                write_elem(fid,elnam,'PU',[0,0]);
            otherwise
                write_multipole(fid,lm,tbl.(elnam)(1));
        end
    else
        write_multipole(fid,lm,tbl.(elnam)(1));
    end
end

fprintf(fid,' *** STRUCTURE ***\n%6d\n',nstru);
stnames=cell(nstru,1);
b=false(nstru,1);
j=0;
for i=1:npercell
    lm=ring{i};
    if isdipole(lm)
        stnames{j+1}=[lm.FamName '_IN'];
        stnames{j+2}=lm.FamName;
        stnames{j+3}=[lm.FamName '_OUT'];
        b(j+1:j+3)=a(i);
        j=j+3;
    else
        stnames{j+1}=lm.FamName;
        b(j+1)=a(i);
        j=j+1;
    end
end

split=[find(b);j+1];
beg=1;
for i=1:length(split)
    bg=beg:10:split(i)-1;
    nd=[bg(2:end)-1 split(i)-1];
    for j=1:length(bg)
        fprintf(fid,'%s %s %s %s %s %s %s %s %s %s',stnames{bg(j):nd(j)});
        fprintf(fid,'\n');
    end
    beg=split(i);
end
fprintf(fid,'%d\n',nperiods);
fprintf(fid,' *** PARTICLE TYPE ***\n E\n');
fclose(fid);

    function write_elem(fid,name,code,params)
        fprintf(fid,'  %s %s\t%.7g\t%.12g\t%.12g\t%.12g',name,code,params);
        fprintf(fid,'\n');
    end

    function write_multipole(fid,lm,i)
        if isfield(lm,'PolynomB')
            order=find(lm.PolynomB ~= 0,1,'first');
        else
            order=[];
        end
        if isfield(lm,'Length') && lm.Length > 0	% Thick magnet
            if isempty(order) || order > 3
                warning('AT:WriteXl:Undefined','Element %i(%s) looks like a drift',i,lm.FamName);
                params=zeros(1,3);
            else
                params=[lm.PolynomB zeros(1,3)];
            end
            write_elem(fid,lm.FamName,'SX',[lm.Length,params(3)]);
        else                                        % Thin lens
            if isempty(order)
                warning('AT:WriteXl:Undefined','Element %i(%s) looks like a marker',i,lm.FamName);
                write_elem(fid,lm.FamName,'SD',0);
            else
                write_elem(fid,lm.FamName,'LD',[lm.PolynomB(order),2*order]);

            end
        end
    end

    function isd=isdipole(lm)
        isd=isfield(lm,'Class') && any(strcmp(lm.Class,{'Bend','Dipole'}));
    end

end

