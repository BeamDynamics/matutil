function [ring,nper,idsym]=buildesrferr(fname,varargin)
%BUILDESRFERR			build ESRF lattice with errors
%
%[ring,periods,idsym]=BUILDESRF(filename,cavipass,bendpass,quadpass)
%
%filename:	error file (optional)
%		(default /machfs/appdata/qem/race_quadco.aug28.nocor.dat)
%CAVIPASS:	pass method for cavities (default IdentityPass)
%BENDPASS:	pass method for cavities (default BendLinearPass)
%QUADPASS:	pass method for cavities (default QuadLinearPass)
%
%PERIODS: number of periods
%IDSYM: index of periodical elements

if nargin < 1 || isempty(fname)
   fname='/machfs/appdata/qem/race_quadco.aug28.nocor.dat';
end
[ring0,nper,idsym]=buildesrfdip(varargin{:});
ring=atqemreadq(ring0,fname);
