function eps=atinvariant(rin,data)

beta=atvalue(data,'beta',1)';
alpha=atvalue(data,'alpha',1)';
x=rin(1,:);
xp=alpha.*rin(1,:)+beta.*rin(2,:);
eps=(x.*x+xp.*xp)./beta;