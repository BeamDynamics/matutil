function atwritesurvey(ring,filename,varargin)
%ATWRITESURVEY	Generates a survey file
%
%ATWRITESURVEY(RING)
%   Prints the result in the command window
%ATWRITESURVEY(RING,FILENAME)
%   Prints the result in a file
%ATWRITESURVEY(RING,FILENAME,OPTNAME,OPTVAL,...)
%   Available options:
%   'offset',[0 0]      OFFSET for geometry computation
%   'bpms',false        Rename BPMs starting from 1
%   'drifts',0          Rename drifts starting from 1-n
%See also ATWRITEXL

[drbpms,options]=getoption(varargin,'bpms',false);
[drshift,options]=getoption(options,'drifts',0);
[offset,options]=getoption(options,'offset',[0 0]);

if nargin>=2 && ~isempty(filename)
    [pname,fname,ext]=fileparts(filename);
    if isempty(ext), ext='.txt'; end
    fn=fullfile(pname,[fname ext]);
    [fid,mess]=fopen(fullfile(pname,[fname ext]),'wt');
    if fid==-1
        error('AT:FileErr','Cannot Create file %s\n%s',fn,mess);
    end
else
    fid=1;
end

if drbpms
    bpms=atgetcells(ring,'FamName','BPM.*');
    nbpms=0.5*sum(bpms);
    bpmnames=arrayfun(@(n) sprintf('BPM_%2d',n),[1:nbpms 1:nbpms],'UniformOutput',false);
    ring(bpms)=atsetfieldvalues(ring(bpms),'FamName',bpmnames');
end

if drshift
    drifts=atgetcells(ring,'Class','Drift');
    nd=sum(drifts);
    ndrifts=ceil(0.5*nd);
    driftnames=arrayfun(@(n) sprintf('DR_%2d',n),[(1:ndrifts)-drshift 1:(nd-ndrifts)],'UniformOutput',false);
    ring(drifts)=atsetfieldvalues(ring(drifts),'FamName',driftnames');
end

% Compute optics and geometry

[lindata,~,~]=atlinopt(ring,0,2:length(ring)+1);
posdata=atgeometry(ring,2:length(ring)+1,offset,options{:});

data=arrayfun(@(ld,pd) [ld.SPos pd.y pd.x -pd.angle ld.beta ld.Dispersion(1:2)'],...
    lindata',posdata,'UniformOutput',false);

% titles={'Type','Length','Angle','Radius','B','dB/dx','d2B/dx2','d3B/dx3'...
%     's','x','y','Theta','betax','betaz','D','D'''};
titles={'Type','Length','Angle','Radius','B [T]','G [T/m]','H [T/m^2]','O [T/m^3]'...
    's','x','y','Theta','betax','betaz','D','D'''};

fprintf(fid,['Name' repmat('\t%s',1,length(titles)) '\n'],titles{:});
cellfun(@(elem,data) at2survey(elem,data),ring,data);

if nargin>=2 && ~isempty(filename)
    fclose(fid);
end

    function at2survey(elem,data)
        atclass=atguessclass(elem,'UseClass');
        %BRo=20.147;  % 6.04 GeV
        BRo=20.008;	% 6.00 GeV
        switch atclass
            case 'Drift'
                mout('Drift',[],[],[],[],[],[]);
            case 'Quadrupole'
                mout('Quadrupole',[],[],[],elem.PolynomB(2)*BRo,[],[]);
            case 'Sextupole'
                %mout('Sextupole',[],[],[],[],2*elem.PolynomB(3)*BRo,[]);
                mout('Sextupole',[],[],[],[],elem.PolynomB(3)*BRo,[]);
            case 'Bend'
                ba=elem.BendingAngle;
                rd=elem.Length/ba;
                mout('Dipole',ba,rd,BRo/rd,elem.PolynomB(2)*BRo,[],[]);
            case 'Corrector'
            case 'Multipole'
                %mout('Octupole',[],[],[],[],[],6*elem.PolynomB(4)*BRo);
                mout('Octupole',[],[],[],[],[],elem.PolynomB(end)*BRo);
            case 'ThinMultipole'
            case 'RFCavity'
            case 'RingParam'
            otherwise
                mout('BPM',[],[],[],[],[],[]);
        end
        function mout(eltype,varargin)
            try
                ll=elem.Length;
            catch
                ll=0;
            end
            v=[{eltype} ...
                cellfun(@(v) num2str(v,13),[{ll} varargin],'UniformOutput',false) ...
                arrayfun(@(v) num2str(v,13),data,'UniformOutput',false)];
            fprintf(fid,['%s' repmat('\t%s',1,length(v)) '\n'],elem.FamName,v{:});
        end
    end
end

