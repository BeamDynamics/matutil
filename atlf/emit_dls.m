function modemit = emit_dls(lindata)
%
%[1] Sagan, Rubin, "Linear Analysis of Coupled Lattices"
%    Phys.Rev.Spec.Top. - Accelerators and Beams, vol2, 1999


lg=length(lindata);
f1001=zeros(lg,1);
f1010=zeros(lg,1);
beta_a=zeros(lg,1);
beta_b=zeros(lg,1);
for i=1:lg
    [beta_a(i),beta_b(i),f1001(i),f1010(i)]=beamrdt(lindata(i).A,lindata(i).B,...
        lindata(i).C,lindata(i).gamma);
end

bm44=permute(cat(3,lindata.beam44),[3 1 2]);
ex_a=bm44(:,1,1)./beta_a;
ey_a=bm44(:,3,3)./beta_b;

% compute the quantities that depend on the RDTs
P     = sqrt(abs(f1010).^2 - abs(f1001).^2);
C     = cosh(2.*P);
Sdiff = sinh(2.*P)./P.*abs(f1001);
Ssum  = sinh(2.*P)./P.*abs(f1010);
qdiff = angle(f1001);
qsum  = angle(f1010);

% extract the invarient (normal mode) emittances
Uterm = Sdiff.^2 + Ssum.^2 - 2.*Sdiff.*Ssum.*cos(qsum + qdiff);
Vterm = Sdiff.^2 + Ssum.^2 - 2.*Sdiff.*Ssum.*cos(qsum - qdiff);
modemit = [(C.^2.*ex_a - Uterm.*ey_a)./(C.^4-Uterm.*Vterm) ...
    (C.^2.*ey_a - Vterm.*ex_a)./(C.^4-Uterm.*Vterm)];

    function [beta_a,beta_b,f1001,f1010] = beamrdt(A,B,C,gamma)
        [beta_a,G_a]=gg(A);
        [beta_b,G_b]=gg(B);
        C_=G_a*C/G_b;
        f1001 = 1 / (4 * gamma) * (  C_(1,2) - C_(2,1) + 1i*C_(1,1) + 1i*C_(2,2));
        f1010 = 1 / (4 * gamma) * ( -C_(1,2) - C_(2,1) + 1i*C_(1,1) - 1i*C_(2,2));
        function [beta,s]=gg(t)
            cosmu=0.5*trace(t);
            sinmu=sign(t(1,2))*sqrt(1-cosmu*cosmu);
            alpha=0.5*(t(1,1)-t(2,2))/sinmu;
            beta=t(1,2)/sinmu;
            sqbeta=sqrt(beta);
            s=[1/sqbeta 0; alpha/sqbeta sqbeta];
        end
    end
end
