function [ring2,k,k0]=atfit(ring,constraints,vars,varargin)
%ATFIT fit ring parameters to satisfy constraints
%
% RINGFIT=ATFIT(RING,CONSTRAINTS,VARIABLES)	Fit VARIABLES to satisfy CONSTRAINTS
%
%	RING		AT ring structure
%	CONSTRAINTS	array of constraints created with ATCONSTRAINT
%	VARIABLES	array of constraints created with ATVARIABLE
%
% RINGFIT=ATFIT(RING,@EFUNC,VARIABLES)		Use a user-supplied evaluation function
%
%	EFUNC		user function called as: CHI=EFUNC(RING). This function must compute an evaluation
%			value corresponding to the input RING structure. This value will be minimised.
%
% RINGFIT=ATFIT(RING,CONSTRAINTS,@VFUNC,V0,...)	Use a user-supplied ring modification function
%
%	VFUNC		user function called as: RING2=VFUNC(RING,V,...). This function must build the
%			RING2 cell array corresponding to the test value V of variables. The function is
%			passed any additional argument of ATFIT
%	V0		initial value of variables
%
% See also ATVARIABLE, ATCONSTRAINT

if isa(vars,'function_handle')
   k0=varargin{1}(:);				% extract initial conditions
   vfunc=@(k) vars(ring,k,varargin{2:end});
else
   k0=zeros(numel(vars),1);			% prepare initial conditions
   for i=1:numel(vars)
      v=getcellstruct(ring,vars(i).field,vars(i).idx,vars(i).prm{:});
      k0(i)=v(1);
   end
   vfunc=@setvarstruct;
end

if isa(constraints,'function_handle')
   fitfunc=@(k) constraints(vfunc(k));
else
   allcodes={constraints.field};
   dpp=any(strcmp(allcodes,'chromaticities'))|any(strcmp(allcodes,'Dispersion'));
   refpts=sort(cat(2,1,constraints.idx));	% look for reference points
   repeated=(refpts == [-1 refpts(1:end-1)]);
   refpts(repeated)=[];				% eliminate redundant points
   fitfunc=@(k) feval(vfunc(k));
end

k=fminsearch(fitfunc,k0,...
	optimset(optimset('fminsearch'),'Display','iter','TolX',1.e-5))

ring2=vfunc(k);

   function c=feval(ring2)			%default evaluation function
   if dpp
      [ld,tune,chrom]=atlinopt(ring2,0,refpts);
      globdata.chromaticities=chrom;
   else
      [ld,tune]=atlinopt(ring2,0,refpts);
   end
   lindata(refpts)=ld;
   globdata.tunes=tune;
   dt2=0;
   nc=0;
   for i=1:numel(constraints)
      if isempty(constraints(i).idx)
         dv=deviate(globdata,constraints(i));
	 dt2=dt2+sum(dv.*dv);
	 nc=nc+numel(dv);
      else
	 for j=constraints(i).idx
            dv=deviate(lindata(j),constraints(i));
	    dt2=dt2+sum(dv.*dv);
	    nc=nc+numel(dv);
	 end
      end
   end
   c=dt2/nc;

   end

   function dv=deviate(data,constr)		% faster if not nested in feval
   dv=abs(getfield(data,constr.field,constr.prm)-constr.value)./constr.weight;
   end

   function r2=setvarstruct(k)			% default modification function
   r2=ring;
   for i=1:numel(vars)
      r2=setcellstruct(r2,vars(i).field,vars(i).idx,k(i),vars(i).prm{:});
   end
   end
   
end
