function el=atcleanelkeep(el,varargin)
%ATCELANELEMENT    Remove unnecessary fields from an element

%NEWELEMENT=ATCLEANELEMENT(ELEMENT)

%remove unnecessary fields, round the drift lengths, check the consistency
%of PolynomA, PolynomB, MaxOrder, set NumIntSteps to 20.
%Verbosity can be set with the GLOBVAL.verbosity global variable:
%   1 : adjusted drifts
%   2 : adjusted Polynom and NumIntSteps, removed fields

useful={'FamName','PassMethod','Length','Class'};
passmethod=el.PassMethod;

if isfield(el,'Class')
    switch el.Class
        case 'Monitor'
            el=eval(at2str(el));
            useful=[useful,{'Offset','Scale','Reading','Rotation'}];
        case 'Octupole'
            el.Class='Multipole';
        case {'Bend','Quadrupole'}
            useful=[useful,{'K'}];
        case 'Drift'
            inil=el.Length;     % Round drift lengths
            lum=1.e6*inil;
            diff=abs(lum-round(lum));
            if (diff > 0) && (diff < 0.002)
                el.Length=1.e-6*round(lum);
                dfprintf(1,'%s: %s -> %s (%g)\n',el.FamName,num2str(inil,15),num2str(el.Length,15),diff);
            end
        case 'RFCavity'
            passmethod='RFCavityPass';
        case 'RingParam'
            useful=[useful,{'Energy','Periodicity'}];
        case 'Corrector'
            useful=[useful,{'KickAngle'}];
    end
end
[req,opt]=feval(passmethod);
useful=[useful,req',opt'];

el=keepfield(el,useful{:});     % Clean unnecessary fields

    function el=keepfield(el,varargin)
        names=fieldnames(el);
        fnd=cellfun(@(fn) ~any(strcmp(fn,varargin)), names);
        if any(fnd)
            el=rmfield(el,names(fnd));
            dfprintf(2,['%s: Removed' repmat(' %s',1,sum(fnd)) '\n'],el.FamName,names{fnd});
        end
    end
    function dfprintf(level,varargin)
        global GLOBVAL
        if isfield(GLOBVAL,'verbosity') && level <= GLOBVAL.verbosity
            fprintf(varargin{:});
        end
    end
end
