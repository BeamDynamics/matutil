function c=feval(ring2,constraints,refpts)
[ld,tune]=atlinopt(ring2,0,refpts);
lindata(refpts)=ld;
globdata.tune=tune;
dt2=0;
nc=0;
for i=1:numel(constraints)
   if isempty(constraints(i).idx)
      dv=deviate(globdata,constraints(i));
      dt2=dt2+sum(dv.*dv);
      nc=nc+numel(dv);
   else
      for j=constraints(i).idx
         dv=deviate(lindata(j),constraints(i));
	 dt2=dt2+sum(dv.*dv);
	 nc=nc+numel(dv);
      end
   end
end
c=dt2/nc;

   function dv=deviate(data,constr)
   dv=abs(getfield(data,constr.field,constr.prm)-constr.value)./constr.weight;
   end

end
