function [bm,tune]=atbeam66(ring)

t=findm66(ring);
[vv,ee]=eig(t);
bm=zeros(6,6,3);
bm(:,:,1)=-2*real(vv(:,1)*vv(:,1)')/imag(det(vv(1:2,1:2))+det(vv(3:4,1:2))+det(vv(5:6,1:2)));
bm(:,:,2)=-2*real(vv(:,3)*vv(:,3)')/imag(det(vv(1:2,3:4))+det(vv(3:4,3:4))+det(vv(5:6,3:4)));
bm(:,:,3)=-2*real(vv(:,5)*vv(:,5)')/imag(det(vv(1:2,5:6))+det(vv(3:4,5:6))+det(vv(5:6,5:6)));
vp=diag(ee);
tune=angle(vp([1 3 5]))/2/pi;
