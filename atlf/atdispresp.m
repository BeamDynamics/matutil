function [respo,respd] = atdispresp(mach,plane)
%ATDISPRESP Compute the BPM dispersion response to steerers
%   [RESPO,RESPD]=ATDISPRESP(ATSTRUCT,PLANE)

step=1.e-6;
if strcmp(plane,'h')
   [idn,namen]=selcor(8);
   coord=1;
   cof=-step;
   fieldname='PolynomB';
else
   [idn,namen]=selcor(9);
   coord=3;
   cof=step;
   fieldname='PolynomA';
end
bpmidx=findcells(mach,'BetaCode','PU');
idx=allsexts(mach);
stidx=idx(idn);
[lin0,tune,xsi]=atlinopt(mach,0.0,bpmidx);
respd=NaN(length(bpmidx),length(stidx));
respo=NaN(length(bpmidx),length(stidx));
for i=1:length(stidx)
   mach{stidx(i)}.(fieldname)(1)=cof;
   [lin,tune,xsi]=atlinopt(mach,0.0,bpmidx);
   orbit=(cat(2,lin.ClosedOrbit)-cat(2,lin0.ClosedOrbit))';
   dispersion=(cat(2,lin.Dispersion)-cat(2,lin0.Dispersion))';
   respo(:,i)=orbit(:,coord)/step;
   respd(:,i)=dispersion(:,coord)/step;
   mach{stidx(i)}.(fieldname)(1)=0;
end
end

function idx=allsexts(atstruct)
thinsext=findcells(atstruct,'BetaCode','LD3');
thicksext=findcells(atstruct,'BetaCode','SX');
idx=sort([thinsext thicksext]);
end
