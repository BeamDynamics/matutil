function varargout=atgetparam(ring,varargin)
%ATGETPARAM			Extracts parameter values from AT structure
%
%[VALUES1,...]=ATGETPARAM(RING,ELEMS1,...)
%[VALUES1,...]=ATGETPARAM(RING,PARAMS1,ELEMS1,...)
%
%   ELEMS: element type ('steerh','steerv','ld3','cornq','corsq')
%          otherwise stadard BETA code ('SD','QP'...)
%          or element name
%   PARAMS: optionally indicates which information is requested
%   PARAMS is a cell array, in the form {RESNAME} or {RESNAME,INDEX}
%
%   The default PARAMS are:
%   ELEMS             PARAMS               description
%
%   'steerh'       {'KickAngle',1}       horizontal kick [rad]
%   'steerv'       {'KickAngle',2}       vertical kick [rad]
%     'qp'            {'K'}              strength [m^-2]
%    'chv'         {'KickAngle'}         [hkick vkick] [rad]
%    'ld3'         {'PolynomB',3}        strength
%   'cornq'        {'PolynomB',2}        strength
%   'corsq'        {'PolynomA',2}        strength

%   VALUES: array of extracted values

narg=1;
out=1;
nin=length(varargin);
varargout=cell(1,nargout);
while (narg <= nin) & (out <= nargout)
   if isnumeric(varargin{narg})
   elseif iscell(varargin{narg})
      parms=varargin{narg};
   else
      incode=upper(varargin{narg});
      switch incode
	 case 'STEERH'
	    idx=findcells(ring,'BetaCode','CHV');
	    parm0={'KickAngle',1};
	 case 'STEERV'
	    idx=findcells(ring,'BetaCode','CHV');
	    parm0={'KickAngle',2};
	 case 'QP'
	    idx=findcells(ring,'BetaCode',incode);
	    parm0={'K'};
	 case 'CHV'
	    idx=findcells(ring,'BetaCode',incode);
	    parm0={'KickAngle'};
	 case 'LD3'
	    idx=findcells(ring,'BetaCode',incode);
	    parm0={'PolynomB',3};
	 case 'CORNQ'
	    idx=findcells(ring,'BetaCode','LD3');
	    [idn,namen]=selcor(1);
	    idx=idx(idn);
	    parm0={'PolynomB',2};
	 case 'CORSQ'
	    idx=findcells(ring,'BetaCode','LD3');
	    [idn,namen]=selcor(2);
	    idx=idx(idn);
	    parm0={'PolynomA',2};
	 otherwise
	    idx=findcells(ring,'BetaCode',incode);
	    if isempty(idx), idx=findcells(ring,'FamName',incode); end
      end
      if ~exist('parms','var'), parms=parm0; end
      varargout{out}=getcellstruct(ring,parms{1},idx,parms{2:end});
      out=out+1;
      clear parm0 parms
   end
   narg=narg+1;
end
