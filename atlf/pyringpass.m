function pyrout = pyringpass(pyring, pyrin, nturns, varargin)
%RINGPASS tracks particles through each element of the cell array RING
% calling the element-specific tracking function specified in the
% RING{i}.PassMethod field.
%
% PYROUT=PYRINGPASS(PYRING,PYRIN,NTURNS) tracks particle(s) with initial
%    condition(s) PYRIN for NTURNS turns
%
%   PYRING      AT lattice
%   PYRIN       Nx6 numpy array: input coordinates of N particles
%   NTURNS      Number of turns to perform (default: 1)
%
%   PYROUT      (N*NTURNS)x6 numpy array: output coordinates of N particles
%               at the exit of each turn
%
%
% ROUT=PYRINGPASS(...,'reuse') with 'reuse' flag is more efficient because
%    it reuses some of the data  and functions stored in the persistent
%    memory from previous calls to RINGPASS.
%
%    !!! In order to use this option, RINGPASS or LINEPASS must first be
%    called without the reuse flag. This will create persistent data structures
%    and keep pointers to pass-method functions.
%
% ROUT=PYRINGPASS(...,'silent') does not output the particle coordinates at
%    each turn but only at the end of the tracking
%
% See also: LINEPASS

% Check input arguments

if nargin < 3
    nturns = 1;
end
[reuse,args]=getflag(varargin, 'reuse');
[silent,args]=getflag(args, 'silent'); %#ok<ASGLU>

if reuse
    pya={pyargs('reuse',uint8(1))};
else
    pya={};
end

if silent
    refpts=py.numpy.array({},pyargs('dtype',py.numpy.uint32));
else
    refpts=py.numpy.array({length(pyring)},pyargs('dtype',py.numpy.uint32));
end

if ~isa(pyring,'py.list')
    if size(pyrin,1)~=6
        error('Matrix of initial conditions, the second argument, must have 6 rows');
    end
    pyring=atwritepy(pyring);
end
if ~isa(pyrin,'py.numpy.ndarray')
    pyrin=array2numpy(pyrin);
end

pyrout = py.at.atpass(pyring,pyrin,int32(nturns),refpts,pya{:});

end
