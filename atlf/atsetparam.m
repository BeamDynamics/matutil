function ring2=atsetparam(ring,varargin)
%ATSETPARAM			Sets parameter values into an AT structure
%
%RING2=ATSETPARAM(RING,ELEMS1,VALUES1...)
%RING2=ATSETPARAM(RING,PARAMS1,ELEMS1,VALUES1,...)
%
%   ELEMS: element type ('steerh','steerv','ld3','cornq','corsq')
%          otherwise stadard BETA code ('SD','QP'...)
%          or element name
%   PARAMS: optionally indicates which information is set
%   PARAMS is a cell array, in the form {RESNAME} or {RESNAME,INDEX}
%
%   The default PARAMS are:
%   ELEMS             PARAMS               description
%
%   'steerh'       {'KickAngle',1}       horizontal kick [rad]
%   'steerv'       {'KickAngle',2}       vertical kick [rad]
%     'qp'            {'K'}              strength [m^-2]
%    'chv'         {'KickAngle'}         [hkick vkick] [rad]
%    'ld3'         {'PolynomB',3}        strength
%   'cornq'        {'PolynomB',2}        strength
%   'corsq'        {'PolynomA',2}        strength
%
%   VALUES: array of desired values

ring2=ring;
narg=1;
nin=length(varargin);
while narg <= nin
   if isnumeric(varargin{narg})
   elseif iscell(varargin{narg})
      parms=varargin{narg};
   else
      incode=upper(varargin{narg});
      switch incode
	 case 'STEERH'
	    idx=findcells(ring2,'BetaCode','CHV');
	    parm0={'KickAngle',1};
	 case 'STEERV'
	    idx=findcells(ring2,'BetaCode','CHV');
	    parm0={'KickAngle',2};
	 case 'QP'
	    idx=findcells(ring,'BetaCode',incode);
	    parm0={'K'};
	 case 'CHV'
	    idx=findcells(ring,'BetaCode',incode);
	    parm0={'KickAngle'};
	 case 'LD3'
	    idx=findcells(ring,'BetaCode',incode);
	    parm0={'PolynomB',3};
	 case 'CORNQ'
	    idx=findcells(ring,'BetaCode','LD3');
	    [idn,namen]=selcor(6);
	    idx=idx(idn);
	    parm0={'PolynomB',2};
	 case 'CORSQ'
	    idx=findcells(ring,'BetaCode','LD3');
	    [idn,namen]=selcor(7);
	    idx=idx(idn);
	    parm0={'PolynomA',2};
	 otherwise
	    idx=findcells(ring,'BetaCode',incode);
	    if isempty(idx), idx=findcells(ring,'FamName',incode); end
      end
      narg=narg+1;
      if ~exist('parms','var'), parms=parm0; end
      ring2=setcellstruct(ring2,parms{1},idx,varargin{narg},parms{2:end});
      clear parm0 parms
   end
   narg=narg+1;
end
