function elstr=at2xl(elem,varargin)
%AT2XL	Generates the xlOptics for an AT element
%
%ELSTR=AT2XL(ELEM)
%
%See also: ATWRITEXL

% if isfield(elem,'Class')
%     atclass=elem.Class;
% else
    atclass=atguessclass(elem, 'UseClass');
% end

switch atclass
    case 'Drift'
        elstr=write_elem(elem.FamName,'SS',elem.Length);
    case 'Quadrupole'
        elstr=write_elem(elem.FamName,'Quad',[elem.Length,elem.PolynomB(2:elem.MaxOrder+1)]);
    case 'Sextupole'
        elstr=write_elem(elem.FamName,'QuadSext',[elem.Length,elem.PolynomB(2:elem.MaxOrder+1)]);
    case 'Bend'
        wedge=parseargs({[true true]},varargin);
        radius=elem.Length/elem.BendingAngle;
        findex=-elem.PolynomB(2)*radius*radius;
        elstr={};
        if wedge(1)
            elstr=[elstr;write_elem([elem.FamName '_IN'],'Edge',[elem.EntranceAngle,radius])];
        end
        elstr=[elstr;write_elem(elem.FamName,'Sbend',[elem.BendingAngle,radius,findex,0])];
        if wedge(2)
            elstr=[elstr;write_elem([elem.FamName '_OUT'],'Edge',[elem.ExitAngle,radius])];
        end
    case 'Corrector'
        elstr=write_elem(elem.FamName,'Steerer',elem.KickAngle);
    case 'Multipole'
        params=[elem.PolynomB zeros(1,4)];        % at least 4 values
        elstr=write_elem(elem.FamName,'QuadSext',[elem.Length,params(2:4)]);
    case 'ThinMultipole'
        order=find(elem.PolynomB ~= 0,1,'first');
        if isempty(order) || order <= 5
            params=[elem.PolynomB zeros(1,5)];    % at least 5 values
            elstr=write_elem(elem.FamName,'ThinMag',params(2:5));
        else
            elstr= write_elem(elem.FamName,'Nmult',[order,elem.PolynomB(order)]);
        end
    case 'RFCavity'
        elstr=write_elem(elem.FamName,'Cav',[elem.Length,0.001*elem.Voltage,elem.HarmNumber]);
    case 'RingParam'
        elstr={};
    otherwise %'Marker'
        elstr=write_elem(elem.FamName,'BPM',[]);
end

    function elstr=write_elem(name,code,params)
        sparms=cellfun(@mat2str,num2cell(params),'UniformOutput',false);
        elstr={sprintf(['%s\t%s' repmat('\t%s',1,length(sparms))],...
            name,code,sparms{:})};
    end
end
