function r2=setvtest(ring,k,vars)
r2=ring;
for i=1:numel(vars)
   r2=setcellstruct(r2,vars(i).field,vars(i).idx,k(i),vars(i).prm{:});
end
end
