function [mach,machindex]=buildesrf2012a(quadvals,sextvals)
%

global GLOBVAL

if nargin < 2
    sextvals=struct(...
        'S4',3.04942,...
        'S6',-4.05506,...
        'S13',-2.2975,...
        'S19',11.32845699,...
        'S20',-10.66694506,...
        'S22',-2.49759,...
        'S24',3.5249,...
        'S4Z',6.09884 ...
        );
end
if nargin < 1
    quadvals=struct(...
        'QF2',0.391007488,...
        'QD3',-0.603553433,...
        'QD4',-0.601525562,...
        'QF5',0.734586753,...
        'QD6',-0.818812048,...
        'QF7',0.681566528 ...
        );
end

%esrf2012=atreadbeta('/machfs/laurent/orbit/esrf2012a_multibunch/betathickmodel.str');
%high=esrf2012(77+102*0+(1:51));
%low=esrf2012(26+102*0+(1:51));
%high6=esrf2012(77+102*4+(1:51));
%high6l=esrf2012(77+102*5+(1:51));
%low6=esrf2012(26+102*14+(1:51));
%mach7=atreadbeta('/machfs/laurent/dbeta/id23-minibz-thick.str');
%low7=mach7(26+102*9+(1:55));
%

cells=load('/machfs/laurent/dbeta-frag/esrfcells');
harmnumber=992;
GLOBVAL.E0=6.04e9;
cav=atrfcavity('CAV',0,8e6/3,0,harmnumber,GLOBVAL.E0,'IdentityPass');
rfcell=[cells.low(1:25);{cav};cells.low(26:end)];
mach=[...
    cells.high(26:end);...  % cell 4
    rfcell;...              % cell 5
    cells.high;...          % cell 6
    rfcell;...              % cell 7
    cells.high;...          % cell 8
    cells.low;...           % cell 9
    cells.high;...          % cell 10
    cells.low;...           % cell 11
    cells.high;...          % cell 12
    cells.low;...           % cell 13
    cells.high6;...         % cell 14
    cells.low;...           % cell 15
    cells.high6l;...        % cell 16 canting 2.7 mrad
    cells.low;...           % cell 17
    cells.high6;...         % cell 18
    cells.low;...           % cell 19
    cells.high6;...         % cell 20
    cells.low;...           % cell 21 
    cells.high;...          % cell 22
    cells.low;...           % cell 23
    cells.high6;...         % cell 24
    rfcell;...              % cell 25
    cells.high;...          % cell 26
    cells.low;...           % cell 27
    cells.high;...          % cell 28
    cells.low;...           % cell 29
    cells.high6l;...        % cell 30 canting 2.2 mrad
    cells.low;...           % cell 31 
    cells.high6;...         % cell 32
    cells.low;...           % cell 1
    cells.high;...          % cell 2
    cells.low6;...          % cell 3
    cells.high(1:25)...     % cell 4
    ];
machindex=atindex(mach);
frev=2.997924E8/findspos(mach,length(mach)+1);
% Set frequency
mach(machindex.CAV)=cellfun(@(elem) ...
    setfield(elem,'Frequency',harmnumber*frev),...
    mach(machindex.CAV),'UniformOutput',false);
% Set quadrupoles
for fn=fieldnames(quadvals)'
    fname=char(fn);
    mach(machindex.(fname))=cellfun(@(elem) ...
        setfield(setfield(elem,'K',quadvals.(fname)),'PolynomB',{2},quadvals.(fname)),...
        mach(machindex.(fname)),'UniformOutput',false);
end
% Set sextupoles
for fn=fieldnames(sextvals)'
    fname=char(fn);
    mach(machindex.(fname))=cellfun(@(elem) ...
        setfield(elem,'PolynomB',{3},sextvals.(fname)),...
        mach(machindex.(fname)),'UniformOutput',false);
end
end
