function [eps,beta,alpha] = plotbeam2d(x0,xp0)
%PLOTBEAM2D Analyse and plot a 2-D particle distribution
%
%   PLOTBEAM2D(X,XP)
%
%see also: atplotbeam
xave=mean(x0);
xpave=mean(xp0);
x=x0-xave;
xp=xp0-xpave;
betaeps=mean(x.*x);     % analyse the distribution
gammeps=mean(xp.*xp);
alpheps=-mean(x.*xp);
eps=sqrt(betaeps.*gammeps-alpheps.*alpheps);
beta=betaeps./eps;
alpha=alpheps./eps;

ang=(0:50)'/50*2*pi;    % generate the ellipse
neps=sqrt(eps);
nbeta=sqrt(beta);
nx=cos(ang)*neps;
nxp=sin(ang)*neps;
a=xave+nbeta(ones(51,1),:).*nx;
ap=xpave+(nxp-alpha(ones(51,1),:).*nx)./nbeta(ones(51,1),:);

subplot(2,1,1);
plot(x0,xp0,'.',a,ap);
subplot(2,1,2);
hist(x0,20);
end

