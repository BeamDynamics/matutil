function resp = atorm(ring,plane,stidx,bpmidx)
%ATORM Compute an orbit response matrix [m/rad]
%
% RESPONSE=ATORM(RING,PLANE,STIDX,BPMIDX)
%
%RING:  AT structure
%PLANE: plane selection, must be  h|H|x|X|1 or v|V|z|Z|2
%STIDX: steerer selection (array of indexes or logical mask)
%BPMIDX:BPM selection (array of indexes or logical mask)
%
%The PassMethod of steerers must be CorrectorPass or StrMPoleSymplectic4Pass

persistent sleh slev
if isempty(sleh)
    sleh.CorrectorPass=@hcorrector;
    sleh.StrMPoleSymplectic4Pass=@hmultipole;
    sleh.QuadMPoleFringePass=@hquadmotion;
    slev.CorrectorPass=@vcorrector;
    slev.StrMPoleSymplectic4Pass=@vmultipole;
    slev.QuadMPoleFringePass=@vquadmotion;
end

if islogical(stidx)
    stidx=find(stidx);
end
orbit=selectplane(plane,{@(st) horbit(st,0.33e-4),@(st) vorbit(st,0.25e-4)});
res=arrayfun(orbit,stidx,'UniformOutput',false);
resp=cat(2,res{:});

    function r=horbit(idst,kick)
        elsave=ring{idst};
        ring{idst}=sleh.(elsave.PassMethod)(elsave,kick);
        orb=findsyncorbit(ring,0,bpmidx);
        ring{idst}=elsave;
        r=orb(1,:)'/kick;
    end

    function r=vorbit(idst,kick)
        elsave=ring{idst};
        ring{idst}=slev.(elsave.PassMethod)(elsave,kick);
        orb=findsyncorbit(ring,0,bpmidx);
        ring{idst}=elsave;
        r=orb(3,:)'/kick;
    end

    function el=hcorrector(el,kh)
        el.KickAngle(1)=el.KickAngle(1)+kh;
    end
    function el=hmultipole(el,kh)
        ll=el.Length;
        el.PolynomB(1)=el.PolynomB(1)-kh/ll;
    end
    function el=hquadmotion(el0,kh)
        el=atshiftelem(el0,kh,0);
    end
    function el=vcorrector(el,kv)
        el.KickAngle(2)=el.KickAngle(2)+kv;
    end
    function el=vmultipole(el,kv)
        ll=el.Length;
        el.PolynomA(1)=el.PolynomA(1)+kv/ll;
    end
    function el=vquadmotion(el0,kv)
        el=atshiftelem(el0,0,kv);
    end
end
