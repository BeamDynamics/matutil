function atplotbeam(particles)
%ATPLOTBEAM Plot a 6-D particle distribution
%
%See also: PLOTBEAM2D

figure(1);
[epsx,betax,alphax]=plotbeam2d(1.e3*particles(1,:)',1.e3*particles(2,:)')
subplot(2,1,1);
title('HORIZONTAL');
xlabel('x [mm]');
ylabel('x'' [mrad]');
subplot(2,1,2);
xlabel('x [mm]');


figure(2);
[epsz,betaz,alphaz]=plotbeam2d(1.e3*particles(3,:)',1.e3*particles(4,:)')
subplot(2,1,1);
title('VERTICAL');
xlabel('z [mm]');
ylabel('z'' [mrad]');
subplot(2,1,2);
xlabel('z [mm]');

figure(3);
[eps,beta,alpha]=plotbeam2d(1.e3*particles(6,:)',particles(5,:)');
subplot(2,1,1);
title('LONGITUDINAL');
xlabel('l [mm]');
ylabel('\deltap/p');
subplot(2,1,2);
xlabel('l [mm]');

lbunch=sqrt(eps*beta)
dpp=sqrt((1+alpha*alpha)*eps/beta)
end
