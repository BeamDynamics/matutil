function st=atconstraint(idx,field,value,varargin)
%ATCONSTRAINT	build a fit constraint for ATFIT
%
%CST=ATCONSTRAINT(REFPTS,CODE,VALUE,WEIGHT)	creates a structure array for fitting
%
%	REFPTS			reference point where the constraint must be
%				applied
%	CODE			any field in the structure returned by ATLINOPT
%	VALUE			desired value
%	WEIGHT			weighting factor for the fit (default 1)
%
%CST=ATCONSTRAINT(REFPTS,CODE,VALUE,WEIGHT,M)
%CST=ATCONSTRAINT(REFPTS,CODE,VALUE,WEIGHT,M,N)	specify a single element if the field
%						value is an array
% See also ATLINOPT, ATFIT, ATVARIABLE

if nargin >= 4
   weight=varargin{1};
   prm=varargin(2:end);
else
   weight=1;
   prm={};
end
st=struct('idx',idx,'field',field,'prm',{prm},'value',value,'weight',weight);
