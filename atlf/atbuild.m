function ring=atbuild(elist)
%ATBUILD		  build AT structure
%
%RING=ATBUILD(ELIST)  stores elements from FAMLIST into a cell array
% in the order given by ineger arry ELIST

global FAMLIST GLOBVAL
ring=cell(size(elist));
for i=1:numel(ring)
   ring{i} = FAMLIST{elist(i)}.ElemData;
   ring{i}.Energy=GLOBVAL.E0
end
