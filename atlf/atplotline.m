function atplotline(line, datain)

lg=length(line)+1;
td=twissline(line, 0.0, datain, 1:lg);
s=cat(1,td.SPos);
beta=cat(1,td.beta);
plot(s,[sqrt(1.3e-7*beta(:,1)) sqrt(0.65e-7*beta(:,2))]);