function [xbpm,ybpm]=BPMReadings(x,y,Rx,Gx,Ox,Ry,Gy,Oy,R)
%function [xbpm,ybpm]=ApplyBPMErr(x,y,Rx,Gx,Ox,Ry,Gy,Oy,R)
% apply monitor reading distortions. (resolution->rotation->gain->offset)
%
% x and y are the horizontal and vertical orbit at the BPM
%
% possible usages:
%
% (1): [xbpm,ybpm]=ApplyBPMErr(x,y,Rx,Gx,Ox,Ry,Gy,Oy,R)
%      where Rxy are reading errors
%            Oxy are offsets errors
%            Gxy gain errors
%            R   rotation errors
%
% (2): [xbpm,ybpm]=ApplyBPMErr(x,y,bpms)
%      where   bpms is the call array of BPM elements from the AT lattice
%              the elements in bpmindex must have the fields:
%                                                           'Offset'
%                                                           'Reading'
%                                                           'Rotation'
%                                                           'Scale'
%
%see also: rotation

if nargin==3
    
    ring=Rx;
    
    % misalignment of BPM considered here.
    T1x=getparam(ring,0,'T1',{1,1});
    T1y=getparam(ring,0,'T1',{3,1});
    
    % Rotation error R1 of BPM considered here.
    R1=getparam(ring,0,'RotAboutS',{1,1});
    
    
    % BPM offset error.
    Ox=getparam(ring,0,'Offset',{1,1});
    Oy=getparam(ring,0,'Offset',{1,2});
    
    Gx=getparam(ring,1,'Scale',{1,1});
    Gy=getparam(ring,1,'Scale',{1,2});
    
    Rx=getparam(ring,0,'Reading',{1,1});
    Ry=getparam(ring,0,'Reading',{1,2});
    
    R =getparam(ring,0,'Rotation',{1,1});
    
end

% apply BPM errors

yn=y(:)+Ry(:).*randn(size(y(:))); % resolution
xn=x(:)+Rx(:).*randn(size(x(:)));

[xr,yr]=rotation(xn,yn,R(:)+R1(:)); % rotation

xg=xr(:).*Gx(:); % gain
yg=yr(:).*Gy(:);

xbpm=xg(:)+Ox(:)+T1x(:); % offset and misalignment errors
ybpm=yg(:)+Oy(:)+T1y(:);

xbpm=xbpm';
ybpm=ybpm';

    function [x1,y1]=rotation(x,y,a)
        % function []=rotation(x,y,a)
        %
        % rotate x y coordinates by an angle a to x1 y1
        %
        % Simone Maria Liuzzo PhD@LNF
        
        % make row vectors
        xc=iscolumn(x);
        if xc
            x=x';
        end
        yc=iscolumn(y);
        if yc
            y=y';
        end
        
        nanvec=isnan(x) | isnan(y);
        
        if size(x)==size(y)
            if length(a)==1
                % rotate everything by the same angle.
                a=a*ones(length(x),1);
            elseif length(a)~=length(x)
                disp(['size of x: ' num2str(size(x))]);
                disp(['size of y: ' num2str(size(y))]);
                disp(['size of a: ' num2str(size(a))]);
                error('a must be a number or a vector of the same length of x and y')
                return %#ok<UNRCH>
            end
            
            % rotate only non NaN
            a=a(~nanvec);
            
            Rot=[diag(cos(a)),diag(sin(a));...
                diag(-sin(a)),diag(cos(a))];
            
            v=Rot*[x(~nanvec),y(~nanvec)]';
            
            
            % return vectors of the same shape.
            if xc
                x1(~nanvec)=v(1:length(x(~nanvec)));
                x1(nanvec)=NaN;
            else
                x1(~nanvec)=v(1:length(x(~nanvec)))';
                x1(nanvec)=NaN;
            end
            
            if yc
                y1(~nanvec)=v(1+length(x(~nanvec)):end);
                y1(nanvec)=NaN;
            else
                y1(~nanvec)=v(1+length(x(~nanvec)):end)';
                y1(nanvec)=NaN;
            end
            
            
        else
            
            error('x and y size must be equal');
            
        end
        
    end

    function res=getparam(ring, defvalue, varargin)
        res=atgetfieldvalues(ring, varargin{:});
        res(isNaN(res))=defvalue;
    end

end


