function [lindata,avebeta,avemu,avedisp,nu,xsi]=atavedata(ring,dpp,refpts,varargin)
%ATAVEDATA       Average of optical functions on selected elements
%
%[LINDATA,AVEBETA,AVEMU,AVEDISP,TUNES,CHROMS]=ATAVEDATA(RING,DPP,REFPTS)
%
%LINDATA : Identical to ATLINOPT output
%AVEBEA :  Average Beta functions
%AVEMU :   Average phase advance
%AVEDISP : Average dispersion
%TUNES : Vector of tunes
%CHROMS : Vector of chromaticites

lr=length(ring)+1;
if islogical(refpts)
    refs=reshape(find(refpts),[],1);
else
    refs=refpts(:);
end
elems=refs(refs<lr);
longel=atgetcells(ring(elems),'Length',@(elem,lg) lg~=0);
[selected,~,ic]=unique([refs(:);elems+1]);
ibeg=ic(1:length(refs));
iend=ic(length(refs)+1:end);

[lind,nu,xsi]=atlinopt(ring,dpp,selected,varargin{:});

lindata=lind(ibeg);
lindend=lind(iend);
avebeta=cat(1,lindata.beta);
avemu=cat(1,lindata.mu);
disp0=cat(2,lindata.Dispersion)';
disp1=cat(2,lindend.Dispersion)';
avetapx=disp0(:,2);
avetapx(refs<lr)=0.5*(disp0(refs<lr,2)+disp1(:,2));
avedisp=[disp0(:,1) avetapx disp0(:,[1 1]).*avebeta];

if any(longel)
    long=false(size(refs));
    long(refs<lr)=longel;
    longelems=ring(elems(longel));
    lindlong=lindend(longel);
    L=atgetfieldvalues(longelems,'Length');
    L2=[L L];
    
    beta0=avebeta(long,:);
    alpha0=cat(1,lindata(long).alpha);
    etax0=disp0(long,1);
    etapx0=disp0(long,2);
    gamma0=(1+alpha0.*alpha0)./beta0;
    
    beta1=cat(1,lindlong.beta);
    alpha1=cat(1,lindlong.alpha);
    etax1=disp1(longel,1);
    etapx1=disp1(longel,2);
    
    invrho=zeros(size(L));
    bend=atgetcells(longelems,'BendingAngle');
    invrho(bend)=atgetfieldvalues(longelems(bend),'BendingAngle')./atgetfieldvalues(longelems(bend),'Length');
    
    aved=0.5*(etax0+etax1)-invrho.*L.*L/12;
    aveb=0.5*(beta0+beta1)-gamma0.*L2.*L2/6;
    
    K2=zeros(size(L2));
    quad=atgetcells(longelems,'PolynomB',@(el,polb) length(polb)>=2 && polb(2)~=0);
    kfoc=atgetfieldvalues(longelems(quad),'PolynomB',{2});
    K2(quad,:)=[kfoc -kfoc];
    %     K2(bend,1)=K2(bend,1)+invrho(bend).*invrho(bend);
    %     K2(bend,:)
    edg0= bend & atgetcells(longelems,'EntranceAngle',@(elem,ea) ea~=0);    % Correct for entrance pole face
    ea=invrho(edg0).*tan(atgetfieldvalues(longelems(edg0),'EntranceAngle'));
    alpha0(edg0,:)=alpha0(edg0,:)-[ea -ea].*beta0(edg0,:);
    etapx0(edg0)=etapx0(edg0)+etax0(edg0).*ea;
    edg1= bend & atgetcells(longelems,'ExitAngle',@(elem,ea) ea~=0);        % Correct for exit pole face
    ea=invrho(edg1).*tan(atgetfieldvalues(longelems(edg1),'ExitAngle'));
    etapx1(edg1)=etapx1(edg1)-etax1(edg1).*ea;
    alpha1(edg1,:)=alpha1(edg1,:)+[ea -ea].*beta1(edg1,:);
    adbdp0=2.*alpha0.*etax0(:,[1 1])+beta0.*etapx0(:,[1 1]);
    adbdp1=2.*alpha1.*etax1(:,[1 1])+beta1.*etapx1(:,[1 1]);
    
    foc=K2~=0;
    gkb=gamma0+K2.*beta0;
    aveb(foc)=0.5*(gkb(foc)+(alpha1(foc)-alpha0(foc))./L2(foc))./K2(foc);

    foc1=K2(:,1)~=0;
    aved(foc1)=(invrho(foc1).*L(foc1)+etapx0(foc1)-etapx1(foc1))./K2(foc1,1)./L(foc1);

    avebd=aved(:,[1 1]).*aveb-(2.*alpha0-gamma0.*L2).*etapx0(:,[1 1]).*L2.*L2./12 ...
        -(alpha0./12-2.*gamma0.*L2./45).*L2.*L2.*L2.*invrho(:,[1 1]);
    
    foc2=any(foc,2);
    avebd(foc2,:)=(2.*gkb(foc2,:).*aved(foc2,[1 1])+(adbdp1(foc2,:)-adbdp0(foc2,:))./L2(foc2,:) ...
        -invrho(foc2,[1 1]).*aveb(foc2,:))./(4.*K2(foc2,:)-K2(foc2,[1 1]));
    
    avebeta(long,:)=aveb;
    avedisp(long,1)=aved;
    avedisp(long,2)=(etax1-etax0)./L;
    avedisp(long,3:4)=avebd;
    avemu(long,:)=0.5*(avemu(long,:)+cat(1,lindlong.mu));
end

end
