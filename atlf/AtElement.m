classdef AtElement < handle
    %ELEMENT Base class for AT elements
    
    properties(Access=private)
    end
    properties
        AtStruct
        FamName
    end
    properties(Dependent)
        Length
        PassMethod
        PolynomA
        PolynomB
        K
    end
    
    methods(Static,Access=protected)
        function cc=counter(code)
            persistent c
            if ~isfield(c,code), c.(code)=0; end
            c.(code)=c.(code)+1;
            cc=sprintf('%s%d',code,c.(code));
        end
    end
    methods
        function elem=AtElement(elemstruct)
            if nargin<1, elemstruct=atdrift(AtElement.counter('DR'),0); end
            elem.FamName=elemstruct.FamName;
            elem.AtStruct=rmfield(elemstruct,'FamName');
        end
        function set.Length(elem,length)
            elem.AtStruct.Length=length;
        end
        function l=get.Length(elem)
            l=elem.AtStruct.Length;
        end
        function set.PassMethod(elem,method)
            elem.AtStruct.PassMethod=method;
        end
        function method=get.PassMethod(elem)
            method=elem.AtStruct.PassMethod;
        end
        function set.PolynomA(elem,poly)
            elem.AtStruct.PolynomA=poly;
        end
        function poly=get.PolynomA(elem)
            poly=elem.AtStruct.PolynomA;
        end
        function set.PolynomB(elem,poly)
            elem.AtStruct.PolynomB=poly;
        end
        function poly=get.PolynomB(elem)
            poly=elem.AtStruct.PolynomB;
        end
        function set.K(elem,strength)
            elem.AtStruct.PolynomB(2)=strength;
        end
        function strength=get.K(elem)
            strength=elem.AtStruct.PolynomB(2);
        end
        function disp(elem)
            disp(elem.AtStruct);
        end
    end
end
