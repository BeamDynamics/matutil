function mach=atsetcorn(mach0,index,values)
%atsetcorn  Set normal quad corrector strengths
%
%MACH=ATSETCORN(MACH,INDEX,VALUES)
%
% VALUES :  Set of quad corrector strengths [m^-1]
%
mach=setcellstruct(mach0,'PolynomB',index,values,2);