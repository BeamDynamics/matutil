function el=atcleanelremove(el, varargin)
%ATCELANELEMENT    Remove unnecessary fields from an element

%NEWELEMENT=ATCLEANELEMENT(ELEMENT)

%remove unnecessary fields, round the drift lengths, check the consistency
%of PolynomA, PolynomB, MaxOrder, set NumIntSteps to 20.
%Verbosity can be set with the GLOBVAL.verbosity global variable:
%   1 : adjusted drifts
%   2 : adjusted Polynom and NumIntSteps, removed fields

useless={'BetaCode','PolynomAMod','PolynomBMod','PolynomBErr','Tilt',...
    'iscorH','iscorV','iscorS','RotAboutS','PolynomB0','PolynomACor','PolynomBCor'...
    'RefRadius','IronLength','MagNum','MagneticLength'};

if all(isfield(el,{'Frequency','Periodicity','Energy'})==[false false true])
    useless=[useless {'Energy'}];
end
if ~any(isfield(el,{'PolynomA','PolynomB'}))
    useless=[useless {'MaxOrder','NumIntSteps'}];
end

if isfield(el,'Class')
    if strcmp(el.Class,'Monitor')
        el=eval(at2str(el));
    else
        useless=[useless {'Offset','Scale','Reading','Rotation'}];
    end
    if strcmp(el.Class,'Octupole'), el.Class='Multipole'; end
    if strcmp(el.Class,'Drift')	% Tune length (round to um if diff less than 2 nm)
        inil=el.Length;
        lum=1.e6*inil;
        diff=abs(lum-round(lum));
        if (diff > 0) && (diff < 0.002)
            el.Length=1.e-6*round(lum);
            dfprintf(1,'%s: %s -> %s (%g)\n',el.FamName,num2str(inil,15),num2str(el.Length,15),diff);
        end
    end
end

el=delfield(el,useless{:});     % Clean unnecessary fields

    function el=delfield(el,varargin)
        fnd=isfield(el,varargin);
        if any(fnd)
            el=rmfield(el,varargin(fnd));
            dfprintf(2,['%s: Removed' repmat(' %s',1,sum(fnd)) '\n'],el.FamName,varargin{fnd});
        end
    end
    function dfprintf(level,varargin)
        global GLOBVAL
        if isfield(GLOBVAL,'verbosity') && level <= GLOBVAL.verbosity
            fprintf(varargin{:});
        end
    end
end
