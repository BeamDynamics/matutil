function st=atvariable(idx,field,varargin)
%ATVARIABLE	build a fit variable for ATFIT
%
%VAR=ATVARIABLE(REFPTS,CODE)		creates a structure array for fitting
%
%	REFPTS			list of elements to be varied
%	CODE			any field of the element definition
%
%VAR=ATVARIABLE(REFPTS,CODE,M)
%VAR=ATVARIABLE(REFPTS,CODE,M,N)	specify a single element if the field
%					value is an array
% See also ATLINOPT, ATFIT, ATCONSTRAINT

st=struct('idx',idx,'field',field,'prm',{varargin});
