function ring2=atqemreadq(ring,varargin)
%ATQEMREADQ			      reads qem description of focusing errors
%
%RING2=ATQEMREADQ(RING,FILENAME)
%
%RING: AT structure
%FILENAME: qem file (default $APPHOME/qem/race_quadco.dat)

persistent initdone
if isempty(initdone)
   k0=qeminit;
   initdone=true;
end
q=-qemreadq(varargin{:});
ring2=atsetparam(ring,{'K'},'QP',q);
