function pp = atsynop(varargin)
%ATSYNOP Plots lattice elements
%
%ATSYNOP                 Plots elements from THERING in the current axes
%
%ATSYNOP(RING)           Plots elements from the lattice specified by RING
%
%ATSYNOP(AX,RING)        Plots in the axes specified by AX
%
%ATSYNOP(...,[SMIN SMAX])  Zoom on the specified range
%
%ATSYNOP(...,'OptionName',OptionValue,...) Available options:
%           'labels',REFPTS     Display the selected element names
%
%PATCHES=ATSYNOP(...) Returns handles to some objects:
%   PATCHES.Dipole      Handles to the Dipole patches
%   PATCHES.Quadrupole	Handles to the Quadrupole patches
%   PATCHES.Sextupole	Handles to the Sextupole patches
%   PATCHES.Multipole	Handles to the Multipole patches
%   PATCHES.BPM         Handles to the BPM patches
%   PATCHES.Label       Handles to the element labels

global THERING

narg=1;
% Select axes for the plot
superp=narg<=length(varargin) && isscalar(varargin{narg}) && ishandle(varargin{narg});
if superp
    ax=varargin{narg};
    narg=narg+1;
else
    cla reset
    ax=gca;
    set(gca,'YLim',[0 1],'Visible','off');
end
% Select the lattice
if narg<=length(varargin) && iscell(varargin{narg});
    [elt0,~,ring0]=get1cell(varargin{narg});
    narg=narg+1;
else
    [elt0,~,ring0]=get1cell(THERING);
end
s0=findspos(ring0,1:elt0+1);
srange=s0([1 end]);     %default plot range

firstarg=nargin+1;
for iarg=narg:nargin
    % explicit plot range
    if isnumeric(varargin{iarg}) && (numel(varargin{iarg})==2)
        srange=varargin{iarg};
    else
        firstarg=iarg;
        break
    end
end
if ~superp
    set(ax,'XLim',srange);
end
pp=atplotsyn(ax,ring0,varargin{firstarg:nargin});  % Plot lattice elements

    function [cellsize,np,cell]=get1cell(ring)
        [cellsize,np]=size(ring);
        cell=ring(:,1);
        params=atgetcells(cell,'Class','RingParam');
        if any(params)
            np=ring{find(params,1)}.Periodicity;
        end
    end

end

