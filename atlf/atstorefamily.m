function z = atstorefamily(ElemData)
%ATSTOREFAMILY			Stores element if the FAMLIST global array
%
%Z=ATSTOREFAMILY(ELEMENT) returns the index in FAMLIST
%
% See also: ATBUILD

global FAMLIST
z = length(FAMLIST)+1; % number of declared families including this one
FAMLIST{z}.FamName = ElemData.FamName;
FAMLIST{z}.NumKids = 0;
FAMLIST{z}.KidsList= [];
FAMLIST{z}.ElemData= ElemData;

