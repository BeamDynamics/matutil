function testcall2

[v1,v2,v3,v4,v5,v6,v7,v8,v9,v10]=deal(1:10);
f1=@(k) fc(k,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10);

t=cputime;for i=1:1000, f1(i); end; cputime-t

function fc(k,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)
c=[a1,a2,a3,a4,a5,a6,a7,a8,a9,a10];
