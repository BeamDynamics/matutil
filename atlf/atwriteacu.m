function atwriteacu(ring,filename)
%ATWRITEACU Creates a text file used by CTRM applications
%
%ATWRITEACU(RING)
%   Prints the result in the command window
%ATWRITEACU(RING,FILENAME)
%   Prints the result in a file

if nargin>=2
    [pname,fname,ext]=fileparts(filename);
    if isempty(ext), ext='.txt'; end
    fn=fullfile(pname,[fname ext]);
    [fid,mess]=fopen(fullfile(pname,[fname ext]),'wt');
    if fid==-1
        error('AT:FileErr','Cannot Create file %s\n%s',fn,mess);
    end
else
    fid=1;
end

s0=num2cell(findspos(ring,1:length(ring)));
qp=atgetcells(ring,'Class','Quadrupole');
dp=atgetcells(ring,'Class','Bend');
fringefield=atgetcells(ring,'FamName','ml.*');
dp=dp & ~fringefield;
sx=atgetcells(ring,'Class','Sextupole');
bpm=atgetcells(ring,'Class','Monitor');

fprintf(fid,'\\\\ Quadrupoles\n');
cellfun(@showqp,ring(qp),s0(qp)');
fprintf(fid,'\\\\ Dipoles\n');
cellfun(@showdp,ring(dp),s0(dp)');
fprintf(fid,'\\\\ Sextupoles\n');
cellfun(@showsx,ring(sx),s0(sx)');
fprintf(fid,'\\\\ BPMs\n');
cellfun(@showbpm,ring(bpm),s0(bpm)',num2cell(0:sum(bpm)-1)');

if nargin>=2
    fclose(fid);
end

    function showqp(elem,s)
        x=s+elem.Length*[0 0 0.5 1 1];
        y=[0 1 1 1 0];
        y(3)=y(3)+0.4*sign(elem.PolynomB(2));
        show(elem.FamName,[x;y]);
    end

    function showdp(elem,s)
        x=s+elem.Length*[0 0 1 1];
        y=[0 1 1 0];
        show(elem.FamName,[x;y]);
    end

    function showsx(elem,s)
        x=s+elem.Length*[0.5 0.5];
        y=[0 1];
        show(elem.FamName,[x;y]);
    end

    function showbpm(elem,s,i)
        y=s+elem.Length;
        fprintf(fid,'%s { %g,%g }\n',elem.FamName,[i;y]);
    end

    function show(name,xy)
        fprintf(fid,'%s { ',name);
        fprintf(fid,'%g,%g ; ',xy(:,1:end-1));
        fprintf(fid,'%g,%g }\n',xy(:,end));
    end
end
