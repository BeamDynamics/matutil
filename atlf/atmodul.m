function [mh,mv]=atmodul(ring,nper,lindata,lindataref)
%ATMODUL			displays modulation of optical functions
%
%ATMODUL(RING,PERIODS,LINDATA)
% The modulation is computed with respect to the average values over the
% different cells.
%
%ATMODUL(RING,PERIODS,LINDATA,LINDATAREF) uses the values in LINDATAREF
% as reference values.
%
%[MH,MV]=ATMODUL(...)	returns the modulation of beta functions
%                       at BPM locations
%See also: ATREADBETA, ATX, ATLINOPT

lt=length(ring);
orbit=atvalue(lindata,'ClosedOrbit');
dispersion=atvalue(lindata,'Dispersion');
beta=atvalue(lindata,'beta');
if nargin < 4
   betaref=[vref(beta(:,1),nper) vref(beta(:,2),nper)];
else
   betaref=atvalue(lindataref,'beta');
end
modul=(beta-betaref)./betaref;

disp('Start of superperiod:');
id0=1:lt/nper:lt;
rms_co=rms(orbit(id0,:))
aver_disp=mean(dispersion(id0,:))
rms_disp=std(dispersion(id0,:),1)
rmsmodul=std(modul(id0,:),1)
betamodul=max(abs(modul(id0,:)))

disp('On BPMs:');
idbpm=findcells(ring,'BetaCode','PU');
rms_co=rms(orbit(idbpm,:))
rmsmodul=std(modul(idbpm,:),1)
betamodul=max(abs(modul(idbpm,:)))

if nargout == 2
   mh=modul(idbpm,1);
   mv=modul(idbpm,2);
end


function v=rms(v0)
v=sqrt(sum(v0.*v0));

function v=vref(v0,nper)
v=repmat(mean(reshape(v0,[],nper),2),nper,1);
