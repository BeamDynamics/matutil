function [lindata,varargout]=atbeamsr(varargin)

[lindata,varargout{1:nargout-1}]=atlinopt(varargin{:});
%CP=[lindata.C(2,2) -lindata.C(1,2);-lindata.C(2,1) lindata.C(1,1)];
%v=[g lindata.C; -CP g];
%vinv=[g -lindata.C;CP g];
S2=[0 1;-1 0];
NPTS=length(lindata);
for i=1:NPTS
   g=[lindata(i).gamma 0;0 lindata(i).gamma];
   v=[g lindata(i).C;-S2*(lindata(i).C)'*S2' g];
   %vinv=[g -lindata.C;S2*(lindata.C)'*S2' g];
   lindata(i).BeamA=v*[beammatr(lindata(i).A) zeros(2,2);zeros(2,4)]*v';
   lindata(i).BeamB=v*[zeros(2,4);zeros(2,2) beammatr(lindata(i).B)]*v';
end

function bim=beammatr(tr)
sinmu=sign(tr(1,2))*sqrt(-tr(1,2)*tr(2,1)-(tr(1,1)-tr(2,2))^2/4);
alfa=(tr(1,1)-tr(2,2))/2/sinmu;
beta=tr(1,2)/sinmu;
bim=[beta -alfa;-alfa (alfa*alfa+1)/beta];
