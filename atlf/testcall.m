function testcall

[v1,v2,v3,v4,v5,v6,v7,v8,v9,v10]=deal(1:10);
f1=@(k) fa(k,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10);
f2=@fb;

t=cputime;for i=1:1000, f1(i); end; cputime-t
t=cputime;for i=1:1000, f2(i); end; cputime-t
t=cputime;for i=1:1000, fb(i); end; cputime-t

   function fa(k,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)
   c=[a1,a2,a3,a4,a5,a6,a7,a8,a9,a10];
   end
   
   function fb(k)		%much faster
   c=[v1,v2,v3,v4,v5,v6,v7,v8,v9,v10];
   end

end
