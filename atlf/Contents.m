% LF's Accelerator Toolbox additions
% Version ESRF Sep-2006
%
% Files
%   atbuild       - build AT structure
%   atgetparam    - Extracts parameter values from AT structure
%   atlinopt      - performs linear analysis of the COUPLED lattices
%   atmodul       - displays modulation of optical functions
%   atqemreadq    - reads qem description of focusing errors
%   atradon       - switches RF and radiation on
%   atreadbeta    - reads a BETA file
%   atsemreadq    - reads sem description of coupling errors
%   atsetparam    - Sets parameter values into an AT structure
%   atstorefamily - Stores element if the FAMLIST global array
%   atvalue       - extract array from lindata structure
%   atx           - computes and displays global information
%   buildesrf     - build ESRF lattice
%   buildesrfdip  - build modified ESRF lattice
%   buildesrferr  - build ESRF lattice with errors
%   atconstraint  - build a fit constraint for ATFIT
%   atfit         - fit ring parameters to satisfy constraints
%   atnuampl      - computes tune shift with amplitude
%   atnudp        - computes tune shift with momentum
%   atvariable    - build a fit variable for ATFIT
