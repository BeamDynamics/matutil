function ring=atseterrors(ring,dx,dz,dll,rot)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

quads=atgetcells(ring,'Class','Quadrupole');
nquads=sum(quads);
vdx=num2cell(dx*randn(nquads,1));
vdz=num2cell(dz*randn(nquads,1));
vdll=num2cell(dll*randn(nquads,1));
vrot=num2cell(rot*randn(nquads,1));
ring(quads)=cellfun(@setquad,ring(quads),vdx,vdz,vdll,vrot,'UniformOutput',false);

    function elem=setquad(elem,dx,dz,dll,rot)
        elem=atshiftelem(elem,dx,dz);
        elem=attiltelem(elem,rot);
        newk=(1+dll)*elem.PolynomB(2);
        elem.K=newk;
        elem.PolynomB(2)=newk;
    end
end

