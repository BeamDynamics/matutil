function [ring,nper,idsym]=buildesrf(cavipass,bendpass,quadpass)
%BUILDESRF				build ESRF lattice
%[ring,periods,idsym]=BUILDESRF(cavipass,bendpass,quadpass)
%
%CAVIPASS:	pass method for cavities (default IdentityPass)
%BENDPASS:	pass method for cavities (default BendLinearPass)
%QUADPASS:	pass method for cavities (default QuadLinearPass)
%
%PERIODS: number of periods
%IDSYM: index of periodical elements

global FAMLIST GLOBVAL

GLOBVAL.E0 = 6.04e9;	% Design Energy [eV]

if nargin < 3, quadpass='QuadLinearPass'; end
if nargin < 2, bendpass='BendLinearPass'; end
if nargin < 1, cavipass='IdentityPass'; end

circ=844.39;		% m
clight=299792458;	% m/s
cav=rfcavity('CAV1', 0, 3.0e6, clight/circ*992, 992, cavipass);
FAMLIST{cav}.ElemData.BetaCode='CA';

dr03=drift('DR03',.383000E+00,'DriftPass');
dr23=drift('23',.954750E+00,'DriftPass');

qd1=quadrupole('QD1',.438050E+00,-.114872E+00,quadpass);                                   
qf2=quadrupole('QF2',.943410E+00,.450488E+00,quadpass);                                          
qd3=quadrupole('QD3',.533670E+00,-.617498E+00,quadpass);                                         
qd4=quadrupole('QD4',.422110E+00,-.660510E+00,quadpass);
qf5=quadrupole('QF5',.519620E+00,.749561E+00,quadpass);
qd6=quadrupole('QD6',.519010E+00,-.758908E+00,quadpass);             
qf7=quadrupole('QF7',.924320E+00,.776046E+00,quadpass);
qd8=quadrupole('QD8',.434210E+00,-.412518E+00,quadpass);
for qp=[qd1 qf2 qd3 qd4 qf5 qd6 qf7 qd8], FAMLIST{qp}.ElemData.BetaCode='QP'; end

s4 =thinmagnet('S4', [0 0  1.03265],'ThinMPolePass'); %.103265E+07  
s6 =thinmagnet('S6', [0 0 -1.16663],'ThinMPolePass'); %-.116663E+07  
s13=thinmagnet('S13',[0 0 -2.50991],'ThinMPolePass'); %-.250991E+07  
s19=thinmagnet('S19',[0 0  4.25267],'ThinMPolePass'); %.425267E+07  
s22=thinmagnet('S22',[0 0 -1.39990],'ThinMPolePass'); %-.139990E+07
s24=thinmagnet('S24',[0 0  2.20851],'ThinMPolePass'); %.220851E+07  
for sx=[s4 s6 s13 s19 s22 s24], FAMLIST{sx}.ElemData.BetaCode='LD3'; end

softangle=0.00585;
hardangle=pi/32-softangle;
faceangle=pi/64;
rhard=23.3663;
rsoft=50.036;
hard1=rbend('BH1',rhard*hardangle,hardangle,...
	faceangle,hardangle-faceangle,0,bendpass);
soft1=rbend('BS1',rsoft*softangle,softangle,...
	faceangle-hardangle,faceangle,0,bendpass);
soft2=rbend('BS2',rsoft*softangle,softangle,...
	faceangle,faceangle-hardangle,0,bendpass);
hard2=rbend('BH2',rhard*hardangle,hardangle,...
	hardangle-faceangle,faceangle,0,bendpass);

dipole1=[hard1 soft1];
dipole2=[soft2 hard2];
for di=[hard1 soft1 soft2 hard2], FAMLIST{di}.ElemData.BetaCode='DI'; end

%   35 CA   .000000E+00   .100000E+01   .604000E+10  

bpm=marker('PICK','IdentityPass');
FAMLIST{bpm}.ElemData.BetaCode='PU';

CHV=corrector('CHV',0.0,[0.0 0.0],'IdentityPass');
FAMLIST{CHV}.ElemData.BetaCode='CHV';

dr45=drift('DR45',.305260E+01,'DriftPass');
dr47=drift('DR47',.283000E+00,'DriftPass');
SD1U=drift('SD1U',.979750E-01,'DriftPass');                               
SD1D=drift('SD1D',.330975E+00,'DriftPass');                                                        
SD2U=drift('SD2U',.328295E+00,'DriftPass');                                                       
SD2D=drift('SD2D',.952950E-01,'DriftPass');                                                       
SD3U=drift('SD3U',.333165E+00,'DriftPass');                                               
SD3D=drift('SD3D',.105542E+01,'DriftPass');                                                       
SD4U=drift('SD4U',.105945E+00,'DriftPass');
SD4D=drift('SD4D',.338945E+00,'DriftPass');                                                     
SD5U=drift('SD5U',.420190E+00,'DriftPass');                                                     
SD5D=drift('SD5D',.107190E+00,'DriftPass');                                                   
S5PU=drift('S5PU',.490190E+00,'DriftPass');
S5PD=drift('S5PD',.420190E+00,'DriftPass');                                                 
SD6U=drift('SD6U',.106275E+01,'DriftPass');                                                   
SD6D=drift('SD6D',.340495E+00,'DriftPass');                                                
SD7U=drift('SD7U',.104840E+00,'DriftPass');                                                  
SD7D=drift('SD7D',.337840E+00,'DriftPass');
SD8U=drift('SD8U',.332895E+00,'DriftPass');                                                     
SD8D=drift('SD8D',.998950E-01,'DriftPass');

highb=[dr45 bpm SD1U qd1 SD1D s4 SD2U qf2 SD2D ...
	bpm dr47 CHV s6 SD3U qd3 SD3D];

lowb=[dr45 bpm SD8D qd8 SD8U s24 SD7D qf7 SD7U ...
	bpm dr47 CHV s22 SD6D qd6 SD6U];
	
achro1=[dipole1 dr23 bpm SD4U qd4  SD4D s13 ... 
 SD5U qf5  SD5D bpm  dr03 CHV s19  S5PU qf5  S5PD ...
 s13 SD4D qd4  SD4U bpm  dr23 dipole2];
	
achro2=[dipole1 dr23 bpm SD4U qd4  SD4D s13 ... 
 SD5U qf5  SD5D bpm dr03 CHV  s19  S5PU qf5  S5PD ...
 s13 SD4D qd4  SD4U bpm dr23 dipole2];
 
superp=[highb achro1 reverse(lowb) lowb achro2 reverse(highb)];

ring=atbuild([...
highb achro1 reverse(lowb) cav lowb achro2 reverse(highb) ... % cells 4 5
highb achro1 reverse(lowb) cav lowb achro2 reverse(highb) ... % cells 6 7
superp superp superp superp superp superp superp superp ... & cells 8-23
highb achro1 reverse(lowb) cav lowb achro2 reverse(highb) ... % cells 24 25
superp superp superp superp superp			... & cells 26-3
]);


cavities=findcells(ring,'Frequency');   % adjust RF frequency to perimeter
frev=clight/findspos(ring,length(ring)+1);
for i=cavities, ring{i}.Frequency=frev*ring{i}.HarmNumber; end

evalin('base','global GLOBVAL FAMLIST');

if nargout > 0
   nper=16;
   idsym=1:length(ring);
   idsym(cavities)=[];
end
