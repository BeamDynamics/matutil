function z = thinmagnet(fname,strength, method)
%THINMAGNET(FAMILYNAME,STRENGTH,METHOD)
%	creates a new family in the FAMLIST - a structure with fields
%		FamName			family name
%		Length 			is set to 0 for thinmagnet type 
%		KickAngle       [kickx, kicky] in radians (small) - unis of d(x,y)/ds
%       PassMethod		name of the function on disk to use for tracking
%
% returns assigned index in the FAMLIST that is uniquely identifies
% the family

if nargin < 3
   method='ThinMPolePass';
end
polb=strength(1,:);
if size(strength,1) >= 2
   pola=strength(2,:);
else
   pola=zeros(size(polb));
end;

ElemData.FamName = fname;  % add check for identical family names
ElemData.Length = 0;
ElemData.MaxOrder = length(polb)-1;
ElemData.NumIntSteps = 10;
ElemData.R1 = diag(ones(6,1));
ElemData.R2 = diag(ones(6,1));
ElemData.T1 = zeros(1,6);
ElemData.T2 = zeros(1,6);
ElemData.PolynomA= pola;	 
ElemData.PolynomB= polb; 
ElemData.PassMethod=method;
z=atstorefamily(ElemData);
