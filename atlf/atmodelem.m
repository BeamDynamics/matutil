function newelem = atmodelem(oldelem,newa,newb)
%ATMODELEM Modifies A and B multipole expansions
%NEWELEM=ATMODELEM(OLDELEM,NEWA,NEWB)
%
%NEWA: New A polynomial
%NEWB: New B poynomial
%
%See also: atshiftelem, attiltelem

newelem=oldelem;
oldl=oldelem.MaxOrder+1;
newl=max([oldl length(newa) length(newb)]);
newelem.PolynomA=newpol(oldelem.PolynomA(1:oldl),newa,newl);
newelem.PolynomB=newpol(oldelem.PolynomB(1:oldl),newb,newl);
newelem.MaxOrder=newl-1;
end

function np=newpol(op,modp,newl)
ol=length(op);
np=zeros(1,newl);
np(1:ol)=op;
if ~isempty(modp)
   nl=length(modp);
   np(1:nl)=np(1:nl)+modp;
end
end
