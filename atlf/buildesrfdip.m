function [ring,nper,idsym]=buildesrfdip(cavipass,bendpass,quadpass)
%BUILDESRFDIP				build modified ESRF lattice
%[ring,periods,idsym]=BUILDESRFDIP(cavipass,bendpass,quadpass)
%                     Adds a maker (DIPC) in the middle of the dipole hard region
%
%CAVIPASS:	pass method for cavities (default IdentityPass)
%BENDPASS:	pass method for cavities (default BendLinearPass)
%QUADPASS:	pass method for cavities (default QuadLinearPass)

global FAMLIST

if nargin < 3, quadpass='QuadLinearPass'; end
if nargin < 2, bendpass='BendLinearPass'; end
if nargin < 1, cavipass='IdentityPass'; end

[ring0,nper0,idsym0]=buildesrf(cavipass,bendpass,quadpass);

softangle=0.00585;				% modified elements
hardangle=pi/32-softangle;
faceangle=pi/64;
rhard=23.3663;
dicenter=marker('DIPC','IdentityPass');
hard1a=rbend('BH1',0.5*rhard*hardangle,0.5*hardangle,...
	faceangle,0,0,bendpass);
hard1b=rbend('BH1',0.5*rhard*hardangle,0.5*hardangle,...
	0,hardangle-faceangle,0,bendpass);
hard2b=rbend('BH2',0.5*rhard*hardangle,0.5*hardangle,...
	hardangle-faceangle,0,0,bendpass);
hard2a=rbend('BH2',0.5*rhard*hardangle,0.5*hardangle,...
	0,faceangle,0,bendpass);
for di=[hard1a hard1b hard2b hard2a], FAMLIST{di}.ElemData.BetaCode='DI'; end

dipole1=atbuild([hard1a dicenter hard1b]);	% build 2 dipoles
dipole2=atbuild([hard2b dicenter hard2a]);

dips1=findcells(ring0,'FamName','BH1');		% locate existing dipoles
dips2=findcells(ring0,'FamName','BH2');
dips=[dips1+1;dips2-1;dips2+1;dips1(2:end)-1 numel(ring0)];

ring={ring0{1:dips1(1)-1}};			% substitute new dipoles
for i=1:size(dips,2)
ring={ring{:} dipole1{:} ring0{dips(1,i):dips(2,i)} ...
              dipole2{:} ring0{dips(3,i):dips(4,i)}};
end

if nargout > 0
   nper=nper0;
   idsym=1:length(ring);
   idsym(findcells(ring,'Frequency'))=[];
end
