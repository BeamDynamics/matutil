function [up,dn,angle] = atintersect(vline)
%ATINTERSECT Compute the intersection of input and output directions
%
%[SUP,SDOWN]=ATINTERSECT(LINE)
%   computes the intersection between upstream and downstream directions
%   of a line segment, and gives the lengths of the corresponding straight
%   sections
%
pos=atgeometry(vline);
angle=pos(end).angle;
dn=pos(end).y/sin(angle);
up=pos(end).x-pos(end).y/tan(angle);
end

