function ring2=atsemreadq(ring,varargin)
%ATSEMREADQ			      reads sem description of coupling errors
%
%RING2=ATSEMREADQ(RING,FILENAME)
%
%RING: AT structure
%FILENAME: sem file (default $APPHOME/sem/race_skewco.dat)

persistent initdone
if isempty(initdone)
   kl0=seminit;
   initdone=true;
end
s=-semreadq(varargin{:});
idq=findcells(ring,'BetaCode','QP');
k=getcellstruct(ring,'K',idq);
l=getcellstruct(ring,'Length',idq);
rot=0.5*asin(s./k./l);
ring2=ring;
for i = 1:length(idq)
   rm=mkSRotationMatrix(rot(i));
   ring2{idq(i)}.R1=rm;
   ring2{idq(i)}.R2=rm';
end
