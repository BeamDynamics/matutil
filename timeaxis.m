function timeaxis(ax,t1,t2)

tmima=get(ax,'Xlim');
if nargin >= 3, tmima(2)=datenum(t2);  end
if nargin >= 2, tmima(1)=datenum(t1);  end
nbdays=tmima(2)-tmima(1);
tmin=datevec(tmima(1));
disp(datestr(tmin,0))
if nbdays<0.5		% use hours
elseif nbdays<2		% use 4 hours
   tmin(4)=mod(tmin(4),4)+4;
   vals=(datenum(tmin):1/6:tmima(2))';
   labs=cellstr(datestr(vals,'dd/mm HH:MM'))
elseif nbdays<7		% use days
   vals=(floor(tmima(1)-0.001)+1:tmima(2))';
   labs=cellstr(datestr(vals,'dd/mm/yy'));
elseif nbdays<60	% use weeks
   offset=weekoffset(tmima(1));
   tmin(3)=tmin(3)+offset;
   tmin(4:6)=0;
   vals=(datenum(tmin):7:tmima(2))';
   labs=cellstr(datestr(vals,'dd/mm/yy'));
else			% use months
end
set(ax,'XLim',tmima,'XTick',vals,'XTickLabel',labs,'Color','none');
xlabel(['time (starting ' datestr(tmima(1),0) ')']);
grid on

function off=weekoffset(tim)
switch datestr(tim,'ddd')
case 'Mon', off=7;
case 'Tue', off=6;
case 'Wed', off=5;
case 'Thu', off=4;
case 'Fri', off=3;
case 'Sat', off=2;
case 'Sun', off=1;
end
