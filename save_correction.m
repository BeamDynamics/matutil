function save_correction(filename,code,strength,varargin)
%SAVE_CORRECTION(FILENAME,CODE,CURRENTS)
%
%FILENAME:	text file containing corrector currents
%CODE	1: 16 quad correctors
%	2: 16 skew quad correctors
%	3: 12 sextupole correctors
%	4:  8 horizontal correctors
%	5:  8 vertical correctors
%	6: 32 quad correctors      106: without qp-n4/c4
%	7: 32 skew correctors
%	8: all H steerers
%	9: all V steerers
%      10: 64 skew correctors      110: with qp-s4/c4
%      11: 12 sextupole corrector + 8 indep. sextupoles
%
%CURRENTS:	corrector currents
%
%SAVE_CORRECTION(FILENAME,CODE,STRENGTHS,OPTICS)
%
%STRENGTHS:	corrector strengths
%OPTICS   : may be one of the following:
%
%- 'sr'
%           machine name, path defaults to $(APPHOME)/mach/optics/settings/theory
%- ['sr',]'/machfs/appdata/sr/optics/settings/opticsname':
%           full path of optics directory
%- ['sr',]'opticsname'
%           machine and optics name

[list,devlist]=selcor(code); %#ok<ASGLU>
if ~isempty(varargin)
    strength=strength./reshape(load_corcalib(varargin{:},code),size(strength));
end

[fp,message]=fopen(filename,'wt');
if fp < 0
    error('File:Open','%s: %s',message,filename);
else
    for dev=1:length(devlist)
        fprintf(fp,'%s\t%.6f\n',devlist{dev},strength(dev));
    end
    fclose(fp);
end
