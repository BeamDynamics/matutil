function [resp,current]=load_srresp(filename,varargin)
%LOAD_SRRESP    get measured response matrix
%
%deprecated: see SR.LOAD_RESP, sr_LOAD_NORMRESP
%RESP=LOAD_SRRESP(DIRNAME,PLANE,STEERLIST)
%
%DIRNAME:   Directory containing measured data or
%           file containing a full response matrix
%STEERLIST: list of steerers (1..96)
%PLANE:     'H', 'V', 'H2V', 'V2H'
%
%RESP:      normalized BPM response [m]
%
%       The steerer calibration is taken from the default directory
%       $(APPHOME)/sr/optics/settings/theory
%
%RESP=LOAD_SRRESP(DIRNAME,OPTICS,PLANE,STEERLIST)
%		takes the steerer calibration from the specified optics dir.
%
%OPTICS   : may be one of the following:
%
%- '/machfs/appdata/sr/optics/settings/opticsname'[,'sr']:
%			full path of optics directory, machine
%- 'sr' or 'sy':	machine name
%			path defaults to $(APPHOME)/mach/optics/settings/theory
%- 'opticsname'[,'sr']:	optics name, machine
%
%RESP=LOAD_SRRESP(DIRNAME,PLANE,'m/A',...)
%           returns the response in m/A, without using the steerer
%           calibration
%
%[RESP,CURRENT]=LOAD_SRRESP(DIRNAME,PLANE,STEERLIST)
%           returns the non-normalized response and the corresponding
%           excitations
%RESP:    BPM response [m]
%CURRENT: list of steerer currents [A]
%
%   See also SR.LOAD_RESP,SR.LOAD_NORMRESP,LOAD_RSP2.

if nargout < 2
    resp=sr.load_normresp(filename,varargin{:});
else
    [resp,current]=sr.load_resp(filename,varargin{1:2});
end
end
