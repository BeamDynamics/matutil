function savetext(fname,format,values,header)
%SAVETEXT(FILENAME,FORMAT,VALUES,HEADER) saves matrix in user defined format
%
%	FORMAT should describe one row of the matrix
%	HEADER (optional) is a string printed before the values

fid=fopen(fname,'w');
if (nargin >= 4),if isstr(header)
   for s=header', fprintf(fid,'%s\n',s); end
end,end
fprintf(fid,format,values');
fclose(fid);
