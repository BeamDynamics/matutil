function show_matfile(filename)
%SHOW_MATFILE   Display the contents of a .mat file
%
%SHOW_MATFILE(FILENAME)
%
if nargin < 1, filename='matlog'; end
v=load(filename);
fields=fieldnames(v);
for i=1:length(fields)
    fname=fields{i};
    hidden=strfind(fname,'x_');
    if isempty(hidden) || hidden(1)~= 1
        if ischar(v.(fname))
            fprintf('%20s: %s\n',fname,v.(fname));
        else
            fprintf('%20s:\n',fname);
            disp(v.(fname));
            fprintf('\n');
        end
    end
end
