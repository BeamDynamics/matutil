function orbits=rbump_bump(resph,respv,bpm,step)

orbits=step(ones(224,1),:).*[resph(:,bpm.h.bump)*bpm.h.kicks respv(:,bpm.v.bump)*bpm.v.kicks];
