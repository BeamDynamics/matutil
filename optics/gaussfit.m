function [sigma,xmax,ymax]=gaussfit(x,y)
% compute the rms value = FWHM/2.71828
% returns the rms value, the x value of the ymax, and the ymax


sigma=0.0
npos=length(x)
if length(y) == npos
	ymax=max(y)
	i=1
	while y(i) < ymax
		i=i+1
	end
	xmax=x(i)
	
	ymax2=max(y)/2
	i=1
	while y(i) < ymax2 
		i=i+1
	end
	p1=i
	x1=x(i-1)+(ymax2-y(i-1))*(x(i)-x(i-1))/(y(i)-y(i-1))

	i=npos
	while y(i) < ymax2 
		i=i-1
	end
	p2=i
	x2=x(i)+(ymax2-y(i+1))*(x(i)-x(i+1))/(y(i)-y(i+1))

	sigma=(x2-x1)/2.71828
end
end
