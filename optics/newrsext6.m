function [response,correction]=newrsext6(sext,nux,nuz)

h1=floor(3*nux);
h3=floor(nux+2*nuz);
h5=floor(nux-2*nuz);
% p=(3*nux-h1)/3;
% n=(3*nux-h2)/3;
% m=(nux+2*nuz-h3)/3;
%						response (6+1)x12
a=[...
    0.002*r3nux(sext,h1,nux);...            3nux=109
    0.002*r3nux(sext,h1-1,nux);...          3nux=108
    0.002*rnuxp2nuz(sext,h3,nux,nuz);...    nux+2nuz=63
%     0.002*rnuxp2nuz(sext,h3+1,nux,nuz);...    nux+2nuz=64
%     0.002*rnuxm2nuz(sext,h5,nux,nuz);...        nux-2nuz=9
    0.002*rnuxm2nuz(sext,h5+1,nux,nuz);...    nux-2nuz=10
    ones(1,size(sext,1)) ...
    ];

calr=ones(1,size(sext,1));
calr(9)=0.551;      % short sextu S24/C23
calc=1./calr';

%a=a.*calr(ones(size(a,1),1),:);

figure(4);
resonplot(a(1:6,:),[h1 h1+1 h3]);
[u,s,v]=svd(a,0);		% take pseudo-inverse
lambda=diag(s) %#ok<NOPRT>
keep=1:length(lambda);
ainv=v(:,keep)*diag(1./lambda(keep))*u(:,keep)';
correction=ainv(:,1:8)'.*calc(:,ones(1,8))';	% transpose for C order

i_rms=std(correction') %#ok<UDIM,NOPRT,NASGU>
response=a(1:8,:)'.*calr(ones(8,1),:)';		% transpose for C order
info=ver('matlab');
matvers=sscanf(info.Version,'%d.%d');
if matvers(1) >= 5, fmode=' -v4'; else fmode='';end
eval(['save sextcor response correction' fmode]);
store_variable('matlog','sext_reson_mode','free');

    function exc=r3nux(sext,p,nuxx)
        disp(['3*nuX = ' num2str(p)]);
        n=(3*nuxx-p)/3;
        nx=nuxx-n;
        phase=2*pi*(3*nx*sext(:,3)+(p-3*nx)*sext(:,1));
%         phase=2*pi*(3*nuxx*sext(:,3)-p*sext(:,1));
        ampl=sext(:,2).^1.5;
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

    function exc=rnuxp2nuz(sext,p,nuxx,nuzz)
        disp(['nuX + 2 nuZ = ' num2str(p)]);
        m=(nuxx+2*nuzz-p)/3;
        nx=nuxx-m;
        nz=nuzz-m;
        phase=2*pi*(nx*sext(:,3)+2*nz*sext(:,6)+(p-(nx+2*nz))*sext(:,1));
%         phase=2*pi*(nuxx*sext(:,3)+2*nuzz*sext(:,6)-p*sext(:,1));
        ampl=sqrt(sext(:,2)).*sext(:,5);
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

    function exc=rnuxm2nuz(sext,p,nuxx,nuzz)
        disp(['nuX - 2 nuZ = ' num2str(p)]);
        m=(nuxx-2*nuzz-p)/3;    % Dissonance
        nx=nuxx-m;
        nz=nuzz+m;
        phase=2*pi*(nx*sext(:,3)-2*nz*sext(:,6)+(p-(nx-2*nz))*sext(:,1));
%         phase=2*pi*(nuxx*sext(:,3)-2*nuzz*sext(:,6)-p*sext(:,1));
        ampl=sqrt(sext(:,2)).*sext(:,5);
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

end
