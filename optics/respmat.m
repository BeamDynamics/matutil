function [resp,obs,exc]=respmat(varargin)
%[resp,obs,exc]=respmat(optics,mode,nu)
%
%   optics: machine description: looks for:
%
% '/machfs/appdata/sr/optics/settings/theory'[,'sr']:
%			full path of optics directory
% 'sr' or 'sy':	machine name
%			path defaults to $(APPHOME)/mach/optics/settings/theory
% 'nombz2.5','sr':	optics name and machine
% atstrcut[,'sr']:	an Accelerator Toolbox structure
%
%   mode:
%*	sh:	horizontal steerers			-> BPM H orbit	m/rad
%*	sfh:horizontal steerers + dp/p	-> BPM H orbit	m/rad (fixed length)
%*	sv:	vertical steerers			-> BPM V orbit	m/rad
%	qh:	horizontal quadrupole displacement	-> BPM H orbit	m/m
%	qv:	vertical quadrupole displacement	-> BPM V orbit	m/m
%	qdh:	quadrupole strength	-> BPM horizontal dispersion	m/.
%	qdv:	quadrupole tilt		-> BPM vertical dispersion	m/rad
%*	sdh:	horizontal steerers	-> BPM horizontal dispersion	m/rad
%*	sdv:	vertical steerers	-> BPM vertical dispersion	m/rad
%*	sxdh:	sextupole H displacement	-> BPM H dispersion	m/m
%*	sxdv:	sextupole V displacement	-> BPM V dispersion	m/m
%*	cornqdh:quad correctors		-> BPM horizontal dispersion	m/(m-1)
%*	corsqdv:skew correctors		-> BPM vertical dispersion	m/(m-1)
%*	pin9h:	horizontal steerers	-> pinhole D9 H orbit
%*	pin25h:	horizontal steerers	-> pinhole ID25 H orbit
%*	idh:    horizontal kick in straight section	-> BPM H orbit m/rad
%*	idv:    vertical kick in straight section	-> BPM V orbit m/rad
%
%    nu: (optional) desired tune (defaults to structure tune);

[srmodel,options]=getmodel(varargin{:});
mode=options{1};
plane=mode(end);
if length(options) >= 2
	nu=options{2};
else
    nu=srmodel.(['nu' plane]);
end

action=mode(1:end-1);

if strncmp(action,'ID',2), action=['pin' action(3:end)]; end

%elseif strcmp(mode,'cor1dh')
%   [obs,exc,alpha,nuf]=model(srmodel,plane,'COR1','pu');
%   if nargin < narg, nu=nuf; end
%   resp=responsem(obs(:,2:3),exc(:,2:3),nu)*diag(exc(:,4));
if strcmp(action,'s')
   [obs,exc]=model(srmodel,plane,'st','bpm');
   resp=responsem(obs(:,2:3),exc(:,2:3),nu);
elseif strcmp(action,'sf')
   [obs,exc]=model(srmodel,plane,'st','bpm');
   resp=[responsem(obs(:,2:3),exc(:,2:3),nu)-obs(:,4)*exc(:,4)'/srmodel.ll ...
        obs(:,4)];
elseif strcmp(action,'q')
   [obs,exc]=model(srmodel,plane,'qp','bpm');
   if strcmp(plane,'h')
      resp=responsem(obs(:,2:3),exc(:,2:3),nu)*diag(exc(:,5));
   else
      resp=responsem(obs(:,2:3),exc(:,2:3),nu)*diag(-exc(:,5));
   end
elseif strcmp(action,'qd')
   [obs,exc]=model(srmodel,plane,'qp','bpm');
   if strcmp(plane,'h')
      resp=responsem(obs(:,2:3),exc(:,2:3),nu)*diag(-exc(:,4).*exc(:,5));
   else
      resp=responsem(obs(:,2:3),exc(:,2:3),nu)*diag(-2*exc(:,4).*exc(:,5));
   end
elseif strcmp(action,'sd')
   [~,exc]=model(srmodel,plane,'st','sx');
   [obs,dsx]=model(srmodel,plane,'sx','bpm');
   if strcmp(plane,'h')
      r2=responsem(obs(:,2:3),dsx(:,2:3),nu)*diag(2*dsx(:,4).*dsx(:,5));
   else
      r2=responsem(obs(:,2:3),dsx(:,2:3),nu)*diag(-2*dsx(:,4).*dsx(:,5));
   end
%  resp=responsem(obs(:,2:3),exc(:,2:3),nu) - r2*responsem(dsx(:,2:3),exc(:,2:3),nu);
   resp=-r2*responsem(dsx(:,2:3),exc(:,2:3),nu);
elseif strcmp(action,'sxd')
   [obs,exc]=model(srmodel,plane,'sx','bpm');
%  scale=2*exc(:,4).*exc(:,5);
%  resp=responsem(obs(:,2:3),exc(:,2:3),nu).*(ones(size(obs,1),1)*scale');
   if strcmp(plane,'h')
      resp=responsem(obs(:,2:3),exc(:,2:3),nu)*diag(2*exc(:,4).*exc(:,5));
   else
      resp=responsem(obs(:,2:3),exc(:,2:3),nu)*diag(-2*exc(:,4).*exc(:,5));
   end
elseif strncmp(action,'id',2)
   [obs,exc]=model(srmodel,plane,action,'bpm');
   resp=responsem(obs(:,2:3),exc(:,2:3),nu);
elseif isempty(strfind(action,'cor'))	% ID8 D9 ID25
   [obs,exc]=model(srmodel,plane,'st',action);
   resp=responsem(obs(:,2:3),exc(:,2:3),nu);
else		% cornqdh corsqdv
   [obs,exc]=model(srmodel,plane,action(1:5),'bpm');
   resp=responsem(obs(:,2:3),exc(:,2:3),nu)*diag(exc(:,4));
end
