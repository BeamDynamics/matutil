function [qexc,kexc,sexc]=beta_reson3(origin)
%BETA_RESON	produces resonance correction files for skmag
%	[qexc,kexc,sexc]=beta_reson(origin)
%^	origin: cell number of origin of phases

if nargin < 1, origin=4; end

[bpm,sext,nux,nuz]=load_betalog;

[qexc rr]=rquad3(sext(selcor(1,2),:),nux,nuz);
%[kexc rr]=rskew(sext(selcor(2,origin),:),nux,nuz);
[kexc rr]=newrskew(sext(selcor(2,origin),:),nux,nuz);
[sexc rr]=rsext(sext(selcor(3,2),:),nux,nuz);
