function resp2=srmresp(resp,periods)
%SRMRESP		Build response matrix of symmetrical storage ring
%
%SRESP=SRMRESP(RESP,PERIODS)
%

%function [resp2,respm]=srmresp(resp,periods)
[nbpm,nst]=size(resp);
if nst == 1
    resp2=repmat(mean2(reshape(resp,[],periods),2),periods,1);
else
    rots=reshape(1:nst,[],periods);
    rotb=(0:periods-1)*nbpm/periods;
    respm=zeros(nbpm,nst/periods,periods);
    for cell=1:periods
	respm(:,:,cell)=circshift(resp(:,rots(:,cell)),[-rotb(cell) 0]);
    end
    respa=mean2(respm,3);
    resp2=NaN(nbpm,nst);
    for cell=1:periods
	resp2(:,rots(:,cell))=circshift(respa,[rotb(cell) 0]);
    end
end
