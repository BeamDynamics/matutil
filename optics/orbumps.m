classdef orbumps < handle
    %Container for response matrices and bump coefficients
    
    properties
        steerdev    % Global steerer device (96 channels)
        freqdev     % RF frequency device
        inisteerval % Initial steerer values
        resp        % Response matrix
        bumps       % Kicker indices for local bumps
        kicks       % Kicker coefficients for local bumps
        steerval    % Actual steerer values (for simulation)
    end
    
    methods (Static)
        function rb=get(plane,varargin)
            %GET  Get current response matrices
            %
            %GET(plane)
            %   Use cached response matrices
            %
            %GET(PLANE,'opname')
            %   Reload response matrices from the 'opname' optics directory
            %
            %GET(PLANE,PATH)
            %   Reload response matrices from the selected path
            
            persistent bps
            hv=selectplane(plane,{'h','v'});
            if ~(isfield(bps,hv) && isempty(varargin))
                bps.(hv)=orbumps(plane,varargin{:});
            end
            rb=bps.(hv);
        end
    end
    
    methods
        function this=orbumps(plane,varargin)
            %ORBUMPS  Object giving access to the optical parameters
            %
            %ORBUMPS(plane)
            %   Default optics ($APPHOME/sr/optics/settings/theory)
            %
            %ORBUMPS(plane,'opname')
            %   Select 'opname'  ($APPHOME/sr/optics/settings/opname)
            %
            %ORBUMPS(plane,'/operation/appdata/sr/optics/settings/theory')
            %   Select a response measurement path
            
            [pth,~,mach]=getmodelpath(varargin{:});
            [ish,devname]=selectplane(plane,{true,false},{'sr/st-h/all','sr/st-v/all'});
            fprintf('loading the orbit response matrices from %s\n',pth);
            
            steerdev=tango.Device(devname);
            inival=steerdev.Current.set;
            ring=getmodel(mach,pth);
            s_bpm=ring.bpm(2);
            if ish
                freqdev=tango.Device('sy/ms/1');
                inival=[inival,freqdev.Frequency.set];
                [rfreq,~]=sr.load_fresp(pth);
                resp=[refurb(sr.load_normresp(fullfile(pth,'resH.rsp'),'h',...
                    'm/A','TooLarge',0,'Zeros',0,'Fixed',0)) refurb(rfreq)];
                s_st=ring.steerh(2);
            else
                freqdev=[];
                resp=refurb(sr.load_normresp(fullfile(pth,'resV.rsp'),'v',...
                    'm/A','TooLarge',0,'Zeros',0,'Fixed',0));
                s_st=ring.steerv(2);
            end
            this.steerdev=steerdev;
            this.freqdev=freqdev;
            this.inisteerval=inival(:);
            this.resp=resp;
            this.steerval=inival(:);
            [bumps,kicks]=arrayfun(@ff,1:224,'UniformOutput',false);
            this.bumps=cat(1,bumps{:});
            this.kicks=cat(2,kicks{:});
            
            function resp2=refurb(resp)
                resp2=srmresp(resp,16);
                ok=all(isfinite(resp),2);
                resp2(ok,:)=resp(ok,:);
            end
            
            function [bump,kick]=ff(bpmindex)
                [bump,kick]=local_bump3(resp,s_st,s_bpm,bpmindex);
            end
        end
        
        function setbump(this,bpmidx,amplitude)
            %SETBUMP    Set bumps at the selected BPMs
            %
            %SETBUMP(BPM,AMPLITUDE)
            %BPM:       List of BPM indices (1 <=> C4-1)
            %AMPLITUDE:	List of bump amplitudes [m]
            
            steerv=this.inisteerval;
            for i=1:length(bpmidx)
                bpi=this.bumps(bpmidx(i),:);
                di=this.kicks(:,bpmidx(i));
                steerv(bpi)=steerv(bpi)+amplitude(i)*di;
            end
            %             this.steerdev.Current=steerv(1:96)';     % DANGEROUS
            if ~isempty(this.freqdev)
                %                 this.freqdev.Frequency=steerv(97);	% DANGEROUS
            end
            this.steerval=steerv;
        end
        
        function orbit=getorbit(this)
            orbit=this.resp*(this.steerval-this.inisteerval);
        end
        
        function orbit=showbump(this,bpmidx,amplitude)
            %SHOWBUMP    Display bumps at the selected BPMs
            %
            %SHOWBUMP(BPM,AMPLITUDE)
            %BPM:       List of BPM indices (1 <=> C4-1)
            %AMPLITUDE: List of bump amplitudes [m]
            
            steerv=zeros(size(this.inisteerval));
            for i=1:length(bpmidx)
                bpi=this.bumps(bpmidx(i),:);
                di=this.kicks(:,bpmidx(i));
                steerv(bpi)=steerv(bpi)+amplitude(i)*di;
            end
            orbit=this.resp*steerv;
        end
    end
end
