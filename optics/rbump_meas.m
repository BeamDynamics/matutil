function rbump_meas(s, sh, sv, bpmindex, mode)
%RBUMP_MEAS(S,SH,SV,BPMINDEX,MODE)	Measure BPM offset
%
%S,SH,SV : structures returned by rbump_init
%BPMINDEX : BPM index (1 <=> C4-1)
%MODE :     measurement mode
%		'check' : computes deviation without moving the beam
%		'fit' : moves +/- one step and takes least square
%		'calibrate' : start with two points to determine a first estimate, then fit
%
%See also: RBUMP_INIT, RBUMP_ID

global APPHOME

if nargin < 5, mode='check'; end
bpm=s.bpm(bpmindex);
theoresp=[responsem(s.betaphi(:,2:3),bpm.h.qp,s.params.nuh) ...
    responsem(s.betaphi(:,5:6),bpm.v.qp,s.params.nuv)];
bpm.shuntdev=tango.Device(bpm.shuntname);
diary(fullfile(APPHOME,'shuntfiles',['out_' datestr(now,29)]));
fprintf('\nBPM %3d %s',bpmindex,bpm.bpmname);
diary('off');

if strcmp(mode,'check')
    vhv=evalfunc('Initial',s,bpm,theoresp); %#ok<NASGU>
else
    step = [sh.step sv.step];
    rbump_plot(1,'bumps',rbump_bump(sh.resp,sv.resp,bpm,step),bpmindex);
    try
        estimate = [0.0 0.0];
        vhv=NaN(7,10);
        if strcmp(mode,'calibrate')
            ampl(1:2,:)=set_amplitudes(estimate,[0;1]*step);
            [vhv(1,:),orbit0]=evalfunc('Initial',s,bpm,theoresp);
            [vhv(2,:),orbit]=evalfunc('Calibration',s,bpm,theoresp,sh,sv,ampl(2,:));
            rbump_plot(2,'bumps',orbit-orbit0,bpmindex);
            %AB=[0 -1;1 -1]\vhv(1:2,5:6);	% Using harmonic
            AB=[0 -1;1 -1]\vhv(1:2,7:8);	% Using fit
            best=real(AB(2,:)./AB(1,:));
            estimate = step.*best;
            bestrange = 1:6;
        else
            bestrange = 3:6;
        end
        
        pt3=[-1 -1;0 -1;1 -1];
        ampl(3:5,:)=set_amplitudes(estimate,pt3(:,1)*step);
            amp0(1,:)=set_amplitudes(estimate,-1.1*step);
            setbump(sh,bpm.h,amp0(1,1));
            setbump(sv,bpm.v,amp0(1,2));
            pause(1);
        vhv(3,:)=evalfunc('Estimate-step',s,bpm,theoresp,sh,sv,ampl(3,:));
        vhv(4,:)=evalfunc('Estimate',s,bpm,theoresp,sh,sv,ampl(4,:));
        vhv(5,:)=evalfunc('Estimate+step',s,bpm,theoresp,sh,sv,ampl(5,:));
        %AB=pt3\vhv(3:5,5:6);	% Using harmonic
        AB=pt3\vhv(3:5,7:8);	% Using fit
        best=real(AB(2,:)./AB(1,:));
        estimate = estimate + step.*best;
        
        ampl(6,:)=set_amplitudes(estimate, 0*step);
            amp0(2,:)=set_amplitudes(estimate,-0.1*step);
            setbump(sh,bpm.h,amp0(2,1));
            setbump(sv,bpm.v,amp0(2,2));
            pause(1);
        vhv(6,:)=evalfunc('Control',s,bpm,theoresp,sh,sv,ampl(6,:));
        
        vok=vhv(bestrange,:);
        [~,idx]=sort(abs(vok(:,[7 8]))); % Using fit
        vhv(7,1:2:10)=vok(idx(1,1),1:2:10);
        vhv(7,2:2:10)=vok(idx(1,2),2:2:10);
        rbump_disp('Best', vhv(7,:));
        
        rbump_plot(4,'calib',vhv(:,1:2),vhv(:,3:4));
        %rbump_plot(5,'harms',vhv(:,5:6));
        rbump_plot(3,'fit',vhv(:,1:2),vhv(:,7:8));

        setbump(sh, [], []);
        setbump(sv, [], []);
        save(fullfile(APPHOME,'shuntfiles',bpm.bpmname(16:end)),'vhv');
        bpm.shuntdev=0;
    catch err
        err
        disp('-Error: setting bumps to zero');
        setbump(sh, [], []);
        setbump(sv, [], []);
        bpm.shuntdev=0;
        rethrow(err);
    end
end

    function [v,orbit]=evalfunc(mess,s,bpm,theoresp,sh,sv,ampl)
        if nargin >= 7
            setbump(sh, bpm.h, ampl(1));
            setbump(sv, bpm.v, ampl(2));
        else
            ampl=[0.0 0.0];
        end
        if nargout >= 2
            [v1,orbit]=rbump_eval(s,bpm,theoresp);
            v=[ampl v1];
        else
            v=[ampl rbump_eval(s,bpm,theoresp)];
        end
        rbump_disp(mess, v);
    end

    function setbump(steerer,bpms,ampl)
        steerval=steerer.inisteerval;
        for bp=1:length(bpms)
            steerval(bpms(bp).bump)=steerval(bpms(bp).bump)+bpms(bp).kicks'*ampl(bp);
        end
        steerer.steerdev.Current=steerval(1:96);	% DANGEROUS
        if isfield(steerer,'freqdev')
            %     steerer.freqdev.Frequency=steerer.deltaf*steerval(97);	% DANGEROUS
        end
    end

    function ampls=set_amplitudes(estimate, step)
        active=true(size(estimate));
        active(~(abs(estimate) < 0.0005)) = false;
        ampls=repmat(estimate,size(step,1),1)+step;
        ampls(:,~active)=0;
    end

end
