function [a,ainv]=newrskew(sext,nux,nuz,code,bpm)

h1=round(nux+nuz);
h2=round(nux-nuz);
p=0.5*(nux+nuz-h1);
m=0.5*(nux-nuz-h2);
%					response 5x16
nskew=size(sext,1);
scale=sext(:,4)';
resp=responsem(bpm(:,5:6),sext(:,5:6),nuz).*scale(ones(size(bpm,1),1),:);
a=[...
    0.02*rnuxpnuz(sext,h1,nux-p,nuz-p);...
    0.02*rnuxmnuz(sext,h2,nux-m,nuz+m);...
    ones(1,nskew);...
%	0.0045*respmat('cor2dv',nuz) ...
    0.0045*resp ...
    ];

disprange=6:size(a,1);

figure(2);
resonplot(a(1:4,:),[h1 h2]);
%					v:16x16 u:5x5 s:5x16
[u,s,v]=svd(a,0);
%[u,s,v]=svd(a(1:5,:),0);
lambda=diag(s) %#ok<NOPRT>
nbvect=length(lambda);
keep=1:min([10 nbvect]);
ainv=v(:,keep)*diag(1./lambda(keep))*u(:,keep)';

i_rms=std2(ainv(:,1:5)) %#ok<NOPRT,NASGU>
eta_rms=std2(a(disprange,:)*ainv(:,1:5)) %#ok<NOPRT,NASGU>

figure(3);
resonplot3(a*v(:,1:nbvect),[h1 h2]);
response=a(1:4,:)';		%transpose for C order
correction=ainv(:,1:4)';	%transpose for C order
respdisp=a(disprange,:)';	%transpose for C order
corrdisp=ainv(:,disprange)';	%transpose for C order
info=ver('matlab');
matvers=sscanf(info.Version,'%d.%d');
if matvers(1) >= 5, fmode=' -v4'; else fmode='';end
if nskew==32
    fname='skew32cor';
else
    fname='skewcor';
    store_variable('matlog','skew_reson_mode','free');
end
eval(['save ' fname ' response correction respdisp corrdisp' fmode]);

    function exc=rnuxpnuz(sext,p,nux,nuz)
        disp(['nuX + nuZ = ' num2str(p)]);
        phase=2*pi*(nux*sext(:,3)+nuz*sext(:,6)+(p-(nux+nuz))*sext(:,1));
        ampl=sqrt(sext(:,2).*sext(:,5));
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

    function exc=rnuxmnuz(sext,p,nux,nuz)
        disp(['nuX - nuZ = ' num2str(p)]);
        phase=2*pi*(nux*sext(:,3)-nuz*sext(:,6)+(p-(nux-nuz))*sext(:,1));
        ampl=sqrt(sext(:,2).*sext(:,5));
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

end
