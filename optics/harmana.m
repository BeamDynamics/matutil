function h=harmana(range,bpm,y,nu)

npu=size(bpm,1);
c=size(y,2);
copyr=ones(1,length(range));

if size(bpm,2) == 3
   phase=(2*pi*1i*nu)*(bpm(:,3)-bpm(:,1))*copyr + bpm(:,1)*(range*(2*pi*1i));
   ynorm=y(1:npu,:)./(sqrt(bpm(:,2)*ones(1,c)));
else
   phase=bpm(:,2)*(range*(2*pi*1i));
   ynorm=y(1:npu,:)./(sqrt(bpm(:,1)*ones(1,c)));
end
%betay=(sqrt(bpm(ok,1))*ones(1,c)).*y(ok,:);
%exp_ikpsi=exp(phase)/length(ok);
exp_ikpsi=exp(phase);

h=zeros(c,length(range));
for k=1:c
   ok=find(isfinite(ynorm(:,k)));
   h(k,:)=sum((ynorm(ok,k)*copyr).*exp_ikpsi(ok,:))/length(ok);
end
