classdef ebsatmodel < ebs.model
    %EBSATMODEL Deprecated: Use ebs.model instead
    methods
        function this=ebsatmodel(varargin)
            warning('obsolete:model','%s is deprecated: Use ebs.model instead',mfilename);
            this@ebs.model(varargin{:});
        end
    end
end
