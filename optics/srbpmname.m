function varargout=srbpmname(idx,varargin)
%SRBPMNAME	returns the name of BPM with index idx
%
%Deprecated: see SR.BPMNAME
%BPMNAME=SRBPMNAME(IDX)	returns the name of BPM with index IDX
% IDX: vector of indexes in the range [1 224] (1 is C4-1)
%       or n x 2 matrix in the form [cell index] with
%       cell in [1 32], index in [1 7]
%
%BPMNAME=SRBPMNAME(IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : 'SR/D-BPM/C%i-%i')
%
%[NM1,NM2...]=SRBPMNAME(...)    May be used to generate several names
%
%[...,KDX]=SRBPMNAME(...)       returns in addition the "hardware" index KDX
%
%KDX=SRBPMNAME(IDX,'')          returns only the "hardware" index KDX
%
%   See also SR.BPMINDEX, SR.QPNAME, SR.SXNAME

[varargout{1:max(1,nargout)}]=sr.bpmname(idx,varargin{:});
end
