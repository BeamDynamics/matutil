function [exc,rexc]=rquad(sext,nux,nuz)

px1=ceil(2*nux);
px2=floor(2*nux);
pz1=ceil(2*nuz);
pz2=floor(2*nuz);
a=[...
	r2nux(sext,px1,0.5*px1);...
	r2nux(sext,px2,0.5*px2);...
	r2nuz(sext,pz1,0.5*pz1);...
	r2nuz(sext,pz2,0.5*pz2);...
%	r2nux(sext,px1,nux);...
%	r2nux(sext,px2,nux);...
%	r2nuz(sext,pz1,nuz);...
%	r2nuz(sext,pz2,nuz);...
];
exc=0.01*(a(:,1:8)-a(:,9:16));
rexc=inv(exc);
figure(1);
resonplot(exc,[px1 px2 pz1 pz2]);
savetext('quadcor2','%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n',...
[exc;rexc]);
%response=[exc -exc;ones(1,16)]';			%transpose for C order
response=[0.02*a;ones(1,16)]';				%transpose for C order
correction=0.5*[rexc zeros(8,1);-rexc zeros(8,1)]';	%transpose for C order
save quadcor response correction
