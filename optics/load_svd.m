function [invmat,respmat]=load_svd(fname, nmax)
%load_svd	load data for SVD orbit correction
%
%FNAME	SVD filename
%NMAX	Max. number of Eigenvectors

a=load(fname);
if nargin < 2
    nmax=size(a.svdbase,2);
end
invmat=a.svdsolve(:,1:nmax)*a.svdbase(:,1:nmax)';
respmat=a.svdbase*diag(a.lambda.*a.lambda)*a.svdsolve';
