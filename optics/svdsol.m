function cor=svdsol(y,svdinfo,nvect)
%cor=svdsol(y,svdinfo,nvect) computes eigen corrections
%	y: orbit(s) to correct
%   svdinfo: structure returned by svdcomp
%   nvect: nmber of Eigen vectors
%
%   See also: svdcomp
%
%   These functions supersede corsvd2/gosvd

ok=all(isfinite([svdinfo.base y]),2);
eigsorb=svdinfo.base(ok,:)'*(-y(ok,:));
if nvect > length(svdinfo.lambda)
nvect=length(svdinfo.lambda);
warning('Svd:maxsize',['number of vectors limited to ' num2str(nvect)]');
end
eigscor=eigsorb(1:nvect,:)./(svdinfo.lambda(1:nvect,ones(1,size(eigsorb,2))));
cor=svdinfo.v(:,1:nvect)*eigscor;
