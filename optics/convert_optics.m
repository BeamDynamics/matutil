function ats = convert_optics(varargin)
%CONVERT_OPTICS Gets the AT structure from an optics directory
%   CONVERT_OPTICS()     uses the current directory
%   CONVERT_OPTICS('sr') uses /operation/appdata/sr/optics/settings/theory
%   CONVERT_OPTICS(path) uses the specified path
%   CONVERT_OPTICS(opticsname[,'sr'])
%                        looks for opticsname in /operation/appdata/sr/optics/settings
%
%Deprecated: Use getmodelat

warning('obsolete:model','%s is deprecated: Use getmodelat instead',mfilename());
if nargin < 1
    pth=pwd;
    narg=1;
else
    [narg,pth,~]=getmachenv(varargin{:});
end
if exist(fullfile(pth,'betamodel.mat'),'file') == 2
    a=load(fullfile(pth,'betamodel.mat'));
    ats=a.betamodel;
else
    ats=sr.checklattice(atreadbeta(fullfile(pth,'betamodel.str')),varargin{narg:end});
end
end
