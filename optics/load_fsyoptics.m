function [pm,smach]=load_fsyoptics(path,fileorder)
%[PARAMS,SMACH]=LOAD_FSYOPTICS(PATH) Load optics parameters
%	scans the files "BETAOUT" and "BETALOG"

pm=load_betaout(path);
try
   bpmh=load_elems(path,pm,'BPMH',fileorder(1:9));
   bpmv=load_elems(path,pm,'BPMV',fileorder(1:9));
%  sx=load_elems(path,pm,'SX',fileorder);
   smach.qp=load_elems(path,pm,'QP',fileorder);
   nbh=size(bpmh,1);
   nbv=size(bpmv,1);
   smach.bpm=NaN*ones(nbh+nbv,9);
   [dum,bid]=sort([bpmh(:,1);bpmv(:,1)]);
   [dum,plid]=sort(bid);
   smach.h_bpms=logical(zeros(nbh+nbv,1));
   smach.h_bpms(plid( 1:nbh))=logical(1);
   smach.v_bpms=logical(zeros(nbh+nbv,1));
   smach.v_bpms(plid((1:nbv)+nbh))=logical(1);
   smach.bpm(smach.h_bpms,:)=bpmh;
   smach.bpm(smach.v_bpms,:)=bpmv;
   smach.steerh=load_elems(path,pm,'CH',fileorder(1:9));
   smach.steerv=load_elems(path,pm,'CV',fileorder(1:9));
   smach.bpm_name=cell(nbh+nbv,1);
   smach.bpm_name(smach.h_bpms)=strcat('SY/D-BPM/',cellstr(num2str([28:38 1:27]','%-1i-F')));	% generate bpm names
   smach.bpm_name(smach.v_bpms)=strcat('SY/D-BPM/',cellstr(num2str([28:39 2:13 15:27]','%-1i-D')));
   hcrate=zeros(39,1);						% generate steerer names
   hcrate(1:14)=5;
   hcrate(15:21)=4;
   hcrate(22:25)=1;
   hcrate(26:39)=2;
   smach.steerh_name=steername('CH',hcrate);
   vcrate=zeros(39,1);
   vcrate(1:14)=6;
   vcrate(15:20)=4;
   vcrate(21:25)=1;
   vcrate(26:39)=3;
   smach.steerv_name=steername('CV',vcrate);
   smach.qp_name=cell(78,1);
   smach.qp_name(1:2:78)=cellstr(num2str([28:39 1:27]','SY/PS-QF/%-1i'));
   smach.qp_name(2:2:78)=cellstr(num2str([28:39 1:27]','SY/PS-QD/%-1i'));
catch
   disp(lasterr);
end

function sname=steername(code,cratelist)
steerer=[28:39 1:27]';
crate=cratelist(steerer);
[domain{1:39}]=deal('SY');
[domain{crate==1}]=deal('TL1SY');
sname=cell(size(cratelist));
for l=1:length(crate)
sname{l}=sprintf('%s/PS-C%i/%s%i',domain{l},crate(l),code,steerer(l));
end
