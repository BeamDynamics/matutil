function resp=syexpand(r)
%SYEXPAND		Introduces missing BPMS in SY response matrix
%
%XRESP=SYEXPAND(RESP)
%
%	RESP:  75xN matrix or 38xM (horizontal) or 37xN (vertical)
%	RESPX: 78xN matrix
%
nb=size(r,1);
resp=NaN*ones(78,size(r,2));
ok=true(78,1);
ok([23 26 52])=false;
if nb == 38		% horizontal
    ok(2:2:78)=false;
elseif nb == 37		% vertical
    ok(1:2:78)=false;
end
resp(ok,:)=r;
