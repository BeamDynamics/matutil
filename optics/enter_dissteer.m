function disabled=enter_dissteer(ini, code)

disabled=ini(:)';
fprintf('%i disabled %s steerers:\n',length(disabled),code);
%for steer=disabled,disp(sr.steername(steer),code);end
for steer=disabled,disp(sr.steername(steer,[code '%i/C%i']));end
str=input('Enter disabled steerer: ','s');
while ~isempty(str)
   try
      disabled=sort([disabled sr.steerindex(str,[code '%i/C%i'])]);
   catch err
      disp(err);
   end
   fprintf('%i disabled %s steerers:\n',length(disabled),code);
   for steer=disabled,disp(sr.steername(steer,[code '%i/C%i']));end
   str=input('Enter disabled steerer: ','s');
end
end
