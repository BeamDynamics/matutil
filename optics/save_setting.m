function [s,w]=save_setting(appliname, filename, devlist,devset,comment)
%SAVE_SETTING saves an application setting file
%
%[STATUS,MESSAGE]=SAVE_SETTING(APPLICATION,FILENAME,DEVLIST,DEVSET,COMMENT);

if nargin < 5, comment='Created by MATLAB'; end
[s,tmpfile]=unix('mktemp');
tmpfile=strtok(tmpfile);

fid=fopen(tmpfile,'wt');
for i=find(finite(devset))'
   fprintf(fid,'%s\t%.6f\n',deblank(devlist(i,:)),devset(i));
end
fclose(fid);

command=sprintf('newfile -m "%s" %s %s <%s',comment,appliname,filename,tmpfile);
[s,w]=unix(command);
delete(tmpfile);
