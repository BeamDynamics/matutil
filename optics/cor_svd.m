function cormat = cor_svd(uu,lambda,vv,nvects,mode)
%COR_SVD Compute svd correction matrix
%   CORMAT=COR_SVD(U,LAMBDA,V,NVECTS,MODE)

linv=1./lambda;
if strcmp(mode,'clip')
    linv(nvects+1:end)=linv(nvects);
else
    linv(nvects+1:end)=0;
end
cormat=vv*diag(linv)*uu';    
end

