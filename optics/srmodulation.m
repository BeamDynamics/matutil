function [vmodul,vave] = srmodulation(v,vave)
%SRMODULATION Compute the modulation of a variable
%MODUL=SRMODULATION(V,V0)

if nargin < 2
    vave=zeros(size(v));
    for col=1:size(v,2)
        vave(:,col)=srunfold(repmat(mean(srfold(v(:,col)),2),1,32));
    end
end
vmodul=(v-vave)./vave;
end
