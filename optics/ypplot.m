%haxe=betaxe;
scell=299.618774/3;
hh=betaline(1);
sbh0=get(hh,'xData');
bbh0=get(hh,'yData');
hh=betaline(2);
sbv0=get(hh,'xData');
bbv0=get(hh,'yData');
figure(2)
plot(sbh0,bbh0,'r');
ax=axis;
ax(2)=scell;
axis(ax);
hold on
plot(sbv0,bbv0);
%plot(sbv,bbv0);
legend('Horizontal','Vertical');
xlabel('s [m]');
ylabel('\beta [m]');

bbhx=reshape(syexpand(bbh),26,3);
sbhx=299.618774*reshape(syexpand(sbh),26,3);
sbhx(25)=299.618774*sbh(24);
sbhx(:,2)=sbhx(:,2)-scell;
sbhx(:,3)=sbhx(:,3)-2*scell;
plot(sbhx(:,1),bbhx(:,1),'ro');
plot(sbhx(:,2),bbhx(:,2),'ro');
plot(sbhx(:,3),bbhx(:,3),'ro');

bbvx=reshape(syexpand(bbv),26,3);
sbvx=299.618774*reshape(syexpand(sbv),26,3);
sbvx(:,2)=sbvx(:,2)-scell;
sbvx(:,3)=sbvx(:,3)-2*scell;
plot(sbvx(:,1),bbvx(:,1),'o');
plot(sbvx(:,2),bbvx(:,2),'o');
plot(sbvx(:,3),bbvx(:,3),'o');
hold off
