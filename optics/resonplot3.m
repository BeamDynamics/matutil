function resonplot3(orbits,tit)

subplot(2,2,1)
bar(sum(orbits(1:2,:).*orbits(1:2,:)));
if nargin >=2, title(int2str(tit(1))); end

subplot(2,2,2)
bar(sum(orbits(3:4,:).*orbits(3:4,:)));
if nargin >=2, title(int2str(tit(2))); end

subplot(2,2,3)
bar(orbits(5,:));
title('Sum of intensities');

subplot(2,2,4)
disprange=6:size(orbits,1);
bar(std(orbits(disprange:229,:)));
title('rms. dispersion');
