function [pm,varargout]=load_optics(varargin)
%[PARAMS,V1,V2,...]=LOAD_OPTICS(OPTICS,CODE1,CODE2,...) Load optics parameters
%
%	OPTICS	machine description: looks for:
%
% '/machfs/appdata/sr/optics/settings/theory'[,'sr']:
%			full path of optics directory
% 'sr' or 'sy':	machine name
%			path defaults to $(APPHOME)/mach/optics/settings/theory
% 'nombz2.5','sr':	optics name and machine
% atstrcut[,'sr']:	an Accelerator Toolbox structure
%
%	CODE1,CODE2... name of desired parameter table.
%		'bpm'		 all bpms
%		'hbpm'		 horizontal bpms
%		'vbpm'		 vertical bpms
%		'steerh'	 horizontal steerers
%		'steerv'	 vertical steerers
%		'qp'		 quadrupoles
%		'h_bpms'	 flags the horizontal bpms
%		'v_bpms'	 flags the vertical bpms
%    numeric vector  code of selected parameters
%
%	For SR
%		'sx'		 sextupoles
%		'cornq'		 quadrupole correctors
%		'corsq'		 skew quadrupole correctors
%		'id'		 middle of straight sections
%		'idx','hilox'middle if IDx section
%		'bm'		 bending magnet sources (entrance of D2 dipole)
%       'dipole'     middle of D1 dipole
%       'd1'         entrance of D1
%       'd2'         entrance of D2
%		'high'		 middle of high beta straight sections
%		'low'		 middle of low beta straight sections
%		'pin25'		 ID25 pinhole camera
%		'pin9'		 D9 pinhole camera
%
%	PARAMS	structure containing the following fields
%		periods,nuh,nuv,ll,alpha,emitx,emitz,strname
%
%      parameter code (0 available only for AT structures):
%
% [ 0  1    2       3       4      5       6    7      8      9      10   ]
% [id  Kl theta/2pi betax alphax phix/2pi etax eta'x betaz alphaz phiz/2pi]
%
%      default parameters:
%
%      [   2         3         5        6      8       10      ]
%      [theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux]

%		'dipole'	middle of hard-end dipole

[narg,pth,mach]=getmachenv(varargin{:});
if iscell(varargin{1})
    disp('load_optics using AT structure');
    if strcmp(mach,'sr')
        [pm,varargout{1:nargout-1}]=load_atoptics(varargin{[1 narg:end]});
    else
        [pm,varargout{1:nargout-1}]=sy.load_atoptics(varargin{[1 narg:end]});
    end
else
    if exist(fullfile(pth,'betamodel.mat'),'file') == 2
        disp('load_optics using betamodel.mat...');
        a=load(fullfile(pth,'betamodel.mat'));
        if strcmp(mach,'sr')
            [pm,varargout{1:nargout-1}]=load_atoptics(a.betamodel,varargin{narg:end});
        else
            [pm,varargout{1:nargout-1}]=sy.load_atoptics(a.betamodel,varargin{narg:end});
        end
    else
        fileorder=[2:10 1];
        oldfile=fullfile(pth,'matoptics');
        matfile=fullfile(pth,'optics');
        
        if exist([matfile '.mat'],'file') == 2
            disp(['load_optics using ' matfile '.mat...']);
            load(matfile);
        elseif exist([oldfile '.mat'],'file') == 2
            disp(['load_optics using ' oldfile '.mat...']);
            smach=load(oldfile);
            pm=smach.pm;
        elseif strcmp(mach,'sr')
            disp(['cannot open ' matfile '.mat']);
            try
                [pm,smach]=load_fsroptics(pth,fileorder);
                save(matfile,'pm','smach')
            catch %#ok<*CTCH>
                disp(lasterr); %#ok<*LERR>
            end
        elseif strcmp(mach,'sy')
            disp(['cannot open ' matfile '.mat']);
            try
                [pm,smach]=load_fsyoptics(pth,fileorder);
                save(matfile,'pm','smach');
            catch
                disp(lasterr);
            end
        end
        [varargout{1:nargout-1}]=decode(pth,pm,fileorder,smach,varargin{narg:end});
    end
end

    function varargout=decode(path,pm,fileorder,smach,varargin)
        nin=1;
        nout=1;
        locs(fileorder)=1:length(fileorder);
        params=locs([2 3 5 6 8 10]);	% default parameter list as in load_elems
        while (nin<=length(varargin))
            if isnumeric(varargin{nin})
                params=locs(varargin{nin});
            else
                incode=lower(varargin{nin});
                if strncmp(incode(2:end),'bpm',3)
                    if isfield(smach,[incode(1) '_bpms'])
                        varargout{nout}=smach.bpm(smach.([incode(1) '_bpms']),params); %#ok<*AGROW>
                    else
                        varargout{nout}=smach.bpm(:,params);
                    end
                elseif isfield(smach,incode)
                    if size(smach.(incode),2) == 1
                        varargout{nout}=smach.(incode);
                    else
                        varargout{nout}=smach.(incode)(:,params);
                    end
                else
                    switch incode
                        case 'high'
                            varargout{nout}=smach.id(1:2:32,params);
                        case 'low'
                            varargout{nout}=smach.id(2:2:32,params);
                        case 'pin25'
                            q=load_elems(path,pm,'SD6U SD',fileorder);
                            varargout{nout}=q(20,params);
                        case 'pin9'
                            try
                                q=load_elems(path,pm,'23 SD',fileorder);
                            catch
                                q=load_elems(path,pm,'SDBD SD',fileorder);
                            end
                            varargout{nout}=q(12,params);
                        case 'bm'
                            try
                                q=load_elems(path,pm,'23 SD',fileorder);
                            catch
                                q=load_elems(path,pm,'SDBD SD',fileorder);
                            end
                            varargout{nout}=q(2:2:64,params);
                        case 'd1'
                            q=load_elems(path,pm,'B1IN',fileorder);
                            varargout{nout}=q(:,params);
                        case 'd2'
                            q=load_elems(path,pm,'B2IN',fileorder);
                            varargout{nout}=q(:,params);
                            % 			   case 'pin8'
                            % 				  q=load_elems(path,pm,'SD3D SD',fileorder);
                            % 				  varargout{out}=q(5,params);
                        case 'dipole'
                            q=load_elems(path,pm,'B1H  DI',fileorder);
                            varargout{nout}=q(:,params);
                        otherwise
                            if strncmp(incode,'id',2)
                                varargout{nout}=smach.id(hlrange(incode(3:end)),params);
                            elseif strncmp(incode,'hilo',4)
                                varargout{nout}=smach.id(hlrange(incode(5:end)),params);
                            else
                                varargout{nout}=zeros(0,length(params));
                            end
                    end
                end
                nout=nout+1;
            end
            nin=nin+1;
        end
        
        function rg=hlrange(cx)
            if isempty(cx)
                rg=1:32;
            else
                cx2=str2num(cx)-4; %#ok<ST2NM>
                rg=cx2+32*(cx2<0)+1;
            end
        end
    end
end
