function xave=systeeraverage(x,plane)
%SYBPMAVERAGE			average BPM values using SY symmetries

if strcmp(plane,'h')
    a=reshape([x;NaN*ones(1,39)],26,3);
else
    a=reshape([NaN*ones(1,39);x],26,3);
end
vsave=a([23 25 52]);
a([23 25 52])=NaN;	% remove H12, H13, V25
a=mean2(a');
a=repmat(mean2([a;a([25:-1:1 26])]),1,3);
a([23 25 52])=vsave;
if strcmp(plane,'h')
    xave=a(1:2:78);
else
    xave=a(2:2:78);
end
