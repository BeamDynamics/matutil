function [xm,xpm]=matchemit(x0,xp0,x,xp,drift)
%computes the surrounding matched emittance
%	x0, xp0: reference emittance (alpha=0)
%	x , xp : unmatched emittance defined at distance drift where alpha=0
%	xm, xpm: matched emittance

b0=x0/xp0;
b=x/xp;
c=(b0*b0+b*b+drift*drift)/b0/b/2;
eps=x*xp*(c+sqrt(c*c-1));
xm=sqrt(eps*b0);
xpm=sqrt(eps/b0);
