function [hv_bpms,h_steerers,v_steerers,kh,kv,ks]=beta_model(path)
%BETA_MODEL     Prepare matrices for the autocor device server
%
%Deprecated: Use sr.autocor_model
warning('obsolete:model','%s is deprecated: Use sr.autocor_model instead',mfilename());

if nargin < 1, path=pwd; end

[coefh, coefv]=load_corcalib('sr',8,9);     % get steerer calibration (rad/A)

[pm,bpm,bpm_name,steerh,steerh_name,steerv,steerv_name]=load_optics(path,'bpm','bpm_name',...
   'steerh','steerh_name','steerv','steerv_name');

if exist('./alpha','file') ~= 2,
   savetext('alpha','%.4e\n',pm.alpha);
end

scaling=input('Energy scaling factor (1.0) : ');

ns=size(steerh,1);
nb=size(bpm,1);
periods=16;

xlist=[1 3 4 6];	% select a subset from useful steerer
sorder=reshape(1:ns,ns/periods,periods);
xok=sorder(xlist,:);

h_resp=responsem(bpm(:,2:3),steerh(:,2:3),pm.nuh).*coefh(ones(1,nb),:);
sh_ok=true(1,size(h_resp,2));
v_resp=responsem(bpm(:,5:6),steerv(:,5:6),pm.nuv).*coefv(ones(1,nb),:);
sv_ok=true(1,size(v_resp,2));
h_fresp=-bpm(:,4)*pm.ll/pm.alpha/992/2.997924e8;
stdisp=steerh(:,4)';

[b,f,pu,npu]=bump_coefs(h_resp,bpm(:,1),steerh(:,1),periods);
kh=[b(1:3,:)' pu-1 npu f];

[b,f,pu,npu]=bump_coefs(v_resp,bpm(:,1),steerv(:,1),periods);
ks=[b' pu-1 npu f];

[b,f,pu,npu]=bump_coefs(v_resp(:,xok),bpm(:,1),steerv(xok,1),periods);
kv=[b' pu-1 npu f];

if ~isempty(scaling)
  if (scaling ~= 1)
   kh(:,1:3)=kh(:,1:3)*scaling;
   kv(:,1:3)=kv(:,1:3)*scaling;
   ks(:,1:3)=ks(:,1:3)*scaling;
   h_resp=h_resp/scaling;
   v_resp=v_resp/scaling;
   coefh=coefh/scaling;
   coefv=coefv/scaling;
  end
end

hv_bpms=[bpm(:,1:2) 2*pi*bpm(:,3) bpm(:,4:5) 2*pi*bpm(:,6)];
h_steerers = [steerh(:,1:2) 2*pi*steerh(:,3) coefh(:)];
v_steerers = [steerv(:,[1 5]) 2*pi*steerv(:,6) coefv(:)];
store_bpm('bpms',hv_bpms,bpm_name);
store_steerer('h_steerers',h_steerers,steerh_name);
store_steerer('v_steerers',v_steerers,steerv_name);

%disabled_b=[18 31 47 73 89 115 143 147 164 206];% select interlock BPMs
disabled_b=[];
%disabled_b=[18 147];   % 6-4, C24-7
disabled_b=enter_disbpm(disabled_b);

b_ok=true(nb,1);
b_ok(disabled_b)=false;

sh_ok(enter_dissteer([],'SR/ST-H')) = false;
sv_ok(enter_dissteer([],'SR/ST-V')) = false;

resh=store_svd('h_svd',h_resp,b_ok,sh_ok,h_fresp,stdisp);  %#ok<NASGU>
resv=store_svd('v_svd',v_resp,b_ok,sv_ok); %#ok<NASGU>
store_bump('h_bumps',kh,steerh_name);
store_bump('v_bumps',kv,steerv_name(xok));
store_bump('v_short_bumps',ks,steerv_name);
store_variable('matlog','disabled_bpms',sr.bpmname(find(~b_ok))); %#ok<FNDSB>
store_variable('matlog','disabled_sh',sr.steername(~sh_ok,'SR/ST-H%i/C%i'));
store_variable('matlog','disabled_sv',sr.steername(~sv_ok,'SR/ST-V%i/C%i'));
store_variable('matlog','resp','theoretical');
