function g=girder2sext
%G=GIRDER builds the girder to sextupole displacement matrix

g=srsparse([1 1 2 2 2 3 3],3);
