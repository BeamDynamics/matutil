function [pm,smach]=load_fsroptics(path,fileorder)
%[PARAMS,SMACH]=LOAD_FSROPTICS(PATH) Load optics parameters
%	scans the files "BETAOUT" and "BETALOG"
% expects:
%	BPMs have the name "PICK"
%	Thin sextupoles are represented by code "SX"
%	High beta straight have name 45 or SDHI
%	Low beta straight sections have name 45 or 1 or HILO

[nqlist,smach.cornq_name]=selcor(6);
[sqlist,smach.corsq_name]=selcor(10);
[sxlist,smach.corsx_name]=selcor(3);
if nargin >= 2
   pm=load_betaout(path);
   smach.bpm=load_elems(path,pm,'PICK',fileorder);
   smach.h_bpms=true(size(smach.bpm,1),1);
   smach.v_bpms=smach.h_bpms;
   smach.sx=load_elems(path,pm,'SX',fileorder);
   smach.qp=load_elems(path,pm,'QP',fileorder);
   ns=size(smach.sx,1);
   slist=[2 4 6 9 11 13];
   sorder=reshape(1:ns,ns/16,16);
   sok=sorder(slist,:);
   smach.steerh=smach.sx(sok,1:9);
   smach.steerv=smach.sx(sok,1:9);
   smach.cornq=smach.sx(nqlist,1:9);
   smach.corsq=smach.sx(sqlist,1:9);
   smach.corsx=smach.sx(sxlist,1:9);
   sd=load_elems(path,pm,{'  45 SD','   1 SD','SDHI SD','SDHX SD','SDLO SD',...
       'SDLX SD','SDLW SD'},fileorder);
   sd(end,[1 4 9])=0;	% theta, phix, phiz
   if size(sd,1) == 33		% single period
	  keep=[33 2:32]';
   elseif size(sd,1) == 48		% 16 periods
	  keep=reshape([48 1:47],3,16);
	  keep=reshape(keep([1 3],:),32,1);
   else
	  error('Cannot identify straight sections (45 SD)');
   end
   smach.id=sd(keep,:);
else
   pm=[];
end

cell_list=(0:31)+4;
cell_list=int2cellstr(cell_list-32*(cell_list>32));

[bpm_num,bpm_cell]=gen_name(int2cellstr([1:7 1:7]'),cell_list);			% generate BPM names
smach.bpm_name=strcat('SR/D-BPM/C',bpm_cell,'-',bpm_num);

[steer_num,steer_cell]=gen_name(int2cellstr([6;19;22;22;19;6]),cell_list);	% generate steerer names
smach.steerh_name=strcat('SR/ST-H',steer_num,'/C',steer_cell);
smach.steerv_name=strcat('SR/ST-V',steer_num,'/C',steer_cell);

if size(smach.qp,1) == 320		% generate quad names
   qnum={	'QD1';'QF2';'QD3';'QD4';'QD5';'QD5';'QD4';'QD6';'QF7';'QD8';...
	  'QD8';'QF7';'QD6';'QD4';'QD5';'QD5';'QD4';'QD3';'QF2';'QD1'};
   qnu2={	'';'';'';'.1';'.1';'.2';'.2';'';'';'';...
	  '';'';'';'.1';'.1';'.2';'.2';'';'';''};
else
   qnum={'QF2';'QD3';'QD4';'QD5';'QD5';'QD4';'QD6';'QF7';...		% generate quad names
	  'QF7';'QD6';'QD4';'QD5';'QD5';'QD4';'QD3';'QF2'};
   qnu2={'';'';'.1';'.1';'.2';'.2';'';'';...
	  '';'';'.1';'.1';'.2';'.2';'';''};
end
[qp_nu2,qp_cell]=gen_name(qnu2,cell_list); %#ok<NASGU>
[qp_num,qp_cell]=gen_name(qnum,cell_list);
smach.qp_name=strcat('SR/M-',qp_num,'/C',qp_cell,qp_nu2);

snum={	'S4';'S6';'S13';'S19';'S20';'S22';'S24';...				% generate sextu names
   'S24';'S22';'S20';'S19';'S13';'S6';'S4'};
[sx_num,sx_cell]=gen_name(snum,cell_list);
smach.sx_name=strcat('SR/M-',sx_num,'/C',sx_cell);

function [nlist,clist]=gen_name(name_list,cell_list)
periods=length(cell_list)/2;
npercell=length(name_list)/2;
nb=npercell*length(cell_list);
nlist=reshape(name_list(:,ones(periods,1)),nb,1);
clist=reshape(cell_list(ones(npercell,1),:),nb,1);

function nm=int2cellstr(list)
nm=cell(size(list));
for i=1:length(list(:))
   nm{i}=int2str(list(i));
end
