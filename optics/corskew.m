function [resph,respv,list,devlist]=corskew(bpm0,sext0,nux,nuz,range)
%CORSKEW	computes response matrices for coupling correction
%
%[RH,RV,LIST,DEVLIST]=CORSKEW(BPM,SEXT,NUX,NUZ,RANGE)
%	RH, RV: (m^2 / rad^2) / (rad / m)
%	RANGE:  list of steerers to be cumulated

ns=size(sext0,1);
keepk=reshape(1:ns,ns/16,16);
keepk=keepk([2 4 6 9 11 13],:);
[list,devlist]=selcor(2);

h_kb=responsem(bpm0(:,2:3),sext0(keepk,2:3),nux);
v_kb=responsem(bpm0(:,5:6),sext0(keepk,5:6),nuz);
h_ks=responsem(sext0(list,2:3),sext0(keepk,2:3),nux);
v_ks=responsem(sext0(list,5:6),sext0(keepk,5:6),nuz);
h_sb=responsem(bpm0(:,2:3),sext0(list,2:3),nux);
v_sb=responsem(bpm0(:,5:6),sext0(list,5:6),nuz);

resph=zeros(size(h_sb));
for i=1:size(resph,2)
resp=h_kb.*(v_sb(:,i)*h_ks(i,:));
resph(:,i)=sum(resp(:,range)')';
end

respv=zeros(size(v_sb));
for i=1:size(respv,2)
resp=v_kb.*(h_sb(:,i)*v_ks(i,:));
respv(:,i)=sum(resp(:,range)')';
end
