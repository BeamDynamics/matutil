function cor=gosvd(lambda, base, y)
%cor=GOSVD(lambda,base,y) computes eigen corrections
%	lambda: eigen values
%	base: base of eigen orbits
%	y: orbit(s) to correct
%
%	The correction vector(s) are v * cor
%
%   See also: corsvd2
%
%   These functions are superseded by svdcomp/svdsol

ok=find(all(finite([base y]')));
cor=(base(ok,:)'*(-y(ok,:)))./(lambda*ones(1,size(y,2)));
