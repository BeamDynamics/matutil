function rcor=defreq(r,rfreq)

%rext=[r rfreq];
%[base,lambda,v]=corsvd2(rext);
%[dum,id]=sort(abs(v(97,:)));
%freqvec=id(97)
%fcor=gosvd(lambda(freqvec),base(:,freqvec),r);
%rcor=r+r*diag(fcor);

fok=isfinite(rfreq);
rcor=r;
db2=rfreq/sqrt(rfreq(fok)'*rfreq(fok));	% normalize dispersion
for i=1:size(r,2)
   ok=isfinite(r(:,i));
   if sum(ok) > 0
   rcor(:,i)=rcor(:,i)-(r(ok,i)'*db2(ok)) * db2;
   end
end
