function [cor,orms,krms]=svdanal(varargin)
%SVDANAL	Simulates SVD orbit correction
%
%[COR,ORMS,KRMS]=SVDANAL(OPTICS,PLANE,NSAMPLE)
%generates closed orbit distortions by displacing quadrupoles (0.1 mm rms)
%and introducing BPM reading errors (0.1 mm rms),
%and analizes the corresponding SVD correction.
%
%OPTICS:     - full path of the optics directory
%		(ex.: '/machfs/appdata/sr/optics/settings/nombz2.5')
%	     - 'sr' or 'sy' (using the 'theory' optics for the selected machine)
%	     - both
%PLANE:      - 'h' or 'v'
%NSAMPLE:    - number of machine samples (defaults to 10)

nsample=10;
[narg,path,mach]=getmachenv(varargin{:});
plane=selectplane(varargin{narg},{'h','v'});
narg=narg+1;
if nargin >= narg
   nsample=varargin{narg};
   narg=narg+1;
end
respx=respmat(path,mach,['q' plane]);			%generate orbits
def=0.0001*randn(size(respx,2),nsample);
y=respx*def+0.0001*randn(size(respx,1),nsample);

[respc,bpm,steerer]=respmat(path,mach,['s' plane]);	%generate correction

[u,s,v]=svd(respc,0);
lambda=diag(s);
nvects=length(lambda);
cor=-diag(1./lambda)*u'*y;
for meas=1:size(cor,2);
vects=cumsum(v(:,1:nvects)*diag(cor(:,meas)),2);
orb=y(:,meas*ones(1,nvects))+respc*vects;
orms(:,meas)=[std(y(:,meas)) std(orb)]';
krms(:,meas)=[0 std(vects)]';
end

plot((0:nvects)',orms);
xlabel('Eigen vectors');
ylabel(['Residual ' plane ' orbit (m)']);
grid on
