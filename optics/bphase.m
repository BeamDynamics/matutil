function [phloc,phave]=bphase(normr,sb,sk,tune,bph0,kph0)

nbpm=size(normr,1);
if nbpm == 224
   mirror=[1 14 13 4 5 10 9 8 7 6 11 12 3 2];
elseif nbpm == 96
   mirror=[1 6 5 4 3 2];
else
   mirror=[1 4 3 2];
end

% remove unprecise values ( cos > 1 )
out=find(abs(normr)>0.85);
normr(out)=NaN*ones(length(out),1);

% keep only valid BPMs
ok=find(any(finite(normr')));
nb=length(ok);
nk=size(normr,2);
ff=acos(normr(ok,:))/(2*pi);

% invert acos as on estimate
b=(0.5 - abs(bph0*ones(size(kph0)) - ones(size(bph0))*kph0))*tune;
b=b-floor(b);
c=sb*ones(size(sk)) > ones(size(sb))*sk;
inv=find(b(ok,:)>0.5);
ff(inv)=1-ff(inv);
inv=find(c(ok,:));
ff(inv)=1-ff(inv);
clear b inv

for k=1:nk
   below=1:nb;
   above=find(c(ok,k))';
   below(above)=[];
   perm=[above below];
   avph=[NaN;diff(ff(perm,k))];
   inv=find(avph<0);
   avph(inv)=avph(inv)+1;
   ff(perm,k)=avph;
end

deltaphi=mean2(ff')';

phave=NaN*ones(nbpm,1);
phave(ok) = deltaphi;

error=NaN*ones(nbpm,1);
error(ok) = std2(ff')'./deltaphi;

phloc=NaN*ones(nbpm,1);

nok=[2:nbpm 1];
nok(ok)=[];
phave(nok)=NaN*ones(length(nok),1);
phave=mean2(reshape(phave,nbpm/16,16)');	% average over cells
phave=mean2([phave;phave(mirror)]);		% average/mirror
phave=phave'*ones(1,16);
ave_tune=sum(phave(:))				% average tune
phave=(-0.5*phave(1)+cumsum(phave(:)))/ave_tune;% normalized average ph. adv.

tune=sum(deltaphi)				% uncorrected tune
phloc(ok)=(cumsum(deltaphi))/tune;
phloc=phloc-mean2(phloc-phave);			% normalized ph. adv.
