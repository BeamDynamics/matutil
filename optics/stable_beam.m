function [inj,lost,iok]=stable_beam(i,nmin,imin,imax)
%[inj,lost,i_ok]=stable_beam(i,nb_min,imin,imax)	selects stable beam condiitons
%
%	i:	intensity
%	nb_min:	min. number of points in a decay (default 5)
%	imin:	keep only data for i >= imin (default 30)
%	imax:	keep only data for i <= imax (default 210)

if nargin < 3, imax=210; end
if nargin < 3, imin=30; end
if nargin < 2, nmin=5; end
j=i(:,1);
jprev=[NaN;j(1:end-1)];
jnext=[j(2:end);NaN];

hole=find(~isfinite(j) & (jprev-jnext)<1);	% interpolates through single holes
j(hole)=0.5*(jprev(hole)+jnext(hole));
jprev=[NaN;j(1:end-1)];                      % rebuild limits
%jnext=[j(2:end);NaN];

ok=(j<=imax) & (j>=imin) & (j < jprev) & ((j+1) > jprev);
inj=find(ok & ~([false;ok(1:end-1)]))-1;    % OK and previous not OK
lost=find(ok & ~([ok(2:end);false]));       % OK and next not OK
short=find((lost-inj) < nmin);
inj(short)=[];
lost(short)=[];
if nargout >= 3
   iok=NaN*ones(size(j));
   for decay=1:length(inj)
      range=inj(decay):lost(decay);
      iok(range)=j(range);
   end
end
