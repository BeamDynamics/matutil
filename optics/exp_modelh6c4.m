function [kh,kv,ks]=exp_model

[bpm,sext,betanux,betanuz,betaperiods,alpha]=load_betalog;

scaling=input('Energy scaling factor (1.0) : ');

ns=size(sext,1);
nb=size(bpm,1);
periods=16;

bpm_num=[1:7 1:7]';				   % generate bpm names
bpm_num=reshape(bpm_num(:,ones(periods,1)),nb,1);

steer_num=[4;6;13;19;20;22;24;24;22;20;19;13;6;4]; % generate steerer names
steer_num=reshape(steer_num(:,ones(periods,1)),ns,1);

cell_list=(0:31)+4;
cell_list=cell_list-32*(cell_list>32);
bpm_cell=reshape(cell_list(ones(nb/32,1),:),nb,1);
steer_cell=reshape(cell_list(ones(ns/32,1),:),ns,1);

shlist=[2 4 6 9 11 13];				% select useful steerers
svlist=[2 4 6 9 11 13];
sorder=reshape(1:ns,ns/periods,periods);
shok=sorder(shlist,:);
svok=sorder(svlist,:);

nhok=prod(size(shok));
nvok=prod(size(svok));

xlist=[1 3 4 6];	% select a subset from useful steerer
sorder=reshape(1:nvok,nvok/periods,periods);
xok=sorder(xlist,:);

h_resp=load_rsp('h');
v_resp=load_rsp('v');
rfreq=-bpm(:,4)/352.2e6/alpha;
remove_off=~strcmp(input('remove off-momentum (y/n): ','s'),'n');
aver_bumps= strcmp(input('averaged bumps (n/y) ? :','s'),'y');

if size(h_resp,2) > 0
   if remove_off
      disp('remove off-momentum');
      h_mod=defreq(h_resp,rfreq);
      [b f pu npu]=bump_coefs(h_mod,bpm(:,1),sext(shok,1),periods,aver_bumps);
      clear h_mod
   else
      [b f pu npu]=bump_coefs(h_resp,bpm(:,1),sext(shok,1),periods,aver_bumps);
   end
   kh=[b' pu-1 npu f];
else
   kh=[];
end

if size(v_resp,2) == 96
   [b f pu npu]=bump_coefs(v_resp,bpm(:,1),sext(svok,1),periods,aver_bumps);
   ks=[b' pu-1 npu f];
   [b f pu npu]=bump_coefs(v_resp(:,xok),bpm(:,1),sext(svok(xok),1),periods,aver_bumps);
   kv=[b' pu-1 npu f];
elseif size(v_resp,2) > 0
   disp(['only ' int2str(size(v_resp,2)) ' V steerers']);
   ks=ones(96,12);
   [b f pu npu]=bump_coefs(v_resp,bpm(:,1),sext(svok(xok),1),periods,aver_bumps);
   kv=[b' pu-1 npu f];
else
   kv=[];
   ks=[];
end


if ~isempty(scaling)
  if (scaling ~= 1)
   kh(:,1:3)=kh(:,1:3)*scaling;
   kv(:,1:3)=kv(:,1:3)*scaling;
   ks(:,1:3)=ks(:,1:3)*scaling;
   h_resp=h_resp/scaling;
   v_resp=v_resp/scaling;
  end
end

if size(v_resp,2) < 96
end

enabled_b=find(all(finite(h_resp)'));	% keep valid BPMs

disabled_b=1:nb;
disabled_b(enabled_b)=[];
disabled_b=enter_disbpm(disabled_b,'srbpmindex',bpm_cell, bpm_num);
enabled_b=1:nb;
enabled_b(disabled_b)=[];

store_svd('h_svd',h_resp,enabled_b,2:nhok,rfreq,alpha*352.2e6);
store_svd('v_svd',v_resp,enabled_b,2:nvok);
store_bump('h_bumps',kh,'SR/ST-H',steer_num(shok),steer_cell(shok));
store_bump('v_bumps',kv,'SR/ST-V',steer_num(svok(xok)),steer_cell(svok(xok)));
store_bump('v_short_bumps',ks,'SR/ST-V',steer_num(svok),steer_cell(svok));
store_variable('matlog','disabled_bpms',[bpm_cell(disabled_b) bpm_num(disabled_b)]);
store_variable('matlog','resp','measured');
