function filpat_set(id, linac_pulses, bunch_list, moulinette)
%FILPAT_SET(ID, LINAC_PULSES, BUNCH_LIST, ROT)    Fills wuth a given filling pattern

if moulinette, rot='DevOpen'; else rot='DevClose'; end
[output,status]=dvcmd(id.bunch_list,'DevWrite',bunch_list(:)');
if status ~= 0; error('Inject:bunchclock','Cannot set bunch list'); end
[output,status]=dvcmd(id.rot,rot);
if status ~= 0; error('Inject:bunchclock','Cannot set pulse rotation'); end
set_linac_pulses(id.linac_pulses, linac_pulses);

function set_linac_pulses(id,delays)
conversion=[1 3 7 15 31];
nb_pulses=length(delays(:));
[output,status]=dvcmd(id(1:nb_pulses+1),'DevSetValue',[conversion(nb_pulses);delays(:)]);
if status ~= 0; error('Inject:linac','Cannot set pulse number'); end
