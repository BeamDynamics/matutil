function varargout = gui_cormatrix(varargin)
% GUI_CORMATRIX MATLAB code for gui_cormatrix.fig
%      GUI_CORMATRIX by itself, creates a new GUI_CORMATRIX or raises the
%      existing singleton*.
%
%      H = GUI_CORMATRIX returns the handle to a new GUI_CORMATRIX or the handle to
%      the existing singleton*.
%
%      GUI_CORMATRIX('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_CORMATRIX.M with the given input arguments.
%
%      GUI_CORMATRIX('Property','Value',...) creates a new GUI_CORMATRIX or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_cormatrix_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_cormatrix_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui_cormatrix

% Last Modified by GUIDE v2.5 14-Nov-2012 15:32:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_cormatrix_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_cormatrix_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before gui_cormatrix is made visible.
function gui_cormatrix_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui_cormatrix (see VARARGIN)

% Choose default command line output for gui_cormatrix
handles.output = [];
opticsdir='sr';
datadir=[];

if(nargin > 3)
    for index = 1:2:(nargin-3),
        if nargin-3==index, break, end
        switch lower(varargin{index})
         case 'opticsdir'
             opticsdir=varargin{index+1};
         case 'datadir'
             datadir=varargin{index+1};
        end
    end
end

[narg,xdata.opticsdir]=getmachenv(opticsdir); %#ok<ASGLU>
if isempty(datadir)
    datadir=xdata.opticsdir;
    hdatadir=fullfile(datadir,'resH.rsp');
    vdatadir=fullfile(datadir,'resV.rsp');
else
    [hdatadir,vdatadir]=deal(datadir);
end

%                   Load response matrices
xdata.h_resp=load_rsp2('h',hdatadir,[-1 1]);
xdata.v_resp=load_rsp2('v',vdatadir,[-1 1]);
%[xdata.f_resp,~]=sr.load_disp(datadir, pm); 
[xdata.f_resp,~]=sr.load_fresp(datadir); 

%                   Look for valid BPMs and steerers
[bpm_ok,sth_ok,stv_ok]=getvalidresp;
% bpm_ok=true(224,1);
% bpm_ok([1 78 224])=false;
% sth_ok([1 23 96])=false;
% stv_ok([1 23 96])=false;
% sth_ok=sth_ok & ~sr.steerselect({'sr/st-h22/c32','sr/st-h19/c31','sr/st-h22/c31'});
% stv_ok=stv_ok & ~sr.steerselect({'sr/st-v22/c32','sr/st-v19/c31','sr/st-v22/c31'});

bpm_ok=bpm_ok & all(isfinite([xdata.h_resp xdata.v_resp xdata.f_resp]),2);

if any(~bpm_ok)
    set(findobj(hObject,'Tag','text2'),'String',sr.bpmname(~bpm_ok));
end
if any(~sth_ok)
    set(findobj(hObject,'Tag','text3'),'String',sr.steername(~sth_ok','SR/ST-H%i/C%i'));
end
if any(~stv_ok)
    set(findobj(hObject,'Tag','text4'),'String',sr.steername(~stv_ok','SR/ST-V%i/C%i'));
end

xdata.bpm_ok=bpm_ok;
xdata.sth_ok=sth_ok;
xdata.stv_ok=stv_ok;
% Update handles structure
handles.xdata=xdata;
guidata(hObject, handles);

% Determine the position of the dialog - centered on the callback figure
% if available, else, centered on the screen
FigPos=get(0,'DefaultFigurePosition');
OldUnits = get(hObject, 'Units');
set(hObject, 'Units', 'pixels');
OldPos = get(hObject,'Position');
FigWidth = OldPos(3);
FigHeight = OldPos(4);
if isempty(gcbf)
    ScreenUnits=get(0,'Units');
    set(0,'Units','pixels');
    ScreenSize=get(0,'ScreenSize');
    set(0,'Units',ScreenUnits);

    FigPos(1)=1/2*(ScreenSize(3)-FigWidth);
    FigPos(2)=2/3*(ScreenSize(4)-FigHeight);
else
    GCBFOldUnits = get(gcbf,'Units');
    set(gcbf,'Units','pixels');
    GCBFPos = get(gcbf,'Position');
    set(gcbf,'Units',GCBFOldUnits);
    FigPos(1:2) = [(GCBFPos(1) + GCBFPos(3) / 2) - FigWidth / 2, ...
                   (GCBFPos(2) + GCBFPos(4) / 2) - FigHeight / 2];
end
FigPos(3:4)=[FigWidth FigHeight];
set(hObject, 'Position', FigPos);
set(hObject, 'Units', OldUnits);

% Make the GUI modal
set(handles.figure1,'WindowStyle','modal')

% UIWAIT makes gui_cormatrix wait for user response (see UIRESUME)
uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = gui_cormatrix_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% The figure can be deleted now
delete(handles.figure1);

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = handles.xdata;

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
uiresume(handles.figure1);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
uiresume(handles.figure1);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end


% --- Executes on key press over figure1 with no controls selected.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Check for "enter" or "escape"
if isequal(get(hObject,'CurrentKey'),'escape')
    uiresume(handles.figure1);
elseif isequal(get(hObject,'CurrentKey'),'return')
    pushbutton1_Callback(hObject, eventdata, handles)
end    
