function next=mtphimod(prev)

global phas avephase
dphi=phas-avephase;
if nargin > 0, next=[prev dphi(:)]; else next=dphi(:);end
clf
plot(next);
axis([1 224 -.05 .05]);
xlabel('BPM number');
ylabel('phase modulation');
grid on
 