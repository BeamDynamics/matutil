function [rg,vmin,vmax]=decays(current,imin,imax)

decay=[0;diff(current)<0];
rg=[find(decay & ~[false;decay(1:end-1)])-1 ...
    find(decay & ~[decay(2:end);false])];
vmin=zeros(size(rg,1),1);
vmax=vmin;
for dec=1:size(rg,1)
   curdec=current(rg(dec,1):rg(dec,2));
   vmin(dec)=min(curdec);
   vmax(dec)=max(curdec);
end
if nargin >= 3
   ok=vmin<=imin & vmax>=imax;
else
   ok=rg(:,2)-rg(:,1)>10;
end
rg=rg(ok,:);
vmin=vmin(ok);
vmax=vmax(ok);
