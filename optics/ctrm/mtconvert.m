function [vx0,aveorbit] = mtconvert(mtpath)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
global plane
persistent fpath

if ischar(plane)
   switch upper(plane)
   case {'H','X'}
	  varname='DDX';
   case {'V','Z'}
	  varname='DDZ';
   otherwise
	  error('MT:plane','plane should be h|H|x|X|0 or v|V|z|Z|1');
   end
else
   suflist='XZ'
   varname=['DD' suflist(plane+1)];
end

if nargin < 1
   	[fname,ftemp]=uigetfile([fpath '*.mat']);
	if ischar(fname)
	   fpath=ftemp;
	   mtpath=fullfile(fpath,fname);
	end
end
s=load(mtpath);
v=s.(varname);
%v=[v(1:end-1,22:end) v(2:end,1:21)];
%v=[v(1:end-1,22:end) v(1:end-1,1:19) v(2:end,20:21)];
v=[v(1:end,22:end) v(1:end,1:21)];
[nturns,nbpms]=size(v); %#ok<NASGU>
aveorbit=mean2(v);
vx0=v-aveorbit(ones(nturns,1),:);
end

