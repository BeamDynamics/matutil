function [corh,maxch,corv,maxcv] = test_cormatrix(varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

args1={'sr',[]};
args1(1:nargin)=varargin;
[narg,opticsdir,mach]=getmachenv(args1{1}); %#ok<ASGLU>
args2={'opticsdir',opticsdir};
if ~isempty(args1{2})
    args2=[args2 {'datadir',args1{2}}];
end

xdata=gui_cormatrix(args2{:});

if ~isempty(xdata)
    resh=store_svd('',xdata.h_resp,xdata.bpm_ok,xdata.sth_ok,xdata.f_resp);
    resv=store_svd('',xdata.v_resp,xdata.bpm_ok,xdata.stv_ok);
    disp(resh);
    disp(resv);

    bpm_list=find(xdata.bpm_ok)';
    sth_list=find(xdata.sth_ok);
    stv_list=find(xdata.stv_ok);
    [corfullH,corh,maxch]=CalCorMatopH(resh.resp2,bpm_list,sth_list,...
        resh.scaledf,resh.scalei);

    [corfullV,corv,maxcv]=CalCorMatopV(resv.resp2,bpm_list,stv_list,...
        1.0,1.0);
end


    function res=store_svd(~,resp,bselect,stlist,fresp,stweight)
        
        % vector 1 : RF dominant
        % vector 2 : sum I dominant
        %
        % inverts a normalized response scaleb*resp*scales
        %
        %Response matrix	:	scaleb^-1*U*S*V'*scales^-1
        % or                    scaleb^-2*base*lambda^2*solve'*scales^-2
        %
        %Correction matrix	:	scales*V*S^-1*U'*scaleb
        % or                    solve(:,1:nmax)*base(:,1:nmax)'
        %
        % corr*resp=I
        
        [nb,nk]=size(resp);
        if nb <= 0, return, end
        
        bpmlist=all([bselect isfinite(resp)],2);	% keep valid BPMs
        
        if nargin > 4
            if nargin > 5
                res.stweight=stweight;
            else
                stweight=ones(1,nk);
            end
            orbits=rms([resp(bpmlist,stlist) fresp(bpmlist)]);
            rmsorbits=mean(orbits(1:end-1));
            rmssteer=rms(stweight(:));
            res.scalei =4*rmsorbits/rmssteer;	% normalize Sigma I
            res.scaledf=  rmsorbits/orbits(end);% normalize frequency resp
            %   respx=    [resp,           fresp;           stweight,0];
            res.resp2=[resp,res.scaledf*fresp;res.scalei*stweight,0]; %expand response matrix
            [nb,nk]=size(res.resp2);
            bpmlist=[bpmlist;true];
            stlist=[stlist true];
            
            [u,s,v]=svd(res.resp2(bpmlist,stlist),'econ');
            
            lambda=diag(s);
            
            [~,id]=sort(abs([v(end,:);sum(v(1:end-1,:))]),2);
            u(end,:)=u(end,:)*res.scalei;		% rescale Sigma I in Amps
            v(end,:)=v(end,:)*res.scaledf;		% rescale freq in dp/p
            
            extract=id(:,end)';
            reorder=1:length(lambda);
            reorder(extract)=[];            % SigmaI: vector 2
            reorder=[extract reorder];		% freq: vector 1
            u=u(:,reorder);
            lambda=lambda(reorder);
            v=v(:,reorder);
        else
            %   respx=resp;
            res.resp2=resp;
            
            [u,s,v]=svd(res.resp2(bpmlist,stlist),'econ');
            
            lambda=diag(s);
            
            %  v=v(:,1:numel(lambda)); %rajoute par TP, not necessary with 'econ' flag
            
        end
        nvects=length(lambda);
        vv=zeros(nk,nvects);
        vv(stlist,:)=v;
        res.svdsolve2=vv*diag(1./lambda);
        res.svdbase2=zeros(nb,nvects);
        res.svdbase2(bpmlist,:)=u;
        res.lambda=lambda;
        
        function a=rms(x)
            a=sqrt(sum(x.*x)./size(x,1));
        end
    end

end

