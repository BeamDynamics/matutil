function [fname,finfo]=scanfile
%[FNAME,FINFO]=SCANFILE		Checks for valid FBPM archive files
%% 
% FNAME	:	file name
% FINFO	:	[date current Global ID14 ID21 ID24]
%
% for feedback state : 1=OFF 2=ON 23=FAULT ...

!ls -1rt | grep '.*-2000_.*\.mat' >bid
fname=textread('bid','%s');

sz=length(fname);
finfo=zeros(sz,6);

for i=1:sz
   finfo(i,:)=epfile(fname{i});
end
