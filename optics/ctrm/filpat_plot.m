function h=filpat_plot(linac_pulses, bunch_list)

[a,b]=ndgrid(linac_pulses',bunch_list);
w=zeros(992,1);
w(a+b+1)=1;
h=bar(w);
axis([0 992 0 1.5]);
