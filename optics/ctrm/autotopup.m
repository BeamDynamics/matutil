% topup
% Matlab function to maintain the current to a given value
% Injection every 10 mn
% manage BPSS,LINAC



period = input('Period in seconds[Typ 600]:');

current_ref=input('Beam intensity : ');
clean=strcmpi(input('Do cleaning (n/y) :','s'),'y');

if current_ref < 5 || current_ref > 305, error('Invalid value');end
uiwait(warndlg({'Check:',...
  '- Linac ON with beam OFF',...
  '- SYEXT ON',...
  '- Scaper jaw SR/D-SCR4/JAW-INT open at 40'},'Topup','modal'));
current_min_hist=[];
current_max_hist=[];
injection_time_hist=[];

%id.scraper=dvopen('SR/D-SCR4/JAW-INT');
%dvset(id.scraper, 40);
%disp('Waiting for scraper...');
%pause(100);
%disp('Ready !');


tic;
waittime=200;
while true
    while toc < (waittime-180)
        pause (0.1);
    end
    disp('Prepare Injector');
        switchinjector(2)
        disp('Injector Ready, wait stabilization');
        
    while toc < (waittime)
        pause (0.1);
    end
    
    tic;
    try
        disp('Starting injection');
        [cur1,cur2]=doinject(current_ref);
        switchinjector(12)
        if clean
            disp('>>Start cleaning');
            docleaning('doall')
            disp('>>End cleaning');
        end
        disp('Stopping injection');
    catch
        err=lasterror;
	disp(err.message);
        disp('Injection failed, loop stoped');
        break;
    end
    current_min_hist=[current_min_hist;cur1];
    current_max_hist=[current_max_hist;cur2];
    injection_time_hist=[injection_time_hist;toc];
    subplot(2,1,1);
    plot([current_min_hist current_max_hist]);
    title('current min/max');
    subplot(2,1,2);
    plot(injection_time_hist);
    title('injection time');
    %switchinjector(12)
    waittime=period;
end
