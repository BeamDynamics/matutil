function exits = update_cormatrix(varargin)
%UPDATE_CORMATRIX       Updates the autocor correction matrices
%
%UPDATE_CORMATRIX(OPTICSDIR,DATADIR)
%
%OPTICSDIR: Directory containing the model optics and where results will be
%           stored (default: $APPHOME/sr/optics/settings/theory)
%DATADIR:   Directory containing the response matrices
%           (default: opticsdir/resp[HV].rsp
%
%   The BPM and steerer validity is taken from the state of the device
%
%UPDATE_CORMATRIX(...,BPMOK,SHOK,SVOK)
%
%BPMOK:     Valid BPMs (logical 224x1)
%SHOK:      Valid horizontal steerers (logical 1x96)
%SVOK:      Valid horizontal steerers (logical 1x96)
%

% n2=[];
% for n=1:nargin
%     if islogical(varargin{n}),n2=n; break; end
% end
% n2=find(cellmap(varargin,@islogical),1,'first');
% if isempty(n2), n2=nargin+1; end
% args1=[varargin(1:n2-1) cell(1,3-n2)];
% args2=[varargin(n2:end) cell(1,2+n2-nargin)];
% [opticsdir,datadir]=deal(args1{:});
% [bpm_ok,sth_ok,stv_ok]=deal(args2{:});

global APPHOME

args1={'sr',[]};
args1(1:nargin)=varargin;
[narg,opticsdir,mach]=getmachenv(args1{1}); %#ok<ASGLU>
args2={'opticsdir',opticsdir};
if ~isempty(args1{2})
    args2=[args2 {'datadir',args1{2}}];
end

xdata=gui_cormatrix(args2{:});

if ~isempty(xdata)
    tango.Device('sr/svd-orbitcor/auto').Off();
    [resh,resv]=compute_srcor('sr',xdata.opticsdir,xdata.opticsdir,'measured',...
        xdata.h_resp,xdata.v_resp,xdata.f_resp,...
        xdata.bpm_ok,xdata.sth_ok,xdata.stv_ok);
    disp(resh);
    disp(resv);

    bpm_list=find(xdata.bpm_ok)';
    sth_list=find(xdata.sth_ok);
    stv_list=find(xdata.stv_ok);
    tnow=time_label();
    [corfullH,corh,maxch]=CalCorMatopH(resh.resp2,bpm_list,sth_list,...
        resh.scaledf,resh.scalei);
    [Mcorh,err]=SetCorMatX(corh);
    err=SetMatGainH(maxch);
    hfile=fullfile(APPHOME,'FPGAconfig',['MCorH' tnow]);
    try
        save(hfile,'corfullH','bpm_list','sth_list','maxch');
    catch
        warning('UpdateCormatrix:CannotWrite',['Cannot write ' hfile '.']);
    end

    [corfullV,corv,maxcv]=CalCorMatopV(resv.resp2,bpm_list,stv_list,...
        1.0,1.0);
    [Mcorv,err]=SetCorMatZ(corv);
    err=SetMatGainV(maxcv);
    vfile=fullfile(APPHOME,'FPGAconfig',['MCorV' tnow]);
    try
        save(vfile,'corfullV','bpm_list','stv_list','maxcv');
    catch
        warning('UpdateCormatrix:CannotWrite',['Cannot write ' vfile '.']);
    end

    try
        tango.Device('sr/svd-orbitcor/h').Init();
    catch
        warning('UpdateCormatrix:CannotInit','Cannot reinitialize sr/svd-orbitcor/h');
    end
    try
        tango.Device('sr/svd-orbitcor/v').Init();
    catch
        warning('UpdateCormatrix:CannotInit','Cannot reinitialize sr/svd-orbitcor/v');
    end
    exits=0;
else
    exits=1;
end
end
