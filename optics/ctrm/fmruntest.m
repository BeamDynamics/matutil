function code=fmrun(xk,yk,dpath)
% CODE=FMRUN(HKICKER,VSHAKER,DIRNAME)	performs a MT map measurement
%
% HKICKER :	array of H kicker values
% VSHAKER :	array of V shaker values
% DIRNAME : 	Directory where MT files will be saved
%
% CODE :	1= beam lost
%		2= kicker failure
%		9= MT timeout (sequence blocked)
%		23= MT Fault

global DEVS

if isempty(DEVS)
   DEVS=dvopen('SR/PS-K/1','SR/D-TM/NEW','SR/MTOUR/UP')
end

[xf,yf]=ndgrid(xk,yk);
code=1;
[s,bid]=unix('sr_test 2>/dev/null');
%if bitand(s,2) ~= 0, return; end
save([dpath '/currents.mat'],'xk','yk');

for j=1:size(xf,2)
dvcmd(DEVS(1),'DevStandby');
pause(5);
for i=1:size(xf,1)
   fname=[dpath '/X' num2str(xf(i,j),'%04.0f') 'Z' num2str(100*yf(i,j),'%04.0f')];
   if exist([fname 'h.mat'],'file') ~= 2 | exist([fname 'v.mat'],'file') ~= 2
      dvcmd(DEVS(1),'DevSetValue',xf(i,j))		% set currents
      vals=dvcmd(DEVS(2),'DevGetStrength');
      dvcmd(DEVS(2),'DevSetStrength',[vals(1) yf(i,j)]);
      pause(2);
      dvcmd(DEVS(1),'DevOn');
      pause(3);

%     code=mtrun(DEVS(3),200)				% start measurement
%     if ismember(code,[9 23]), break, end		% check measurement
      disp('MT measurement');
      pause(3);
      
      code=2;						% check currents
      kstate=dvcmd(DEVS(1),'DevState');
      if kstate == 2, code=0; end
      if code ~= 0, break, end
      dvcmd(DEVS(1),'DevStandby');

      code=1;						% check beam
      [s,bid]=unix('sr_test 2>/dev/null');
      if bitand(s,2) == 0, code=0; end
      code=0;
      if code ~= 0, break, end

      for plane='hv'
         [vx,aveorbit]=load_mtour('test',plane);	% load data
         [first,coher]=disp1000(aveorbit,vx);
         save([fname plane],'vx','aveorbit','plane')	% save file
      end
      eval(['!say ''horizontal ' num2str(xf(i,j)) ', vertical ' num2str(yf(i,j)) ' done'''])
   end
end
dvcmd(DEVS(1),'DevOff');
pause(2);
end
!say 'measurement finished'

vals=dvcmd(DEVS(2),'DevGetStrength');			% set shaker to 0
dvcmd(DEVS(2),'DevSetStrength',[vals(1) 0]);
