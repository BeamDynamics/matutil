function [tims,vals]=procfile(fname,fdate,plane,range)
%[TIMES,VALS]=PROCFILE(FNAME,FDATE.RANGE)	Processes FBPM archive files
%
% Averages rms values over 24h periods for all valid FBPM archive files
%
% TIMES	:	date of each record
% VALS	:	34 time domain rms + 34 freq. domain
% RANGE	:	frequency range (defaults to 2:4 = 4.4,8.8,13.2 Hz)

if nargin < 4
   range=2:4;
end
first=floor(fdate(1));
last=ceil(fdate(end));
tims=(first:last-1)'+datenum('07:00:00');
vals=NaN*ones(length(tims),68);

for k=1:length(tims)

   today=find((fdate >= tims(k)) & (fdate < tims(k)+1));
   ntoday=length(today)
   if ntoday >= 12
=      v=NaN*ones(ntoday,68);
      for i=1:ntoday
         [info,vtime,vfreq]=epfile(fname{today(i)},range);
	 if strcmp(plane,'h')
	 xy=[reshape(vtime(:,1:2)',1,34) reshape(vfreq(:,1:2)',1,34)];
	 if info(4) ~= 2, xy(13+[0 1 34 35])=NaN;end
	 if info(5) ~= 2, xy(21+[0 1 34 35])=NaN;end
	 if info(5) ~= 2, xy(25+[0 1 34 35])=NaN;end
	 else
	 xy=[reshape(vtime(:,3:4)',1,34) reshape(vfreq(:,3:4)',1,34)];
	 end
         xy(xy > 0.02)=NaN;
	 v(i,:)=xy;
      end
      for i=1:68
        col=v(finite(v(:,i)),i);
        nn=length(col);
        if nn > 0
        vals(k,i)=sqrt(sum(col.*col)/nn);
        end
      end
   end
end
