nu=14.39;
[respv,bpm,steerer]=respmat('sr','sv',nu);
respxv=responsexm(bpm(:,2:3),steerer(:,2:3),nu);
blist=bumpbpmlist(bpm,steerer);

[rv,bpm,hilo]=respmat('sr','hilov',nu);
rxv=responsexm(bpm(:,2:3),hilo(:,2:3),nu);

bnum=6;
bok=logical(ones(224,1));
bok(blist{bnum})=logical(0);
cor=[respv(bok,bnum) respxv(bok,bnum)]\orbit(bok);

cell=6;
cnum=mod(cell-4,32)+1;
%bnum=reshape([87:96 1:86],3,32);
bnum=reshape([96 1:95],3,32);
bok=logical(ones(224,1));
bok([blist{bnum(1,cnum)};blist{bnum(2,cnum)}])=logical(0);
cor=[rv(bok,cnum) rxv(bok,cnum)]\orbit2(bok);
