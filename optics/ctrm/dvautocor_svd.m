function [kicks,orbitfit] = dvautocor_svd(orbit, plane, nk, nv)
%DVAUTOCOR_SVD Compute SVD correction
%   [KICKS,ORBITFIT]=DVAUTOCOR_SVD(ORBIT,PLANE,NK,NVECTS)
%
%ORBIT:     orbit to be analysed [m]
%PLANE:     orbit plane
%           'h', 'H', 'x', 'X', 1 => horizontal
%           'v', 'V', 'z', 'Z', 2 => vertical
%NK:        Maximum number of steerers to use (default: all steerers)
%NVECTS:    Number of eigen vectors (default: all vectors)
%
%KICKS:     Steerer kicks
%ORBITFIT:  Orbit fitted with the computed correction
%
%See also: dvautocor_mosteff

global APPHOME

hv='hv';
hvlabel={'HORIZONTAL','VERTICAL'};
if ischar(plane)
   switch upper(plane)
   case {'H','X'}
	  plane=1;
   case {'V','Z'}
	  plane=2;
   otherwise
	  error('Orbit:plane','plane should be h|H|x|X|1 or v|V|z|Z|2');
   end
end

stform=['sr/st-' hv(plane) '%i/c%i'];
ok=isfinite(orbit(:));
a=load([APPHOME '/sr/optics/settings/theory/' hv(plane) '_svd.mat']);
transfer=a.svdbase*diag(a.lambda.*a.lambda)*a.svdsolve';

[u,s,v]=svd(transfer(ok,1:96),0);
lambda=1./diag(s);
if nargin<4, nv=length(lambda); end
lambda(nv+1:end)=0;
kicks=v*(lambda.*(u'*orbit(ok)));
[ksort,iksort]=sort(-abs(kicks));

if (nargin < 3) || isempty(nk), nk=length(lambda); end
if nk < length(lambda)
   [u,s,v]=svd(transfer(ok,iksort(1:nk)),0);
   lambda=1./diag(s);
   kicks=zeros(96,1);
   kicks(iksort(1:nk))=v*(lambda.*(u'*orbit(ok)));
   for n=1:nk
	  fprintf('%s: %g\n', srsteername(iksort(n),stform), kicks(iksort(n)));
   end
end
orbitfit=transfer(1:224,1:96)*kicks;
residual=std2(orbit-orbitfit);
fprintf('maximum kick on %s: %g\nRms. orbit: %g\nResidual: %g\n',...
    srsteername(iksort(1),stform),kicks(iksort(1)), std2(orbit), residual);

sbpm=(0.5+(0:223)')/224;
sticks=linspace(0,1,17);
slabels=[4:2:32 2 4];
subplot(2,1,1);
plot(sbpm, [orbit(:) orbitfit]);
ax=axis;
axis([0 1 ax(3:4)]);
set(gca,'XTick',sticks,'XTickLabel',slabels);
xlabel('Cell');
ylabel('orbit');
title(hvlabel{plane});
grid on
legend('measured',sprintf('reconstructed with %i vectors', nv));

skick=(0.5+(0:95)')/96;
subplot(2,1,2);
bar(skick,kicks);
ax=axis;
axis([0 1 ax(3:4)]);
set(gca,'XTick',sticks,'XTickLabel',slabels);
xlabel('Cell');
ylabel('Strength');
text(0.01,ax(3)+0.06*(ax(4)-ax(3)), sprintf('maximum kick on %s: %g',...
    srsteername(iksort(1),stform),kicks(iksort(1))));
