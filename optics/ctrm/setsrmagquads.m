function ring=setsrmagquads(ring,fname)

qp=atgetcells(ring,'Class','Quadrupole');
qpl=sr.fold(atgetfieldvalues(ring(qp),'Length'));
kl=zeros(8,32);

f1=fopen(fname,'rt');
c=textscan(f1,'%s%f');
[qpnames,qpvals]=deal(c{:});
fclose(f1);
kl(1,:)=getgl('SR/PS-QF2/0');
kl(2,:)=getgl('SR/PS-QD3/0');
kl([3 6],:)=getgl('SR/PS-QD4/0');
kl(4:5,:)=getgl('SR/PS-QF5/0');
kl(7,:)=getgl('SR/PS-QD6/0');
kl(8,:)=getgl('SR/PS-QF7/0');
kl(5,19)=getgl('SR/PS-QF5/C22');
kl(6,19)=getgl('SR/PS-QD4/C22');
kl(7,19)=getgl('SR/PS-QD6HG/C22');
kl(8,19)=getgl('SR/PS-QF7HG/C22');
kl(8,20)=getgl('SR/PS-QF7HG/C23');
kl(7,20)=getgl('SR/PS-QD6HG/C23');
kl(6,20)=getgl('SR/PS-QD4/C23');
kl(5,20)=getgl('SR/PS-QF5/C23');
k=kl./qpl/20.13;
ring(qp)=atsetfieldvalues(ring(qp),'PolynomB',{2},sr.unfold(k));

    function gl = getgl(name)
        current=qpvals(find(cellfun(@(nm) ...
            ~isempty(strfind(nm,name)),qpnames),1));
        gl=i2gl(name,current);
    end
end
