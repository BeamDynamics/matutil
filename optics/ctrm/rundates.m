function out = rundates(nums,varargin)
%RUNDATES retrieves information on run dates
%
%D=RUNDATES(RUNNAME)
%  RUNNAME: string as 'run15-1' or 'run15_1' or '15-1'
%           numeric array as [15 1] or [2015 1]
%
%D=RUNDATES(YEAR,ORD)
%  YEAR:
%  ORD:     run index 1 to 5
%
%D=RUNDATES(_,'base',db) uses the database db to get the dates
%
%RUNDATES returns a cell array of strings appropriate for LOAD_HDB
%           {'begin_date' 'end_date' 'archive_name'}

global APPHOME
persistent runbase

if isempty(runbase)
    load(fullfile(APPHOME,'run','runbase'));
end
narg=1;
if ischar(nums)
    a=regexp(nums,'\D*(\d*)[-_](\d*)','tokens');
    nums=str2double(a{1});
elseif isscalar(nums)
        nums=[nums varargin{narg}];
        narg=narg+1;
end
db=getoption(varargin(narg:end),'base',runbase);
if nums(1)>=2000
    nums(1)=nums(1)-2000;
elseif nums(1)>=1900
    nums(1)=nums(1)-1900;
end
fd=sprintf('run%02d_%d',nums(1),nums(2));
v=db.(fd);
if datenum(v{2}) > now-365
    v{3}='libout';
end
out=v;
end

