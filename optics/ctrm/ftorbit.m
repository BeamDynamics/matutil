function [ aveorbit,stdorbit,datav ] = ftorbit(nave, turn, suppress)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

persistent kicker

if isempty(kicker)
    kicker=dvopen('tango:sy/ps-ke/1','tango:sy/ps-ke/1/current');
end

datav=NaN(nave,224);
bpmsok=true(1,224);
bpmsok(srbpmindex(suppress))=false;
bpmskeep=bpmsok;
bpmskeep([1 2 end-2 end-1])=false;
for acq=1:nave
    dvcmd(kicker(1), 'DevOn');
    pause(3);
    [vx0h,vx0v,intens]=load_abcd();
    trns=isbeam(intens,0.1);
    disp(find(trns));
    ocentre=mean2(vx0v(turn,bpmskeep));
    datav(acq,bpmsok)=vx0v(turn,bpmsok)-ocentre;
%   ocentre=mean2(vx0h(turn,bpmskeep));
%   datav(acq,bpmsok)=vx0h(turn,bpmsok)-ocentre;
end

aveorbit=mean2(datav);
stdorbit=std2(datav);

function trns=isbeam(intens,threshold)
intave=sum2(intens,2);
intmax=max(intave);
trns=(intave>threshold*intmax);
end

end