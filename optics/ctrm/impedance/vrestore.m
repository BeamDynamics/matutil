function vrestore
%VRESTORE		Resets steerer currents saved by impedance measurement

idv=dvopensteer(1);
load vset
dvsetsteer(idv,vset);
