function [Zres,rms,cor,current]=idvz(cell,ampl,ncycle,navg,nstep)
%IDVZ(CELL,AMPL,SYMM,CYCLES,BPMAVG,NSTEPS)	measure a bumped orbit
%
% CELL   : cell number (1:32)
% AMPL   : bump amplitude (m)
% SYMM   : bump symmetry : 1 => ++, -1 => +-
% CYCLES : number of bump cycle (to cancel hysteresis) (default 3)
% BPMAVG : averaging number for BPMs (default 5)
% NSTEPS : number of steps when rising the bump (default 2)

global BPMDEV SRA VSTEER CTDEV
if isempty(BPMDEV)
   BPMDEV=dvopen('tango:SR/D-BPMLIBERA/ALL');
end
if isempty(SRA)
   SRA=dvopen('+sr/autocor/1');
end
if isempty(VSTEER)
   VSTEER=dvopensteer(1);
end
if isempty(CTDEV)
   CTDEV=dvopen('tango:sr/d-ct/1/current');
end

if nargin < 6, nstep=2; end
if nargin < 5, navg=5; end
if nargin < 4, ncycle=3; end

nu=14.288;
[respv,bpm,steerer]=respmat('sr','sv',nu);
[rv,bpm,hilo]=respmat('sr','idv',nu);
blist=bumpbpmlist(bpm,steerer);
rxv=responsexm(bpm(:,2:3),hilo(:,2:3),nu);
cnum=mod(cell-4,32)+1;
bnum=reshape([96 1:95],3,32);
bok=true(224,1);
bok([blist{bnum(1,cnum)};blist{bnum(2,cnum)}])=false;

[vset,vread]=dvreadsteer(VSTEER);
save vset vset;

dvcmd(SRA,'DevOff');

Zres=zeros(224,1);
rms=zeros(ncycle,1);
cor=zeros(ncycle,2);
current=0;
Zprev=NaN*ones(224,1);

amplim=min(ampl,0.0005);
dvsetparam(BPMDEV, 'BpmAverage', navg);
dvsetparam(BPMDEV, 'BpmIgnoreIncoherence', 'on');
for j=0:ncycle
%  setbump(SRA,[],[],bnum(1:2,cnum),ampl,nstep)
   settablebump(SRA,[],[],bnum(1:2,cnum),ampl,nstep)
   Zplus=zorbit(BPMDEV);
%  setbump(SRA,[],[],bnum(1:2,cnum),-2*ampl,2*nstep)
   settablebump(SRA,[],[],bnum(1:2,cnum),-2*ampl,2*nstep)
%  settablebump(SRA,[],[],bnum(1:2,cnum),-ampl-amplim,2*nstep)
   [iset,iread]=dvread(CTDEV);
   Zminus=zorbit(BPMDEV);
%  setbump(SRA,[],[],bnum(1:2,cnum),ampl,nstep)
   settablebump(SRA,[],[],bnum(1:2,cnum),ampl,nstep)
%  settablebump(SRA,[],[],bnum(1:2,cnum),amplim,nstep)
   Zbump=Zplus-Zminus;
   Zdif=Zbump-Zprev;
   bok2=bok&isfinite(Zdif);
   if j>0
      Zres=Zres+Zbump;
      current=current+iread;
      cor(j,:)=([rv(bok2,cnum) rxv(bok2,cnum)]\Zdif(bok2))'
      rms(j)=std2(Zdif)
   end
   plot(1:224,Zbump);
   hold on

   Zprev=Zbump;
end
Zres=Zres/ncycle;
current=current/ncycle;
plot(1:224,Zres,'r');
hold off
!say 'end of measurement'

fn=input('File name: ','s');
if ~isempty(fn)
   save(fn,'Zres','current','cell');
end

function zz=zorbit(dev)
[xx,zz]=dvreadbpm(dev,'wait');
