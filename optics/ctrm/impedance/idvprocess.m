function [cor,orbit,fit]=idvprocess(cell)

funi=input('Uniform file name: ','s');
suni=load(funi);
fsingle=input('Single file name:','s');
ssingle=load(fsingle);
if isfield(ssingle,'cell'), cell=ssingle.cell;end
if ~exist('cell','var'), cell=input('cell: ');end
disp(['Current: ' num2str(ssingle.current) ' mA']);
orbit=ssingle.Zres-suni.Zres;
[cor,fit]=idkick(cell,orbit);
