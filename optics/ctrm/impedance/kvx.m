function [Zres,rms,current]=kvx(stlist,ampl,ncycle,navg)
%KVX(STEERER,AMPL,CYCLES,BPMAVG)	measure a bumped orbit
%
% STEERER: list of steerer numbers (1:96)
% AMPL   : steerer strength (A)
% CYCLES : number of bump cycle (to cancel hysteresis) (default 3)
% BPMAVG : averaging number for BPMs (default 5)

global BPMDEV SRA HSTEER CTDEV
if isempty(BPMDEV)
   BPMDEV=dvopen('+sr/bpm/master_up');
end
if isempty(SRA)
   SRA=dvopen('+sr/autocor/1');
end
if isempty(HSTEER)
   HSTEER=dvopensteer(0);
end
if isempty(CTDEV)
   CTDEV=dvopen('+sr/d-ct/1');
end

if nargin < 4, navg=5; end
if nargin < 3, ncycle=3; end

[hset,hread]=dvreadsteer(HSTEER);
save hset hset;

dvcmd(SRA,'DevOff');

fn=input('File name: ','s');

for steerer=stlist
   dset=zeros(size(hset));
   dset(steerer)=ampl;
   Zres=zeros(224,1);
   rms=NaN*ones(ncycle,1);
   Zprev=NaN*ones(224,1);
   current=0;

   dvsetparam(BPMDEV, 'BpmAverage', navg);
   dvsetparam(BPMDEV, 'BpmIgnoreIncoherence', 'on');
   disp(['Powering steerer ' int2str(steerer)]);
   for j=1:ncycle
      dvsetsteer(HSTEER, hset+dset);
      pause(3);
      Zplus=xorbit(BPMDEV);
      dvsetsteer(HSTEER, hset);
      pause(3);
      dvsetsteer(HSTEER, hset-dset);
      pause(3);
      [iset,iread]=dvread(CTDEV);
      Zminus=xorbit(BPMDEV);
      dvsetsteer(HSTEER, hset);
      pause(3);
      Zbump=Zplus-Zminus;
      Zres=Zres+Zbump;
      current=current+iread;
      if j > 1, rms(j)=std2(Zbump-Zprev), end
      Zprev=Zbump;
   end
   Zres=Zres/ncycle;
   current=current/ncycle;
   plot(1:224,Zres);

   if ~isempty(fn)
      save([fn num2str(steerer,'%02i')],'Zres','current','steerer');
   end
end

function xx=xorbit(dev)
[xx,zz]=dvreadbpm(dev,'wait');
