function [Zres,current,rms]=bumploop(fcode,blist,ampl,ncycle,navg,nstep)
%BUMPLOOP(FCODE,BUMPLIST,AMPL,CYCLES,BPMAVG,NSTEPS)	measure bumped orbits
%
% FCODE    : file name root
% BUMPLIST : cell number (1:32)
% AMPL     : bump amplitude (m) (default 0.001)
% CYCLES   : number of bump cycle (to cancel hysteresis) (default 3)
% BPMAVG   : averaging number for BPMs (default 5)
% NSTEPS   : number of steps when rising the bump (default 3)

global BPMDEV SRA VSTEER CTDEV STOPIT
if isempty(BPMDEV)
   BPMDEV=dvopen('+sr/bpm/master_up');
end
if isempty(SRA)
   SRA=dvopen('+sr/autocor/1');
end
if isempty(VSTEER)
   VSTEER=dvopensteer(1);
end
if isempty(CTDEV)
   CTDEV=dvopen('+sr/d-ct/1');
end

if nargin < 6, nstep=3; end
if nargin < 5, navg=5; end
if nargin < 4, ncycle=3; end
if nargin < 3, ampl=0.001; end

%nu=14.288;
%[respv,bpm,steerer]=respmat('sr','sv',nu);
%blist=bumpbpmlist(bpm,steerer);
bnum=[[96 1:95];1:96]';

[vset,vread]=dvreadsteer(VSTEER);
save vset vset;
dvsetparam(BPMDEV, 'BpmAverage', navg);
dvsetparam(BPMDEV, 'BpmIgnoreIncoherence', 'on');

dvcmd(SRA,'DevOff');

STOPIT=0;
hstop=warndlg('Cancel measurement ?','Cancel');
set(hstop,'DeleteFcn','global STOPIT;STOPIT=1');
for bump=blist
   Zres=zeros(224,1);
   rms=zeros(1,ncycle);
   current=0;
   Zprev=NaN*ones(224,1);
%  bok=logical(ones(224,1));
%  bok([blist{bnum(bump,1)};blist{bnum(bump,2}])=logical(0);

   for j=1:ncycle
      disp(['Bump ' int2str(bump) ', cycle ' int2str(j)]);
      setbump(SRA,[],[],bnum(bump,:),ampl,nstep);
%     setbump(SRA,bnum(bump,:),ampl,[],[],nstep);
      Zplus=zorbit(BPMDEV);
      setbump(SRA,[],[],bnum(bump,:),-2*ampl,2*nstep);
%     setbump(SRA,bnum(bump,:),-2*ampl,[],[],2*nstep);
      [iset,iread]=dvread(CTDEV);
      Zminus=zorbit(BPMDEV);
      setbump(SRA,[],[],bnum(bump,:),ampl,nstep);
%     setbump(SRA,bnum(bump,:),ampl,[],[],nstep);
      Zbump=Zplus-Zminus;
      Zdif=Zbump-Zprev;
%     bok2=bok&isfinite(Zdif);
      if j>1, rms(j)=std2(Zdif), end
      Zres=Zres+Zbump;
      current=current+iread;

      Zprev=Zbump;
      if STOPIT, break; end
   end
   if STOPIT, break; end
   Zres=Zres/ncycle;
   current=current/ncycle;
   plot(1:224,Zres);

   fn=sprintf([fcode '%i'],bump);
   save(fn,'Zres','current','bump');
end
delete(hstop);

function zz=zorbit(dev)
[xx,zz]=dvreadbpm(dev,'wait');
