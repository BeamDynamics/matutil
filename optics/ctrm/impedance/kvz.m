function [Zres,rms,current]=kvz(stlist,ampl,ncycle,navg)
%KVZ(STEERER,AMPL,CYCLES,BPMAVG)	measure a bumped orbit
%
% STEERER: list of steerer numbers (1:96)
% AMPL   : steerer strength (A)
% CYCLES : number of bump cycle (to cancel hysteresis) (default 3)
% BPMAVG : averaging number for BPMs (default 5)

global BPMDEV SRA VSTEER CTDEV
if isempty(BPMDEV)
   BPMDEV=dvopen('tango:SR/D-BPMLIBERA/ALL');
end
if isempty(SRA)
   SRA=dvopen('+sr/autocor/1');
end
if isempty(VSTEER)
   VSTEER=dvopensteer(1);
end
if isempty(CTDEV)
   CTDEV=dvopen('tango:sr/d-ct/1/current');
end

if nargin < 4, navg=5; end
if nargin < 3, ncycle=3; end

[vset,vread]=dvreadsteer(VSTEER);
save vset vset;

dvcmd(SRA,'DevOff');

fn=input('File name: ','s');

try
   load([fn '_errors']);
   nr=min(size(measerr,1),ncycle);
catch
   measerr=NaN*ones(ncycle,96);
   nr=ncycle;
end

for steerer=stlist
   dset=zeros(size(vset));
   dset(steerer)=ampl;
   Zres=zeros(224,1);
   rms=NaN*ones(ncycle,1);
   Zprev=NaN*ones(224,1);
   current=0;

   dvsetparam(BPMDEV, 'BpmAverage', navg);
   dvsetparam(BPMDEV, 'BpmIgnoreIncoherence', 'on');
   disp(['Powering steerer ' int2str(steerer)]);
   for j=0:ncycle
      dvsetsteer(VSTEER, vset+dset);
      pause(5);
      Zplus=zorbit(BPMDEV);
      dvsetsteer(VSTEER, vset);
      pause(2);
      dvsetsteer(VSTEER, vset-dset);
      pause(5);
      [iset,iread]=dvread(CTDEV);
      Zminus=zorbit(BPMDEV);
      dvsetsteer(VSTEER, vset);
      pause(2);
      Zbump=Zplus-Zminus;
      if j > 0
         Zres=Zres+Zbump;
         current=current+iread;
         rms(j)=std2(Zbump-Zprev)
      end
      Zprev=Zbump;
   end
   measerr(1:nr,steerer)=rms(1:nr);
   Zres=Zres/ncycle;
   current=current/ncycle;
   plot(1:224,Zres);
   if ~isempty(fn)
      save([fn num2str(steerer,'%02i')],'Zres','current','steerer','rms');
      save([fn '_errors'],'measerr');
   end
end
!say 'end of measurement'
slist=find(all(finite(measerr)));
disp([slist;measerr(:,slist)]);

function zz=zorbit(dev)
[xx,zz]=dvreadbpm(dev,'wait');
