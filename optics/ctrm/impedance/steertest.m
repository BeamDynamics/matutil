function res=steertest

idh=dvopensteer(0);
idv=dvopensteer(1);
[kh,bid]=dvreadsteer(idh);
[kv,bid]=dvreadsteer(idv);
res=[-1.41*kh -kh-kv kh-kv];
