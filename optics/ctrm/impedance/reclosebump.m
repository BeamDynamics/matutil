function Zres=reclosebump(fname)
%RECLOSEBUMP(FNAME)	close an orbit bump
%
% FNAME  : filename

global BPMDEV SRA VSTEER BVTABLE
if isempty(BVTABLE)
%  BVTABLE=load_bump([getenv('APPHOME') '/sr/optics/settings/theory/v_short_bumps']);
   error('SetBump:Error','No calibration table.');
end

if nargin < 1, fname=input('file: ','s'); end
load(fname);
if exist('bnum') ~= 1
   cell=input('cell: ');
   id=input('id: ');
   bnum=3*mod(cell-4,32)+id;
end
if exist('ampl') ~= 1, ampl=input('ampl: '); end
nu=input('nu: ');
[respv,bpm,steerer]=respmat('sr','sv',nu);

coef=load_corcalib('sr','v');
coef=reshape(coef(ones(16,1),:)',1,96);

kicks=banal3(bnum,-Zres,respv,bpm,steerer);
kick2=banal7(bnum,-Zres,respv,bpm,steerer);
%plot([Zres Zres+respv*kicks Zres+respv*kick2]);
plot([Zres Zres+respv*kicks]);
bcof(1,:)=BVTABLE(bnum,:);
bcof(2,:)=bcof(1,:)+kicks'./coef/2/ampl;
%bcof(3,:)=bcof(1,:)+kick2'./coef/2/ampl;
bcof

BVTABLE(:,bnum)=BVTABLE(:,bnum)+kicks./coef'/2/ampl;

fn=input('File name (bump calibration): ','s');
if ~isempty(fn)
   save(fn,'BVTABLE');
end

function kick=banal3(bnum,orbit,respv,bpm,steerer)
[nb,nk]=size(respv);
blist=bumpbpmlist(bpm,steerer);
bok=finite(orbit);
bok(blist{bnum})=false;
kok=[nk 1:nk-1;1:nk;2:nk 1];
kok=kok(:,bnum);
kick=zeros(nk,1);
kick(kok)=respv(bok,kok)\orbit(bok);

function kick=banal7(bnum,orbit,respv,bpm,steerer)
[nb,nk]=size(respv);
blist=bumpbpmlist(bpm,steerer);
bok=finite(orbit);
bok(blist{bnum})=false;
kok=[nk 1:nk-1;1:nk;2:nk 1];
kok=kok(:,bnum);
kick=zeros(nk,1);
[u,s,v]=svd(respv(bok,kok),0);
kick(kok)=v*inv(s)*u'*orbit(bok);

function zz=zorbit(dev)
[xx,zz]=dvreadbpm(dev,'wait');
