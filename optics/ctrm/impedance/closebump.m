function Zres=closebump(cell,id,ampl,ncycle,navg,nstep)
%CLOSEBUMP(CELL,ID,AMPL,CYCLES,BPMAVG,NSTEPS)	close an orbit bump
%
% CELL   : cell number (1:32)
% ID     : bump identification (1:3)
% AMPL   : bump amplitude (m) (default 0.001)
% CYCLES : number of bump cycle (to cancel hysteresis) (default 2)
% BPMAVG : averaging number for BPMs (default 5)
% NSTEPS : number of steps when rising the bump (default 2)

global BPMDEV SRA VSTEER BVTABLE CALINFO
if (isempty(BVTABLE) || isempty(CALINFO))
%  BVTABLE=load_bump([getenv('APPHOME') '/sr/optics/settings/theory/v_short_bumps']);
   error('SetBump:Error','No calibration table.');
end
if isempty(BPMDEV)
   BPMDEV=dvopen('tango:SR/D-BPMLIBERA/ALL');
end
if isempty(SRA)
   SRA=dvopen('+sr/autocor/1');
end
if isempty(VSTEER)
   VSTEER=dvopensteer(1);
end

if nargin < 6, nstep=2; end
if nargin < 5, navg=5; end
if nargin < 4, ncycle=2; end
if nargin < 3, ampl=0.001; end

nu=input('nu: ');
[respv,bpm,steerer]=respmat('sr','sv',nu);
bnum=3*mod(cell-4,32)+id;
mup=(1:nstep)/nstep;
mdown=((nstep-1):-1:-nstep)/nstep;
mback=((1-nstep):0)/nstep;

[vset,vread]=dvreadsteer(VSTEER);
save vset vset;

dvcmd(SRA,'DevOff');

Zres=zeros(224,1);

dvsetparam(BPMDEV, 'BpmAverage', navg);
dvsetparam(BPMDEV, 'BpmIgnoreIncoherence', 'on');
for j=0:ncycle
   setexpbump(vset,bnum,ampl,mup);
   Zplus=zorbit(BPMDEV);
   setexpbump(vset,bnum,ampl,mdown);
   Zminus=zorbit(BPMDEV);
   setexpbump(vset,bnum,ampl,mback);
   Zbump=Zplus-Zminus;
   plot(Zbump);
   hold on
   if j>0
      Zres=Zres+Zbump;
   end
end
hold off
Zres=Zres/ncycle;

coef=load_corcalib('sr','v');
coef=reshape(coef(ones(16,1),:)',1,96);

kicks=banal3(bnum,-Zres,respv,bpm,steerer);
kick2=banal7(bnum,-Zres,respv,bpm,steerer);
%plot([Zres Zres+respv*kicks Zres+respv*kick2]);
plot([Zres Zres+respv*kicks]);
bcof(1,:)=BVTABLE(bnum,:);
bcof(2,:)=bcof(1,:)+kicks'./coef/2/ampl;
%bcof(3,:)=bcof(1,:)+kick2'./coef/2/ampl;
bcof

BVTABLE(:,bnum)=BVTABLE(:,bnum)+kicks./coef'/2/ampl;
CALINFO(bnum).date=datestr(now);
CALINFO(bnum).amplitude=ampl;

fn=input('File name (bump calibration): ','s');
if ~isempty(fn)
   save(fn,'BVTABLE','CALINFO');
end

function kick=banal3(bnum,orbit,respv,bpm,steerer)
[nb,nk]=size(respv);
blist=bumpbpmlist(bpm,steerer);
bok=isfinite(orbit);
bok(blist{bnum})=false;
kok=[nk 1:nk-1;1:nk;2:nk 1];
kok=kok(:,bnum);
kick=zeros(nk,1);
kick(kok)=respv(bok,kok)\orbit(bok);

function kick=banal7(bnum,orbit,respv,bpm,steerer)
[nb,nk]=size(respv);
blist=bumpbpmlist(bpm,steerer);
bok=isfinite(orbit);
bok(blist{bnum})=false;
kok=[nk 1:nk-1;1:nk;2:nk 1];
kok=kok(:,bnum);
kick=zeros(nk,1);
[u,s,v]=svd(respv(bok,kok),0);
kick(kok)=v*inv(s)*u'*orbit(bok);

function zz=zorbit(dev)
[xx,zz]=dvreadbpm(dev,'wait');
