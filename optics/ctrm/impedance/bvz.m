function [Zres,rms,cor,current]=bvz(cell,id,ampl,ncycle,navg,nstep)
%BVZ(CELL,id,AMPL,CYCLES,BPMAVG,NSTEPS)	measure a bumped orbit
%
% CELL   : cell number (1:32)
% ID     : bump identification (1:3)
% AMPL   : bump amplitude (m) (default 0.001)
% CYCLES : number of bump cycle (to cancel hysteresis) (default 3)
% BPMAVG : averaging number for BPMs (default 5)
% NSTEPS : number of steps when rising the bump (default 3)

global BPMDEV SRA VSTEER CTDEV
if isempty(BPMDEV)
   BPMDEV=dvopen('tango:SR/D-BPMLIBERA/ALL');
end
if isempty(SRA)
   SRA=dvopen('+sr/autocor/1');
end
if isempty(VSTEER)
   VSTEER=dvopensteer(1);
end
if isempty(CTDEV)
   CTDEV=dvopen('tango:sr/d-ct/1/current');
end

if nargin < 6, nstep=3; end
if nargin < 5, navg=5; end
if nargin < 4, ncycle=3; end
if nargin < 3, ampl=0.001; end

bnum=3*mod(cell-4,32)+id;
mup=(1:nstep)/nstep;
mdown=((nstep-1):-1:-nstep)/nstep;
mback=((1-nstep):0)/nstep;

[vset,vread]=dvreadsteer(VSTEER);
save vset vset;

dvcmd(SRA,'DevOff');

Zres=zeros(224,1);
rms=NaN*ones(ncycle,1);
cor=zeros(ncycle,2);
Zprev=NaN*ones(224,1);
current=0;

dvsetparam(BPMDEV, 'BpmAverage', navg);
dvsetparam(BPMDEV, 'BpmIgnoreIncoherence', 'on');
for j=0:ncycle
   setexpbump(vset,bnum,ampl,mup);
   Zplus=zorbit(BPMDEV);
   setexpbump(vset,bnum,ampl,mdown);
   [iset,iread]=dvread(CTDEV);
   Zminus=zorbit(BPMDEV);
   setexpbump(vset,bnum,ampl,mback);
   Zbump=Zplus-Zminus;
   if j>0
      Zres=Zres+Zbump;
      current=current+iread;
      rms(j)=std2(Zbump-Zprev)
   end
   Zprev=Zbump;
end
Zres=Zres/ncycle;
current=current/ncycle;
plot(1:224,Zres);
!say 'end of measurement'

fn=input('File name: ','s');
if ~isempty(fn)
   save(fn,'Zres','current','bnum','ampl');
end

function zz=zorbit(dev)
[xx,zz]=dvreadbpm(dev,'wait');
