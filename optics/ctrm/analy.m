function analy(varargin)
%ANALY		Checks the presence of necessary files in the optics directory
%
%ANALY(FPATH)
%
%FPATH: path of the optics directory
%           (default $APPHOME/sr/optics/setings/theory)

[~,fpath,mach]=getmachenv(varargin{:});

fprintf('\nScanning %s\n',fpath);
fprintf('\nBase files:\n');
if strcmp(mach,'sr')
    checkfile('BETALOG');
    checkfile('BETAOUT');
    checkfile('matlog.mat');
    checkfile('betaout.mat');
elseif strcmp(mach,'sy')
    checkfile('betamodel.mat');
end

fprintf('\nResponse matrices:\n');
if strcmp(mach,'sr')
    checkfile('resH.rsp',[2 7]);
    checkfile('resV.rsp',[2 7]);
    checkfile('resH2V.rsp',[2 7]);
    checkfile('resV2H.rsp',[2 7]);
elseif strcmp(mach,'sy')
    checkfile('expresp.mat');
end

fprintf('\nOrbit correction:\n');
show_matfile(fullfile(fpath,'matlog'));
checkfile('alpha');
checkfile('bpms');
checkfile('h_steerers');
checkfile('v_steerers');
checkfile('h_bumps');
checkfile('v_bumps');

if strcmp(mach,'sr')
    checkfile('v_short_bumps');
    checkfile('h_svd2.mat');
    checkfile('v_svd2.mat');
elseif strcmp(mach,'sy')
    checkfile('h_resp.csv');
    checkfile('v_resp.csv');
end
checkfile('h_svd.mat');
checkfile('v_svd.mat');

if strcmp(mach,'sr')
    fprintf('\nResonance correction:\n');
    checkfile('skewcor.mat');
    checkfile('quadcor.mat');
    checkfile('skewcor.mat');
    checkfile('quad32cor.mat');
    checkfile('skew32cor.mat');
    checkfile('base_skew2');
    checkfile('base_quad2');
    
    fprintf('\nEmittance growth analysis:\n');
    checkfile('Beta_Constants');
    
    fprintf('\nInjection:\n');
    checkfile('kicker_bump');
    checkfile('steerer_bump');
    checkfile('srinj/tl2_h');
    checkfile('srinj/tl2_v');
    
    fprintf('\nEmittance computation:\n');
    checkfile('emittance.res');
    
    fprintf('\nFeedback coupling:\n');
    checkfile('fdbk.mat');
end

    function ok=checkfile(fname,code)
        %CHECKFILE(FNAME,CODE) checks and displays the existence of FNAME.
        if nargin<2, code=2; end
        ok=any(exist(fullfile(fpath,fname),'file')==code);
        if ok, okstr='OK'; else okstr='missing'; end
        fprintf('%20s: %s\n',fname, okstr);
    end
end
