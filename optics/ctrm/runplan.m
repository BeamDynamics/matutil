function tmima=runplan(runname)

global APPHOME
if nargin < 1, runname='run'; end
fname=[APPHOME '/hdb/archive/hdb.' runname];
[status,result]=unix(['tmintmax -f "%d-%b-%Y %T" ' fname]);
if status ~= 0, error('runplan:NoFile','No matching run file.');end
[v1,remain]=strtok(result, [char(9) char(10)]);
v2=strtok(remain, [char(9) char(10)]);
tmima=[v1;v2];
%tmima={v1;v2};
