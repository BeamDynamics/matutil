function [orbith,orbitv]=setexpbump(vini,listv,amplv,steps)
%SETEXPBUMP(VINI,LISTH,AMPLH,LISTV,AMPLV,NSTEPS)	rises orbit bumps
%
% VINI  : initial steerer values
% LISTV : list of vertical bump numbers (1:96)
% AMPLV : list of vertical amplitudes
% STEPS : list of steps
%
global VSTEER BVTABLE

if isempty(BVTABLE)
%  BVTABLE=load_bump([getenv('APPHOME') '/sr/optics/settings/theory/v_short_bumps']);
   error('SetBump:NoCalib','The calibration table is missing.');
end
if isempty(VSTEER)
   VSTEER=dvopensteer(1);
end

nv=prod(size(amplv));
if (nv ~= 1) && (nv ~= prod(size(listv)))
   error('SetBump:Error','Wrong argument size.');
end

for step=steps
   bv=zeros(size(BVTABLE,2),1);
   bv(listv)=step*amplv;
   incrv=BVTABLE*bv;
   disp(['step ' num2str(step)]);
   dvsetsteer(VSTEER,vini+incrv);
   pause(2);
end
pause(3);
