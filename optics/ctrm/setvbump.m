function setvbump(dev,cell,id,ampl,nstep)
bumpvalue=zeros(1,7);
bumpvalue(3)=1;	% 1 for H, 3 for V
bumpvalue(6)=mod(3*cell+id-13,96);
bumpvalue(7)=ampl/nstep;

for j=1:nstep
   [output,err]=dvcmd(dev,'DevAutoCorBump',bumpvalue);
   disp(['step ' int2str(j) ', error ' int2str(err)]);
   if err ~= 0
      error('SetBump:Error','Error %i while setting bump.',err);
   end
   pause(7);
end
