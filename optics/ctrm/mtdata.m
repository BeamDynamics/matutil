function [vx,vz]=mtdata(dev,fcode)

disp('Starting acquisition...');
[s,err]=dvcmd(dev,'DevStart');		% start acquisition sequence

for i=1:300				% wait for end of sequence
   pause(2);
   [state,err]=dvcmd(dev, 'DevState');
   if state ~= 9, break;end
end
if state == 23
   error('MT in fault.');
else					% get data from files
   [vx,avex]=load_mtour(fcode, 'h');
   [vz,avez]=load_mtour(fcode, 'v');
end
