function [endcor,mach]=idcouplingcorrection(esrf,cell,idcor,dist)

if nargin < 4, dist=1.8; end
if mod(numel(esrf),16) == 0
   mach=semaddid(reshape(esrf(:),[],16),cell,[0 dist 2.75]);
elseif mod(numel(esrf),16) == 1
   mach=[semaddid(reshape(esrf(1:end-1),[],16),cell,[0 dist 2.75]);esrf(end)];
end
cor1=findcells(mach,'FamName','ID[HL]1');
cor2=findcells(mach,'FamName','ID[HL]5');
id6=findcells(mach,'FamName',sprintf('ID[HL]%d',idcor+1));
bpmidx=findcells(mach,'BetaCode','PU');
lindata0=atx(mach,0,bpmidx);
[resp1,resp2]=semrdtresp(mach,bpmidx,[cor1 id6 cor2]);

skstr=5.e-3;
skewstrength=skstr*[0;1;0];
mach6=setcellstruct(mach,'PolynomA',id6,skewstrength(2),2);
lindata=atx(mach6,0,bpmidx);
f1=resp1*skewstrength;
f2=resp2*skewstrength;
[eta,mess]=semdisp(lindata,lindata0,f1,f2);
endcor=skewclosedbump(mach,bpmidx,[cor1 id6 cor2]);

skewstrength=skstr*[endcor(1);1;endcor(2)];
machc=setcellstruct(mach6,'PolynomA',cor1,skewstrength(1),2);
machc=setcellstruct(machc,'PolynomA',cor2,skewstrength(3),2);
lindata=atx(machc,0,bpmidx);
f1=resp1*skewstrength;
f2=resp2*skewstrength;
[eta,mess]=semdisp(lindata,lindata0,f1,f2);
