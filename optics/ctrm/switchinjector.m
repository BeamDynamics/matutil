function [state]=switchinjector(state,timeout)
% Function to prepare the booster for topup
% case 2 Switching ON
% case 12 Switching Standby
if nargin < 2, timeout=200; end
id.bpss=dvopen ('+tango:sy/bpss/manager');
id.syrf=dvopen('+tango:sy/rfssa-tra/tra0');

switch state
    case 2
          fprintf('>>Switching Injector to ON\n');
          system('say "injection in 3 minutes"');
         
        % 12 STBY, 2 ON
          if ~isdevstate(id.bpss, 2)
            fprintf('>>Switching Bpss ON\n');
            wait4device(id.bpss,'DevOn',@(dev) ~isdevstate(dev,9),{4,90});
          end
       
          if isdevstate(id.bpss, 2)
            fprintf('>>Execute OptimizedOperation\n');
            wait4device(id.bpss,'OptimizedOperation',@(dev) ~isdevstate(dev,9),{4,90});
          end  
          
          % 12 STBY, 2 ON
          if ~isdevstate(id.syrf, 2)
            fprintf('>>Switching syrf ON\n');
            wait4device(id.syrf,'DevOn',@(dev) ~isdevstate(dev,9),{4,90});
          end 
    
    case 12
        fprintf('>>Switching Injector to STANDBY\n');
        
         % fprintf('>>Switching Syrf to STANDBY\n');
         % dvx(@dvcmd,id.syrf,'DevStandby');
       
          fprintf('>>Switching BPSS to STANDBY\n');
          dvx(@dvcmd,id.bpss,'DevStandby'); 
    otherwise
    
        fprintf('>>Wrong state required\n');
        
        
end

dvclose([id.bpss;id.syrf]);
