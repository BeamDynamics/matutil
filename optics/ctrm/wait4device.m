function wait4device(dev,command,testfunc,timeout)
%WAIT4DEVICE Send a command and wait for the result
%
%WAIT4DEVICE(DEVICE,COMMAND,TESTFUNC,TIMEOUT)
%
%DEV        array of device handles
%COMMAND:   Command name (no action if empty)
%TESTFUNC:  Handle to the test function. Called as ok=testfunc(dev) with
%           ok having the same size as dev
%TIMEOUT:   Timeout for fulfilling the test, otherwise send the exception
%           'WaitForDevice:Timeout.
%           If TIMEOUT is a cell array, timeout{1} before the 1st test,
%           wait timeout{2} before sending the exception

if ~iscell(timeout), timeout={0,timeout}; end
[tim1,tim2]=deal(timeout{:});

%if ~isempty(command) && ~all(testfunc(dev))
if ~isempty(command)
    dvx(@dvcmd,dev,command);
end

pause(tim1);
%pending=ok;
interval=0.5;
ok=testfunc(dev);
for t=0:tim2/interval
    pause(interval);
    ok=testfunc(dev);
    if all(ok), break; end
    %pending=~ok;
end

if ~all(ok)
    error('WaitForDevice:Timeout','Timeout reached for %s',...
        char(dvname(dev(~ok)))');
end
end
