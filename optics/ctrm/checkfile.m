function ok=checkfile(fname,code)
%CHECKFILE(FNAME.CODE) checks and displays the existence of FNAME.

if exist('code','var') ~= 1, code=2; end
ok=(exist(fname,'file') == code);
if ok, okstr='OK'; else okstr='missing'; end
disp(sprintf('%20s: %s',fname, okstr))
