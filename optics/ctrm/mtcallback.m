function mtcallback(action)

global vx aveorbit plane
global ampl phas avephase
global alphaid betaid eps
global beta0 beta invr modul
global high_beta low_beta avebeta
global tune1 tune2 tune3 tune4
global mtpath
global bpms
persistent vx0 intens stdx stdi
persistent bid aid

pllist={'h';'v'};
kicklist={'sy/ps-ke/1','sr/ps-k1/1','sr/ps-k2/2','sr/ps-k3/3','sr/ps-k4/4'};
switch (action)
    
    case 'kick'
        plidx=get(findobj(gcbf,'Tag','setplane'),'Value');
        idx=get(findobj(gcbf,'Tag','SelKick'),'Value');
        if idx > 1
            Kdev=tango.Device(kicklist{idx-1});
            %         Kname=['tango:' kicklist{idx-1}];
            %         Kdev=dvopen(Kname,[Kname '/current']);
        else
            Kdev=[];
        end
        %    Kstrength=getbox('Kstrength');
        %    if ~isempty(Kstrength)
        % 	  dvcmd(Kdev(2),'DevWrite',Kstrength);
        % 	  dvcmd(Kdev(1),'DevOn');
        %    end
        [vx0,intens,stdx,stdi]=mtkick(Kdev, getbox('Kstrength'), getbox('Nave'));
        plane=pllist{plidx};
        trns=isbeam(intens,0.05);
        fprintf('1st beam ar %d turns\n', find(trns,1));
        [aveorbit,vx]=preparevv(vx0(:,:,plidx),trns);
        
    case 'loade'
        plidx=get(findobj(gcbf,'Tag','setplane'),'Value');
        [vx0h,vx0v,intens]=load_abcd();
        vx0=cat(3,vx0h,vx0v);
        plane=pllist{plidx};
        trns=isbeam(intens,0.05);
        [aveorbit,vx]=preparevv(vx0(:,:,plidx),trns);
        
    case 'loadb'
        plidx=get(findobj(gcbf,'Tag','setplane'),'Value');
        %   [vx0,aveorbit]=load_mtour(get(findobj(gcbf,'Tag','dataname'),'String'), plane);
        vx0h=load_dd('h');
        vx0v=load_dd('v');
        intens=load_sumdd();
        vx0=cat(3,vx0h,vx0v);
        plane=pllist{plidx};
        trns=isbeam(intens,0.05);
        fprintf('1st beam ar %d turns\n', find(trns,1));
        [aveorbit,vx]=preparevv(vx0(:,:,plidx),trns);
        
    case 'loadf'
        [fname,mttemp]=uigetfile([mtpath '*.mat']);
        if ischar(fname)
            mtpath=mttemp;
            pltog=findobj(gcbf,'Tag','setplane');
            aa=load(fullfile(mtpath,fname));
            if isfield(aa,'vx0')
                if isfield(aa,'plane')
                    plane=aa.plane;
                    set(pltog,'Value',findstr('hv',plane));
                    fprintf('Version 2 file: %s plane\n',plane);
                    vx=aa.vx0;
                else
                    plidx=get(pltog,'Value');
                    plane=pllist{plidx};
                    if isfield(aa,'ave0'), aa.aveorbit=aa.ave0(plidx,:); end
                    fprintf('Version 3 file: both planes\n');
                    vx=aa.vx0(:,:,plidx);
                end
            else
                plane=aa.plane;
                set(pltog,'Value',findstr('hv',plane));
                fprintf('Version 1 file: %s plane\n',plane);
                vx=aa.vx;
            end
            nturns=size(vx,1);
            if isfield(aa,'intens')
                trns=isbeam(aa.intens,0.05);
                fprintf('1st beam ar %d turns\n', find(trns,1));
            else
                trns=true(nturns,1);
            end
            if isfield(aa,'aveorbit')
                vx=vx+aa.aveorbit(ones(nturns,1),:);
            end
            [aveorbit,vx]=preparevv(vx,trns);
            % 	  aveorbit=aa.aveorbit;
            % 	  vx=preparevx(vx,aveorbit);
        end
        
    case 'savef'
        [fname,mttemp]=uiputfile([mtpath 'untitled.mat'],'Save as');
        if ischar(fname)
            mtpath=mttemp;
            %		eval(['save ''' fullfile(mtpath,fname) ''' vx0 aveorbit plane']);
            save(fullfile(mtpath,fname),'vx0','intens','stdx','stdi');
        end
        
    case 'plotb'
        range=getbox('turnrange','s');
        ibpm=sr.bpmindex(getbox('plotb'));
        disp_b(eval(['vx(' range ',ibpm)']),ibpm,plane);
        enable_plot({'ap','aprange'},'on');
        setbox('xxprange',range);
        tune1=findtune(eval(['vx(' range ',:)']),1);
        tune2=findtune(eval(['vx(' range ',:)']),2);
        tune3=findtune(eval(['vx(' range ',:)']),3);
        tune4=findtune(eval(['vx(' range ',:)']),4);
        
    case 'plott'
        disp_t(vx,getbox('plott'),plane);
        
    case 'ap'
        range=getbox('turnrange','s');
        [ampl,phas,tune]=mtap(eval(['vx(' range ',:)']),getbox('ap'));
        phas0=(0:15)/16*phas(224);
        cellphase=phas-phas0(ones(1,14),:);
        avp=mean2(cellphase')';
        avephase=avp(:,ones(1,16))+phas0(ones(1,14),:);
        
        range=getbox('aprange');
        subplot(2,1,1);
        plot(ampl(:,range));
        set(gca,'Xlim',[1 14]);
        title('Amplitude');
        xlabel('BPM number');
        
        subplot(2,1,2);
        plot(cellphase(:,range));
        set(gca,'Xlim',[1 14]);
        xlabel('BPM number');
        title('Phase advance');
        posgr(0.1,0.6,sprintf(['\\nu_' plane '=%g\n'], tune));
        enable_plot({'xxp','xxprange'},'on');
        
    case 'xxp'
        range=getbox('xxprange','s');
        id=mod(getbox('xxp')-4,32)+1;
        [xx,xxp]=mtxxp(vx,range);		% compute (x, x') in ID straights
        [betaid,alphaid,eps]=mtabg(xx,xxp);	% fit ellipses to phase space
        beta=mtscale(betaid,alphaid,ampl);	% rescale amplitudes to get all betas
        avebeta=mean2(beta')';
        avebeta=avebeta(:,ones(1,16));
        [bid,aid]=mtbetaid(beta,betaid);		% rebuild alpha beta in the middle of ID
        high_beta=bid(1:2:32)
        low_beta=bid(2:2:32)
        
        [x1,xp1]=disp_eps(betaid,alphaid,eps);
        [x2,xp2]=disp_eps(bid,aid,eps);
        subplot(2,1,1);
        plot(eps,'-o');
        v=axis;
        axis([1 32 0 v(4)]);
        text(3,0.25*v(4),sprintf('invariant: %.3e mm.mrad',mean2(eps')));
        ylabel('statistical invariant');
        xlabel('ID number');
        set(gca,'xTick',1:2:32,'xTickLabel',[4:2:32 2]);
        subplot(2,1,2);
        h212=plot(xx(:,id),xxp(:,id),'ob',x1(:,id),xp1(:,id),x2(:,id),xp2(:,id));
        set(h212(2),'LineWidth',2);
        legend(h212(2:3),'Phase-space analysis','Fourier analysis');
        xlabel('x (mm)');
        ylabel('x'' (mrad)');
        %  gtext({['\alpha = ' num2str(alphaid(id))],['\beta = ' num2str(betaid(id))],['\epsilon = ' num2str(eps(id))]});
        enable_plot({'betab','invarck'},'on');
        if plane == 'h'
            beta0=reshape(bpms.bx,14,16);
        else
            beta0=reshape(bpms.bz,14,16);
        end
        
    case 'betaf'
        
        subplot(2,1,1);
        range=getbox('aprange');
        plot(beta(:,range));
        v=axis;
        axis([1 14 0 v(4)]);
        title('\beta function');
        xlabel('BPM number');
        ylabel('\beta (m)');
        
        subplot(2,1,2);
        set(gca,'xTick',1:16,'xTickLabel',[4:2:32 2]);
        plot([betaid(1:2:32)' high_beta']);
        %  plot([betaid(2:2:32)' low_beta']);
        %  axis([1 16 0 v(4)]);
        title('High-\beta straight sections');
        xlabel('ID number');
        ylabel('\beta (m)');
        %  plot2([betaid(2:2:32)' low_beta']);
        %  axis([1 16 0 1]);
        legend('Phase-space analysis','Frequency analysis');
        
    case 'modul'
        
        modul=(beta-beta0)./beta0;
        modulation=std2(modul(:))
        clf
        plot(bpms.s,modul(:))
        
    case 'invar'
        
        %  range=getbox('turnrange','s');
        range=getbox('xxprange','s');
        v=get(findobj(gcbf,'Tag','invarck'),'Value');
        switch v
            case 1
                disp('theoretical beta');
                [bid0,aid0]=mtbetaid(beta0,betaid);
                invr=mtinvariant(vx,bid0,aid0);
            case 2
                disp('phase-space beta');
                invr=mtinvariant(vx,betaid,alphaid);
            case 3
                disp('Fourier beta');
                invr=mtinvariant(vx,bid,aid);
        end
        invm=mean2(eval(['invr(' range ',:)'])')';
        invmax=max(invm);
        coher20=length(find(invm > 0.2*invmax))
        coher60=length(find(invm > 0.6*invmax))
        plot(invm);
        title('Average ID invariant');
        posgr(0.5, 0.8, {sprintf('Max. invariant: %.3e mm.mrad',invmax);['duration: ' int2str(coher20)]});
        
end

function trns=isbeam(intens,threshold)
intave=sum2(intens,2);
intmax=max(intave);
trns=(intave>threshold*intmax);

function [aver,oscill]=preparevv(vx0,trns)
unsmr=logical(get(findobj(gcbf,'Tag','unsmear'),'Value'));
suppress=sr.bpmindex(getbox('suppressbox'));
nturns=sum(trns);
oscill=NaN(size(vx0));
if nturns<size(vx0,1)	% First turn
    fprintf(1,'Single turn (%d)', nturns);
    disp(find(trns));
    [oscill(trns,:),aver]=mtclean(vx0(trns,:),zeros(1,size(vx0,2)),suppress);
else					% Stored beam
    disp('Stored Beam');
    [oscill(trns,:),aver]=mtclean(vx0(trns,:),mean2(vx0(trns,:)),suppress);
end
if unsmr, oscill=mtunsmear(oscill); end
[first,coher]=disp1000(aver,oscill,0.1);
enable_plot({'plotb','turnrange','plott','savefile','invarb'},'on');
enable_plot({'ap','aprange','xxp','xxprange','betab','invarck'},'off');
setbox('peakbox','');
setbox('turnrange',[int2str(first) ':end']);
set(findobj(gcbf,'Tag','invarck','Style','CheckBox'),'Value',0);

function vx=preparevx(vx0,aveorbit)
unsmr=logical(get(findobj(gcbf,'Tag','unsmear'),'Value'));
suppress=sr.bpmindex(getbox('suppressbox'));
[vx,aveorbit]=mtclean(vx0,aveorbit,suppress);
if unsmr, vx=mtunsmear(vx); end
[first,coher]=disp1000(aveorbit,vx,0.1);
enable_plot({'plotb','turnrange','plott','savefile','invarb'},'on');
enable_plot({'ap','aprange','xxp','xxprange','betab','invarck'},'off');
setbox('peakbox','');
setbox('turnrange',[int2str(first) ':end']);
set(findobj(gcbf,'Tag','invarck','Style','CheckBox'),'Value',0);

function vx=prepareabcd(vx0,aveorbit)
unsmr=logical(get(findobj(gcbf,'Tag','unsmear'),'Value'));
suppress=sr.bpmindex(getbox('suppressbox'));
vx=vx0;
vx(:,suppress)=NaN;
[first,coher]=disp1000(aveorbit,vx,0.1);
enable_plot({'plotb','turnrange','plott','savefile','invarb'},'on');
enable_plot({'ap','aprange','xxp','xxprange','betab','invarck'},'off');
setbox('peakbox','');
setbox('turnrange',[int2str(1) ':end']);
set(findobj(gcbf,'Tag','invarck','Style','CheckBox'),'Value',0);

function enable_plot(tags,code)
for i=1:length(tags)
    set(findobj(gcbf,'Tag',tags{i}),'enable',code);
end

function vstr=getbox(tag,code) %#ok<INUSD>
obj=findobj(gcbf,'Tag',tag);
vstr=char(get(findobj(obj,'flat','Style','edit'),'String'));
%vstr=char(get(findobj(gcbf,'Tag',tag,'Style','edit'),'String'));
if nargin <= 1
    vstr=str2num(vstr); %#ok<ST2NM>
end

function setbox(tag,value)
set(findobj(gcbf,'Tag',tag,'Style','Edit'),'String',value)
