function [first,coher]=disp1000(aveorbit,vx,threshold)

orbits=std2(vx,1,2);
orbmax=max(orbits);
on=find(orbits>threshold*orbmax);
if isempty(on)
first=[];
coher=[];
else
first=on(1);
coher=on(end)-first;
end

clf
subplot(3,1,1);
plot(1000*orbits);
xlabel('turns');
ylabel('rms. orbit (\mum)');
title('OSCILLATION DAMPING');
posgr(0.75, 0.8, {['first: ' int2str(first)],['duration: ' int2str(coher)]});

subplot(3,1,2);
plot(1000*aveorbit);
xlabel('Bpm #');
ylabel('Average orbit (\mum)');
title('ORBIT');
posgr(0.02, 0.92, ['Rms orbit: ' num2str(1000*std2(aveorbit),3)]);

subplot(3,1,3);
plot(1000*std2(vx,1));
xlabel('Bpm #');
ylabel('Rms orbit (\mum)');
title('FLUCTUATIONS');
posgr(0.02, 0.92, ['Average noise: ' num2str(1000*mean2(std2(vx)'),3)]);
