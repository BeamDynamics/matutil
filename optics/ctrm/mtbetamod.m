function next=mtbetamod(prev)
global beta avebeta

dbeta=(beta-avebeta)./avebeta;
if nargin > 0, next=[prev dbeta(:)]; else next=dbeta(:);end
clf
plot(next);
axis([1 224 -.5 .5]);
xlabel('BPM number');
ylabel('beta modulation');
grid on
