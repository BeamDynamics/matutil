function setvidbump(dev,ncell,amp,pa,nstep)
bumpvalue=zeros(1,9);
bumpvalue(3)=2;	% 1 for H, 3 for V
bumpvalue([6 7])=mod(3*ncell-[13 12],96);
bumpvalue([8 9])=[1 pa]*amp/nstep;
for j=1:nstep
   [output,err]=dvcmd(dev,'DevAutoCorBump',bumpvalue);
   disp(['step ' int2str(j) ', error ' int2str(err)]);
   if err ~= 0
      error('SetBump:Error','Error %i while setting bump.',err);
   end
   pause(7);
end
