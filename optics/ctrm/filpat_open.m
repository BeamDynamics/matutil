function id=filpat_open
% fill_open             open device servers used for topping-up
    
id.bunch_list=dvopen('tango:sy/tim/bclock/bunch_list');
id.rot=dvopen('sy/t-inj/pulse_rotation');
id.current=dvopen('+tango:sr/d-ct/pct-id10/current');
%id.current_single=dvopen('+tango:sr/d-ct/ict1/current');
id.current_single=dvopen('+tango:sr/d-ct/2/current');
id.linac_pulses=dvopen('sy/t-inj/pulse_select','sy/t-inj/pulse_1','sy/t-inj/pulse_2','sy/t-inj/pulse_3','sy/t-inj/pulse_4','sy/t-inj/pulse_5');
id.linac_gun=dvopen('+tango:elin/beam/run');
