function sec2wait=time2nextinj(period,tnow)
if nargin<2, tnow=now; end
nowvec=datevec(tnow);
nowsec=3600*(15+nowvec(4))+60*nowvec(5)+nowvec(6);
injsec=ceil(nowsec/period)*period;
sec2wait=injsec-nowsec;
end
