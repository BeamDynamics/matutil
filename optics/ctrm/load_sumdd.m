function v = load_sumdd()
%LOAD_SUMDD Load turn-by-turn intensity
%

dd=dvopen('tango:sr/d-bpmlibera/all/sum_dd');
v=dvcmd(dd,'DevRead');
dvclose(dd)
nturns=length(v)/224;
v=reshape(v,nturns,224);
%v=v(:,[22:224 1:21]);	% sort to start in cell 4
disp([int2str(nturns) ' turns']);
