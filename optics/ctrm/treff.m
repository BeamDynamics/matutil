function eff=treff

global TL2DEV SRDEV

if isempty(TL2DEV)
TL2DEV=dvopen('tl2/d-ct/ct')
end
if isempty(SRDEV)
SRDEV=dvopen('sr/d-ct/1')
end

sr1=dvcmd(SRDEV,'DevReadValue');
pause(10);
sr2=dvcmd(SRDEV,'DevReadValue')-sr1;
tl2h=reshape(dvcmd(TL2DEV,'DevReadValueCycle'), 3, 20)';
eff=3*sr2(1)/sum(tl2h(1:10,1));
