function varargout=load_bpi(dirname)
%[H1,H8,H10,H14,H16,H20,H24,H27]=LOAD_BPI	Load BPI data
%
%	H1 : date for BPI head 1
%	H8 : date for BPI head 8
%	...

headlist={'01','08','10','14','16','20','24','27'};

if nargin < 1
   [y,m,d,h,mn,s]=datevec(now);
   inidir=[getenv('APPHOME') ...
	   sprintf('/itlk/bpm_interlock/%4i/*',y)];
   [fname,dirname]=uigetfile(inidir,'Select a file');
end
if ~isequal(dirname,0)
   for i=1:min(nargout,8)
      head=['head' headlist{i}];
%      cmd=['dlmread(''' dirname '/bpi_' head ''',''\t'',[1 1 1024 8]);'];
%      varargout{i}=eval(cmd,'seterr(head)');
      try
          varargout{i}=dlmread([dirname '/bpi_' head],'\t',[1 1 1024 8]);
      catch
          varargout{i}=seterr(head);
      end
   end
end

function val=seterr(head)
[msg,msgid]=lasterr;
disp(msg);
warning(['Missing information for ' head]);
val=[];
