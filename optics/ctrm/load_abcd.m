function [xx,zz,sumabcd] = load_abcd()
%LOAD_ABCD Compute turn-by-turn position from electrodes

global APPHOME
global VDEBUG
figure(5);
offsets=load([APPHOME '/sr/bpm/offsets/libera/offsets']);
bpmname='tango:sr/d-bpmlibera/all';
dd=dvopen([bpmname '/va_dd'],[bpmname '/vb_dd'],[bpmname '/vc_dd'],[bpmname '/vd_dd']);
v=dvcmd(dd,'DevRead');
VDEBUG=v;
dvclose(dd);

%fn='/machfs/MDT/2010/Feb11/FT_TRAJ/abcd_debug/std_singleturn.mat';
%fn='/machfs/MDT/2010/Feb11/FT_TRAJ/abcd_debug/std_abcd_debug_400.mat';
%fn='/machfs/MDT/2010/Feb11/FT_TRAJ/abcd_debug/maf_abcd_debug_K1_600.mat';
%fn='/machfs/MDT/2010/Feb11/FT_TRAJ/abcd_debug/maf_abcd_debug_V_2.mat';
%load(fn);
%v=VDEBUG;

nturns=size(v,2)/224;
disp([int2str(nturns) ' turns']);

va=mtunsmear(reshape(v(1,:),nturns,224));
vb=mtunsmear(reshape(v(2,:),nturns,224));
vc=mtunsmear(reshape(v(3,:),nturns,224));
vd=mtunsmear(reshape(v(4,:),nturns,224));

% va=reshape(v(1,:),nturns,224);
% vb=reshape(v(2,:),nturns,224);
% vc=reshape(v(3,:),nturns,224);
% vd=reshape(v(4,:),nturns,224);

sumabcd=va+vb+vc+vd;
sumi=sum(sumabcd,2);
plot(sumi);
%turnok=all((sumi>0.4e9),2);
turnok=true(nturns,1);
xx=NaN(size(sumabcd));
zz=NaN(size(sumabcd));
Qh=0.5*((va-vc)./(va+vc)+(vd-vb)./(vd+vb));
Rv=0.5*((va-vd)./(va+vd)+(vb-vc)./(vb+vc));
xx(turnok,:)=11.469*real(atanh(1.3658*Qh(turnok,:)));
xb=xx(turnok,:)-0.0592;
zz(turnok,:)=14.37*(1+0.01452*(xb.*xb)).*Rv(turnok,:);
xx=xx+offsets.xoffset(:,ones(1,nturns))';
zz=zz+offsets.zoffset(:,ones(1,nturns))';
figure(2);
