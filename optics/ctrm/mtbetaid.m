function [betaid,alphaid]=mtbetaid(beta,betaguess)

dist=6.1052*ones(1,32);
dist(27)=7.2052;	% 6 m straights in ID30

idm=reshape([224 1:223],7,32);
beta1=beta(idm(1,:));		% upstream BPM
beta2=beta(idm(2,:));		% downstream BPM
avebeta=0.5*(beta1+beta2);
delta=beta1.*beta2-dist.*dist;
delta(delta<0)=NaN;
betat=0.5*[(avebeta+sqrt(delta));(avebeta-sqrt(delta))];
[x,k]=sort(abs(betat-[betaguess;betaguess]));
betaid=betat((0:2:62)+k(1,:));
alphaid=-0.5*(beta2-beta1)./dist;
