function [x,xp]=disp_eps(beta,alpha,eps)

ang=(0:50)'/50*2*pi;
neps=sqrt(eps);
nbeta=sqrt(beta);
nx=cos(ang)*neps;
nxp=sin(ang)*neps;
x=nbeta(ones(51,1),:).*nx;
xp=(nxp-alpha(ones(51,1),:).*nx)./nbeta(ones(51,1),:);
