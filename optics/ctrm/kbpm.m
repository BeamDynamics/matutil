function [kdx,nord,cell]=kbpm(idx)
%KDX=KBPM(IDX) returns the "hardware" index of SR BPM with index IDX
%
%  [KDX,ID,CELL]=KBPM(IDX) returns in addition the ID and cell number
%
%   See also CBPM, SRBPMNAME, SRBPMINDEX

kdx=idx+21;
kdx=kdx-224*(kdx>224);
if nargout > 1
   cell=floor((kdx-1)/7)+1;
   nord=kdx-7*(cell-1);
end
