function dd=processPM(dd)
%PROCESSPM Process postmortem data
%

if ischar(dd)
    dd=load(dd);
end
try
    [T,OV,OH,Xdif,Zdif] = analyPM(dd, dd.PMSum, dd.PMXPosition, dd.PMZPosition,...
        dd.XItlkNotifications, dd.ZItlkNotifications);
catch
    err=lasterror
    disp(err.message)
    errordlg('Error in data processing','Error');
end
end

