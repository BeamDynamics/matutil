function [T, OV, OH, Xdif, Zdif] = analyPM(dd,SumPM, XPM, ZPM, XIntNotif, ZIntNotif) %#ok<INUSL>
% all the input data should have as 1rst BPM (i.e. 1rst value in a colomn of 224) the C4-1
% the 2 left subplots also start with C4-1 at the left (and C3-7 at the right)
% the Xdif and Zdif outputs also have as 1rst BPM the C4-1

dec=64;
upzlim=0.7*ones(224,1); lowzlim=-0.7*ones(224,1);
upxlim=2*ones(224,1); lowxlim=-2*ones(224,1);
ZIntlkstations=[31 47 75 89 115 143 164 206]; XIntlkstations=[15  43  71  85  99 127 155 183 211];
ZIntdeclared=find(ZIntNotif); XIntdeclared=find(XIntNotif);
toomuchSnan=(sum(isnan(SumPM))>100); toomuchXnan=(sum(isnan(XPM))>100); toomuchZnan=(sum(isnan(ZPM))>100);
display('excessive Nans in respect. Sum, X and Z data :');
sum(toomuchSnan)
sum(toomuchXnan)
sum(toomuchZnan)

scaling=dec/(352202/992);
SumPM2=SumPM(:,~toomuchSnan);
Cur=mean2(SumPM2,2);
[dummy,Curdroppoint]=max(-diff(Cur));
T=scaling*((1:10000)-Curdroppoint);
XPM2=XPM(1:Curdroppoint-2,~toomuchXnan);
ZPM2=ZPM(1:Curdroppoint-2,~toomuchZnan);
OV=std(ZPM2,0,2); OH=std(XPM2,0,2);
[OHmax,iFXmax]=max(OH); [OHmin,iFXmin]=min(OH);%Xend=mean2(XPM(Curdroppoint-4:Curdroppoint-1,:));
[OVmax,iFZmax]=max(OV); [OVmin,iFZmin]=min(OV);%Zend=mean2(ZPM(Curdroppoint-4:Curdroppoint-1,:));
TXmax=scaling*(iFXmax-Curdroppoint);
TZmax=scaling*(iFZmax-Curdroppoint);
TXmin=scaling*(iFXmin-Curdroppoint);
TZmin=scaling*(iFZmin-Curdroppoint);

motionstart=1;
motionX=find(OH>1.1*OHmin,1);
motionZ=find(OV>1.1*OVmin,1);
motionXZ=min([motionX, motionZ])-200;
if motionXZ>0;
    motionstart=motionXZ;
end;

Xdif=(XPM(iFXmax,:)-XPM(iFXmin,:))'; Zdif=(ZPM(iFZmax,:)-ZPM(iFZmin,:))';
OH(Curdroppoint-1:10000)=NaN; OV(Curdroppoint-1:10000)=NaN;

Trange=[T(motionstart) T(end)];
sbpm=(0.5+(0:223)')/224;
sticks=linspace(0,1,9);
slabels=[4:4:32 4];

figure('Position',[20, 20, 1500, 1000]);
ax1=subplot(2,2,1);
set(ax1,'FontSize',16,'DefaultTextFontSize',16);
plot(ax1,T(motionstart:end),Cur(motionstart:end),'.-k');
axt=axis(ax1);
axis(ax1,[Trange axt(3:4)]);
grid('on');
xlabel(ax1,'Time [millisec]');
ylabel(ax1,' Current [a.u]');
title(ax1,' Average of all BPMs Sum Signal ');

ax3=subplot(2,2,3);
set(ax3,'FontSize',16);
h1=plot(ax3,T(motionstart:end),OH(motionstart:end),'.-b','MarkerSize',12,'LineWidth',2); hold on ; 
h2=plot(ax3,T(motionstart:end),OV(motionstart:end),'.-r','MarkerSize',12,'LineWidth',2);
plot(ax3,TXmax,OHmax,'.b','MarkerSize',25);
plot(ax3,TZmax,OVmax,'.r','MarkerSize',25);
plot(ax3,TXmin,OHmin,'.g','MarkerSize',25);
plot(ax3,TZmin,OVmin,'.g','MarkerSize',25);
hold off
axp=axis(ax3);
axis(ax3,[Trange axp(3:4)]);
grid('on');
xlabel(ax3,'Time [millisec]');
ylabel(ax3,'[mm]');
title(ax3,' r.m.s. orbit');
legend(ax3,[h1 h2],'Horizontal','Vertical','Location','NorthWest');

ax2=subplot(2,2,2);
set(ax2,'FontSize',16);
plot(ax2,sbpm,XPM(iFXmax,:)','k','LineWidth',2); hold on ; 
plot(ax2,sbpm,XPM(iFXmin,:)','g','LineWidth',2); 
plot(ax2,sbpm,upxlim,'c','LineWidth',2);
plot(ax2,sbpm,lowxlim,'c','LineWidth',2);
plot(ax2,sbpm(XIntdeclared),XPM(iFXmax,XIntdeclared),'.r','MarkerSize',15);
plot(ax2,sbpm(XIntlkstations),XPM(iFXmax,XIntlkstations),'.m','MarkerSize',20);
hold off
ms=max([max(XPM2(iFXmax,:)), 2.1]) ; mi=min([min(XPM2(iFXmax,:)), -2.1]);
axis(ax2,[0, 1, mi, ms]); grid('on');
set(ax2,'XTick',sticks,'XTickLabel',slabels);
xlabel(ax2,'Cell', 'FontSize',16);
ylabel(ax2,'[mm]', 'FontSize',16);
title(ax2,'Horizontal Orbit');

ax4=subplot(2,2,4);
set(ax4,'FontSize',16);
plot(ax4,sbpm,ZPM(iFZmax,:)','k','LineWidth',2); hold on ; 
plot(ax4,sbpm,ZPM(iFZmin,:)','g','LineWidth',2); 
plot(ax4,sbpm,upzlim,'c','LineWidth',2);
plot(ax4,sbpm,lowzlim,'c','LineWidth',2);
plot(ax4,sbpm(ZIntdeclared),ZPM(iFZmax,ZIntdeclared),'.r','MarkerSize',15);
plot(ax4,sbpm(ZIntlkstations),ZPM(iFZmax,ZIntlkstations),'.m','MarkerSize',20);
ms=max([max(ZPM2(iFZmax,:)), 0.7]); mi=min([min(ZPM2(iFZmax,:)), -0.7]);
axis(ax4,[0, 1, mi, ms]); grid('on');
set(ax4,'XTick',sticks,'XTickLabel',slabels);
xlabel(ax4,'Cell');
ylabel(ax4,'[mm]');
title(ax4,'Vertical Orbit');

figure('Position',[20, 20, 1000, 600]);
%[corr,orbitfit]=dvautocor_svd(Xdif,'H');
[corr,orbitfit]=dvautocor_mosteff(Xdif,'H'); %#ok<NASGU>
figure('Position',[120, 120, 1000, 600]);
%[corr,orbitfit]=dvautocor_svd(Zdif,'V');
[corr,orbitfit]=dvautocor_mosteff(Zdif,'V'); %#ok<NASGU>
