function data = getpostmortem(attrname,tmout)
%GETPOSTMORTEM  read post-mortem data on the liberas
%
%DATA=GETPOSTMORTEM(ATTRNAME)
%
%ATTRNAME: attribute name
%DATA:     data buffer

dev=dvopen(['tango:sr/d-bpmlibera/all/' attrname]);
if nargin >= 2
    dvtimeout(dev, tmout);
end
data=reshape(dvx(@dvcmd, dev, 'DevRead'),[],224);
dvclose(dev);
end
