function resetPM()
%RESETPM Reset all PM notifications
%   Detailed explanation goes here

dev=dvopen('tango:sr/d-bpmlibera/all');
trydev(dev,'ResetInterlocksNotification');
trydev(dev,'ResetPMNotification');
dvclose(dev);

function trydev(dev,cmd)
for trial=1:3
    pause(2);
    try
        dvx(@dvcmd,dev,cmd);
        break;
    catch
        err=lasterror;
        if ~strncmp(err.identifier,'Machdev:',8)
            rethrow(err);
        end
    end
end
end

end
