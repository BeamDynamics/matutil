function data = testpostmortem(attrname)
%GETPOSTMORTEM  read post-mortem data on the liberas
%
%DATA=GETPOSTMORTEM(ATTRNAME)
%
%ATTRNAME: attribute name
%DATA:     data buffer

dev=dvopen(['tango:sr/d-bpmlibera/all/' attrname]);
dvquery(dev);
dvclose(dev);
end
