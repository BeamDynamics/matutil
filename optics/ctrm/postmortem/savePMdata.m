function dd=savePMdata2(filename)
%SAVEPMDATA Read and save Postmortem data
%
bpm=tango.Device('sr/d-bpmlibera/all');
bpm.Timeout=5;
dd.date=now;
dd.PMNotifications=bpm.All_PM_Notified.read;
dd.PMCounters=double(bpm.All_PM_Notified_counter.read);
dd.XItlkNotifications=bpm.All_InterlockXNotified.read;
dd.ZItlkNotifications=bpm.All_InterlockZNotified.read;
fprintf(1, '>> Flags recorded\n');
dd.PMSum=bpm.Sum_PM.read';
fprintf(1, '>> Sum data recorded\n');
pause(1);
dd.PMXPosition=bpm.X_Position_PM.read';
fprintf(1, '>> X_Position data recorded\n');
pause(1);
dd.PMZPosition=bpm.Z_Position_PM.read';
fprintf(1, '>> Z_Position recorded\n');
try
    bpm.ResetInterlocksNotification();
    fprintf(1, '>> Interlocks reset\n');
catch err
    if isa(err,'cs.DevError')
        fprintf(2,err.message);
    else
        rethrow(err);
    end
end
try
    bpm.ResetPMNotification();
    fprintf(1, '>> PM Notifications reset\n');
catch err
    if isa(err,'cs.DevError')
        fprintf(2,err.message);
    else
        rethrow(err);
    end
end

if nargin >= 1
    save(filename,'-struct','dd');
end
end
