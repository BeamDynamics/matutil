function [zp,za]=idload(cell)

psingle=load(['Zres' int2str(cell) 'p_single']);
puni=load(['Zres' int2str(cell) 'p_uni']);
zp=psingle.Zres(:,5)-puni.Zres(:,5);
asingle=load(['Zres' int2str(cell) 'a_single2']);
auni=load(['Zres' int2str(cell) 'a_uni2']);
za=asingle.Zres(:,5)-auni.Zres(:,5);
