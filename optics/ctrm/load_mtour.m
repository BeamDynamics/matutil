function [vx,aveorbit]=load_mtour(fn,plane)

if ischar(plane)
   if strcmp(plane,'h'), suffix='.X';
   elseif strcmp(plane,'v'), suffix='.Z';
   else error('Plane should be ''h'' or ''v''');
   end
else
   if plane == 0,suffix='.X';
   elseif plane == 1, suffix='.Z';
   else error('Plane should be 0, 1, ''h'' or ''v''');
   end
end
%badbpm=[3 39 52 68 94 110 122 136 164 168 185];
badbpm=[3 39 52 68 94 110 136 164 168 185];	%interlock BPMS

rotate=[22:224 1:21];
fname=[getenv('APPHOME') '/mille_tours/' fn suffix];
fid=fopen(fname,'rb');
if fid >= 0
   disp(['Loading data from ' fname]);
   vx=fread(fid,[224 Inf],'float');
   fclose(fid);
   nturns=size(vx,2);
%  vx(badbpm,:)=NaN*ones(length(badbpm),nturns);
   vx(badbpm,:)=NaN;
   vx=vx(rotate,:)';
   disp([int2str(nturns) ' turns']);
   aveorbit=mean2(vx);
   vx=vx-aveorbit(ones(nturns,1),:);
else
   error(['Cannot open file ' fname]);
end
