function disp_b(vx,ibpm,plane)

[nturns,nbpms]=size(vx);
[bpmname,kdx]=srbpmname(ibpm);
w=hann_window(nturns);
if nturns <= 1024, ndisp=1024; else, ndisp=2048; end
vxft=fft([vx;zeros(ndisp-nturns,nbpms)]);
vxftw=fft([vx.*w(:,ones(1,nbpms));zeros(ndisp-nturns,nbpms)]);
frequency=(0:ndisp-1)'/ndisp;

peaks=mtmax(abs(vxft(1:ndisp/2,1)));
fprintf('%.0f %f\n',[peaks;frequency(peaks)']);
[y,k]=sort(-abs(vxft(1:ndisp/2,:)));	%detect peak amplitude below nu=0.5
tune=frequency(k(1,:));

subplot(2,1,1);
plot(vx);
title(['BPM ' bpmname '/' upper(plane) ' (' int2str(kdx) ')']);
xlabel('turns');
ylabel('\Delta x (mm)');
grid on
subplot(2,1,2)
plot(frequency,abs([vxft vxftw]));
hold on
plot(frequency(peaks), abs(vxft(peaks)),'o');
hold off
xlabel('tune');
posgr(0.1,0.8,sprintf(['\\nu_' plane '=%g\n'], tune));

function w=hann_window(n)
w=0.5*(1-cos(2*pi*(0:n-1)'/n));
