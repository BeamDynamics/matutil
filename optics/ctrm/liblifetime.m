function [lt,errlt]=liblifetime(span)
%LIBLIFETIME         Computes lifetime from all the SR liberas
%LIFETIME=LIBLIFETIME(SPAN)
%
%SPAN:  Integration time [s]
%
dev=dvcache('SAsum','tango:sr/d-bpmlibera/all/sum_sahistory');
sumSA=dvx(@dvcmd,dev,'DevRead');
sumSA=reshape(sumSA,[],224);

nb=10*span;             % number of points at 10 Hz
t=0.100946*(0:nb-1)';        % time vector

figure(3);
ltinv=liblifecompute(t, sumSA(end-nb+1:end,:),1);% compute 224 lifetimes

[bad1,bad2,bad3]=libliferejectbpm(ltinv,2,2,2); % reject "bad" ones
ok=~any([bad1;bad2;bad3]);
rejected=sum([bad1;bad2;bad3],2)

figure(1);
[a,b,erra]=liblifecompute(t, sum(sumSA(end-nb+1:end,ok),2),1);
lt=-1./a;
errlt=erra./a./a;
lt2=-1./mean(ltinv(ok));			% or take the average of 1/lifetime
lt3=mean(-1./ltinv(ok));			% or take the average of lifetime
lt4=-1./median(ltinv(ok));			% or take the median of 1/lifetime
disp([lt lt2 lt3 lt4]/3600);

% figure(2);              % Plot all
% subplot(2,1,1);
% bar(-1./ltinv/3600);
% subplot(2,1,2);			% Plot good
% bar(-1./ltinv(ok)/3600);

%eval(sprintf('!say ''lifetime is %5.2f hours''',lt/3600));
end
