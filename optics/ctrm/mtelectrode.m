function [A,B,C,D] = mtelectrode(fn)
% reads the 4 electrode files and returns
% in 4 arrays (1 for each electrode) the 224 stations and N turns

A=rdf([getenv('APPHOME') '/mille_tours/' fn '.ElA']);
B=rdf([getenv('APPHOME') '/mille_tours/' fn '.ElA']);
C=rdf([getenv('APPHOME') '/mille_tours/' fn '.ElA']);
D=rdf([getenv('APPHOME') '/mille_tours/' fn '.ElA']);

function v=rdf(fname)
fid=fopen(fname, 'rb');
Temp=fread(fid, Inf, 'float');
v=reshape(Temp, 224, length(Temp)/224)';
fclose(fid);
