function [r1,r2]=posgr(fx,fy,labtext)

xl=get(gca,'XLim');
yl=get(gca,'YLim');
x=xl(1)+fx*(xl(2)-xl(1));
y=yl(1)+fy*(yl(2)-yl(1));
if nargin >= 3
   r1=text(x,y,labtext);
else
   r1=x;
   r2=y;
end
