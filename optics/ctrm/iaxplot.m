function iaxplot(plane)


global MACHFS

measbase=fullfile(MACHFS,'MDT');
dirname=uigetdir(fullfile(measbase,datestr(now(),'yyyy'),''),'Select the data directory:');
if ~ischar(dirname), error('qem:nodir','No directory selected'); end

[bpm,~]=sr.load_resp(dirname,plane,1:6:96); 
[iax,~]=load_sriaxresp(dirname,plane,1:6:96); 

for cell=[5,10,11,14,18,21,25,26,29,31,3]
%iaxidx=find([5,10,11,14,18,21,25,26,29,31,3]==cell);
ziax=iax(mod(cell-4,32)+1,:);
zbpm=bpm(sr.bpmindex(cell,3),:);
coef=sum(zbpm.*ziax)/sum(zbpm.*zbpm) %#ok<NOPRT,NASGU>
plot(1.0E6*[zbpm;ziax]')
title(sprintf('C%i',cell));
xlabel('Steerer #');
ylabel('z [\mum]');
legend('BPM','IAX')
grid on
pause
end
