function [ltinv,oa,err]=liblifecompute(t,SASum,cplot)
%LIBLIFECOMPUTE		Fits the inverse of lifetime of a current array
%
%LTINV=LIBLIFECOMPUTE(T,SASUM,CPLOT)
%
%T:     Time vector (s)
%SASUM:	SA sum history buffer (NPOINTS x NBPMS)
%CPLOT: column of SUMSA to be plotted with the fit (default: no plot)
%
%LTINV: inverse of lifetime [s-1]
%
%[LTINV,LOGI0,ERRLT]=LIBLIFECOMPUTE(...)
%       Also returns the log of beam current at the origin and the error on
%       inverse lifetime

[npts,nbpm]=size(SASum);
sumlog=log(SASum);
sx=sum(t);
sy=sum(sumlog);
sy2=dot(sumlog,sumlog);
sxy=dot(t(:,ones(1,nbpm)),sumlog);
sx2=dot(t,t);

sigx2=npts*sx2-sx.*sx;
sigy2=npts*sy2-sy.*sy;
covxy=npts*sxy-sx.*sy;

ltinv=covxy./sigx2;
oa=(sy.*sx2-sx.*sxy)./sigx2;
err=sqrt((sigy2-covxy.*covxy./sigx2)./((npts-2)*sigx2));

%a=[ones(nb,1) t]\sumlog;	% Linear fit of 1/lifetime on all BPMs
%ltinv=a(2,:);
%oa=a(1,:);


if nargin >= 3			% Optional plot
    deltai=SASum(1,cplot)-SASum(npts,cplot);
    curlog=sumlog(:,cplot)-sumlog(1,cplot);
    plot(t,curlog,'.',t,oa(cplot)+ltinv(cplot)*t-sumlog(1,cplot),'r-');
    text(t(2),curlog(npts-1),{['lifetime ' num2str(-1/ltinv(cplot)/3600) ' h'];...
        ['error ' num2str(err(cplot)./ltinv(cplot)./ltinv(cplot)/3600)];...
        ['\DeltaI ' num2str(deltai)]});
end
end