function [ oscill,aveorbit ] = load_dd(plane,varargin)
%LOAD_DD Load turn-by-turn position
%
%OSCILL=LOAD_DD(PLANE)
%  PLANE:  h, H, x, X, 1 => horizontal
%        v, V, z, Z, 2 => vertical
%        s,S,0         => sum
%  OSCILL: Turn-by-turn positions (nturnsx224), 1st bpm is C4-1
%
%OSCILL=LOAD_DD(PLANE,KICKER)
%  Sends forst a "On" command on KICKER and waits for the data
%
%[OSCILL,COD]=LOAD_DD(PLANE)
%OSCILL: Beam oscillation with average subtracted
%COD:    average beam position
%

bpm=tango.cache('sr/d-bpmlibera/all', @tango.SrBpmDevice);
oscill=bpm.read_dd(plane,varargin{:})';
nturns=size(oscill,1);
disp([int2str(nturns) ' turns']);
if nargout >= 2
    aveorbit=mean2(oscill);
    oscill=oscill-aveorbit(ones(nturns,1),:);
end
