function [corr,orbitfit,orbitmeas] = orbit_pb(plane,method,orbit,reforbit)
%ORBIT_PB Analyzes orbit difference
%[CORRE,ORBITFIT,ORBITMEAS] = ORBIT_PB(PLANE,METHOD,ORBIT,REFORBIT)
%
%Analyses and plots the difference of two orbits
%
%PLANE:    h|H|x|X|1 or v|V|z|Z|2
%METHOD:   ignored (most effective only at the moment)
%ORBIT:	   file name of the perturbated orbit
%REFORBIT: file name of the reference orbit (optional, default: no reference)
%
%CORR:	   10x3 matrix of most effective steerers (index, strength, chi2)
%ORBITFIT: orbit fitted with the most effective corrector
%ORBITMEAS:initial orbit difference
%

if nargin < 1
    plane       =	input('Orbit plane to be analysed (z or x):  ','s');
    orbit       =	input('File name of disturbed orbit saved with SRCO (e.g. bad_orbit):  ','s');
    reforbit 	=	input('File name of reference orbit (optional)  (e.g. ref_orbit):  ','s');
else
    if nargin < 4, reforbit = ''; end
end

if ischar(plane)
    switch upper(plane)
        case {'H','X'}
            plane=1;
        case {'V','Z'}
            plane=2;
        otherwise
            error('Orbit:plane','plane should be h|H|x|X|1 or v|V|z|Z|2');
    end
end

orbit = ['/operation/appdata/sr/co/orbit/' orbit];
if ~isempty(reforbit), reforbit = ['/operation/appdata/sr/co/orbit/' reforbit]; end

refo=loadorb(reforbit);
o=loadorb(orbit);
orbitmeas=o-refo;
[corr,orbitfit]=dvautocor_mosteff(orbitmeas,plane);

    function orbit=loadorb(orbit)
        if ischar(orbit)
            disp(['>' orbit '<']);
            if isempty(orbit)
                orbit=zeros(224,1);
            else
                orb=zeros(224,2);
                [orb(:,1),orb(:,2),intens,comment]=load_orbit(orbit);
                orbit=orb(:,plane);
            end
        end
    end
end




