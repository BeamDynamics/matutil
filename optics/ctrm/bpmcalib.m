function [bpmvalx,bpmvalz]=bpmcalib(bpm,motor,prange,nmeas)
%BPMVAL=BPMCALIB(BPM,MOTOR,PRANGE,NMEAS)
%
%bpm=machdev.TangoDevice('SR/D-BPMLIBERA-TEST/C23-8');
%motorx=machdev.TangoDevice('SR/D-MOT/C22-BPM-H');
%prange=-1.1:0.1:1;
%
tmeas=5;
if nargin < 4, nmeas=1; end
ok=motor.check('On',@(dev) dev.IsState(machdev.DevState.On),{2,2});
bpmvalx=NaN(length(prange),nmeas);
bpmvalz=NaN(length(prange),nmeas);
for meas=1:nmeas
    motor.Position=2*prange(1)-prange(2);
    motor.check(@(dev) ~dev.IsState(machdev.DevState.Moving),{1,30});
    pause(2);
    for i=1:length(prange)
        motor.Position=prange(i);
        pause(3+tmeas);
        bpmvalx(i,meas)=mean(bpm.XPosSAHistory.read(end-10*tmeas:end));
        bpmvalz(i,meas)=mean(bpm.ZPosSAHistory.read(end-10*tmeas:end));
    end
end

motor.Position=0;
motor.check(@(dev) ~dev.IsState(machdev.DevState.Moving),{1,30});
try
    motor.cmd('GoHome');
catch
    disp('Could not go home');
end

end
