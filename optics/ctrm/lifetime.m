function lt=lifetime(dev, nb)

current=zeros(nb,1);
t=zeros(nb,1);
tic
for i=1:nb
[s,r]=dvread(dev);
current(i)=r;
t(i)=toc;
pause(1);
end
b=log(current);
deltai=current(1)-current(nb);
a=[ones(nb,1) t]\b;
bref=log(current(1));
plot(t,b-bref,'o',t,a(1)+a(2)*t-bref,'r-');
lt=-1/a(2);
text(t(2),b(nb-1)-bref,{['lifetime ' num2str(lt/3600) ' h'];['\DeltaI ' num2str(deltai)]});
eval(sprintf('!say ''lifetime is %5.2f hours''',lt/3600));
