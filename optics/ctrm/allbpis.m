function fig=allbpis(varargin)

offset=0:2000:14000;
Zecht=NaN*ones(1024,8);
it=zeros(1024,8);

for j=1:length(varargin)
   if ~isempty(varargin{j})
      Zecht(:,j)=zp(varargin{j})+offset(j);
      it(:,j)=varargin{j}(:,4);
   end
end

keep=(mean(it(700:1000,:))>200);
it2=it(:,keep)/5+offset(ones(size(it,1),1),keep)-500;

fig=figure ('Position',[10, 30, 450, 680]);

plot(Zecht,'.b');
hold on
if ~isempty(it2), plot(it2,'m');end
plot([1;840],[offset;offset],'k');
plot([1;1024],[[offset;offset]-700,[offset;offset]+700],'--r');

axis([1, 1000, -1000 , 15000]);
set(gca, 'YTick',reshape([offset-700;offset;offset+700],1,24));
set(gca, 'YTicklabel','-0.7mm |C1|+0.7mm|-0.7mm|C8|+0.7mm|-0.7mm |C10|+0.7mm|-0.7mm|C14|+0.7mm|-0.7mm |C16|+0.7mm|-0.7mm|C20|+0.7mm|-0.7mm |C24|+0.7mm|-0.7mm|C27|+0.7mm')
set(gca, 'XTick',[60,214,368,522,676,830,984]);
set(gca, 'XTicklabel','-100|-80|-60|-40|-20|0|t[ms]');
text(850*ones(8,1),offset',...
   {'C1 zero';'C8 zero';'C10 zero';'C14 zero';...
   'C16 zero';'C20 zero';'C24 zero';'C27 zero'});
title ('Blue : Z pos. of all 8 BPIs over ~100ms before Interlock (Purple)');

function Zecht=zp(C1)
Zecht=C1(:,1);
zer=(C1(:,1)==0);
Zecht(zer)=-C1(zer,2);
E=sum(C1(:,5:8),2);
Dif=[0;-diff(E)];
[md,ns]=max(Dif);
if ns > 4, Zecht(ns-4:end)=NaN; end
