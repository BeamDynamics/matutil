function [tabx,tabz]=pbpm(bpm,sz,x0,z0)

tabx=NaN*ones(224,sz);
tabz=tabx;
try
    for i=1:sz
        pause(2)
        [x,z]=dvreadbpm(bpm);
        tabx(:,i)=x-x0;
        tabz(:,i)=z-z0;
        figure(1);
        surf(tabx);
        figure(2);
        surf(tabz);
    end
catch
end