function [x,xp]=mtxxp(vx,range)

%range: turns to take into account

dist=6.1052*ones(1,32);
dist(27)=7.2052;	% 6 m straights in ID30

rg1=1:size(vx,1);
if nargin >= 2
   rg1=eval(['rg1(' range ')']);
end
x2=vx(rg1,1:7:218);		% select downstream bpms
if rg1(1) == 1			% select upstream bpms
   x1=[[NaN;vx(rg1(2:end)-1,224)] vx(rg1,7:7:217)];
else
   x1=[vx(rg1-1,224) vx(rg1,7:7:217)];
end
x =(x2+x1)*0.5;
xp=(x2-x1)./dist(ones(size(x1,1),1),:);
