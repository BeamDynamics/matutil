function [vx0,intens,stdx,stdi] = mtkick(kickdev,strength,naverage,fname)
%MTKICK Averages TbT data over several kicks
%
%[AVEXZ,AVEI,STDXZ,STDI]=MTKICK(KICKDEV, STRENGTH, NAVERAGE, FILENAME)
%
%Kick the beam and record TbT data over several kicks. Compute and save
%the average value.
%
%KICKDEV:  Kicker device
%          KICKDEV(1): main device,
%          KICKDEV(2): optional, strength attribute
%STRENGTH: Kicker strength (A)
%NAVERAGE: averaging number
%FILENAME: File name for saving the average in a file compatible with
%          MTPANEL
%
%AVEXZ:    TbT position (nturns x 224 x 2)
%AVEI:     TbT beam intensity
%STDXZ:    standard deviation between the position measurements
%STDI:     standard deviation between the beam intensity measurements

if ~(isempty(kickdev) || isempty(strength))
    kickdev.Current=strength;               % set kicker strength once
end
[nturns,nbpms]=size(load_dd('h'));          % get buffer size
allx=NaN(nturns,nbpms,naverage);            % initialize arrays
allz=NaN(nturns,nbpms,naverage);
alli=NaN(nturns,nbpms,naverage);
k=0;
while k<naverage
    try
        if ~isempty(kickdev)
            kickdev.On();
        end
        pause(4);
        k=k+1;
        allx(:,:,k)=load_dd('h');
        allz(:,:,k)=load_dd('v');
        alli(:,:,k)=load_sumdd();
        fprintf('BPM dataset %d loaded\n',k);
    catch
        pause(1);
    end
end
if (naverage > 1)
%    vx0=cat(3,mean2(allx,3),mean2(allz,3));    % Average over acquisitions
%    intens=mean2(alli,3);                      % Average over acquisitions
%    stdx=cat(3,std2(allx,1,3),std2(allz,1,3)); % Deviation between acquisitions
%    stdi=std2(alli,1,3);                       % Deviation between acquisitions
    vx0=[];                                     % Skip averaging
    intens=[];
    stdx=[];
    stdi=[];
else
    vx0=cat(3,allx,allz);
    intens=alli;
    stdx=[];
    stdi=[];
end
if nargin >= 4 && ischar(fname) && ~isempty(fname)
    save(fname,'vx0','intens','stdx','stdi','allx','allz');
end
end

