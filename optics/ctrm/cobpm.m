function [vx,vz]=cobpm

filen=input('reference file name:','s');
[ax,az]=load_orbit([getenv('APPHOME') '/sr/co/orbit/' filen]);
rotat=[204:224 1:203];
vx=1000*ax(rotat);
vz=1000*az(rotat);
