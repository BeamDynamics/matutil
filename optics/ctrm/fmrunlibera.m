function fmrunlibera(xk,yk,dpath)
% CODE=FMRUNLIBERA(HKICKER,VSHAKER,DIRNAME)	performs a MT map measurement
%
% HKICKER :	array of H kicker values
% VSHAKER :	array of V shaker values
% DIRNAME : 	Directory where MT files will be saved
%
% CODE :	1= beam lost
%		2= kicker failure
%		3= BPM error

global DEVS libh libv

if isempty(DEVS)
   DEVS=dvopen('SR/PS-K/1','+tango:sr/ps-k/vertical','+tango:sr/ps-k/vertical/vpfn');
end
if isempty(libh)
   libh=dvopen('tango:sr/d-bpmlibera/c8-3/XPosDD',...
   'tango:sr/d-bpmlibera/c10-5/XPosDD',...
   'tango:sr/d-bpmlibera/c14-3/XPosDD',...
   'tango:sr/d-bpmlibera/c16-5/XPosDD',...
   'tango:sr/d-bpmlibera/c20-3/XPosDD',...
   'tango:sr/d-bpmlibera/c24-3/XPosDD',...
   'tango:sr/d-bpmlibera/c27-3/XPosDD',...
   'tango:sr/d-bpmlibera/c1-3/XPosDD');
end
if isempty(libv)
   libv=dvopen('tango:sr/d-bpmlibera/c8-3/ZPosDD',...
   'tango:sr/d-bpmlibera/c10-5/ZPosDD',...
   'tango:sr/d-bpmlibera/c14-3/ZPosDD',...
   'tango:sr/d-bpmlibera/c16-5/ZPosDD',...
   'tango:sr/d-bpmlibera/c20-3/ZPosDD',...
   'tango:sr/d-bpmlibera/c24-3/ZPosDD',...
   'tango:sr/d-bpmlibera/c27-3/ZPosDD',...
   'tango:sr/d-bpmlibera/c1-3/ZPosDD');
end

[xf,yf]=ndgrid(xk,yk);
code=1;
%[s,bid]=unix('sr_test 2>/dev/null');
%if bitand(s,2) ~= 0, return; end
save([dpath '/currents.mat'],'xk','yk');

for j=1:size(xf,2)
dvcmd(DEVS(1),'DevStandby');
dvcmd(DEVS(2),'Standby');
pause(5);
for i=1:size(xf,1)
   fname=[dpath '/X' num2str(xf(i,j),'%04.0f') 'Z' num2str(100*yf(i,j),'%04.0f')];
   mess=['horizontal ' num2str(xf(i,j)) ', vertical ' num2str(yf(i,j))];
   if exist([fname 'h.mat'],'file') ~= 2 | exist([fname 'v.mat'],'file') ~= 2
      dvcmd(DEVS(1),'DevSetValue',xf(i,j))		% set currents
      dvcmd(DEVS(3),'DevWrite',yf(i,j));
      pause(2);
      dvcmd(DEVS(1),'DevOn');
      dvcmd(DEVS(2),'DevOn');
      pause(1);

      code=3;
      [xx,err]=dvcmd(libh, 'DevRead');
      if any(err ~= 0)
	 errfmrun(DEVS,code);
      end
      [zz,err]=dvcmd(libv, 'DevRead');
      if any(err ~= 0)
	 errfmrun(DEVS,code);
      end

      code=2;						% check currents
      kstate=dvcmd(DEVS(1),'DevState');
      if kstate == 2, code=0; end
      if code ~= 0
	 errfmrun(DEVS,code);
      end
      dvcmd(DEVS(1),'DevStandby');
      dvcmd(DEVS(2),'Standby');

      code=1;						% check beam
      [s,bid]=unix('sr_test 2>/dev/null');
      if bitand(s,2) == 0, code=0; end
%     code=0;
      if code ~= 0
	 errfmrun(DEVS,code);
      end

      xx=xx';
      zz=zz';
      subplot(2,1,1);
      plot(xx);
      subplot(2,1,2);
      plot(zz);
      save([fname 'h'],'xx')	% save file
      save([fname 'v'],'zz')	% save file

%     eval(['!say ''' mess ', done'''])
   else
      disp(['skipping ' mess]);
%     eval(['!say ''skipping ' mess '''']);
   end
end
dvcmd(DEVS(1),'DevOff');
pause(2);
end
!say 'measurement finished'

dvcmd(DEVS(3),'DevWrite',0);		% set shaker to 0

function errfmrun(devs,code)
pause(2);
dvcmd(devs(1),'DevOff');				% stop kicker
dvcmd(devs(3),'DevWrite',0);		% set shaker to 0
dvcmd(devs(2),'Standby');
message=['measurement error, code ' num2str(code)];
eval(['!say "' message '"']);
error('Frmap:error',message);
