plane=input('Plane (h/v): ','s');
if exist('wrong') == 1
   [vx,aveorbit]=load_mtour(fn, plane, wrong);
else
   [vx,aveorbit]=load_mtour(fn, plane);
   wrong=[];
end
nturns=size(vx,1);
orbits=std2(vx');
threshold=0.1*max(orbits);
on=find(orbits > threshold);
first=on(1)
coher=on(length(on))-first

figure(2);
clf
subplot(3,1,1);
plot(1000*orbits);
xlabel('turns');
ylabel('rms. orbit (\mum)');
title('OSCILLATION DAMPING');
posgr(0.75, 0.8, {['first: ' int2str(first)],['duration: ' int2str(coher)]});

subplot(3,1,2);
rmsorbit=std2(aveorbit)
plot(1000*aveorbit);
xlabel('Bpm #');
ylabel('Average orbit (\mum)');
title('ORBIT');
posgr(0.02, 0.92, {['Rms orbit: ' num2str(1000*rmsorbit,3)],...
	['wrong: ' int2str(wrong)]});

out=find(abs(aveorbit) > 4.0*rmsorbit);
wrong=[wrong mod(out+20,224)+1]

subplot(3,1,3);
plot(1000*std2(vx));
xlabel('Bpm #');
ylabel('Rms orbit (\mum)');
title('FLUCTUATIONS');
posgr(0.02, 0.92, ['Average noise: ' num2str(1000*mean2(std2(vx)'),3)]);
drawnow

vxft=fft(vx);
frequency=(0:nturns-1)'/nturns;
amplitude=reshape(max(abs(vxft)),14,16);
figure(1);
clf
plot(amplitude);
set(gca, 'Xlim', [1 14]);
xlabel('Bpm #');
ylabel('Harmonic amplitude');
