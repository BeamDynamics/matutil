function orbit = dvautocor_simul(kicks, plane)
%DVAUTOCOR_SIMUL Compute orbit response to kicks
%   ORBIT=DVAUTOCOR_SIMUL(KICKS,PLANE)
%

global APPHOME

hv='hv';
pad={0 []};
if ischar(plane)
   switch upper(plane)
   case {'H','X'}
	  plane=1;
   case {'V','Z'}
	  plane=2;
   otherwise
	  error('Orbit:plane','plane should be h|H|x|X|1 or v|V|z|Z|2');
   end
end

a=load([APPHOME '/sr/optics/settings/theory/' hv(plane) '_svd.mat']);
vects=a.lambda.*a.lambda.*(a.svdsolve'*[kicks;pad{plane}]);
o=a.svdbase*vects;
orbit=o(1:224);

