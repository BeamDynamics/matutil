function lt=ctlifetime(nb)
%CTLIFETIME         Computes lifetime from the current transformer
%
%LIFETIME=CTLIFETIME(SPAN)
%
%SPAN:  Integration time [s]
%LIFETIME: Lifetime [s]
%
dev=dvcache('CT','+tango:sr/d-ct/1/current');
current=zeros(nb,1);
t=zeros(nb,1);

tic
for i=1:nb          % Acquisition loop
[s,r]=dvx(@dvread,dev);
current(i)=r;
t(i)=toc;
pause(1);
end

lt=-1./liblifecompute(t,current,1);
eval(sprintf('!say ''lifetime is %5.2f hours''',lt/3600));
end
