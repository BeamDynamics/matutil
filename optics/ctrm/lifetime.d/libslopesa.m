function [a,erra]=libslopesa(span,wait,cplot)
%LIBSLOPESA         Computes inverse lifetime from all the SR liberas
%LTI=LIBSLOPESA(SPAN)
%
%SPAN:  Integration time [s]
%LTI  : inverse of lifetime [s-1]
%       The function uses the Libera 10 Hz buffer. It therefore computes the
%       lifetime over the past 'SPAN' seconds
%
%LTI=LIBSLOPESA(SPAN,'wait')
%       Starts integrating when called: it computes the lifetime over the 'SPAN'
%       next seconds
%
%LTI=LIBSLOPESA(SPAN,'wait',CPLOT)
%  CPLOT: 0 => no plot (default)
%        >0 => plots the fit for the current of bpm(cplot)
%        -1 => plots the fit for the summ of currents
%
%[LTI,ERR]=LIBSLOPESA(...)
%       returns also the estimated error on the inverse lifetime
%
if nargin < 3, cplot=0; end
if (nargin >= 2) && ischar(wait) && strcmp(wait,'wait')
    pause(span+1)
end
dev=dvcache('SAsum','tango:sr/d-bpmlibera/all/sum_sahistory');
sumSA=dvx(@dvcmd,dev,'DevRead');
sumSA=reshape(sumSA,[],224);

nb=10*span;             % number of points at 10 Hz
t=0.100946*(0:nb-1)';   % time vector

ltinv=liblifecompute(t, sumSA(end-nb+1:end,:),cplot);% compute 224 lifetimes

[bad1,bad2,bad3]=libliferejectbpm(ltinv,2,2,2); % reject "bad" ones
ok=~any([bad1;bad2;bad3]);
rejected=sum([bad1;bad2;bad3],2) %#ok<NOPRT,NASGU>

[a,b,erra]=liblifecompute(t, sum(sumSA(end-nb+1:end,ok),2),-cplot);

end
