function [lt,errlt]=liblifetime(varargin)
%LIBLIFETIME         Computes the lifetime from all the SR liberas
%
%LIFETIME=LIBLIFETIME(SPAN)
%
%SPAN:  Integration time [s]
%LIFETIME: Lifetime [s]
%       The function uses the Libera 10 Hz buffer to compute the lifetime. It
%       therefore computes the lifetime over the past 'SPAN' seconds
%
%LIFETIME=LIBLIFETIME(SPAN,'wait')
%       Starts integrating when called: it computes the lifetime over the 'SPAN'
%       next seconds
%
%LIFETIME=LIBLIFETIME(SPAN,'wait',CPLOT)
%  CPLOT: 0 => no plot (default)
%        >0 => plots the fit for the current of bpm(cplot)
%        -1 => plots the fit for the summ of currents
%
%[LIFETIME,ERR]=LIBLIFETIME(...)
%       returns also the estimated error on the lifetime value
%
[a,erra]=libslopesa(varargin{:});
lt=-1./a;
errlt=erra./a./a;

eval(sprintf('!say ''lifetime is %5.2f hours''',lt/3600));
end
