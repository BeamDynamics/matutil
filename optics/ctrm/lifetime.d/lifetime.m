function lt=lifetime(dev, nb)
%CTLIFETIME         Computes lifetime from the current transformer
%
%LIFETIME=CTLIFETIME(SPAN)
%
%SPAN:  Integration time [s]
%LIFETIME: Lifetime [s]
%
disp(' ');
disp('This function conflicts with F.E.''s function "LifeTime"');
disp(' ');
disp('Instead you should use "ctlifetime" or "liblifetime"');
disp(' ');
lt=[];
%current=zeros(nb,1);
%t=zeros(nb,1);
%tic
%for i=1:nb
%[s,r]=dvread(dev);
%current(i)=r;
%t(i)=toc;
%pause(1);
%end
%lt=-1./liblifecompute(t,current,1);
%eval(sprintf('!say ''lifetime is %5.2f hours''',lt/3600));
