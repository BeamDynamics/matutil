function [linac_pulses,bunch_list]=filpat2(npulses, spacing, repeat, nbunches, jump)
%[linac_pulses,bunch_list]=filpat_grid(npulses,spacing,repeat,nbunches,jump)	Computes filling
%
%NPULSES:	Number of linac pulses
%SPACING:	Spacing between linac pulses
%REPEAT:	Number of repetitions
%NBUNCHES:	Number of bunches (default: npulses*spacing*repeat, to get full bunch trains)
%JUMP:		Spacing between repetitions (default: 992/repeat, to get equidistant trains)

trainlength=spacing;
if nargin < 5, jump=floor(992/repeat); end
if nargin >= 4, trainlength=floor(nbunches/npulses/repeat); end
if trainlength > spacing, error('Filpat:blist','Too many bunches'); end
ntr=0:(trainlength-1);
linac_pulses=spacing*(0:(npulses-1));
rpt=jump*(0:(repeat-1))';
bunch_list=rpt(:,ones(1,trainlength))+ntr(ones(repeat,1),:);
bunch_list=bunch_list(:);
