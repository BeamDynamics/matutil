function [vdate,vstd,count,vduration]=pattern_evolution(data,mode,varargin)
%PATTERN_EVOLUTION	Process data from the periodic measurement of filling pattern
%
%[VDATE,VSTD,COUNT,DURATION]=PATTERN_EVOLUTION(DATA,MODE)
%
%VDATE:     Meaasurement date as datetime values
%VSTD:      Relative pattern homogeneity
%COUNT:     Total number of counts
%DURATION:  Total measurement integration time

persistent bl
if isempty(bl), bl=bunch_list(); end

if ischar(data)
    fdata=load(data);
    data=fdata.data;
end
[sb_position,args]=getoption(varargin,'SBPosition',1); %#ok<ASGLU>
bunches=bl.get(mode,sb_position);
[count,vstd,daten,vduration]=arrayfun(@scan,data);
vdate=datetime(daten,'ConvertFrom','datenum','TimeZone','local');

   function [vsum,vstd,fdate,duration]=scan(a)
        v=a.profile(bunches);
        sx2=sum(v.*v);
        sx=sum(v);
        vstd=sqrt(sum(bunches)*sx2/sx/sx-1);
        vsum=sum(a.total_count);
        duration=sum(a.duration);
        fdate=a.fdate;
    end
end
