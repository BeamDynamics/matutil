function peak=mtmax(ampl)

dima=length(ampl);
dampl=diff(ampl);
amin=min(ampl);
np=5;
peak=zeros(1,np);
for i=1:np
[amax,im]=max(ampl);
peak(i)=im;
i2=im+find(dampl(im:dima-1) >= 0)-1;
i1=im-find(dampl(im-1:-1:1) <= 0)+1;
ampl(i1:i2)=amin;
end
