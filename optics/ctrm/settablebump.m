function [orbith,orbitv]=settablebump(dev,listh,amplh,listv,amplv,nstep)
%SETBUMP(DEV,LISTH,AMPLH,LISTV,AMPLV,NSTEPS)	rises orbit bumps
%
% DEV : AutoCor device handle
% LISTH : list of horizontal bump numbers (1:96)
% AMPLH : list of horizontal amplitudes
% LISTV : list of vertical bump numbers (1:96)
% AMPLV : list of vertical amplitudes
% NSTEPS : number of steps
%
%[orbith,orbitv]=setbump(dev,listh,amplh,listv,amplv)
%          simulates the bumps and returns the simulated orbits on the BPMs
%
%setbump(dev,listh,amplh,listv,amplv,nsteps)
%          applies the bumps
global VSTEER BVTABLE

if isempty(BVTABLE)
%  BVTABLE=load_bump([getenv('APPHOME') '/sr/optics/settings/theory/v_short_bumps']);
   error('SetBump:Error','No calibration table.');
end
if isempty(VSTEER)
   VSTEER=dvopensteer(1);
end

if (nargin < 6) nstep=0; end

nh=prod(size(listh));
nv=prod(size(listv));

if (nh ~= 0)
   error('SetBump:Error','Wrong argument size.');
end
if (prod(size(amplv)) ~= 1) && (prod(size(amplv)) ~= nv)
   error('SetBump:Error','Wrong argument size.');
end

if nstep > 0
   bv=zeros(size(BVTABLE,2),1);
   bv(listv)=amplv;
   incrv=BVTABLE*bv;
   [vini,bid]=dvreadsteer(VSTEER);
   for frac=(1:nstep)/nstep
      for trynum=1:3
         dvsetsteer(VSTEER,vini+incrv*frac);
	 disp(['step ' num2str(frac)]);
%	disp(['step ' num2str(frac) ', error ' int2str(err)]);
%	if err == 0
             break;
%	elseif err ~= 58
%	   error('SetBump:Error','Error %i while setting bump.',err);
%	end
 	 pause(1);
      end
      pause(2);
   end
   pause(3);
else
   bumpvalue=[nh 0 nv 0 0 reshape(listh-1,1,nh) zeros(1,nh) reshape(listv-1,1,nv) zeros(1,nv)];
   bumpvalue(5+nh+(1:nh))=amplh(:)';
   bumpvalue(5+2*nh+nv+(1:nv))=amplv(:)';
   [output,err]=dvcmd(dev,'DevAutoCorTestBump',bumpvalue);
   disp(['step 1, error ' int2str(err)]);
   if err ~= 0
      error('SetBump:Error','Error %i while setting bump.',err);
   end
   nbpm=length(output)/2;
   orbith=output(1:nbpm);
   orbitv=output(nbpm+1:2*nbpm);
   plot(orbitv');
   axis([0 25 -0.002 0]);
end
