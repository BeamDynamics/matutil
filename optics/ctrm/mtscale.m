function betaall=mtscale(beta,alpha,ampl)

%MTSCALE: scales amplitudes to match betas at end of high-beta straights
%         and returns betas on all BPMs

d=3.0526*ones(1,32);
d(27)=3.6026;	% 6 m straights in ID30

									% Compute beta at end of straight
gamma=(1+alpha.*alpha)./beta;
beta12=[beta+2*d.*alpha+d.*d.*gamma;beta-2*d.*alpha+d.*d.*gamma];

idm=reshape([224 1:223],7,32);		% Extract amplitude at same points
ampl12=[ampl(idm(1,:));ampl(idm(2,:))];

coefs=beta12./ampl12./ampl12;		% Compute ratio

%[mean2(mean(coefs(:,1:2:32))') std2(std(coefs(:,1:2:32))')]	%high beta
%[mean2(mean(coefs(:,2:2:32))') std2(std(coefs(:,2:2:32))')]	%low beta

%coef=mean2(mean(coefs)');		% all ID
coef=mean2(mean(coefs(:,1:2:32))');	% high beta
%coef=mean2(mean(coefs(:,2:2:32))');	% low ID

betaall=ampl.*ampl*coef;
