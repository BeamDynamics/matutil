function ok = isdevstate(dev,okstate)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

% DEVOFF:       1
% DEVON:        2
% DEVMOVING:    9
% DEVSTANDBY:   12
state=dvx(@dvcmd,dev,'DevState');
ok=(state == okstate);
end

