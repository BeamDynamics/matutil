function code=sextuloop(reson)
%SEXTULOOP	Scans the sextupole for maximizing the lifetime
%
%CODE=SEXTULOOP(RESONANCE)
%
%RESONANCE:	Resonance to be corrected
%		1: 3*NuX = 109
%		2: NuX + 2*NuZ = 63
%
%CODE:	error code
%		0 : Success
%		1 : Correction amplitude too small, cannot scan
%		2 : Beam drift, scan failed
%

if nargin < 1
    reson=1;
elseif ischar(reson)
    reson=str2double(reson);
end
switch reson
    case 1
        devs=dvcache('x3nux','sr/ampl-7/p109','sr/phase-7/p109');
    case 2
        devs=dvcache('x2nuxpnuz','sr/ampl-9/p59','sr/phase-9/p59');
end

% 5 stdevs, 3 measurements
% 0.1 amplitude step
% 0.5 amplitude max.
% 20   deg. phase step

%code=resonloop(devs, @() measfun(20),{5,3},0.1,0.5,20);
code=resonloop(devs, @() measfun(20),0.25e-6,0.1,0.5,20);

% 20 s integration time

    function [b,errb]= measfun(integ)
        [a,erra]=libslopesa(integ,'wait');
        b=-a;
        errb=4*erra;
    end

end
