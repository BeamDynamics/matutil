function [emz,erremz,em]=averzemittance(nave)
%AVERZEMITTANCE		Read the average vertical emiitance on IAX
%
%[EMZ,ERREMZ]=AVERZEMITTANCE(NAVE)
%
%NAVE	: Averaging number
%
%EMZ	: Average value of the vertical emittance over NAVE values
%ERREMZ	: Standard deviation of the NAVE measurements
%
if nargin<1, nave=5; end
dev=dvcache('emz','tango:sr/d-emit/average/Zemittance');
pause(1);
em=NaN(nave,1);
for i=1:nave
pause(1);
[bid,em(i)]=dvx(@dvread,dev);
disp(i);
end
emz=mean2(em);
erremz=std2(em);
