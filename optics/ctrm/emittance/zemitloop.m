function code=zemitloop
%ZEMITLOOP	Scans the coupling resonance for minimizing the vertical emittance
%
%CODE=ZEMITLOOP
%
%CODE:	error code
%		0 : Success
%		1 : Correction amplitude too small, cannot scan
%		2 : Beam drift, scan failed
%
persistent inited
devs=dvcache('nuxmnuz','sr/ampl-24/p22','sr/phase-24/p22');
if isempty(inited)
    dvsetparam(devs(1),'ResonSetOffset',...
        '/operation/appdata/sr/optics/settings/theory/base_skew2');
    inited=true;
end

% 0.05 pm significance threshold
% 0.01 amplitude step
% 0.12 amplitude max.
% 10   deg. phase step

code=resonloop(devs, @()averzemittance(5,1),0.00005,0.01,0.12,10);

% Emittance measurement, average 5, wait 1
