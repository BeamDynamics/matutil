function code=phaseloop

persistent inited
devs=dvcache('nuxmnuz','sr/ampl-18/p22','sr/phase-18/p22');
if isempty(inited)
    dvsetparam(devs(1),'ResonSetOffset',...
        '/operation/appdata/sr/optics/settings/theory/base_skew2');
    inited=true;
end

[amplini,bid]=dvx(@dvread,devs(1));
[phaseini,bid]=dvx(@dvread,devs(2));
disp(amplini);
code=[];
if abs(ampl)<0.1, return; end

[vact,vbest,moved]=phasel(devs(2),phaseini);

[vact,vbest,moved]=amplloop(devs(1),amplini);

function [vact,vbest,moved]=phasel(phasedev,phaseini)
phasestep=10;
vp=degrange(phaseini,phasestep);
vm=-degrange(-phaseini,phasestep);
try
    [vact,vbest,moved]=paramloop(phasedev,@()averzemittance(5),vp,vm);
catch
    err=lasterror;
    if strcmp(lasterror.identifier,'ParamLoop:Drift')
    else
    rethrow(err);
    end
end

function [vact,vbest,moved]=amplloop(ampldev,amplini)
amplstep=0.01;
vp=amplini+amplstep:amplstep:0.12;
vm=amplini-amplstep:-amplstep:0;
try
    [vact,vbest,moved]=paramloop(ampldev,@()averzemittance(5),vp,vm);
catch
end

function result=degrange(vini,step)
vdeg=180*vini/pi;
vv=vdeg+step:step:vdeg+180;
overf=vv>180;
vv(overf)=vv(overf)-360;
result=vv*pi/180;
