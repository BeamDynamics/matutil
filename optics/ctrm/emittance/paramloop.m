function [vact,vbest,moved]=paramloop(devact,measfunc,nstd,vp,vm)
%PARAMLOOP	Looks for the minimum value of a parameter
%
%[VACT,VBEST,MOVED]=PARAMLOOP(DEV,MEASFUNC,NSTD,VP,VM)
%
%DEV:      device handle for variable parameter
%MEASFUNC: function returning the value to be minimized, as [val,errval]=MEASFUNC;
%          VAL: measured value, ERRVAL: accuracy of the measurement (std. dev.)
%NSTD:     if numerical: fixed significance threshold
%          if cell array: NSTD{1} Number of std deviations for the significance threshold
%                         NSTD{2} nb o measurements for threshold determination
%VP:	   array of values to be tested in the first direction
%VM:	   array of values to be tested in the other direction
%
%VACT:	Best value for the variable
%VBEST:	Corresponding minimum parameter value
%MOVED:	logical indicating if the best result is different from the initial value
%
setpause=6;                   % Pause between resonance setting and measurement
%fprintf('initial, measure\n');
if iscell(nstd)
    nini=3;                   % Reference taken on 3 measurements
    if length(nstd) >= 2, nini=nstd{2}; end
    vv=NaN(nini,1);
    errvv=NaN(nini,1);
    for i=1:nini
        [vv(i) errvv(i)]=measfunc();
    end
    vini=mean2(vv);
    threshold=nstd{1}*mean2(errvv);
else
    [vini,bid]=measfunc();
    threshold=nstd(1);
end
[vactini,bid]=dvx(@dvread,devact); %#ok<NASGU>
fprintf('initial %g -> %g, (%g-%g)\n',vactini,vini,vini-threshold,vini+threshold);
vact=vactini;
vbest=vini;

moved=false;
for vnext=vp                % try 1st direction
    dvx(@dvset,devact,vnext);             % set correction
%   fprintf('up %g\n',vnext);
    pause(setpause);
%   fprintf('up %g, measure\n',vnext);
    [vv,bid]=measfunc(); %#ok<NASGU>      % measure result
    bad=(vbest-vv) < threshold;
    fprintf('up %g -> %g (%d)\n',vnext,vv,bad);
    if bad; break; end	% No improvement => stop
    vact=vnext;
    vbest=vv;
    moved=true;
end

if ~moved                   % if it fails, try other direction
    for vnext=vm
        dvx(@dvset,devact,vnext);        % set correction
%       fprintf('down %g\n',vnext);
        pause(setpause);
%       fprintf('down %g, measure\n',vnext);
        [vv,bid]=measfunc(); %#ok<NASGU> % measure result
        bad=(vbest-vv) < threshold;
        fprintf('down %g -> %g (%d)\n',vnext,vv,bad);
        if bad; break; end	% No improvement => stop
        vact=vnext;
        vbest=vv;
        moved=true;
    end
end

if moved
    dvx(@dvset,devact,vactini);	% check if nothing changed during optimization
    pause(1.5*setpause);
    [vv,bid]=measfunc(); %#ok<NASGU>
%   bad=abs(vini-vv) > threshold;   % check nothing moved
    bad=vbest > (vv-threshold);     % check improvement compared to initial
    fprintf('check %g -> %g (%d)\n',vactini,vv,bad);
    if bad; error('ParamLoop:Drift','Drift during optimisation'); end
end
dvx(@dvset,devact,vact);
fprintf('final %g\n',vact);
end
