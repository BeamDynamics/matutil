function code=resonloop(devs,measfunc,nstd,amplstep,amplmax,phasestep)
%RESONLOOP	Scans pahse and amplitude of a resonance correction
%
%CODE=RESONLOOP(DEVS,MEASFUNC,NSTD,AMPLSTEP,AMPLMAX,PHASESTEP)
%
%DEVS:     DEVS(1):phase, DEVS(2):amplitude of the correction
%MEASFUNC: function returning the value to be minimized, as [val,errval]=MEASFUNC;
%          VAL: measured value, ERRVAL: accuracy of the measurement (std. dev.)
%NSTD:     if numerical: fixed significance threshold
%          if cell array: NSTD{1} Number of std deviations for the significance threshold
%                         NSTD{2} nb o measurements for threshold determination
%AMPLSTEP: Step in correction amplitude
%AMPLMAX:  Maximum corretion amplitude
%PHASESTEP:Step in correction phase
%
%CODE:	error code
%		0 : Success
%		1 : Correction amplitude too small, cannot scan
%		2 : Beam drift, scan failed
%

if nargin < 6, phasestep=10; end
if nargin < 5, amplmax=0.12; end
if nargin < 4, amplstep=0.01; end
if nargin < 3, nstd={5,3}; end
[amplini,bid]=dvx(@dvread,devs(1)); %#ok<NASGU>
[phaseini,bid]=dvx(@dvread,devs(2)); %#ok<NASGU>
code=1;
%if abs(amplini)<0.01, return; end
code=0;
try
    [vact,vbest,moved]=phaseloop(devs(2),phaseini,phasestep);	% Look for best phase
    [vact,vbest,moved]=amplloop(devs(1),amplini,amplstep,amplmax);	% Look for best amplitude
catch
    err=lasterror;
    if strcmp(err.identifier,'ParamLoop:Drift')
        code=2;
    else
        rethrow(err);
    end
end

function [vact,vbest,moved]=phaseloop(phasedev,phaseini,phasestep)
phaseinideg=180*phaseini/pi;
vp=pi*degrange(phaseinideg,phasestep)/180;
vm=-pi*degrange(-phaseinideg,phasestep)/180;
[vact,vbest,moved]=paramloop(phasedev,measfunc,nstd,vp,vm);
end

function [vact,vbest,moved]=amplloop(ampldev,amplini,amplstep,amplmax)
vp=amplini+amplstep:amplstep:amplmax;
vm=amplini-amplstep:-amplstep:0;
[vact,vbest,moved]=paramloop(ampldev,measfunc,nstd,vp,vm);
end

function vv=degrange(vdeg,step)
vv=vdeg+step:step:vdeg+180;
overf=vv>180;
vv(overf)=vv(overf)-360;
end

end
