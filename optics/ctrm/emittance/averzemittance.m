function [emz,erremz,em]=averzemittance(nave,period)
%AVERZEMITTANCE		Read the average vertical emittance on IAX
%
%[EMZ,ERREMZ]=AVERZEMITTANCE(NAVE)
%
%NAVE	: Averaging number (default 5)
%WAIT	: Time to wait between 2 measurements [s] (default 1)
%
%EMZ	: Average value of the vertical emittance over NAVE values
%ERREMZ	: Standard deviation of the NAVE measurements
%
if nargin<2, period=1; end
if nargin<1, nave=5; end
dev=dvcache('emz','tango:sr/d-emit/average/Zemittance');
em=NaN(nave,1);
for i=1:nave
    pause(period);
    [bid,em(i)]=dvx(@dvread,dev);
end
emz=mean2(em);
erremz=std2(em);
