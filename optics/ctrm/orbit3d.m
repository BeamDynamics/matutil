function orbit3d(run,f1st,select)

global APPHOME
if nargin < 3, select=15; end
if nargin < 2, f1st=[]; end
if nargin < 1, run=''; end
if isempty(run), run='run'; end
dname=fullfile(APPHOME,'sr','co','orbit','archive',['orbit.' run]);
d=dir(dname);
[x,ok]=sort(datenum(cat(1,d.date)));
ok(cat(1,d(ok).isdir))=[];
if ~isempty(f1st), ok(1:f1st-1)=[]; end
flist={d(ok).name}';
tlist={d(ok).date}';
nok=length(ok);
orbitx=NaN*ones(224,nok);
orbitz=orbitx;
for i=1:nok
[orbitx(:,i),orbitz(:,i),intens,comment]=load_orbit(fullfile(dname,flist{i}));
end
orbitx=1.e6*(orbitx-repmat(orbitx(:,1),1,nok));
orbitz=1.e6*(orbitz-repmat(orbitz(:,1),1,nok));
leglist=cell(size(select));
for i=1:numel(select), leglist{i}=srbpmname(select(i)); end

figure(1);
subplot(2,2,1);
surf(datenum(tlist),1:224,orbitx);
shifttick('x',19);
subplot(2,2,3);
plot(datenum(tlist),orbitx(select,:));
shifttick('x',19);
legend(leglist{:});
grid on
subplot(2,2,2);
plot(orbitx(:,end));
title(tlist{end});
ax=axis;
axis([1 224 ax(3:4)]);
grid on

figure(2);
subplot(2,2,1);
surf(datenum(tlist),1:224,orbitz);
shifttick('x',19);
subplot(2,2,3);
plot(datenum(tlist),orbitz(select,:));
shifttick('x',19);
legend(leglist{:});
grid on
subplot(2,2,2);
plot(orbitz(:,end));
title(tlist{end});
ax=axis;
axis([1 224 ax(3:4)]);
grid on
