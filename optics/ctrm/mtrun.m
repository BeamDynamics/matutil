function code=mtrun(tmout)

mtourdev=dvcache('mtour','SR/MTOUR/UP');

dvcmd(mtourdev,'DevStart')		% start acquisition sequence

for i=1:tmout/2			% wait for end of sequence
   pause(2);
   code=dvcmd(mtourdev, 'DevState');
   if code ~= 9, break;end
end
