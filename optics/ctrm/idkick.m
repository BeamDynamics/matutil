function [cor,fit]=idkick(cell,orbit)

nu=14.39;
cnum=mod(cell-4,32)+1;
[respv,bpm,steerer]=respmat('sr','sv',nu);
blist=bumpbpmlist(bpm,steerer);

[rv,bpm,hilo]=respmat('sr','hilov',nu);
rxv=responsexm(bpm(:,2:3),hilo(:,2:3),nu);
cellresp=[rv(:,cnum) rxv(:,cnum)];

bnum=reshape([96 1:95],3,32);
%bok=logical(ones(224,1));
bok=isfinite(orbit);
bok([blist{bnum(1,cnum)};blist{bnum(2,cnum)}])=logical(0);
cor=cellresp(bok,:)\orbit(bok);
fit=cellresp*cor;
legend('Measurement','Fit');
plot(844.39*bpm(:,1),1.e6*[orbit fit]);
xlabel('s (m)');
ylabel('V orbit (\mum)');
legend('Measurement','Fit');
grid on
set(gca,'Xlim',[0 844.39]);
