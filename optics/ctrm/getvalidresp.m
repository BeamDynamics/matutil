function [bpm_ok,sth_ok,stv_ok] = getvalidresp()
%GETVALIDRESP       Get the list of active BPMs and steerers
%
%[BPMOK,SHOK,SVOK]=GETVALIDRESP
%
%BPMOK:     Valid BPMs (logical 224x1)
%SHOK:      Valid horizontal steerers (logical 1x96)
%SVOK:      Valid horizontal steerers (logical 1x96)
%

%                   Get disabled BPMs
liberadev=tango.Device('sr/d-bpmlibera/all');
bpmstate=liberadev.All_Status.read;
bpm_ok=(bitand(uint32(bpmstate)',uint32(2))==0);

%                   Get disabled H steerers
sthdev=tango.Device('sr/st-h/all');
hstates=sthdev.SteererStates.read;
sth_ok=(hstates==tango.DevState.On);

%                   Get disabled V steerers
stvdev=tango.Device('sr/st-v/all');
vstates=stvdev.SteererStates.read;
stv_ok=(vstates==tango.DevState.On);
end
