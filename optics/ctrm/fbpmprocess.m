%[fname,finfo]=scanfile;
%save multiok fname finfo
%load multiok
%%ok=finfo(:,2) > 150 & finfo(:,3) == 2;
%ok=finfo(:,2) > 150;
%gname=fname(ok);
%ginfo = finfo(ok,:);
%%					54: eliminated (too low)
%high=[39 40 45 46 49:53 57 58 61 62 65:68];
%low=[35:38 43 44 63 64];
%fdbk=[47 48 55 56 59 60];
%[tims,vals]=procfile(gname,ginfo(:,1),'h');
%[tims,vals]=procfile(gname,ginfo(:,1),'h',2:12);	% 0<f<50 Hz
%beg=datevec(tims(1));
%lst=datevec(tims(end));
%months=(beg(2):lst(2)+1);
%scalen=datenum(beg(1)*ones(length(months),1),months',ones(length(months),1));
%plot(tims,1000*vals(:,fdbk));
%ylabel('rms position (\mum)');
%set(gca,'Xtick',scalen,'XtickLabel',datestr(scalen,3));
%set(gca,'Ylim',[0 15]);
%grid on

[info,rt,rf,spec]=epfile(gname{end});
spec=permute(spec,[1 3 2]);
spec=reshape(spec(:,1:2,:),50,34);
