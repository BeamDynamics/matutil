function endcor = skewclosedbump(mach0,bpmidx,skewidx)
%SKEWCLOSEDBUMP computes skew bump closure
%   Detailed explanation goes here

method=2;
switch method
   case 2
	  skewstrength=zeros(3,1);
	  skewstrength(2)=1.e-3;
	  [resp1,resp2]=semrdtresp(mach0,bpmidx,skewidx);
	  f1=resp1*skewstrength;
	  f2=resp2*skewstrength;
	  rsp=[...
		 real(resp1(:,[1 3]));imag(resp1(:,[1 3]));...
		 real(resp2(:,[1 3]));imag(resp2(:,[1 3]))];
	  b=[...
		 real(f1);imag(f1);...
		 real(f2);imag(f2)];
	  endcor=(-rsp\b)/skewstrength(2);
   case {3,4,5,6,7}
	  mach=setcellstruct(mach0,'PolynomA',skewidx(2),1.e-3,2);	%set skew error
	  options=optimset(optimset('fminsearch'),'Display','iter');
	  [fun,options]=semcorargs(method,mach,bpmidx,skewidx([1 3]),options);
	  inicor=[0;0];
	  options.TolFun
	  endcor=fminsearch(fun,inicor,options);
end
end

