function eps=mtinvariant(vx,betaid,alphaid)

[xx,xxp]=mtxxp(vx);			%extract x,x' in IDs
nt=size(xx,1);
betall=betaid(ones(nt,1),:);
xpn=xx.*alphaid(ones(nt,1),:)+xxp.*betall;
eps=(xx.*xx+xpn.*xpn)./betall;
