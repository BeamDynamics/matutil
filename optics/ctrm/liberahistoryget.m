function orbits=liberahistoryget(plane)

if isnumeric(plane)
    plchar='XZ';
    plane=plchar(plane+1);
end
switch upper(plane)
    case {'H','X'}
        libname='tango:SR/D-BPMLIBERA/ALL/X_POSITION_SAHISTORY';
    case {'V','Z'}
        libname='tango:SR/D-BPMLIBERA/ALL/Z_POSITION_SAHISTORY';
end
libera=dvopen(libname);
vals=dvcmd(libera,'DevRead');
vals=reshape(vals,1800,224);
orbits=vals(:,[22:224 1:21]);