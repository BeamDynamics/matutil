function [idx,nord,cell]=cbpm(kbpm)
%IDX=CBPM(KDX) returns the index of SR BPM with "harware" index KDX
%
%  [IDX,ID,CELL]=CBPM(KDX) returns in addition the ID and cell number
%
%   See also KBPM, SRBPMNAME, SRBPMINDEX

if nargout >1
   cell=floor((kbpm-1)/7)+1;
   nord=kbpm-7*(cell-1);
end
idx=kbpm-21;
idx=idx+224*(idx<1);
