function [bcount,bindex] = getbunchcount(purity)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
bcount=purity.BunchCount.read;
base=double(purity.BucketNumber.read+purity.BunchCountOriginOffset.read+1);
bindex=mod(base-(1:length(bcount)),992);
end

