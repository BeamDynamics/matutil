function monispecrecord3
load lastfile
n=10;
fbpm_list=[ 3 24 ]';
sz=size(fbpm_list,1)
monisprecord=zeros(456,sz,4);
size(monisprecord(:,sz,1))
for j=1:sz,
cell=fbpm_list(j);
ds='sr/d-fbpm/c';
if cell==6|cell==8|cell==14|cell==16|cell==22|cell==24|cell==30|cell==32;
dev=[ds,num2str(cell),'s:mca'];
else
dev=[ds,num2str(cell),'m:mca'];
end
fbpmsp=dvopen(dev);
for i=1:n,

monisprecord(:,j,1)=monisprecord(:,j,1)+dvcmd(fbpmsp,'DevRead',8)';
monisprecord(:,j,2)=monisprecord(:,j,2)+dvcmd(fbpmsp,'DevRead',9)';
monisprecord(:,j,3)=monisprecord(:,j,3)+dvcmd(fbpmsp,'DevRead',10)';
monisprecord(:,j,4)=monisprecord(:,j,4)+dvcmd(fbpmsp,'DevRead',11)';
end
monisprecord(1,:,:)=.00001;
monisprecord(:,j,1)=20*log10(abs(monisprecord(:,j,1))/n);
monisprecord(:,j,2)=20*log10(abs(monisprecord(:,j,2))/n);
monisprecord(:,j,3)=20*log10(abs(monisprecord(:,j,3))/n);
monisprecord(:,j,4)=20*log10(abs(monisprecord(:,j,4))/n);
end
id14fb=dvopen('fe/d-xfdbk/id14');
id21fb=dvopen('fe/d-xfdbk/id21');
id24fb=dvopen('fe/d-xfdbk/id24');
Gfdbk=dvopen('sr/d-fdbk/global');
CT=dvopen('sr/d-ct/1');
ID14state=dvcmd(id14fb,'DevState');
ID21state=dvcmd(id21fb,'DevState');
ID24state=dvcmd(id24fb,'DevState');
GFDBKstate=dvcmd(Gfdbk,'DevState');
Iread=dvcmd(CT,'DevReadValue');
Ibeam=Iread(1);
prevfile=lastfile;
numclock=clock;
lastfile=[date,'_',num2str(numclock(4)),'h',num2str(numclock(5))]
save([num2str(lastfile),'_2K'],'monisprecord','ID14state','ID21state','ID24state','GFDBKstate','Ibeam','prevfile');
save lastfile lastfile;
dvclose(id14fb);
dvclose(id21fb);
dvclose(id24fb);
dvclose(Gfdbk);
dvclose(CT);

