function setemittanceproperties(varargin)
%SETEMITTANCEPROPERTIES Set the attributes of the "orbit" device
%
% SETEMITTANCEPROPERTIES()          use $APPHOME/sr/optics/settings/theory
% SETEMITTANCEPROPERTIES('opname')  use $APPHOME/sr/optics/settings/opname
% SETEMITTANCEPROPERTIES(path)      uses the specified path
% SETEMITTANCEPROPERTIES(atstruct)	uses the given AT structure

vsr=sr.model(varargin{:});
espread=1.06e-3;

%  PINHOLE CAMERAS
% get optics
[vpin{1:4}]=vsr.get([3 4 6 7 8 9 11 12],'pin_d9','pin_d11','pin_id25','pin_id25');
% names of tango properties
lab={'betax','alphax','etax','etapx','betaz','alphaz','etaz','etapz','sigmaex','sigmaez'};
% names of pinhole objects
objname={'optic-d9-emit';'optic-d11_lens-emit';'optic-id25_1-emit';'optic-id25_lens-emit'};
for id=1:4
    labval=[lab;num2cell([vpin{id} espread espread])];
    setpinhole(objname{id},labval{:});
end
for pinext={'d11','d9-a','d9-b','id25-a','id25-b'}
    pinname=['sr/d-emit/' pinext{1}];
    try
        pindev=tango.Device(pinname);
        pindev.Init();
        fprintf('Re-initialized %s\n',pinname);
    catch err
        warning('MATLAB:tango','Cannot re-initialize %s',pinname);
        disp(err.message)
    end
end

%  IAX MONITORS

vsr.iaxlist=[5;10;11;14;18;21;25;26;29;31;3];   % Select IAX positions
viax=vsr.get([8 11],'iax');                     % get betaz, alphaz
iaxname=vsr.name('iax');                        % get device names
lab={'betaz','etaz','sigmaez'};                 % names of tango properties
for iax=1:size(viax,1)
    labval=[lab;num2cell([viax(iax,:) espread])];
    setiax(iaxname{iax},labval{:});
end

    function setpinhole(pinname,varargin)
        try
            pinobj=tango.Object(pinname);
            pinobj.set_property(varargin{:})
            fprintf('Set properties on %s\n',pinname);
        catch err1
            warning('MATLAB:tango','Cannot set properties on %s',pinname);
            disp(err1.message)
        end
    end

    function setiax(iaxname,varargin)
        try
            iaxdev=tango.Device(iaxname);
            iaxdev.set_property(varargin{:})
            fprintf('Set properties on %s\n',iaxname);
            iaxdev.Init()
            fprintf('Re-initialized %s\n',iaxname);
        catch err2
            warning('MATLAB:tango','Cannot set properties on %s',iaxname);
            disp(err2.message)
        end
    end
end
