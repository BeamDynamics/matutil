function v=plot_electrode(fname, device, electrode)

fn=[getenv('APPHOME') '/mille_tours/' fname]
fid=fopen(fn, 'rb');
if fid > 0
   vx=fread(fid, Inf, 'ushort');
   fclose(fid);
   lg=length(vx);
   ntours=lg/224
   elec=reshape(vx,lg/8,8);
   vals=reshape(elec(:,device),lg/32,4);
   v=reshape(vals(:,electrode), 7, ntours)';
else
   disp(['Cannot open file ' fn]);
   v=[];
end
