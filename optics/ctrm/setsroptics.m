function setsroptics(varargin)
%SETSROPTICS Set the attributes of the "orbit" device
%
% SETSROPTICS()         use $APPHOME/sr/optics/settings/theory
% SETSROPTICS('opname')	use $APPHOME/sr/optics/settings/opname
% SETSROPTICS(path)     uses the specified path
% SETSROPTICS(atstruct)	uses the given AT structure

vsr=sr.model(varargin{:});
[iddata,d_Vbeta]=vsr.get([0 3:4 6:9],'id',8,'bm');
idvars=num2cell(iddata,1);
[ididx,id_Hbeta,id_Halpha,id_Heta,id_HetaPrime,id_Vbeta,id_Valpha]=deal(idvars{:});
sl=2*vsr.getfieldvalue(ididx+1,'Length');
emh=4.0e-9;
emv=4.0e-12;

try
    orbitdev=tango.Device('sr/d-orbit/1');
    orbitdev.id_Hbeta=id_Hbeta';
    orbitdev.id_Halpha=id_Halpha';
    orbitdev.id_Vbeta=id_Vbeta';
    orbitdev.id_Valpha=id_Valpha';
	orbitdev.id_Heta=id_Heta';
	orbitdev.id_HetaPrime=id_HetaPrime';
    orbitdev.d_Vbeta=d_Vbeta';
    orbitdev.ref_H_emit=emh;
    orbitdev.ref_V_emit=emv;
    orbitdev.straight_section_length=sl';
    fprintf(2,'Attributes of the orbit device are updated\n');
catch err
    disp(err)
    warning(err.identifier,'Could not set orbit device attributes:\n%s',err.errorstack(1).desc);
end
tryInit('sr/reson-quad/m2_n0_p73','Setup resonance correction devices');

    function tryInit(devname,message)
        try
            tango.Device(devname).Init();
            fprintf(2,[message ' done\n']);
        catch
            fprintf(2,['Could not ' message '\n']);
        end
    end
end

