idlist=[1:3 6 8:24 26:32];
for id=idlist
   [descr,devname,signame]=iddescr(id);
   filename=[getenv('APPHOME') '/equip_list/Ins. Devices/ID' num2str(id)];
   [fid,message]=fopen(filename,'wt');
   if fid >= 0
      fprintf(fid,'@tGaps of ID%i\n',id);
      for gap=1:length(descr)
         fprintf(fid,'\n@n%s\n@m %s:vfelem#L[%s]\n@d %s\n',descr{gap},devname{gap},descr{gap},signame{gap});
      end
      fclose(fid);
   else
      disp(message)
   end
end
quit
