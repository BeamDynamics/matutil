function [beta,alpha,eps]=mtabg(x,xp)

betaeps=mean(x.*x);
gammeps=mean(xp.*xp);
alpheps=-mean(x.*xp);
eps=sqrt(betaeps.*gammeps-alpheps.*alpheps);
epsm=aveps(eps)
beta=betaeps./epsm;
alpha=alpheps./epsm;
%beta=betaeps./eps;
%alpha=alpheps./eps;
eps=2*eps;

function epsm=aveps(eps)
ok=(eps-mean2(eps) < 3*std2(eps));
epsm=mean(eps(ok));
