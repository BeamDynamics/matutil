function monispecrecord
% error in the averaging rsulting in a -10dB error on the 
%recorded spectrums
% not corrected to stay consistent with the previous records
%corrected at the display...

n=10;
fbpm_list={...
'sr/d-fbpm/c1m';...
'sr/d-fbpm/c3m';...
'sr/d-fbpm/c6s';...
'sr/d-fbpm/c8s';...
'sr/d-fbpm/c9m';...
'sr/d-fbpm/c12m';...
'sr/d-fbpm/c14s';...
'sr/d-fbpm/c16s';...
'sr/d-fbpm/c18m';...
'sr/d-fbpm/c20m';...
'sr/d-fbpm/c21m';...
'sr/d-fbpm/c22s';...
'sr/d-fbpm/c24s';...
'sr/d-fbpm/c26m';...
'sr/d-fbpm/c27m';...
'sr/d-fbpm/c30s';...
'sr/d-fbpm/c32s'};

gadcm=dvopen(fbpm_list);
sz=size(gadcm,1);
monisprecord=NaN*ones(50,sz,4);

for j=1:sz
   rms2=zeros(1,4);
   specm=zeros(64,4);
   nok=0;
   disp(fbpm_list{j});
   for i=1:n
      buffer=dvcmd(gadcm(j),'DevRead',16)';
      if ~isempty(buffer)
	 spec=reshape(buffer(1:256),64,4);
	 rms=buffer(256+[2 4 6 8])';
	 specm = specm + spec.^2;
	 rms2 = rms2 + rms.^2;
	 nok=nok+1;
      end
   end

   specm(1,:)=sqrt(rms2/nok);
   spec=10*log10(specm/1024/1024/nok/n)+60;	% n should not be there
   monisprecord(:,j,:)=spec(1:50,:);

end
dvclose(gadcm)

id14fb=dvopen('fe/d-xfdbk/id14');
id21fb=dvopen('fe/d-xfdbk/id21');
id24fb=dvopen('fe/d-xfdbk/id24');
Gfdbk=dvopen('sr/d-fdbk/global');
CT=dvopen('sr/d-ct/1');
ID14state=dvcmd(id14fb,'DevState');
ID21state=dvcmd(id21fb,'DevState');
ID24state=dvcmd(id24fb,'DevState');
GFDBKstate=dvcmd(Gfdbk,'DevState');
Iread=dvcmd(CT,'DevReadValue');
Ibeam=Iread(1);

load lastfile
prevfile=lastfile;
numclock=clock;
lastfile=[date,'_',num2str(numclock(4)),'h',num2str(numclock(5))]

save(lastfile,'monisprecord','ID14state','ID21state','ID24state','GFDBKstate','Ibeam','prevfile');
save lastfile lastfile;
dvclose(id14fb);
dvclose(id21fb);
dvclose(id24fb);
dvclose(Gfdbk);
dvclose(CT);
span=200;
%fspan=span/4.375;
%span=round(fspan);
%plot(monisprecord(2:span,:));
%axis([0 span -50 25]);
%grid;
%set(gca, 'xtick',[0;span/4;span/2;span*.75;span]);
%set(gca,'xticklabels',[0;fspan*4.375/4;fspan*4.375/2;fspan*4.375*.75;fspan*4.375]);
%title('monitor BPM noise spectrum ');
%xlabel(['Xaxis:4.4 Hz/unit ']);
%ylabel(['Yaxis: dBMicrometer/4.4HzBW']);
