devlist2={	'FE/D-XFDBK/ID14';...
		'FE/D-XFDBK/ID21';...
		'FE/D-XFDBK/ID24';...
		'SR/D-FDBK/GLOBAL'};

%bpss=dvopen('SY/PS/1');
dipo=dvopen('sy/PS-C/D');
fdbk=dvopen(devlist2);
ct=dvopen('SR/D-CT/1');

[vs,vr]=dvread(ct);			% current transformer

state=dvcmd(fdbk,'DevState');		% feedbacks
jfdbk = (state==2) | (state==45);

vdip=dvcmd(dipo,'DevReadVoltage');	% BPSS
if vdip > 400
   jdip = 2;
elseif vdip > 80
   jdip = 1;
else
   jdip =0;
end

dt=now;					% date snd time

fid=fopen([getenv('APPHOME') '/seismic/flags'],'at');
fprintf(fid,'%.8f\t%.3f\t%d\t%d\t%d\t%d\t%d\t\n',dt,vr,jfdbk,jdip);
fclose(fid);
