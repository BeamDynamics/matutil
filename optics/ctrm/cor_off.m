addpath /operation/machine/s700/matmex
clear
close all
% IMPORT DEVICES

s20_c5_id=dvopen('+sr/qp-s20/c5');
dvcmd (s20_c5_id,'DevOff');

s20_c7_id=dvopen('+sr/qp-s20/c7');
dvcmd (s20_c7_id,'DevOff');

s20_c9_id=dvopen('+sr/qp-s20/c9');
dvcmd (s20_c9_id,'DevOff');

s20_c11_id=dvopen('+sr/qp-s20/c11');
dvcmd (s20_c11_id,'DevOff');

s20_c13_id=dvopen('+sr/qp-s20/c13');
dvcmd (s20_c13_id,'DevOff');

s20_c15_id=dvopen('+sr/qp-s20/c15');
dvcmd (s20_c15_id,'DevOff');

s20_c17_id=dvopen('+sr/qp-s20/c17');
dvcmd (s20_c17_id,'DevOff');

s20_c19_id=dvopen('+sr/qp-s20/c19');
dvcmd (s20_c19_id,'DevOff');

s20_c21_id=dvopen('+sr/qp-s20/c21');
dvcmd (s20_c21_id,'DevOff');

s20_c23_id=dvopen('+sr/qp-s20/c23');
dvcmd (s20_c23_id,'DevOff');

s20_c25_id=dvopen('+sr/qp-s20/c25');
dvcmd (s20_c25_id,'DevOff');

s20_c27_id=dvopen('+sr/qp-s20/c27');
dvcmd (s20_c27_id,'DevOff');

s20_c29_id=dvopen('+sr/qp-s20/c29');
dvcmd (s20_c29_id,'DevOff');

s20_c31_id=dvopen('+sr/qp-s20/c31');
dvcmd (s20_c31_id,'DevOff');

s20_c1_id=dvopen('+sr/qp-s20/c1');
dvcmd (s20_c1_id,'DevOff');

s20_c3_id=dvopen('+sr/qp-s20/c3');
dvcmd (s20_c3_id,'DevOff');
