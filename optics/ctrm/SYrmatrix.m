function [x,xref,z,zref,vals,code]=SYrmatrix(steerh,steerv,nav)
% CODE=SYrmatrix(hst,vst,nav)	performs a response matrix
% measurement in the booster
%
%     hst :	array of H steerer values
%     vdt :	array of V steerer values
%     nav : 	number of averaged orbits
%
% CODE :	1= beam lost
%		2= kicker failure
%		9= MT timeout (sequence blocked)
%		23= MT Fault

for i=1:14
DEVSH{i}=['SY/PS-C5/CH' num2str(i)];
DEVSV{i}=['SY/PS-C6/CV' num2str(i)];
end

for i=15:21
DEVSH{i}=['SY/PS-C4/CH' num2str(i)];
DEVSV{i}=['SY/PS-C4/CV' num2str(i)];
end

DEVSV{21}='TL1SY/PS-C1/CV21';

for i=22:25
DEVSH{i}=['TL1SY/PS-C1/CH' num2str(i)];
DEVSV{i}=['TL1SY/PS-C1/CV' num2str(i)];
end

for i=26:39
DEVSH{i}=['SY/PS-C2/CH' num2str(i)];
DEVSV{i}=['SY/PS-C3/CV' num2str(i)];
end

BPM=dvopen('sy/bpm/master_up');
dvsetparam(BPM,'BpmAverage',nav);


for i=1:39

% reference orbit
  [xref{2*i-1},zref{2*i-1}]=dvreadbpm(BPM,'wait');

% horizontal steerer manipulation 
  DEV=dvopen(DEVSH(i));
  val=dvread(DEV);
  vals(2*i-1)=val(1)+steerh
  dvcmd(DEV,'DevSetValue',vals(2*i-1));
  pause(1);
  
% check steerer state    
  sstate=dvcmd(DEV,'DevState');
  
  if sstate == 2
    code(2*i-1)=0; 
  else
    code(2*i-1)=1;
    dvcmd(DEV,'DevReset');
    pause(1);
    dvcmd(DEV,'DevOn');
  end

% orbit acquisition
  [x{2*i-1},z{2*i-1}]=dvreadbpm(BPM,'wait');
  
% plot the results
  subplot(2,1,1);
  plot (xref{2*i-1}-x{2*i-1},'-o');
  subplot(2,1,2);
  plot (zref{2*i-1}-z{2*i-1},'-o');
  pause(1);


  dvcmd(DEV,'DevSetValue',val(1));
  if dvcmd(DEV,'DevState') ~=2
     dvcmd(DEV,'DevReset');
     pause(1);
  end
  dvclose(DEVSH(i));
 
% reference orbit
  [xref{2*i},zref{2*i}]=dvreadbpm(BPM,'wait');
 
% vertical steerer manipulation  
  
  DEV=dvopen(DEVSV(i));
  val=dvread(DEV);
%  if val(1)>0.01
%    vals(2*i+1)=val(1)/2;
%  else
%    vals(2*i+1)=0.01;
%  end
  vals(2*i)=val(1)+steerv
  dvcmd(DEV,'DevSetValue',vals(2*i));
  pause(1);
  

  % check steerer state    
  sstate=dvcmd(DEV,'DevState');
  
  if sstate == 2
    code(2*i)=0; 
  else
    code(2*i)=1;
      dvcmd(DEV,'DevReset');
      pause(1);
      dvcmd(DEV,'DevOn');
  end

      
% orbit acquisition      
  [x{2*i},z{2*i}]=dvreadbpm(BPM,'wait');
  
% plot the results
  subplot(2,1,1);
  plot (xref{2*i}-x{2*i},'-o');
  subplot(2,1,2);
  plot (zref{2*i}-z{2*i},'-o');
  pause(1);

  dvcmd(DEV,'DevSetValue',val(1));  
  if dvcmd(DEV,'DevState') ~=2
      dvcmd(DEV,'DevReset');
pause(1);
  end
  dvclose(DEVSV(i));

end


dvclose(BPM);
