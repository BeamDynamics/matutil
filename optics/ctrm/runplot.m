function runplot(rg,t,v)
range=runrange(rg);
r1=range(1)-1;
npts=range(end)-r1;
t2=NaN*ones(npts,1);
v2=NaN*ones(npts,size(v,2));
t2(range-r1)=t(range);
v2(range-r1,:)=v(range,:);
plot(t2,v2);
