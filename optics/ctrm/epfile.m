function [info,rms1,rms2,spec]=epfile(fname,range)
%[rms_time,rms_freq]=EPFILE(FILENAME,RANGE)	reads an FBPM archive file
%
%RANGE : frequency range for rms_freq computation (default 49*4.4 = 83.6 Hz)

%[date,time]=strtok(fname,'_');
%dstr=datenum(date)+datenum(sprintf('%d:%d',sscanf(time,'_%gh%g')));
dstr=0;
ok=1;
eval('load(fname)','ok=0');
if ok
   if isempty(GFDBKstate) GFDBKstate=NaN;end
   if isempty(ID14state) ID14state=NaN;end
   if isempty(ID21state) ID21state=NaN;end
   if isempty(ID24state) ID24state=NaN;end
   info=[dstr Ibeam GFDBKstate ID14state ID21state ID24state];
else
   info=NaN*ones(1,6);
end

if nargout >= 2
   if nargin < 2
      range=2:size(monisprecord,1);
   end
   if ok
      spec=exp((monisprecord-60)*log(10)/10)*(10*1024)^2;% unfold dB
      wrong=spec(2,:,:) < 2.6e-4;		% eliminate bad readings
      spec(wrong(ones(50,1),:,:))=NaN;
      rms1(:,:)=spec(1,:,:);			% extract time domain
      spec=2*spec/10/1024;			% 2 for 1-side PSD
      spec(1,:,:)=NaN;				% 10 for missing averaging
      rms2(:,:)=sqrt(sum(spec(range,:,:))/1024);% compute freq. domain
   else
      rms1=NaN*ones(17,4);
      rms2=NaN*ones(17,4);
      spec=NaN*ones(50,17,4);
   end
end
