function [hmatr,vmatr] = fdbk_autocor
global APPHOME

try
%   hmatr=getmatrix('Horizontal matrix')/2500;
%   vmatr=getmatrix('Vertical matrix')/2500/3;
   hmatr=getmatrix('Horizontal matrix');
   vmatr=getmatrix('Vertical matrix');

   if all([size(hmatr) size(vmatr)] == [32 224 32 224])
      [name,path]=uiputfile([APPHOME '/sr/optics/settings/theory/fdbk.mat']);
      if ischar(name)
         save(fullfile(path,name),'-V6','hmatr','vmatr');
      end
   else
      error('GetFile:WrongSize','Wrong matrix size');
   end
catch
   [msg,msgid]=lasterr;
   if isempty(strfind(msgid,'NoResponse')); errordlg(msg); end
   hmatr=[];
   vmatr=[];
end

function resp=getmatrix(label)
[name,path]=uigetfile('*.mat',label,...
	'/users/diag/matlab/Gfeed_op/C60Mres_files/');
if ~ischar(name); error('GetFile:NoResponse','Cancel pressed'); end
hdata=load(fullfile(path,name));
fields=fieldnames(hdata);
if length(fields) ~= 1; error('GetFile:TooManyVars','Too many variables'); end
resp=getfield(hdata, fields{1})';
wrong=max(abs(resp)) <= 0;
missing=srbpmname(find(wrong(:)))
size(resp)
resp(:,wrong) = NaN;

