function [descr,devname,signame]=iddescr(id)
device=['id/id/' num2str(id)];
dev=dvopen(device);
nms=dvcmd(dev,'DevHello');
axs=nms(4:end);
nbaxes=sum(axs);
descr=cell(nbaxes,1);
devname=cell(nbaxes,1);
signame=cell(nbaxes,1);
if nbaxes > 0
   config=dvcmd(dev,'DevReadConfig');
   ngap=0;
   offconfig=0;
   for meca=1:nms(3)
      idname=config{offconfig+2};
      offconfig=offconfig+2;
      for ax=1:axs(meca)
         ngap=ngap+1;
         descr{ngap}=[idname ' ' config{offconfig+2}];
         devname{ngap}=[device '/' num2str(2*ngap-1)];
         signame{ngap}=[device '/gap_axis' num2str(ngap)];
         offconfig=offconfig+5;
      end
   end
end
dvclose(dev)
