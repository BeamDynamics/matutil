function [ampl,phase,tuneap]=mtap(vx,peak)

periods=16;
[nturns,nbpms]=size(vx);
nbpms=nbpms/periods;

shft=[1 1:nturns-1];			% shift one row
ok=find(all(isfinite(vx)));		% keep only valid bpms
ampl=NaN*ones(nbpms,periods);
if false
   w=hann_window(nturns);
   vxft=fft([[vx(shft,ok(end)) vx(:,ok)].*w(:,ones(1,length(ok)+1));...
	  zeros(1024-nturns,1+length(ok))]);
   freq=vxft(:,2:end);
   if isempty(peak)
	  [y,k]=sort(-abs(vxft));
	  k1=k(1,:);
	  kmax=mean(k1(k1>1))
   else
	  kmax=peak
   end
   
   ampl(ok)=abs(vxft(round(kmax),2:end));
   ang=angle(vxft(round(kmax),:));
   tunef=(kmax-1)/1024;
else
   ampl=NaN*ones(nbpms,periods);
   rslt=matnafterms([[vx(shft,ok(end)) vx(:,ok)];...
	  zeros(1024-nturns,1+length(ok))],1);
   ampl(ok)=rslt(2:end,4);
   ang=sign(rslt(:,3)).*rslt(:,5);
   tunef=NaN;
end
pdiff=diff(ang)/2/pi;
%pdiff=diff(phase([okp(end) okp]));
%pdiff=[0;diff(phase(okp))];
decr=(pdiff < 0);
pdiff(decr)=pdiff(decr)+1;
incr=(pdiff >= 0.85);
pdiff(incr)=pdiff(incr)-1;
phase=NaN(nbpms,periods);
phase(ok)=cumsum(pdiff);
tuneap=phase(224);
disp(['Tune from selected frequency   : ' num2str(tunef)]);
disp(['Tune from sum of phase advances: ' num2str(tuneap)]);

%pdif2=reshape(diff([0;phase(:)]),nbpms,periods);
%fullcells=all(isfinite(pdif2))
%avecell=sum(pdif2(:,fullcells))
%cellave=mean2(pdif2')'

function w=hann_window(n)
w=0.5*(1-cos(2*pi*(0:n-1)'/n));
