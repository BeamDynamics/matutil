function docleaning(varargin)
%DOCLEANING performs beam cleaning with predefined parameters
%
%DOCLEANING(ACTION)
%
%ACTION:	Action to be performed (default 'auto'):
%               'clean' Initiates the sequence by closing the scraper
%               'sweep' Sweeps the frequency for actual cleaning
%               'done'  Ends the sequence by opening the scraper
%               'auto'  Full sequence, as 'clean','sweep','done'
%               'doall' Full sequence done by the server
%
%The function waits for completion before returning
%
%DOCLEANING(MCLEAN,ACTION) uses the device handle MCLEAN for the device

mclean=[];
code='auto';
switch nargin
    case 0
    case 1
        if isnumeric(varargin{1})
            mclean=varargin{1};
        else
            code=varargin{1};
        end
    otherwise
        mclean=varargin{1};
        code=varargin{2};
end
if isempty(mclean), mclean=dvcache('cleaning','tango:sr/d-mclean/vertical');end
    
switch lower(code)
    case 'clean'
        if ~isdevstate(mclean,2)
            try
                wait4device(mclean,'StartCleaning',@(dev) isdevstate(dev,2),30);
            catch exception
                rethrow(exception);
            end
        end
    case 'sweep'
        if isdevstate(mclean,2)
            wait4device(mclean,'Sweep',@(dev) ~isdevstate(dev,9),30);
        else
            error('Cleaning:WrongState','Scrapers not ready for cleaning');
        end
    case 'done'
        if ~isdevstate(mclean,1)
            try
                wait4device(mclean,'EndCleaning',@(dev) isdevstate(dev,1),30);
             catch exception
                rethrow(exception);
            end
        end
    case 'doall'
        if isdevstate(mclean,1)
            try
                wait4device(mclean,'DoAll',@(dev) isdevstate(dev,1),45);
            catch exception
                rethrow(exception);
            end
        end
    case 'auto'
        fprintf('>>Closing the scraper\n');
        docleaning(mclean,'clean');
        fprintf('>>Sweeping\n');
        docleaning(mclean,'sweep');
        fprintf('>>Opening the scraper\n');
        docleaning(mclean,'done');
end

