function [vx,ave]=mtclean(vx0,ave0,wrong)

[nturns,nbpms]=size(vx0);
ok=true(1,nbpms);		% all existing BPMS
dispn('eliminated:', wrong);
ok(wrong)=false;			% remove user choice

missing=~all(isfinite(vx0));
dispn('NaN values:', find(missing));
ok(missing)=0;				% remove BPMs with NaNs


for i=1:3
    rmsorbit=std2(ave0(ok));
    if rmsorbit < 50e-6, rmsorbit=50e-6; end
    outave=abs(ave0)>4.0*rmsorbit;
    ok(outave)=0;           % remove average exceptions
end

dispn('average out:',find(outave));

oscill=std(vx0,0,1);		% Safe for nturns=1
%oscill(~ok)=NaN;
rmsoscil=mean2(reshape(oscill,14,16)')';
if min(rmsoscil) < 0.010
   rmsmin=zeros(14,1);
   rmsmax=Inf(14,1);
else
   rmsmin=0.25*rmsoscil;
   rmsmax=3*rmsoscil;
end
outrms=oscill < reshape(rmsmin(:,ones(1,16)),1,nbpms);
ok(outrms)=false;			% remove fixed BPMs
dispn('oscillation too small:', find(outrms));
outrms=oscill > reshape(rmsmax(:,ones(1,16)),1,nbpms);
ok(outrms)=false;			% remove large oscillations
dispn('oscillation too large:', find(outrms));

vx=NaN(size(vx0));
vx(:,ok)=vx0(:,ok)-ave0(ones(nturns,1),ok);
ave=NaN(size(ave0));
ave(ok)=ave0(ok);

function dispn(code, list)
disp([num2str(length(list)) ' ' code]);
for wb=list
    [bname,kdx]=sr.bpmname(wb);
    fprintf('     %s(%i)\n',bname,kdx);
end
