function res=fbpm
dev=dvopen('sr/d-fbpm/c12m:tcp');
res=reshape(dvcmd(dev,'DevReadValue'),1024,4);
dvclose(dev);
