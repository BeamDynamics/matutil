%MTPANEL	Processing of turn-by-turn data
%
%MTPANEL allows processing of turn-by-turn data
%
%KICK:	set the desired current and send a 'DevOn' command to the K1 kicker
%
%Load BPM data:	Get position data from the BPM system
%Load abcd:	Get Electrode data from the BPM system and compte positions
%Load file:	Get positions from a file previously stored
%
%	Data for both planes are collected, but results are displayed only
%	for the selected plane.
%	Wrong BPMs are detected and eliminated as follows:
%	1) BPM specified in the "suppress" box (called "eliminated")
%	2) NaN value for any turn (called "NaN values")
%	3) Average position > 4*rms(average orbit) (called "average out")
%	4) Oscillation < 0.25*rms. oscillation for same position
%					(called "oscillation too small")
%	5) Oscillation > 3*rms. oscillation for same position
%					(called "oscillation too large")
%	Unsmearing is applied if requested
%
%	The plot displays:
%		Turn-by-turn rms orbit, showing the decoherence
%		Average position for each BPM (same as slow closed orbit)
%		Rms. oscillation for each BPM
%
%Plot BPM
%
%	Plot the position on the specified turn range for the specified BPM
%	Displays the spectrum for the specified turn range
%		(blue: no windowing, green: hanning window)
%	Compute the tunes far all BPM and print the average value over all BPMS
%	for  different methods
%
%Plot turn
%
%	Plot the position on all BPM for the specified turn number
%
%Ampl/Phase
%
%	Plot the spectral amplitude and phase for the main frequency (using
%	NAFF).
%
%Phase space
%
%	Compute positon and angles in the middle of straight sections and plot
%	the phase space. Derive alpha,beta,eps in the middle of straight
%	sections. Deduce the beta value and both BPMS, and scale the spectral
%	amplitude to match those values. This gives beta values on all PBMS
%	resulting from scaled spectral amplitudes

global vx aveorbit plane intens
global ampl phas
global alphaid betaid
global beta
global high_beta low_beta avebeta avephase
mtpanel2
