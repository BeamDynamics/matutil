function refill()
%REFILL() Performs a refill using topup macros
%
%   The injector should be ready:
%   Linac on with gun off, BPSS on, STRF on, SYInj on, SYExt on, Ke standby
%
% In the topup application, uncheck all "Enable ???" boxes
%                           set the desired current

cmd1=tango.Device('sys/door-topup/01');
cmd2=tango.Device('sys/door-topup/02');

waitfor()
disp('Starting injector');
cmd1.RunMacro({'CheckBooster'});
cmd2.RunMacro({'SRInjOn'});
waitfor()
pause(4);
disp('Injecting');
cmd1.RunMacro({'Injection','0.03','180'});
waitfor()
cmd1.RunMacro({'SYExtOn'});     % restart extraction for the next time

    function waitfor()
        while true
            if ~any([cmd1.State,cmd2.State]==tango.DevState.Run)
                break;
            end
            pause(1);
        end
    end
end
