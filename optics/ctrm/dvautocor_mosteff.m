function [corr,orbitfit] = dvautocor_mosteff(orbit, plane)
%DVAUTOCOR_MOSTEFF Compute most-effective correction
%   [CORR,ORBITFIT]=DVAUTOCOR_MOSTEFF(ORBIT,PLANE)
%
%ORBIT:     orbit to be analysed [m]
%PLANE:     orbit plane
%           'h', 'H', 'x', 'X', 1 => horizontal
%           'v', 'V', 'z', 'Z', 2 => vertical
%
%CORR:      10 most effective corrections [index strength[A] chi2]
%ORBITFIT:  Orbit fitted with the most effective steerer
%
%See also: dvautocor_svd

nmeff=10;
[label,devname]=selectplane(plane,{'HORIZONTAL','VERTICAL'},{'sr/meff-orbitcor/h','sr/meff-orbitcor/v'});
autocor=tango.cache(devname);

result=autocor.Compute(reshape(orbit,1,[]));
corr=reshape(result.dvalue,nmeff,[]);
bestcor=sr.steername(corr(1,1)+1);
fprintf('     %10s  %6s  %5s\n','steerer','strength','Chi2');
for snum=1:10
    ids=corr(snum,1)+1;
    fprintf('%4d %10s  %6.3f  %g\n',ids,...
        srsteername(ids), corr(snum,2),corr(snum,3));
end
kicks=zeros(96,1);
kicks(corr(1,1)+1)=corr(1,2);
testcor=[1,corr(1,1:2)];
result=autocor.Test(testcor);
orbitfit=-result';

sbpm=(0.5+(0:223)')/224;
sticks=linspace(0,1,17);
slabels=[4:2:32 2 4];
subplot(2,1,1);
plot(sbpm, [orbit orbitfit]);
ax=axis;
axis([0 1 ax(3:4)]);
set(gca,'XTick',sticks,'XTickLabel',slabels);
xlabel('Cell');
ylabel('orbit');
title(label);
grid on
legend('measured',['reconstructed with ' bestcor]);

skick=(0.5+(0:95)')/96;
subplot(2,1,2);
bar(skick,kicks);
ax=axis;
axis([0 1 ax(3:4)]);
set(gca,'XTick',sticks,'XTickLabel',slabels);
xlabel('Cell');
ylabel('Strength');
end

