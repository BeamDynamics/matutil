function disp_t(vx,turn,plane)

oscill=vx(turn,:);
plot(oscill);
title(['TURN ' int2str(turn)]);
xlabel('Bpm #');
ylabel('position (mm)');
rmsorbit=std2(oscill);
posgr(0.02, 0.92, ['Rms oscillation: ' num2str(rmsorbit,3)]);
