function [stdcell,ring] = myesrf(varargin)
%[STDCELL,RING] = MYESRF()
%
%[STDCELL,RING] = MYESRF(...,'quiet',true)
%   do not display the modifications
%
%[STDCELL,RING] = MYESRF(...,'remove',famnames)
%   remove elements identified by the family names (cell array)
%
%[STDCELL,RING] = MYESRF(...,'reduce',true)
%   group similar elements together

energy=6.04E9;
voltage=8E6;
nbper=16;

v=atreadbeta('/machfs/laurent/dbeta/s13s20thick.str');
h=992/nbper;
rfcav=atrfcavity('RFC',0,voltage/nbper,...
    h*2.99792458e8/findspos(v,size(v,1)+1),...
    h,energy,'IdentityPass');
stdcell=sr.model.checklattice([v(1:end-1,1);{rfcav}],'energy',energy,'periods',nbper,'hardangle',0.00785,varargin{:});

%v=atreadbeta('/machfs/laurent/orbit/esrf2013a_multibunch/betathickmodel.str');
v=atreadbeta('/machfs/laurent/orbit/esrf2015a/betathickmodel.str'); % high chroma
ring=sr.model.checklattice(v,'energy',energy,'hardangle',0.00785,varargin{:});

end
