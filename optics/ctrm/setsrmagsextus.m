function ring=setsrmagsextus(ring,fname,scorhl)

sx=atgetcells(ring,'Class','Sextupole');
sxl=atgetfieldvalues(ring(sx),'Length');
gl=zeros(7,32);

f1=fopen(fname,'rt');
c=textscan(f1,'%s%f');
[sxnames,sxvals]=deal(c{:});
fclose(f1);
gl(1,:)=getgl('SR/PS-S4/0');
gl(2,:)=getgl('SR/PS-S6/0');
gl(3,:)=getgl('SR/PS-S13/0');
gl(4,:)=getgl('SR/PS-S19/0');
gl(5,:)=getgl('SR/PS-S20/0');
gl(6,:)=getgl('SR/PS-S22/0');
gl(7,:)=getgl('SR/PS-S24/0');
gl(1,12)=getgl('SR/PS-S4/C15');
gl(1,13)=getgl('SR/PS-S4/C16');
gl(1,26)=getgl('SR/PS-S4/C29');
gl(1,27)=getgl('SR/PS-S4/C30');
gl(6,19:20)=getgl('SR/PS-S22/C22-23');
gl(7,19)=getgl('SR/PS-S24/C22');
gl(7,20)=getgl('SR/PS-S24/C23');
hl=srunfold(gl)/20.13;
if nargin >= 3
    scoridx=selcor(3);
    hl(scoridx)=hl(scoridx)+scorhl;
end
ring(sx)=atsetfieldvalues(ring(sx),'PolynomB',{3},hl./sxl);

    function gl = getgl(name)
        current=sxvals(cellfun(@(nm) ~isempty(strfind(upper(nm),name)),sxnames),1);
        gl=i2gl(name,current);
    end
end
