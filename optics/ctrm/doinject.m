function [cur1,cur2]=doinject(current_ref, timeout)

if nargin < 2, timeout=200; end
id.current=dvopen ('+tango:sr/d-ct/1/current');
id.linac_gun=dvopen('+tango:elin/beam/run:hstate');
id.syct=dvopen('+sy/d-ct/ct');
id.syrf=dvopen('+tango:sy/rfssa-tra/tra0');
id.ke=dvopen('+tango:sy/ps-ke/1:hstate');
[s,cur1]=dvread(id.current);
current=cur1;

if cur1 < current_ref
    % 12 STBY, 2 ON
    if ~isdevstate(id.syrf, 2)
        fprintf('>>Switching syrf ON\n');
        wait4device(id.syrf,'DevOn',@(dev) ~isdevstate(dev,9),{4,90});
    end
    
    dvx(@dvcmd,id.linac_gun,'DevOn');
    fprintf('>>Checking the Booster\n');
    
    pause(1);					% Check booster current
    [s,sycurrent]=dvread(id.syct);
    if sycurrent < 0.08
        error('Inject:sycurrent','No beam in the booster');
    end
    
    fprintf('>>Booster current = %g\n>>Stopping Linac\n',sycurrent);
    dvx(@dvcmd,id.linac_gun,'DevOff');
    
    % Start srinj
    % option -f n' agite pas les scrapers
    statinjon=system('SRInjProcess -f ON 2>/dev/null');
    
    try
        fprintf('>>Switching Ke ON\n');
        wait4device(id.ke,'DevOn',@(dev) isdevstate(dev,2),4);

        fprintf('>>Switching Linac ON\n');
        dvx(@dvcmd,id.linac_gun,'DevOn');

        fprintf('>>Waiting for max. current\n');
        wait4device(id.current,[],@(dev)(dvcmd(dev,'DevRead')>=current_ref),timeout);
    catch exception
        % Stop everything
        dvcmd(id.linac_gun,'DevOff');
        statinjoff=system('SRInjProcess -f OFF 2>/dev/null');
        dvcmd(id.ke,'DevStandby')'
        rethrow(exception);
    end
    
    % Stop everything
    fprintf('>>Switching Linac OFF\n');
    dvcmd(id.linac_gun,'DevOff');
    statinjoff=system('SRInjProcess -f OFF 2>/dev/null');
    fprintf('>>Switching Ke OFF\n');
    wait4device(id.ke,'DevStandby',@(dev) isdevstate(dev,12),4);
    fprintf('>>Switching Syrf to STANDBY\n');
    dvx(@dvcmd,id.syrf,'DevStandby');
end
[s,cur2]=dvread(id.current);
dvclose([id.current;id.linac_gun;id.syct;id.syrf;id.ke]);
