function range=runrange(rg)
range=[];
for i=1:size(rg,1)
range=[range rg(i,1):rg(i,2)];
end
