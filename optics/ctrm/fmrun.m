function fmrun(xk,yk,dpath)
% CODE=FMRUN(HKICKER,VSHAKER,DIRNAME)	performs a MT map measurement
%
% HKICKER :	array of H kicker values
% VSHAKER :	array of V shaker values
% DIRNAME : 	Directory where MT files will be saved
%
% CODE :	1= beam lost
%		2= kicker failure
%		9= MT timeout (sequence blocked)
%		23= MT Fault

global DEVS

if isempty(DEVS)
   DEVS=dvopen('SR/PS-K/1','tango:sr/ps-k/vertical','tango:sr/ps-k/vertical/vpfn','SR/MTOUR/UP')
end

[xf,yf]=ndgrid(xk,yk);
code=1;
[s,bid]=unix('sr_test 2>/dev/null');
if bitand(s,2) ~= 0, return; end
save([dpath '/currents.mat'],'xk','yk');

for j=1:size(xf,2)
dvcmd(DEVS(1),'DevStandby');
dvcmd(DEVS(2),'Standby');
pause(5);
for i=1:size(xf,1)
   fname=[dpath '/X' num2str(xf(i,j),'%04.0f') 'Z' num2str(100*yf(i,j),'%04.0f')];
   mess=['horizontal ' num2str(xf(i,j)) ', vertical ' num2str(yf(i,j))];
   if exist([fname 'h.mat'],'file') ~= 2 | exist([fname 'v.mat'],'file') ~= 2
      dvcmd(DEVS(1),'DevSetValue',xf(i,j))		% set currents
      dvcmd(DEVS(3),'DevWrite',yf(i,j));
      pause(2);
      dvcmd(DEVS(1),'DevOn');
      dvcmd(DEVS(2),'DevOn');
      pause(3);

      code=mtrun(200)				% start measurement
      if ismember(code,[9 23])				% check measurement
         dvcmd(DEVS(1),'DevStandby');
	 errfmrun(DEVS,code);
      end

      code=2;						% check currents
      kstate=dvcmd(DEVS(1),'DevState');
      if kstate == 2, code=0; end
      if code ~= 0
	 errfmrun(DEVS,code);
      end
      dvcmd(DEVS(1),'DevStandby');
      dvcmd(DEVS(2),'Standby');

      code=1;						% check beam
      [s,bid]=unix('sr_test 2>/dev/null');
      if bitand(s,2) == 0, code=0; end
      if code ~= 0
	 errfmrun(DEVS,code);
      end

      for plane='hv'
         [vx0,aveorbit]=load_mtour('test',plane);	% load data
         [vx,first,coher]=mtclean(vx0,aveorbit,'');
         disp1000(aveorbit,vx,first,coher);
         save([fname plane],'vx','aveorbit','plane')	% save file
      end

      eval(['!say ''' mess ', done'''])
   else
      disp(['skipping ' mess]);
%     eval(['!say ''skipping ' mess '''']);
   end
end
dvcmd(DEVS(1),'DevOff');
pause(2);
end
!say 'measurement finished'

dvcmd(DEVS(3),'DevWrite',0);		% set shaker to 0

function errfmrun(devs,code)
pause(2);
dvcmd(devs(1),'DevOff');				% stop kicker
dvcmd(DEVS(3),'DevWrite',0);		% set shaker to 0
dvcmd(DEVS(2),'Standby');
!say 'measurement error'
error('Frmap:error','error code %s',code);
