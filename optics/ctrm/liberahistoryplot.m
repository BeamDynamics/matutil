function hist=liberahistoryplot(x,varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

nbpm=0;
if nargin >= 2
    if ischar(varargin{1})
        nbpm=0;
    else
        nbpm=varargin{1};
    end
end

if nbpm <=0
    if nargin >=3
        x=x-x(varargin{2}*ones(1800,1),:);
    end
    hist=sqrt(sum(x.*x,2));
else
    hist=x(:,nbpm);
end
plot(orb);