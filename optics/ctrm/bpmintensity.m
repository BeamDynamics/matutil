function i=bpmintensity
%[current,elecs]=bpmintensity

!bpmintensity
fid=fopen([getenv('HOME') '/bpm/elecout.wkz'],'r');
fskip(fid,'MIM',5);
elecs=fscanf(fid,'%d%g%g%g%g',[5 inf]);
fclose(fid);
elecs(:,1:10)'
elecs=elecs(2:5,:)';
i=sum(elecs')';
