function filpat_fill(id, imax, tmout)
%FILPAT_FILL(ID,IMAX,TMOUT)    Fills up to the desired current

if nargin < 3, tmout=1200; end			% seconds
[output,status]=dvcmd(id.linac_gun,'DevOn');
if status ~= 0, error('Inject:linac','Cannot start linac'); end
for  i=1:tmout					% Wait dor desired current
    [s,current] = dvread(id.current);
    if current > imax; break; end   
    pause(1);
end 
[output,status]=dvcmd(id.linac_gun,'DevOff');
if i >= tmout; error('Inject:tmout','Injection timed out'); end
if status ~= 0; error('Inject:linac','Cannot stop linac'); end
