%	Set Linac in short pulse
%	Set Bunchclock at 10 Hz


%id=filpat_open;

% 104 pulses
[lp,bl]=filpat_grid(4,88,2,104);
filpat_plot(lp,bl);
filpat_set(id,lp,bl,true);		% pulse rotation true with 4 pulses

% 328 pulses (~ 1/3)
[lp,bl]=filpat_grid(4,88,2,328);
filpat_set(id,lp,bl,true);		% pulse rotation true with 4 pulses

% 656 pulses (~ 2x1/3)
[lp,bl]=filpat_grid(4,88,2,656);
filpat_set(id,lp,bl,true);		% pulse rotation true with 4 pulses

% 870 pulses with gap (~ 7/8)
[lp,bl]=filpat_grid(2,109,4,872,218);
filpat_set(id,lp,bl,false);		% pulse rotation false with 2 pulses

% 870 pulses without gap
[lp,bl]=filpat_grid(2,124,4,872);
filpat_set(id,lp,bl,false);		% pulse rotation false with 2 pulses

% uniform
[lp,bl]=filpat_grid(2,124,4,992);
filpat_set(id,lp,bl,false);		% pulse rotation false with 2 pulses

%filpat_close(id);
