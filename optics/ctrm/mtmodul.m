function modul=mtmodul(bet)

betav=mean2(bet')';
modul=(bet-betav(:,ones(1,size(bet,2))))./betav(:,ones(1,size(bet,2)));
