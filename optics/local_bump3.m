function [bump,kicks]=local_bump3(resp,sk,sb,bindex)
%[bump,kicks]=LOCAL_BUMP3(resp,sk,sb,bindex) computes local bumps
%	resp: response matrix
%	sk: kicker positions
%	sb: BPM positions
%	bindex: index of selected bpm

nst=length(sk);
[k1,k2]=ndgrid([1;1;2;2;2;3;3;4;4;5;5;5;6;6],0:nst/16:nst-1);
ks=reshape(k1+k2,224,1);
[npu,ncor]=size(resp);
b2=ks(bindex);
bump=[b2-1 b2 b2+1];
bump(bump<=0)=bump(bump<=0)+nst;
bump(bump>nst)=bump(bump>nst)-nst;
if (ncor > nst), bump=[bump ncor]; end	% add frequency

s1=sk(bump(1));
s3=sk(bump(3));
if s1<s3                                % ignore bpms inside the bump
    ok=(sb<s1) | (sb>s3);
else
    ok=(sb<s1) & (sb>s3);
end
ok(bindex)=true;                        % but keep the selected one
ok=ok & all(isfinite(resp(:,bump)),2);	% eliminate missing BPMs

target=zeros(npu,1);                    % set desired amplitude on selected BPM
target(bindex)=1;

kicks=resp(ok,bump)\target(ok);         % compute the bump
orbit=resp(:,bump)*kicks;
kicks=kicks*1/orbit(bindex);            % renormalize to 1
