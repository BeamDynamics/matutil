function [runs,ok]=split_run(inj,lost,val)

nruns=length(inj);
decay=max(lost-inj+1);
runs=NaN*ones(decay,nruns);
ok=NaN*ones(size(val));
for k=1:nruns
   if lost(k)>=inj(k)
      nb=lost(k)-inj(k)+1;
      from=inj(k):lost(k);
      runs(1:nb,k)=val(from);
      ok(from)=val(from);
   end
end
