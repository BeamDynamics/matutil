function [v,orbit]=rbump_eval(s, bpm, theoresp)
%[V,ORBIT]=RBUMP_EVAL(S,BPM,SHUNTDEV,THEORESP) Quantify offset error
%
%S:         Structure returned by rbump_init
%BPM:       Current bpm structure
%SHUNTDEV:  Current shunt device
%THEORESP:  Theoretical beam response to the shunt
%
%See also: RBUMP_INIT

try
   bpm.shuntfunc(bpm.shuntdev, 'On');			% DANGEROUS
   disp('                     Shunt ON');
   pause(3);
   [x,z]=s.bpmdev.read_sa('Wait',true);
   disp('                     Shunt OFF');
   bpm.shuntfunc(bpm.shuntdev, 'Off');
   pause(3);
catch error
   disp('-Error: setting shunt');
   bpm.shuntfunc(bpm.shuntdev, 'Off');
   rethrow(error);
end
[x0,z0]=s.bpmdev.read_sa('Wait',true);
dxz=[x-x0 z-z0];
%hharm=s.hharmdev.OrbitDecomposition(x-x0);
%vharm=s.vharmdev.OrbitDecomposition(z-z0);
figure(5)
subplot(2,1,1)
plot(subplot(2,1,1),x-x0)
plot(subplot(2,1,2),z-z0)
hvharm=[0 0]; % [hharm(round(s.params.nuh)) vharm(round(s.params.nuv))];
hvfit=fit_orbit(theoresp,dxz);

v=[x0(bpm.id) z0(bpm.id) hvharm hvfit std2(dxz)];
if nargout >= 2, orbit=[x0 z0]; end

function res=fit_orbit(theoresp,y)
ok=all(isfinite(y),2);
xok=theoresp(ok,:);
res=sum(xok.*y(ok,:))./sum(xok.*xok);
