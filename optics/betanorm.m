function [bb2,bk2]=betanorm(bbm,bkm,aver_bpm)
% comptes beta values on BPM and steerers at the end of iteration

nb=length(bbm)/16;
nk=length(bkm)/16;
avebbm=mean2(reshape(bbm.^2,nb,16)');
if nk == 6
   avebkm=mean2(reshape(bkm.^2,nk,16)');
   r=sqrt((avebbm(4)+avebbm(11))/(avebkm(2)+avebkm(5)))
else
   disp('No Bpm/Steerer balance possible');
   r=input('balance (1.0 ?):');
end
if nargin >= 3
   scaling=(mean(aver_bpm(1:nb)))/mean(avebbm)*r
else
   scaling=1
end
bb2=reshape(bbm.^2,nb,16)*scaling/r;
bk2=reshape(bkm.^2,nk,16)*scaling*r;
