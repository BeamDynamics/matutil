function [h2h,v2h,h2v,v2v]=applyskew(bpm0,sext0,nux,nuz,sxdisp,corr)
%[H2H,V2H,H2V,V2V]=APPLYSKEW(bpm0,sext0,nux,nuz,sxdisp,corr)
%
%SXDISP:		Integrated skew strength (m-1) at sextupole locations
%CORR:			Integrated skew strength (m-1) at corrector locations
%H2H,V2H,H2V,V2V:	(m/rad)

ns=size(sext0,1);
keepk=reshape(1:ns,ns/16,16);
keepk=keepk([2 4 6 9 11 13],:);

if isempty(sxdisp)
   if isempty(corr)
      keeps=[];
   else
      keeps=selcor(2);
   end
else
   if isempty(corr)
      keeps=keepk(:);
   else
      keeps=[keepk(:);selcor(2)];
   end
end
skew=[sxdisp(:);corr(:)];

if ~isempty(skew)
   h_ks=responsem(sext0(keeps,2:3),sext0(keepk,2:3),nux);
   v_ks=responsem(sext0(keeps,5:6),sext0(keepk,5:6),nuz);
   h_ss=responsem(sext0(keeps,2:3),sext0(keeps,2:3),nux);
   v_ss=responsem(sext0(keeps,5:6),sext0(keeps,5:6),nuz);
   h_sb=responsem(bpm0(:,2:3),sext0(keeps,2:3),nux);
   v_sb=responsem(bpm0(:,5:6),sext0(keeps,5:6),nuz);

   ns = length(skew);
   dskew=diag(skew);
   mh=h_sb*dskew/(eye(ns)-v_ss*dskew*h_ss*dskew);
   mv=v_sb*dskew/(eye(ns)-h_ss*dskew*v_ss*dskew);

   h2h=mh*v_ss*dskew*h_ks;
   v2h=mh*v_ks;
   h2v=mv*h_ks;
   v2v=mv*h_ss*dskew*v_ks;
else
   h2h=[];
   v2h=[];
   h2v=[];
   v2v=[];
end
