function [respmat,scalef,scalei] = load_sraresp(varargin)
%[RESPMAT,SCALEF,SCALEI]=LOAD_SRARESP(OPTICS,PLANE)	Load the response matrix used by SR AutoCor
%   Load the response matrix from the selected optics directory
%
%OPTICS:	machine description: looks for:
%
%       'sr' or 'sy':	machine name
%           path defaults to $(APPHOME)/mach/optics/settings/theory
%
%       'optname','sr':	optics name and machine
%           path is $(APPHOME)/mach/optics/settings/optname
%
%       '/machfs/appdata/sr/optics/settings/optname'[,'sr']:
%           explicit full path of optics directory
%
%PLANE: 'h' or 'v'
%
% In the horizontal plane, the steerer response SRESP is combined with the
% frequency response FRESP in the following way:
%
%       RESPMAT = [ SRESP SCALEF*FRESP ; SCALEI*ONES(96,1) 0]
%
%RESPMAT: response matrix [m/A]
%SCALEF:  scaling factor for the frequency response [Hz/A]
%SCALEI:  scaling factor for the sum of steerer currents [m/A]
%

[narg,path,mach]=getmachenv(varargin{:}); %#ok<ASGLU>
a=load(fullfile(path,selectplane(varargin{narg},{'h_svd','v_svd'})));
respmat=a.resp2;
if nargout >= 2
    if isfield(a,'scaledf')
        scalef=a.scaledf;
    elseif isfield(a,'scalef')
        pm=load_optics(varargin{1:end-1});
        scalef=-pm.alpha*992*2.997924E8/pm.ll*a.scalef;
    else
        scalef=NaN;
    end
end
if nargout >= 3
    if isfield(a,'scalei')
        scalei=a.scalei;
    else
        scalei=NaN;
    end
end
end
