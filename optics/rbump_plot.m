function rbump_plot(h,code,varargin)

pt3=[-1 -1;0 -1;1 -1];
figure(h);
if strcmp(code,'bumps')
   orbits=varargin{1};
   plot((1:224)',orbits);
   if nargin >= 4
	  bpmindex=varargin{2};
	  hold on
	  plot(bpmindex,orbits(bpmindex,:),'o');
   end
   title('Bump');
   xlabel('BPM number');
   ylabel('orbit');
elseif strcmp(code, 'orbit')
   [hharm,vharm]=deal(varargin{:});
   subplot(2,1,1);
   bar(abs(hharm));
   subplot(2,1,2);
   bar(abs(vharm));
elseif strcmp(code, 'harms')
   vhv=varargin{1};
   AB=pt3\vhv(3:5,1:2);
   best=real(AB(2,:)./AB(1,:));
   plot(vhv(3:5,:),'o');	% measured points
   hold on
   plot([-1 -1;0 -1;1 -1]*AB);	% linear fit of measured points
   % best estimate
   plot([best(1) -1]*AB(:,1),'s','MarkerEdgeColor',[0 0 1]);
   plot([best(2) -1]*AB(:,2),'s','MarkerEdgeColor',[0 0.5 0]);
   % control
   plot(vhv(6,1),'*','MarkerEdgeColor',[0 0 1]);
   plot(vhv(6,2),'*','MarkerEdgeColor',[0 0.5 0]);
elseif strcmp(code, 'fit')
%  [ampl,vhv]=deal(varargin{:});
   ampl=1.e6*varargin{1};
   vhv=1.e6*varargin{2};
   AB=pt3\vhv(3:5,1:2);
   vfit=pt3*AB;
   plot(ampl(3:5,1),vfit(:,1),'-',ampl(3:5,2),vfit(:,2),'-');
   legend('H','V');
   hold on
   plot(ampl(3:5,1),vhv(3:5,1),'o',ampl(3:5,2),vhv(3:5,2),'o');
   % control
   plot(ampl(6,1),vhv(6,1),'*','MarkerEdgeColor',[0 0 1]);
   plot(ampl(6,2),vhv(6,2),'*','MarkerEdgeColor',[0 0.5 0]);
   if isfinite(ampl(1,1))
	  ls=plot(ampl(1:2,1),vhv(1:2,1),'-o',ampl(1:2,2),vhv(1:2,2),'-o');
	  set(ls,'LineWidth',1.5);
   end
   title('Beam displacement');
   xlabel('Bump amplitude [\mum]');
   ylabel('Fitted oscillation [\mum]');
elseif strcmp(code, 'calib')
%  [ampl,vhv]=deal(varargin{:});
   ampl=1.e6*varargin{1};
   vhv=1.e6*varargin{2};
   plot(ampl(3:5,1),vhv(3:5,1),'-o',ampl(3:5,2),vhv(3:5,2),'-o');
   legend('H','V');
   hold on
   if isfinite(ampl(1,1))
	  ls=plot(ampl(1:2,1),vhv(1:2,1),'-o',ampl(1:2,2),vhv(1:2,2),'-o');
	  set(ls,'LineWidth',1.5);
   end
   title('Bump efficiency');
   xlabel('Bump amplitude [\mum]');
   ylabel('BPM reading [\mum]');
end
hold off
grid on
