function [s,sh,sv]=rbump_init(varargin)
%[S,SH,SV]=RBUMP_INIT(PATH)	Initialize BPM offset measurement
%
%[S,SH,SV]=RBUMP_INIT()
%           path defaults to $(APPHOME)/mach/optics/settings/theory
%[S,SH,SV]=RBUMP_INIT(OPTICSNAME)
%           path is taken as $(APPHOME)/mach/optics/settings/opticsname
%[S,SH,SV]=RBUMP_INIT(PATH)
%           Uses the given path
%
%See also: RBUMP_MEAS, RBUMP_ID

periods=16;

bpm2shunt={
    'SR/SH-QF2/C%02d',...
    'SR/SH-QF2/C%02d',...
    'SR/SH-QD4/C%02d-1',...
    'SR/SH-QF5/C%02d-1',...
    'SR/SH-QD4/C%02d-2',...
    'SR/SH-QF7/C%02d',...
    'SR/SH-QF7/C%02d',...
    'SR/SH-QF7/C%02d',...
    'SR/SH-QF7/C%02d',...
    'SR/SH-QD4/C%02d-1',...
    'SR/SH-QF5/C%02d-1',...
    'SR/SH-QD4/C%02d-2',...
    'SR/SH-QF2/C%02d',...
    'SR/SH-QF2/C%02d'};

[pth,~,mach]=getmodelpath('sr',varargin{:});
pm=getmodel(mach,pth);
[bpms,qp,sth,stv]=pm.get('bpm','qp','steerh','steerv');
[sth_name,stv_name]=pm.name('steerh','steerv');

if size(qp,1) == 320
    bpm2qp=[2;2;4;5;7;9;9;12;12;14;15;17;19;19];
    qpindex=20*floor((0:223)/14)'+reshape(bpm2qp(:,ones(1,periods)),224,1);
elseif size(qp,1) == 256
    bpm2qp=[1;1;3;4;6;8;8;9;9;11;12;14;16;16];
    qpindex=16*floor((0:223)/14)'+reshape(bpm2qp(:,ones(1,periods)),224,1);
end

rmode=3;
switch rmode
    case 1      % theoretical bump
        coefh=repmat(load_corcalib('sr',8),size(bpms,1),1);
        resph=responsem(bpms(:,2:3),sth(:,2:3),pm.nuh).*coefh;
        coefv=repmat(load_corcalib('sr',9),size(bpms,1),1);
        respv=responsem(bpms(:,5:6),stv(:,5:6),pm.nuv).*coefv;
        rfreq=bpms(:,4);
    case 2      % experimental bump + average over superperiods
        resph=sr.load_normresp(fullfile(pth,'resH.rsp'),'h','m/A','TooLarge',0,'Zeros',0,'Fixed',0);
        respv=sr.load_normresp(fullfile(pth,'resV.rsp'),'v','m/A','TooLarge',0,'Zeros',0,'Fixed',0);
        resph=srmresp(resph,16);
        respv=srmresp(respv,16);
        [rfreq,~]=sr.load_disp(pth,pm);
        rfreq=srmresp(rfreq,16);
    case 3      % experimental bump
        resph=sr.load_normresp(fullfile(pth,'resH.rsp'),'h','m/A','TooLarge',0,'Zeros',0,'Fixed',0);
        respv=sr.load_normresp(fullfile(pth,'resV.rsp'),'v','m/A','TooLarge',0,'Zeros',0,'Fixed',0);
        resph=refurb(resph);
        respv=refurb(respv);
        [rfreq,~]=sr.load_disp(pth,pm);
        rfreq=refurb(rfreq);
end

[resphf,deltaf]=expandhresp(resph, rfreq, pm);

sh=buildstructure('h', sth, sth_name, resphf(1:224,:), deltaf);
sv=buildstructure('v', stv, stv_name, respv);

bpmh=bpmset(bpms, sth, sth_name, resphf(1:224,:),qp(qpindex,2:3));
bpmv=bpmset(bpms, stv, stv_name, respv,qp(qpindex,5:6));

[qpname,qpfunc]=c23config();
shuntn=shuntname(224,bpm2shunt);
shuntn(131:137)=qpname;					% Overwrite data for cell 23
shuntfunc=repmat({@(dev,cmd) dev.cmd(cmd)},224,1);
shuntfunc(131:137)=qpfunc;
s=struct('path', pth,...
    'params', pm,...
    'hharmdev', tango.Device('sr/harm-analysis/h'),...
    'vharmdev', tango.Device('sr/harm-analysis/v'),...
    'bpmdev', tango.SrBpmDevice('sr/d-bpmlibera/all'),...
    'betaphi',bpms,...
    'bpm', struct(...
    'id', num2cell(1:224,1)',...
    'bpmname',sr.bpmname(1:224)',...
    'shuntname', shuntn,...
    'shuntdev',0,...
    'shuntfunc',shuntfunc,...
    'h',mat2cell(bpmh, ones(1,224), 1),...
    'v',mat2cell(bpmv, ones(1,224), 1)...
    )); %#ok<MMTC>
s.bpmdev.Averaging=3;
s.bpmdev.IgnoreIncoh=true;

    function names=shuntname(nbpms,bpm2shunt)
        names=cell(nbpms,1);
        
        nincell=mod((0:nbpms-1)',14)+1;
        cellnum=floor((0:nbpms-1)'/7)+4;
        cellnum(cellnum>32)=cellnum(cellnum>32)-32;
        
        for bp=1:nbpms
            names{bp}=sprintf(bpm2shunt{nincell(bp)}, cellnum(bp));
        end
    end

    function bpmhv=bpmset(bpms, st, stname, resp, qp)
        nbpms=size(bpms,1);
        %nk=4;
        nk=3;
        nk2=nk+size(resp,2)-size(st,1);
        bump=zeros(nbpms,nk2);
        kicks=zeros(nk2,nbpms);
        kname=cell(nbpms,1);
        
        for bp=1:nbpms
            %   [bmp,kicks(:,bp)]=local_bump4(resp,st(:,1), bpms(:,1), bp);
            [bmp,kicks(:,bp)]=local_bump3(resp,st(:,1), bpms(:,1), bp);
            bump(bp,:)=bmp;
            kname{bp}=stname(bmp(1:nk));
        end
        bpmhv=struct(...
            'qp',num2cell(qp,2),...
            'bump', num2cell(bump,2),...
            'kname', kname,...
            'kicks',num2cell(kicks,1)');
    end

    function qpset(qpdev,cmd,offvalue,onvalue)
        if strcmp(cmd,'On')
            qpdev.Current=onvalue;
        else
            qpdev.Current=offvalue;
        end
    end

    function s=buildstructure(plane, st, stname, resp, deltaf)
        steerdev=tango.Device(['sr/st-' lower(plane) '/all']);
        setv=steerdev.Current.set;
        devices={'steerdev',steerdev};
        if (lower(plane) == 'h') && (nargin >= 5)
            freqdev=tango.Device('sy/ms/1');
            devices=[devices {'freqdev',freqdev,'deltaf',deltaf}];
            setv=[setv,freqdev.Frequency.set];
        end
        s=struct(devices{:},...
            'inisteerval', setv,...
            'step', 150E-06,...
            'steerer', struct('vals',num2cell(st,2),'name',stname),...
            'resp' , resp);
    end

    function resp2=refurb(resp)
        resp2=srmresp(resp,16);
        ok=all(isfinite(resp),2);
        resp2(ok,:)=resp(ok,:);
    end

    function [qpname,qpfunc]=c23config()
        qpn={...
            'sr/ps-qd4/c22';...
            'sr/ps-qf7hg/c22';...
            'sr/ps-qf7hg/c23';...
            'sr/ps-qd4/c23';...
            'sr/ps-qf5/c23'};
        qpv=arrayfun(@(d) d.Current.set,tango.Device(qpn));
        qpf={...
            @(dev,cmd) qpset(dev,cmd,qpv(1),0.99*qpv(1));...
            @(dev,cmd) qpset(dev,cmd,qpv(2),0.99*qpv(2));...
            @(dev,cmd) qpset(dev,cmd,qpv(3),0.99*qpv(3));...
            @(dev,cmd) qpset(dev,cmd,qpv(4),0.99*qpv(4));...
            @(dev,cmd) qpset(dev,cmd,qpv(5),0.99*qpv(5))};
        qpname=qpn([1 2 2 3 3 4 5]);
        qpfunc=qpf([1 2 2 3 3 4 5]);
    end
end
