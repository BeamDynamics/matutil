function sds=load_sds(varargin)

[params,sd]=load_optics(varargin{:},'id');

sds.s=params.ll*sd(:,1);
sds.bx=sd(:,2);
sds.phix=params.nuh*sd(:,3);
sds.bz=sd(:,5);
sds.phiz=params.nuv*sd(:,6);
