function r=trajem(bpm,st,nu,nbeam)
%R=TRAJEM(OBS,EXC,NU,NBEAM) builds trajectory response from EXC to OBS
%
%	OBS,EXC:	[beta Phi/2PiNu]
%	NU:			tune
%	NBEAM:		number of BPMS
%
%	see also RESPONSEM

nbpms=size(bpm,1);
if nargin < 4
	nbeam=nbpms;
else
	nturns=ceil(nbeam/nbpms);			% number of turns
	dphi=0:nturns-1;
	bet=bpm(:,ones(1,nturns));			% extend the BPM data
	phi=bpm(:,2*ones(1,nturns))+dphi(ones(nbpms,1),:);
	bpm=[bet(1:nbeam) phi(1:nbeam)];	% keep valid only;
end

a=sqrt(bpm(:,1)*st(:,1)');
b=(bpm(:,2)*ones(size(st(:,2)')) - ones(size(bpm(:,2)))*st(:,2)')*(2*pi*nu);
a(b<=0)=0;
r=a.*sin(b);
