function f=syfit(x,ring,resph,respv)

vqp=[x(2);repmat([x(1);x(2)],13,1)];
ring2=repmat(atsetparam(ring,'QP',vqp),3,1);
[params,bpm,steerh,steerv]=load_optics(ring2,'bpm','steerh','steerv');
h_resp=responsem(bpm(:,2:3),steerh(1:2:2*size(resph,2),2:3),params.nuh)*x(3);
v_resp=responsem(bpm(:,5:6),steerv(2:2:2*size(respv,2),5:6),params.nuv)*x(4);
dh=resph-h_resp;
dv=respv-v_resp;
f=std2(dh(:))+std2(dv(:));
