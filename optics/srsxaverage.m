function [ave,ave6m]=srsxaverage(sxdata,varargin)
%SRSXAVERAGE Averages over sextupoles after eliminating non-standard ones

sx6m=[...
    84 85 ...	% ID16
    182 183 ... % ID30
    ];

unfold=false;
mfunc=@mean2;
for i=1:length(varargin)
    if ischar(varargin{i})&&strcmpi(varargin{1},'unfold'), unfold=true; end
    if isa(varargin{i},'function_handle'), mfunc=varargin{i}; end
end
sz=size(sxdata);
v=reshape(sxdata,sz(1),[]);
[n1,n3]=size(v);
v6m=v(sx6m,:);
v(sx6m,:)=NaN;
vave=mfunc(srfold(v),2);
v6mave=mfunc(v6m,1);
if unfold
    v=srunfold(repmat(vave,[1 32 1]));
    v(sx6m,:)=repmat(v6mave,size(v6m,1),1);
    ave=reshape(v,sz);
else
    ave=reshape(squeeze(vave),[sz(1)/32 sz(2:end)]);
    ave6m=reshape(v6mave,[1 sz(2:end)]);
end
end

