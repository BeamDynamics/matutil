function f=reson_plot(norder_min,norder_max,nharm_max,ires,nuxmin,nuxmax,nuzmin,nuzmax)

%Usage: plot of all resonaces up to a given order
%Plot of resonances m*nux+n*nuz=M, with |m|+|n|=norder


fig1=findobj('Type','figure','Name','Tune Diagram');
	if isempty(fig1)
		fig1=figure('Name','Tune Diagram');
	else 
		figure(fig1)
	end
clf
hold on
axis([nuxmin nuxmax nuzmin nuzmax])


color={[0 0 0] [1 0 0],[0 0 1],[0 1 0],[1 0 1],[0 1 1 ] [1 1 0] [0 0 0] [1 .4 .6],[0 0 0],[0 0 0],[0 0 0],[0 0 0],[0 0 0],[0 0 0]};


nux=linspace(nuxmin,nuxmax,200);

for norder=norder_min:1:norder_max
	
	for nharm=1:1:nharm_max
	nharm;
		for m=-norder_max:1:norder_max
			for n=-norder_max:1:norder_max
				if(abs(m)+abs(n) == norder) 
				method=search_line(m,n);
				else
					method=4;
				end	
			
	
		switch method

		%plot of n*nuz=nharm		
		case 1
			yy=[nharm./n nharm./n];
			xx=[nuxmin nuxmax];
			if(yy(1)>=nuzmin & yy(2)<=nuzmax) 
				plot(xx,yy,'-','Color',color{norder})
				fprintf('order        m * nux + n * nuz = N      %5.0f   %5.0f    %5.0f        %5.0f \n',norder,m,n,nharm)
				if(ires==1)
					text(xx(2)+.002,yy(1),nharm,[num2str(m) ',' num2str(n) ',' num2str(nharm)] );
				end
			else
			end
			
		%plot of m*nux=nharm
		case 2
			xx=[nharm./m nharm./m];
			yy=[nuzmin nuzmax];
			if(xx(1)>=nuxmin & xx(2)<=nuxmax) 	
				plot(xx,yy,'-','Color',color{norder})
				fprintf('order        m * nux + n * nuz = N      %5.0f   %5.0f    %5.0f        %5.0f \n',norder,m,n,nharm)
				if(ires==1)
					text(xx(1)-.01,yy(2)+.005,nharm,[num2str(m) ',' num2str(n) ',' num2str(nharm)] );
				end
			else
			end
			
		%plot of m*nux+n*nuz=nharm
		case 3
			x=[0 0];
			y=[0 0];
			nuz=(nharm-m*nux)/n;
			test=(nuz>=nuzmin) & (nuz<=nuzmax);
			if(any(test)==1)
				igood=find(test);
				x=nux(igood);
				y=nuz(igood);
				plot(x,y,'-','Color',color{norder})
				fprintf('order        m * nux + n * nuz = N      %5.0f   %5.0f    %5.0f        %5.0f \n',norder,m,n,nharm)
				if(ires==1)
					text(x(1),y(1),nharm,[num2str(m) ',' num2str(n) ',' num2str(nharm)] );
				end
			end
		
			case 4
		end
			end
		end	
	end
end

axis square
grid on
xlabel('\nu_{x}');
ylabel('\nu_{z}');

function method=search_line(m,n)
if(m == 0 & n == 0 ) method=4;
else
	if(n == 0) method=2;
	else
		if(m == 0) method=1;
		else
			method=3;
		end
	end
end

