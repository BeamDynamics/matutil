function bpms=load_bpms(varargin)

[params,bpm]=load_optics(varargin{:},'bpm');
bpms.s=params.ll*bpm(:,1);
bpms.bx=bpm(:,2);
bpms.phix=params.nuh*bpm(:,3);
bpms.bz=bpm(:,5);
bpms.phiz=params.nuv*bpm(:,6);
