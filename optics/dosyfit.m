[ring,periods]=atreadbeta(fullfile(getenv('DBETA'),'model_sysym.str'));
vqp=atgetparam(ring,'QP');
[coefh,coefv]=load_corcalib('sy');
x0=[vqp(2);vqp(1);coefh;coefv];
[h0,v0]=load_syrsp(fullfile(getenv('APPHOME'),'sy','optics','settings','nom_sy','expresp.mat'));
hh=symresp(h0,3,[12 13]);
vv=symresp(v0,3,[13 26]);
%f=syfit(x0,ring,hh,vv);
%[x,f]=fminsearch(@syfit,x0,optimset(optimset('fminsearch'),'Display','iter'),ring,hh,vv);
[t,f]=fminsearch(@(t)syfit(t.*x0,ring,hh,vv),ones(4,1),optimset(optimset('fminsearch'),'Display','iter','TolX',1.e-5));
x=t.*x0;
[x0 x]
f

% result [0.735942,-0.659112,0.003870,0.003983]
% result [0.736106,-0.659255,0.00379587,0.00393469]
