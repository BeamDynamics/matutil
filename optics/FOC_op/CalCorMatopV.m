function [corfull, cor,maxcoef]= CalCorMatopV(Mres,bpm_list,steer_list,scalef, scalei)
%cor: correction matrix coefficient ready to be loaded using by the script SetCorMat ((24576X1 vector)
%corfull: 96X256 correction matrix
%maxc: must be less than 32768 (16 bits number)
nbpm1=size(bpm_list);
nbpm=max(nbpm1)
%bpm_list=[bpm_list,225];
nsteer1=size(steer_list);
nsteer=max(nsteer1)
%steer_list=[steer_list,97];
Mres2=zeros(nbpm,nsteer);
Mres2=Mres(bpm_list,steer_list);
[U,S,V]=svd(Mres2);
sizeS=size(S)
T=zeros(sizeS);
for i=1:nsteer
    T(i,i)=1/S(i,i);
end

Mcor=V*T'*U';

Mcor(nsteer,:)=Mcor(nsteer,:)*scalef;
Mcor(:,nbpm)=Mcor(:,nbpm)*scalei;
%size(Mcor)
corfull=zeros(97,256);
corfull(:,256)=zeros(97,1);
corfull(steer_list,bpm_list)=Mcor;
corfull(:,225)=-1*corfull(:,225);
size(corfull)
maxc=max(max(abs(corfull*21.33)));
maxcoef=maxc/30000
corfull=round(corfull*21.33/maxcoef);
%corfull=round(corfull*21.33);
corsort=corfull;
Corfull=reshape(corsort',24832,1);
cor=Corfull';
max(abs(cor));
