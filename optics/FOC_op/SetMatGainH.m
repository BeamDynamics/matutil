function err= SetMatGainH(maxcH)
nameMG=['tango:sr/d-fofbcorrection/globalX//MatrixGain'];
devMG=dvopen(nameMG)
err=dvcmd(devMG,'DevWrite',maxcH);
