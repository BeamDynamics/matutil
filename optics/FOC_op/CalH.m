
function [corfull, cor,maxcoef]= CalCorMatopH(Mres,bpm_list,steer_list,scalef, scalei)
%cor: correction matrix coefficient ready to be loaded using by the script SetCorMat ((24576X1 vector)
%corfull: 96X256 correction matrix
%maxc: must be less than 32768 (16 bits number)
nbpm1=size(bpm_list);
nbpm=max(nbpm1)+1;
bpm_list=[bpm_list,225];
nsteer1=size(steer_list);
nsteer=max(nsteer1)+1;
steer_list=[steer_list,97];
Mres2=zeros(nbpm,nsteer);
Mres2=Mres(bpm_list,steer_list);
[U,S,V]=svd(Mres2);
sizeS=size(S)
T=zeros(sizeS);
%for i=1:nsteer
    %T(i,i)=1/S(i,i);
%end
for i=1:nsteer
T(i,i)=1/S(i,i);
end
%for i=80:nsteer
%T(i,i)=1/S(33,33);
%end


Mcor=V*T'*U';
%plot(Mcor(nsteer,1:nbpm-1)');

Mcor(nsteer,:)=Mcor(nsteer,:)*scalef/125;

% OK pour G= -20) et scaling par par maxcoef=.3 ...
Mcor(:,nbpm)=Mcor(:,nbpm)*scalei*5e3*120*1e-2;
%plot(Mcor(nsteer,1:nbpm-1)');

size(Mcor);
corfull=zeros(97,256);
corfull(:,256)=zeros(97,1);
corfull(steer_list,bpm_list)=Mcor;
plot(corfull([97],1:224)');

%-1 multiplication to deal with a DS convention... 
corfull(:,225)=-1*corfull(:,225);

corfull(97,225)=0;

[Maxi,ind]=max(corfull(1:96,1:224));
maxc=max(max(abs(corfull*21.33)));
maxcoef=maxc/30000
corfull=(round(corfull*21.33/maxcoef));


% trim the coefficients of corfull to make the sum of the corrections to be as
% close to zero as possible
 for i=1:224
sumC=0;
for j=1:96
sumC=sumC+corfull(j,i);
end
corfull(ind(i),i)=corfull(ind(i),i)-sumC;
 end


size(corfull)
corsort=corfull;
Corfull=reshape(corsort',24832,1);
cor=Corfull';

maxc=max(max(abs(cor)));