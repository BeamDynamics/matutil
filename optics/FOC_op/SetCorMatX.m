function [ Mcor,err]= SetCorMatX(Mcor)
nameMcor=['tango:sr/d-fofbcorrection/globalX//CorrectionMatrix'];
devMcor=dvopen(nameMcor)
nameMcor_sp=['tango:sr/d-fofbcorrection/globalX//CorrectionMatrix_sp'];
devMcor_sp=dvopen(nameMcor_sp)
err=dvcmd(devMcor_sp,'DevWrite',Mcor);
Mcor=dvcmd(devMcor,'DevRead');