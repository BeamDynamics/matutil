function [ Mcor,err]= SetCorMatZ(Mcor)
nameMcor=['tango:sr/d-fofbcorrection/globalZ//CorrectionMatrix'];
devMcor=dvopen(nameMcor)
nameMcor_sp=['tango:sr/d-fofbcorrection/globalZ//CorrectionMatrix_sp'];
devMcor_sp=dvopen(nameMcor_sp)
err=dvcmd(devMcor_sp,'DevWrite',Mcor);
Mcor=dvcmd(devMcor,'DevRead');