function err= SetMatGainV(maxcV)
nameMG=['tango:sr/d-fofbcorrection/globalZ//MatrixGain'];
devMG=dvopen(nameMG)
err=dvcmd(devMG,'DevWrite',maxcV);
