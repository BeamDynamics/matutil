function [resh,resv]=compute_srcor(varargin)
%function [resh,resv]=compute_srcor(optics,targetdir,code,h_resp,v_resp,h_fresp,b_ok,sh_ok,sv_ok)
%COMPUTE_SRCOR    Compute autocor correction matrices
%
%COMPUTE_SRCOR(OPTICS,TARGETDIR,HRESP,VRESP,FRESP,BPMOK,SHOK,SVOK)
%
%OPTICS:	machine description: looks for:
%
% '/machfs/appdata/sr/optics/settings/theory'[,'sr']:
%			full path of optics directory
% 'sr' or 'sy':	machine name
%			path defaults to $(APPHOME)/mach/optics/settings/theory
% 'nombz2.5','sr':	 optics name and machine
%  at_struct[,'sr']: an Accelerator Toolbox structure
%TARGETDIR: Location of the results files
%CODE:      identifies the response matrix; 'theoretical' or 'measured'
%H_RESP:	Horizontal response matrix
%V_RESP:	Vertical response matrix
%H_FRESP:	Horizontal frequency response
%BPMOK:     Valid BPMs (logical 224x1)
%SHOK:      Valid horizontal steerers (logical 1x96)
%SVOK:      Valid horizontal steerers (logical 1x96)
%
%RESH:      Structure with fields resp2, svdsolve2, svdbase2, lambda, scalei, scaledf
%RESV:      Structure with fields resp2, svdsolve2, svdbase2, lambda
%

[srmodel,options]=getmodel(varargin{:});
[resdir,code,h_resp,v_resp,h_fresp]=deal(options{1:5});	% mandatory args
optarg={true(size(h_resp,1),1) true(1,size(h_resp,2)) true(1,size(v_resp,2))};
validargs=~cellfun(@isempty,options(6:end));          % optional arguments
optarg(validargs)=options([false(1,5) validargs]);
[b_ok,sh_ok,sv_ok]=deal(optarg{:});

[bpm,steerh,steerh_name,steerv,steerv_name]=srmodel.get('bpm','steerh',...
    'steerh_name','steerv','steerv_name');

periods=16;

xlist=[1 3 4 6];	% select a subset of vertical steerers for "long" bumps
nsv=size(steerv,1);
sorder=reshape(1:nsv,nsv/periods,periods);
xok=sorder(xlist,:);

[b,f,pu,npu]=bump_coefs([h_resp h_fresp],bpm(:,1),steerh(:,1),periods);
kh=[b(1:3,:)' pu-1 npu f];

[b,f,pu,npu]=bump_coefs(v_resp,bpm(:,1),steerv(:,1),periods);
ks=[b' pu-1 npu f];

[b,f,pu,npu]=bump_coefs(v_resp(:,xok),bpm(:,1),steerv(xok,1),periods);
kv=[b' pu-1 npu f];

dlmwrite('h_resp.csv',[h_resp,h_fresp]);
dlmwrite('v_resp.csv',v_resp);
resh=store_svd(fullfile(resdir,'h_svd'),h_resp,b_ok,sh_ok,h_fresp);
resv=store_svd(fullfile(resdir,'v_svd'),v_resp,b_ok,sv_ok);
store_bump(fullfile(resdir,'h_bumps'),kh,steerh_name);
store_bump(fullfile(resdir,'v_bumps'),kv,steerv_name(xok));
store_bump(fullfile(resdir,'v_short_bumps'),ks,steerv_name);
store_variable(fullfile(resdir,'matlog'),'disabled_bpms',sr.bpmname(~b_ok));
store_variable(fullfile(resdir,'matlog'),'disabled_sh',sr.steername(~sh_ok','SR/ST-H%i/C%i'));
store_variable(fullfile(resdir,'matlog'),'disabled_sv',sr.steername(~sv_ok','SR/ST-V%i/C%i'));
store_variable(fullfile(resdir,'matlog'),'resp',code);
end
