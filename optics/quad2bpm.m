function b=quad2bpm
%B=QUAD2BPM builds the quad to bpm displacement matrix

b=srsparse([1 2 4 5 7 9 10],10);
