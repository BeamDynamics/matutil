function g=girder2quad
%G=GIRDER builds the girder to quad displacement matrix

g=srsparse([1 1 1 2 2 2 2 3 3 3],3);
