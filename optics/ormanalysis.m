function res=ormanalysis(opticsdir,code,varargin)
%ORMANALYSIS Analysis of orbit response matrices
%   Detailed explanation goes here

if nargin < 1
   code=-1;
end
switch code
   case {1,'orm'}
	  planes={'H','V','V2H','H2V'};
	  
	  for i=1:4
		 pl=planes{i};
		 [res.(pl),current]=load_srresp(opticsdir,pl,varargin{:});
		 subplot(4,1,i);
		 bar(1.e6*std2(res.(pl)));
		 title(pl);
		 set(gca,'XLim',[0 length(current)+1]);
		 xlabel('Steerer #');
		 ylabel('x [\mum]');
	  end
   otherwise
	  disp('help');
end
end

