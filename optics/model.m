function [bpm,steerer]=model(srmodel,plane,excode,obscode)
%[obs,exc,alpha,nu,params]=MODEL(RINGMODEL,PLANE,EXCCODE,OBSCODE) get optics data
%
%	RINGMODEL	Object of class sr.model or st.model ot ebs.model
%	PLANE:		'h' or 'v'
%	EXCCODE:	ST | QP SX ID CORNQ CORSQ CORSX | HIGH LOW IDx
%	OBSCODE:	PU | QP SX ID| PIN8 PIN25 PIN9 HIGH LOW IDx
%
%	parameters are: theta/2Pi Beta Phi/2PiNu etax [strength]
%
% For ST structures, valid codes are:
%	EXCCODE:	ST | SX ID CORNQ CORSQ CORSX | HIGH LOW IDx
%	OBSCODE:	PU | SX ID | HIGH LOW IDx
%
% PARAMS:	structure containing the following fields:
%           periods,nuh,nuv,ll,alpha,emitx,emitz,strname

[pars,obspl,excpl]=selectplane(plane,{[2 3 5 6],[2 8 10 6]},...
    {'hbpm','vbpm'},{'steerh','steerv'});

if strcmpi(obscode,'pu'), obscode=obspl; end

if strcmpi(excode,'st'), excode=excpl; end

switch lower(excode)
case {'qp','sx'}
   parsexc=[pars 1];
otherwise
   parsexc=pars;
end

[bpm,steerer]=srmodel.get(pars,obscode,parsexc,excode);
end
