function [bpm,h_steerers,v_steerers,kh,kv]=sy_model(path,exper)

if nargin < 2, exper=false; end
if nargin < 1, path=pwd; end

error('You should use "sy.autocor_model"')
[coefh,coefv]=load_corcalib('sy');

[params,bpm,bpm_name,h_bpms,v_bpms,steerh,steerh_name,steerv,steerv_name]=load_optics(path,'sy','bpm','bpm_name',...
   'h_bpms','v_bpms','steerh','steerh_name','steerv','steerv_name');
sh_bpm=size(h_bpms)
sv_bpm=size(v_bpms)
if exist('./alpha') ~= 2,
   savetext('alpha','%.4e',params.alpha);
end

scaling=input('Energy scaling factor (1.0) : ');

nb=size(bpm,1);
b_ok=true(nb,1);
b_okh=true(nb,1);
b_okv=true(nb,1);
disabled_b=[];

if ~exper
   h_resp=responsem(bpm(:,2:3),steerh(:,2:3),params.nuh)*coefh;
   sh_ok=true(1,size(h_resp,2));
   v_resp=responsem(bpm(:,5:6),steerv(:,5:6),params.nuv)*coefv;
   sv_ok=true(1,size(v_resp,2));
   rfreq=bpm(:,4);
else
%The next line was used for processing matrices measured by Yannis 
   %[h_resp,v_resp]=load_syrsp(fullfile(path,'expresp.mat'));
   [h_resp,v_resp,bpmfaulth,bpmfaultv]=load_syresp2(fullfile(path,'expresp.mat'));
    sh_ok=true(1,size(h_resp,2));
   sv_ok=true(1,size(v_resp,2));
   rfreq=bpm(:,4);

end

[b f pu npu]=bump_coefs(h_resp,bpm(:,1),steerh(:,1),params.periods);
kh=[b' pu-1 npu f];

[b f pu npu]=bump_coefs(v_resp,bpm(:,1),steerv(:,1),params.periods);
kv=[b' pu-1 npu f];

if ~isempty(scaling)
  if (scaling ~= 1)
   kh(:,1:3)=kh(:,1:3)*scaling;
   kv(:,1:3)=kv(:,1:3)*scaling;
   h_resp=h_resp/scaling;
   v_resp=v_resp/scaling;
   coefh=coefh/scaling;
   coefv=coefv/scaling;
  end
end

hv_bpms=[bpm(:,1:2) 2*pi*bpm(:,3) bpm(:,4:5) 2*pi*bpm(:,6)];
h_steerers = [steerh(:,1:2) 2*pi*steerh(:,3) coefh*ones(size(steerh,1),1)];
v_steerers = [steerv(:,[1 5]) 2*pi*steerv(:,6) coefh*ones(size(steerv,1),1)];
store_bpm('bpms',hv_bpms,bpm_name);
store_steerer('h_steerers',h_steerers,steerh_name);
store_steerer('v_steerers',v_steerers,steerv_name);
%added by T.P. Removes bad bpms selected by the operator after questioning in loadsyresp2 
b_okh(disabled_b)=false;
b_okh(bpmfaulth)=false; 
b_okv(disabled_b)=false;
b_okv(bpmfaultv)=false;

%b_okh(disabled_b)=false;
%b_okv(disabled_b)=false;


store_svd('h_svd',h_resp,h_bpms&b_okh,sh_ok,rfreq);
store_svd('v_svd',v_resp,v_bpms&b_okv,sv_ok);
store_bump('h_bumps',kh,steerh_name);
store_bump('v_bumps',kv,steerv_name);
store_variable('matlog','disabled_bpms',bpm_name(disabled_b));
store_variable('matlog','resp','theoretical');
