function [pm,varargout]=load_atoptics(atstruct,varargin)
%[PARAMS,V1,V2,...]=LOAD_OPTICS(OPTICS,CODE1,CODE2,...) Load optics parameters
%
%	OPTICS	AT ring description
%
% Accelerator Toolbox processing for load_optics

out=1;
nout=nargout-1;
narg=1;
nin=nargin-1;
ndata=length(atstruct)+1;
pm.ll=findspos(atstruct,ndata);
pm.alpha=mcf(atstruct);
pm.strname='?';
[lindata,avebeta,avemu,avedisp,tune,chrom]=atavedata(atstruct,0.0,1:ndata); %#ok<ASGLU>
tune=lindata(end).mu/2/pi;
pm.nuh=tune(1);
pm.nuv=tune(2);
if nout > 0
    spos=cat(1,lindata.SPos)/pm.ll;
    vlave=[[0.5*(spos(1:end-1)+spos(2:end));spos(end)] ...
        avebeta ...
        cat(1,lindata.alpha) ...
        avemu./tune(ones(ndata,1),:)/2/pi ...
        avedisp ...
        NaN*ones(ndata,1) (1:ndata)'];
    vlini=[spos ...
        cat(1,lindata.beta) ...
        cat(1,lindata.alpha) ...
        cat(1,lindata.mu)./tune(ones(ndata,1),:)/2/pi ...
        cat(2,lindata.Dispersion)' ...
        NaN*ones(ndata,1) (1:ndata)'];
    
    fileorder=[2 3 8 4 9 5 10 6 7 11 12 1 0];
    locs(fileorder+1)=1:length(fileorder);
    params=locs([2 3 5 6 8 10]+1);	% default parameter list as in load_elems
    
    while ((narg<=nin) && (out<=nout))
        if isnumeric(varargin{narg})
            params=locs(varargin{narg}+1);
        else
            incode=lower(varargin{narg});
            suffix=strfind(lower(incode),'_name');
            if isempty(suffix)
                idx=elselect(atstruct,incode);
                switch incode
                    case {'id','bpm','hbpm','vbpm','pin25','pin9','pin8','bm','d2','d1','high','low'}
                        varargout{out}=vlini(idx,params); %#ok<*AGROW>
                    case {'steerh','steerv','corsx'}
                        varargout{out}=vlave(idx,params);
                    case 'sx'
                        vlave(idx,12)=getcellstruct(atstruct,'PolynomB',idx,3);
                        varargout{out}=vlave(idx,params);
                    case 'qp'
                        if exist('k','var') ~= 1
                            k=getcellstruct(atstruct,'PolynomB',idx,2);
                            l=getcellstruct(atstruct,'Length',idx);
                            vlave(idx,12)=k.*l;
                        end
                        varargout{out}=vlave(idx,params);
                    case 'cornq'
                        vlave(idx,12)=getcellstruct(atstruct,'PolynomB',idx,2);
                        varargout{out}=vlave(idx,params);
                    case 'corsq'
                        vlave(idx,12)=getcellstruct(atstruct,'PolynomA',idx,2);
                        varargout{out}=vlave(idx,params);
                    otherwise
                        if strncmp(incode,'id',2)
                            varargout{out}=vlini(idx,params);
                        elseif strncmp(incode,'hilo',4)
                            varargout{out}=vlini(idx,params);
                        else
                            varargout{out}=vlave(idx,params);
                        end
                end
            else
                [~,varargout{out}]=elselect(atstruct,incode(1:suffix-1));
            end
            out=out+1;
        end
        narg=narg+1;
    end
    
end

    function [idx,namex]=elselect(atstruct,code)
        namex={};
        switch lower(code)
            case 'id'
                idx=find(atgetcells(atstruct,'FamName','IDMarker'));
            case {'bpm','hbpm','vbpm'}
                idx=findcells(atstruct,'Class','Monitor');
            case 'steerh'
                idx=allsexts(atstruct);
                [idn,namex]=selcor(8);
                idx=idx(idn);
            case 'steerv'
                idx=allsexts(atstruct);
                [idn,namex]=selcor(9);
                idx=idx(idn);
            case 'sx'
                idx=allsexts(atstruct);
            case 'qp'
                idx=findcells(atstruct,'Class','Quadrupole');
            case 'cornq'
                idx=allsexts(atstruct);
                [idn,namex]=selcor(6);
                idx=idx(idn);
            case 'corsq'
                idx=allsexts(atstruct);
                [idn,namex]=selcor(10);
                idx=idx(idn);
            case 'corsx'
                idx=allsexts(atstruct);
                [idn,namex]=selcor(3);
                idx=idx(idn);
            case 'pin25'
                idx=reshape(findcells(atstruct,'FamName','B1H'),[],32);
                idx=idx(1,22);
            case 'pin9'
                idx=reshape(findcells(atstruct,'FamName','B2S'),[],32);
                idx=idx(1,6);
            case {'bm','d2'}
                idx=reshape(findcells(atstruct,'FamName','B2S'),[],32);
                idx=idx(1,:);
            case 'd1'
                idx=reshape(findcells(atstruct,'FamName','B1H'),[],32);
                idx=idx(1,:);
            case 'high'
                idx=find(atgetcells(atstruct,'FamName','IDMarker'));
                idx=idx(1:2:end);
            case 'low'
                idx=find(atgetcells(atstruct,'FamName','IDMarker'));
                idx=idx(2:2:end);
            otherwise
                if strncmp(code,'id',2)
                    idx=find(atgetcells(atstruct,'FamName','IDMarker'));
                    idx=idx(hlrange(code(3:end)));
                elseif strncmp(code,'hilo',4)
                    idx=find(atgetcells(atstruct,'FamName','IDMarker'));
                    idx=idx(hlrange(code(5:end)));
                else
                    idx=findcells(atstruct,'FamName',code);
                end
        end
        if nargout >= 2 && isempty(namex)
            namex=atgetfieldvalues(atstruct(idx),'FamName');
        end
    end

    function idx=allsexts(atstruct)
        sx=atgetcells(atstruct,'Class','ThinMultipole') | ...
            atgetcells(atstruct,'Class','Sextupole');
        idx=find(sx);
    end

    function rg=hlrange(cx)
        if isempty(cx)
            rg=1:32;
        else
            cx2=str2num(cx)-4; %#ok<ST2NM>
            rg=cx2+32*(cx2<0)+1;
        end
    end

end
