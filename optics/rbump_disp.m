function rbump_disp(mess,v)
%RBUMP_DISP(MESS,V)	Display offset error

diary('on');
disp(sprintf('%15s: ampl: %6.3f/%6.3f, rms: %6.3f/%6.3f, reading: %6.3f/%6.3f',...
	upper(mess), 1000*v([1:2 9:10 3:4])));
%disp(sprintf('%37s harm: %g/%g',' ',1000*v(5:6)));
disp(sprintf('%37s fit:  %g/%g',' ',1000*v(7:8)));
diary('off');
