function resonplot(exc,tit,devs)

sz=max(size(exc));
ind=reshape(1:sz,2,sz/2);
style={'y-';'r-';'m-';'c-';...
    'y:';'r:';'m:';'c:'};

lines=size(exc,1)/2;
sets=size(exc,2)/2;
rows=ceil(lines/2);

for reson=1:lines
    subplot(rows,2,reson);
    for k=1:sets
        styleidx=mod(k-1,8)+1;
        z=exc(ind(:,reson),ind(:,k));
        hl(ind(:,k))=compass(z(1,:),z(2,:),style{styleidx});
        %text(z(1,1),z(2,1),int2str(ind(1,k)),'FontSize',8);
        hold on
    end
    hold off
    if nargin >=2, title(int2str(tit(reson))); end
end
