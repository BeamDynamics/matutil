function [qexc,kexc,sexc]=beta_reson(origin)
%BETA_RESON	produces resonance correction files for skmag
%	[qexc,kexc,sexc]=beta_reson(origin)
%^	origin: cell number of origin of phases

if nargin < 1, origin=4; end

[params,bpm,sext]=load_optics(pwd,'bpm','sx');
nux=params.nuh;
nuz=params.nuv;

[lquad,devquad]=selcor(1,origin);
[lskew,devskew]=selcor(2,origin);
[lsext,devsext]=selcor(3,origin);
[lhdip,devhdip]=selcor(4,origin);
[lvdip,devvdip]=selcor(5,origin);
[lquad32,devquad32]=selcor(6,origin);
[lskew32,devskew32]=selcor(7,origin);
[qexc,rr]=newrquad(sext(lquad,:),nux,nuz,'free');
%[qexc rr]=newrquad(sext(lquad,:),nux,nuz,'coupled');
%[qexc rr]=newrquad(sext(lquad,:),nux,nuz,'coupledbz2.5');
[kexc,rr]=newrskew(sext(lskew,:),nux,nuz,'free',bpm);
%[sexc rr]=newrsext(sext(lsext,:),nux,nuz,'free');
[sexc,rr]=newrsext6(sext(lsext,:),nux,nuz);
[hexc,rr]=newrhdip(sext(lhdip,:),nux,nuz);
[vexc,rr]=newrvdip(sext(lvdip,:),nux,nuz);
[qexc32,rr]=newrquad(sext(lquad32,:),nux,nuz,'free');
[kexc32,rr]=newrskew(sext(lskew32,:),nux,nuz,'free',bpm);
