function hlist=tunediag(varargin)
%Plot resonances in the tune diagram
%
%tunediag(order,tunerange)
%   order: array of resonance orders
%   tunerange: [nuxmin nuxmax nuzmin nuzmax]
%
%tunediag(order,tunerange,select)
%   select: array of 3 logicals:
%       plot normal resonances
%       plot skew resonances
%       plot non-systematic resonances
%   default: all true
%
%tunediag(order,tunerange,select,ncells)
%   ncells: number of cells in the ring (to identify systematic resonances)
%
%tunediag(order,tunerange,...,tunes)
%   tunes: actual tunes to be plotted on the diagram
%
%tunediag(ax,order,tunerange...)
%   ax: plot resonances in the specified axes

select=true(1,3);
ncell=1;
lw2=[1 1];
here=[];
n1st=2;
if isscalar(varargin{1}) && ishandle(varargin{1}) && strcmp(get(varargin{1},'Type'),'axes')
    axes(varargin{1});
    n1st=3;
else
    fig1=findobj('Type','figure','Name','Tune Diagram');
    if isempty(fig1)
        figure('Name','Tune Diagram');
    else
        figure(fig1);
        clf
    end
    set(gca,'DataAspectRatio',[1 1 1]);
    xlabel('\nu_x','FontSize',14);
    ylabel('\nu_z','FontSize',14);
    %set(gcf,'Color','none');
end
[orderrange,tns]=deal(varargin{n1st-1:n1st});
if isnumeric(tns)&&(length(tns)==4)
    n1st=n1st+1;
    tunes=tns;
    axis(tunes);
else
    tunes=axis();
end
args=varargin(n1st:end);
idncell=selarg(@(arg) isnumeric(arg)&&isscalar(arg),args);  % look for cell number
if ~isempty(idncell), ncell=args{idncell};select(3)=false; end
idsel=selarg(@islogical,args);                              % look for selector
if ~isempty(idsel), select(1,1:length(args{idsel}))=args{idsel}; end
idhere=selarg(@(arg) isnumeric(arg)&&(length(arg)==2),args);% look for actual tunes
if ~isempty(idhere), here=args{idhere}; end
%if select(3) && ncell>1, lw2(2)=3; end
if ncell>1, lw2(2)=3; end

hold('on');

color={ [0 0 0],[1 0 0],[0 0 1],...
    [0 0.5 0],[0.75 0 0.75],[0 0.75 0.75],...
    [0.5 0.5 0.5],[1 0.5 0.5],[0.5 0.5 1]};
style={'-','--'};

hlist=[];
for order=orderrange(end:-1:1)
    line=color{mod(order-1,length(color))+1};
    plotxz(0,order);
    for m=1:order-1
        plotxz(m,order-m);
        plotxz(m,m-order);
    end
    plotxz(order,0);
end
if ~isempty(here)
    h=plot(here(1),here(2),'.g','MarkerSize',18);
    hlist=[hlist;h];
end

    function h=plotxz(m,n)
        skew=mod(n,2)+1;
        h=[];
        if select(skew)
            plist=[m*tunes(1)+n*tunes(3) m*tunes(2)+n*tunes(3) m*tunes(1)+n*tunes(4) m*tunes(2)+n*tunes(4)];
            disp([m n ceil(min(plist)) floor(max(plist))])
            for p=ceil(min(plist)):floor(max(plist))
                system=mod(p,ncell)==0;
                if system || select(3)
                    x=[tunes(1:2) (p-n*tunes(3:4))./m];
                    z=[(p-m*tunes(1:2))./n tunes(3:4)];
                    ok=(x>=tunes(1)) & (x<=tunes(2)) & (z>=tunes(3)) & (z<=tunes(4));
                    if sum(ok) > 2
                        [x,mm]=unique(x(ok));
                        z=z(ok);
                        z=z(mm);
                        ok=true(1,2);
                    end
                    %linewidth=lw2((system&&(skew==1))+1);
                    linewidth=lw2(system+1);
                    h=plot(x(ok),z(ok),'Color',line,'LineStyle',style{skew},...
                        'LineWidth',linewidth,...
                        'DisplayName',sprintf('%d\\nu_x+%d\\nu_z=%d',m,n,p));
                    hlist=[hlist;h]; %#ok<AGROW>
                end
            end
        end
    end
    function idx=selarg(testfun,args)
        v=cellfun(testfun,args,'UniformOutput',false);
        idx=find(cat(1,v{:}),1,'first');
    end
end
