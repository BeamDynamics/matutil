function [qexc,kexc,sexc]=ar_reson(origin)

if nargin < 1, origin=4; end

if exist('./quadcor') == 2
   load quadcor -ascii
   qexc=quadcor(1:8,:);
   figure;
   resonplot(qexc,[73 72 23 22]);
else
   qexc=[];
end
if exist('./skewcor') == 2
   load skewcor -ascii
   kexc=skewcor(1:4,:);
   figure;
   resonplot(kexc,[48 25]);
else
   kexc=[];
end
if exist('./sextcor') == 2
   load sextcor -ascii
   sexc=sextcor(1:6,:);
   figure;
   resonplot(sexc,[109 108 67]);
else
   sexc=[];
end
