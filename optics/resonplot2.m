function resonplot2(exc,tit)

lines=fix(size(exc,1)/2);
histo=size(exc,1);
if histo == 2*lines, histo=0; end
ind=reshape(1:2*lines,2,lines);
style=['y-';'r-';'m-';'c-';'y:';'r:';'m:';'c:'];

sets=size(exc,2);
rows=1+ (lines>2) + (histo>0);

for reson=1:lines
subplot(rows,2,reson);
for k=1:sets
z=exc(ind(:,reson),k);
compass(z(1,:),z(2,:),style(k,:))
hold on
end
hold off
if nargin >=2, title(int2str(tit(reson))); end
end

if histo
subplot(rows,2,2*rows-1)
bar(exc(histo,:));
end
