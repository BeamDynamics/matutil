classdef syatmodel < sy.model
    %SYATMODEL Deprecated: Use sy.model instead
    methods
        function this=syatmodel(varargin)
            warning('obsolete:model','%s is deprecated: Use sy.model instead',mfilename);
            this@sy.model(varargin{:});
        end
    end
end
