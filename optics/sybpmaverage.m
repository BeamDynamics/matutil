function xave=sybpmaverage(x,plane)
%SYBPMAVERAGE			average BPM values using SY symmetries

a=reshape(syexpand(x),26,3);
vsave=a(25);
a(25)=NaN;
a=mean2(a,2);
a=repmat(mean2([a a([25:-1:1 26])],2),3,1);
a(25)=vsave;
xave=syreduce(a,plane);
