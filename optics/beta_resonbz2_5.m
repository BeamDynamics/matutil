function [qexc,kexc,sexc]=beta_reson(origin)
%BETA_RESON	produces resonance correction files for skmag
%	[qexc,kexc,sexc]=beta_reson(origin)
%^	origin: cell number of origin of phases

if nargin < 1, origin=4; end

[bpm,sext,nux,nuz]=load_betalog;

[lquad,devquad]=selcor(1,2);
[lskew,devskew]=selcor(2,origin);
[lsext,devsext]=selcor(3,2);
[lhdip,devhdip]=selcor(4,origin);
[lvdip,devvdip]=selcor(5,origin);
[lquad32,devquad32]=selcor(6,origin);
[lskew32,devskew32]=selcor(7,origin);
%[qexc rr]=newrquad(sext(lquad,:),nux,nuz,'free');
%[qexc rr]=newrquad(sext(lquad,:),nux,nuz,'coupled');
[qexc rr]=newrquad(sext(lquad,:),nux,nuz,'coupledbz2.5');
[kexc rr]=newrskew(sext(lskew,:),nux,nuz,'free',bpm);
[sexc rr]=newrsext(sext(lsext,:),nux,nuz,'free');
[hexc rr]=newrhdip(sext(lhdip,:),nux,nuz,'free');
[vexc rr]=newrvdip(sext(lvdip,:),nux,nuz,'free');
[qexc32 rr]=newrquad(sext(lquad32,:),nux,nuz,'free');
[kexc32 rr]=newrskew(sext(lskew32,:),nux,nuz,'free',bpm);
