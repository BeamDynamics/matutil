% OPTICS
%
% Files
%   apply_bump       - 
%   applyskew        - [H2H,V2H,H2V,V2V]=APPLYSKEW(bpm0,sext0,nux,nuz,sxdisp,corr)
%   applysxdisp      - [H2H,V2H,H2V,V2V]=SEXTUDISP(SKEW) computes coupled resp. matrix for skew atrengths at steerer locations
%   ar_reson         - 
%   beta_constants   - A=BETA_CONSTANTS(SD_CODE,D9_CODE,ID8_CODE creates "Beta_Constants" and "emittance.res"
%   beta_f           - leaves in the workspace:
%   beta_inj         - 
%   beta_model       - 
%   beta_reson       - produces resonance correction files for skmag
%   beta_resonbz2_5  - BETA_RESON	produces resonance correction files for skmag
%   betanorm         - comptes beta values on BPM and steerers at the end of iteration
%   bphase           - remove unprecise values ( cos > 1 )
%   bump_coefs       - [kicks,eff,first_bpm,nb_bpm]=BUMP_COEFS(resp,sb,sk,periods,averaging)
%   bumpbpmlist      - 
%   corskew          - computes response matrices for coupling correction
%   corsvd2          - [base,lambda,v]=corsvd2(response) computes SVD correction
%   cosmatr          - 
%   defreq           - rext=[r rfreq];
%   emit_growth      - EM=EMIT_GROWTH(X,XP,BETA,EMIT0)
%   enter_disbpm     - 
%   exp_model        - 
%   getmachenv       - % extracts machine information from argument list
%   girder2quad      - G=GIRDER builds the girder to quad displacement matrix
%   girder2sext      - G=GIRDER builds the girder to sextupole displacement matrix
%   girderalign      - corresp=qresp*girder2quad;
%   girdergo         - bar(0:nmax,std(cumcor));
%   girdrot2quad     - G=GIRDER builds the girder rotation to quad displacement matrix
%   gnrot2quad       - G=GNROT2QUAD builds the girder scaled rotation to quad displacement matrix
%   gosvd            - cor=GOSVD(lambda,base,y) computes eigen corrections
%   harmana          - betay=(sqrt(bpm(ok,1))*ones(1,c)).*y(ok,:);
%   harmodisp        - imagesc(0:col-1,1:row,abs(h));
%   jack2rot         - 
%   jack2trans       - 
%   load_atoptics    - [PARAMS,V1,V2,...]=LOAD_OPTICS(OPTICS,CODE1,CODE2,...) Load optics parameters
%   load_bpms        - 
%   load_bump        - 
%   load_fsroptics   - [PARAMS,SMACH]=LOAD_FSROPTICS(PATH) Load optics parameters
%   load_fsyoptics   - [PARAMS,SMACH]=LOAD_FSROPTICS(PATH) Load optics parameters
%   load_optics      - [PARAMS,V1,V2,...]=LOAD_OPTICS(OPTICS,CODE1,CODE2,...) Load optics parameters
%   load_sds         - sd=sd(keep(range),:);
%   local_bump       - [kicks,eff,orbit]=LOCAL_BUMP(resp,sb,sk,kindex) computes local bumps
%   ma2rad           - 
%   matchemit        - computes the surrounding matched emittance
%   meffsolve        - cor=meffsolve(y,resp) computes most effective corrector
%   missing_bpms     - 
%   model            - [obs,exc,alpha,nu]=MODEL(OPTICS,PLANE,EXCCODE,OBSCODE) get optics data
%   newrhdip         - response 5x8
%   newrquad         - response 9x16
%   newrsext         - response (6+1)x12
%   newrskew         - response 5x16
%   newrvdip         - response 5x8
%   posang           - 
%   quad2bpm         - B=QUAD2BPM builds the quad to bpm displacement matrix
%   r2nux            - 
%   r2nuz            - 
%   r3nux            - 
%   resonplot        - text(z(1,1),z(2,1),int2str(ind(1,k)),'FontSize',8);
%   resonplot2       - 
%   resonplot3       - 
%   resontest        - BETA_RESON	produces resonance correction files for skmag
%   respmat          - [resp,obs,exc]=respmat(optics,mode,nu)
%   responsem        - R=RESPONSEM(OBS,EXC,NU) buids response matrix from EXC to OBS
%   responsexm       - R=RESPONSEM(OBS,EXC,NU) buids response matrix from EXC to OBS
%   rnux             - 
%   rnuxmnuz         - 
%   rnuxp2nuz        - 
%   rnuxpnuz         - 
%   rnuz             - 
%   rquadbz2_5       - r2nux(sext,px1,nux);...
%   rsext            - r3nux(sext,h1,nux);...
%   rskew            - rnuxpnuz(sext,h1,nux,nuz);...
%   save_setting     - saves an application setting file
%   selcor           - [LIST,DEVLIST]=SELCOR(CODE) selects the correctors from the sextupole list
%   show_conditions  - 
%   showresp         - (OPTICS,PLANE,TUNE)		Displays SVD properties for the selected optics
%   split_run        - 
%   sresp            - 
%   srfold           - A=SRFOLD(B)			reshapes data for SR symmetry
%   srsparse         - G=SRSPARSE(GINDEX,NPERCELL,FACTOR) builds the quad to girder transformation
%   srunfold         - A=SRUNFOLD(B)			reshapes data for SR symmetry
%   stable_beam      - [inj,lost,i_ok]=stable_beam(i,nb_min,imin)	selects stable beam condiitons
%   steerscale       - 
%   stepb            - iterates on BPM beta values
%   stepk            - iterates on steerer beta values
%   store_bpm        - 
%   store_bump       - 
%   store_conditions - 
%   store_steerer    - 
%   store_svd        - vector 1 : RF dominant
%   svdanal          - Simulates SVD orbit correction
%   svdcomp          - svdinfo=svdcomp(response) computes SVD correction
%   svdsol           - cor=SVDSOLVE(y,svdinfo,nvect) computes eigen corrections
%   sy_model         - 
%   trajem           - R=TRAJEM(OBS,EXC,NU,NBEAM) builds trajectory response from EXC to OBS
