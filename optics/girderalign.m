if exist('corresp') ~= 1
cd([getenv('APPHOME') '/sr/optics/settings/theory']);
nu=input('Vertical tune: ');
qresp=respmat('qv',nu);
sresp=respmat('sv',nu);
%corresp=qresp*girder2quad;
%corresp=qresp*girdrot2quad;
corresp=qresp*(girder2quad*jack2trans + girdrot2quad*jack2rot);
load v_svd
norm=sqrt(sum(svdsolve.*svdsolve));
v=svdsolve./norm(ones(96,1),:);
idbpm=eval('dvopen(''SR/BPM/MASTER_UP'')','NaN');
idv=eval('dvopen(''SR/ST-V/ALL:SRVglob'')','NaN');
end

while 1
   resp=input('go ? (n/y): ','s');
   if ~strcmp(resp,'y'), break;end

   mode=input('Simul steerers (s) or Simul BPMs (b) or Server BPM (): ','s');
   if strcmp(mode,'b')
      load samplebpm
      oz=orbitz;
   elseif strcmp(mode,'s')
      load samplesteer
      oz=sresp*ma2rad('v',steerv);
   else
      [ox,oz]=dvreadbpm(idbpm)
   end

   correction=girdergo(corresp,-oz);
   disp(['max. cor(mm/mrad): ' num2str(1000*max(correction))]);
   disp(['rms. cor(mm/mrad): ' num2str(1000*std(correction))]);
end
