function [response,correction]=newrsext(sext,nux,nuz,code)

h1=floor(3*nux);
h3=floor(nux+2*nuz);
if nargin < 4, code='coupled'; end
%						response (6+1)x12
a=[...
	0.002*r3nux(sext,h1,nux);...
	0.002*r3nux(sext,h1-1,nux);...
	0.002*rnuxp2nuz(sext,h3,nux,nuz);...
	ones(1,size(sext,1)) ...
];
if strcmp(code,'coupled')
   exc=a(1:6,1:6)-a(1:6,7:12);	% apply pairs
   figure(4);
   resonplot(exc,[h1 h2 h3]);
   rexc=inv(exc);		% invert
   correction=[rexc ;-rexc]';	% transpose for C order
else
   figure(3);
   resonplot(a(1:6,:),[h1 h1-1 h3]);
   [u,s,v]=svd(a,0);		% take pseudo-inverse
   lambda=diag(s) %#ok<NOPRT>
   keep=1:length(lambda);
   ainv=v(:,keep)*diag(1./lambda(keep))*u(:,keep)';
   correction=ainv(:,1:6)';	% transpose for C order
end
i_rms=std(correction') %#ok<UDIM,NASGU,NOPRT>
response=a(1:6,:)';		% transpose for C order
info=ver('matlab');
matvers=sscanf(info.Version,'%d.%d');
if matvers(1) >= 5, fmode=' -v4'; else fmode='';end
eval(['save sextcor response correction' fmode]);
store_variable('matlog','sext_reson_mode',code);

    function exc=r3nux(sext,p,nuxx)
        disp(['3*nuX = ' num2str(p)]);
        n=(3*nuxx-p)/3;
        nx=nuxx-n;
        phase=2*pi*(3*nx*sext(:,3)+(p-3*nx)*sext(:,1));
%         phase=2*pi*(3*nuxx*sext(:,3)-p*sext(:,1));
        ampl=sext(:,2).^1.5;
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

    function exc=rnuxp2nuz(sext,p,nuxx,nuzz)
        disp(['nuX + 2 nuZ = ' num2str(p)]);
        m=(nuxx+2*nuzz-p)/3;
        nx=nuxx-m;
        nz=nuzz-m;
        phase=2*pi*(nx*sext(:,3)+2*nz*sext(:,6)+(p-(nx+2*nz))*sext(:,1));
%         phase=2*pi*(nuxx*sext(:,3)+2*nuzz*sext(:,6)-p*sext(:,1));
        ampl=sqrt(sext(:,2)).*sext(:,5);
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end
end
