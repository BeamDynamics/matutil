function [base,lambda,v2]=corsvd2(a)
%[base,lambda,v]=corsvd2(response) computes SVD correction
%	The correction is cor = - v * diag(1./lambda) * base' * y
%	or 		cor = v * gosvd(lambda,base,y)
%
%   See also: gosvd
%
%   These functions are superseded by svdcomp/svdsol

ok=find(all(isfinite(a')));
[u,s,v]=svd(a(ok,:),0);
sz=min(size(a));
base=NaN*ones(size(a,1),sz);
base(ok,:)=u;
lambda=diag(s);
v2=v(:,1:sz);
