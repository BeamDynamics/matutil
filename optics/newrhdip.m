function [a,ainv]=newrhdip(sext,nux,nuz)

px1=floor(nux);
px2=ceil(nux);
ncor2=size(sext,1)/2;
%				response 5x8
a=[...
    0.02*rnux(sext,px1,px1,nuz);...
    0.02*rnux(sext,px2,px2,nuz);...
    ones(1,size(sext,1)) ...
    ];
figure(1);
resonplot(a(1:4,:),[px1 px2]);

[u,s,v]=svd(a,0);
lambda=diag(s) %#ok<NOPRT>
keep=1:length(lambda);
ainv=v(:,keep)*diag(1./lambda(keep))*u(:,keep)';
correction=ainv(:,1:4)';			%transpose for C order

i_rms = std(correction') %#ok<UDIM,NOPRT,NASGU>
response=a(1:4,:)';				%transpose for C order
info=ver('matlab');
matvers=sscanf(info.Version,'%d.%d');
if matvers(1) >= 5, fmode=' -v4'; else fmode='';end
eval(['save hdipcor response correction' fmode]);
store_variable('matlog','dip_reson_mode','free');

    function exc=rnux(sext,p,nux,nuz)
        disp(['nuX = ' num2str(p)]);
        phase=2*pi*(nux*sext(:,3)+(p-nux)*sext(:,1));
        ampl=sqrt(sext(:,2));
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

end
