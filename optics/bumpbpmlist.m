function [blist,slist]=bumpbpmlist(bpm,steerer)
nbumps=size(steerer,1);
klist=[[nbumps 1:nbumps-1];1:nbumps;[2:nbumps 1]];
s=bpm(:,1);
blist=cell(nbumps,1);
slist=cell(nbumps,1);
for i=1:nbumps
	s1=steerer(klist(1,i),1);
	s3=steerer(klist(3,i),1);
	if (s1 > s3)
		blist{i}=[find(s>s1);find(s<s3)];
	else
		blist{i}=find((s>s1) & (s<s3));
	end
	slist{i}=klist(:,i);
end
