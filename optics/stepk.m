function [bk2,residu]=stepk(rr,bkm,rmsval)
% iterates on steerer beta values
%	rr:			matrix of cosines
%	bkm:			estimate of sq. root of steerer beta
%	residu:			residual error

if nargin >= 3
   scale=std2(rr)./rmsval;
else
   scale=max2(abs(rr));
end
bk2=bkm.*scale;
residu=std2((bk2-bkm)')
