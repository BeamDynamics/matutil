function harmodisp(h,index)

[row col]=size(h);

figure(1);
colormap(jet);
imagesc(0:col-1,1:row,log(abs(h)+1.e-6));
%imagesc(0:col-1,1:row,abs(h));
axis('xy');
xlabel('harmonic');
ylabel('eigen vector');
colorbar;

%figure(2);
%surfl(0:col-1,1:row,abs(h));
%colormap(gray);

if nargin >= 2
figure(3);
bar((0:col-1)'*ones(1,length(index)),abs(h(index,:))');
ax=axis;
ax(1)=0;
ax(2)=col-1;
axis(ax);
end
