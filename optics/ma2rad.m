function kick=ma2rad(plane,val)

coef=load_corcalib(plane);
[rows,cols]=size(val);
kick=val.*reshape(coef(:)*ones(1,16),rows,cols);
