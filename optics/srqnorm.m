function [dkk,kstd,kmean]=srqnorm(k)
%[dkk,kstd,kmean]=srknorm(k)   computes average strengths and deviations

[nq,ndata]=size(k);
nc=nq/32;
dkk=NaN*ones(size(k));
kmean=dkk;
kstd=dkk;
for i=1:ndata
    kf=srfold(k(:,i));
    if nc == 10
        kf(4:7,:)=0.5*(kf(4:7,:)+kf(7:-1:4,:));
    end
    km=mean(kf,2);
    ks=std(kf,1,2)./abs(km);
    kmean(:,i)=srunfold(km(:,ones(1,32)));
    kstd(:,i)=srunfold(ks(:,ones(1,32)));
end
dkk=(k-kmean)./kmean;
