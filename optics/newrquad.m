function [a,ainv]=newrquad(sext,nux,nuz,code)

px1=ceil(2*nux);
px2=floor(2*nux);
pz1=ceil(2*nuz);
pz2=floor(2*nuz);
nquad=size(sext,1);
if nargin < 4, code='coupled'; end
%				response 9x16
a=[...
    %	0.02*r2nux(sext,px1,nux);...
    %	0.02*r2nux(sext,px2,nux);...
    %	0.02*r2nuz(sext,pz1,nuz);...
    %	0.02*r2nuz(sext,pz2,nuz);...
    0.02*r2nux(sext,px1,0.5*px1);...
    0.02*r2nux(sext,px2,0.5*px2);...
    0.02*r2nuz(sext,pz1,0.5*pz1);...
    0.02*r2nuz(sext,pz2,0.5*pz2);...
    ones(1,nquad) ...
    ];
figure(1);
if strcmp(code,'coupled')
    exc=a(1:8,1:8)-a(1:8,9:16);
    resonplot(exc,[px1 px2 pz1 pz2]);
    rexc=inv(exc);
    ainv=[rexc;-rexc];
elseif strcmp(code,'coupledbz2.5')
    exc=a(1:8,1:8)-a(1:8,9:16);
    resonplot(exc,[px1 px2 pz1 pz2]);
    keep=[1 2 5 6];			% keep only half-integer resonances
    %  keep=[1 2 3 4 5 6];			% discard 2 nuz = 28
    rexc=zeros(8,8);
    rexc(keep,keep)=inv(exc(keep,keep));
    ainv=[rexc;-rexc];
else
    resonplot(a(1:8,:),[px1 px2 pz1 pz2]);
    [u,s,v]=svd(a,0);
    lambda=diag(s) %#ok<NOPRT>
    keep=1:length(lambda);
    ainv=v(:,keep)*diag(1./lambda(keep))*u(:,keep)';
end
response=[a(1:8,:);ones(1,nquad)]';	%transpose for C order
correction=[ainv(:,1:8) zeros(nquad,1)]';	%transpose for C order

i_rms = std(correction') %#ok<UDIM,NOPRT,NASGU>
info=ver('matlab');
matvers=sscanf(info.Version,'%d.%d');
if matvers(1) >= 5, fmode=' -v4'; else fmode='';end
if nquad==32
    fname='quad32cor';
else
    fname='quadcor';
    store_variable('matlog','quad_reson_mode',code);
end
eval(['save ' fname ' response correction' fmode]);

    function exc=r2nux(sext,p,nux)
        disp(['2*nuX = ' num2str(p)]);
        phase=2*pi*(2*nux*sext(:,3)+(p-2*nux)*sext(:,1));
        exc=[sext(:,2).*cos(phase) sext(:,2).*sin(phase)]';
    end

    function exc=r2nuz(sext,p,nuz)
        disp(['2*nuZ = ' num2str(p)]);
        phase=2*pi*(2*nuz*sext(:,6)+(p-2*nuz)*sext(:,1));
        exc=[sext(:,5).*cos(phase) sext(:,5).*sin(phase)]';
    end

end
