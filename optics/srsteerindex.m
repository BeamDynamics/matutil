function idx=srsteerindex(steername,varargin)
%SRSTEERINDEX	find index of Steerer with name steername
%
%IDX=SRSTEERINDEX(STEERNAME)	STEERNAME may be a string, a char array or
%				a cell array of strings
%IDX=SRSTEERINDEX(STEERNAME,FORMAT) Looks for steerer names matching FORMAT
%				Default FORMAT: 'SR/ST-%*[HVS]%i/C%i'
%
%   See also STEERNAME

idx=sr.steerindex(steername,varargin{:});
end
