function varargout=srsteername(idx,varargin)
%SRSTEERNAME	returns the name of Steerer with index idx
%
%Deprecated: see SR.STEERNAME
%STEERNAME=SRSTEERNAME(IDX)		returns the name of steerer with index IDX
%                               IDX in [1 96]
%STEERNAME=SRSTEERNAME(IDX,FORMAT)	uses FORMAT to generate the name
%					(default : 'S%i/C%i')
%
%[NM1,NM2...]=SRSTEERNAME(...)		May be used to generate several names
%
%
%   See also SRSTEERINDEX

[varargout{1:max(1,nargout)}]=sr.steername(idx,varargin{:});
end
