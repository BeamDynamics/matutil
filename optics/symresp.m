function b=symresp(resp,periods,knoper)
%SYMRESP		Build response matrix of symmetrical booster
%
%SRESP=SYMRESP(RESP,PERIODS,KNOPER)

%SY H PERIODS=3, knoper=[12,13]
%SY V PERIODS=3, knoper=[13,26]
%
if size(resp,2) <= 1
   a=reshape(syexpand(resp),[],3);
   a=repmat(mean2(a,2),3,1);
else
%   mirror=reshape(flipud(resp(:)),nbp,periods*nk);
%   mirror(nomirror,:)=NaN;
%   a=reshape(mean2([resp(:) mirror(:)],2),nb,nk);
   a=syexpand(resp);	% expand response
   a(25,:)=NaN;		% eliminate BPM
   a(:,knoper)=NaN;	% eliminate steerers
   [nb,nk]=size(a);
   nkp=nk/periods;
   nbp=nb/periods;

   c=zeros(nb,nk,periods);
   for i=1:periods				% periodicity
      c(:,:,i)=a;
      a=circshift(a,[-nbp,-nkp]);
   end
   b=zeros(nb,nk);
   for i=1:nk
      b(:,i)=mean2(squeeze(c(:,i,:)),2);
   end
end
