function a=srunfold(r,varargin)
%A=SRUNFOLD(B)			reshapes data for SR symmetry
%
%	R(n,32) -> A(32*n,1)

a=sr.unfold(r,varargin{:});
end
