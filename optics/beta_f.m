% leaves in the workspace:
%	r:			response matrix
%	steer_num, steer_cell:	list of steerer names
%	bpm_num, bpm_cell:	list of bpm names
%	plane:			'h' or 'v'
%	c2pinu:			normalization constant
%	sb:			bpm location
%	pb0:			bpm phase
%	sk:			steerer location
%	pk0:			steerer phase

%	rr:			matrix of cosines
%	residu:			residual error
%	bbm:			estimate of sq. root of BPM beta
%	bkm:			estimate of sq. root of steerer beta
%	bok:			"good" bpms for fit (large beta)
periods=16;
filename=input('.rsp file: ','s');
plane=input('plane (h/v): ','s');
tune=input([plane ' tune: ']);

if plane == 'h'
index=2;
blist=[1;2;4;6;9;11;13;14];
else
index=5;
blist=[2;3;5;6;9;10;12;13];		%order of bpms in array
end;

coef=load_corcalib('sr',plane);

[bpm,sext,betanux,betanuz,betaperiods,alpha]=load_betalog('sr');

ns=size(sext,1);
nb=size(bpm,1);
copyr=ones(1,periods);

cell_list=(0:31)+4;
cell_list=cell_list-32*(cell_list>32);
bpm_num=[1:7 1:7]';
bpm_num=reshape(bpm_num*copyr,nb,1);
steer_num=[4;6;13;19;20;22;24;24;22;20;19;13;6;4];
steer_num=reshape(steer_num*copyr,nb,1);
bpm_cell=reshape(ones(nb/32,1)*cell_list,nb,1);
steer_cell=reshape(ones(ns/32,1)*cell_list,ns,1);

%r=load_rsp(plane,[],filename);
r=load_rsp2(plane,filename);
if plane == 'h'
if input('remove off-momentum (y/n): ','s') ~= 'n'
   disp('remove off-momentum');
   rfreq=-bpm(:,4)/352.2e6/alpha;	% theoretical displacement for 1 Hz
%  rfreq=bpm(:,4);			% theoretical displacement for 1 %
   r=defreq(r,rfreq);
   clear rfreq
end
end

%disp('compute harmonic analysis...');
%hmes=harmana(0:40,bpm(:,2:3),r);
%harmodisp(hmes);
%clear hmes

if size(r,2) == 96
   slist=[2 4 6 9 11 13];
elseif size(r,2) == 64
   slist=[2 6 9 13];
else
   slist=[];
end

sok=reshape(1:ns,ns/periods,periods);
sok=sok(slist,:);
nkcell=length(slist);
bok=reshape(1:nb,nb/periods,periods);
bok=bok(blist,:);
nbcell=nb/periods;

clear cell_list slist

sb=bpm(:,1);
bb0=bpm(:,index);
pb0=bpm(:,index+1);
bbm=sqrt(bb0);
bkm=sqrt(sext(sok,index)');
sk=sext(sok,1)';
bk0=sext(sok,index)';
pk0=sext(sok,index+1)';
clear bpm sext sok index
coef=reshape(coef(:)*ones(1,periods),1,6*periods);
c2pinu=2*sin(pi*tune)./coef(ones(224,1),:);
rr=(r./(bbm*bkm)).*c2pinu;

clear coef

while 1
   for j=1:5
      [bbm residu]=stepb(rr,bbm);
      rr=(r./(bbm*bkm)).*c2pinu;
      [bkm residu]=stepk(rr,bkm);
      rr=(r./(bbm*bkm)).*c2pinu;
%     if residu < 2.0e-4, break, end
   end
   rep=input('stop ? (y/n)','s');
   if ~strcmp(rep,'n'), break, end
end

%if exist('FLAG')
cosb=mean2(reshape(std2(rr(:,:)'),size(rr,1)/periods,periods)');
cosb=mean2([cosb;cosb([14 13 12 4 10 9 8 7 6 5 11 3 2 1])]);
scaleb=cosb'*copyr;
cosk=mean2(reshape(std2(rr(bok,:)),size(rr,2)/periods,periods)');
cosk=mean2([cosk;cosk(length(cosk):-1:1)]);

scalek=cosk'*copyr;
while 1
   for j=1:10
      [bbm residu]=stepb(rr,bbm,scaleb(:));
      rr=(r./(bbm*bkm)).*c2pinu;
      [bkm residu]=stepk(rr(bok,:),bkm,scalek(:)');
      rr=(r./(bbm*bkm)).*c2pinu;
      if residu < 2.0e-4, break, end
   end
   rep=input('stop ? (y/n)','s');
   if ~strcmp(rep,'n'), break, end
end
clear scaleb scalek
%end

[bb,bk]=betanorm(bbm,bkm,bb0);


averb=mean2(bb');			% compute average and modulation
averb=mean2([averb;averb([14 13 12 4 10 9 8 7 6 5 11 3 2 1])]);
dd=averb'*copyr;
modulb=(bb(:)-dd(:))./dd(:);
bpm_modulation=std2(modulb);

bad_bpm=find(abs(modulb) > (2.5*bpm_modulation));
good_bpm=1:nb;
good_bpm(bad_bpm)=[];
if (~isempty(bad_bpm))
   disp('rejected bpms (too high modulation):');
   disp([bad_bpm bpm_cell(bad_bpm) bpm_num(bad_bpm)]');

   dd=bb;
   dd(bad_bpm)=NaN*ones(length(bad_bpm),1);
   averb=mean2(dd');			% recompute average and modulation
   averb=mean2([averb;averb([14 13 12 4 10 9 8 7 6 5 11 3 2 1])]);
   dd=averb'*copyr;
   modulb=(bb(:)-dd(:))./dd(:);
   bpm_modulation=std2(modulb(good_bpm));
end

%averk=mean2(srfold(bk(:))');
%dd=[averk averk(length(averk):-1:1)]'*copyr;
averk=mean2(bk');
averk=mean2([averk;averk([6 5 4 3 2 1])]);
dd=averk'*copyr;
modulk=(bk(:)-dd(:))./dd(:);
clear dd

bpm_modulation


if size(r,2) == 96
   check=[reshape(bb([4 11],:),2*periods,1) reshape(bk([2 5],:),2*periods,1)];
   precision=std2(check(:,2)-check(:,1))/mean2(check(:,1))
else
   check=reshape(bb([4 11],:),2*periods,1);
end

figure(1)
clf
plot(check);
lg=legend('BPM','Steerer');
title('comparison BPM/steerer');

[pb pbave]=bphase(rr,sb,sk,tune,pb0,pk0);
[pk pkave]=bphase(rr',sk',sb',tune,pk0',pb0');

bx=1:nb/16;
kx=1:size(rr,2)/16;
axes(lg);

figure(2);

subplot(2,1,1);
plot(sb(1:nbcell),(averb')./bb0(1:nbcell),sk(1:nkcell)',(averk./bk0(1:nkcell))');
%plot(sb(1:nbcell),(averb')./bb0(1:nbcell));
title('average measured beta/model');

subplot(2,1,2);
plot(sb(bx),pbave(bx)-pb0(bx),sk(kx)',pkave(kx)-pk0(kx)');
legend('BPM','Steerer');
title('average phase advance - model');

figure(3);
subplot(2,1,1);
plot(sb(good_bpm),modulb(good_bpm),sk,modulk);
hold on
plot(sb(bad_bpm),modulb(bad_bpm),'ro');
hold off
title('beta / average beta');

subplot(2,1,2);
%plot(sb,pb-pbave,sk,pk-pkave);
plot(sb,pb-pb0,sk',pk-pk0');
legend('BPM','Steerer');
title('phase advance - model');

clear periods nb ns blist copyr
