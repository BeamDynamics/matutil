function [ave,ave7m]=srqpaverage(qpdata,varargin)
%SRBPMAVERAGE Averages over quadrupoles after eliminating non-standard ones

qp7m=[149:152 156:-1:153];   % ID23

unfold=false;
mfunc=@mean2;
for i=1:length(varargin)
    if ischar(varargin{i})&&strcmpi(varargin{1},'unfold'), unfold=true; end
    if isa(varargin{i},'function_handle'), mfunc=varargin{i}; end
end
sz=size(qpdata);
v=reshape(qpdata,sz(1),[]);
[n1,n3]=size(v);
v7m=v(qp7m,:);
v(qp7m,:)=NaN;
vave=mfunc(srfold(v),2);
v7mave=mfunc(reshape(v7m,[],2,n3),2);
if unfold
    v=srunfold(repmat(vave,[1 32 1]));
    v(qp7m,:)=reshape(repmat(v7mave,[1 2 1]),[],n3);
    ave=reshape(v,sz);
else
    ave=reshape(squeeze(vave),[sz(1)/32 sz(2:end)]);
    ave7m=reshape(squeeze(v7mave),[4 sz(2:end)]);
end
end

