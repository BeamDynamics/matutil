function [idx,kdx]=srbpmindex(varargin)
%SRBPMINDEX	find index of BPM from its name
%
%IDX=SRBPMINDEX(BPMBAME)
%   BPMNAME:    string, char array or cell array of strings
%
%IDX=SRBPMINDEX(KDX)
%   KDX:        "hardware" index of the BPM
%
%IDX=SRBPMINDEX(CELL,ID)
%   CELL:   cell number in [1 32]
%   ID:     index in cell, in [1 7]
%   CELL and ID must have the same dimensions or be a scalar
%
%[IDX,KDX]=SRBPMINDEX(...)	returns in addition the "hardware" index
%
%   See also SR.BPMNAME, SR.QPINDEX, SR.SXINDEX

[idx,kdx]=sr.bpmindex(varargin{:});
end
