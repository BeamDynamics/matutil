function [cor,ccc]=meffsolve(y,resp)
%cor=meffsolve(y,resp) computes most effective corrector
%	y: orbit(s) to correct
%    resp: response matrix

ok=find(all(finite([resp y]')));
ccc=y(ok)'*resp(ok,:)./sum(resp(ok,:).*resp(ok,:));
chi2=std2(y(:,ones(1,size(resp,2)))-resp.*ccc(ones(size(resp,1),1),:))
[b,ix]=sort(chi2);
chi2(ix)
ix(1)
cor=zeros(size(resp,2),1);
cor(ix(1))=ccc(ix(1));
