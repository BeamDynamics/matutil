function info=svdprobe(info,varargin)
%svdprobe(svdinfo,plane,idb,ids)		plots contents of SVD analysis
%
%	svdinfo: structure returned by svdcomp
%	plane:	 'h' or 'v' [default = 'h']
%	idb:	 Orbit Eigen vector to plot [default = 1]
%	ids:	 Correction Eigen vector tp plot [default = idb]
%
%svdprobe(plane,idb,ids) takes data from the file '[hv]_svd.mat'
%
%	See also svdcomp, svdsol

params={'h',1,[]};
if isstruct(info)
   params(1:nargin-1)=varargin;
else
   params(1:nargin)={info,varargin{:}};
   info=load_svdinfo(info);
end
[plane,idb,ids]=deal(params{:});

path=pwd;
maxharm=40;
if isempty(ids), ids=idb; end
[pm,bpm,steerer]=load_optics(path,[plane 'bpm'],['steer' plane]);
neig=length(info.lambda);
disp([int2str(neig) ' Eigen vectors.']);
%svdbase=svdinfo.svdbase;				% normalized orbits
bharm=harmana(0:maxharm,bpm(:,2:3),info.base*diag(info.lambda));
sharm=harmana(0:maxharm,steerer(:,2:3),info.v);
harmodisp(bharm);	%figure(1)

figure(2);
semilogy(info.lambda);
grid on

figure(3);
subplot(2,1,1);
bar((0:maxharm)',abs(bharm(idb,:))');
ax=axis;
ax(1)=0;
ax(2)=maxharm;
axis(ax);
subplot(2,1,2);
plot(bpm(:,1),info.base(1:size(bpm,1),idb));
ax=axis;
ax(1)=0;
ax(2)=1;
axis(ax);

figure(4)
subplot(2,1,1);
bar((0:maxharm)',abs(sharm(ids,:))');
ax=axis;
ax(1)=0;
ax(2)=maxharm;
axis(ax);
subplot(2,1,2);
plot(steerer(:,1),info.v(1:size(steerer,1),ids));
ax=axis;
ax(1)=0;
ax(2)=1;
axis(ax);

function info=load_svdinfo(plane)
vls=load([plane '_svd']);
if isfield(vls,'lambda')
   info.lambda=vls.lambda;
else
   info.lambda=1./sqrt(sum(vls.svdsolve.*vls.svdsolve)');
end
info.base=vls.svdbase;					% normalized orbits
info.v=vls.svdsolve*diag(info.lambda);	% normalized steerers
