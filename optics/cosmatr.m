function b=cosmatr(bpm,st)

b=0.5 - abs(bpm*ones(size(st)) - ones(size(bpm))*st);
