function rbump_id(s, sh, sv, id, mode)
%RBUMP_ID(S,SH,SV,ID,MODE)	Measure BPM offset
%
%S,SH,SV : structures returned by rbump_init
%ID : ID number
%MODE :     measurement mode
%		'check' : computes deviation without moving the beam
%		'fit' : moves +/- one step and takes least square
%		'calibrate' : start with two points to determine a first estimate, then fit
%
%See also: RBUMP_INIT, RBUMP_MEAS

global APPHOME

if nargin < 5, mode='check'; end
id2=id-4;
if id2 < 0, id2=id2+32; end
bpmidx=7*id2+[-1 0 1 2];
shft=(bpmidx <= 0);
bpmidx(shft)=bpmidx(shft)+224;

bpms=s.bpm(bpmidx);
bpmslist=cat(2,bpms.id);

bpmh=cat(2,bpms([1 4]).h);
bpmv=cat(2,bpms([1 4]).v); %#ok<NASGU>
theoresp=[responsem(s.betaphi(:,2:3),cat(1,bpmh.qp),s.params.nuh) ...
    responsem(s.betaphi(:,5:6),cat(1,bpmh.qp),s.params.nuv)];
bpms(1).shuntdev=tango.Device(bpms(1).shuntname);
bpms(4).shuntdev=tango.Device(bpms(4).shuntname);
diary(fullfile(APPHOME,'shuntfiles',['out_' datestr(now,29)]));
fprintf('\nID %3d',id);
diary('off');

if strcmp(mode,'check')
    [vhv,orbit]=evalfunc('Initial',s,bpms,theoresp); %#ok<ASGLU>
    diary('on');disp(1000*orbit(bpmslist,:));diary('off');
else
    step = [sh.step sv.step sh.step sv.step];
    rbump_plot(1,'bumps',rbump_bump(sh.resp,sv.resp,bpms(1),step(1:2)),bpms(1).id);
    hold on
    rbump_plot(1,'bumps',rbump_bump(sh.resp,sv.resp,bpms(4),step(3:4)),bpms(4).id);
    hold off
    try
        estimate = [0.0 0.0 0.0 0.0];
        vhv=NaN(6,16);

        pt3=[0 0 -1;-1 0 -1;1 0 -1;0 -1 -1;0 1 -1];
        ampl(1:5,:)=set_amplitudes(estimate,pt3(:,[1 1 2 2]).*repmat(step,5,1));
            amp0(1:2,:)=set_amplitudes(estimate,[-1.1 -1.1 0 0;-0.1 -0.1 -1.1 -1.1].*repmat(step,2,1));
            setbump(sh, cat(2,bpms([1 4]).h), amp0(1,[1 3]));
            setbump(sv, cat(2,bpms([1 4]).v), amp0(1,[2 4]));
            pause(1);
        [vhv(2,:),orbit1]=evalfunc('Up-step',s,bpms,theoresp,sh,sv,ampl(2,:));
        vhv(3,:)=evalfunc('Up+step',s,bpms,theoresp,sh,sv,ampl(3,:));
            setbump(sh, cat(2,bpms([1 4]).h), amp0(2,[1 3]));
            setbump(sv, cat(2,bpms([1 4]).v), amp0(2,[2 4]));
            pause(1);
        [vhv(4,:),orbit2]=evalfunc('Down-step',s,bpms,theoresp,sh,sv,ampl(4,:));
        [vhv(1,:),orbit0]=evalfunc('Initial',s,bpms,theoresp,sh,sv,ampl(1,:));
        vhv(5,:)=evalfunc('Down+step',s,bpms,theoresp,sh,sv,ampl(5,:));
        rbump_plot(2,'bumps',orbit0-orbit1,bpms(1).id);
        hold on
        rbump_plot(2,'bumps',orbit0-orbit2,bpms(4).id);
        hold off
        AB=(pt3\vhv(1:5,9:12));		% Using fit
        best=reshape([AB(3,[1 3])/AB(1:2,[1 3]);AB(3,[2 4])/AB(1:2,[2 4])],1,4);
        estimate = estimate + step.*best;
        
        ampl(6,:)=set_amplitudes(estimate, 0*step);
            amp0(3,:)=set_amplitudes(estimate, -0.1*step);
            setbump(sh, cat(2,bpms([1 4]).h), amp0(3,[1 3]));
            setbump(sv, cat(2,bpms([1 4]).v), amp0(3,[2 4]));
            pause(1);
        [vhv(6,:),orbit]=evalfunc('Control',s,bpms,theoresp,sh,sv,ampl(6,:));
        diary('on');disp(1000*orbit(bpmslist,:));diary('off');
        
        vhv(1:6,9:12)
        rbump_plot(3,'fit',ampl([6 6 2 1 3 6],1:2),vhv([6 6 2 1 3 6],9:10));
        rbump_plot(4,'fit',ampl([6 6 4 1 5 6],3:4),vhv([6 6 4 1 5 6],11:12));
        
        setbump(sh, [], []);
        setbump(sv, [], []);
        bpms(1).shuntdev=0;
        bpms(4).shuntdev=0; %#ok<NASGU>
    catch error
        disp('-Error: setting bumps to zero');
        setbump(sh, [], []);
        setbump(sv, [], []);
        bpms(1).shuntdev=0;
        bpms(4).shuntdev=0; %#ok<NASGU>
        rethrow(error);
    end
end

    function [v,orbit]=evalfunc(mess,s,bpms,theoresp,sh,sv,ampl)
        if nargin >= 7
            setbump(sh, cat(2,bpms([1 4]).h), ampl([1 3]));
            setbump(sv, cat(2,bpms([1 4]).v), ampl([2 4]));
        else
            ampl=[0.0 0.0 0.0 0.0];
        end
        [v1,orb1]=rbump_eval(s,bpms(1),theoresp(:,1:2));
        [v2,orb2]=rbump_eval(s,bpms(4),theoresp(:,3:4));
        orbit=0.5*(orb1+orb2);
        v=[reshape(orbit([bpms.id],:)',1,8) v1(5:6) v2(5:6) v1(7:8) v2(7:8)];
        rbump_disp(mess, [ampl(1:2) v1]);
        rbump_disp(mess, [ampl(3:4) v2]);
    end

    function setbump(steerer,bpms,ampl)
        steerval=steerer.inisteerval;
        for bp=1:length(bpms)
            steerval(bpms(bp).bump)=steerval(bpms(bp).bump)+bpms(bp).kicks'*ampl(bp);
        end
        steerer.steerdev.Current=steerval(1:96);
        if isfield(steerer,'freqdev')
            %steerer.freqdev.Frequency=steerer.deltaf*steerval(97);	% DANGEROUS
        end
    end

    function ampls=set_amplitudes(estimate, step)
        active=true(size(estimate));
        active(~(abs(estimate) < 0.0005)) = false;
        ampls=repmat(estimate,size(step,1),1)+step;
        ampls(:,~active)=0;
    end

end
