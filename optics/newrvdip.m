function [a,ainv]=newrvdip(sext,nux,nuz)

pz1=floor(nuz);
pz2=ceil(nuz);
ncor2=size(sext,1)/2;
%				response 5x8
a=[...
    0.02*rnuz(sext,pz1,nux,pz1);...
    0.02*rnuz(sext,pz2,nux,pz2);...
    ones(1,size(sext,1)) ...
    ];
figure(1);
resonplot(a(1:4,:),[pz1 pz2]);

[u,s,v]=svd(a,0);
lambda=diag(s) %#ok<NOPRT>
keep=1:length(lambda);
ainv=v(:,keep)*diag(1./lambda(keep))*u(:,keep)';
correction=ainv(:,1:4)';			%transpose for C order

i_rms = std(correction') %#ok<UDIM,NOPRT,NASGU>
response=a(1:4,:)';				%transpose for C order
info=ver('matlab');
matvers=sscanf(info.Version,'%d.%d');
if matvers(1) >= 5, fmode=' -v4'; else fmode='';end
eval(['save vdipcor response correction' fmode]);
store_variable('matlog','dip_reson_mode','free;');

    function exc=rnuz(sext,p,nux,nuz)
        disp(['nuZ = ' num2str(p)]);
        phase=2*pi*(nuz*sext(:,6)+(p-nuz)*sext(:,1));
        ampl=sqrt(sext(:,5));
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

end
