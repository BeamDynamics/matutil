function emgr=emit_growth(x,xp,beta,emit0)
%EM=EMIT_GROWTH(X,XP,BETA,EMIT0)

epsco=x.*x./beta + xp.*xp.*beta;
emgr=2.0*sqrt(epsco/emit0);
