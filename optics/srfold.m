function r=srfold(a,varargin)
%A=SRFOLD(B)			reshapes data for SR symmetry
%
%	A(32*n,1) -> R(n,32)

r=sr.fold(a,varargin{:});
end
