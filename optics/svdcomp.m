function svdinfo=svdcomp(a)
%svdinfo=svdcomp(response) computes SVD correction
%	The correction is cor = - v * diag(1./lambda) * base' * y
%	or 		cor = svdsol(y,svdinfo,neigen)
%
%svdinfo is a structure used by svdsol, with fields:
%   lambda :Eigen values
%   base   :orbit base, eigen components are base' * y
%   v      :correction base
%
%   See also: svdsol
%
%   These functions supersede corsvd2/gosvd

ok=find(all(isfinite(a')));
[u,s,v]=svd(a(ok,:),0);
svdinfo.lambda=diag(s);
nmax=length(svdinfo.lambda);
svdinfo.base=NaN(size(a,1),nmax);
svdinfo.base(ok,:)=u(:,1:nmax);
svdinfo.v=v;
