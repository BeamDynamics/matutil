classdef sratmodel < sr.model
    %SRATMODEL Deprecated: Use sr.model instead
    methods
        function this=sratmodel(varargin)
            warning('obsolete:model','%s is deprecated: Use sr.model instead',mfilename);
            this@sr.model(varargin{:});
        end
    end
end
