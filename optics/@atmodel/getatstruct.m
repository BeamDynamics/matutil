function at = getatstruct(machid,location,varargin)
%GETATSTRUCT	% Returns an AT structure from model, AT struct or directory
%
%AT=GETATSTRUCT(MACHID,LOCATION,VARARGIN)
%
%MACHID:    'sr', 'sy' or 'ebs'
%LOCATION:  directory, AT structure or atmodel object
%VARARGIN:  Arguments transmitted to the 'checklattice' function
%
%AT:        Resulting AT structure
%
%GETATSTRUCT(...,'energy',energy)
%   Set the ring energy
%
%GETATSTRUCT(...,'reduce',true,'keep',pattern)
%   Remove elements with PassMethod='IdentityPass' and merge adjacent
%   similar elements, but keeps elements with FamName matching "pattern"
%   Pattern may be a logical mask. (Default: 'BPM.*|ID.*').
%
%GETATSTRUCT(...,'remove',famnames)
%   remove elements identified by the family names (cell array)
%
%GETATSTRUCT(...,'MaxOrder',n)
%   Limit the order of polynomial field expansion to n at maximum.
%   Default 999
%
%GETATSTRUCT(...,'NumIntSteps',m)
%   Set the NumIntSteps integration parameter to at least m.
%   Default 20
%
%GETATSTRUCT(...,'QuadFields',quadfields)
%   When reading a BETA file, set quadrupoles fields to the specified ones.
%   Default: {'FringeQuadEntrance',1,'FringeQuadExit',1}
%
%GETATSTRUCT(...,'BendFields',bendfields)
%   When reading a BETA file, set bending magnet fields to the specified ones.
%   Default: {'FringeQuadEntrance',1,'FringeQuadExit',1}
%
%GETATSTRUCT(...,'hardangle',value)         (SR only)
%GETATSTRUCT(...,'softangle',value)
%   Set the angle of bending magnet sources
%
%GETATSTRUCT(...,'PinholeID',cellnbs)       (EBS only)
%   Select the cells where ID pinhole cameras are located. Default [7 25]
%
%GETATSTRUCT(...,'PinholeBM',cellnbs)       (EBS only)
%   Select the cells where BM pinhole cameras are located. Default [17 27 1]
%
%GETATSTRUCT(...,'DeviceNames',true)        (EBS only)
%   add device names in AT structure.

dirfunc=str2func([machid '.model.getdirectory']);
checkfunc=str2func([machid '.model.checklattice']);

[quadfields,args]=getoption(varargin,'QuadFields',{'FringeQuadEntrance',1,'FringeQuadExit',1});
[bendfields,args]=getoption(args,'BendFields',{'FringeQuadEntrance',1,'FringeQuadExit',1});

if isa(location, [machid '.model'])
    fprintf('initializing the optics using model object\n')
    at=checkfunc(location.ring,args{:});
elseif iscell(location)	% AT structure
    fprintf('initializing the optics using AT structure\n')
    at=checkfunc(location,args{:});
elseif isdir(location)	% optics directory
    at=load_at(location,checkfunc,args{:});
else                    % optics name
    at=load_at(dirfunc(location),checkfunc,args{:});
end

    function at=load_at(path,checkfunc,varargin)
        try
            matfile=fullfile(path,'betamodel.mat');
            a=load(matfile);
            at=checkpass(a.betamodel,'QuadMPoleFringePass','StrMPoleSymplectic4Pass');
            at=checkpass(at,'BndMPoleSymplectic4FrgFPass','BndMPoleSymplectic4Pass');
            at=checkfunc(at,varargin{:});
            fprintf('initializing the optics using %s\n',matfile);
        catch err
            if ~strcmp(err.identifier,'MATLAB:load:couldNotReadFile'), err.rethrow(); end
            disp(err.message);
            betafile=fullfile(path,'betamodel.str');
            at=checkfunc(atreadbeta(betafile),varargin{:},...
                'QuadFields',quadfields,'BendFields',bendfields);
            fprintf('initializing the optics using %s\n',betafile);
        end
    end

    function ring=checkpass(ring,oldpass,newpass)
        sel=atgetcells(ring,'PassMethod',oldpass);
        if sum(sel) > 0
            fprintf('Replacing "%s" with "%s"\n',oldpass,newpass);
            v=atsetfieldvalues(ring(sel),'PassMethod',newpass);
            v=atsetfieldvalues(v,'FringeQuadEntrance',1);
            ring(sel)=atsetfieldvalues(v,'FringeQuadExit',1);
        end
    end
end

