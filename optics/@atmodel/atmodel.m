classdef atmodel < handle
    %Class providing access to optical values
    %Used as a base class for sr.model, sy.mode, ebs.model
    %
    %Available locations:
    %
    %bpm:   All BPMs
    %qp:    All quadrupoles
    %sx:    All sextpoles
    %oc:    All octupoles
    %steerh:    Horizontal steerers
    %steerv:    Vertical steerers
    %
    %Available parameters:
    %
    %0:     index in the AT structure
    %1:     Integrated strength
    %2:     theta/2pi
    %3:     betax
    %4:     alphax
    %5:     phix/2pi/nux
    %6:     etax
    %7:     eta'x
    %8:     betaz
    %9:     alphaz
    %10:    phiz/2pi/nux
    %11:    etaz
    %12:    eta'z
    %13:    s position (entrance)
    %
    %Default parameters:
    %
    %[   2         3         5        6      8       10      ]
    %[theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux]
    
    properties (Constant, Access=protected, Hidden)
        fileorder=[2 3 8 4 9 5 10 6 7 11 12 1 0 13]
        locs=atmodel.getlocs()      % Parameter indexing
        prms=[2 3 5 6 8 10]         % Default parameters
    end
    
    properties (Access=protected, Hidden)
        idx     % Cache of element indices
        inidata % Optical values at entrance
        vlave   % Average optical values
        modified=true% indicate that recompute is needed
        ll_
        alpha_
        tunes_
        emith_=[]
        emitv_
        espread_
        blength_
    end
    
    properties (SetAccess=private)
        ring    % AT structure
    end
    
    properties (Dependent, SetAccess=private)
        ll      % Circumference
        alpha   % Momentum compaction factor
        nuh     % Horizontal tune
        nuv     % Vertical tune
        emith   % Horizontal emittance
        emitv   % Vertical emittance
        energy_spread
        bunch_length
    end
    properties (Dependent)
        tunes   % Tunes
    end
    
    properties
        strname % Machine identifier
        periods % Number of periods
    end
    
    methods (Static, Access=private, Hidden)
        function locs=getlocs()
            locs(atmodel.fileorder+1)=1:length(atmodel.fileorder);
        end
    end
    
    methods (Static, Access=protected, Hidden)
        function idx=mcellid(idx,cell,id)
            sel=reshape(false(sum(idx),1),32,[]);
            sel(cell,id)=true;
            idx(idx)=circshift(sel,-3);
        end
        
    end
    
    methods (Static)
        function pth=getdirectory(varargin)
            pth='';
        end
        
        at = getatstruct(machid,location,varargin)
        
        function [machid,location,args]=getpath(varargin)
            %GETPATH	% Get the full path of an optics directory
            %
            %[MACHID,LOCATION,ARGS]=GETMODELPATH(VARARGIN) scans the input arguments looking for:
            %
            %- 'sr'|'sy'|'ebs'
            %           machine name, path defaults to $(APPHOME)/mach/optics/settings/theory
            %- ['sr'|'sy'|'ebs',] 'opticsname'
            %           machine and optics name
            %- ['sr'|'sy'|'ebs',] '/machfs/appdata/sr/optics/settings/opticsname':
            %           full path of optics directory
            %- ['sr'|'sy'|'ebs',] atstruct:
            %           AT structure
            %- ['sr'|'sy'|'ebs',] atmodel:
            %           atmodel object
            %
            %MACHID:	'sr, 'sy' or 'ebs'
            %LOCATION:	selected directory, optics name or AT structure
            %ARGS:      remaining arguments
            
            if ischar(varargin{1}) && any(strcmpi(varargin{1},{'sr','sy','ebs'}))
                machid=lower(varargin{1});
                args=varargin(2:end);
            else
                machid='sr';
                args=varargin;
            end
            iarg=mod(length(args),2);
            location=getargs(args(1:iarg),{'theory'});
            args=args(iarg+1:end);
        end
        
        function at = getat(varargin)
            %GETAT	% Return an AT structure
            %
            %ATSTRUCT=GETAT(VARARGIN) scans the input arguments looking for:
            %
            %- 'sr'|'sy'|'ebs'
            %           machine name, path defaults to $(APPHOME)/mach/optics/settings/theory
            %- ['sr'|'sy'|'ebs',] 'opticsname'
            %           machine and optics name
            %- ['sr'|'sy'|'ebs',] '/machfs/appdata/sr/optics/settings/opticsname':
            %           full path of optics directory
            %- ['sr'|'sy'|'ebs',] atstruct:
            %           AT structure
            %- ['sr'|'sy'|'ebs',] atmodel:
            %           atmodel object
            %
            %
            %AT:        Resulting AT structure
            %
            %GETAT(...,'energy',energy)
            %   Set the ring energy
            %
            %GETAT(...,'reduce',true,'keep',pattern)
            %   Remove elements with PassMethod='IdentityPass' and merge adjacent
            %   similar elements, but keeps elements with FamName matching "pattern"
            %   Pattern may be a logical mask. (Default: 'BPM.*|ID.*').
            %
            %GETAT(...,'remove',famnames)
            %   remove elements identified by the family names (cell array)
            %
            %GETAT(...,'MaxOrder',n)
            %   Limit the order of polynomial field expansion to n at maximum.
            %   Default 999
            %
            %GETAT(...,'NumIntSteps',m)
            %   Set the NumIntSteps integration parameter to at least m.
            %   Default 20
            %
            %GETAT(...,'QuadFields',quadfields)
            %   When reading a BETA file, set quadrupoles fields to the specified ones.
            %   Default: {}
            %
            %GETAT(...,'BendFields',bendfields)
            %   When reading a BETA file, set bending magnet fields to the specified ones.
            %   Default: {}
            %
            %GETAT(...,'hardangle',value)       (SR only)
            %GETAT(...,'softangle',value)
            %   Set the angle of bending magnet sources
            %
            %GETAT(...,'PinholeID',cellnbs)     (EBS only)
            %   Select the cells where ID pinhole cameras are located. Default [7 25]
            %
            %GETAT(...,'PinholeBM',cellnbs)     (EBS only)
            %   Select the cells where BM pinhole cameras are located. Default [17 27 1]
            %
            %GETAT(...,'DeviceNames',true)      (EBS only)
            %   add device names in AT structure.
            
            [machid,location,args]=atmodel.getpath(varargin{:});
            at=atmodel.getatstruct(machid,location,args{:});
        end
        function mdl=getmodel(varargin)
            %GET	% Return an AT model object
            %
            %MODEL=GET(VARARGIN) scans the input arguments looking for:
            %
            %- 'sr'|'sy'|'ebs'
            %           machine name, path defaults to $(APPHOME)/mach/optics/settings/theory
            %- ['sr'|'sy'|'ebs',] 'opticsname'
            %           machine and optics name
            %- ['sr'|'sy'|'ebs',] '/machfs/appdata/sr/optics/settings/opticsname':
            %           full path of optics directory
            %- ['sr'|'sy'|'ebs',] atstruct:
            %           AT structure
            %- ['sr'|'sy'|'ebs',] atmodel:
            %           atmodel object
            %
            %MODEL:	AT model object (subclass of atmodel)
            %
            %GETMODEL(...,'energy',energy)
            %   Set the ring energy
            %
            %GETMODEL(...,'reduce',true,'keep',pattern)
            %   Remove elements with PassMethod='IdentityPass' and merge adjacent
            %   similar elements, but keeps elements with FamName matching "pattern"
            %   Pattern may be a logical mask. (Default: 'BPM.*|ID.*').
            %
            %GETMODEL(...,'remove',famnames)
            %   remove elements identified by the family names (cell array)
            %
            %GETMODEL(...,'MaxOrder',n)
            %   Limit the order of polynomial field expansion to n at maximum.
            %   Default 999
            %
            %GETMODEL(...,'NumIntSteps',m)
            %   Set the NumIntSteps integration parameter to at least m.
            %   Default 20
            %
            %GETMODEL(...,'QuadFields',quadfields)
            %   When reading a BETA file, set quadrupoles fields to the specified ones.
            %   Default: {}
            %
            %GETMODEL(...,'BendFields',bendfields)
            %   When reading a BETA file, set bending magnet fields to the specified ones.
            %   Default: {}
            %
            %GETMODEL(...,'hardangle',value)        (SR only)
            %GETMODEL(...,'softangle',value)
            %   Set the angle of bending magnet sources
            %
            %GETMODEL(...,'PinholeID',cellnbs)      (EBS only)
            %   Select the cells where ID pinhole cameras are located. Default [7 25]
            %
            %GETMODEL(...,'PinholeBM',cellnbs)      (EBS only)
            %   Select the cells where BM pinhole cameras are located. Default [17 27 1]
            %
            %GETMODEL(...,'DeviceNames',true)       (EBS only)
            %   add device names in AT structure.
            
            [machid,location,args]=atmodel.getpath(varargin{:});
            cls=selectplane(machid,'choices',{'sr','sy','ebs'},{@sr.model,@sy.model,@ebs.model});
            mdl=cls(location,args{:});
        end
    end
    
    methods (Access=private, Hidden)
    end
    
    methods (Access=protected)
        function recompute(this,atstruct)
            %Compute the stored parameters
            if nargin >= 2
                this.ring=atstruct;
                this.modified=true;
            end
            if this.modified
                ndata=length(this.ring)+1;
                this.ll_=findspos(this.ring,ndata);
                this.alpha_=mcf(this.ring);
                [lindata,avebeta,avemu,avedisp,~,~]=atavedata(this.ring,0.0,1:ndata);
                this.inidata=lindata;
                tune=lindata(end).mu/2/pi;
                this.tunes_=tune;
                tt=cat(1,lindata.SPos)/this.ll_;
                spos = cat(1,lindata.SPos);% element entrance positions
                this.vlave=[[0.5*(tt(1:end-1)+tt(2:end));tt(end)] ...   % theta (1)
                    avebeta ...                                         % beta (2,3)
                    cat(1,lindata.alpha) ...                            % alpha (4,5)
                    avemu./tune(ones(ndata,1),:)/2/pi ...               % phi (6,7)
                    avedisp ...                                         % eta (8,9,10,11)
                    NaN*ones(ndata,1) (1:ndata)' ...                    % strength (12), id (13)
                    spos];                                              % s pos (14)
                %             this.vlini=[spos ...
                %                 cat(1,lindata.beta) ...
                %                 cat(1,lindata.alpha) ...
                %                 cat(1,lindata.mu)./tune(ones(ndata,1),:)/2/pi ...
                %                 cat(2,lindata.Dispersion)' ...
                %                 NaN*ones(ndata,1) (1:ndata)'];
                idz=this.getid('qp');
                this.vlave(idz,12)=setintegfield(this.ring(idz),'PolynomB',{2});
                idz=this.getid('sx');
                this.vlave(idz,12)=setintegfield(this.ring(idz),'PolynomB',{3});
                idz=this.getid('oc');
                this.vlave(idz,12)=setintegfield(this.ring(idz),'PolynomB',{4});
                this.modified=false;
                this.emith_=[];    % Triger computation of emittances
            end
            
            function v=setintegfield(at,varargin)
                k=atgetfieldvalues(at,varargin{:});
                l=atgetfieldvalues(at,'Length');
                v=k.*l;
            end
        end
        
        function recompute_emittance(this)
            if isempty(this.emith_)
                [~,params]=atx(this.ring,0.0,1);
                this.emith_=params.modemittance(1);
%                 this.emitv_=max(params.modemittance(2),5.e-12);
                this.emitv_=params.modemittance(2);
                this.blength_=params.blength;
                this.espread_=params.espread;
            end
        end
        
        function v=getave(this,code,prms)
            %Return stored parameters
            if nargin < 3, prms=this.prms; end
            this.recompute();
            v=this.vlave(this.getid(code),this.locs(prms+1));
        end
        
        function id=getid(this,code)
            %Return index of selected locations in the AT structure
            if ischar(code) % string code
                lcode=lower(code);
                if ~isfield(this.idx,lcode)
                    try
                        id=this.elselect(lcode);
                        this.idx.(lcode)=id;
                    catch
                        warning('atmodel:getid','Cannot store %s in the cache',code);
                    end
                else
                    id=this.idx.(lcode);
                end
            else            % logical mask or index list
                id=code;
            end
        end
        
        function idx=elselect(this,code)
            %Return indices of selected elements
            switch code
                case 'bpm'
                    idx=atgetcells(this.ring,'Class','Monitor');
                case 'qp'
                    idx=atgetcells(this.ring,'Class','Quadrupole');
                case 'sx'
                    idx=atgetcells(this.ring,'Class','ThinMultipole') | ...
                        atgetcells(this.ring,'Class','Sextupole');
                case 'oc'
                    idx=false(1,length(this.ring));
                case 'steerh'
                    idx=false(1,length(this.ring));
                case 'steerv'
                    idx=false(1,length(this.ring));
                otherwise
                    idx=atgetcells(this.ring,'FamName',code);
            end
        end
        
        function nm=elname(this,code)
            %Return the name of selected elements
            nm=atgetfieldvalues(this.ring(this.getid(code)),'FamName');
        end
    end
    
    methods
        function this=atmodel(atstruct)
            %Object giving access to the optical parameters
            %
            %ATMODEL(ATring)
            %
            %ATring:    AT structure
            ringpar=atgetcells(atstruct,'Class','RingParam');
            if any(ringpar)
                ringparam=atstruct{find(ringpar,1)};
                this.strname=ringparam.FamName;
                this.periods=ringparam.Periodicity;
            else
                this.strname='?';
                this.periods=1;
            end
            this.recompute(atstruct);
        end
        
        % dependent properties computed by atlinopt
        function ll=get.ll(this)
            this.recompute();
            ll=this.periods*this.ll_;
        end
        function alpha=get.alpha(this)
            this.recompute();
            alpha=this.alpha_;
        end
        function tunes=get.tunes(this)
            this.recompute();
            tunes=this.periods*this.tunes_;
        end
        function set.tunes(this,tunes)
            this.settune(tunes/this.periods);
        end
        function nuv=get.nuv(this)
            this.recompute();
            nuv=this.periods*this.tunes_(2);
        end
        function nuh=get.nuh(this)
            this.recompute();
            nuh=this.periods*this.tunes_(1);
        end
        
        % dependent properties computed by atx
        function emith=get.emith(this)
            this.recompute_emittance();
            emith=this.emith_;
        end
        function emitv=get.emitv(this)
            this.recompute_emittance();
            emitv=this.emitv_;
        end
        function bunch_length=get.bunch_length(this)
            this.recompute_emittance();
            bunch_length=this.blength_;
        end
        function energy_spread=get.energy_spread(this)
            this.recompute_emittance();
            energy_spread=this.espread_;
        end
        
        function varargout=name(this,varargin)
            %Return the names ('FamName' field of AT elements) of selected elements
            %
            %[NAMES_1,NAMES_2,...]=NAMES(CODE_1,CODE_2,...)
            %CODE_n:    One of the available locations ('bpm','qp',...)
            %NAMES_n:	Cell array of element names
            varargout=cellfun(@this.elname,varargin,'UniformOutput',false);
        end
        
        function varargout=get(this,varargin)
            %Return optical parameters at selected locations
            %
            %[V_1,V_2...]=GET(CODE_1,CODE_2...)
            %CODE_n:    One of the available locations ('bpm','qp',...)
            %V_n:       Array of optical values at selected locations
            %
            %[...]=GET(...,PRMS,...]
            %PRMS:      Numerical array defining the list of parameters for
            %           the following outputs
            prs=this.prms;
            nout=1;
            nin=1;
            while nin<nargin
                code=varargin{nin};
                if isnumeric(code)
                    prs=code;
                else
                    varargout{nout}=this.getave(code,prs); %#ok<AGROW>
                    nout=nout+1;
                end
                nin=nin+1;
            end
        end
        
        function varargout=select(this,varargin)
            %Return a logical mask selecting the desired elements
            %
            %[MASK_1,MASK_2...]=SELECT(CODE_1,CODE_2...)
            %CODE_n:    One of the available locations
            %MASK_n:	Logical mask
            varargout=cellfun(@this.getid,varargin,'UniformOutput',false);
        end
        
        function varargout=lindata(this,varargin)
            %Return optical parameters at selected locations
            %
            %[lindata1,lindata2,...]=LINDATA(CODE_1,CODE_2...)
            %CODEn:     One of the available locations, or AT index
            %lindatan:	"atlinopt" data at the entrance of selected elements
            this.recompute();
            varargout=cellfun(@(arg) this.inidata(this.getid(arg)), varargin,...
                'UniformOutput',false);
        end
        
        function varargout=plot(this,varargin)
            %Plot the optical functions
            %
            %ATPLOT(DPP)                Plots at momentum deviation DPP
            %ATPLOT(...,[SMIN SMAX])	Zoom on the specified range
            %ATPLOT(...,'OptionName',OptionValue,...)
            %                           See ATPLOT for available ooptions
            this.recompute();
            if (length(varargin) > 1) && isscalar(varargin{1}) && ishandle(varargin{1})
                [varargout{1:nargout}]=atplot(varargin{1},this.ring,varargin{2:end});
            else
                [varargout{1:nargout}]=atplot(this.ring,varargin{:});
            end
        end
        
        function settune(this,tunes,varargin) %#ok<INUSD>
            %Change tunes
            %
            %SETTUNE(TUNES)
            % TUNES:    1x2 vector of desired tunes
            error('atmodel:NotImplemented','No generic imlementation of retuning');
        end
        
        function setchrom(this,tunes,varargin) %#ok<INUSD>
            %Change chromaticity
            %
            %SETCHROM(CHROMATICITY)
            % CHROMATICITY:    1x2 vector of desired chromaticities
            error('atmodel:NotImplemented','No generic imlementation of chromaticity setting');
        end
        
        function setfieldvalue(this,code,varargin)
            %Set the value of selected element field values
            %
            %SETFIELDVALUE(CODE,FIELDNAME,INDICES,...,VALUES)
            % CODE:     One of the available locations, or AT index
            % FIELDNAME, INDICES, VALUES:
            %           See ATSETFIELDVALUES for these arguments
            %
            %Example:
            %>> RNG.SETFIELD('qp','PolynomB',{2},STRENGTHS) will set
            %  the strength of all quadrupoles
            idz=this.getid(code);
            this.ring(idz)=atsetfieldvalues(this.ring(idz),varargin{:});
            this.modified=true;
            this.emith_=[];
        end
        
        function varargout=getfieldvalue(this,code,varargin)
            %Return the value of selected element field values
            %
            %VALUE=GETFIELDVALUE(CODE,FIELDNAME,INDICES,...)
            % CODE:     One of the available locations, or AT index
            % FIELDNAME, INDICES:
            %           See ATGETFIELDVALUES for selection details
            %
            %Example:
            %>> VALUE=RNG.GETFIELD('qp','PolynomB',{2}) will return the strength
            %    of all quadrupoles
            idz=this.getid(code);
            varargout{1}=atgetfieldvalues(this.ring(idz),varargin{:});
        end
        
        function data=bpm(this,varargin)
            data=this.getave('bpm',varargin{:});
        end
        
        function data=qp(this,varargin)
            data=this.getave('qp',varargin{:});
        end
        
        function data=sx(this,varargin)
            data=this.getave('sx',varargin{:});
        end
        
        function data=steerh(this,varargin)
            data=this.getave('steerh',varargin{:});
        end
        
        function data=steerv(this,varargin)
            data=this.getave('steerv',varargin{:});
        end
    end
end
