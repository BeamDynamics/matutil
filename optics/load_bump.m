function bump=load_bump(fname)

fid=fopen(fname,'rt');
i=0;
while 1
   line=fgetl(fid);
   if ~ischar(line), break, end
   i=i+1;
   bp(i,:)=sscanf(line,'%*s %f %*s %f %*s %f',[1 3]);
end
ns=size(bp,1);
bump=zeros(ns,ns);
bump(1,[ns 1 2])=bp(1,:);
for i=2:ns-1
bump(i,i+[-1 0 1])=bp(i,:);
end
bump(ns,[ns-1 ns 1])=bp(ns,:);

fclose(fid);
