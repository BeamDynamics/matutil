function [resh,resv]=exp_model(pth)
%BETA_MODEL     Prepare matrices for the autocor device server
%
%Deprecated: Use sr.autocor_model

warning('obsolete:model','%s is deprecated: Use sr.autocor_model instead',mfilename());

if nargin < 1, pth=pwd; end

h_resp=load_rsp2('h',fullfile(pth,'resH.rsp'));
v_resp=load_rsp2('v',fullfile(pth,'resV.rsp'));
[h_fresp,~]=sr.load_fresp(pth);

b_ok=all(isfinite([h_resp v_resp h_fresp]),2);
b_ok(enter_disbpm(find(~b_ok)))=false;

sh_ok=true(1,size(h_resp,2));
sh_ok(enter_dissteer([],'SR/ST-H'))=false;

sv_ok=true(1,size(v_resp,2));
sv_ok(enter_dissteer([],'SR/ST-V'))=false;

[resh,resv]=compute_srcor(pth,'sr',pth,'measured',h_resp,v_resp,h_fresp,b_ok,sh_ok,sv_ok);
end