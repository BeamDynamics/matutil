[ring,periods]=atreadbeta(fullfile(getenv('DBETA'),'noqd1qd8.str'));
%[ring,periods]=atreadbeta(fullfile(getenv('DBETA'),'noqd1qd8betaz3_5.str'));
vqp0=atgetparam(ring,'QP');
[coefh0,coefv0]=load_corcalib('sr');
x0=[vqp0([2:5 8:9]);coefh0(1:3)';coefv0(1:3)'];
location=fullfile(getenv('APPHOME'),'sr','optics','settings','noqd1qd8');
%location=fullfile(getenv('APPHOME'),'sr','optics','settings','noqd1qd8betaz3_5');
[vh,ih]=sr.load_resp(fullfile(location,'resH.rsp'),'H');
[vv,iv]=sr.load_resp(fullfile(location,'resV.rsp'),'V');
h0=vh./ih(ones(224,1),:);
v0=vv./iv(ones(224,1),:);
%h0(:,55)=NaN; %for noqd1qd8 resp.1
%v0(:,55)=NaN;
hh=srmresp(h0,16);
vv=srmresp(v0,16);
%f=srfit(x0,ring,hh,vv);
options=optimset(optimset('fminsearch'),'Display','iter','TolX',1.e-3,'MaxFunEvals',600);
t=ones(size(x0));
%optf=@(t)srfit((t+99)/100.*x0,ring,hh,vv);
optf=@(t)srfittune((t+99)/100.*x0,ring,hh,vv);
[t,f]=fminsearch(optf,t,options);
x=(t+99)/100.*x0;
vqp=[0;x([1:4 4:-1:3 5:6]);0;0;x([6:-1:5 3:4 4:-1:1]);0];
coefh=x([7:9 9:-1:7])';
coefv=x([10:12 12:-1:10])';
disp([vqp0 vqp]);
%[x0 x]
f

% noqd1qd8
%x0=[0.390736;	-0.631476;	-0.601718;	0.734393;	-0.819474;	0.681578;...
%    4.40295e-4; 3.760713e-4;	4.373988e-4;	4.374393e-4;	3.760647e-4;	4.401928e-4];
%x0=[0.390736;	-0.631475;	-0.601717;	0.734394;	-0.819473;	0.681577;...
%    4.40519e-4; 3.76262e-4;	4.48556e-4;	4.47230e-4;	3.76256e-4;	4.40416e-4;...
%    2.69437e-4; 2.50927e-4;	2.73387e-4;	2.74439e-4;	2.50927e-4;	2.69437e-4];
xfit2=[0.391613;	-0.603006;	-0.601015;	0.734863;	-0.814573;	0.683174;...
    4.40519e-4; 3.76262e-4;	4.48556e-4;	4.47230e-4;	3.76256e-4;	4.40416e-4;...
    2.69433e-4; 2.51111e-4;	2.74603e-4;	2.73910e-4;	2.50929e-4;	2.69342e-4];

%f=4.86117
xfit3=[0.391792;	-0.603065;	-0.600994;	0.734867;	-0.814944;	0.682608;...
    4.38999e-4; 3.76087e-4;	4.37146e-4;...
    2.69529e-4; 2.51222e-4;	2.74201e-4];

%f=4.86117
xfit4=[0.391792;	-0.603065;	-0.600994;	0.734867;	-0.814944;	0.682608;...
    4.38999e-4; 3.76087e-4;	4.37146e-4;...
    2.69529e-4; 2.51222e-4;	2.74201e-4];
