function [reshexp,deltaf]=expandhresp(resh,dispersion,params)
%[reshexp,deltaf]=expandhresp(resh,dispersion,params)   expands hor. response matrix with RF frequency

[nb,nk]=size(resh);
orbits=rms2([resh dispersion]);
rmsorbits=mean(orbits(1:end-1));
scalef=rmsorbits/orbits(end);
scalei=4*rmsorbits;
reshexp=[resh scalef*dispersion;scalei*ones(1,nk) 0];
deltaf=params.alpha*992*2.997924E8/params.ll*scalef;

function a=rms2(x,dim)
dimx=size(x);
if nargin < 2
    dim=find(dimx ~= 1, 1);
    if isempty(dim), dim = 1; end
end
a=sqrt(sum2(x.*x, dim)./sum2(isfinite(x),dim));
