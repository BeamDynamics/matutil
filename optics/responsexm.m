function r=responsexm(bpm,st,nu)
%R=RESPONSEM(OBS,EXC,NU) buids response matrix from EXC to OBS
%
%	OBS,EXC: [beta Phi/2PiNu]

a=sqrt(bpm(:,1)*(1./st(:,1)'));
arg=bpm(:,2)*ones(size(st(:,2)')) - ones(size(bpm(:,2)))*st(:,2)';
arg=((arg>0)-0.5) - arg;
b=sin(arg*(2*pi*nu));
r=a.*b./(2*sin(pi*nu));
