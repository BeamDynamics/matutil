function showresp(varargin)
%SHOWRESP(OPTICS,PLANE,TUNE)		Displays SVD properties for the selected optics
%
%Plots the harmonic contents of Eigen vectors and amplitude of Eigen values
%
%OPTICS:     - full path of the optics directory
%		(ex.: '/machfs/appdata/sr/optics/settings/nombz2.5')
%	     - 'sr' or 'sy' (using the 'theory' optics for the selected machine)
%	     - both
%
%PLANE:      - 'h' or 'v'
%
%TUNE:	     - tune values (defaults to the theoretical tune)

[narg,path,mach]=getmachenv(varargin{:});
plane=selectplane(varargin{narg},{'h','v'});
narg=narg+1;

[pm,bpm,steerer]=load_optics(path,[plane 'bpm'],['steer' plane]);
if strcmp(mach,'sy')
   coef=load_corcalib('sy',plane);
else
   coef=load_corcalib('sr',plane);
   coef=reshape(coef(ones(16,1),:)',1,96);
   coef=coef(ones(size(bpm,1),1),:);
end

if (strcmp(plane,'h'))
   cols=[2 3];
   betatune=pm.nuh;
else
   cols=[5 6];
   betatune=pm.nuv;
end

if nargin >= narg
   tune=varargin{narg};
   narg=narg+1;
else
   tune=betatune;
end

disp('compute response matrix...');
r=coef.*responsem(bpm(:,cols),steerer(:,cols),tune);

disp('compute eigen vectors...');
[base lamsim v]=corsvd2(r);

disp('compute harmonic analysis...');
hsim=harmana(0:40,bpm(:,2:3),base*diag(lamsim));
harmodisp(hsim,1);

figure(2);
if exist('lammes') == 1
semilogy([lamsim lammes]);
else
semilogy(lamsim);
end
