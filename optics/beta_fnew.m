% leaves in the workspace:
%	r:			response matrix
%	steer_num, steer_cell:	list of steerer names
%	bpm_num, bpm_cell:	list of bpm names
%	plane:			'h' or 'v'
%	c2pinu:			normalization constant
%	sb:			bpm location
%	pb0:			bpm phase
%	sk:			steerer location
%	pk0:			steerer phase

%	rr:			matrix of cosines
%	residu:			residual error
%	bbm:			estimate of sq. root of BPM beta
%	bkm:			estimate of sq. root of steerer beta
%	bok:			"good" bpms for fit (large beta)
filename=input('.rsp file: ','s');
plane=input('plane (h/v): ','s');
tune=input([plane ' tune: ']);

if plane == 'h'
    pars=[2 3 5 6];
    plnum=1;
else
    pars=[2 8 10];
    plnum=2;
end;


%fullfname=which(filename)
%if isempty(strfind(fullfname,'sy'))	% SR
if false				% SR

    periods=16;
    if plane == 'h'
    index=2;
    blist=[1;2;4;6;9;11;13;14];
    else
    index=5;
    blist=[2;3;5;6;9;10;12;13];		%order of bpms in array
    end;

    coef=load_corcalib('sr',plane);

    [bpm,sext,betanux,betanuz,betaperiods,alpha]=load_betalog('sr');

    ns=size(sext,1);
    nb=size(bpm,1);
    copyr=ones(1,periods);

    cell_list=(0:31)+4;
    cell_list=cell_list-32*(cell_list>32);
    bpm_num=[1:7 1:7]';
    bpm_num=reshape(bpm_num*copyr,nb,1);
    steer_num=[4;6;13;19;20;22;24;24;22;20;19;13;6;4];
    steer_num=reshape(steer_num*copyr,nb,1);
    bpm_cell=reshape(ones(nb/32,1)*cell_list,nb,1);
    steer_cell=reshape(ones(ns/32,1)*cell_list,ns,1);

    %r=load_rsp(plane,[],filename);
    r=load_rsp2(plane,filename);
else					% SY

    periods=3;

    [params,bpm,steerer,bok]=load_optics('sy',pars,[plane 'bpm'],['steer' plane],[plane '_bpms']);
    coef=load_corcalib('sy',plane)*ones(size(steerer,1),1);

    [rh,rv]=load_syrsp(filename);
    r=eval(['r' plane '(bok,:)']);
end

if plane == 'h'
if input('remove off-momentum (y/n): ','s') ~= 'n'
   disp('remove off-momentum');
   rfreq=-bpm(:,4)/352.2e6/params.alpha;% theoretical displacement for 1 Hz
%  rfreq=bpm(:,4);			% theoretical displacement for 1 %
   r=defreq(r,rfreq);
   clear rfreq
end
end

nb=size(bpm,1);
copyr=ones(1,periods);

sb=bpm(:,1);
bb0=bpm(:,2);
pb0=bpm(:,3);
bbm=sqrt(bb0);
sk=steerer(:,1)';
bk0=steerer(:,2)';
pk0=steerer(:,3)';
bkm=sqrt(bk0);

c2pinu=2*sin(pi*tune)./repmat(coef',size(r,1),1);
rr=(r./(bbm*bkm)).*c2pinu;

clear coef

while 1
   for j=1:5
      [bbm residu]=stepb(rr,bbm);
      rr=(r./(bbm*bkm)).*c2pinu;
      [bkm residu]=stepk(rr,bkm);
      rr=(r./(bbm*bkm)).*c2pinu;
%     if residu < 2.0e-4, break, end
   end
   rep=input('stop ? (y/n)','s');
   if ~strcmp(rep,'n'), break, end
end

scaleb=sqrt(sybpmaverage(var2(rr')',plane));
scalek=sqrt(systeeraverage(var2(rr),plane));

while 1
   for j=1:10
      [bbm residu]=stepb(rr,bbm,scaleb(:));
      rr=(r./(bbm*bkm)).*c2pinu;
      [bkm residu]=stepk(rr,bkm,scalek(:)');
      rr=(r./(bbm*bkm)).*c2pinu;
      if residu < 2.0e-4, break, end
   end
   rep=input('stop ? (y/n)','s');
   if ~strcmp(rep,'n'), break, end
end
%clear scaleb scalek

%[bb,bk]=betanorm(bbm,bkm,bb0);
bb1=syexpand(bbm);
select=true(78,1);
select([23 25 52])=false;
if strcmp(plane,'h')
    bk1=[bkm;NaN*ones(1,39)];
    select(2:2:78)=false;
else
    bk1=[NaN*ones(1,39);bkm];
    select(1:2:78)=false;
end
r=sqrt(mean2(bb1(select).^2./bk1(select).^2))
bb=bbm.^2/r;
bk=bkm.^2*r;

averb=sybpmaverage(bb,plane);	% compute average and modulation
modulb=(bb-averb)./averb;
bpm_modulation=std2(modulb);

bad_bpm=find(abs(modulb) > (2.5*bpm_modulation));
good_bpm=1:nb;
good_bpm(bad_bpm)=[];
if (~isempty(bad_bpm))
   disp('rejected bpms (too high modulation):');
   disp(bad_bpm);
   dd=bb;
   dd(bad_bpm)=NaN;

   averb=sybpmaverage(dd,'h');	% compute average and modulation
   modulb=(bb-averb)./averb;
   bpm_modulation=std2(modulb);
end

averk=systeeraverage(bk,plane);
modulk=(bk-averk)./bk;

bpm_modulation

bb1=reshape(syexpand(bb),2,39);
bb1(25)=NaN;
if strcmp(plane,'h')
    check=[bb1(1,:);bk]';
    check(13,1)=NaN;
    check([12 13],2)=NaN;
else
    check=[bb1(2,:);bk]';
    check(26,2)=NaN;
end

figure(1)
clf
plot(check);
lg=legend('BPM','Steerer');
title('comparison BPM/steerer');

%[pb pbave]=bphase(rr,sb,sk,tune,pb0,pk0);
%[pk pkave]=bphase(rr',sk',sb',tune,pk0',pb0');

%bx=1:nb/16;
%kx=1:size(rr,2)/16;
axes(lg);

figure(2);

subplot(2,1,1);

plot(sb(1:nbcell),(averb')./bb0(1:nbcell),sk(1:nkcell)',(averk./bk0(1:nkcell))');
%plot(sb(1:nbcell),(averb')./bb0(1:nbcell));
title('average measured beta/model');

subplot(2,1,2);
%plot(sb(bx),pbave(bx)-pb0(bx),sk(kx)',pkave(kx)-pk0(kx)');
%legend('BPM','Steerer');
%title('average phase advance - model');

figure(3);
subplot(2,1,1);
plot(sb(good_bpm),modulb(good_bpm),sk,modulk);
hold on
plot(sb(bad_bpm),modulb(bad_bpm),'ro');
hold off
title('beta / average beta');

subplot(2,1,2);
%plot(sb,pb-pbave,sk,pk-pkave);
%plot(sb,pb-pb0,sk',pk-pk0');
%legend('BPM','Steerer');
%title('phase advance - model');

clear periods nb ns blist copyr
