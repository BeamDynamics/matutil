function [ave,ave6m,ave7m]=srbpmaverage(bpmdata,varargin)
%SRBPMAVERAGE Averages over bpms after eliminating non-standard ones

bpm6m=[...
    70 71 ...   % ID14
    84 85 ...   % ID16
    98 99 ...   % ID18
    112 113 ... % ID20
    140 141 ... % ID24
    182 183 ... % ID30
    196 197 ... % ID32
    203 204 ... % ID1
    ];

bpm7m=[...
    131 132 133 136 135 134 ... % ID23
    ];
unfold=false;
mfunc=@mean2;
for i=1:length(varargin)
    if ischar(varargin{i})&&strcmpi(varargin{1},'unfold'), unfold=true; end
    if isa(varargin{i},'function_handle'), mfunc=varargin{i}; end
end
sz=size(bpmdata);
v=reshape(bpmdata,sz(1),[]);
[n1,n3]=size(v);

v6m=v(bpm6m,:);     % eliminate non-standard ones
v7m=v(bpm7m,:);
v([bpm6m bpm7m],:)=NaN;

v2=NaN(16,16,n3);
v2([1:4 6:8 16:-1:13 11:-1:9],:,:)=reshape(v,14,16,n3);

vave=mfunc(reshape(v2,8,32,n3),2);
v6mave=mfunc(v6m,1);
v7mave=mfunc(reshape(v7m,[],2,n3),2);
if unfold
    v2=reshape(repmat(vave,[1 32 1]),16,16,n3);
    v=reshape(v2([1:4 6:8 16:-1:13 11:-1:9],:,:),n1,n3);
    v(bpm6m,:)=repmat(v6mave,size(v6m,1),1);
    v(bpm7m,:)=reshape(repmat(v7mave,[1 2 1]),[],n3);
    ave=reshape(v,sz);
else
    ave=reshape(squeeze(vave),[8 sz(2:end)]);
    ave6m=reshape(v6mave,[1 sz(2:end)]);
    ave7m=reshape(squeeze(v7mave),[3 sz(2:end)]);
end
end

