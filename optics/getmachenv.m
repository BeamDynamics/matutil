function [narg,pth,mach]=getmachenv(varargin)
%GETMACHENV		% extracts machine information from argument list
%
%[narg,path,mach]=getmachenv(varargin) scans the input arguments looking for:
%
%- '/machfs/appdata/sr/optics/settings/theory'[,'sr']:
%			full path of optics directory
%- 'nombz2.5'[,'sr']
%           optics name and machine
%- 'sr' or 'sy' or 'ebs'
%           machine name, path defaults to $(APPHOME)/mach/optics/settings/theory
%- atstrcut[,'sr']
%           an Accelerator Toolbox structure, machine name
%
%NARG:	next remaining argument
%PATH:	full path of the optics directory
%MACH:	'sr' or 'sy'

global APPHOME
narg=1;
mach='sr';
pth='';
%                               Look for machine definition
if (nargin >= narg) && ischar(varargin{narg}) && tmach(varargin{narg})
    mach=lower(varargin{narg});
    narg=narg+1;
end

if (nargin >= narg)	%           Look for AT or directory
    if iscell(varargin{narg})
        narg=narg+1;              % AT structure
    elseif ischar(varargin{narg}) && ~tmach(varargin{narg}) && ~tplane(varargin{narg})
        pth=varargin{narg};      % Full path or optics name
        if ~isempty(strfind(pth,'sy'))
            mach='sy';
        elseif ~isempty(strfind(pth,'ebs'))
            mach='ebs';
        end
        narg=narg+1;
    end
end
%                               Look for machine definition again
if (nargin >= narg) && ischar(varargin{narg}) && tmach(varargin{narg})
    mach=lower(varargin{narg});
    narg=narg+1;
end

if isempty(pth)
    pth=fullfile(APPHOME,mach,'optics','settings','theory');
elseif isempty(fileparts(pth))
    pth=fullfile(APPHOME,mach,'optics','settings',pth);
end

    function ok=tplane(argx)
        try
            ok=selectplane(argx,{true,true,true,true});
        catch %#ok<CTCH>
            ok=false;
        end
    end

    function ok=tmach(argx)
        ok=any(strcmpi(argx,{'sr','sy','ebs'}));
    end
end
