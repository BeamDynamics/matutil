function correction=girdergo(resp, orbit, sig)

nmax=size(resp,2);
[base,lambda,v]=corsvd2(resp);
cor=gosvd(lambda, base, orbit);
vcor=v*diag(cor);
cumcor=[zeros(nmax,1) cumsum(vcor')'];		% no correction as 1st vector
if nargin < 3
orbits=resp*cumcor+orbit*ones(1,nmax+1);	% initial orbit in 1st vector
else
orbits=resp*(cumcor+sig*randn(nmax,nmax+1))+orbit*ones(1,nmax+1);	% initial orbit in 1st vector
end

subplot(2,1,1);
%bar(0:nmax,std(cumcor));
%hold on
%plot(0:nmax,max(abs(cumcor)));
%hold off
plot(0:nmax,[std(cumcor);max(abs(cumcor))]);
subplot(2,1,2);
bar(0:nmax,std2(orbits));
eval('save temp nmax cumcor orbits','disp(''Cannot save temp. files'')');

nvects=input('Number of vectors: ');		% in [0 nmax]
correction=cumcor(:,nvects+1);
if nvects > 0
   %corrot=correction;
   corrot=jack2rot*correction;
   cortrans=jack2trans*correction;
   savetext([getenv('APPHOME') '/hls-user/girrot.dat'],'%.4f\n',-1000*corrot([88:96,1:87]));
   savetext([getenv('APPHOME') '/hls-user/girtrans.dat'],'%.4f\n',-1000*cortrans([88:96,1:87]));
end
