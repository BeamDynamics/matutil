function [h2h,v2h,h2v,v2v]=applysxdisp(bpm0,sext0,nux,nuz,skew)
%[H2H,V2H,H2V,V2V]=SEXTUDISP(SKEW) computes coupled resp. matrix for skew atrengths at steerer locations
%
%SKEW:			Integrated skew strength (m-1)
%H2H,V2H,H2V,V2V:	(m/rad)

ns=size(sext0,1);
keepk=reshape(1:ns,ns/16,16);
keepk=keepk([2 4 6 9 11 13],:);

h_kb=responsem(bpm0(:,2:3),sext0(keepk,2:3),nux);
v_kb=responsem(bpm0(:,5:6),sext0(keepk,5:6),nuz);
h_kk=responsem(sext0(keepk,2:3),sext0(keepk,2:3),nux);
v_kk=responsem(sext0(keepk,5:6),sext0(keepk,5:6),nuz);

ns = length(skew);
dskew=diag(skew);
mh=h_kb*dskew/(eye(ns)-v_kk*dskew*h_kk*dskew);
mv=v_kb*dskew/(eye(ns)-h_kk*dskew*v_kk*dskew);

h2h=h_kb+mh*v_kk*dskew*h_kk;
v2h=mh*v_kk;
h2v=mv*h_kk;
v2v=v_kb+mv*h_kk*dskew*v_kk;

%h2h=h_kb;
%v2h=h_kb*dskew*v_kk;
%h2v=v_kb*dskew*h_kk;
%v2v=v_kb;
