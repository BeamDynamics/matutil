function xres=posang(orbit1,orbit2,dist)

oper=[0.5 -1/6.05;0.5 1/6.05];
if nargin >= 3
   oper=[oper oper(:,1)+dist*oper(:,2)];
end
oper

if (nargin >= 2) & ~isempty(orbit2)
   orbit=orbit2-orbit1;
else
   orbit=orbit1;
end

orbit=reshape(orbit,7,32)';
x1=orbit(:,1);
x7=[orbit(32,7);orbit(1:31,7)];
xres=[x7 x1]*oper;
