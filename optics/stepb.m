function [bb2,residu]=stepb(rr,bbm,rmsval)
% iterates on BPM beta values
%	rr:			matrix of cosines
%	bbm:			estimate of sq. root of BPM beta
%	residu:			residual error

if nargin >= 3
   scale=std2(rr')'./rmsval;
else
   scale=max2(abs(rr'))';
end
bb2=bbm.*scale;
residu=std2(bb2-bbm)
