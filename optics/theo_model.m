function [resh,resv]=theo_model(pth)
%BETA_MODEL     Prepare matrices for the autocor device server
%
%Deprecated: Use sr.autocor_model

warning('obsolete:model','%s is deprecated: Use sr.autocor_model instead',mfilename());

if nargin < 1, pth=pwd; end

[coefh, coefv]=load_corcalib('sr',8,9);     % get steerer calibration (rad/A)

try
    load(fullfile(pth,'betamodel'));
catch
    betamodel=convert_optics(pth);
    save betamodel betamodel
end
sx=find(atgetcells(betamodel,'Class','Sextupole'));
sthidx=sx(selcor(8));
stvidx=sx(selcor(9));
bpmidx=find(atgetcells(betamodel,'Class','Monitor'));
[pm,bpm,bpm_name,steerh,steerh_name,steerv,steerv_name]=load_optics(betamodel,'bpm','bpm_name',...
   'steerh','steerh_name','steerv','steerv_name');
nb=size(bpm,1);

if exist('./alpha','file') ~= 2,
   savetext('alpha','%.4e\n',pm.alpha);
end

h_resp=atorm(betamodel,'h',sthidx,bpmidx).*coefh(ones(nb,1),:);
v_resp=atorm(betamodel,'v',stvidx,bpmidx).*coefv(ones(nb,1),:);
dct=4.e-4;
forb=findsyncorbit(betamodel,dct,bpmidx);
h_fresp=-forb(1,:)'/dct.*pm.ll*pm.ll/992/2.997924e8;

b_ok=true(nb,1);
b_ok(enter_disbpm(find(~b_ok)))=false;

sh_ok=true(1,size(h_resp,2));
sh_ok(enter_dissteer([],'SR/ST-H'))=false;

sv_ok=true(1,size(v_resp,2));
sv_ok(enter_dissteer([],'SR/ST-V'))=false;

hv_bpms=[bpm(:,1:2) 2*pi*bpm(:,3) bpm(:,4:5) 2*pi*bpm(:,6)];
h_steerers = [steerh(:,1:2) 2*pi*steerh(:,3) coefh(:)];
v_steerers = [steerv(:,[1 5]) 2*pi*steerv(:,6) coefv(:)];
store_bpm('bpms',hv_bpms,bpm_name);
store_steerer('h_steerers',h_steerers,steerh_name);
store_steerer('v_steerers',v_steerers,steerv_name);

[resh,resv]=compute_srcor(betamodel,pth,'theoretical',h_resp,v_resp,h_fresp,b_ok,sh_ok,sv_ok);
end