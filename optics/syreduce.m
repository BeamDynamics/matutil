function r=syreduce(rexp,code)
%SYREDUCE		Selects existing BPMS
%			and removes mispositioned BPM (25)
%
%X=SYEXPAND(XEXP,CODE)
%
%	XEXP : 78xN matrix
%	CODE : 'h' or 'v' or 'all' (default 'all')
%	X   :  75xN matrix or 38xM (horizontal) or 37xN (vertical)
%
if nargin < 2, code='all'; end

resp(25)=NaN;
ok=true(78,1);
ok([23 26 52])=false;
if strcmp(code,'h')
    ok(2:2:78)=false;
elseif strcmp(code,'v')
    ok(1:2:78)=false;
end
r=rexp(ok);
