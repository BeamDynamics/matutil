function rbump_arch(cl,blk)
%RBUMP_ARCH(CELL,BPM) Plot an archived offset measurement
%

load(sprintf('C%d-%d',cl,blk));
rbump_plot(4,'calib',vhv(:,1:2),vhv(:,3:4));
rbump_plot(3,'fit',vhv(:,1:2),vhv(:,7:8));
end

