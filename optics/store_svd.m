function res=store_svd(fname,resp,bselect,stlist,fresp,stweight)

% vector 1 : RF dominant
% vector 2 : sum I dominant
%
% inverts a normalized response scaleb*resp*scales
%
%Response matrix	:	scaleb^-1*U*S*V'*scales^-1
% or                    scaleb^-2*base*lambda^2*solve'*scales^-2
%
%Correction matrix	:	scales*V*S^-1*U'*scaleb
% or                    solve(:,1:nmax)*base(:,1:nmax)'
%
% corr*resp=I

[nb,nk]=size(resp);
if nb <= 0, return, end

bpmlist=all([bselect isfinite(resp)],2);	% keep valid BPMs

if nargin > 4
    if nargin > 5
        res.stweight=stweight;
    else
        stweight=ones(1,nk);
    end
    orbits=rms([resp(bpmlist,stlist) fresp(bpmlist)]);
    rmsorbits=mean(orbits(1:end-1));
    rmssteer=rms(stweight(:));
    res.scalei =4*rmsorbits/rmssteer;	% normalize Sigma I
    res.scaledf=  rmsorbits/orbits(end);% normalize frequency resp
%   respx=    [resp,           fresp;           stweight,0];
    res.resp2=[resp,res.scaledf*fresp;res.scalei*stweight,0]; %expand response matrix
    [nb,nk]=size(res.resp2);
    bpmlist=[bpmlist;true];
    stlist=[stlist true];

    [u,s,v]=svd(res.resp2(bpmlist,stlist),'econ');

    lambda=diag(s);

    [dum,id]=sort(abs([v(end,:);sum(v(1:end-1,:))]),2);
    u(end,:)=u(end,:)*res.scalei;		% rescale Sigma I in Amps
    v(end,:)=v(end,:)*res.scaledf;		% rescale freq in dp/p

    extract=id(:,end)';
    reorder=1:length(lambda);
    reorder(extract)=[];            % SigmaI: vector 2
    reorder=[extract reorder];		% freq: vector 1
    u=u(:,reorder);
    lambda=lambda(reorder);
    v=v(:,reorder);
else
%   respx=resp;
    res.resp2=resp;

    [u,s,v]=svd(res.resp2(bpmlist,stlist),'econ');

    lambda=diag(s);

    %  v=v(:,1:numel(lambda)); %rajoute par TP, not necessary with 'econ' flag

end
nvects=length(lambda);
vv=zeros(nk,nvects);
vv(stlist,:)=v;
res.svdsolve2=vv*diag(1./lambda);
res.svdbase2=zeros(nb,nvects);
res.svdbase2(bpmlist,:)=u;
res.lambda=lambda;

save(fname,'-struct','res','-v6');
% if nargin > 5
%     save(fname,'svdbase','svdsolve','lambda','resp2', 'scalei','scalef','stweight','-v6');
% elseif nargin > 4
%     save(fname,'svdbase','svdsolve','lambda','resp2', 'scalei','scalef','-v6');
% else
%     save(fname,'svdbase','svdsolve','lambda','resp2','-v6');
% end
%svdbase2=res.resp2*svdsolve; %#ok<NASGU>
%save([fname '2'],'svdbase2');
%save([fname '_raw'],'resp2','scalei','scalef','bpmlist','stlist');

function a=rms(x)
a=sqrt(sum(x.*x)./size(x,1));
