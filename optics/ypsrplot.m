scell=844.39/16;
hh=betaline(1);
sbh0=get(hh,'xData');
bbh0=get(hh,'yData');
hh=betaline(2);
sbv0=get(hh,'xData');
bbv0=get(hh,'yData');
figure(2)
plot(sbh0,bbh0,'r');
ax=axis;
ax(2)=scell;
axis(ax);
hold on
plot(sbv0,bbv0,'b');
%plot(sbv,bbv0);
legend('Horizontal','Vertical');
xlabel('s [m]');
ylabel('\beta [m]');

plot(844.39*sbh(1:14),bbh,'ro');
plot(844.39*sbv(1:14),bbv,'bo');
hold off
