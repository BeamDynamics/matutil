function f=srfit(x,ring,resph,respv)

vqp=[0;x([1:4 4 3 5 6]);0;0;x([6 5 3 4 4:-1:1]);0];
coefh=repmat(x([7:9 9:-1:7])',size(resph,1),16);
coefv=repmat(x([10:12 12:-1:10])',size(respv,1),16);
ring2=repmat(atsetparam(ring,'QP',vqp),16,1);
[params,bpm,steerh,steerv]=load_optics(ring2,'bpm','steerh','steerv');
h_resp=responsem(bpm(:,2:3),steerh(:,2:3),params.nuh).*coefh;
v_resp=responsem(bpm(:,5:6),steerv(:,5:6),params.nuv).*coefv;
dh=resph-h_resp;
dv=respv-v_resp;
f=std2(dh(:))+std2(dv(:));
