function kicks=apply_bump(k,n)

ncor=size(k,1);
nn=length(n);
index=[n-1;n;n+1] + ncor*[(n<=1);zeros(1,nn);-(n>=ncor)];
kicks=zeros(ncor,nn);
for i=1:nn
kicks(index(:,i),i)=k(index(2,i),1:3)';
end
