function show_conditions(filename)

if nargin < 1, filename='matlog'; end
v=load(filename);
fields=fieldnames(v);
for i=1:length(fields)
    fname=fields{i};
    disp([char(10) fname ':']);
    disp(v.(fname));
end
