function [a,ainv]=newrquad(sext,nux,nuz,code)

px1=ceil(2*nux);
px2=floor(2*nux);
pz1=ceil(2*nuz);
pz2=floor(2*nuz);
if nargin < 4, code='coupled'; end
%				response 9x16
a=[...
%	0.02*r2nux(sext,px1,nux);...
%	0.02*r2nux(sext,px2,nux);...
%	0.02*r2nuz(sext,pz1,nuz);...
%	0.02*r2nuz(sext,pz2,nuz);...
	0.02*r2nux(sext,px1,0.5*px1);...
	0.02*r2nux(sext,px2,0.5*px2);...
	0.02*r2nuz(sext,pz1,0.5*pz1);...
	0.02*r2nuz(sext,pz2,0.5*pz2);...
	ones(1,size(sext,1)) ...
];

amod=a(:,[2:10 12:16]);

if strcmp(code,'coupled')
   exc=a(1:8,1:8)-a(1:8,9:16);
   figure(1);
   resonplot(exc,[px1 px2 pz1 pz2]);
   rexc=inv(exc);
   response=[a(1:8,:);ones(1,16)]';			%transpose for C order
   correction=[rexc zeros(8,1);-rexc zeros(8,1)]';	%transpose for C order
else
   figure(1);
   resonplot(amod(1:8,:),[px1 px2 pz1 pz2]);

   [u,s,v]=svd(amod,0);
   lambda=diag(s)
   keep=1:length(lambda);
   binv=v(:,keep)*diag(1./lambda(keep))*u(:,keep)';	% corr 16x9
   ainv=[zeros(1,9);binv(1:9,:);zeros(1,9);binv(10:14,:)];
   correction=[ainv(:,1:8) zeros(16,1)]';	%transpose for C order
end

i_rms = std(correction')
response=[a(1:8,:);ones(1,16)]';		%transpose for C order
matvers=sscanf(version,'%d.%d');
if matvers(1) >= 5, fmode=' -v4'; else, fmode='';end
eval(['save quadcor response correction' fmode]);
store_variable('matlog','quad_reson_mode',code);
