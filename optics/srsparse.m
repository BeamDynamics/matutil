function g=srsparse(j0,npercell,value)
%G=SRSPARSE(GINDEX,NPERCELL,FACTOR) builds the quad to girder transformation
%	32 cells are assumed
%	GINDEX:	vector indicating girder number for each element in 1st cell
%	NPERCELL: number of girders per cell
%	FACTOR: vector indicating weight factor for each element in 1st cell

n2=32*length(j0);
cols=reshape(1:32*npercell,npercell,32);
cols=cols(j0,:);
if nargin >= 3
   vals=value(:)*ones(1,32);
else
   vals=1;
end
g=sparse(1:n2,cols(:)',vals(:),n2,32*npercell,n2);
