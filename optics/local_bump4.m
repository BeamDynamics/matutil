function [bump,kicks]=local_bump4(resp,sk,sb,bindex)
%[bump,kicks]=LOCAL_BUMP4(resp,sk,sb,bindex) computes local bumps
%	resp: response matrix
%	sk: kicker positions
%	sb: BPM positions
%	bindex: index of selected bpm

nst=length(sk);
[npu,ncor]=size(resp);
b2=find(sk<sb(bindex),1,'last');
if isempty(b2), b2=nst; end
bump=[b2-1 b2 b2+1 b2+2];
bump(bump<=0)=bump(bump<=0)+nst;
bump(bump>nst)=bump(bump>nst)-nst;
if (ncor > nst), bump=[bump ncor]; end	% add frequency

s1=sk(bump(1));
s4=sk(bump(4));
if s1<s4                                % ignore bpms inside the bump
    ok=(sb<s1) | (sb>s4);
else
    ok=(sb<s1) & (sb>s4);
end
ok(bindex)=true;                        % but keep the selected one
ok=ok & all(isfinite(resp(:,bump)),2);	% eliminate missing BPMs

target=zeros(npu,1);                    % set desired amplitude on selected BPM
target(bindex)=1;

kicks=resp(ok,bump)\target(ok);         % compute the bump
orbit=resp(:,bump)*kicks;
kicks=kicks*1/orbit(bindex);            % renormalize to 1
