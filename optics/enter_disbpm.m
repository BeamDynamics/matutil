function disabled=enter_disbpm(ini)

disabled=ini(:)';
fprintf('%i disabled BPMs:\n',length(disabled));
for bpm=disabled,disp(sr.bpmname(bpm));end
str=input('Enter disabled bpm (like C4-1): ','s');
while ~isempty(str)
   try
      disabled=sort([disabled sr.bpmindex(str,'C%i-%i')]);
   catch err
      disp(err);
   end
   fprintf('%i disabled BPMs:\n',length(disabled));
   for bpm=disabled,disp(sr.bpmname(bpm));end
   str=input('Enter disabled bpm (like C4-1): ','s');
end
