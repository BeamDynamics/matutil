function [kicks,eff,first_bpm,nb_bpm]=bump4_coefs(resp,sb,sk,periods,flag)
%[kicks,eff,first_bpm,nb_bpm]=BUMP_COEFS(resp,sb,sk,periods,averaging)
%	computes local bumps
%
%	resp: response matrix
%	sb: BPM abscissa
%	sk: steerer abscissa
%	periods: number of periods in the machine
%	averaging: (optional) uses average over periods if true

if nargin < 5, flag=0; end
[npu ncor]=size(resp);
nb=floor(ncor/periods); % frf possibly added to real correctors
copyr=ones(1,periods);

maxpu=7;

k=(1:ncor)';
refk=[k-1 k k+1];
kicks=zeros(3,ncor);
eff=NaN*ones(maxpu,ncor);
first_bpm=zeros(ncor,1);
nb_bpm=zeros(ncor,1);
%			compute non-normalized bumps (least square)
for i=1:ncor
   [kicks(:,i) eff2 in_bpm] = local_bump(resp,sb,sk,refk(i,:));
   neff=min([maxpu;length(eff2)]);
   eff(1:neff,i)=eff2(1:neff);
   nb_bpm(i)=neff;
   if neff > 0, first_bpm(i)=in_bpm(1);end
end
%					compute average values over periods
meaneff=mean2(reshape(eff,nb*maxpu,periods),2);
avereff=reshape(meaneff*copyr,maxpu,ncor);
averkick=reshape(mean2(reshape(kicks,nb*3,periods),2)*copyr,3,ncor);

undef=find(~isfinite(eff));		% replace missing with average
eff(undef)=avereff(undef);

[fs js]=sort(-reshape(meaneff,maxpu,nb));%compute normalization values
k=reshape(js(1,:)'*copyr,1,ncor)+maxpu*(0:ncor-1);

if flag
   best=avereff(k(:));			% most efficient	AVERAGE
   kicks=averkick./(ones(3,1)*best');	% normalize kicks
   eff=avereff'./(best*ones(1,maxpu));	% normalize efficiencies
else
   best=eff(k(:));			% most efficient	INDEPENDENT
   kicks=kicks./(ones(3,1)*best');	% normalize kicks
   eff=eff'./(best*ones(1,maxpu));	% normalize efficiencies
end
