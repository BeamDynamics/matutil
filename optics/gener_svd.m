function [uu,lambda,vv]=gener_svd(resp,bpmlist,stlist,hdisp)
%GENER_SVD  
% vector 1 : RF dominant
% vector 2 : sum I dominant
%
%Response matrix   : U*S*V'
%Correction matrix : V*S-1*U'
%
% corr*resp=I

[nb,nk]=size(resp);
if nb <= 0, return, end

if nargin > 3
    orbits=rms([resp(bpmlist,stlist) hdisp(bpmlist)]);
    rmsorbits=mean(orbits(1:end-1));
    scalei=4*rmsorbits	%#ok<NOPRT> % normalize Sigma I
    scalef=  rmsorbits/orbits(end)	%#ok<NOPRT> % normalize frequency resp
    resp2=[resp,scalef*hdisp;scalei*ones(1,nk),0]; %expand response matrix
    [nb,nk]=size(resp2);
    bpmlist=[bpmlist;true];
    stlist=[stlist true];

    [u,s,v]=svd(resp2(bpmlist,stlist),'econ');

    lambda=diag(s);
    
    [dum,id]=sort(abs([v(end,:);sum(v(1:end-1,:))]),2); %#ok<ASGLU>
    u(end,:)=u(end,:)*scalei;		% rescale Sigma I in Amps
    v(end,:)=v(end,:)*scalef;		% rescale freq in dp/p

    extract=id(:,end)';
    reorder=1:length(lambda);
    reorder(extract)=[];			% SigmaI: vector 2
    reorder=[extract reorder];		% freq: vector 1
    u=u(:,reorder);
    lambda=lambda(reorder);
    v=v(:,reorder);
else
    resp2=resp;

    [u,s,v]=svd(resp2(bpmlist,stlist),'econ');

    lambda=diag(s);
end
nvects=length(lambda);
vv=zeros(nk,nvects);
vv(stlist,:)=v;
uu=zeros(nb,nvects);
uu(bpmlist,:)=u;

function a=rms(x)
a=sqrt(sum(x.*x)./size(x,1));
