function parms=load_elems(path,params,type,selected)
%MODEL=LOAD_ELEMS(PATH,PARAMS,CODE,SELECTION)
%	loads optics parameters for all elements with name "CODE" (SD23,...)
%				 or all elements with type "CODE" (QP,SX,DI,...)
%
%	PATH	directry containing BETALOG and BETAOUT
%	PARAMS	machine parameters
%	CODE	element name or code
%	SELECTION indicates the desired values. The column order is:
%
%[ 1	2	3	4	5	6	7	8	9	10   ]
%[Kl theta/2pi betax alphax  phix/2pi etax    eta'x   betaz   alphaz phiz/2pi]
%
%	Default value for SELECTION is:
%
%	[   2	      3		5	 6	8	10	   1]
%	[theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux, Kl]

if nargin < 4
    selected=[2 3 5 6 8 10 1];
end

if iscell(type)
    result=[];
    ok=[];
    for i=1:numel(type)
        try
            [result1,ok1]=load_1elem(path,params,type{i});
            result=[result;result1]; %#ok<AGROW>
            ok=[ok;ok1(:)]; %#ok<AGROW>
        catch %#ok<CTCH>
        end
    end
    [~,ii]=sort(ok);
    result=result(ii,:);
else
    [result,ok]=load_1elem(path,params,type); %#ok<NASGU>
end

copyr=ones(1,params.periods);		% reproduce periods
t=ones(size(result,1),1) * ((0:params.periods-1)/params.periods);

kl=result(:,copyr);
sbpm=result(:,2*copyr)*(1/2/pi) + t;
hbeta=result(:,3*copyr);
halpha=result(:,4*copyr);
hpsi=result(:,5*copyr)*(1/2/pi/params.nuh) + t;
heta=result(:,6*copyr);
hetap=result(:,7*copyr);
vbeta=result(:,8*copyr);
valpha=result(:,9*copyr);
vpsi=result(:,10*copyr)*(1/2/pi/params.nuv) + t;

allv=[ kl(:) sbpm(:) hbeta(:) halpha(:) hpsi(:) heta(:) hetap(:) ...
    vbeta(:) valpha(:) vpsi(:)];
parms=allv(:,selected);

function [result,ok]=load_1elem(path,params,type)
if strcmp(type,'QP')
    longtype='QUADRUPOLES';
elseif strcmp(type,'SX')
    longtype='SEXTUPOLES';
else
    longtype=0;
end

conv1=[2*pi/params.ll 1 1 2*pi 1 1 1 1 2*pi];% normalize s to [0 2 Pi]
ok=[];					% phases to [0 , 2 Pi nu]
nok=0;
nb=0;

fid=fopen(fullfile(path,'BETALOG'),'r');
disp('reading BETALOG');

buffer=fskip(fid,'NAME TYPE',1);		% get 1st line of GR output
values=sscanf(buffer,'%g');			% and returns initial values
nb=nb+1;
result(nb,:)=[NaN conv1.*values(1:9)'];

skip=0;
while 1
    buffer=fgetl(fid);
    if ~ischar(buffer)
        break					% end of file
    elseif skip
        skip=0;					% skip line after found item
    elseif ~isempty(strfind(buffer,'****'))
        break					% end of section
    else
        next=0;
        [~,~,~,n1]=sscanf(buffer(next+1:end),'%s',1);	% name
        next=next+n1;
        [~,~,~,n1]=sscanf(buffer(next+1:end),'%s',1);	% code
        next=next+n1;
        values=sscanf(buffer(next+1:end),'%g');			% values
        nb=nb+1;
        result(nb,:)=[NaN conv1.*values(1:9)'];
        if ~isempty(strfind(buffer,type))
            nok=nok+1;
            ok(nok)=nb; %#ok<AGROW>
            skip=1;
        end
    end % if
end % while 1
fclose(fid);

if ischar(longtype)				% type QP or SX
    fid=fopen(fullfile(path,'BETAOUT'),'r');
    disp('reading BETAOUT');
    skip=0;
    nb=0;
    while 1
        buffer=fgetl(fid);
        if ~ischar(buffer)
            break					% end of file
        elseif ~skip
            if ~isempty(strfind(buffer,longtype))	% look for right table
                skip=3;
            end
        elseif skip > 1
            skip=skip-1;				% skip header
        else
            next=0;
            [ord,~,~,n1]=sscanf(buffer(next+1:length(buffer)),'%f',1);
            if ~isempty(ord)
                next=next+n1;
                values=sscanf(buffer(next+1:length(buffer)),'%g');
                nb=nb+1;
                result(ok(nb),[1 2 3 5 8 10])=values(1:6)';
            else
                break				% end of list
            end
        end % if
    end % while 1
    fclose(fid);
end

result(:,5)=unwrap(result(:,5));		% unwrap phase advances
result(:,10)=unwrap(result(:,10));

result=result(ok,:);				% pick selected items
if isempty(result)
    error('LoadElems:NotFound',['??? Element "' type '" not found']);
end
