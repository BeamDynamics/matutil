function d=dirdeep(varargin)
%DIRDEEP Recursive directory
%   D = DIRDEEP(D1,D2,...)
%      returns a M-by-1 structure for file matching "D1/D2/..." with
%      wildcards allowd
%
%See also dir

[pth,nm,ext,vrs]=fileparts(varargin{1}); %#ok<NASGU>
if nargin==1
   d=dir(varargin{1});
   for i=1:length(d)
	  d(i).name=fullfile(pth,d(i).name);
   end
else
   d=[];
   fprintf('dir %s\n',varargin{1});
   d1=dir(varargin{1});
   for dd1=d1';
	  if dd1.isdir && dd1.name(1) ~= '.'
%		 d2=dirdeep(fullfile(pth,dd1.name,varargin{2}),varargin{3:end});
		 fprintf('trying %s\n',fullfile(dd1.name,varargin{2}));
		 d2=dirdeep(fullfile(dd1.name,varargin{2}),varargin{3:end});
		 d=[d;d2]; %#ok<AGROW>
	  end
   end
end
