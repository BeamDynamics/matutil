function [icor,isx]=sxcorhl2i(code,hl0,hlcor)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
corid=selcor(code);
mainname=sr.sxname(corid);
i0=cellfun(@gl2i,mainname,num2cell(hl0(corid)));
i1=cellfun(@gl2i,mainname,num2cell(hl0(corid)+hlcor));
icor=19/379*(i1(1:12)-i0(1:12));
isx=i1(13:end);
end

