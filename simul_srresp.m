function resp = simul_srresp(mach,code,slist,kick)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

if length(code) < 3, code=[code(1) '2' code(1)];end
bpmidx=findcells(mach,'BetaCode','PU');
coridx=findcells(mach,'BetaCode','LD3');
if code(1)=='H'
   if nargin < 4, kick=.4421e-3*0.08; end
   steeridx=coridx(selcor(8));
   sfield='PolynomB';
   setkick=@(mch,stidx,val) setcellstruct(mch,'PolynomB',stidx,-val,1);
elseif code(1)=='V'
   if nargin < 4, kick=.2557e-3*0.10; end
   steeridx=coridx(selcor(9));
   sfield='PolynomA';
   setkick=@(mch,stidx,val) setcellstruct(mch,'PolynomA',stidx,val,1);
end
if code(3)=='H'
   bpmplane=1;
elseif code(3)=='V'
   bpmplane=3;
end
steeridx=steeridx(slist);
orb0=findsyncorbit(mach,0.0,bpmidx);
%orbit0=findorbit4(mach,0.0,bpmidx);
steer0=getcellstruct(mach,sfield,steeridx,1);
kicks=zeros(size(steer0));
kicks(:)=kick;
resp=NaN(length(bpmidx),length(steeridx));
for steerer=1:length(steeridx)
   steerv=steer0;
   steerv(steerer)=kicks(steerer);
%  orb=findsyncorbit(setcellstruct(mach,sfield,steeridx,steerv,1),0.0,bpmidx)-orb0;
   orb=findsyncorbit(setkick(mach,steeridx,steerv),0.0,bpmidx)-orb0;
   resp(:,steerer)=orb(bpmplane,:)'./kicks(steerer);
end
