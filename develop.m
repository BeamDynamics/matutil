function develop(devpath)
%DEVELOP Change the path to the development path

global MACHFS

if nargin < 1
    devpath=fullfile(MACHFS,'laurent','dev','libraries','at');
end
    
if ~(exist('isdeployed','builtin') && isdeployed)
    currentpath=fileparts(atroot);
    atlfpath=genpath(fileparts(which('atgetparam')));
    rmpath(atlfpath);
    rmpath(genpath(fullfile(currentpath,'atmat')));
    rmpath(genpath(fullfile(currentpath,'atintegrators')));
    rmpath(genpath(fullfile(currentpath,'machine_data')));
    addpath(genpath(fullfile(devpath,'machine_data')));
    addpath(genpath(fullfile(devpath,'atintegrators')));
    addpath(genpath(fullfile(devpath,'atmat')));
    addpath(atlfpath);  % put atlf in front of at
end

end
