function [pm,varargout]=load_atoptics(atstruct,varargin)
%[PARAMS,V1,V2,...]=LOAD_OPTICS(OPTICS,CODE1,CODE2,...) Load optics parameters
%
%	OPTICS	AT ring description
%
% Accelerator Toolbox processing for load_optics

persistent hb vb        % extract H and V from all BPMs
if isempty(hb)
    hb=false(78,1);
    vb=false(78,1);
    hb(1:2:end)=true;
    vb(2:2:end)=true;
    hb([23 26 52])=[];
    vb([23 26 52])=[];
end

out=1;
nout=nargout-1;
narg=1;
nin=nargin-1;
ndata=length(atstruct)+1;
pm.ll=findspos(atstruct,ndata);
pm.alpha=mcf(atstruct);
pm.strname='?';
pm.periods=1;
if nout <= 0
    [lindata,tune,chrom]=atlinopt(atstruct,0.0,ndata); %#ok<ASGLU,*NASGU>
    pm.nuh=tune(1);
    pm.nuv=tune(2);
else
    [lindata,avebeta,avemu,avedisp,tune,chrom]=atavedata(atstruct,0.0,1:ndata); %#ok<ASGLU>
    tune=lindata(end).mu/2/pi;
    pm.nuh=tune(1);
    pm.nuv=tune(2);
    spos=cat(1,lindata.SPos)/pm.ll;
    vlave=[[0.5*(spos(1:end-1)+spos(2:end));spos(end)] ...
        avebeta ...
        cat(1,lindata.alpha) ...
        avemu./tune(ones(ndata,1),:)/2/pi ...
        avedisp ...
        NaN*ones(ndata,1) (1:ndata)'];
    
    fileorder=[2 3 8 4 9 5 10 6 7 11 12 1 0];
    locs(fileorder+1)=1:length(fileorder);
    params=locs([2 3 5 6 8 10]+1);	% default parameter list as in load_elems
    
    while ((narg<=nin) && (out<=nout))
        if isnumeric(varargin{narg})
            params=locs(varargin{narg}+1);
        else
            incode=lower(varargin{narg});
            suffix=strfind(lower(incode),'_name');
            if isempty(suffix)
                switch incode
                    case 'sx'
                        idx=getid(atstruct,incode);
                        vlave(idx,12)=atgetfieldvalues(atstruct(idx),'PolynomB',{3});
                        varargout{out}=vlave(idx,params); %#ok<*AGROW>
                    case 'qp'
                        idx=getid(atstruct,incode);
                        if exist('k','var') ~= 1
                            k=atgetfieldvalues(atstruct(idx),'PolynomB',{2});
                            l=atgetfieldvalues(atstruct(idx),'Length');
                            vlave(idx,12)=k.*l;
                        end
                        varargout{out}=vlave(idx,params);
                    case 'h_bpms'
                        varargout{out}=hb;
                    case 'v_bpms'
                        varargout{out}=vb;
                    otherwise
                        idx=getid(atstruct,incode);
                        varargout{out}=vlave(idx,params);
                end
            else
                varargout{out}=getname(atstruct,incode(1:suffix-1));
            end
            out=out+1;
        end
        narg=narg+1;
    end
    
end

    function idx=getid(atstruct,code)
        switch lower(code)
            case 'bpm'
                idx=atgetcells(atstruct,'Class','Monitor');
            case 'hbpm'
                idx=atgetcells(atstruct,'Class','Monitor');
                idx(idx)=hb;
            case 'vbpm'
                idx=atgetcells(atstruct,'Class','Monitor');
                idx(idx)=vb;
            case 'steerh'
                idx=atgetcells(atstruct,'FamName','.*ch[0-9]+');
            case 'steerv'
                idx=atgetcells(atstruct,'FamName','.*cv[0-9]+');
            case 'sx'
                idx=atgetcells(atstruct,'Class','ThinMultipole') | ...
                    atgetcells(atstruct,'Class','Sextupole');
            case 'qp'
                idx=atgetcells(atstruct,'Class','Quadrupole');
            otherwise
                idx=atgetcells(atstruct,'FamName',code);
        end
    end

    function namex=getname(atstruct,code)
        id=getid(atstruct,code);
        namex=atgetfieldvalues(atstruct(id),'FamName');
    end

end
