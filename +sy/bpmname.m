function varargout=bpmname(idx,format)
%BPMNAME	returns the name of BPM with index idx
%
%BPMNAME=BPMNAME(IDX)	returns the name of BPM with index IDX
% IDX: vector of indexes in the range [1 75] (1 is qf28)
%
%BPMNAME=BPMNAME(IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : 'SY/D-BPMLIBERA-SP/%s')
%
%[NM1,NM2...]=BPMNAME(...)      May be used to generate several names
%
%[...,KDX]=BPMNAME(...)         returns in addition the "hardware" index KDX
%
%KDX=BPMNAME(IDX,'')            returns only the "hardware" index KDX
%
%   See also BPMINDEX

if nargin < 2, format='sy/d-bpmlibera-sp/%s'; end

persistent nlist kd
if isempty(nlist)
    kd=[53:75 1:52];
    l=1:78;
    l([2 28 77])=[];
    l=l(kd);
    cl=floor((l+1)/2);
    pl(1:75)='f';
    pl(logical(rem(l+1,2)))='d';
    nlist=arrayfun(@(id) ['q' char(pl(id)) int2str(cl(id))], 1:75, 'UniformOutput', false);
end

if islogical(idx)
    idx=find(idx);
end
if isempty(format) || strcmp(format,'noname')
    varargout={};
else
    textfunc=@(id) sprintf(format,nlist{id});
    nms=arrayfun(textfunc,idx,'UniformOutput',false);
    nin=length(nms);
    if (nargout==nin) || (nargout==(nin+1))
        varargout=nms;
    else
        varargout={nms};
    end
end

nout=length(varargout);
if nargout > nout
    varargout{nout+1}=reshape(kd(idx),size(idx));
end
