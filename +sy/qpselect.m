function idx=qpselect(varargin)
%QPSELECT	find index of quadrupoles from their names
%
%IDX=QPSELECT(QPNAME)
%   QPNAME:    string, char array or cell array of strings
%
%IDX=QPSELECT(CELL,ID)
%   CELL:   cell number in 1:39
%   ID:     index in cell, in 1:2 (QF,QD)
%   CELL and ID must have the same dimensions or be a scalar
%
%   See also QPNAME, SXINDEX, BPMINDEX

if isnumeric(varargin{1})
    idx=getmask(2,39,27,varargin{:});
else
    idx=getmask(2,39,27,@bindex,varargin{:});
end

    function [cellnum,id]=bindex(bname)
        qpattern='^Q([FD])(\d{1,2})$';
        toks=regexpi(bname,qpattern,'tokens');
        if isempty(toks),error('QP:wrongName','Wrong quadrupole name'); end
        id=strfind('fd',lower(toks{1}{1}));
        cellnum=str2double(toks{1}{2});
    end
end
