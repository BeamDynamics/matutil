function setbpms(varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[~,fpath,~]=getmachenv(varargin{:});
data=load(fullfile(fpath,'matlog'));
tango.Attribute('sy/svd-orbitcor/h/DisabledBPMs').write(data.x_no_h_bpms');
tango.Attribute('sy/svd-orbitcor/v/DisabledBPMs').write(data.x_no_v_bpms');
end
