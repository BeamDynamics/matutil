function [h_resp,v_resp,h_rmot,v_rmot]=autocor_model(varargin)
%AUTOCOR_MODEL Creates data files for the autocor device server
%
%[RESPH,RESPV]=SY.AUTOCOR_MODEL(PATH)
%PATH:  Data directory (Default: pwd)
%
%[RESPH,RESPV]=SY.AUTOCOR_MODEL(..,'exp',...)
%   Use a measured response matrix
%
%[RESPH,RESPV]=SY.AUTOCOR_MODEL(..,'small',...)
%   selects a subset of BPMs for each plance

args={pwd};
[exper,options]=getflag(varargin,'exp');
[small,options]=getflag(options,'small');
args(1:length(options))=options;
pth=args{1};

[coefh,coefv]=load_corcalib('sy');

atmodel=sy.model(pth);
[bpmidx,sthidx,stvidx]=atmodel.select('bpm','steerh','steerv');
[bpm,steerh,steerv,qp]=atmodel.get('bpm','steerh','steerv','qp');
qstrength=atmodel.get(1,'qp');
nb=size(bpm,1);
nsth=size(steerh,1);
nstv=size(steerv,1);

if small                                            % For old server
    [h_bpms,v_bpms]=atmodel.get('h_bpms','v_bpms');	% For old server
else                                                % For old server
    h_bpms=true(nb,1);                              % For old server
    v_bpms=true(nb,1);                              % For old server
end                                                 % For old server

if ~exist('./alpha','file')
    savetext('alpha','%.4e',atmodel.alpha);
end

fprintf('Computing horizontal response\n');
h_resp=atorm(atmodel.ring,'h',sthidx,bpmidx).*coefh;
fprintf('Computing vertical response\n');
v_resp=atorm(atmodel.ring,'v',stvidx,bpmidx).*coefv;
fprintf('Computing frequency response\n');
dct=4.e-4;
forb=findsyncorbit(atmodel.ring,dct,bpmidx);
h_fresp=-forb(1,:)'/dct.*atmodel.ll*atmodel.ll/352/2.997924e8;
if exper
    [hr,vr,b_ok]=sy.load_resp(fullfile(pth,'expresp.mat'));
    sh_ok=true(1,nsth);
    sv_ok=true(1,nstv);
    h_resp(b_ok,:)=hr(b_ok,:);
    v_resp(b_ok,:)=vr(b_ok,:);
    matmode='measured';
else
    b_ok=true(nb,1);
    sh_ok=true(1,nsth);
    sv_ok=true(1,nstv);
    matmode='theoretical';
end

hv_bpms=[bpm(:,1:2) 2*pi*bpm(:,3) bpm(:,4:5) 2*pi*bpm(:,6)];
bpm_name=sy.bpmname((1:nb)');
store_bpm('bpms',hv_bpms,bpm_name);

h_steerers = [steerh(:,1:2) 2*pi*steerh(:,3) coefh*ones(nsth,1)];
v_steerers = [steerv(:,[1 5]) 2*pi*steerv(:,6) coefv*ones(nstv,1)];
steerh_name=sy.steername((1:nsth)','h');
steerv_name=sy.steername((1:nstv)','v');
store_steerer('h_steerers',h_steerers,steerh_name);
store_steerer('v_steerers',v_steerers,steerv_name);

[b,f,pu,npu]=bump_coefs(h_resp,bpm(:,1),steerh(:,1),1);
store_bump('h_bumps',[b' pu-1 npu f],steerh_name);

[b,f,pu,npu]=bump_coefs(v_resp,bpm(:,1),steerv(:,1),1);
store_bump('v_bumps',[b' pu-1 npu f],steerv_name);

[ih,h_rmot]=respmot(atmodel,'qh',{'QF30','QF37','QF39','QF4','QF10','QF17','QF22','QF26'});
[iv,v_rmot]=respmot(atmodel,'qv',{'QD29','QD3','QD12','QD18'});
h_motors = [qp(ih,1:2) 2*pi*qp(ih,3) qstrength(ih)];
v_motors = [qp(iv,[1 5]) 2*pi*qp(iv,6) -qstrength(iv)];
motorh_name=sy.qpname(ih','sy/d-mot/%s%d');
motorv_name=sy.qpname(iv','sy/d-mot/%s%d');
store_steerer('h_motors',h_motors,motorh_name);
store_steerer('v_motors',v_motors,motorv_name);

[b,f,pu,npu]=bump_coefs(h_rmot,bpm(:,1),qp(ih,1),1);
store_bump('h_motorbumps',[b' pu-1 npu f],motorh_name);

[b,f,pu,npu]=bump_coefs(v_rmot,bpm(:,1),qp(iv,1),1);
store_bump('v_motorbumps',[b' pu-1 npu f],motorv_name);

dlmwrite('h_resp.csv',[h_resp,h_fresp]);
dlmwrite('v_resp.csv',v_resp);
dlmwrite('h_motresp.csv',[h_rmot,h_fresp]);
dlmwrite('v_motresp.csv',v_rmot);
dlmwrite('h_allresp.csv',[h_resp,h_rmot,h_fresp;zeros(size(h_resp)),h_rmot,h_fresp]);
dlmwrite('v_allresp.csv',[v_resp,v_rmot;zeros(size(v_resp)),v_rmot]);

b_ok=enter_bpm(b_ok);                                       % For old server
store_svd('h_svd',h_resp,h_bpms&b_ok,sh_ok,h_fresp);        % For old server
store_svd('v_svd',v_resp,v_bpms&b_ok,sv_ok);                % For old server
store_variable('matlog','disabled_bpms',sy.bpmname(~b_ok));	% For old server
store_variable('matlog','x_no_h_bpms',sy.bpmname(~(h_bpms&b_ok)));	% For old server
store_variable('matlog','x_no_v_bpms',sy.bpmname(~(v_bpms&b_ok)));	% For old server
store_variable('matlog','resp',matmode);

    function ok=enter_bpm(ok)
        fprintf('%i disabled BPMs:\n',sum(~ok));
        disp(sr.bpmname(~ok'));
        str=input('Enter disabled bpms (comma separated list, ex. "qf13,qd21"): ','s');
        if ~isempty(str)
            ok=ok & ~sy.bpmselect(strsplit(str,','))';
        end
    end

    function [id,resp]=respmot(atmodel,rcode,qplist)
        id=sy.qpselect(qplist);
        r=respmat(atmodel,rcode);
        resp=r(:,id);
    end

end

