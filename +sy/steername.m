function varargout = steername(idx,plane,format)
%STEERNAME	returns the name of Steerer with index idx
%
%STEERNAME=STEERNAME(IDX,PLANE)		returns the name of steerer with index IDX
% IDX:	vector of indexes in the range 1:39 (1 is C[H|V]28)
%       logical array of length 39
%
%STEERNAME=STEERNAME(IDX,PLANE,FORMAT)	uses FORMAT to generate the name
%
%[NM1,NM2...]=STEERNAME(...)		May be used to generate several names
%
%   See also SY.STEERINDEX, SY.BPMNAME

persistent cratename crateidx
if isempty(cratename)
    cratename={'tl1sy/ps-c1/c%s%d','sy/ps-c2/c%s%d','sy/ps-c3/c%s%d',...
        'sy/ps-c4/c%s%d','sy/ps-c5/c%s%d','sy/ps-c6/c%s%d'};
    crateidx=[5,5,5,5,5,5,5,5,5,5,5,5,5,5,...
        4,4,4,4,4,4,4,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2;...
        6,6,6,6,6,6,6,6,6,6,6,6,6,6,...
        4,4,4,4,4,4,1,1,1,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3];
end

[pl,plid]=selectplane(plane,{'h';'v'},{1;2});
if nargin >= 3
    textfunc=@(idn) sprintf(format,pl,idn);
else
    textfunc=@(idn) sprintf(cratename{crateidx(plid,idn)},pl,idn);
end
if islogical(idx)
    idx=find(idx);
end
idn=idx+27;
overflow=idn>39;
idn(overflow)=idn(overflow)-39;
nms=arrayfun(textfunc,idn,'UniformOutput',false);
if nargout==length(nms)
    varargout=nms;
else
    varargout={nms};
end
end

