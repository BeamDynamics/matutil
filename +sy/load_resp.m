function [rx,rz,bok]=load_resp(fname,tim)
%LOAD_SYRSP		Load Booster response matrix
%RX and RZ are the horizontal (vertical) response matrices suitable for the
%syco server. 
% FNAME is the .mat file containing the matrices finalmath and finalmatv, 
%corresponding to the row measurement. the first
%line of these matrix is a vector which first element gives the current
%injected in the steerers for the measurement and the following values are
%the times at which the measurements have been done.
%TIM is the time index for which the matrix is computed.


if nargin < 2, tim=1; end
rmat=load(fname);
rx=rmat.finalmath((tim-1)*75+2:tim*75+1,:);
fprintf('H: time %d, current %g A\n', rmat.finalmath(1,tim+1), rmat.finalmath(1,1));
fprintf('V: time %d, current %g A\n', rmat.finalmatv(1,tim+1), rmat.finalmatv(1,1));
rz=rmat.finalmatv((tim-1)*75+2:tim*75+1,:);
rx=circshift(rx,[0,-27]);
rz=circshift(rz,[0,-27]);
bok=all(isfinite([rx rz]),2);
% disp('Missing BPMs:');
% disp(sy.bpmname(~bok'));
end
