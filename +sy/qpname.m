function varargout=qpname(idx,format)
%QPNAME	returns the names of quadrupoles with index idx
%
%QPNAME=QPNAME(IDX)	returns the names of quadrupoles with indexes IDX
% IDX: vector of indexes in the range 1:78 (1 is QF28)
%       logical array of length 78
%       or n x 2 matrix in the form [cell index] with
%       cell in 1:39, index in 1:2
%
%QPNAME=QPNAME(IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : '%s%d')
%
%
%   See also QPSELECT, SXNAME, BPMNAME

if nargin < 2, format='%s%d'; end

qfoc={'qf','qd'};

[~,celnum,id]=sy.cellnum(2,idx);
cellfunc=@(c,i) sprintf(format,qfoc{i},c);
nms=arrayfun(cellfunc,celnum,id,'UniformOutput',false);
if nargout==length(nms)
    varargout=nms;
else
    varargout={nms};
end
end
