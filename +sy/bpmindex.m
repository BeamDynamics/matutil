function [idx,kdx]=bpmindex(varargin)
%BPMINDEX	find index of BPM from its name
%
%IDX=BPMINDEX(BPMBAME)
%   BPMNAME:	string, char array or cell array of strings
%
%IDX=BPMINDEX(KDX)
%   KDX:        "hardware" index of the BPM
%
%[IDX,KDX]=BPMINDEX(...)	returns in addition the "hardware" index
%
%   See also BPMNAME

persistent kexpand kreduce
if isempty(kexpand)
    kexpand=1:78;
    kexpand([2 28 77])=[];
    kreduce=zeros(78,1);
    kreduce(kexpand)=1:75;
end

if isnumeric(varargin{1})
    if isvector(varargin{1}) && length(varargin)<2
        [~,kd78]=getindex(2,39,27,kexpand(varargin{1}));
    else
        [~,kd78]=getindex(2,39,27,varargin{:});
    end
else
    [~,kd78]=getindex(2,39,27,@bindex,varargin{:});
end
kdx=reshape(kreduce(kd78),size(kd78));
idx=kdx-52 + 75*(kdx<=52);

    function [cellnum,id]=bindex(name)
        bpattern='^(?:(tango:)?sy/d-bpm(libera)?(-sp)?/)?q([df])(\d{1,2})$';
        toks=regexpi(name,bpattern,'tokens');
        if isempty(toks),error('BPM:wrongName','Wrong BPM name'); end
        cellnum=str2double(toks{1}{2});
        id=double(toks{1}{1}=='d')+1;
    end
end
