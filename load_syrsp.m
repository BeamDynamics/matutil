function [rx,rz]=load_syrsp(fname,tim)
%LOAD_SYRSP		Load Booster response matrix
%
%[RX,RZ]=LOAD_SYRSP(FILENAME,TIM)	response matrices in m/A
%
%		TIM [1..6] is the time index (default 1)
%
if nargin < 2, tim=1; end
[pth,fnm]=fileparts(fname);
dname=fullfile(pth,'expdata');
load(fname);
try
   s=load(dname);
   hcurrent=s.hcurrent;
   vcurrent=s.vcurrent;
catch
   hcurrent=input('H kick (A): ');
   vcurrent=input('V kick (A): ');
   try
      save(dname,'hcurrent','vcurrent');
   end
end
rx=(xpack(x,tim)-xpack(xref,tim))/hcurrent;
rz=(zpack(z,tim)-zpack(zref,tim))/vcurrent;

function out=xpack(in,tim)
out=cat(1,in{[55:2:78 1:2:54]});
out=reshape(out(:,tim),75,39);

function out=zpack(in,tim)
out=cat(1,in{[56:2:78 2:2:54]});
out=reshape(out(:,tim),75,39);
