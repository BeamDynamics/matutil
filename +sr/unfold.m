function a=unfold(r,dim)
%A=SRUNFOLD(B)			reshapes data for SR symmetry
%
%	R(n,32) -> A(32*n,1)

nd=ndims(r);
if nd==2
    n=size(r,1);
    a=reshape([r(:,1:2:end);r(n:-1:1,2:2:end)],32*n,1);
elseif nargin<2 || dim==1
    sz=size(r);
    r1=reshape(r,sz(1),sz(2),[]);
    a=reshape(cat(1,r1(:,1:2:end,:),r1(sz(1):-1:1,2:2:end,:)),[sz(1)*sz(2) sz(3:end)]);
else
    ord1=[dim dim+1 1:dim-1 dim+2:nd];
    ord2=[dim 1:dim-1 dim+1:nd-1];
    a=ipermute(sr.unfold(permute(r,ord1)),ord2);
end
end
