function [idx,plane]=sxselect(varargin)
%SXSELECT	find index of sextupole from its name
%
%MASK=SXSELECT(SXNAME)
%   SXNAME:    string, char array or cell array of strings
%
%MASK=SXSELECT(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:7
%   CELL and ID must have the same dimensions or be a scalar
%
%[MASK,PLANE]=SXSELECT(...)	returns in addition the "plane":
%           h,v,s,n for correctors
%
%   See also SR.SXNAME, SR.SXSELECT, SR.BPMSELECT, SR.QPSELECT, SR.CORSELECT

if isnumeric(varargin{1})
    idx=getmask(7,32,3,varargin{:});
else
    idx=getmask(7,32,3,@bindex,varargin{:});
end

    function [celnum,id]=bindex(bname)
        [celnum,id,plane]=sr.sxdecode(bname);
    end
end
