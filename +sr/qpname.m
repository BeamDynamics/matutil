function varargout=qpname(idx,format)
%QPNAME	returns the names of quadrupoles with index idx
%
%QPNAME=QPNAME(IDX)	returns the names of quadrupoles with indexes IDX
% IDX: vector of indexes in the range 1:256 (1 is QF2/C4)
%       logical array of length 256
%       or n x 2 matrix in the form [cell index] with
%       cell in 1:32, index in 1:8
%
%QPNAME=QPNAME(IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : 'SR/PS-%s/%s')
%
%
%   See also QPSELECT, SXNAME, BPMNAME

if nargin < 2, format='SR/PS-%s/%s'; end

qname={'QF2','QD3','QD4','QF5','QF5','QD4','QD6','QF7'};
qext={'','','-1','-1','-2','-2','',''};

[~,celnum,id,rid]=sr.cellnum(8,idx);
cellfunc=@(cnum,id) sprintf('C%i%s',cnum,qext{id});
textfunc=@(cnum,id,rid) sprintf(format,qname{rid},cellfunc(cnum,id));
nms=arrayfun(textfunc,celnum,id,rid,'UniformOutput',false);
if nargout==length(nms)
    varargout=nms;
else
    varargout={nms};
end
end
