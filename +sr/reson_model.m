function [qexc32,kexc32,sexc12]=reson_model(varargin)
%RESON_MODEL	produces resonance correction files for skmag
%	[qexc,kexc,sexc]=reson_model()
%
%RESON_MODEL()               takes the lattice from the current directory
%
%RESON_MODEL('opticsname')   takes the lattice from
%           $APPHOME/sr/optics/settings/opticsname/betamodel.mat
%RESON_MODEL(path)           takes the lattice from
%           path or path/betamodel.mat or path/betamodel.str
%RESON_MODEL(AT)             Uses the given AT structure

args={pwd};
args(1:length(varargin))=varargin;

atmodel=sr.model(args{:});

qpok=true(1,32);
qpok(1)=false;      % Disable the N4/C4 quadrupole

qexc32=sr.resonquad(atmodel,qpok);
kexc32=sr.resonskew(atmodel);
sexc12=sr.resonsext(atmodel);
end
