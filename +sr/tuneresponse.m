function [resp,devs]=tuneresponse(atmodel)

% get initial tunes
nuh=atmodel.nuh;
nuv=atmodel.nuv;

tunemode='QF7QD6';
%tunemode='Matching';

Mat=[];

Q0=[nuh nuv];

% change horizontal tune
DQ=.001;

Q1=Q0+[DQ 0.0];

DI=getdeltaI(Q1);
Mat(:,1)=DI/DQ;

% change vertical tune

Q1=Q0+[0.0 DQ];

DI=getdeltaI(Q1);
Mat(:,2)=DI/DQ;

% select changed gradients and get device familiy names.

[famDeviceInd,devs]=famdevs(sr.qpname(1:length(Mat)));

resp=Mat(famDeviceInd,:);

fff=fopen('tunemat.csv','w+');
for iq=1:length(devs)
    fprintf(fff,'%s,%2.10f,%2.10f,\n',devs{iq},resp(iq,1),resp(iq,2));
end
fclose(fff);


function DI=getdeltaI(Q1)
cligth=299792458;
latnew=sr.model(atmodel);
latnew.retune(Q1,tunemode);
latnew.retune(Q1,tunemode); %iterate to assure convergence
latnew.retune(Q1,tunemode);
GL0=atmodel.get(1,'qp');
GLf=latnew.get(1,'qp');
quadDevice=sr.qpname(1:length(GL0));

quadDevice=fixquaddev(quadDevice);

Iq0 = cellfun(@(d,g)gl2i(d,g),quadDevice',num2cell(GL0*atmodel.ring{1}.Energy/cligth),'un',1);
Iqf = cellfun(@(d,g)gl2i(d,g),quadDevice',num2cell(GLf*atmodel.ring{1}.Energy/cligth),'un',1);

DI= Iqf - Iq0;
end


end

function quadDevice=fixquaddev(quadDevice)
try quadDevice{cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QD6/C22'))}='SR/PS-QD6HG/C22'; end
try quadDevice{cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QF7/C22'))}='SR/PS-QF7HG/C22'; end
try quadDevice{cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QF7/C23'))}='SR/PS-QF7HG/C23'; end
try quadDevice{cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QD6/C23'))}='SR/PS-QD6HG/C23'; end
end

function [famDeviceInd,famDevice]=famdevs(quadDevice)

famDeviceInd(1)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QF2/C4')),1,'first');
famDevice{1}=[quadDevice{famDeviceInd(1)}(1:9) '/0'];
famDeviceInd(2)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QD3/C4')),1,'first');
famDevice{2}=[quadDevice{famDeviceInd(2)}(1:9) '/0'];
famDeviceInd(3)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QD4/C4')),1,'first');
famDevice{3}=[quadDevice{famDeviceInd(3)}(1:9) '/0'];
famDeviceInd(4)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QF5/C4')),1,'first');
famDevice{4}=[quadDevice{famDeviceInd(4)}(1:9) '/0'];
famDeviceInd(5)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QD6/C4')),1,'first');
famDevice{5}=[quadDevice{famDeviceInd(5)}(1:9) '/0'];
famDeviceInd(6)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QF7/C4')),1,'first');
famDevice{6}=[quadDevice{famDeviceInd(6)}(1:9) '/0'];

famDeviceInd(7)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QD4/C22-2')),1,'first');
famDevice{7}=[quadDevice{famDeviceInd(7)}(1:9) '/C22'];
famDeviceInd(8)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QF5/C22-2')),1,'first');
famDevice{8}=[quadDevice{famDeviceInd(8)}(1:9) '/C22'];
famDeviceInd(9)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QD6/C22')),1,'first');
famDevice{9}=[quadDevice{famDeviceInd(9)}(1:9) 'HG/C22'];
famDeviceInd(10)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QF7/C22')),1,'first');
famDevice{10}=[quadDevice{famDeviceInd(10)}(1:9) 'HG/C22'];

famDeviceInd(11)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QD6/C23')),1,'first');
famDevice{11}=[quadDevice{famDeviceInd(11)}(1:9) 'HG/C23'];
famDeviceInd(12)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QF7/C23')),1,'first');
famDevice{12}=[quadDevice{famDeviceInd(12)}(1:9) 'HG/C23'];
famDeviceInd(13)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QD4/C23-1')),1,'first');
famDevice{13}=[quadDevice{famDeviceInd(13)}(1:9) '/C23'];
famDeviceInd(14)=find(cellfun(@(a)not(isempty(a)),strfind(quadDevice,'SR/PS-QF5/C23-1')),1,'first');
famDevice{14}=[quadDevice{famDeviceInd(14)}(1:9) '/C23'];

end