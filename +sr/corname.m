function varargout=corname(code,idx,format)
%CORNAME	returns the name of a corrector with index idx
%
%NAME=CORNAME(CODE,IDX)	returns the names of sextupoles with indexes IDX
%   CODE:   1: 16 quad correctors
%           2: 16 skew quad correctors
%           3: 12 sextupole correctors
%           4:  8 horizontal correctors
%           5:  8 vertical correctors
%           6: 32 quad correctors      106: without qp-n4/c4
%           7: 32 skew correctors
%           8: all H steerers
%           9: all V steerers
%           10: 64 skew correctors      110: with qp-s4/c4
%           11: 12 sextupole corrector + 8 indep. sextupoles
%	IDX: vector of indexes in the range 1:ncor
%        logical array of length ncor
%
%NAME=CORNAME(CODE,IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : 'SR/PS-S%i/C%i')
%
%[NM1,NM2...]=CORNAME(...) May be used to generate several names
%
%   See also SR.CORINDEX, SR.CORSELECT, SR.BPMNAME, SR.STEERNAME

persistent corformat corconv

if isempty(corformat)
corconv=[1,2,3,4,5,1,2,4,5,2,3];
corformat={'SR/QP-N%d/C%d','SR/QP-S%d/C%d','SR/SX-C%d/C%d',...
    'SR/ST-H%d/C%d','SR/ST-V%d/C%d'};
end
sel=selcor(code);
sxidx=reshape(sel(idx),size(idx));
if nargin < 3, format=corformat{corconv(mod(code,100))};end
[varargout{1:nargout}]=sr.sxname(sxidx,format);
end
