function [mask,plane]=steerselect(varargin)
%STEERSELECT	find index of Steerer with name steername
%
%MASK=STEERSELECT(STEERNAME)
%   STEERNAME:    string, char array or cell array of strings
%
%MASK=STEERSELECT(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:7
%   CELL and ID must have the same dimensions or be a scalar
%
%[MASK,PLANE]=STEERSELECT(...)
%   Returns the plane of the last steerer in the list
%
%   See also STEERNAME, STEERINDEX

if isnumeric(varargin{1})
    mask=getmask(3,32,3,varargin{:});
else
    mask=getmask(3,32,3,@bindex,varargin{:});
end

    function [celnum,id]=bindex(bname)
        [celnum,id,plane]=sr.steerdecode(bname);
    end
end
