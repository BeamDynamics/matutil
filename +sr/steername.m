function varargout=steername(varargin)
%STEERNAME	returns the name of Steerer with index idx
%
%STEERNAME=STEERNAME(IDX)		returns the name of steerer with index IDX
% IDX: vector of indexes in the range 1:96 (1 is S6/C4)
%       logical array of length 96
%       or n x 2 matrix in the form [cell index] with
%       cell in 1:32, index in 1:3
%
%STEERNAME=STEERNAME(IDX,FORMAT)	uses FORMAT to generate the name
%					(default : 'S%i/C%i')
%
%[NM1,NM2...]=STEERNAME(...)		May be used to generate several names
%
%   See also SR.STEERINDEX, SR.QPNAME, SR.BPMNAME, SR.SXNAME

charargs=cellfun(@ischar,varargin);
args=varargin(~charargs);
format={'S%i/C%i'};
format(1:sum(charargs))=varargin(charargs);

sid=[6,19,22];

[~,celnum,~,rid]=sr.cellnum(length(sid),args{:});
textfunc=@(cnum,rid) sprintf(format{1},sid(rid),cnum);
nms=arrayfun(textfunc,celnum,rid,'UniformOutput',false);
if nargout==length(nms)
    varargout=nms;
else
    varargout={nms};
end
end
