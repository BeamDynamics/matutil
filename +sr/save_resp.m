function save_resp(filename,current,response,plane,steerlist)
%SAVE_RESP Save a response matrix
%
%SAVE_RESP(DIRNAME,CURRENT,RESPONSE,PLANE,STEERLIST)
%DIRNAME:   Directory where ot store the data
%CURRENT:	list of steerer currents [A]
%RESPONSE:	BPM response [m]
%PLANE:     'H', 'V', 'H2V', 'V2H'
%STEERLIST: list of steerers (1..96)
%
[nb,nk]=size(resp); %#ok<ASGLU>
if nargin < 5, steerlist=1:nk; end
if isdir(filename)
    encodef=@(idx) sr.steername(idx,['tango:SR/ST-' upper(plane(1)) '%i/C%i/CURRENT']);
    for st=steerlist
        fn=fullfile(filename,sprintf('steer%s%02d',upper(plane),st));
        sr.save_steerresp(fn,st,current(st),response(:,st),encodef);
    end
else
end
end
