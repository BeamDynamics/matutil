function varargout=iaxname(varargin)
%IAXNAME	returns the name of IAX with index idx
%
%NAME=IAXNAME(IDX)	returns the name of BPM with index IDX
%  IDX:     vector of indexes in the range 1:32 (1 is C4)
%
%NAME=IAXNAME(CELL,NUM)
%NAME=IAXNAME([CELL NUM])
%  CELL:	Cell number (1:32)
%  NUM:     1
%
%NAME=IAXNAME(...,FORMAT)	uses FORMAT to generate the name
%                           (default : 'SR/D-EMIT/C%i')
%
%[NM1,NM2...]=IAXNAME(...)	May be used to generate several names
%
%[...,KDX]=IAXNAME(...)     returns in addition the "hardware" index KDX
%
%KDX=IAXNAME(IDX,'')        returns only the "hardware" index KDX
%
%   See also IAXINDEX, QPNAME, SXNAME

charargs=cellfun(@ischar,varargin);
args=varargin(~charargs);
format={'SR/D-EMIT/C%i'};
format(1:sum(charargs))=varargin(charargs);

[celnum,~,kdx]=getcellnum(1,32,3,args{:});
if isempty(format) || strcmp(format,'noname')
    varargout={};
else
    textfunc=@(celnum) sprintf(format{1},celnum);
    nms=arrayfun(textfunc,celnum,'UniformOutput',false);
    nin=length(nms);
    if (nargout==nin) || (nargout==(nin+1))
        varargout=nms;
    else
        varargout={nms};
    end
end

nout=length(varargout);
if nargout > nout
    varargout{nout+1}=kdx;
end
