function [strength,devlist] = read_mag(varargin)
%SR.READ_MAG    Read the values of magnet families from the control system
%
%CURRENT=SR.READ_MAG()
%   Return the family currents [A]
%
%CURRENT=SR.READ_MAG(MAGLIST)
%   Return the family currents for the selected magnets [A]
%
%MAGLIST:   List of magnet family names
%
%STRENGTH=SR.READ_MAG(...,'Energy',energy)
%STRENGTH=SR.READ_MAG(...,'BRo',bro)
%   Return the family strengths [rad, m^-1, m^-2]
%
%[...,DEVNAME]=SR.READ_MAG(...)
%   Return in addition the device names

[energy,args]=getoption(varargin,'Energy',NaN);
[bro,args]=getoption(args,'BRo',energy/2.997924e8);
devlist=getargs(args,{sr.magname()});
strength=cellfun(@getcurrent,devlist);
if isfinite(bro)
    strength=cellfun(@i2gl,devlist,num2cell(strength))/bro;
end

    function c=getcurrent(magname)
        toks=regexpi(magname,'^(?:tango:)?(sr/ps-\w+/[^/]+)(?:/current)?$','tokens');
        c=tango.Attribute([toks{1}{1} '/Current']).setpoint('',NaN);
    end
end
