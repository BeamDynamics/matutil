function [dh,dv]=load_disp(filename,pm)
%LOAD_DISP2    get measured dispersion
%
%[DX,DZ]=LOAD_DISP(FILENAME)
%
%FILENAME:  orbit reference file name or
%           directory containing an orbit file named "dispersion"
%
%[DX,DZ]=LOAD_DISP(DIRNAME,PARAMS)
%
%DIRNAME:   directory containing orbit files steerF2H00 and steerF2V00
%PARAMS:    structure containing fields "alpha" and "ll"
%
%DX: horizontal dispersion [m]
%DZ: vertical dispersion [m]
%
%See also: SR.LOAD_FRESP

if isdir(filename)
    if exist(fullfile(filename, 'dispersion'),'file')
        [dh,dv]=load_orbit(fullfile(filename, 'dispersion'));
    else
        cnst=-992*2.997924E8*pm.alpha/pm.ll;
        [oh,ov]=sr.load_fresp(filename);
        dh=cnst*oh;
        dv=cnst*ov;
    end
else
    [dh,dv]=load_orbit(filename);
end
end
