function [quadvals,skewvals,sextvals]=reset_base(datadir)
%SR.RESET_BASE	Re-create base files for resonance correction
%
%[QUADVALS,SKEWVALS,SEXTVALS]=RESET_BASE(DATADIR)

global APPHOME

quadvals=sr.setcorbase(fullfile(datadir, 'cornew.dat'),'sr/qp-n/all',...
    'sr/reson-quad/m2_n0_p73','base_quad2');
skewvals=sr.setcorbase(fullfile(datadir, 'skewnew.dat'),'sr/qp-s/all',...
    'sr/reson-skew/m1_n1_p50','base_skew2');
% sextvals=setbase(fullfile(datadir, 'sextnew.dat'),'sr/sx-c/all',...
%     'sr/reson-sext/m3_n0_p109','base_sext2');
[devvals,devnames]=load_setting('binaryfile',fullfile(APPHOME,'sr','optics','settings','theory','base_sext2'));
psnames=tango.Device('sr/sx-c/all').CorrectorNames.read;
sextvals=selectfrom(@(a,b) any(strcmpi(b,{a,['tango:' a '/CURRENT']})),...
    psnames,devnames,devvals);
resondev=tango.Device('sr/reson-sext/m3_n0_p109');
resondev.Offset=sextvals;
end
