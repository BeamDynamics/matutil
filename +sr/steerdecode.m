function [celnum,id,plane]=steerdecode(bname)

qpattern='^(?:(tango:)?sr/)?((ps|m|qp|sx|st)-)?([chvsn])(\d{1,2})/c(\d{1,2})(?:/\w+)?$';
toks=regexpi(bname,qpattern,'tokens');
if isempty(toks),error('SX:wrongName','Wrong steerer name'); end
plane=upper([toks{1}{1:2}]);
magnum=str2double(toks{1}{3});
celnum=str2double(toks{1}{4});
oddcell=logical(bitget(uint8(celnum),1));
id=find(magnum==[6,19,22]);
if isempty(id), error('SX:wrongName','Wrong sextupole name'); end
if oddcell, id=4-id; end
end
