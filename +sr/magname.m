function nms = magname(format)
%SR.MAGNAME	Get the list a SR magnet power supplies
%
%NAMES=SR.MAGNAME()         Return the list of taco+tango power supplies
%
%NAMES=SR.MAGNAME(FORMAT)   Use the given format
%                           (Default: Taco syntax)

persistent tacolist tangolist
if isempty(tacolist)        % Flex 1 missing: not controlled, no calibration
    tacolist={'sr/ps-d/0';'sr/ps-qf2/0';'sr/ps-qd3/0';'sr/ps-qd4/0';...
        'sr/ps-qf5/0';'sr/ps-qd6/0';'sr/ps-qf7/0';...
        'sr/ps-s4/0';'sr/ps-s6/0';'sr/ps-s13/0';'sr/ps-s19/0';'sr/ps-s20/0';'sr/ps-s22/0';'sr/ps-s24/0'};
    tangolist={'sr/ps-s4/c15';'sr/ps-s4/c16';'sr/ps-s4/c29';'sr/ps-s4/c30';'sr/ps-s22/c22-23';...
        'sr/ps-qd6hg/c22';'sr/ps-qf7hg/c22';'sr/ps-s24/c22';'sr/ps-qd4/c22';'sr/ps-qf5/c22';...
        'sr/ps-qd6hg/c23';'sr/ps-qf7hg/c23';'sr/ps-s24/c23';'sr/ps-qd4/c23';'sr/ps-qf5/c23'};
end

tacoformat='%s';
tangoformat='tango:%s/Current';
if nargin >= 1, [tacoformat,tangoformat]=deal(format); end
nms=[cellfun(@(n) sprintf(tacoformat,n),tacolist,'UniformOutput',false);...
    cellfun(@(n) sprintf(tangoformat,n),tangolist,'UniformOutput',false)];

end

