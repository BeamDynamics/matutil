function model = getmodel(varargin)
%GETMODEL  Object giving access to the optical parameters
%
%GETMODEL()               takes the lattice from
%           $APPHOME/sr/optics/settings/theory/betamodel.mat
%GETMODEL('opticsname')   takes the lattice from
%           $APPHOME/sr/optics/settings/opticsname/betamodel.mat
%GETMODEL(path)           takes the lattice from
%           path or path/betamodel.mat or path/betamodel.str
%GETMODEL(AT)             Uses the given AT structure

persistent srm

if isempty(srm) || nargin > 0
    srm=sr.model(varargin{:});
end
model=srm;
end
