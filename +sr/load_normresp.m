function [resp,algos]=load_normresp(filename,varargin)
%LOAD_RESP    get measured response matrix
%
%RESP=LOAD_NORMRESP(DIRNAME,PLANE,STEERLIST)
%
%DIRNAME:   Directory containing measured data or
%           file containing a full response matrix
%STEERLIST: list of steerers (1..96)
%PLANE:     'H', 'V', 'H2V', 'V2H'
%
%RESP:      normalized BPM response [m]
%
%       The steerer calibration is taken from the default directory
%       $(APPHOME)/sr/optics/settings/theory
%
%RESP=LOAD_NORMRESP(DIRNAME,OPTICS,PLANE,STEERLIST)
%       takes the steerer calibration from the specified optics dir.
%
%OPTIC may be one of the following:
%
%- 'opticsname'
%       path is taken as $(APPHOME)/sr/optics/settings/opticsname
%- '/machfs/appdata/sr/optics/settings/opticsname':
%       full path of optics directory
%
%RESP=LOAD_NORMRESP(DIRNAME,...,'m/A',...)
%           returns the response in m/A, without using the steerer
%           calibration
%
%RESP=LOAD_NORMRESP(...,'Zeros',action,...)
%RESP=LOAD_NORMRESP(...,'Fixed',action,...)
%RESP=LOAD_NORMRESP(...,'TooLarge',action,...)
%           Define the behaviour for Zero readings, Constant readings and
%           abnormal values (default: keep)
%           	action>0: keep the values
%               action=0: ask
%               action<0: reject the values
%
%   See also LOAD_RESP.

[~,options]=getmodelpath(varargin{:});
n1=length(varargin)-length(options);
[iscur,options]=getflag(options,'m/A');

[resp,current,algos,steerlist]=sr.load_resp(filename,options{:});
[nb,nk]=size(resp); %#ok<ASGLU>

if iscur
    resp=resp./current(ones(nb,1),:);
else
    calinfo=selectplane(options{1},{8,9,8,9});
    kcalib=load_corcalib('sr',varargin{1:n1},calinfo);
    kick=kcalib(steerlist).*current;
    resp=resp./kick(ones(nb,1),:);
end
end
