function idx=qpselect(varargin)
%QPSELECT	find index of quadrupoles from their names
%
%MASK=QPSELECT(QPNAME)
%   BPMNAME:    string, char array or cell array of strings
%
%MASK=QPSELECT(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:8
%   CELL and ID must have the same dimensions or be a scalar
%
%   See also QPNAME, SXSELECT, BPMINDEX

if isnumeric(varargin{1})
    idx=getmask(8,32,3,varargin{:});
else
    idx=getmask(8,32,3,@sr.qpdecode,varargin{:});
end
end
