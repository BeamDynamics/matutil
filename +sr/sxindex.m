function [idx,kdx,plane]=sxindex(varargin)
%SXINDEX	find index of sextupole from its name
%
%IDX=SXINDEX(SXNAME)
%   SXNAME:    string, char array or cell array of strings
%
%IDX=SXINDEX(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:7
%   CELL and ID must have the same dimensions or be a scalar
%
%[IDX,KDX]=SXINDEX(...)
%   Returns the hardware index
%
%[IDX,KDX,PLANE]=SXINDEX(...)	returns in addition the "plane":
%           h,v,s,n for correctors
%
%   See also SR.SXNAME, SR.SXSELECT, SR.QPINDEX, SR.BPMINDEX, SR.CORINDEX

[idx,kdx]=getindex(7,32,3,@bindex,varargin{:});

    function [celnum,id]=bindex(bname)
        [celnum,id,plane]=sr.sxdecode(bname);
    end
end
