function [resh,resv]=autocor_model(varargin)
%AUTOCOR_MODEL Creates data files for the autocor device server
%
%[RESPH,RESPV]=SR.AUTOCOR_MODEL(PATH)
%PATH:  Data directory (Default: pwd)
%
%[RESPH,RESPV]=SR.AUTOCOR_MODEL(..,'exp',...)
%   Use a measured response matrix

args={pwd};
[exper,options]=getflag(varargin,'exp');
args(1:length(options))=options;
pth=args{1};

[coefh, coefv]=load_corcalib('sr',8,9);     % get steerer calibration (rad/A)

atmodel=sr.model(pth);
[bpmidx,sthidx,stvidx]=atmodel.select('bpm','steerh','steerv');
[bpm,steerh,steerv]=atmodel.get('bpm','steerh','steerv');
nb=size(bpm,1);
nsth=size(steerh,1);
nstv=size(steerv,1);

if exist('./alpha','file') ~= 2,
    savetext('alpha','%.4e\n',atmodel.alpha);
end

fprintf('Computing horizontal response\n');
h_resp=atorm(atmodel.ring,'h',sthidx,bpmidx).*coefh(ones(nb,1),:);
fprintf('Computing vertical response\n');
v_resp=atorm(atmodel.ring,'v',stvidx,bpmidx).*coefv(ones(nb,1),:);
fprintf('Computing frequency response\n');
dct=4.e-4;
forb=findsyncorbit(atmodel.ring,dct,bpmidx);
h_fresp=-forb(1,:)'/dct.*atmodel.ll*atmodel.ll/992/2.997924e8;
if exper
    hr=sr.load_normresp(fullfile(pth,'resH.rsp'),'h','m/A','TooLarge',0,'Zeros',0,'Fixed',0);
    vr=sr.load_normresp(fullfile(pth,'resV.rsp'),'v','m/A','TooLarge',0,'Zeros',0,'Fixed',0);
    [hfr,~]=sr.load_fresp(pth);
    b_ok=all(isfinite([hr vr hfr]),2);
    sh_ok=true(1,nsth);
    sv_ok=true(1,nstv);
    h_resp(b_ok,sh_ok)=hr(b_ok,sh_ok);
    v_resp(b_ok,sv_ok)=vr(b_ok,sv_ok);
    h_fresp(b_ok)=hfr(b_ok);
    matmode='measured';
else
    b_ok=true(nb,1);
    sh_ok=true(1,nsth);
    sv_ok=true(1,nstv);
    matmode='theoretical';
end

hv_bpms=[bpm(:,1:2) 2*pi*bpm(:,3) bpm(:,4:5) 2*pi*bpm(:,6)];
bpm_name=sr.bpmname((1:size(hv_bpms,1))');
store_bpm('bpms',hv_bpms,bpm_name);

h_steerers = [steerh(:,1:2) 2*pi*steerh(:,3) coefh(:)];
v_steerers = [steerv(:,[1 5]) 2*pi*steerv(:,6) coefv(:)];
steerh_name=sr.steername((1:nsth)','tango:SR/ST-H%i/C%i/CURRENT');
steerv_name=sr.steername((1:nstv)','tango:SR/ST-V%i/C%i/CURRENT');
store_steerer('h_steerers',h_steerers,steerh_name);
store_steerer('v_steerers',v_steerers,steerv_name);

b_ok=enter_bpm(b_ok);
sh_ok=enter_steer(sh_ok,'SR/ST-H');
sv_ok=enter_steer(sv_ok,'SR/ST-V');
[resh,resv]=compute_srcor(atmodel.ring,pth,matmode,h_resp,v_resp,h_fresp,...
    b_ok,sh_ok,sv_ok);

    function ok=enter_bpm(ok)
        while true
            try
                fprintf('%i disabled BPMs:\n',sum(~ok));
                disp(sr.bpmname(~ok'));
                str=input('Enter disabled bpms (comma separated list, ex. "c4-1,c15-3"): ','s');
                if ~isempty(str)
                    ok=ok & ~sr.bpmselect(strsplit(str,','))';
                end
                break
            catch
                disp('Wrong BPM name');
            end
        end
    end

    function ok=enter_steer(ok, code)
        while true
            try
                fprintf('%i disabled %s steerers:\n',sum(~ok), code);
                disp(sr.steername(~ok'))
                str=input('Enter disabled steerers (comma separated list, ex. "h6/c4,h22/c11"): ','s');
                if ~isempty(str)
                    ok=ok & ~sr.steerselect(strsplit(str,','));
                end
                break
            catch
                disp('Wrong steerer name');
            end
        end
    end
end