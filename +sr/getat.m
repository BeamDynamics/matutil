function atstruct = getat(varargin)
%GETAT	% Return an AT structure
%
%ATSTRUCT=GETAT(VARARGIN) scans the input arguments looking for:
%
%- 'opticsname'
%           optics name (defaults to 'theory')
%- '/machfs/appdata/sr/optics/settings/opticsname':
%           full path of optics directory
%- atstruct:
%           AT structure
%- sr.model:
%           sr.model object
%- '/machfs/MDT/2017/Sep12/resp1'
%           fullpath of a response matrix measurement
%
%
%ATSTRUCT:	Resulting AT structure
%
%GETAT(...,'energy',energy)
%   Set the ring energy
%
%GETAT(...,'reduce',true,'keep',pattern)
%   Remove elements with PassMethod='IdentityPass' and merge adjacent
%   similar elements, but keeps elements with FamName matching "pattern"
%   Pattern may be a logical mask. (Default: 'BPM.*|ID.*').
%
%GETAT(...,'remove',famnames)
%   remove elements identified by the family names (cell array)
%
%GETAT(...,'QuadCor')
%   When accessing a fitted response matrix, load the updated quadrupole corrections
%   instead of the initial ones
%
%GETAT(...,'SkewCor')
%   instead of the initial ones
%   When accessing a fitted response matrix, load the updated skew quad corrections
%
%GETAT(...,'MaxOrder',n)
%   Limit the order of polynomial field expansion to n at maximum.
%   Default 999
%
%GETAT(...,'NumIntSteps',m)
%   Set the NumIntSteps integration parameter to at least m.
%   Default 20
%
%GETAT(...,'QuadFields',quadfields)
%   When reading a BETA file, set quadrupoles fields to the specified ones.
%   Default: {'FringeQuadEntrance',1,'FringeQuadExit',1}
%
%GETAT(...,'BendFields',bendfields)
%   When reading a BETA file, set bending magnet fields to the specified ones.
%   Default: {'FringeQuadEntrance',1,'FringeQuadExit',1}
%
%GETAT(...,'hardangle',value)
%GETAT(...,'softangle',value)
%   Set the angle of bending magnet sources

[qcor,args]=getflag(varargin,'QuadCor');
[scor,args]=getflag(args,'SkewCor');
[machid,location,args]=atmodel.getpath('sr',args{:});
try
    atstruct=atmodel.getatstruct(machid,location,args{:});
catch err
    if ~strcmp(err.identifier,'ReadBeta:couldNotReadFile'), err.rethrow();end
    [qemres,~,qemb]=qempanelset(location);              % load quad corrections
    quaderrfile=fullfile(qemres.datadir,'quaderrors.mat');
    fprintf('Loading quadrupole errors from %s\n',quaderrfile);
    [qemb(2).kn,qemb(2).dipdelta,qemb(2).dxs]=...       % load quad errors
        qemerrload(qemres,quaderrfile);
    skewerrfile=fullfile(qemres.datadir,'skewerrors.mat');
    fprintf('Loading skew quad errors from %s\n',skewerrfile);
    s=load(skewerrfile);  % load skew errors
    qemb(2).ks=s.ks;
    qemb(2).diptilt=s.diprot;
    if qcor
        qemb(2).cor=load_correction(fullfile(qemres.datadir,'cornew.dat'),...
            qemres.qpcode,qemres.opticsdir);	% load quad corrections
    end
    if scor
        qemb(2).skewcor=load_correction(fullfile(qemres.datadir,'skewnew.dat'),...
            qemres.skcode,qemres.opticsdir);	% load skew corrections
    else
        qemb(2).skewcor=load_correction(fullfile(qemres.datadir,'skewcor.dat'),...
            qemres.skcode,qemres.opticsdir);	% load skew corrections
    end
    
    atstruct=qemat(qemres,qemb(2),true);
end
end

