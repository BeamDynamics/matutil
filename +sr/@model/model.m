classdef model < atmodel
    %Class providing access to SR optical values
    %
    %Available locations:
    %
    %bpm:       All BPMs
    %qp:        All quadrupoles
    %sx:        All sextpoles
    %steerh:    Horizontal steerers
    %steerv:    Vertical steerers
    %cornq:     Normal quadrupole correctors
    %corsq:     Skew quadrupole correctors
    %corsx:     Sextupole correctors
    %iax:       In-air X-Ray monitors
    %iax_c<x>   A single iax
    %pin_d9:	Pinhole D9
    %pin_d11:	Pinhole D11
    %pin_id25:	Pinhole ID25
    %id:        All IDs
    %high:      High beta straights
    %low:       Low beta straights
    %bmsoft:    Soft bending magnet sources
    %bmhard:    Hard bending magnet sources
    %id<x>:     ID cell x
    %bmsoft<x>: Soft bending magnet cell x
    %bmhard<x>: Hard bending magnet cell x
    %
    %
    %Available parameters:
    %
    %0:     index in the AT structure
    %1:     Integrated strength
    %2:     theta/2pi
    %3:     betax
    %4:     alphax
    %5:     phix/2pi/nux
    %6:     etax
    %7:     eta'x
    %8:     betaz
    %9:     alphaz
    %10:    phiz/2pi/nux
    %11:    etaz
    %12:    eta'z
    %
    %Default parameters:
    %
    %[   2         3         5        6      8       10      ]
    %[theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux]
    
    properties (Constant, Access=private, Hidden)
        directory=fullfile(getenv('APPHOME'),'sr','optics','settings')
    end
    
    methods (Static)
        newring=checklattice(ring,varargin)
        
        function pth=getdirectory(varargin)
            nm=getargs(varargin,{'theory'});
            pth=fullfile(sr.model.directory,nm);
        end
    end
    
    properties
        iaxlist=[5;10;11;12;14;18;21;25;26;29;31;3]
        corcode=struct('steerh',8,'steerv',9,'cornq',6,'corsq',10,'corsx',3)
    end
    
    methods (Static, Access=private, Hidden)
        function idx=selectsx(code)
            idx=false(1,224);
            idx(selcor(code))=true;
        end
    end
    
    methods
        function this=model(varargin)
            %Builds the linear model of the ring
            %
            %M=SR.MODEL(AT)             Uses the given AT structure
            %
            %M=SR.MODEL()               takes the lattice from
            %           $APPHOME/sr/optics/settings/theory/betamodel.mat
            %
            %M=SR.MODEL('opticsname')   takes the lattice from
            %           $APPHOME/sr/optics/settings/opticsname/betamodel.mat
            %
            %M=SR.MODEL(path)           takes the lattice from
            %           path/betamodel.mat or
            %           path/betamodel.str or
            %           path where path is an ORM measurement directory. Then:
            %    M=SR.MODEL(..., 'QuadCor') uses the computed quad corrections
            %    M=SR.MODEL(..., 'SkewCor') uses the computed skew corrections
            
            this@atmodel(sr.getat(varargin{:}));
        end
        
        function settune(this,tunes,varargin)
            %Change tunes
            %
            %SETTUNE(TUNES,tunefixmode)
            % TUNES:    1x2 vector of desired tunes
            % tunefixmode :    string, 'QF1QD2' (default) or 'Matching'
            % varargin : 'name', value,... see ebs.opticsmatching
            %           use only if tunefixmode is 'Matching'
            %
            %
            %see also: atfittune ebs.opticsmatching
            
            
            expectedmode={'QF7QD6','Matching'};
            
            if ~isempty(varargin)
                tunefixmode=varargin{1};
            else
                tunefixmode=expectedmode{1};
            end
            
            switch tunefixmode
                
                case expectedmode{1}
                    
                    % move tune to Q1
                    rt=atfittune(this.ring,tunes,'QF7\w*','QD6\w*');
                    qpk=atgetfieldvalues(rt,this.getid('qp'),'PolynomB',{1,2});
                    this.setfieldvalue('qp','PolynomB',{1,2},qpk);
                    
                case expectedmode{2}
                    % match tune
                    muxc = tunes(1);
                    muyc = tunes(2);
                    
                    if tunes(1)<1
                        muxc=(36 + tunes(1));
                    end
                    if tunes(2)<1
                        muyc=(13 + tunes(2));
                    end
                    
                    [~,qpk]=sr.opticsmatching(this.ring,...
                        'mux',muxc,'muy',muyc,...
                        varargin{2:end});
                    
                    this.setfieldvalue('qp','PolynomB',{1,2},qpk)
            end
            
            % at the next request for optics, recompute them.
            this.modified=true;
            this.emith_=[];
        end
        
        function data=pin25(this,prms)
            if nargin<2, prms=this.prms; end
            data=this.vlave(this.getid('pin25'),this.locs(prms+1));
        end
        
        function data=pin9(this,prms)
            if nargin<2, prms=this.prms; end
            data=this.vlave(this.getid('pin9'),this.locs(prms+1));
        end
    end
    
    methods (Access=protected)
        function idx=elselect(this,code)
            %Return indices of selected elements
            switch lower(code)
                case 'bpm'
                    idx=atgetcells(this.ring,'Class','Monitor');
                case 'sx'
                    idx=atgetcells(this.ring,'Class','ThinMultipole') | ...
                        atgetcells(this.ring,'Class','Sextupole');
                case 'qp'
                    idx=atgetcells(this.ring,'Class','Quadrupole');
                case {'steerh','steerv','cornq','corsq','corsx'}
                    idx=this.getid('sx');
                    idx(idx)=this.selectsx(this.corcode.(code));
                case 'iax'
                    idx=atgetcells(this.ring,'FamName','iax(_c\d+)?');
                    idx=this.mcellid(idx,this.iaxlist,1);
                case 'id'
                    idx=atgetcells(this.ring,'FamName','IDMarker$|^ID\d+');
                case 'bmsoft'
                    idx=atgetcells(this.ring,'FamName','BMSOFT\d+');
                case {'bmhard','bm'}
                    idx=atgetcells(this.ring,'FamName','BMHARD\d+');
                case 'high'
                    idx=this.getid('id');
                    idx=this.mcellid(idx,2:2:32,1);
                case 'low'
                    idx=this.getid('id');
                    idx=this.mcellid(idx,1:2:32,1);
                otherwise
                    if strncmp(code,'id',2)
                        idx=this.mcellid(this.getid('id'),str2double(code(3:end)),1);
                    elseif strncmp(code,'iax',3)
                        idx=atgetcells(this.ring,'FamName',['iax' code(4:end)]);
                    elseif strncmp(code,'pin_',4)
                        idx=atgetcells(this.ring,'FamName',['pinhole_' code(5:end)]);
                    elseif strncmp(code,'bmsoft',6)
                        idx=this.mcellid(this.getid('bmsoft'),str2double(code(7:end)),1);
                    elseif strncmp(code,'bmhard',6)
                        idx=this.mcellid(this.getid('bmhard'),str2double(code(7:end)),1);
                    elseif strncmp(code,'bm',2)
                        idx=this.mcellid(this.getid('bmhard'),str2double(code(3:end)),1);
                    else
                        idx=atgetcells(this.ring,'FamName',upper(code));
                    end
            end
        end
        
        function nm=elname(this,code)
            %Return the name of selected elements
            switch lower(code)
                case {'bpm','qp','sx'}
                    number=sum(this.select(code));
                    nm=sr.([code 'name'])((1:number)');
                case 'iax'
                    nm=sr.iaxname(this.iaxlist,1);
                case {'steerh','steerv','cornq','corsq','corsx'}
                    %                     number=sum(this.select(code));
                    %                     nm=sr.corname(this.corcode.(code),(1:number)');
                    [~,nm]=selcor(this.corcode.(code),'%s');
                case {'pin_d9','pin_d11','pin_id25'}
                    nm={['sr/d-emit/' code(5:end)]};
                otherwise
                    nm=elname@atmodel(this,code);
            end
        end
    end
end
