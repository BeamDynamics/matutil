function ring = checklattice(ring,varargin)
%ESRF = SR.CHECKLATTICE(RING)
%
%Take en ESRF lattice and add:
%- ID source points ('IDn')
%- Hard and Soft bending magnet source points ('BMHARD' and 'BMSOFT')
%- IAX locations ('IAX')
%- Pinhole camera locations ('PINHOLE')
%And call 'atclean'
%
%CHECKLATTICE(...,'hardangle',value)
%CHECKLATTICE(...,'softangle',value)
%   Set the angle of bending magnet sources
%
%CHECKLATTICE(...,'energy',energy)
%   Set the ring energy
%
%CHECKLATTICE(...,'remove',famnames)
%   remove elements identified by the family names (cell array)
%
%CHECKLATTICE(...,'reduce',true)
%   group similar elements together by calling atreduce
%
%CHECKLATTICE(...,'reduce',true,'keep',pattern)
%   Remove elements with PassMethod='IdentityPass' and merge adjacent
%   similar elements, but keeps elements with FamName matching "pattern"
%   Pattern may be a logical mask. (Default: 'BPM.*|ID.*).
%
%CHECKLATTICE(...,'MaxOrder',n)
%   Limit the order of polynomial field expansion to n at maximum. Default 999
%
%CHECKLATTICE(...,'NumIntSteps',m)
%   Set the NumIntSteps integration parameter to at least m. Default 20
%
%CHECKLATTICE(...,'QuadFields',quadfields)
%   Set quadrupoles fields to the specified ones. Default: {}
%
%CHECKLATTICE(...,'BendFields',bendfields)
%   Set bending magnet fields to the specified ones. Default: {}

[check,options]=getoption(varargin,'CheckLattice',true);
if check
    [energy,periods]=atenergy(ring);
    [energy,options]=getoption(options,'energy',energy);
    [periods,options]=getoption(options,'Periods',periods);
    [hardangle,options]=getoption(options,'hardangle',0.009);
    [softangle,options]=getoption(options,'softangle',0.003);
    reduce=getoption(options,'reduce',false);
    [keep,options]=getoption(options,'keep',false(size(ring)));
    if ~islogical(keep), keep=atgetcells(ring,'FamName',keep); end
    [devnam,options]=getoption(options,'DeviceNames',true);

    precious=atgetcells(ring,'FamName','IDMarker$|^ID\d+$|^SD[HL].') |...
        atgetcells(ring,'Class','Monitor');
    ring=atclean(ring,options{:},'keep',keep|precious);
    ring=addidsourcepoints(ring);

    if ~reduce
        ring=addpinholes(ring);
        ring=addiaxsourcepoints(ring);
        ring=addbmsourcepoints(ring);
    end
    
    if devnam
        ring=adddevicenames(ring);
    end
    
    rp=atgetcells(ring,'Class','RingParam');
    if any(rp)
        params=ring(rp);
        params{1}.Energy=energy;
        params{1}.Periodicity=periods;
    else
        params={atringparam('ESRF',energy,periods)};
    end
    ring=[params(1);ring(~rp)];
end

    function mach=addbmsourcepoints(mach)
        if ~any(atgetcells(mach,'FamName','bm(hard|soft)\d*'))
            fprintf('Add bending magnet source points...\n');
            soft=atgetcells(mach,'FamName','B2S');	% Add bending magnet source points
            nb1=sum(soft);
            hard=soft([end 1:end-1]);   % Next element
            softmag=atgetfieldvalues(mach(soft),'BendingAngle');
            softfrac=softangle./softmag;
            hardfrac=(hardangle-softmag)./atgetfieldvalues(mach(hard),'BendingAngle');
            frac=reshape([softfrac';hardfrac'],[],1);
            if nb1==32
                elems=reshape(...
                    [arrayfun(@(cell) atmarker(sprintf('BMSOFT%d',cell)),[4:32 1:3]);...
                    arrayfun(@(cell) atmarker(sprintf('BMHARD%d',cell)),[4:32 1:3])],...
                    [],1);
            else
                elems=repmat({atmarker('BMSOFT');atmarker('BMHARD')},nb1,1);
            end
            mach=atinsertelems(mach,soft|hard,frac,elems);
        end
    end
    function mach=addiaxsourcepoints(mach)
        if ~any(atgetcells(mach,'FamName','iax(_c\d+)?'))
            fprintf('Add IAX locations...\n');
            h1=atgetcells(mach,'FamName','B1H');	% Add IAX source points
            h1angle=atgetfieldvalues(mach(h1),'BendingAngle');
            h1frac=0.04715./h1angle;
            if sum(h1)==32
                iax=arrayfun(@(cell) atmarker(sprintf('IAX_C%d',cell)),[4:32 1:3]);
            else
                iax=atmarker('IAX');
            end
            mach=atinsertelems(mach,h1,h1frac,iax);
        end
    end
    function mach=addidsourcepoints(mach)
        if ~any(atgetcells(mach,'FamName','IDMarker$|^ID\d+'))
            fprintf('Add ID source points...\n');
            sdf=atgetcells(mach,'FamName','SD[HL].');
            sdf(sdf)=bitand(1:sum(sdf),1);    % Take every 2nd drift
            if sum(sdf)==32
                id=arrayfun(@(cell) atmarker(sprintf('ID%d',cell)),[4:32 1:3]);
            else %2
                id=atmarker('IDMarker');
            end
            mach=atinsertelems(mach,sdf,0,id);
        end
    end
    function mach=addpinholes(mach)
        pinid=~any(atgetcells(mach,'FamName','pinhole(_id\d+)?'));
        pind=~any(atgetcells(mach,'FamName','pinhole(_d\d+)?'));
        if pinid
            fprintf('Add ID pinholes...\n');
            h1=atgetcells(mach,'FamName','B1H');
            if sum(h1)==32
                h1=mcellid(h1,25,1);
                pinid=arrayfun(@(cell) atmarker(sprintf('PINHOLE_ID%d',cell)),25);
            else
                pinid=atmarker('PINHOLE');
            end
            mach=atinsertelems(mach,h1,0,pinid);
        end
        if pind
            fprintf('Add D pinholes...\n');
            s2=atgetcells(mach,'FamName','B2S');
            if sum(s2)==32
                s2=mcellid(s2,[9 11],1);
                pind=arrayfun(@(cell) atmarker(sprintf('PINHOLE_D%d',cell)),[9 11]);
            else
                pind=atmarker('PINHOLE');
            end
            mach=atinsertelems(mach,s2,0,pind);
        end
    end

    function mach=adddevicenames(mach)
        fprintf('Add device names...\n');
        ind=atgetcells(mach,'Class','Quadrupole');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',sr.qpname(1:sum(ind)));
        %mach(ind)=atsetfieldvalues(mach(ind),'FamilyDevice',ebs.qpfamname(1:sum(ind)));
        ind=atgetcells(mach,'Class','Sextupole');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',sr.sxname(1:sum(ind)));
        %mach(ind)=atsetfieldvalues(mach(ind),'FamilyDevice',(1:sum(ind)));
        ind=atgetcells(mach,'Class','Monitor');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',sr.bpmname(1:sum(ind)));
        ind=atgetcells(mach,'FamName','IAX_.*');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',sr.iaxname(1:sum(ind)));
    end

    function idx=mcellid(idx,cell,id)
        sel=reshape(false(sum(idx),1),32,[]);
        sel(cell,id)=true;
        idx(idx)=circshift(sel,-3);
    end

end
