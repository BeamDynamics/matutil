function idx=corindex(code,varargin)
%CORINDEX	find the index of a corrector from its name
%
%IDX=CORINDEX(CODE,CORNAME)
%   CODE:   1: 16 quad correctors
%           2: 16 skew quad correctors
%           3: 12 sextupole correctors
%           4:  8 horizontal correctors
%           5:  8 vertical correctors
%           6: 32 quad correctors      106: without qp-n4/c4
%           7: 32 skew correctors
%           8: all H steerers
%           9: all V steerers
%           10: 64 skew correctors      110: with qp-s4/c4
%           11: 12 sextupole corrector + 8 indep. sextupoles
%   SXNAME:    string, char array or cell array of strings
%
%IDX=CORINDEX(CODE,CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:7
%   CELL and ID must have the same dimensions or be a scalar
%
%   See also SR.CORNAME, SR.CORSELECT, SR.SXINDEX, SR.QPINDEX,SR.BPMINDEX

loc=zeros(1,224);
sel=selcor(code);
loc(sel)=1:length(sel);

[idx,~]=getindex(7,32,3,@bindex,varargin{:});
idx=loc(idx);
if any(idx==0)
    error('SX:wrongName','Wrong corrector name')
end

    function [celnum,id]=bindex(bname)
        [celnum,id,~]=sr.sxdecode(bname);
    end
end
