function varargout=sxname(varargin)
%SXNAME	returns the names of sextupoles with index idx
%
%SXNAME=SXNAME(IDX)	returns the names of sextupoles with indexes IDX
% IDX: vector of indexes in the range 1:224 (1 is S4/C4)
%       logical array of length 224
%       or n x 2 matrix in the form [cell index] with
%       cell in 1:32, index in 1:7
%
%SXNAME=SXNAME(IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : 'SR/PS-S%i/C%i')
%
%[NM1,NM2...]=SXNAME(...) May be used to generate several names
%
%   See also SR.SXINDEX, SR.QPNAME, SR.BPMNAME, SR.STEERNAME

charargs=cellfun(@ischar,varargin);
args=varargin(~charargs);
format={'SR/PS-S%i/C%i'};
format(1:sum(charargs))=varargin(charargs);

sid=[4,6,13,19,20,22,24];

[~,celnum,~,rid]=sr.cellnum(length(sid),args{:});
textfunc=@(cnum,rid) sprintf(format{1},sid(rid),cnum);
nms=arrayfun(textfunc,celnum,rid,'UniformOutput',false);
if nargout==length(nms)
    varargout=nms;
else
    varargout={nms};
end
end
