function save_steerresp(filename,idx,current,resp,encodef)
%SAVE_STEERRESP(FILENAME,IDX,EXCITATION,RESPONSE,ENCODEF)
%
%FILENAME:  tab delimited text file (1 line)
%IDX:       Steerer index
%EXCITATION:Steerer current
%RESPONSE:	BPM response [m]
%ENCODEF:	function to encode steerer index to name
%         (default: sr.steername())

if nargin < 5, encodef=@sr.steername; end
fid=fopen(filename,'wt');
if fid > 0
    fprintf(fid,'%.4f\t%s',current,encodef(idx));
    fprintf(fid,'\t%.5f',1.E3*resp);  % convert m to mm
    fprintf(fid,'\n');
    fclose(fid);
else
    error('Resp:nofile',['Cannot open file ' filename ' for writing.']);
end
