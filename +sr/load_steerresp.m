function [idx,current,resp,algo]=load_steerresp(filename,varargin)
%LOAD_STEERRESP(FILENAME)    get response of a single steerer
%
%[IDX,EXCITATION,RESPONSE,ALGORITHM]=LOAD_STEERRESP(FILENAME,DECODEF)
%
%FILENAME:tab delimited text file (1 line)
%DECODEF: function to decode steerer name into index
%         (default: sr.steerindex())
%
%IDX:       Steerer index
%EXCITATION:Steerer current
%RESPONSE:	BPM response [m]
%ALGORITHM:	BPM algorithm (0 or 1)

fid=fopen(filename,'rt');
if fid > 0
    [idx,current,resp,algo]=sr.get_steerresp(fgetl(fid),varargin{:});
    fclose(fid);
else
    error('Resp:nofile',['Cannot open input file ' filename])
end
end
