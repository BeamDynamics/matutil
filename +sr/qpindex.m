function [idx,kdx]=qpindex(varargin)
%QPINDEX	find index of quadrupoles from their names
%
%IDX=QPINDEX(QPNAME)
%   BPMNAME:    string, char array or cell array of strings
%
%IDX=QPINDEX(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:8
%   CELL and ID must have the same dimensions or be a scalar
%
%[IDX,KDX]=QPINDEX(...)
%   Returns the hardware index
%
%   See also QPNAME, SXSELECT, BPMINDEX

[idx,kdx]=getindex(8,32,3,@sr.qpdecode,varargin{:});
end
