function r=fold(a,dim)
%A=SRFOLD(B)			reshapes data for SR symmetry
%
%	A(32*n,1) -> R(n,32)

if isvector(a)
    n=numel(a)/32;
    b=reshape(a(:),2*n,16);
    r=reshape([b(1:n,:);b(2*n:-1:n+1,:)],n,32);
elseif nargin<2 || dim==1
    sz=size(a);
    n=sz(1)/32;
    a2=reshape(a,2*n,[]);
    r=reshape(a2([1:n 2*n:-1:n+1],:),[n 32 sz(2:end)]);
else
    nd=ndims(a);
    ord1=[dim 1:dim-1 dim+1:nd];
    ord2=[dim dim+1 1:dim-1 1+(dim+1:nd)];
    r=ipermute(sr.fold(permute(a,ord1)),ord2);
end
end





