function [idx,kdx]=bpmindex(varargin)
%SRBPMINDEX	find index of BPM from its name
%
%IDX=BPMINDEX(BPMBAME)
%   BPMNAME:    string, char array or cell array of strings
%
%IDX=BPMINDEX(KDX)
%   KDX:        "hardware" index of the BPM
%
%IDX=BPMINDEX(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:7
%   CELL and ID must have the same dimensions or be a scalar
%
%[IDX,KDX]=BPMINDEX(...)	returns in addition the "hardware" index
%
%   See also BPMNAME, BPMSELECT, QPINDEX, SXINDEX

[idx,kdx]=getindex(7,32,3,@sr.bpmdecode,varargin{:});
end
