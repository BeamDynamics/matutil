function psvals=setcorbase(corfile,allname,resoname,filename)
%SETCORBASE     Set the correction base on resonance correction devices
%
%STRENGTHS=SETCORBASE(CORFILE,ALLNAME,RESONAME,FILENAME)

global APPHOME

[devvals,devnames]=load_setting('textfile',corfile);
psnames=tango.Device(allname).CorrectorNames.read;
psvals=selectfrom(@(a,b) any(strcmpi(b,{a,['tango:' a '/CURRENT']})),...
    psnames,devnames,devvals);
% Create base file for old apps
% bindir=[getenv('MACH_HOME') '/bin/' getenv('MACHARCH') '/'];
[s,w]=unix(['newfile -f mag_cor ' fullfile(APPHOME,'sr','optics','settings','theory',filename) ' <' corfile]); %#ok<ASGLU>
% Set offset for new devices
resondev=tango.Device(resoname);
resondev.Offset=psvals;
end
