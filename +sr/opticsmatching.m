function [rm, qpk]=opticsmatching(r,varargin)
% match lattice optics for ESRF SR
%
% OUTPUT:
% rm    : ESRF SR AT lattice with matched optics knobs
% qpk   : matched quadurpole strengths
%
% INPUT:
% r     : ESRF SR AT lattice
%
% OPTIONAL INPUT (pairs 'name', value, 'name2',value,... ):
% 'mux'     : double  , total hor. phase advance
% 'muy'     : double , total ver. phase advance
% 'byL'   : double   , vertical low beta 
% 'bxH'   : double   , horizontal high beta
% 'bxL'   : double  , horizontal low beta
% 'byH'   : double  , vertial high beta
% 'byL23' : double  , vertical cell23 beta
% 
% default values are those of r (no change)
% 
% 'verbose' : logical (false), print out text information
%
% DESCRIPTION:
% 0) separate superperiod, C23 
% 1) match superperiod
% 2) match c23
%
% EXAMPLES:
% rm=ebs.opticsmatching(r);
% rm=ebs.opticsmatching(r,'verbose',true);
% rm=ebs.opticsmatching(r,'mux',36.47,'noDQ',true);
% rm=ebs.opticsmatching(r,'betyL23',12.0,'muy',12.23);
%
%see also:


%% get cells
% all, cell 23 has independent optics parameters

% get superperiod
indhb=findcells(r,'FamName','SDHX');

if indhb(3)-indhb(2)>20
sp=r(indhb(2):indhb(3));
else
sp=r(indhb(1):indhb(2));
end    

% get cell23
indhb23=findcells(r,'FamName','SDHI');

indhx23=findcells(r,'FamName','SDHX');
 if length(indhb23)>2
c23=r(indhb23(11):indhx23(9));
 else
 end

%% define defaults
indcent=findcells(sp,'FamName','SDLO');
indcent23=findcells(c23,'FamName','SDLW');


% compute defaults and parse inputs
[lall,~,~]=atlinopt(r,0,1:length(r));
defaultmux  = lall(end).mu(1)/2/pi;
defaultmuy  = lall(end).mu(2)/2/pi;

[li,~,~]=atlinopt(sp,0,1);
defaultbxH=li.beta(1);
defaultbyH=li.beta(2);

[lc,~,~]=atlinopt(sp,0,indcent(2));
defaultbxL=lc.beta(1);
defaultbyL=lc.beta(2);

[lc23,~,~]=atlinopt(c23,0,indcent23(2));
defaultbyL23=lc23.beta(2);

p = inputParser;


addRequired(p,'r',@iscell);
addOptional(p,'mux',defaultmux,@isnumeric);
addOptional(p,'muy',defaultmuy,@isnumeric);
addOptional(p,'byL',defaultbyL,@isnumeric);
addOptional(p,'bxH',defaultbxH,@isnumeric);
addOptional(p,'bxL',defaultbxL,@isnumeric);
addOptional(p,'byH',defaultbyH,@isnumeric);
addOptional(p,'byL23',defaultbyL23,@isnumeric);

addOptional(p,'verbose',false,@islogical);

parse(p,r,varargin{:});

muxc       = p.Results.mux/16;
muyc       = p.Results.muy/16;
betyL      = p.Results.byL;
betxH      = p.Results.bxH;
betxL      = p.Results.bxL;
betyH      = p.Results.byH;
betyL23    = p.Results.byL23;

verbose    = p.Results.verbose;

%% start matching

% unbalance to get matching to wished tunes.
[lallc23,~,~]=atlinopt(c23,0,1:length(c23)+1);
muxc23=lallc23(end).mu(1)/2/pi;
muyc23=lallc23(end).mu(2)/2/pi;

Dx=muxc23-muxc;
muxc=muxc-Dx/15;
Dy=muyc23-muyc;
muyc=muyc-Dy/15;

% muxc*15+muxc23
% muyc*15+muyc23

% match superperiod
sp=sr.matchsuperperiodoptics(...
    sp,...
    muxc,muyc,...
    betxH,betyL,...
    betxL,betyH,...
    indcent,verbose);

% copy values of familiy magnets from SP to C23
famname={'QF2','QD3','QD4','QF5','QD6','QF7'};
for ifam=1:length(famname)
    k=atgetfieldvalues(sp,findcells(sp,'FamName',famname{ifam}),'PolynomB',{1,2});
    c23=atsetfieldvalues(c23,findcells(c23,'FamName',famname{ifam}),'PolynomB',{1,2},k(1));
    if verbose, disp(['sp->c23 ' famname{ifam} ' : ' num2str(k(1),'%2.6f')]);end
end

famname={'S4','S6','S13','S19','S20','S22','S24'};
for ifam=1:length(famname)
    k=atgetfieldvalues(sp,findcells(sp,'FamName',famname{ifam}),'PolynomB',{1,3});
    c23=atsetfieldvalues(c23,findcells(c23,'FamName',famname{ifam}),'PolynomB',{1,3},k(1));
    if verbose, disp(['sp->c23 ' famname{ifam} ' : ' num2str(k(1),'%2.6f')]); end
end


% rematch C23 
indcent23=findcells(c23,'FamName','SDLW');
muprecision=0.000001;
c23=matchc23(...
    sp,c23,...
    muxc23,muyc23,muprecision,...
    betyL23,...
    indcent23,verbose);

% recompose lattice
ca5=r(findcells(r,'FamName','CA5'));
ca7=r(findcells(r,'FamName','CA7'));
ca25=r(findcells(r,'FamName','CA25'));

mid=length(sp)/2;
spl=sp(1:mid);
spr=sp((mid+1):end);

% % rebuild lattice
rm=[spl;ca5;spr;spl;ca7;spr;repmat(sp,7,1);c23;spl;ca25;spr;repmat(sp,5,1)];

% get matched quadrupole gradients
qpi=atgetcells(rm,'Class','Quadrupole');
qpk=atgetfieldvalues(rm,qpi,'PolynomB',{1,2});

% set rm gradients in r, to garantee correct ring size and elements.
qpi=atgetcells(r,'Class','Quadrupole');
rm=atsetfieldvalues(r,qpi,'PolynomB',{1,2},qpk);

return


function c231=matchc23(...
    sp,c23,...
    muxc,muyc,muprecision,...
    betyL,...
    indcent,verbose)
% match optics in c23
if verbose, verb=3; end
if ~verbose, verb=0; end


[ld,~,~]=atlinopt(sp,0,length(sp)+1); % matched standard superperiod
%figure; atplot(c23,'inputtwiss',ld(end));

v3=atVariableBuilder(c23,'QD4W',{'PolynomB',{2}});
v4=atVariableBuilder(c23,'QF5W',{'PolynomB',{2}});
v5=atVariableBuilder(c23,'QD6W',{'PolynomB',{2}});
v6=atVariableBuilder(c23,'QF7W',{'PolynomB',{2}});
Variab=[v3,v4,v5,v6];

c1=struct(...
    'Fun',@(~,ld,~)mux(ld),...
    'Weight',1,...
    'RefPoints',[1:length(c23)+1],...
    'Min',muxc-muprecision,...
    'Max',muxc+muprecision);

c2=struct(...
    'Fun',@(~,ld,~)muy(ld),...
    'Weight',1,...
    'RefPoints',[1:length(c23)+1],...
    'Min',muyc-muprecision,...
    'Max',muyc+muprecision);

c4=atlinconstraint(length(c23)+1,{{'beta',{1}}},ld(1).beta(1),ld(1).beta(1),1);
c4b=atlinconstraint(length(c23)+1,{{'beta',{2}}},ld(1).beta(2),ld(1).beta(2),1);
c4c=atlinconstraint(length(c23)+1,{{'Dispersion',{1}}},ld(1).Dispersion(1),ld(1).Dispersion(1),1);

Constr=[c1,c2,c4,c4b,c4c];%,c4d,c4e

c231=c23;
%c231=atmatch(c231,Variab,Constr,10^-9,2000,verb,@fminsearch);%,ld(1),'fminsearch');%
c231=atmatch(c231,Variab,Constr,10^-15,500,verb,@lsqnonlin);%,ld(1),'fminsearch');%


%
% figure;
% atplot(c231);
% figure;
% atplot(repmat(c231,16,1));

return

function c231=matchc23s19(...
    sp,c23,...
    muxc,muyc,muprecision,...
    betyL,...
    indcent)
% match optics in c23
error('function not completed')

[ld,~,~]=atlinopt(sp,0,length(sp)+1); % matched standard superperiod
%figure; atplot(c23,'inputtwiss',ld(end));

% chop C23 and SP at S19
inds19c23=findcells(c23,'FamName','S19');
inds19sp=findcells(sp,'FamName','S19');

c23id=c23((inds19c23(1)+1):(inds19c23(2)-1));

c23l=c23(1:inds19c23(1));
c23r=c23(inds19c23(2):end);

[itw,~,~]=atlinopt(sp,0,inds19sp(1)+1);

figure;ataddmagnetnamestoplot(atplot(c23id,'inputtwiss',itw),c23id);

v3=atVariableBuilder(c23id,'QD4W',{'PolynomB',{2}});
v4=atVariableBuilder(c23id,'QF5W',{'PolynomB',{2}});
v5=atVariableBuilder(c23id,'QD6W',{'PolynomB',{2}});
v6=atVariableBuilder(c23id,'QF7W',{'PolynomB',{2}});
Variab=[v3,v4,v5,v6];

c1=struct(...
    'Fun',@(~,ld,~)mux(ld),...
    'Weight',1,...
    'RefPoints',[1:length(c23id)+1],...
    'Min',muxc-muprecision,...
    'Max',muxc+muprecision);

c2=struct(...
    'Fun',@(~,ld,~)muy(ld),...
    'Weight',1,...
    'RefPoints',[1:length(c23id)+1],...
    'Min',muyc-muprecision,...
    'Max',muyc+muprecision);

c4a=atlinconstraint(length(c23id)+1,{{'beta',{1}}},itw(1).beta(1),itw(1).beta(1),1);
c4b=atlinconstraint(length(c23id)+1,{{'beta',{2}}},itw(1).beta(2),itw(1).beta(2),1);
c4c=atlinconstraint(length(c23id)+1,{{'Dispersion',{1}}},itw(1).Dispersion(1),itw(1).Dispersion(1),1);
c4d=atlinconstraint(length(c23id)+1,{{'alpha',{1}}},itw(1).alpha(1),itw(1).alpha(1),1);
c4e=atlinconstraint(length(c23id)+1,{{'alpha',{2}}},itw(1).alpha(2),itw(1).alpha(2),1);

Constr=[c1,c2,c4a,c4b,c4c,c4d,c4e];%,c4d,c4e

c231id=c23id;
c231id=atmatch(c231id,Variab,Constr,10^-9,2000,3,@fminsearch,'inputtwiss',itw);%,ld(1),'fminsearch');%
c231id=atmatch(c231id,Variab,Constr,10^-15,500,3,@lsqnonlin);%,ld(1),'fminsearch');%

figure;atplot(c231id,'inputtwiss',itw);

c231=[c23l; c231id; c23r];

% 
% figure;
% atplot(c231);
% figure;
% atplot(repmat(c231,16,1));

return


function SR1=matchSR(...
    sp,c23,...
    Qx,Qy,muprecision,...
    betxH,betyL,alphs19,etas19,...
    indcent,inddx)
% match optics in sp

%figure; atplot(c23,'inputtwiss',ld(end));
ring=[repmat(sp,9,1);c23;repmat(sp,6,1)];
initc23=length(sp)*9+1;
endc23=initc23+length(c23);

v1=atVariableBuilder(ring,'QF2',{'PolynomB',{2}});
v2=atVariableBuilder(ring,'QD3',{'PolynomB',{2}});
v3=atVariableBuilder(ring,'QD4',{'PolynomB',{2}});
v4=atVariableBuilder(ring,'QF5',{'PolynomB',{2}});
v5=atVariableBuilder(ring,'QD6',{'PolynomB',{2}});
v6=atVariableBuilder(ring,'QF7',{'PolynomB',{2}});

v7=atVariableBuilder(ring,'QD4W',{'PolynomB',{2}});
v8=atVariableBuilder(ring,'QF5W',{'PolynomB',{2}});
v9=atVariableBuilder(ring,'QD6W',{'PolynomB',{2}});
v10=atVariableBuilder(ring,'QF7W',{'PolynomB',{2}});

Variab=[v1,v4,v5,v6,v7,v8,v9,v10];%v2,v3,

c1=struct(...
    'Fun',@(~,ld,~)mux(ld),...
    'Weight',1,...
    'RefPoints',[1:(length(ring)+1)],...
    'Min',Qx,...
    'Max',Qx);

c2=struct(...
    'Fun',@(~,ld,~)muy(ld),...
    'Weight',1,...
    'RefPoints',[1:(length(ring)+1)],...
    'Min',Qy,...
    'Max',Qy);

c123=struct(...
    'Fun',@(~,ld,~)mux(ld),...
    'Weight',1,...
    'RefPoints',[1:(endc23-1)],...
    'Min',Qx/16*10,...-muprecision,...
    'Max',Qx/16*10);%+muprecision);

c223=struct(...
    'Fun',@(~,ld,~)muy(ld),...
    'Weight',1,...
    'RefPoints',[1:(endc23-1)],...
    'Min',Qy/16*10,...-muprecision,...
    'Max',Qy/16*10);%+muprecision);

c3=atlinconstraint(1,{{'beta',{1}}},betxH,betxH,1);
c3a=atlinconstraint(1,{{'alpha',{2}}},0.0,0.0,1);

c4=atlinconstraint(inddx(1),{{'alpha',{2}}},alphs19,alphs19,1);

%c4=atlinconstraint(indcent(2),{{'beta',{1}}},betxL,betxL,1);
c5=atlinconstraint(indcent(2),{{'beta',{2}}},betyL,betyL,1);
c5a=atlinconstraint(indcent(2),{{'alpha',{1}}},0.0,0.0,1);

c6=atlinconstraint(inddx(1),{{'Dispersion',{1}}},etas19,etas19,1);

Constr=[c1,c2,c123,c223,c3,c3a,c4,c5,c5a,c6];%

Qx
Qy

ring1=ring;
ring1=atmatch(ring1,Variab,Constr,10^-8,10,3,@fminsearch);%,'fminsearch');%

ring1=atmatch(ring1,Variab,Constr,10^-15,10,3,@lsqnonlin);%,'fminsearch');%


SR1=ring1;

return


function c231=matchc23simple(...
    c23,...
    muxc,muyc)
% match mu in c23
% 

v5=atVariableBuilder(c23,'QD6W',{'PolynomB',{2}});
v6=atVariableBuilder(c23,'QF7W',{'PolynomB',{2}});
Variab=[v5,v6];

c1=struct(...
    'Fun',@(~,ld,~)mux(ld),...
    'Weight',1,...
    'RefPoints',[1:length(c23)+1],...
    'Min',muxc,...
    'Max',muxc);

c2=struct(...
    'Fun',@(~,ld,~)muy(ld),...
    'Weight',1,...
    'RefPoints',[1:length(c23)+1],...
    'Min',muyc,...
    'Max',muyc);


Constr=[c1,c2];

c231=c23;
c231=atmatch(c231,Variab,Constr,10^-12,1000,3,@fminsearch);%,'fminsearch');%

return



function m=muy(lindata)

m=lindata(end).mu(2)/2/pi;

return

function m=mux(lindata)

m=lindata(end).mu(1)/2/pi;

return

function [r11]=RM44(lindata,ind1,ind2)
% get value of of indeces ind1 and ind2 (1 to 4) of 
% M44 between two points first and last

Mlast=lindata(2).M44;
Mfirst=lindata(1).M44;

Mfirstinv=[[Mfirst(2,2),-Mfirst(1,2);-Mfirst(2,1),Mfirst(1,1)],...
            zeros(2,2);...
            zeros(2,2),...
            [Mfirst(4,4),-Mfirst(3,4);-Mfirst(4,3),Mfirst(3,3)]];

R=Mlast*Mfirstinv;
%R=Mlast/Mfirst;

r11=R(ind1,ind2);

return

