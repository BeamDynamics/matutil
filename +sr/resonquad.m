function resp=resonquad(atmodel,qpok)

sext=atmodel.get('sx');
idquad=cellfun(@getidq,tango.Device('sr/qp-n/all').CorrectorNames.read');
sext=sext(idquad,:);
nuh=atmodel.nuh;
nuv=atmodel.nuv;
px1=ceil(2*nuh);
px2=floor(2*nuh);
pz1=ceil(2*nuv);
pz2=floor(2*nuv);
%				response 8x32
resp=[...
    0.02*r2nux(sext,px1,0.5*px1);...    % 2nux=73
    0.02*r2nux(sext,px2,0.5*px2);...    % 2nux=72
    0.02*r2nuz(sext,pz1,0.5*pz1);...    % 2nuz=29
    0.02*r2nuz(sext,pz2,0.5*pz2);...    % 2nuz=28
    ];

dlmwrite('quadcor.csv',resp,'precision',10);    % Response for the new device

figure(1);
resonplot(resp,[px1 px2 pz1 pz2]);

% ------------------- For old apps -------------------
rsort=selectfrom(@eq,selcor(6),idquad,1:length(idquad));
nquad=length(rsort);
a=[resp(:,rsort);ones(1,nquad)];                % Response and correction
[u,s,v]=svd(a(:,qpok),0);                       % for the old app
lambda=diag(s) %#ok<NOPRT>
keep=1:length(lambda);
binv=v(:,keep)*diag(1./lambda(keep))*u(:,keep)';
ainv=zeros(nquad,size(a,1));
ainv(qpok,:)=binv;
response=a';	%transpose for C order
response(~qpok,:)=0; %#ok<NASGU>
correction=[ainv(:,1:8) zeros(nquad,1)]';	%transpose for C order

i_rms = std(correction') %#ok<UDIM,NOPRT,NASGU>
save(sprintf('quad%02dcor',nquad),'response','correction');

    function exc=r2nux(sext,p,nux)
        disp(['2*nuX = ' num2str(p)]);
        phase=2*pi*(2*nux*sext(:,3)+(p-2*nux)*sext(:,1));
        exc=[sext(:,2).*cos(phase) sext(:,2).*sin(phase)]';
    end

    function exc=r2nuz(sext,p,nuz)
        disp(['2*nuZ = ' num2str(p)]);
        phase=2*pi*(2*nuz*sext(:,6)+(p-2*nuz)*sext(:,1));
        exc=[sext(:,5).*cos(phase) sext(:,5).*sin(phase)]';
    end

    function idq=getidq(nm)
        try
            idq=sr.sxindex(nm);
        catch
            idq=1;  % Recreate N4/C4
        end
    end

end
