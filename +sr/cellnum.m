function [kdx,celnum,id,rid]=cellnum(npercell,varargin)
%CELLNUM   gives cell number and index in cell
%
%[KDX,CELL,ID]=CELLNUM(NPERCELL,IDX) returns the cell number and the index in cell
%   of the IDX'th elements in a vector with NPERCELL elements per cell,
%   starting at the beginning of cell 4
%
% IDX is any of:
%       1) logical array of length 32*NPERCELL
%       2) vector of indexes in the range [1 32*NERCELL] (1 is Cell 4/1st item)
%
%[KDX,CELL,ID]=CELLNUM(NPERCELL,CELL,ID)
%[KDX,CELL,ID]=CELLNUM(NPERCELL,[CELL,ID])
%       CELL in [1 32], ID in [1 NPERCELL]
%
%[KDX,CELL,ID,RID] returns in addition the index in the cell when odd cells are
%mirrors of even cells
%
%See also: bpmname, bpmindex, qpname, sxname

[celnum,id,kdx]=getcellnum(npercell,32,3,varargin{:});
if nargout >= 4
    reverse=logical(bitand(celnum,1));
    rid=id;
    rid(reverse)=npercell+1-rid(reverse);
end
end
