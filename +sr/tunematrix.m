function matr = tunematrix(atmodel)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

Bro=6.04E9/2.99792458E8;
qpnames=atmodel.name('qp');
qpidx=atmodel.select('qp');
qpstrengths=atmodel.get(1,'qp');
famtunes=cellfun(@getsum,{'QF2','QD3','QD4','QF5','QD6','QF7'},...
    'UniformOutput',false);
matr=cat(2,famtunes{:});
matr(:,[7 14])=getsingle('QF5W');
matr(:,[8 13])=getsingle('QD4W');
matr(:,[9 12])=getsingle('QD6W');
matr(:,[10 11])=getsingle('QF7W');

    function v=getsum(famname)
        idx=atmodel.select(famname);
        strength=mean(qpstrengths(idx(qpidx)));
        fam1=['SR/PS-' famname '/1'];
        slope=i2slope(fam1,gl2i(fam1,Bro*strength))/Bro;
        bxbz=sum(atmodel.get([3 8],famname));
        v=[bxbz(:,1).*slope -bxbz(:,2).*slope]'/4/pi;
    end
    function v=getsingle(famname)
        idx=atmodel.select(famname);
        strength=num2cell(qpstrengths(idx(qpidx)));
        fam1=qpnames(idx(qpidx));
        slope=cellfun(@(nm,str) i2slope(nm,gl2i(nm,Bro*str))/Bro,fam1,strength);
        bxbz=atmodel.get([3 8],famname);
        v=[bxbz(:,1).*slope -bxbz(:,2).*slope]'/4/pi;
    end
end
