function varargout=bpmname(idx,format)
%SRBPMNAME	returns the name of BPM with index idx
%
%BPMNAME=BPMNAME(IDX)	returns the name of BPM with index IDX
% IDX: vector of indexes in the range [1 224] (1 is C4-1)
%       or n x 2 matrix in the form [cell index] with
%       cell in [1 32], index in [1 7]
%
%BPMNAME=BPMNAME(IDX,FORMAT)	uses FORMAT to generate the name
%                       (default : 'SR/D-BPMLIBERA/C%i-%i')
%
%[NM1,NM2...]=BPMNAME(...)    May be used to generate several names
%
%[...,KDX]=BPMNAME(...)       returns in addition the "hardware" index KDX
%
%KDX=BPMNAME(IDX,'')          returns only the "hardware" index KDX
%
%   See also BPMINDEX, QPNAME, SXNAME

if nargin < 2, format='SR/D-BPMLIBERA/C%i-%i'; end

[celnum,id,kdx]=getcellnum(7,32,3,idx);
if isempty(format) || strcmp(format,'noname')
    varargout={};
else
    textfunc=@(celnum,id) sprintf(format,celnum,id);
    nms=arrayfun(textfunc,celnum,id,'UniformOutput',false);
    nin=length(nms);
    if (nargout==nin) || (nargout==(nin+1))
        varargout=nms;
    else
        varargout={nms};
    end
end

nout=length(varargout);
if nargout > nout
    varargout{nout+1}=kdx;
end
