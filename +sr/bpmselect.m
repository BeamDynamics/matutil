function idx=bpmselect(varargin)
%BPMSELECT	find index of BPM from its name
%
%MASK=BPMSELECT(BPMBAME)
%   BPMNAME:    string, char array or cell array of strings
%
%MASK=BPMSELECT(KDX)
%   KDX:        "hardware" index of the BPM
%
%MASK=BPMSELECT(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:7
%   CELL and ID must have the same dimensions or be a scalar
%
%   See also BPMNAME, BPMINDEX, QPSELECT, SXSELECT

if isnumeric(varargin{1})
    idx=getmask(7,32,3,varargin{:});
else
    idx=getmask(7,32,3,@sr.bpmdecode,varargin{:});
end
end
