function idx=corselect(code,varargin)
%CORSELECT	find index of corrector from its name
%
%MASK=CORSELECT(CODE,SXNAME)
%   CODE:   1: 16 quad correctors
%           2: 16 skew quad correctors
%           3: 12 sextupole correctors
%           4:  8 horizontal correctors
%           5:  8 vertical correctors
%           6: 32 quad correctors      106: without qp-n4/c4
%           7: 32 skew correctors
%           8: all H steerers
%           9: all V steerers
%           10: 64 skew correctors      110: with qp-s4/c4
%           11: 12 sextupole corrector + 8 indep. sextupoles
%   SXNAME:    string, char array or cell array of strings
%
%MASK=CORSELECT(CODE,CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:7
%   CELL and ID must have the same dimensions or be a scalar
%
%   See also SR.CORNAME, SR.CORINDEX, SR.SXSELECT, SR.QPSELECT, SR.BPMSELECT

sel=false(1,224);
sel(selcor(code))=true;

if isnumeric(varargin{1})
    msk=getmask(7,32,3,varargin{:});
else
    msk=getmask(7,32,3,@bindex,varargin{:});
end
if any(~sel & msk)
    error('SX:wrongName','Wrong corrector name')
end
idx=msk(sel);

    function [celnum,id]=bindex(bname)
        [celnum,id,~]=sr.sxdecode(bname);
    end
end
