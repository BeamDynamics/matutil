function save_mag(filename,strength,varargin)
%SR.SAVE_MAG    Store the family currents in a text file
%
%SR.SAVE_MAG(FILENAME,CURRENTS)
%
%CURRENTS:  Family currents [A]
%
%SR.SAVE_MAG(FILENAME,STRENGTH,'sr')
%
%STRENGTHS:  Family strengths [rad, m^-1, m^-2]

devlist=sr.magname();
if ~isempty(varargin)
    Bro=6.04e9/2.99792458e8;
    strength=cellfun(@(nm,cur) gl2i(nm,cur),devlist,num2cell(strength.*Bro));
end

[fp,message]=fopen(filename,'wt');
if fp < 0
    error('File:Open','%s: %s',message,filename);
else
    for dev=1:length(devlist)
        fprintf(fp,'%s\t%.6f\n',devlist{dev},strength(dev));
    end
    fclose(fp);
end
end

