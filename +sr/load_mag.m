function [strength,devlist] = load_mag(filename,varargin)
%SR.LOAD_MAG    Read the values of magnet families from a file
%
%CURRENT=SR.LOAD_MAG(FILENAME)
%   Return the family currents [A]
%
%CURRENT=SR.LOAD_MAG(FILENAME,MAGLIST)
%   Return the family currents for the selected magnets [A]
%
%STRENGTH=SR.LOAD_MAG(...,'Energy',energy)
%STRENGTH=SR.LOAD_MAG(...,'BRo',bro)
%   Return the family strengths [rad, m^-1, m^-2]
%
%[...]=SR.LOAD_MAG(...,'FileFormat',format)
%
%FORMAT:    'srmag':        srmag file
%           'binaryfile':   full path of a srmag file
%           'textfile':     full path of a text file (name value)
%           'string':       filename id there a string of (name value) lines
%
%[...,DEVNAME]=SR.LOAD_MAG(...)
%   Return in addition the device names
%

[energy,args]=getoption(varargin,'Energy',NaN);
[bro,args]=getoption(args,'BRo',energy/2.997924e8);
[fileformat,args]=getoption(args,'FileFormat','srmag');
[def,args]=getoption(args,'Default',NaN);
devlist=getargs(args,{sr.magname()});
strength=load_setting(fileformat,filename,devlist,def);

if isfinite(bro)
    strength=cellfun(@i2gl,devlist,num2cell(strength))/bro;
end
end
