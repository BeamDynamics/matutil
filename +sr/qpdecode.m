function [celnum,id]=qpdecode(bname)

qpattern='^(?:(tango:)?sr/)?(?:(ps|m)-)?(\w+)/c(\d{1,2})(-[12])?(?:/\w+)?$';
toks=regexpi(bname,qpattern,'tokens');
if isempty(toks),error('QP:wrongName','Wrong quadrupole name'); end
magname=toks{1}{1};
celnum=str2double(toks{1}{2});
ext=toks{1}{3};
oddcell=logical(bitget(uint8(celnum),1));
if xor(isempty(ext) || strcmp(ext,'-1'),oddcell);
    sel='first';
else
    sel='last';
end
list={'QF2','QD3','QD4','QF5','QF5','QD4','QD6','QF7'};
id=find(strcmpi(magname,list),1,sel);
if isempty(id), error('QP:wrongName','Wrong quadrupole name'); end
if oddcell, id=9-id; end
end
