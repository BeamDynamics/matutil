function resp=resonskew(atmodel)

[bpm,sext]=atmodel.get('bpm','sx');
idskew=cellfun(@getids,tango.Device('sr/qp-s/all').CorrectorNames.read');
sext=sext(idskew,:);
nuh=atmodel.nuh;
nuv=atmodel.nuv;
h1=round(nuh+nuv);
h2=round(nuh-nuv);
p=0.5*(nuh+nuv-h1);
m=0.5*(nuh-nuv-h2);

scale=sext(:,4)';
resp=responsem(bpm(:,5:6),sext(:,5:6),nuv).*scale(ones(size(bpm,1),1),:);
%					response 228x64
resp=[...
    0.02*rnuxpnuz(sext,h1,nuh-p,nuv-p);...      % nux+nuz=50
    0.02*rnuxmnuz(sext,h2,nuh-m,nuv+m);...      % nux-nuz=23
    0.0045*resp ...                             % dispersion
    ];

dlmwrite('skewcor.csv',resp,'precision',10);    % Response for the new device

figure(2);
resonplot(resp(1:4,:),[h1 h2]);

% ------------------- For old apps -------------------
rsort=selectfrom(@eq,selcor(7),idskew,1:length(idskew));
nskew=length(rsort);
a=[resp(1:4,rsort);ones(1,nskew);resp(5:end,rsort)];
[u,s,v]=svd(a,0);
lambda=diag(s);
nbvect=length(lambda);
keep=1:min([10 nbvect]);
ainv=v(:,keep)*diag(1./lambda(keep))*u(:,keep)';

i_rms=std2(ainv(:,1:5)) %#ok<NOPRT,NASGU>
eta_rms=std2(a(6:end,:)*ainv(:,1:5)) %#ok<NOPRT,NASGU>

figure(3);
resonplot3(a*v(:,1:nbvect),[h1 h2]);
response=resp(1:4,rsort)';		%#ok<NASGU> %transpose for C order
correction=ainv(:,1:4)';        %#ok<NASGU> %transpose for C order
respdisp=resp(5:end,rsort)';	%#ok<NASGU> %transpose for C order
corrdisp=ainv(:,6:end)';        %#ok<NASGU> %transpose for C order

save(sprintf('skew%02dcor',nskew),'response','correction','respdisp','corrdisp');

    function exc=rnuxpnuz(sext,p,nux,nuz)
        disp(['nuX + nuZ = ' num2str(p)]);
        phase=2*pi*(nux*sext(:,3)+nuz*sext(:,6)+(p-(nux+nuz))*sext(:,1));
        ampl=sqrt(sext(:,2).*sext(:,5));
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

    function exc=rnuxmnuz(sext,p,nux,nuz)
        disp(['nuX - nuZ = ' num2str(p)]);
        phase=2*pi*(nux*sext(:,3)-nuz*sext(:,6)+(p-(nux-nuz))*sext(:,1));
        ampl=sqrt(sext(:,2).*sext(:,5));
        exc=[ampl.*cos(phase) ampl.*sin(phase)]';
    end

    function ids=getids(nm)
        try
            ids=sr.sxindex(nm);
        catch
            ids=224;    % Recreate S4/C3
        end
    end

end
