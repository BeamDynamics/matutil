function [idx,kdx,plane]=steerindex(varargin)
%STEERINDEX	find index of Steerer with name steername
%
%IDX=STEERINDEX(STEERNAME)
%   STEERNAME:    string, char array or cell array of strings
%
%IDX=STEERINDEX(CELL,ID)
%   CELL:   cell number in 1:32
%   ID:     index in cell, in 1:7
%   CELL and ID must have the same dimensions or be a scalar
%
%[IDX,KDX,PLANE]=STEERINDEX(...)
%   Returns the hardware index and the plane of the last steerer in the list
%
%   See also STEERNAME, STEERSELECT

[idx,kdx]=getindex(3,32,3,@bindex,varargin{:});

    function [celnum,id]=bindex(bname)
        [celnum,id,plane]=sr.steerdecode(bname);
    end
end
