function sp1=matchsuperperiodoptics(...
    sp,...
    muxc,muyc,...
    betxH,betyL,...
    betxL,betyH,...
    indcent,verbose)
% match optics in ESRF superpeiod
%
%
%see also: atmatch

if verbose, verb=3; end
if ~verbose, verb=0; end


v1=atVariableBuilder(sp,'QF2',{'PolynomB',{2}});
v2=atVariableBuilder(sp,'QD3',{'PolynomB',{2}});
v3=atVariableBuilder(sp,'QD4',{'PolynomB',{2}});
v4=atVariableBuilder(sp,'QF5',{'PolynomB',{2}});
v5=atVariableBuilder(sp,'QD6',{'PolynomB',{2}});
v6=atVariableBuilder(sp,'QF7',{'PolynomB',{2}});
Variab=[v1,v2,v3,v4,v5,v6];

c1=struct(...
    'Fun',@(~,ld,~)mux(ld),...
    'Weight',1,...
    'RefPoints',[1:length(sp)+1],...
    'Min',muxc,...
    'Max',muxc);

c2=struct(...
    'Fun',@(~,ld,~)muy(ld),...
    'Weight',1,...
    'RefPoints',[1:length(sp)+1],...
    'Min',muyc,...
    'Max',muyc);

dbeta=0.000001;
c3=atlinconstraint(1,{{'beta',{1}}},betxH-dbeta,betxH+dbeta,1);
c4=atlinconstraint(1,{{'beta',{2}}},betyH-dbeta,betyH+dbeta,1);

c5=atlinconstraint(indcent(2),{{'beta',{1}}},betxL-dbeta,betxL+dbeta,1);
c6=atlinconstraint(indcent(2),{{'beta',{2}}},betyL-dbeta,betyL+dbeta,1);

Constr=[c1,c2,c3,c4,c5,c6];

sp1=sp;
sp1=atmatch(sp,Variab,Constr,10^-12,1000,verb,@fminsearch);%,'fminsearch');%
sp1=atmatch(sp1,Variab,Constr,10^-15,100,verb,@lsqnonlin);%,'fminsearch');%


return

function m=muy(lindata)

m=lindata(end).mu(2)/2/pi;

return

function m=mux(lindata)

m=lindata(end).mu(1)/2/pi;

return