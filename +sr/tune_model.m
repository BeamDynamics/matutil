function [quadmat]=tune_model(varargin)
%TUNE_MODEL	produces tune correction matrix for
%Tango://ebs-simu:10000/srdiag/beam-tune/adjust
%	[quadmat]=tune_model()
%
%TUNE_MODEL()               takes the lattice from
%           $APPHOME/sr/optics/settings/theory/betamodel.mat
%TUNE_MODEL('opticsname')   takes the lattice from
%           $APPHOME/sr/optics/settings/opticsname/betamodel.mat
%TUNE_MODEL(path)           takes the lattice from
%           path or path/betamodel.mat or path/betamodel.str
%TUNE_MODEL(AT)             Uses the given AT structure
%

args={pwd};
args(1:length(varargin))=varargin;
pth=args{1};

atmodel=sr.model(pth);

quadmat=sr.tuneresponse(atmodel);

end
