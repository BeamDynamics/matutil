function [celnum,id]=bpmdecode(bname)

bpattern='^(?:(tango:)?sr/d-bpm(libera)?/)?c(\d{1,2})-([1-7])$';
toks=regexpi(bname,bpattern,'tokens');
if isempty(toks),error('BPM:wrongName','Wrong BPM name'); end
celnum=str2double(toks{1}{1});
id=str2double(toks{1}{2});
end
