function varargout=mkdir2(parentdir,newdir)
%mkdirMKDIR Make new directory.
% [SUCCESS,MESSAGE,MESSAGEID] = MKDIR(PARENTDIR,NEWDIR) makes a new
% directory, NEWDIR, under the parent, PARENTDIR. All intermediate
% directories are created if necessary.
%
% see also mkdir

if ~isdir(parentdir)
    [path,name,ext,vers]=fileparts(parentdir);
    [success,message,messageid]=mkdir2(path, [name ext vers]);
    disp(['Creating ' [name ext vers]]);
    if ~success, error(messageid,message); end
end
[varargout{1:nargout}]=mkdir(parentdir,newdir);
