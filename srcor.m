function srcor(plane,values)
%SRCOR(plane,values)	increments the value of the steerers by specified value
%	plane:	0=horizontal,	1=vertical
%	values:	vector of 96 values in radians

if length(values(:)) ~= 96
   error('srcor needs a vector of 96 current values');
end

if plane == 0
   coef=load_corcalib('h');	% rad / A
   coef=coef(:)*ones(1,16);
elseif plane == 1
   coef=load_corcalib('v');	% rad / A
   coef=coef(:)*ones(1,16);
else
   error('plane must be 0 or 1');
end

cors=values(:)./coef(:);
tmpfile=[tempname '.mat'];
save(tmpfile,'cors');
[s,w]=unix(['matlab_steerer ' num2str(plane) ' ' tmpfile]);
delete(tmpfile);
disp(w);
