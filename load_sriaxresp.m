function [resp,current]=load_sriaxresp(filename,varargin)
%LOAD_SRIAXRESP    get measured response matrix
%
%RESP=LOAD_SRIAXRESP(DIRNAME,PLANE,STEERLIST,KGAIN,BGAIN)
%
%DIRNAME:   Directory containing measured data
%PLANE:     'V', 'H2V'
%STEERLIST: list of steerers (1..96)
%KGAIN:     Optional gain factor applied to steerers
%BGAIN:     Optional gain factor applied to IAXs
%
%RESP:    normalized BPM response [m]
%
%RESP=LOAD_SRIAXRESP(DIRNAME,OPTICS,PLANE,STEERLIST,KGAIN,BGAIN)
%		takes the steerer calibration from the specified optics dir.
%		and return the normalized response [m]
%
%OPTICS   : may be one of the following:
%
%- '/machfs/appdata/sr/optics/settings/opticsname'[,'sr']:
%			full path of optics directory
%- 'sr' or 'sy':	machine name
%			path defaults to $(APPHOME)/mach/optics/settings/theory
%- 'opticsname'[,'sr']:	optics name and machine
%
%
%[RESP,CURRENT]=LOAD_SRIAXRESP(DIRNAME,PLANE,STEERLIST)
%           returns the non-normalized response and the corresponding
%           excitations
%RESP:    IAX response [m]
%CURRENT: list of steerer currents [A]
%

nbt=32;
nkt=96;
[path,args,mach]=getmodelpath(varargin{:});
[plane,calinfo]=selectplane(args{1},{'iaxH','iaxV','iaxV2H','iaxH2V'},{8,9,8,9});
if length(args) >= 2
   steerlist=args{2};
else
   steerlist=1:nkt;
end
kcalib=load_corcalib(mach,path,calinfo);
if length(args) >= 3
   kcalib=kcalib./varargin{3};
end
if length(args) >= 4
   bcalib=varargin{4};
else
   bcalib=ones(nbt,1);
end
kcalib=kcalib(steerlist);
resp=NaN(nbt,nkt);
current=NaN(1,nkt);
if isdir(filename)
   try
       
	  iax=load(fullfile(filename,plane));
 	  iax_index=mod(iax.iax_list-4,32)+1;
%	  iax_index=mod(iax.iax_list([1:6 8 7 9:11])-4,32)+1;	%cabling error
	  scaling=bcalib(iax_index,ones(1,length(iax.steer_list)));
	  resp(iax_index,iax.steer_list)=1.0e-6*iax.resp'.*scaling;
	  current(iax.steer_list)=iax.curr;
   catch
	  iax_index=[];
   end
end
resp=resp(:,steerlist);
current=current(steerlist);

if nargout < 2
   resp=resp./kcalib(ones(nbt,1),:)./current(ones(nbt,1),:);
end
