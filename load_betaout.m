function [params,nux,nuz,ll,alpha,emitx,emitz,strname]=load_betaout(path)

if nargin < 1, path=pwd;end
try
    load(fullfile(path,'betaout'));
catch
end
if exist('params','var') == 1
   disp('Using betaout.mat...');
   if nargout > 1
   periods=params.periods; %#ok<NODEF>
   nux=params.nuh;
   nuz=params.nuv;
   emitx=params.emith;
   emitz=params.emitv;
   ll=params.ll;
   alpha=params.alpha;
   strname=params.strname;
   end
elseif exist('periods','var') == 1
   disp('Using betaout.mat...');
   params=optistruct(periods,nux,nuz,ll,alpha,emitx,emitz,strname); %#ok<NODEF>
else
   [fid, message]=fopen(fullfile(path,'BETAOUT'),'r'); %#ok<NASGU>
   if fid >= 0
      disp('reading BETAOUT...');
      line=fskip(fid,'periods=');
      periods=sscanf(line,'%*s %i');
      line=fskip(fid,'filename=');
      strname=sscanf(line,'%*s %s') %#ok<NOPRT>
      line=fskip(fid,'FIRST-ORDER'); %#ok<NASGU>
      line=fskip(fid,'FIRST-ORDER'); %#ok<NASGU>
      line=fskip(fid,'CELL LENGTH');
      vals=sscanf(line,'%*s %*s = %lg');
      ll=periods*vals %#ok<NOPRT>
      line=fskip(fid,'ALPHAP');
      vals=sscanf(line,'%*s %lg');
      alpha=vals(1) %#ok<NOPRT>
      line=fskip(fid,'INVARIANT',6);
      vals=sscanf(line,'%*s %lg %lg %lg %lg');
      emitx=vals(2) %#ok<NOPRT>
      line=fskip(fid,'BETATRON TUNES',2);
      vals=sscanf(line,'%*s %lg');
      nux=vals(1) %#ok<NOPRT>
      nuz=vals(2) %#ok<NOPRT>
      fclose(fid);
   else
      periods=input('periods: ');
      ll=input('machine length: ');
      nux=input('"BETA" h tune: ');
      nuz=input('"BETA" v tune: ');
      alpha=input('"BETA" alpha: ');
      emitx=input('"BETA" emittance: ');
      strname='?';
   end
   emitz=input('coupling: ')*emitx;
   params=optistruct(periods,nux,nuz,ll,alpha,emitx,emitz,strname);
   eval('save(matfile,''params'')');
end
if nargout > 1
   params=periods;
end
