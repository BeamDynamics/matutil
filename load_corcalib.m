function varargout=load_corcalib(varargin)
%LOAD_CORCALIB			loads corrector calibration
%
%[H,V,S,Q]=LOAD_CORCALIB(OPTICS,...);
%
%The OPTICS argument may be one of the following:
%
%- 'sr'|'sy'
%           machine name, path defaults to $(APPHOME)/mach/optics/settings/theory
%- ['sr'|'sy',]'/machfs/appdata/sr/optics/settings/opticsname':
%           full path of optics directory
%- ['sr'|'sy',]'opticsname'
%           machine and optics name
%
%H,V : hor./vert. steerer calibration in radians/A
%S,Q : skew/normal quad corrector calibration in m-1/A
%
%[COEF1,...]=LOAD_CORCALIB(MACH,PLANE1,...) returns the selected calibs
%
%PLANE : 'h' for horizontal steerers	[1x6]
%        'v' for horizontal steerers	[1x6]
%        's' for skew quad correctors	[32x1]
%        'q' for normal quad correctors	[1x1]
%
%         1: 16 quad correctors         [1x16]
%         2: 16 skew quad correctors	[1x16]
%         3: 12 sextupole correctors	[1x12]
%         4:  8 horizontal correctors	[1x8]
%         5:  8 vertical correctors     [1x8]
%         6: 32 quad correctors         [1x32]
%         7: 32 skew correctors         [1x32]
%         8: all H steerers             [1x96]
%         9: all V steerers             [1x96]
%         10: 64 skew correctors        [1x64]
%
%Values are taken from "corcalib.mat" if present in the optics directory.
%Otherwise, defaults values are used.

[pth,planes,mach]=getmodelpath(varargin{:});

if strcmp(mach,'sr')				% default values
    h=[4.398E-4 3.762E-4 4.368E-4 4.368E-4 3.762E-4 4.398E-4];
    v=[2.693E-4 2.508E-4 2.742E-4 2.742E-4 2.508E-4 2.693E-4];
    q=6.48e-3;
    slist=[4.983e-3;4.420e-3;4.420e-3;4.420e-3;4.420e-3;4.983e-3];
    %         S4       S13      S20      S20      S13      S4
    %                    calibration LF 16/01/2008
elseif strcmp(mach,'sy')
    h=3.79587e-3;
    v=3.93469e-3;
    q=[];
    s=[];
end

try						% local data
    load(fullfile(pth,'corcalib.mat'));
    disp(['Using ' fullfile(pth,'corcalib.mat')]);
    if exist('comment','var'), disp(comment); end
catch %#ok<CTCH>
    disp('Using default calibration');
end

if strcmp(mach,'sr')
    if exist('slength','var') == 0
        slength=ones(14,16);
    end
    xs=zeros(14,16);
    xs([1 3 5 10 12 14],:)=slist(:,ones(1,16));
    xs=xs.*slength;
    s=xs(selcor(7));
    
%     id16=7;
%     id23=10;
%     id30=14;
%     slength=ones(14,16);
%     shortsx=sub2ind(size(xs),[14 1 14 1],[id16-1 id16 id30-1 id30]);
%     shortsx=sub2ind(size(xs),[14 1 7 8 14 1],[id16-1 id16 id23 id23 id30-1 id30]);
%     slength(5,13)=-1;   % S20/C28 inverted
%     slength(shortsx)=0.551;   % 0.551 from Gael's simulation
%     comment=input('Comment: ','s');
%     save corcalib0 h v slist slength comment
end

if isempty(planes)
    varargout={h,v,s,q};
else
    coef=struct('h',h,'v',v,'q',q,'s',s);
    varargout={};
    for i=1:length(planes)
        plcode=planes{i};
        if isnumeric(plcode)
            idx=selcor(plcode);
            switch plcode
                case {4,8}      % H steerers
                    xh=zeros(14,16);
                    xh([2 4 6 9 11 13],:)=h(ones(16,1),:)';
                    xh=xh.*slength;
                    val=xh(idx)';
                case {5,9}      % V steerers
                    xv=zeros(14,16);
                    xv([2 4 6 9 11 13],:)=v(ones(16,1),:)';
                    xv=xv.*slength;
                    val=xv(idx)';
                case {1,6,106}      % normal quads
                    xq=q*ones(14,16).*slength;
                    val=xq(idx);
                case {2,7,10,110}   % skew quads
                    val=xs(idx)';
                case 3          % sextus
                    sxcal=[0;0;0;0;0.383;0;0.443;0.443;0;0.383;0;0;0;0];
                    sxcal=0.5*sxcal(:,ones(1,16));
                    sxcal(7:8,10)=0.5*0.239;
                    val=sxcal(idx)';
            end
        else
            val=coef.(lower(plcode(1)));
        end
        varargout=[varargout{:} {val}];
    end
end
