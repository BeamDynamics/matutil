function save_rsp(a,plane,di)
%SAVE_RSP(plane,a)
%	a:	response matrix (mm/mA)
%	plane:	'h' or 'v'
%	di:	delta I (A)

[nb,nk]=size(a);

if strcmp(plane,'h')
filename='resHmod.rsp';
border=[1 2 4 6 9 11 13 14 7 8 3 5 10 12];
elseif strcmp(plane,'h2v')
filename='resH2Vmod.rsp';
border=[1 2 3 5 10 12 13 14 4 6 9 11 7 8];
elseif strcmp(plane,'v2h')
filename='resV2Hmod.rsp';
border=[1 2 4 6 9 11 13 14 7 8 3 5 10 12];
elseif strcmp(plane,'v')
filename='resVmod.rsp';
border=[1 2 3 5 10 12 13 14 4 6 9 11 7 8];
else
error('Plane must be ''h'',''v'',''h2h'',''v2h''.');
end

nbc=length(border);
periods=size(a,1)/nbc;
nkc=size(a,2)/periods;
if length(di) == nkc
   deltaI=di(:);
   deltaI=reshape(deltaI(:,ones(1,periods)),1,nk);
else
   deltaI=di*ones(1,nk);
end

cell_list=(0:31)+4;
cell_list=cell_list-32*(cell_list>32);

bpm_num=[1:7 1:7]';				% generate bpm names
bpm_num=[reshape(cell_list(ones(nb/32,1),:),nb,1),...
         reshape(bpm_num(:,ones(periods,1)),nb,1)];

steer_num=[6;19;22;22;19;6];			% generate steerer names
steer_num=[reshape(steer_num(:,ones(periods,1)),nk,1),...
	   reshape(cell_list(ones(nk/32,1),:),nk,1)];

bpm=reshape(1:nb,nbc,periods)';
bpm=bpm(:,border);
steerer=reshape(1:nk,nkc,periods)';

a2=a(bpm(:),steerer(:));			% reorder data
bpm_num=bpm_num(bpm(:),:);
steer_num=steer_num(steerer(:),:);
deltaI=deltaI(steerer(:));

fid=fopen(filename,'w');
fprintf(fid,'\t');
fprintf(fid,'\tC%d-%d',bpm_num');
fprintf(fid,'\n');
for i=1:nk
fprintf(fid,'%.4f\tSR/ST-H%i/C%i',deltaI(i),steer_num(i,:));
fprintf(fid,'\t%.4f',1000.0*deltaI(i)*a2(:,i));
fprintf(fid,'\n');
end
fclose(fid);
