function selobj
global CLICKPT

if exist('CLICKPT')
   if ( CLICKPT == get( gcf,'CurrentPoint') )
      temp = get( gco,'Type');
      if ( strcmp( temp,'line') )
         set( gco,'Color', abs(1-get(gco,'Color')) );
         if strcmp( get( gco,'Selected'),'on')
          set( gco,'Selected','off');
         else
          set( gco,'Selected','on');
         end;
      elseif(  ( strcmp( temp,'text') ) | ( strcmp( temp,'axes') ) );
         if strcmp( get( gco,'Selected'),'on');
          set( gco,'Selected','off');
         else;
          set( gco,'Selected','on');
         end;
      end;
   end
end
CLICKPT = get( gcf,'CurrentPoint');
