function dlist=dirfind(basedir,filename)
%DIRFIND   Find subdirectories containing filename
%
%DIRLIST=DIRFIND(BASEDIR,FILENAME)
%   returns all directories below BASEDIR containing FILENAME
%
dlist={};
dirlist=dir(basedir);
for fs=dirlist'
    if (fs.name(1)~= '.') && fs.isdir
        fspath=fullfile(basedir,fs.name);
%       fprintf('Looking at %s\n',fspath);
        if exist(fullfile(fspath,filename),'file') == 2
            dlist=[dlist fspath]; %#ok<AGROW>
        else
            dlist=[dlist dirfind(fspath,filename)]; %#ok<AGROW>
        end
    end
end
end
