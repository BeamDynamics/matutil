function bw(color)
%BW(color) sets background of current figure to 'color'
%	BW('k') resets normal behaviour

    fig=gcf;
    if nargin < 1, color='w'; end
    if color == 'w'
    whitebg(fig);
    set(fig,'defaultaxescolororder',[0 0 0]);
    else
    whitebg(gcf,color);
    set(fig,'defaultaxescolororder','remove')
    end
