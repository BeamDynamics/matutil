function y = median2(x,dim)
%MEDIAN2 Median value.
%   For vectors, MEDIAN2(x) is the median value of the elements in x.
%   For matrices, MEDIAN2(X) is a row vector containing the median value
%   of each column.  For N-D arrays, MEDIAN(X) is the median value of the
%   elements along the first non-singleton dimension of X.
%
%   MEDIAN2(X, DIM) takes the median along the dimension DIM of X.
%
%   Example: If X = [1 2 4 4; 3 4 6 6; 5 6 8 8; 5 6 8 8];
%
%   then median2(X) is [4 5 7 7] and median(X,2) is [3; 5; 7; 7]
%
%   Class support for input X:
%      float: double, single
%      integer: uint8, int8, uint16, int16, uint32, int32, uint64, int64
%
%   See also MEAN2, STD2, MIN2, MAX2, VAR2, COV, MODE.

%   Copyright 1984-2014 The MathWorks, Inc.

if isempty(x) % Not checking flag in this case
    if nargin == 1 || (nargin == 2 && ischar(dim))
        
        % The output size for [] is a special case when DIM is not given.
        if isequal(x,[])
            if isinteger(x)
                y = zeros(class(x)); %#ok<*ZEROLIKE>
            else
                y = nan(class(x));
            end
            return;
        end
        
        % Determine first nonsingleton dimension
        dim = find(size(x)~=1,1);
        
    end
    
    s = size(x);
    if dim <= length(s)
        s(dim) = 1;                  % Set size to 1 along dimension
    end
    if isinteger(x)
        y = zeros(s,class(x));
    else
        y = nan(s,class(x));
    end
    
    return;
end

dimSet = true;
if nargin == 1
    dimSet = false;
end

sz = size(x);

if dimSet
    if ~isscalar(dim) || ~isreal(dim) || floor(dim) ~= ceil(dim) || dim < 1
        error(message('MATLAB:getdimarg:dimensionMustBePositiveInteger'));
    end
    
    if dim > numel(sz)
        y = x;
        return;
    end
end


if isvector(x) && (~dimSet || sz(dim) > 1)
    % If input is a vector, calculate single value of output.
    x = sort(x);
    nCompare = length(x);
    if isnan(x(nCompare))        % Check last index for NaN
        nCompare = find(~isnan(x), 1, 'last');
        if isempty(nCompare)
            y = nan(class(x));
            return;
        end
    end
    half = floor(nCompare/2);
    y = x(half+1);
    if 2*half == nCompare        % Average if even number of elements
        y = meanof(x(half),y);
    end
else
    if ~dimSet              % Determine first nonsingleton dimension
        dim = find(sz ~= 1,1);
    end
    
    % Reshape and permute x into a matrix of size sz(dim) x (numel(x) / sz(dim))
    if dim ~= 1
        n1 = prod(sz(1:dim-1));
        n2 = prod(sz(dim+1:end));
        if n2 == 1 % because reshape(x, [n1, n2, 1]) errors for sparse matrices
            x = reshape(x, [n1, sz(dim)]);
        else
            x = reshape(x, [n1, sz(dim), n2]);
        end
        x = permute(x, [2 1 3]);
    end
    x = x(:, :);
    
    % Sort along columns
    x = sort(x, 1);
    
    if all(~isnan(x(end, :)))
        % Use vectorized method with column
        % indexing.  Reshape at end to appropriate dimension.
        nCompare = sz(dim);          % Number of elements used to generate a median
        half = floor(nCompare/2);    % Midway point, used for median calculation
        
        y = x(half+1,:);
        if 2*half == nCompare
            y = meanof(x(half,:),y);
        end
        
        y(isnan(x(nCompare,:))) = NaN;   % Check last index for NaN
        
    else
        
        % Get median of the non-NaN values in each column.
        y = nan(1, size(x, 2), class(x));
        
        % Number of non-NaN values in each column
        n = sum(~isnan(x), 1);
        
        % Deal with all columns that have an odd number of valid values
        oddCols = find((n>0) & rem(n,2)==1);
        oddIdxs = sub2ind(size(x), (n(oddCols)+1)/2, oddCols);
        y(oddCols) = x(oddIdxs);

        % Deal with all columns that have an even number of valid values
        evenCols = find((n>0) & rem(n,2)==0);
        evenIdxs = sub2ind(size(x), n(evenCols)/2, evenCols);
        y(evenCols) = meanof( x(evenIdxs), x(evenIdxs+1) );
        
    end
    
    % Now reshape output.
    sz(dim) = 1;
    y = reshape(y, sz);
end

%============================

function c = meanof(a,b)
% MEANOF the mean of A and B with B > A
%    MEANOF calculates the mean of A and B. It uses different formula
%    in order to avoid overflow in floating point arithmetic.

if isinteger(a)
    % Swap integers such that ABS(B) > ABS(A), for correct rounding
    ind = b < 0;
    temp = a(ind);
    a(ind) = b(ind);
    b(ind) = temp;
end
c = a + (b-a)/2;
k = (sign(a) ~= sign(b)) | isinf(a) | isinf(b);
c(k) = (a(k)+b(k))/2;

