function x=envdrift(s,x0,xp0)
%DRIFT computes beam size at locations s from waist (x0, x'0)
%	DRIFT(s, x0, xp0) generates a matrix x of beam size corresponding to
%	distances in matrix s form the waist described by x0, x'0

beta=x0/xp0;
x=x0 * sqrt((s/beta).^2 + 1);
