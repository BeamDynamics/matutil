function [nax,xx,yy]=replot(ax,varargin)
%REPLOT(AX) creates a cloned figure
%
%AX:  axes to be cloned
%
%REPLOT(AX,PROPERTYNAME,PROPERTYVALUE,...)
% Modifies the plot according to specified properties:
%   'XScale': multiplier for X values
%   'YScale': multiplier for Y values
%   'Style': linestyles (cell array)
%
%NAX=REPLOT(...)
% Return the new axes

rsrc=struct(varargin{:});
[xsc,rsrc]=getoption(rsrc,'XScale',1);
[ysc,rsrc]=getoption(rsrc,'YScale',1);
[style,rsrc]=getoption(rsrc,'Style','-o'); %#ok<ASGLU>

lns=reshape(findobj(ax,'Type','line'),[],1);
errb=reshape(findobj(ax,'Type','ErrorBar'),[],1);
nlns=length(lns);
nerrb=length(errb);
if ~iscell(style), style=repmat({style},nlns+nerrb,1); end
f=figure; %#ok<NASGU>
[x1,y1,labs1]=cellfun(@getxz,num2cell(lns(end:-1:1)),style(1:nlns),'UniformOutput',false);
[x2,y2,labs2]=cellfun(@geterrxz,num2cell(errb(end:-1:1)),style(nlns+1:end),'UniformOutput',false);
hold off
axis([xsc xsc ysc ysc].*axis(ax));
legend(labs1{:},labs2{:});
title(get(get(ax,'Title'),'String'));
xlabel(get(get(ax,'XLabel'),'String'));
ylabel(get(get(ax,'YLabel'),'String'));
grid on
if nargout >=1, nax=gca; end
if nargout >=2, xx=[x1;x2];yy=[y1;y2]; end

    function [x,y,lab]=getxz(h,style)
        x=xsc*get(h,'XData');
        y=ysc*get(h,'YData');
        lab=get(h,'DisplayName');
        plot(x,y,style);
        hold on
    end

    function [x,y,lab]=geterrxz(h,style)
        x=xsc*get(h,'XData');
        y=ysc*get(h,'YData');
        l=ysc*get(h,'LData');
        u=ysc*get(h,'UData');
        lab=get(h,'DisplayName');
        errorbar(x,y,l,u,style);
        hold on
    end

end
