%hdb.reader: HDB reader object
%
%A hdb.reader object connects to hdb ('ORACLE') or hdb++ ('CASSANDRA')
%and allows data extraction?
%
%Constructor:
%hdb.reader(hdb_type)
%
%Methods:
%attributes  Return the list of available signals
%extract     Extract data from HDB into a hdb.series
%dataset     Return a HDB dataset
%getinfo     Get a signal configuration
%getfullname Get the fully qualified signal name
%
%See also: hdb.series

classdef reader < handle
    
    properties (Access=private, Hidden)
        protocol
    end
    
    properties (Hidden)
        rdr
    end
    
    methods (Static, Access=private)
        
        val=process(dataset,siginfo)
        
        function dt = datim(inpt)
            %Generate a datetime object from string or datenum
            if ischar(inpt)
                dt=tcheck(inpt);
            elseif isnumeric(inpt)
                dt=datetime(inpt,'ConvertFrom','datenum','TimeZone','local');
            else
                dt=inpt;
                dt.TimeZone='local';
            end
            dt.Format='dd/MM/yyyy HH:mm:ss';
            
            function dt=tcheck(inpt)
                try
                    dt=datetime(inpt,'InputFormat','dd/MM/yyyy HH:mm:ss','TimeZone','local');
                    return
                catch
                end
                try
                    dt=datetime(inpt,'InputFormat','dd/MM/yyyy','TimeZone','local');
                    return
                catch
                end
                dt=datetime(inpt,'TimeZone','local');
            end
        end
    end
    
    methods
        function this=reader(type)
            %READER(HDB_TYPE)
            %
            %HDB_TYPE:  Can be 'ORACLE' or 'CASSANDRA'
            if nargin < 1
                type='CASSANDRA';
            end
            hhh=org.tango.jhdb.Hdb();
            switch type
                case 'CASSANDRA'
                    hhh.connectCassandra({'hdbr1','hdbr2','hdbr3'},'hdb','','');
                    this.protocol={'userhost','//orion','domain','.esrf.fr','port',':10000'};
                case 'ORACLE'
                    hhh.connectOracle()
                    this.protocol={'userhost','//aries'};
            end
            this.rdr=hhh.getReader();
            this.rdr.setExtraPointLookupPeriod(3600*24);
            this.rdr.enableExtraPoint();
        end
        
        varargout = extract(this,s1,s2,varargin)
        varargout = dataset(this,s1,s2,varargin)
        
        function attrs = attributes(this)
            %Return the list of available attributes
            %
            %ATTR_LIST=ATTRIBUTES()
            attrs=cell(this.rdr.getAttributeList());
        end
        
        function [signame,siginfo,param]=getinfo(this,signal)
            %Get signal configuration from HDB
            %[SIGNAME,SIGINFO,SIGPARAMS]=GETINFO(SIGNAL)
            %SIGNAME:   Fully qualified signal name
            %SIGINFO:   org.tango.jhdb.HdbSigInfo
            %SIGPARAMS: org.tango.jhdb.HdbSigParam
            signame=this.getfullname(signal);
            siginfo=this.rdr.getSigInfo(signame);
            try
                param=this.rdr.getLastParam(signame);
            catch err
                if isa(err,'matlab.exception.JavaException') && ...
                        isa(err.ExceptionObject,'org.tango.jhdb.HdbFailed')
                    warning('Hdb:Param','No configuration data for signal %s',signal);
                    param=org.tango.jhdb.HdbSigParam();
                    param.label=java.lang.String(signal);
                    param.unit=java.lang.String('');
                    param.format=java.lang.String('%7.3f');
                    param.display_unit=1;
                else
                    rethrow(err);
                end
            end
        end
        
        function sign=getfullname(this,signal)
            %get the fully qualified signal name
            cs=urlsplit(signal,'scheme','tango:',this.protocol{:});
            sign=strcat(cs.scheme,cs.userhost,cs.domain,cs.port,'/',cs.path);
        end
    end
    
    methods (Access=protected, Hidden)
        
        function [datasets,siginfos] = getdata(this,t0,t1,t2,varargin)
            %Get raw data from HDB
            %[DATASETS,SIGINFOS]=GETDATA(T0,T1,T2,SIGNAL1,SIGNAL2,...)
            [signames,siginfos]=cellfun(@(s) this.getsigtype(t0,s),varargin,'UniformOutput',false);
            datasets=cell(this.rdr.getData(signames,char(t1),char(t2),0))';
        end
        
        function [signame,siginfo] = getsigtype(this,t0,signal)
            %Get signal properties from HDB
            %[SIGNAME,SIGINFO]=GETSIGTYPE(T0,SIGNAL)
            [signame,sinfo,param]=this.getinfo(signal);
            siginfo.Name=signal;
            siginfo.Type=char(org.tango.jhdb.HdbSigInfo.typeStr(sinfo.type+1));
            siginfo.StartDate=t0;
            siginfo.Label=char(param.label);
            siginfo.Units=char(param.unit);
            if any(sinfo.type == [41 42])     % Special case for scalar "State" variables
                siginfo.Format='%s';
            else
                siginfo.Format=[char(param.format) ' ' siginfo.Units];
            end
            if isfinite(param.display_unit)
                siginfo.DisplayUnit=param.display_unit;
            else
                siginfo.DisplayUnit=1;
            end
            
            try                             % While info missing in parameters
                attrinfo=tango.Attribute(signal).attrinfo;
                siginfo.YLim=siginfo.DisplayUnit*[attrinfo.min_value attrinfo.max_value];
            catch
                siginfo.YLim=[-Inf Inf];
            end
        end
        
    end
    
end

