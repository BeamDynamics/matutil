function varargout = dataset(self,s1,s2,varargin)
%DATASET Return a HDB dataset
%
%[DATA1,DATA2,...]=DATASET(T1,T2,SIGNAL1,SIGNAL2,...)
%
%T1:        Start time, as a date string or date number
%T2:        End time, as a date string or date number
%SIGNALn:   String identifying the signal (ex.: 'sr/d-ct/1/current')
%
%DATAn:     HDB dataset

%See also:  extract

t1=self.datim(s1);
t2=self.datim(s2);

nout=nargin-3;
tic;
[data,~]=self.getdata(t1,t1,t2,varargin{:}); toc
if nargout==nout
    varargout=data;
else
    varargout={cat(2,data{:})};
end
end