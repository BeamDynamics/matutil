function val=process(dataset,siginfo,varargin)
%PROCESS    Process a HDB dataset into a matlab time series
%
%H=PROCESS(DATASET,SIGINFO)

persistent epoch sel
if isempty(epoch)
    epoch=datetime(0,'ConvertFrom','posixtime','TimeZone','local');
end
if isempty(sel)
    sel=hdb.typeinit();
end

t0=seconds(siginfo.StartDate-epoch);
sz=dataset.size();
typinfo=sel(dataset.getType()+1);

[defval,~]=getoption(varargin,'Default',typinfo.vini);

k=typinfo.dclass(0);
if typinfo.isarray        % Array
    t=NaN(sz,1);
    r=cell(typinfo.dsize,sz);
    for i=1:sz
        hdbdata=dataset.get(i-1);
        t(i)=hdbdata.getDataTime();
        if ~hdbdata.hasFailed()
            r{1,i}=reshape(typinfo.dclass(hdbdata.getValue()),1,[]);
            if typinfo.dsize == 2
                r{2,i}=reshape(typinfo.dclass(hdbdata.getWriteValue()),1,[]);
            end
        end
    end
    if isnumeric(k)
        try
            missing=cellfun(@isempty,r);
            tz=size(r{find(~missing,1)});
            r(missing)={defval*ones(tz)};
            val=hdb.series(cat(1,r{:}),1.0E-6*t-t0,'Name',siginfo.Label);
        catch err
            disp(err);
            val=hdb.cellseries(r',1.0E-6*t-t0,'Name',siginfo.Label);
        end
    else
        val=hdb.cellseries(r',1.0E-6*t-t0,'Name',siginfo.Label);
    end
elseif isfloat(k)
    t=double(dataset.getDataTimeArray());
    if typinfo.dsize == 2
        r=[dataset.getValueAsDoubleArray() dataset.getWriteValueAsDoubleArray()];
    else
        r=dataset.getValueAsDoubleArray();
    end
    val=hdb.series(typinfo.dclass(r),1.0E-6*t-t0,'Name',siginfo.Label);
else
    dataset.setInvalidValueForInteger(defval)
    t=double(dataset.getDataTimeArray());
    if typinfo.dsize == 2
        r=[dataset.getValueAsLongArray() dataset.getWriteValueAsLongArray()];
    else
        r=dataset.getValueAsLongArray();
    end
    val=hdb.series(typinfo.dclass(r),1.0E-6*t-t0,'Name',siginfo.Label);
end

val.TimeInfo.StartDate=datenum(siginfo.StartDate);
if (sz==0) || (seconds(val.Time(end)-val.Time(1)) < days(2))
    val.TimeInfo.Format='HH:MM';
else
    val.TimeInfo.Format='dd/mm';
end
val.TimeInfo.Units='seconds';
val.DataInfo.Units=siginfo.Units;
val.DataInfo.Interpolation=typinfo.interp;
val.SignalInfo=rmfield(siginfo,{'Units','StartDate'});
end
