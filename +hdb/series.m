%HDB.SERIES     HDB series object.
%This is a timeseries object with an additional 'SignalInfo' property
%containing additional properties extracted from HDB.
%
%   H = HDB.SERIES creates an empty HDB series object.
%
%   H = HDB.SERIES(DATA) creates a HDB series object H using
%   data DATA. By default, the time vector ranges from 0 to N-1,
%   where N is the number of samples, and has an interval of 1 second. The
%   default name of the TS object is 'unnamed'.
%
%   H = HDB.SERIES(NAME), where NAME is a string, creates an
%   empty time series object TS called NAME.
%
%   H = HDB.SERIES(DATA,TIME) creates a HDB series object H
%   using data DATA and time in TIME. Note: When the times
%   are date strings, the TIME must be specified as a cell array of date
%   strings. When the data contains three or more dimensions, the length of
%   the time vector must match the size of the last data dimension
%
%Constructor:
%hdb.series(data,time,'Name',name)
%
%Methods:
%datetime       Gives datatime values corresponding to each sample
%plot
%getsamples     Subset obtained by using a subscript/index array on Time
%getdata         Subset obtained by using a subscript/index array on Data

%
%Properties:
%SignalInfo     Signal properties (label, unit, format...)
%Data           Signal value (as returned by the device server)
%DisplayData    Signal values scaled for display
%Time           Signal time, in seconds from the start of the dataset
%StartDate      Beginning of the dataset

classdef series < timeseries
    
    properties
        SignalInfo=struct('DisplayUnit',1,'YLim',[-Inf Inf],'typeStr','none');    %Signal properties (unit, format...)
    end
    
    properties (Dependent=true, SetAccess=private)
        DisplayData     % Data scaled for display
        StartDate   % Starting date of the dataset
    end
    
    methods (Access=private, Hidden)
        function YL=updatelim(self,YLim)
            lim=[YLim;self.SignalInfo.YLim];
            YL=[min(lim(:,1)),max(lim(:,2))];
        end
    end
    
    methods
        function h=series(varargin)
            %hdb.series(data,time,'Name',name)
            h@timeseries(varargin{:});
        end
        
        function v=cellstr(self)
            %Cell array of strings, one line per sample
            fmt=self.SignalInfo.Format;
            ct=arrayfun(@char,self.datetime,'UniformOutput',false);
            cd=arrayfun(@(v) sprintf(fmt,v),self.DisplayData,'UniformOutput',false);
            v=[ct cd];
        end
        
        function h=getdata(self,drange)
            %Extract the selected columns from the data
            h=hdb.series(self.Data(:,drange),self.Time,'Name',self.Name);
            h.TimeInfo=self.TimeInfo;
            h.DataInfo=self.DataInfo;
            h.SignalInfo=self.SignalInfo;
        end
        
        function h=getsamples(self,range)
            %getsamples Obtain a subset of hdb.series samples as another hdb.series
            %   using a subscript/index array.
            %
            %   TSOUT = getsamples(TSIN,I) returns a timeseries obtained from the samples
            %	of the timeseries TSIN corresponding to the time(s) TSIN.TIME(I).
            h=getsamples@timeseries(self,range);
            h.Name=self.Name;
        end
        
        function dt=get.DisplayData(self)
            dt=self.SignalInfo.DisplayUnit.*self.Data;
        end
        
        function tm=get.StartDate(self)
            tm=datetime(self.TimeInfo.StartDate,'convertFrom','datenum');
        end
        
        function varargout=datetime(self,varargin)
            %Gives datatime values corresponding to each sample
            varargout=arrayfun(@dt,self,'UniformOutput',false);
            function tm=dt(h)
                tm=datetime(h.TimeInfo.StartDate,'convertFrom','datenum',varargin{:})+...
                    seconds(h.Time);
            end
        end
        
        function varargout=plot(self,varargin)
            prt=find(cellfun(@(arg) isa(arg,'char') && strcmpi(arg,'parent'),...
                varargin(1:end-1)),1);
            if ~isempty(prt)
                ax=varargin{prt+1};
            else
                ax=gca;
            end
            nb=length(self);
            hpl=plot@timeseries(self(1).SignalInfo.DisplayUnit.*self(1),...
                'DisplayName',self(1).Name,varargin{:});
            if ishold(ax)
                YLim=self(1).updatelim(get(ax,'YLim'));
            else
                YLim=self(1).SignalInfo.YLim;
            end
            
            if nb>1
                hold(ax,'on')
                for i=2:nb
                    hi=self(i);
                    YLim=hi.updatelim(YLim);
                    hpl=[hpl;plot@timeseries(hi.SignalInfo.DisplayUnit.*hi,...
                        'DisplayName',hi.Name,varargin{:})]; %#ok<AGROW>
                end
                hold(ax,'off')
            end
            if all(isfinite(YLim))
                set(ax,'YLim',YLim);
            end
            grid(ax,'on');
            out={hpl};
            varargout=out(1:nargout);
        end
        
    end
    
end
