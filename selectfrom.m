function varargout=selectfrom(matchfunc,lookedfor,list,varargin)
%SELECTFROM     Select elements from lists
%
%[L1,L2,...]=SELECTFROM(MATCHFUNC,LOOKEDFOR,INLIST,V1,V2,...)
%
%MATCHFUNC is a function defining the equivalence between LOOKEDFOR and INLIST
%   elements (for example "@strcmp" for cell arrays of strings  or "@eq" for numeric arrays)
%LOOKEDFOR is a list of desired items. L1, L2,... are the corresponding
%   values extracted from V1, V2,...
%INLIST is the list of available items. V1, V2,... are value arrays associated
%   with INLIST
%Vi arrays may be string arrays, cell arrays or numeric arrays
%
%   LOOKEDFOR and INLIST may be both arrays or cell arrays.
%   In case of several matches, the first match is used,
%   If there is no match, the returned value is  NaN for numeric arrays,
%   '' (empty string) for cell arrays of strings, or [] (empty array) for cell arrays.
%
%[...]=SELECTFROM(...,'Default',DEFAULT_VALUES) lets choose the values returned
%   for elements in LOOKEDFOR which have no match in INLIST. DEFAULT_VALUES
%   is a cell array with as many cells as input arguments, or a single cell

remaining=true(size(list));
[def,args]=getoption(varargin,'Default',{});
defargs=cellfun(@setdefault,args,'UniformOutput',false);
if ~isempty(def)
    defargs(:)=def;
end
if iscell(lookedfor)
    scanfun=@cellfun;
else
    scanfun=@arrayfun;
end
found=scanfun(@look,lookedfor);

varargout=cellfun(@setarg,args,defargs,'UniformOutput',false);

    function idx=look(v1)
        got=false(size(list));
        got(remaining)=scanfun(@(vtest) matchfunc(v1,vtest), list(remaining));
        idx=find(got,1);
        if isempty(idx), idx=0; end
        remaining=remaining & ~got;
    end
    function v=setdefault(ain)
        if iscellstr(ain)
            v={''};
        elseif iscell(ain)
            v={[]};
        else
            v=NaN;
        end
    end
    function aout=setarg(ain,defval)
        sel=[defval;ain(:)];
        aout=reshape(sel(found+1),size(lookedfor));
    end
end

