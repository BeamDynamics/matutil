function [status,out]=termappli(appli,title,dir)

[status,out]=unix(['type ' strtok(appli,' ;')]);
disp(['status(' strtok(appli,' ;') ') =' num2str(status)]);
if status == 0
    cmd='xterm ';
    if nargin >=3
        if ~isdir(dir), error('MATLAB:cd:NonExistentDirectory','Cannot CD'); end
        cmd = ['cd ' dir ' && ' cmd];
    end
    if nargin >=2, cmd=[cmd '-title "' title '" ']; end
    disp([cmd '-e ' appli]);
    [status,out]=unix([cmd '-e ' appli]);
end
