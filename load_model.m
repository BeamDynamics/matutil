function parms=load_model(type,periods,nux,nuz,ll,selected)
%MODEL=LOAD_MODEL(CODE,PERIODS,NUX,NUZ,PERIMETER,SELECTION)
%	loads optics parameters for all elements with name "CODE" (SD23,...)
%				 or all elements with type "CODE" (QP,SX,DI,...)
%
%	SELECTION indicates the desired values. The column order is:
%
%[ 1	2	3	4	5	6	7	8	9	10   ]
%[Kl theta/2pi betax alphax  phix/2pi etax    eta'x   betaz   alphaz phiz/2pi]
%	
%	Default value for SELECTION is:
%
%	[   2	      3		5	 6	8	10	   1]
%	[theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux, Kl]
%
%	This function is superseded by LOAD_ELEMS
%
if nargin < 6
   selected=[2 3 5 6 8 10 1];
end
parms=load_elems(pwd,optistruct(periods,nux,nuz,ll),type,selected);
