function result = getfield2(s,varargin)
%GETFIELD2 Get structure field contents.
%   F = GETFIELD2(S,'field',DEFVAL) returns the contents of the specified
%   field.  This is equivalent to the syntax
%   try
%       F = S.field
%   catch
%       F = DEFVAL
%   end
%
%   S must be a 1-by-1 structure.  
%
%   See also GETFIELD, SETFIELD, RMFIELD, RMFIELD2.
try
    result=getfield(s,varargin{1:end-1});
catch
    result=varargin{end};
end
end
