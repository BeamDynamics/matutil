function startmachine(rootpath)
global APPHOME

if ~isdeployed
   if nargin < 1, rootpath='/operation/machine'; end
   addpath(...
      fullfile(rootpath,'matmex'),...
      genpath(fullfile(rootpath,'matlab')),...
      genpath(fullfile(getenv('HOME'),'matlab')));%   ,'-end');

   %optpath(...
   %    fullfile(getenv('HOME'),'.','matmex'),...	% local mex
   %    fullfile('users','rf','matlab'));			% rf group m
end
set(0,'DefaultFigurePaperType','A4',...
      'DefaultFigurePaperUnit','centimeters',...
      'DefaultFigurePaperPosition',[2.5 9 16 12]);

if isempty(APPHOME), APPHOME=getenv('APPHOME'); end

function optpath(varargin)
for i=1:nargin
   if (exist(varargin{i},'dir') == 7),addpath(varargin{i}); end
end
