function v=spot(sz,varargin)
v=zeros(sz);
v(varargin{:})=1;
end
