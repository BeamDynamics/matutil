function h=plotemit(varargin)
%PLOTEMIT draws emittance ellipse
%	PLOTEMIT(x0, xp0) plot emittance described by "x0","xp0"
%	PLOTEMIT([x0 xp0]) plot emittance described by "x0","xp0"
%	PLOTEMIT(x0, xp0, drift) drifts the emittance by length "drift"
%	PLOTEMIT(x0, xp0, drift, oh, ov) centers the ellipse at [oh ov]
%	PLOTEMIT(x0, xp0, drift, [oh ov]) centers the ellipse at [oh ov]
%	PLOTEMIT(x0, xp0, drift, oh, ov, ...) uses the remaining arguments for plot

[sx,sz,args]=getnumarr(varargin{:});
[drift,args]=getnumarr(args{:});
[xc,xpc,args]=getnumarr(args{:});
t=linspace(0,2*pi,51);
xp=xpc+sz*sin(t);
x=xc+sx*cos(t)+drift*xp;
h=plot(x,xp,args{:});

function [varargout]=getnumarr(varargin)
sz=nargout-1;
if nargin >= 1 && isnumeric(varargin{1}) && length(varargin{1})==sz
    keep=2;
    k=num2cell(varargin{1});
elseif nargin >=sz && all(cellfun(@(x)isnumeric(x)&&isscalar(x), varargin(1:sz)))
    keep=sz+1;
    val=cat(1,varargin{1:sz});
    rem=varargin(keep:end);
    k=varargin(1:sz);
else
    keep=1;
    rem=varargin;
    k=num2cell(zeros(sz,1));
end
[varargout{1:nargout}]=deal(k{:},varargin(keep:end));
