function v=filecat(dirlist,fname,varname,dim)
%FILECAT	Concatenates arrays from different files
%
%V=FILECAT(DIRLIST,FILENAME,VARNAME,DIM)
%
%DIRLIST:	List of directories to look for files (Cell array of strings)
%FILENAME:	Name of the file to take from each directory
%VARNAME:	Variable name in the .mat file
%DIM:		Concatenates along the dimension DIM

v=[];
for i=1:length(dirlist)
s=load(fullfile(dirlist{i},fname));
v=cat(dim,v,s.(varname));
end
