function [qp,sx]=load_srmag(varargin)
%LOAD_SRMAG Extracts magnet strengths from an srmag file
%
%[QP,SX]=LOAD_SRMAG(FILENAME,BRO) Loads values from an srmag file,
%FILENAME:  settings file
%BRO:       beam energy [T.m]
%
%QP:    Integrated quadrupole strengths [m^-1]
%SX:    Integrated sextupole strengths [m^-2]
%
%[QP,SX]=LOAD_SRMAG(FLAGS,INPUT,BRO)
%FLAGS:  input selectors. FLAG may be:
%           'binaryfile': then INPUT is the name of the srmag file
%           'textfile': then INPUT is the name of a text file
%           'string': then INPUT is a string
%           'families' : only magnet families are set
%
%[QP,SX]=LOAD_SRMAG(...,BRO,QP0,SX0)
%QP0:   Default values for quadrupole strengths (defaults to NaN)
%SX0:   Default values for sextupole strengths (defaults to NaN)
%

inputmode='binaryfile';
individual=true;
for ia=1:length(varargin)
    switch varargin{ia}
        case {'','string','textfile','binaryfile'}
            inputmode=varargin{ia};
        case 'families'
            individual=false;
        otherwise
            if ischar(varargin{ia+1})
                inputmode=varargin{ia};
                inext=ia+1;
            else
                inext=ia;
            end
            break;
    end
end
defval={'',6.04e9/2.99792458e8,NaN,NaN};
defval(1:length(varargin)-inext+1)=varargin(inext:end);
[filename,bro,q0,s0]=deal(defval{:});
qp0(1:256,1)=q0;
sx0(1:224,1)=s0;

qfnames={'sr/ps-qf2/0';'sr/ps-qd3/0';'sr/ps-qd4/0';'sr/ps-qf5/0';'sr/ps-qd6/0';'sr/ps-qf7/0'};
sfnames={'sr/ps-s4/0';'sr/ps-s6/0';'sr/ps-s13/0';'sr/ps-s19/0';'sr/ps-s20/0';'sr/ps-s22/0';'sr/ps-s24/0'};
iqf=reshape(repmat([1;2;3;4;4;3;5;6;6;5;3;4;4;3;2;1],1,16),256,1);
isf=reshape(repmat([1;2;3;4;5;6;7;7;6;5;4;3;2;1]+6,1,16),224,1);

if individual
    qinames={'sr/ps-qf5/c22';'sr/ps-qd4/c22';'sr/ps-qd6hg/c22';'sr/ps-qf7hg/c22';...
        'sr/ps-qf7hg/c23';'sr/ps-qd6hg/c23';'sr/ps-qd4/c23';'sr/ps-qf5/c23'};
    sinames={'sr/ps-s4/c15';'sr/ps-s4/c16';'sr/ps-s22/c22-23';'sr/ps-s24/c22';...
        'sr/ps-s24/c23';'sr/ps-s22/c22-23';'sr/ps-s4/c29';'sr/ps-s4/c30'};
    iqi=149:156;
    isi=[84 85 132 133 134 135 182 183];
    values=load_setting(inputmode,filename,[qfnames;sfnames;qinames;sinames]);
    field=cellfun(@(name,value) i2gl(name,value)/bro,...
        [qfnames;sfnames;qinames;sinames],num2cell(values));
    qp=setv(qp0,field(iqf));
    sx=setv(sx0,field(isf));
    qp(iqi)=setv(qp0(iqi),field(14:21));
    sx(isi)=setv(sx0(isi),field(22:29));
else
    values=load_setting(inputmode,filename,[qfnames;sfnames]);
    field=cellfun(@(name,value) i2gl(name,value)/bro,...
        [qfnames;sfnames],num2cell(values));
    qp=setv(qp0,field(iqf));
    sx=setv(sx0,field(isf));
end
    function v=setv(v,field)
        ok=isfinite(field);
        v(ok)=field(ok);
    end
end

