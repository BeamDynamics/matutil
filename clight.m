function c = clight()
%CLIGHT Return the velocity of light [m/s]

c=299792458;
end
