function [] = SendTriggers(period,totalnumber);

[devclock, devclockSRdelay, devclockdivider, devclockTrigSrc] = openclockstuff;
pause(2);

for n=1:totalnumber;
    dvcmd(devclock,'ForcedOneShot');
    pause(period);
    n
end;

display('FINISHED with Triggers');

dvclose(devclockdivider);
dvclose(devclockSRdelay);
dvclose(devclockTrigSrc);
dvclose(devclock);
