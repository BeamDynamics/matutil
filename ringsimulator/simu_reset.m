function [qcors,scors]=simu_reset(ringpath)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

simdev=tango.Device('tango://ebs-simu:10000/sys/ringsimulator/ebs');
if nargin < 1
    ringfile=simdev.get_property('RingFile');
    ringpath=ringfile{1};
end
if ischar(ringpath)
    [~,ringname]=fileparts(ringpath);
    r=load(ringpath);
    ring=r.(ringname);
else
    ring=ringpath;
end
keep=atgetcells(ring,'Class','RingParam|RFCavity') | ...
    atgetcells(ring,'FamName','BPM_..|^ID.*|JL1[AE].*');
redring=atreduce(ring,keep);

%   Set quadrupole family strengths
qcors=zeros(1,514);
quads=redring(atgetcells(redring,'Class','Quadrupole'));
qcors=setmag('m-qf1/all',quads,'QF1[AE]',{2},qcors);
qcors=setmag('m-qd2/all',quads,'QD2[AE]',{2},qcors);
qcors=setmag('m-qd3/all',quads,'QD3[AE]',{2},qcors);
qcors=setmag('m-qf4/all',quads,'QF4[ABDE]',{2},qcors);
qcors=setmag('m-qd5/all',quads,'QD5[BD]',{2},qcors);
qcors=setmag('m-qf6/all',quads,'QF6[BD]',{2},qcors);
qcors=setmag('m-qf8/all',quads,'QF8[BD]',{2},qcors);

qcors=setmag('m-qf1/inject',quads,'QF1[IJ]',{2},qcors);
qcors=setmag('m-qd2/inject',quads,'QD2[IJ]',{2},qcors);
qcors=setmag('m-qf2/inject',quads,'QF2[IJ]',{2},qcors);
qcors=setmag('m-qd3/inject',quads,'QD3[IJ]',{2},qcors);
qcors=setmag('m-qf4/inject',quads,'QF4[IJ]',{2},qcors);

%   Set quadrupole corrections
setall('m-q/all',qcors);

%   Set sextupole family strengths
scors=zeros(1,192);
sexts=redring(atgetcells(redring,'Class','Sextupole'));
scors=setmag('m-sd1ae/all',sexts,'SD1[AE]',{3},scors);
scors=setmag('m-sd1bd/all',sexts,'SD1[BD]',{3},scors);
scors=setmag('m-sf2/all',sexts,'SF2[AE]',{3},scors);

scors=setmag('m-sd1ae/inj-outer',sexts,'SJ1A|SI1E',{3},scors);
scors=setmag('m-sf2/inj-outer',sexts,'SJ2A|SI2E',{3},scors);
scors=setmag('m-sd1bd/inj-outer',sexts,'SJ1B|SI1D',{3},scors);
scors=setmag('m-sd1bd/inj-inner',sexts,'SJ1D|SI1B',{3},scors);
scors=setmag('m-sf2/inj-inner',sexts,'SJ2E|SI2A',{3},scors);
scors=setmag('m-sd1ae/inj-inner',sexts,'SJ1E|SI1A',{3},scors);

%   Set sextupole corrections
setall('m-s/all',scors);

correctors=redring(atgetcells(redring,'FamName','SH..|S[DFIJ][12].'));
%   Set horizontal steerers
setcor('hst/all',correctors,'PolynomB',{1},-1);
%   Set vertical steerers
setcor('vst/all',correctors,'PolynomA',{1},1);
%   Set skew quad correctors
setcor('sqp/all',correctors,'PolynomA',{2},1);

%   Set the master source
rfcav=redring(atgetcells(redring,'FamName','CA05'));
setms(rfcav);

    function cors=setmag(devnm,ring,nm,order,cors)
        idx=atgetcells(ring,'FamName',nm);
        mags=ring(idx);
        strengths=  atgetfieldvalues(mags,'PolynomB',order) .* ...
            atgetfieldvalues(mags,'Length');
        avestrength=mean(strengths);
        cors(idx)=strengths-avestrength;
        fprintf('%s (%d): %g    %g\n',nm,length(mags),avestrength,std(cors));
        try
            ps=tango.Device(['tango://ebs-simu:10000/srmag/' devnm]);
            ps.FamilyStrength=avestrength;
        catch err
            fprintf('%s\n',err);
        end
    end

    function setall(devnm,strengths)
        try
            ps=tango.Device(['tango://ebs-simu:10000/srmag/' devnm]);
            ps.CorrectionStrengths=strengths;
        catch err
            fprintf('%s\n',err.message);
        end
    end

    function setcor(devnm,mags,field,order,si)
        strengths=  atgetfieldvalues(mags,field,order) .* ...
            atgetfieldvalues(mags,'Length');
        fprintf('%s (%d): %g\n',devnm, length(mags),std(strengths));
        try
            ps=tango.Device(['tango://ebs-simu:10000/srmag/' devnm]);
            ps.Strengths=(strengths' * si);
        catch err
            fprintf('%s\n',err.message);
            save(fileparts(devnm),'strengths');
        end
    end

    function setms(cav)
        freq = atgetfieldvalues(cav,'Frequency');
        fprintf('rf: %8f\n',freq);
        try
            ms = tango.Device('tango://ebs-simu:10000/srrf/master-oscillator/1');
            ms.Frequency = freq;
        catch err
            fprintf('%s\n',err.message);
        end
    end
end

