function newring=adercorsext2(ring)
%machine=adercorsext2(ring)

%filenameset=[qemres.datadir '/settings.mat'];
filenameset='settingsApr20MDT';
%load the steere calibration (h and v) factors   
%[h,v]=load_corcalib(qemres.opticsdir);
h=[4.398E-4 3.762E-4 4.368E-4 4.368E-4 3.762E-4 4.398E-4];
v=[2.693E-4 2.508E-4 2.742E-4 2.742E-4 2.508E-4 2.693E-4];
%load the steere set values (hsteerers vsteerers) from the operational file calibration factors h and v
setk=load(filenameset);

calibh=h(ones(1,16),:)
calibv=v(ones(1,16),:)
corstrengthh=calibh(:).*setk.hsteerers;
corstrengthv=calibv(:).*setk.vsteerers;
newring=adercor(ring,corstrengthh,corstrengthv);