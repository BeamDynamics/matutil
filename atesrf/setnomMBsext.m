function newring=setnomMBsext(ring)
% setapr20nom(ring)
% current, K, K/L with L=.4 m
%S4     55.7750   1.23612     3.0903
%S6    -74.0370   -1.64217   -4.1054
%S13   -39.5800   -0.87519    -2.1880
%S19   205.938     4.43087     11.0772
%S20  -193.852     -4.21939     -10.5485  
%S22  -43.0600     -0.95275     -2.3819
%S24   59.3510     1.31582       3.2895

newring=ring;
newring=setsext(newring,'S4',1.23612);
newring=setsext(newring,'S6',-1.64217);
newring=setsext(newring,'S13',-0.87519);
newring=setsext(newring,'S19',4.43087);
newring=setsext(newring,'S20',-4.21939);
newring=setsext(newring,'S22',-0.95275);
newring=setsext(newring,'S24',1.31582);