function newring=addrandsext(ring,val)
%newring=setsextall(ring,fam,val);
%val is the rms value of the scaling factor.
newring=ring;
r=1+val*rand;

newring=scalesext(newring,'S4',r);
newring=scalesext(newring,'S6',r);
newring=scalesext(newring,'S13',r);
newring=scalesext(newring,'S19',r);
newring=scalesext(newring,'S20',r);
newring=scalesext(newring,'S22',r);
newring=scalesext(newring,'S24',r);


%    if newring{l}.BetaCode=='SX'
