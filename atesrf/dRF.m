function delta = dRF(ring,Vrf)
%d = dRF(ring,Vrf)
% Vrf should be in MV
harm=992;
U0=4.88; %(MeV)
E0=6.04e3;
%alphac=mcf(ring);
alphac=1.7758e-04;

%delta =fs*circ/(300000*h*alphac)*100*SQRT(4-2*pi*(1-2*phis/180)*TAN(phis/180*pi));

delta = sqrt(2*U0/(pi*alphac*harm*E0))*sqrt( sqrt((Vrf/U0)^2-1) - acos(U0/Vrf));