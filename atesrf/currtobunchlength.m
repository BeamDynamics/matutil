function strength=currtobunchlength(settings,flag)
%calculates the bunch length in m for a given bunch current interpolating
%from the measured values.  
%flag=1 (default), converts current (mA) to bunchlength (m)
%flag=2 converts bunchlength (m) to current (mA).
if nargin==1
    flag=1;

end

curvals=[0,0.5,1.2,2,2.5,4,5.8,7,8,9.6,12.7]; %mA


 bunchlengthvals=[20,25,29,35,38,43,52,56,58,63,66]; %ps
 
 
set=abs(settings);

if flag==1
  strength=interp1(curvals,bunchlengthvals,set);
  strength=strength*3e-4;
end
if flag==2
  strength=interp1(bunchlengthvals,curvals,set); 
end