function thering=chsext(thering,sextname,newh,errorpat)
%thering=chsext(thering,sextname,newh,errorpat)
%global thering

if errorpat=='n'
    res=findcells(thering,'FamName', sextname);
    actualvalue=thering{res(1)}.PolynomB
    numel(res);
  
    for     i=1:numel(res)
        
        thering{res(i)}.PolynomB=[0 0 newh];
    end
end
if errorpat=='y'
    res=findcells(thering,'FamName', sextname);
        %actualvalue=thering{res(1)}.PolynomB
        randn('state',sum(100*clock))
        error=randn(numel(res))*newh;
    for i=1:numel(res)
        thering{res(i)}.PolynomB=thering{res(i)}.PolynomB+[0 0 error(i)];
        sextval(i,:)=thering{res(i)}.PolynomB+[0 0 error(i)];
    end
sextval
end
