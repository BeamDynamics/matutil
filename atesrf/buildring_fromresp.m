function [mach,qemb,qemres,semres]=buildring_fromresp(dirname);
%Builds a machine with quad strength errors, quadrupol corectors,
%quadrupole tilt, and skew correctors, from the result of a response matrix
%measurement.
%MACH is the output AT structure containing the machine with errors.
%DIRNAME is the directory path where the result of the measurement of the
%response matrix is done (typicaly /machfs/mdt/YEAR/DATE/respX). The measurements have to be done thru sempanel
%and qempanel
global qemb qemres semres
[qemdata,qemb,qemres]=qempanelsettom(dirname);
%qemb(2).q=qemreadq(fullfile(dirname,'race_quadco.dat'));

qemb(2).cor(:,2)=qemreadcor(fullfile(dirname,'cornew.dat'));
qemb(2)=qembuild(qemb(2),qemres);

s=load(fullfile(dirname,'skewerrors'));
    qemb(2).ks=s.ks;
    qemb(2).skewcor(:,2)=load_correction(fullfile(dirname,'skewcor.dat'),qemres.skcode);
    qemb(2).skewcor(:,end)./semres.skewl
    qemb(2).at=sembuildat(qemres.at(:),qemres,semres,qemb(2).kn,-qemb(2).cor(:,end),...	  
qemb(2).ks,qemb(2).skewcor(:,end));
qemb(2)    
plot(qemb(2).eta)  
[dispara]=getparadisp(qemb(1).at,dimeas,qemb(2).at);
data=load(fulfile(dirname,'settings.mat'));
calibst=load(fulfile(dirname,'optics/corcalib.mat'));
[dqv,str_v,I_strv]=finddispquad(data.hsteerers,data.horbit,dispara(:,1),calibst.h,50,0.2,1)    
    
    
    
    qemb(2).skewcor(:,end)./semres.skewl
    qemb(2).at=sembuildat(qemres.at(:),qemres,semres,qemb(2).kn,-qemb(2).cor(:,end),...	  
qemb(2).ks,qemb(2).skewcor(:,end),qemb(2).diptilt);
  mach=qemb(2).at
  
  
  
    function [dispara]=getparadisp(perfmach,dimeas,ermach)
        
        [ dihp,divp ] = plotdispv( perfmach); 
        [diher,diver ] = plotdispv( ermach);
        disp=[diher-dihp diver-divp];
        dispara=dimeas-disp;
        