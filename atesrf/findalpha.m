function [delta,dcTvec]=findalpha(ring,dfneg,dfpos,nper)
%[delta,dcTvec]=findalpha(ring,dfneg,dfpos,nper)
%note that as stands, this dfneg is really dfpos and v.v. because of a -
%sgn
hrf=992;
circ=844.39;
clight=2.998e8;
T0=circ/clight;
f0=hrf*(1/T0);
npts = 150;
df=(dfpos+dfneg)/npts;
dT=(hrf/f0)*(df/f0); %note, really a minus sign here
dcT=clight*dT;

dcTneg=clight*(hrf/f0)*(dfneg/f0);

dcTvec=(1:npts)*dcT;
dcTvec=dcTvec-dcTneg;
for j=1:npts;
orb=findsyncorbit(ring,dcTvec(j)/nper); % note that we need to divide the change in circumference vector by nper
delta(j)=orb(5);
end
nfit=4;
pfit=polyfit(delta,dcTvec,nfit);
%xlabel('delta','FontSize',14);
%ylabel('TC','fontsize',14);
%title('Path length $\delta$ curve','FontSize',14,'Interpreter','latex');
%plot(delta,dcTvec,'o',delta,delta.*pfit(nfit)+delta.*delta.*pfit(nfit-1)+delta.*delta.*delta.*pfit(nfit-2)+delta.*delta.*delta.*delta.*pfit(nfit-3));
alphac1=pfit(nfit)/circ;
alphac2=pfit(nfit-1)/circ;
alphac3=pfit(nfit-2)/circ;
alphac4=pfit(nfit-3)/circ;
alphac1
alphac2
alphac3
alphac4