function [tauvals4bcalc,tauvals16bcalc]=calctaufromdata
esrf=build_simple(0);
load curr4bunch.mat
load epsz4bunch.mat
load tau4bunch.mat
bl4bunch=currtobunchlength(curr4bunch);  
bl4b=bl4bunch*3e-4;%convert bl from ps to m

load curr16bunch.mat
load epsz16bunch.mat
load tau16bunch.mat
bl16bunch=currtobunchlength(curr16bunch);  
bl16b=bl16bunch*3e-4;%convert bl from ps to m

npts4b=length(curr4bunch);
npts16b=length(curr16bunch);

delta_acc4b = 0.017;
delta_acc16b = 0.017;

for j=1:npts4b
   tauvals4bcalc(j) =esrfTouschek(esrf,16,epsz4bunch(j),curr4bunch(j),bl4b(j),delta_acc4b);
end


for j=1:npts16b
   tauvals16bcalc(j) =esrfTouschek(esrf,16,epsz16bunch(j),curr16bunch(j),bl16b(j),delta_acc16b);
end
