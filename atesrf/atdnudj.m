function [dnuxdJx,dnuydJy] =atdnudj(ring)
%dnudJ = atdnudj(ring)
x=.002;
y=.002;

[LinData,NU]=atlinopt(ring,0,1);
%esrf=atreadbeta('/Users/boaznash/work/current_projects/AT/s13s20.str','CavityPass');
%nu0=NU(1)

nux0=NU(1);
nuy0=NU(2);

betax=LinData.beta(1);
betay=LinData.beta(2);
alphax=LinData.alpha(1);
alphay=LinData.alpha(2);

%nu0=dnudj(ring,0);
%nu1=dnudj(ring,x);
%nux0=nuampl(ring,0,1,0);
nux1=nuampl(ring,-x,1,0);
%nuy0=nuampl(ring,0,3,0);
nuy1=nuampl(ring,-y,3,0);

dJx=atGetJ(x,0,betax,alphax);
dJy=atGetJ(y,0,betay,alphay);
dnuxdJx=(nux1-nux0)/dJx/1000;
dnuydJy=(nuy1-nuy0)/dJy/1000;
