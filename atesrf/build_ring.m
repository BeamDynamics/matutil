function ring=build_ring(betafile,sextfile)
%ring=buildring(betafile)

%betafile='/Users/boaznash/MyDocs/work_career/current_projects/AT/esrfdata/
%s13s20thick.str'
ring=atreadbeta(betafile,'CavityPass');
ring=ring(:);
ring=change27(ring);

%ring=setapr20nom(ring);
% [lindat nu xi0]=atlinopt(esrf,0,1);

ring=setsextall(ring,sextfile);

C1320=1.0057;
C19=1.0097;

%C1320=1.0193;
%C19=1.0167;


esrf=scalesext(ring,'S13',C1320);
esrf=scalesext(ring,'S20',C1320);
esrf=scalesext(ring,'S19',C19);


% [lindat nu xi1]=atlinopt(esrf,0,1);
% to change tunes QD3, QF7
% to change chromaticity, S19, S20

ring=adddipolesext(ring,-0.0181,-0.0085);