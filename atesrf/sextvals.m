function svals=sextvals(ring)
%sextvals(ring);

indS4=findcells(ring,'FamName','S4');
indS6=findcells(ring,'FamName','S6');
indS13=findcells(ring,'FamName','S13');
indS19=findcells(ring,'FamName','S19');
indS20=findcells(ring,'FamName','S20');
indS22=findcells(ring,'FamName','S22');
indS24=findcells(ring,'FamName','S24');

svals(1)=ring{indS4(1)}.PolynomB(3);
svals(2)=ring{indS6(1)}.PolynomB(3);
svals(3)=ring{indS13(1)}.PolynomB(3);
svals(4)=ring{indS19(1)}.PolynomB(3);
svals(5)=ring{indS20(1)}.PolynomB(3);
svals(6)=ring{indS22(1)}.PolynomB(3);
svals(7)=ring{indS24(1)}.PolynomB(3);

L=0.4;
svals=svals*L;

%kS4=ring{indS4(1)}.PolynomB(3)
%kS6=ring{indS6(1)}.PolynomB(3)
%kS13=ring{indS13(1)}.PolynomB(3)
%kS19=ring{indS19(1)}.PolynomB(3)
%kS20=ring{indS20(1)}.PolynomB(3)
%kS22=ring{indS22(1)}.PolynomB(3)
%kS24=ring{indS24(1)}.PolynomB(3)