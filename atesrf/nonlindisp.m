function corbx=nonlindisp(ring,deltavals,pos)
%corbx=nonlindisp(ring,deltavals,pos)
%calculates closed orbit at ring element pos at deltavals values of energy
%deviation
corbx=[];
  for j=1:length(deltavals)
     delta=deltavals(j);
     %[lindat, nu, ksi]=atlinopt(ring,delta,(1:length(ring)));
     [lindat, nu, ksi]=atlinopt(ring,delta,pos);
     corbx=[corbx;lindat.ClosedOrbit(1)];
 end
