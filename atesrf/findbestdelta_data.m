function deltavals=findbestdelta_data(ring,sym,curr,bl,epsz,delta_acc_guesses,tau_vac,tau_meas)
%deltavals=findbestdelta_data(ring,sym,curr,bl,epsz,delta_acc_guesses,tau_vac,tau_meas)
%e.g
%deltavals=findbestdelta_data(esrf,16,curr4bunch,bl4bunch,epsz4bunch,.015:.001:.022,200,tau4bunch)

for k=1:length(curr)
deltavals(k)=findbestdelta(ring,sym,curr(k),bl(k),epsz(k),delta_acc_guesses,tau_vac,tau_meas(k));
end