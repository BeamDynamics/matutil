function plotdata=plemittance(lindata,ring,dpp,varargin)
ixz=2;
if length(varargin)>=1, ixz=varargin{1}; end
elt=length(ring);
lind=atx(ring,dpp,1:elt+1);
modemit1=1.e9*cat(1,lind.modemit);
modemit2=1.e9*emit_dls(lind);
modemit1(1,ixz)
modemit2(1,ixz)
plotdata(1).values=[modemit1(:,ixz) modemit2(:,ixz)];
plotdata(1).labels={'atx','I.M.'};
plotdata(1).axislabel='emittance';
end
