function newring=change27(ring)
ind = findcells(ring,'FamName','27');
newring=ring;
for j=1:length(ind)
    newring{ind(j)}.FamName= 'd27';
end