function strength=getHfromcur3(settings,flag)
%calculates from a calibration curve the sectupolar integrated strength
%(L/2 (dBz^2/dx^2)/Brho   (m-2)) from a current setting.
%SETTINGS is the current (A)settings (+for focussing - for defocussing)
%STRENGTH is the resulting integrated strength
% FLAG should be at 1 to convert current to strength, and to 2 to convert
% strength to current. Default value is 1.
if nargin==1
    flag=1

end
 curvals=[0,30,60,90,120,150,180,200,220,240];
brho=20.1489;
 
 Hvals=[0.05232963,4.444091,8.934575,13.411500,17.868101,22.277726,26.552151,29.131663,31.244874,33.031576]*3./brho;
 
 
 
set=abs(settings);
sig=sign(settings);

if flag==1
strength=sig.*interp1(curvals,Hvals,set);
end
if flag==2
  strength=sig.*interp1(Hvals,curvals,set); 
end