function [nux, nuz]= nlchrom2(ring,delta_vals,nper)
%example:   nlchromplot(esrf,-.04,.04,30,16,1)
steps=length(delta_vals);
nu_delta=zeros(2,steps);
for j=1:steps
[lindata,nu_delta(:,j)]=atlinopt(ring,delta_vals(j));
%[lindata,nu_delta(:,j)]=linopt(ring,min_delta+(j-1)*delta_dif);
end
nux=nper*nu_delta(1,:);
nux=nux-floor(nux);
nuz=nper*nu_delta(2,:);
%nuz=1-(nuz-floor(nuz));
nuz=nuz-floor(nuz);
