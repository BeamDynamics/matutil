function [delta,dcTvec]=findalpha2(ring,dfvec,nper)
%[delta,dcTvec]=findalpha(ring,dfneg,dfpos,nper)
%nper is the periodicity of the ring.  A fully developed ring has nper=1,
%ESRF single super-period ring has nper=16.
hrf=992;
circ=844.39;
clight=2.998e8;
T0=circ/clight;
f0=hrf*(1/T0);
npts = length(dfvec);

dcTvec=-clight*(hrf/f0)*(dfvec/f0);

for j=1:npts;
orb=findsyncorbit(ring,dcTvec(j)/nper); % note that we need to divide the change in circumference vector by nper
delta(j)=orb(5);
end
nfit=4;
pfit=polyfit(delta,dcTvec,nfit);
alphac1=pfit(nfit)/circ
alphac2=pfit(nfit-1)/circ
alphac3=pfit(nfit-2)/circ
alphac4=pfit(nfit-3)/circ