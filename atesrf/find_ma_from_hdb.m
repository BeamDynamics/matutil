function mavals=find_ma_from_hdb(ring,time1,time2,tauvac,nbunches,curr,bl,epsz,tau)
%mavals=find_ma_from_hdb(ring,time1,time2,curr,bl,epsz,tau)
%e.g. esrf=build_simple(0,'/machfs/machine/model/s13s20thick.str'); 
% mavals=find_ma_from_hdb(esrf,'16-Sep-2010 08:00:00','16-Sep-2010 09:00:00',125,16)
%mavals=find_ma_from_hdb(esrf,'2-Feb-2011 08:00:00','2-Feb-2011 08:30:00',200,868)

%nbunches= 992 for unif, 868 for 7/8, 192 for hybrid, 16 for 16b, 4 for 4b


%first get the current, vertical emittance and lifetime from hdb. compute
%time from start (unix time with change in days)
if(1)
[utime,cedata]=load_hdb(time1,time2,300,'sr/d-ct/1/current','sr/d-emit/c10/zemittance');
curr=cedata(:,1)/nbunches;
epsz=cedata(:,2);
dt=utime-utime(1);
[tbunch,tcdata]=load_hdb(time1,time2,300,'sr/d-ct/1/lifetime','sr/d-ct/1/current');
tau=tcdata(:,1)/3600;

%now compute bunchlength from current

bl=currtobunchlength(curr)*2/3;
end

%if(0)
%    load '/machfs/nash/lifetime/16b_data/curr16bunch.mat';
%    load '/machfs/nash/lifetime/16b_data/epsz16bunch.mat';
%    load '/machfs/nash/lifetime/16b_data/tau16bunch.mat';
%    load '/machfs/nash/lifetime/16b_data/bl16bunch.mat';
%end    


%plot(dt,bl,'.');

%findbestdelta(esrf, 16, .2, .0043, .01, 0:.001:.025, 150, 0, 60);
%tauvac=200;

mavals=findbestdelta_data(ring,16,curr, bl, epsz, .015:.0002:.025,tauvac,tau);