function newring=adddipolesext(ring,db3H,db3S)
%newring=adddipolesext(ring,db3H,db3S)
newring=ring;
indH=findcells(newring,'FamName','B1H');
indS=findcells(newring,'FamName','B1S');
for j=1:length(indH)
    newring{indH(j)}.PolynomB(3)= db3H;
end
for j=1:length(indS)
    newring{indS(j)}.PolynomB(3)= db3S;
end