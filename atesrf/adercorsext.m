function machine=adercorsext()
global qemres qemb

filenameset=[qemres.datadir '/settings.mat'];
%load the steere calibration (h and v) factors   
[h,v]=load_corcalib(qemres.opticsdir);
%load the steere set values (hsteerers vsteerers) from the operational file calibration factors h and v
setk=load(filenameset);

calibh=h(ones(1,16),:)
calibv=v(ones(1,16),:)
corstrengthh=calibh(:).*setk.hsteerers;
corstrengthv=calibv(:).*setk.vsteerers;
machine=adercor(qemb(2).at,corstrengthh,corstrengthv);