function newring=setquad(ring,quadfam,val);
%newring=setquad(ring,fam,val);
newring=ring;
ind=findcells(newring,'FamName',quadfam);
for j=1:length(ind)
    newring{ind(j)}.PolynomB(2)= val;
end