function plotdata=pltrack(lindata,ring,dpp,coord,ipart) %#ok<INUSL>
if nargin <5, ipart=1; end
elt=length(ring);
npart=size(coord,2);
rout=linepass(ring,coord,1:elt+1);
xx=reshape(rout(1,:),npart,elt+1)';
zz=reshape(rout(3,:),npart,elt+1)';
plotdata(1).values=[xx(:,ipart) zz(:,ipart)];
plotdata(1).labels={'horizontal','vertical'};
plotdata(1).axislabel='x/z [m]';
end
