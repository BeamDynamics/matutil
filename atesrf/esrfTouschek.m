function tauT=esrfTouschek(ring,sym,epsz,Ib,sigL,delta_acc)
%tauT=esrfTouschek(ring,sym,epsz,Ib,sigL,delta_acc)
%puts esrf parameters into touschek formula.  assumes a constant m.a.
%sym is 16 for one supercell.  
%epsz is the vertical emittance in nm
%Ib is the single bunch current in mA.
%sigL is the bunch length in meters

if nargin==1
   sym=1;
end

%freq = 352.202e6; %Hz   
circ = 844.39;
alpha = 1.78e-4;
harm = 992;
E0 = 6.04e9; %eV 
U0=4.88e6;
 Vrf = 9e6;
delta_max_rf = sqrt(2*U0/pi/alpha/harm/E0)*sqrt( sqrt((Vrf/U0).^2-1) - acos(U0./Vrf));

%delta_acc=.18;

%phi_s = asin(U0/Vrf);
%nus = sqrt(harm*Vrf*alpha*cos(phi_s)/2/pi/E0);

[td, tune,chrom] = twissring(ring,0,1:length(ring), 'chrom', 1e-5);
 dppPM=[delta_acc*ones(length(ring),1),-delta_acc*ones(length(ring),1)];

%sigL=.0042747; %multibunch
%sigL= 3.2*.0042747; %16 bunch
%sigL= 4.1*.0042747;  %4 bunch

%emit_x=4;
emit_x=4;
%coupling=.0025;
%coupling=.01;
coupling = epsz/emit_x;
sig_E=1.06e-3;


%ima=200/992; %multibunch
%ima=5.75;  %16 bunch
%ima=10;  % 4 bunch
ima=Ib; 

tauT=atcalc_TouschekPM(td,emit_x,coupling,sigL,sig_E,dppPM,ima,delta_max_rf,E0,circ)/(sym*3600);