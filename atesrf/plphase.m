function plotdata=plphase(lindata,ring,dpp,varargin) %#ok<INUSD>
phaseadv=cat(1,lindata.mu);
plotdata(1).values=phaseadv;
plotdata(1).labels={'horizontal','vertical'};
plotdata(1).axislabel='phase advance [rad]';
end
