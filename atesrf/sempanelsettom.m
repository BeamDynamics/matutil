function [semres]=sempanelsettom(qemres,qemb0)
% Adapted version of sempanelset to fit the BUILDRING_FROMRESP function


%QEMPANELSET
%
%[SEMDATA,SEMB,SEMRES]=SEMPANELSET((DIRNAME,HANDLES)
%
%QEMDATA:  Structure with fields datadir, opticsdir, steerlist, errdef, cordef
%QEMB:     Table of structures with fields q,cor,bh,bv,eta,resph,respv
%   QEMB(1): with no errors
%   QEMB(2): with errors
%QEMRES:   Empty structure to save results 
%

%semdata=[];
semres=[];
semres.wdisp=0;
%set(handles.edit1,'String',qemres.datadir);

semres.hlist=1:6:96;
semres.vlist=1:6:96
shidx=qemres.steerhidx(semres.hlist);
svidx=qemres.steervidx(semres.vlist);

semres.skewidx=qemres.sextidx(selcor(10));
skewl=getcellstruct(qemres.at(:), 'Length', semres.skewidx);
skewl(skewl<=0)=1;
semres.skewl=skewl;
quadl=getcellstruct(qemres.at(:), 'Length', qemres.qpidx);

errresponse=semderiv(qemres.at(:),qemres.qpidx,qemres.bpmidx,shidx,svidx);
etaresponse=semetaresp(@(id,v) atsettilt(qemres.at(:),id,v),qemres.bpmidx,qemres.qpidx,1.e-4);
semres.quadresponse=[errresponse.*repmat(quadl',size(errresponse,1),1);...
	etaresponse.*repmat(-0.5./qemb0.kn',size(etaresponse,1),1)];

semres.skewresponse=[semderiv(qemres.at(:),semres.skewidx,qemres.bpmidx,shidx,svidx);...
	semetaresp(@(id,v) setcellstruct(qemres.at(:),'PolynomA',id,v,2),qemres.bpmidx,semres.skewidx,1.e-4)./...
	skewl(:,ones(1,224))'];

%errok=[handles.pushbutton10 handles.pushbutton11 handles.pushbutton16];		% zero, save, load
%semdata.errdef=[handles.pushbutton10 handles.pushbutton16];		% zero, load
%errno=[handles.pushbutton3 handles.pushbutton9];				% fitq, fitd
%semdata.cordef=[handles.pushbutton13 handles.pushbutton14];		% initial, zero
%corok=[handles.pushbutton14 handles.pushbutton12 handles.pushbutton15 ...
   %handles.pushbutton17 handles.pushbutton20];					% zero, fit, save, load, retune
%corno=handles.pushbutton13;							% initial
%if exist(fullfile(qemres.datadir, 'skewnew.dat'),'file') == 2
    %corok=[corok handles.pushbutton18];
%else
    %corno=[corno handles.pushbutton18];
%end
%set([errok corok],'Enable','On');
%set([errno corno],'Enable','Off');
