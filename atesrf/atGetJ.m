function J=atGetJ(u,up,beta,alpha)
%J=atGetJ(u,up,beta,alpha) with u= x,y and up= x' or y'
%beta and alpha should be the appropriate beta or alpha value.
gamma=(1+alpha^2)/beta;
J= gamma*u^2 + 2*alpha*u*up+ beta*up^2;