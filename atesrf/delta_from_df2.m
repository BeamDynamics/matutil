function delta=delta_from_df2(ring,dfvec,nper)
%delta=delta_from_df2(ring,dfvec,nper)
hrf=992;
circ=844.39;
clight=2.998e8;
%nper = 16;
T0=circ/clight;
f0=hrf*(1/T0);
npts = length(dfvec);
dcTvec=-(clight*hrf/(f0^2))*dfvec;
delta=[];
for j=1:npts;
orb=findsyncorbit(ring,dcTvec(j)/nper); % note that we need to divide the change in circumference vector by nper
delta=[delta;orb(5)];
end
