function [curH,spos]= getcurH(ring)
%[curH,spos]= getcurH(ring)

[td, tune,chrom] = twissring(ring,0,1:length(ring)+1, 'chrom', 1e-5); 
Dx = cat(2, td.Dispersion)';
betxy = cat(1, td.beta);
alfxy = cat(1, td.alpha);

spos = cat(1,td.SPos);



curH = (Dx(:,1).^2 + (betxy(:,1).*Dx(:,2)+alfxy(:,1).*Dx(:,1)).^2)./betxy(:,1);

