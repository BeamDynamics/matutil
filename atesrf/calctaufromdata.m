function tauvals=calctaufromdata(ring,sym,curr,bl,epsz,delta_acc,tau_vac0,tau_vac_curr0)
%tauvals=calctaufromdata(ring,sym,curr,bl,epsz,delta_acc)
%e.g tauvals=calctaufromdata(esrf,16,currunif,blunif,epszunif,0.02,140);
%tau_vac_curr is the current at which tau_vac applies.  We scale with
%actual current.

if nargin==6
    tau_vac=1000;
end

npts=length(curr);

for j=1:npts
   tauvals(j) =esrfTouschek(ring,sym,epsz(j),curr(j),bl(j),delta_acc);
  if(tau_vac_curr0~=0)
      %tau_vac_curr/curr(j)
   tau_vac= tau_vac0*tau_vac_curr0/curr(j);
  end
   tauvals(j) = 1/(1/tauvals(j)+1/tau_vac);
end