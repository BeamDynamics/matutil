
function [qemdata,qemb,qemres,semres]=qempanelsettom(dirname,handles)
% Adapted version of qempanelset to fit the BUILDRING_FROMRESP function




%QEMPANELSET
%
%[QEMDATA,QEMB,QEMRES]=QEMPANELSET((DIRNAME,HANDLES)
%
%QEMDATA:  Structure with fields datadir, opticsdir, steerlist, errdef, cordef
%QEMB:     Table of structures with fields q,cor,bh,bv,eta,resph,respv
%   QEMB(1): with no errors
%   QEMB(2): with errors
%QEMRES:   Empty structure to save results
%

global MACHFS

initname=fullfile(MACHFS,'MDT','respmat','qeminit.mat');
try
   qemres=load(initname);
catch %#ok<*CTCH>
   qemres=[];
   qemres.bhgain=ones(224,1);
   qemres.khgain=ones(1,96);
   qemres.bvgain=ones(224,1);
   qemres.kvgain=ones(1,96);
   qemres.brot=zeros(224,1);
   qemres.krot=zeros(1,96);
end
qemdata=[];
qemres.datadir=dirname;
qemres.opticsdir=fullfile(dirname,'optics','');
qemres.steerlist=1:6:96;
%set(handles.edit1,'String',dirname);
qemres.skcode=10;
qemb1.q=qeminit(qemres.opticsdir);

try
   mach=atreadbeta(fullfile(qemres.opticsdir,'betamodel.str'),...
	  'IdentityPass','BndMPoleSymplectic4E2Pass','QuadMPoleFringePass');
   try
	  stgs=load(fullfile(qemres.datadir,'settings.mat'));
      qemres.tunes=stgs.tunes;
   catch
	  qemres.tunes=[0.44 0.39];
   end
   qemres.at=reshape(atfittune(mach(:),qemres.tunes,'QD6','QF7'),size(mach));	% retune the model
%  qemres.at=mach;															% no retuning

   dipfold=srfold(findcells(qemres.at(:),'BetaCode','DI'));
   qemres.dipidx=reshape(dipfold([1 4],:),1,64);

   qpidx=findcells(qemres.at(:),'BetaCode','QP');
   qemres.qpfit=(qemb1.q ~= 0);		% keep only quads with non-zero setpoint
   qemres.qpidx=qpidx(qemres.qpfit);

   thinsext=findcells(qemres.at(:),'BetaCode','LD3');
   thicksext=findcells(qemres.at(:),'BetaCode','SX');
   qemres.sextidx=sort([thinsext thicksext]);
   sxl=getcellstruct(qemres.at(:), 'Length', qemres.sextidx);
   sxl(sxl<=0)=1;

   qemres.hlist=1:6:96;
   qemres.steerhidx=qemres.sextidx(selcor(8));
   qemres.hsteerl=sxl(selcor(8));

   qemres.vlist=1:6:96;
   qemres.steervidx=qemres.sextidx(selcor(9));
   qemres.vsteerl=sxl(selcor(9));

   qemres.qcoridx=sort(qemres.sextidx(selcor(6)));
   qemres.qcorl=sxl(selcor(6));

   qemres.bpmidx=findcells(qemres.at(:),'BetaCode','PU');
qemres.skewidx=qemres.sextidx(selcor(qemres.skcode));
   qemb1.q=-getcellstruct(qemres.at(:),'K',qpidx,1);
b1idx=findcells(qemres.at(:),'FamName','B1H');
   b2idx=findcells(qemres.at(:),'FamName','B2S');
   qemres.id25idx=b1idx(22);
   qemres.d9idx=b2idx(6);

catch me
   
    qemres.at={};
   warning('qem:noinit','No BETA model in the optics directory');
end

qemb1.cor=zeros(32,1);
qemb1.skewcor=zeros(32,1);
qemb1.diptilt=zeros(length(qemres.dipidx),1);
qemb1.ks=zeros(length(qemres.qpidx),1);
% qemb1.dipangle=getcellstruct(qemb0.at,'BendingAngle',qemres.dipidx);

qemb(1)=qembuild(qemb1,qemres);


   qemb(2).q=qemreadq(fullfile(qemres.datadir,'race_quadco.dat'));	% reads initial qp strengths
   %errok=[handles.pushbutton10 handles.pushbutton11 handles.pushbutton16];	% zero, save, load
   %qemdata.errdef=[handles.pushbutton9 handles.pushbutton10 handles.pushbutton16]; % initial, zero, load
   %errno=[handles.pushbutton3 handles.pushbutton9];				% fit, initial


%qemdata.cordef=[handles.pushbutton13 handles.pushbutton14];			% initial, zero
qemb(2).cor=qemreadcor(fullfile(qemres.datadir,'magcor.dat'));    % reads correctors
qemb(2).skewcor=load_correction(fullfile(qemres.datadir,'skewcor.dat'),...
		qemres.skcode,qemres.opticsdir);
qemb(2).diptilt=qemb(1).diptilt;
% qemb(2).dipangle=qemb(1).dipangle;


mach=sembuildat(qemres.at(:),qemres,[],-qemb(2).q(qemres.qpfit),-qemb(2).cor);	% retune the model
mach=atfittune(mach,qemres.tunes,'QD6','QF7');
qemb(2).q=-getcellstruct(mach,'K',qpidx,1);
qemb(2).ks=zeros(length(qemres.qpidx),1);

semres.skewidx=qemres.sextidx(qemres.skcode);
skewl=getcellstruct(qemres.at(:), 'Length', semres.skewidx);
skewl(skewl<=0)=1;
semres.skewl=skewl;

%corok=[handles.pushbutton14 handles.pushbutton12 handles.pushbutton15 handles.pushbutton17]; % zero, fit, save, load
%corno=handles.pushbutton13;							% initial
%if exist(fullfile(qemres.datadir, 'cornew.dat'),'file') == 2
 %  corok=[corok handles.pushbutton18];
%else
 %  corno=[corno  handles.pushbutton18];
%set([errok corok handles.pushbutton2],'Enable','On');
%set([errno corno],'Enable','Off');


