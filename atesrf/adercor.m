function machine=adercor(machine,corstrengthh,corstrengthv)
%Introduces the field errors of the correctors in the model.
%MACH is the resulting AT structure with errors comming from the
%correctors.
%MACHINE=initial AT structure of the ESRF SR structure should contain
%cavities at each end of cells. The right structure has 1648 ellements.
%CORANGH=H corrector dipolar strength (A)
%CORANGV=V corrector dipolar strength (A)

corstrengthh=reshape(corstrengthh,[],1);
corstrengthv=reshape(corstrengthv,[],1);



thinsext=findcells(machine,'BetaCode','LD3');
   thicksext=findcells(machine,'BetaCode','SX');
   Isext=sort([thinsext thicksext]);
   sxl=getcellstruct(machine, 'Length', Isext);
   sxl(sxl<=0)=1;

   Icorh=Isext(selcor(8));
Icorv=Isext(selcor(9));
   
   corstrengthh=corstrengthh./sxl(selcor(8));
corstrengthv=corstrengthv./sxl(selcor(9));



polycorh=[0 0.0 -86.9 0 2.18e5]';
polycorv=[0 0.0 -34.27 0 -2.21e5]';
matpolyhn=polycorh(:,ones(1,numel(corstrengthh)));
matpolyvn=polycorv(:,ones(1,numel(corstrengthv)));
matpolyh=matpolyhn.*corstrengthh(ones(numel(polycorh,1)),:)
matpolyv=matpolyvn.*corstrengthv(ones(numel(polycorv,1)),:)


res=getcellstruct(machine,'PolynomB',Icorh,1,3);

machine=setcellstruct(machine,'PolynomB',Icorh,res'+matpolyh(3,:),1,3);
machine=setcellstruct(machine,'PolynomB',Icorh,matpolyh(5,:),1,5);
machine=setcellstruct(machine,'MaxOrder',Icorh,4);
res=getcellstruct(machine,'PolynomA',Icorv,1,3);
machine=setcellstruct(machine,'PolynomA',Icorv,res'+matpolyv(3,:),1,3);
machine=setcellstruct(machine,'PolynomA',Icorv,matpolyv(5,:),1,5);
machine=setcellstruct(machine,'MaxOrder',Icorv,4);


