function dppPM=makedelta(ring,d1,d2,d3)
%dppPM=makedelta(ring,d1,d2,d3)

%find H function from optics
 [lindat, nu, ksi]=atlinopt(ring,0,1:length(ring));
  eta=cat(2,lindat.Dispersion);
  etax=eta(1,:)';
  etaxp=eta(2,:)';
  beta=cat(1,lindat.beta);
  betax=beta(:,1);
  alpha=cat(1,lindat.alpha);
  alphax=alpha(:,1);
  gammax=(1+alphax.^2)./betax;
  Hx=gammax.*etax.*etax+2*alphax.*etax.*etaxp+betax.*etaxp.*etaxp;
  
  dppPM=[ones(length(ring),1),-ones(length(ring),1)];
  

  dipind=findcells(ring,'BetaCode','DI');
  
  
  deltavals=[d1,d2,d3,d2,d1];
  
 count=1; 
 for j=1:(length(ring))
    if ismember(j,dipind)
       bval = (deltavals(floor(count))+deltavals(floor(count)+1))/2;
       dppPM(j,1)=bval;
       dppPM(j,2)=-bval;
       count=count+1/2;
    else
    dppPM(j,1)=deltavals(count);
    dppPM(j,2)=-deltavals(count);
    
    end
 end
 
  
 
  
