function ring=build_simple(expand,betafile)
%ring=buildring(betafile)

if nargin==1
    betafile='/Users/boaznash/MyDocs/work_career/current_projects/AT/data/s13s20thick.str';
end
%in CTRM, betafile = '/machfs/machine/model/s13s20thick.str'
    ring=atreadbeta(betafile,'CavityPass');
ring=change27(ring);
 ring=setsextall(ring,'s13ds20calib.sext');
 if(expand==1)
     ring=ring(:);
 end
ring=setnomMBsext(ring);
