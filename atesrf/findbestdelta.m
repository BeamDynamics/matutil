function delta=findbestdelta(ring,sym,curr,bl,epsz,delta_acc_guesses,tau_vac,tau_meas)
%tauvals=findbestdelta(ring,sym,curr,bl,epsz,delta_acc_guesses,tau_vac,tau_meas)
%e.g tauvals=calctaufromdata(esrf,16,currunif,blunif,epszunif,0.02,140,40);
%tau_vac_curr is the current at which tau_vac applies.  We scale with
%actual current.

nguess=length(delta_acc_guesses);

for j=1:nguess
tguess =esrfTouschek(ring,sym,epsz,curr,bl,delta_acc_guesses(j));
tau_total_guess(j) = 1/(1/tguess+1/tau_vac);
tt(j)=tau_total_guess(j)/tau_meas-1;
end

[val,ind]=min(abs(tt));

delta = delta_acc_guesses(ind);
